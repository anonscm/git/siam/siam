--
-- Copyright (C) 2011 tarent Solutions <info@tarent.de>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

DROP TABLE IF EXISTS "public"."actionmatchtype" CASCADE;
DROP TABLE IF EXISTS "public"."actionstype" CASCADE;
DROP TABLE IF EXISTS "public"."actiontype" CASCADE;
DROP TABLE IF EXISTS "public"."applytype" CASCADE;
DROP TABLE IF EXISTS "public"."applytypeexpressionitem" CASCADE;
DROP TABLE IF EXISTS "public"."attributeassignmenttype" CASCADE;
DROP TABLE IF EXISTS "public"."attributedesignatortype" CASCADE;
DROP TABLE IF EXISTS "public"."attributeselectortype" CASCADE;
DROP TABLE IF EXISTS "public"."attributevaluetype" CASCADE;
DROP TABLE IF EXISTS "public"."attributevaluetypecontentitem" CASCADE;
DROP TABLE IF EXISTS "public"."combinerparameterstype" CASCADE;
DROP TABLE IF EXISTS "public"."combinerparametertype" CASCADE;
DROP TABLE IF EXISTS "public"."conditiontype" CASCADE;
DROP TABLE IF EXISTS "public"."defaultstype" CASCADE;
DROP TABLE IF EXISTS "public"."environmentmatchtype" CASCADE;
DROP TABLE IF EXISTS "public"."environmentstype" CASCADE;
DROP TABLE IF EXISTS "public"."environmenttype" CASCADE;
DROP TABLE IF EXISTS "public"."expressiontype" CASCADE;
DROP TABLE IF EXISTS "public"."functiontype" CASCADE;
DROP TABLE IF EXISTS "public"."idreferencetype" CASCADE;
DROP TABLE IF EXISTS "public"."obligationstype" CASCADE;
DROP TABLE IF EXISTS "public"."obligationtype" CASCADE;
DROP TABLE IF EXISTS "public"."policycombinerparameterstype" CASCADE;
DROP TABLE IF EXISTS "public"."policysetcombinerparameterst_0" CASCADE;
DROP TABLE IF EXISTS "public"."policysettype" CASCADE;
DROP TABLE IF EXISTS "public"."policysettypepolicysetorpoli_0" CASCADE;
DROP TABLE IF EXISTS "public"."policytype" CASCADE;
DROP TABLE IF EXISTS "public"."policytypecombinerparameters_0" CASCADE;
DROP TABLE IF EXISTS "public"."pstrepresentationtype" CASCADE;
DROP TABLE IF EXISTS "public"."resourcematchtype" CASCADE;
DROP TABLE IF EXISTS "public"."resourcestype" CASCADE;
DROP TABLE IF EXISTS "public"."resourcetype" CASCADE;
DROP TABLE IF EXISTS "public"."rulecombinerparameterstype" CASCADE;
DROP TABLE IF EXISTS "public"."ruletype" CASCADE;
DROP TABLE IF EXISTS "public"."subjectattributedesignatorty_0" CASCADE;
DROP TABLE IF EXISTS "public"."subjectmatchtype" CASCADE;
DROP TABLE IF EXISTS "public"."subjectstype" CASCADE;
DROP TABLE IF EXISTS "public"."subjecttype" CASCADE;
DROP TABLE IF EXISTS "public"."targettype" CASCADE;
DROP TABLE IF EXISTS "public"."variabledefinitiontype" CASCADE;
DROP TABLE IF EXISTS "public"."variablereferencetype" CASCADE;
DROP sequence IF EXISTS hibernate_sequence;