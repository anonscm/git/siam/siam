/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.policy_repo;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.osiam.policy_repository.PSTRepresentationType;

/**
 * Session Bean implementation class DocumentService.
 * 
 * @author Joscha Haering tarent GmbH
 * @author Christian Preilowski (c.preilowski@tarent.de)
 */
@Stateless
@WebService(name = "PolicyRepoService",
		portName = "PolicySetServicePort", 
		serviceName = "PolicyRepoService", 
		targetNamespace = "http://policy_repo.osiam.org/")
@Remote(PolicySetService.class)
public class PolicySetServiceImpl implements PolicySetService {
	
	private static final String XACML_NAMESPACE = "urn:oasis:names:tc:xacml:2.0:policy:schema:os";
	private static final String POLICYSET_LOCAL_NAME = "PolicySet";
	
	//named queries
	private static final String RETRIEVE_POLICY_SET_QUERY = "retrievePolicySet";
	private static final String RETRIEVE_POLICY_SET_LIST_QUERY = "retrievePolicySetList";

	private static Logger logger = Logger.getLogger(PolicySetServiceImpl.class.getName());

	@PersistenceContext(unitName = "policy_repo")
	protected EntityManager em;


	/* (non-Javadoc)
	 * @see org.osiam.policy_repo.PolicySetService#addPolicySet(java.lang.String, oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType)
	 */
	@WebMethod
	public void addPolicySet(@WebParam(name = "realm") String realm,
			@WebParam(name = "policySet") PolicySetType policySet){
		
		logger.log(Level.INFO,"adding new policy set with id '" + policySet.getPolicySetId()
						+ "' to realm '" + realm + "'");
		// Create new entry
		PSTRepresentationType policyRep = new PSTRepresentationType();
		policyRep.setPolicySetType(policySet);
		policyRep.setRealm(realm);
		em.persist(policyRep);
	}

	/* (non-Javadoc)
	 * @see org.osiam.policy_repo.PolicySetService#removePolicySet(java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void removePolicySet(
			@WebParam(name = "realm") String realm,
			@WebParam(name = "policySetId") String policySetId) {
		
		logger.log(Level.INFO, "removing policy set with id '" + policySetId
				+ "' of realm '" + realm + "'");
		// Query query =
		// em.createQuery("delete PSTRepresentationType p where p.realm=? AND p.policySetType.policySetId=?");
		// query.executeUpdate();
		// Workaround because of bug HHH-2812
		// http://opensource.atlassian.com/projects/hibernate/browse/HHH-2812	
		Query query = em.createNamedQuery(RETRIEVE_POLICY_SET_QUERY);
		query.setParameter(1, realm);
		query.setParameter(2, policySetId);
		PSTRepresentationType pstRep = (PSTRepresentationType) query.getSingleResult();
		em.remove(pstRep);
	}

	/* (non-Javadoc)
	 * @see org.osiam.policy_repo.PolicySetService#updatePolicySet(java.lang.String, java.lang.String, oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType)
	 */
	@WebMethod
	public void updatePolicySet(@WebParam(name = "realm") String realm,
			@WebParam(name = "policySetId") String policySetId,
			@WebParam(name = "policySet") PolicySetType policySet){
		
		logger.log(Level.INFO, "updating policy set with id '" + policySetId
				+ "' of realm '" + realm + "'");
		//delete old 
		Query query = em.createNamedQuery(RETRIEVE_POLICY_SET_QUERY);
		query.setParameter(1, realm);
		query.setParameter(2, policySetId);
		PSTRepresentationType pstRep = (PSTRepresentationType) query.getSingleResult();
		em.remove(pstRep);
		//explicitly commit transaction because hibernate reorders statements -> violation of unique constraints
		// see hibernate bug report: http://opensource.atlassian.com/projects/hibernate/browse/HHH-2801
		em.flush();
		// create new entry
		PSTRepresentationType policyRep = new PSTRepresentationType();
		policyRep.setPolicySetType(policySet);
		policyRep.setRealm(realm);
		em.persist(policyRep);
		
	}
	
	/* (non-Javadoc)
	 * @see org.osiam.policy_repo.PolicySetService#retrievePolicySetList(java.lang.String)
	 */
	@WebMethod
	public List<PolicySetType> retrievePolicySetList(
			@WebParam(name = "realm") String realm){
		
		logger.log(Level.INFO, "loading policy set  list for realm'" + realm + "'");
		List<PolicySetType> policySetList = new ArrayList<PolicySetType>();
		Query query = em.createNamedQuery(RETRIEVE_POLICY_SET_LIST_QUERY);
		query.setParameter(1, realm);

		@SuppressWarnings("unchecked")
		List<PSTRepresentationType> pstListReturn = (List<PSTRepresentationType>) query.getResultList();

		// this is a ugly workaround to enforce eager loading the policy set and
		// all depending objects
		for (PSTRepresentationType pstRepresentation : pstListReturn) {
			try {
				PolicySetType currentPolicySet = pstRepresentation.getPolicySetType();
				// serialize complete policy set
				getStringFromPolicySet(currentPolicySet);
				policySetList.add(currentPolicySet);
			} catch (JAXBException e) {
				logger.log(Level.ERROR,"exception while serializing policy set type to enforce eager loading");
				throw new RuntimeException(e);
			}
		}

		return policySetList;
	}

	/* (non-Javadoc)
	 * @see org.osiam.policy_repo.PolicySetService#retrievePolicySet(java.lang.String, java.lang.String)
	 */
	@WebMethod
	public PolicySetType retrievePolicySet(
			@WebParam(name = "realm") String realm,
			@WebParam(name = "policySetId") String policySetId){

		logger.log(Level.INFO, "loading policy set with id '" + policySetId
				+ "' of realm '" + realm + "'");
		Query query = em.createNamedQuery(RETRIEVE_POLICY_SET_QUERY);
		query.setParameter(1, realm);
		query.setParameter(2, policySetId);
		PSTRepresentationType pstReturn = null;

		pstReturn = (PSTRepresentationType) query.getSingleResult();

		PolicySetType policySet = pstReturn.getPolicySetType();
		// this is a ugly workaround to enforce eager loading the policy set and
		// all depending objects
		try {
			// serialize complete policy set
			getStringFromPolicySet(policySet);
		} catch (JAXBException e) {
			logger.log(Level.ERROR,"exception while serializing policy set type to enforce eager loading");
			throw new RuntimeException(e);
		}
		return policySet;
	}



	/**
	 * convert policy set to string.
	 * 
	 * @param policySetType policy set  
	 * @return policy as string
	 * @throws JAXBException JAXBException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String getStringFromPolicySet(PolicySetType policySetType)
			throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		StringWriter writer0 = new StringWriter();
		context.createMarshaller().marshal(
				new JAXBElement(new QName(XACML_NAMESPACE, POLICYSET_LOCAL_NAME),PolicySetType.class, policySetType), 
				writer0);
		return writer0.toString();
	}

}
