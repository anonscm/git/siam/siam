/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.policy_repo;

import java.rmi.Remote;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

/**
 * policy set service session bean.
 *
 * @author Joscha Haering tarent GmbH
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */

@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface PolicySetService extends Remote {
	
	 /**
	  * add a policy set to database.
	  *
	  * @param realm The realm to which the Set belongs
	  * @param policySet The policy set to be added
	  */
	@WebMethod
	public void addPolicySet (
			@WebParam(name = "realm")String realm, 
			@WebParam(name = "policySet")PolicySetType policySet);
	
	 /**
	  * remove a policy set to database.
	  *
	  * @param realm The realm to which the document belongs
	  * @param policySetId The id of the to be removed policy set
	  */
	@WebMethod
	public void removePolicySet (
			@WebParam(name = "realm")String realm, 
			@WebParam(name = "policySetId")String policySetId);

	 /**
	  * update a policy set.
	  *
	  * @param realm The realm to which the policy set belongs
	  * @param policySetId The policy set ID
	  * @param policySet The policy set to be updated
	  */
	@WebMethod
	public void updatePolicySet (
			@WebParam(name = "realm")String realm, 
			@WebParam(name = "policySetId")String policySetId, 
			@WebParam(name = "policySet")PolicySetType policySet);

	/**
	 * get a List of the realms policy sets.
	 *
	 * @param realm The realm
	 * @return list of policy sets
	 */
	@WebMethod
	public List<PolicySetType> retrievePolicySetList (
			@WebParam(name = "realm")String realm);

	 /**
	  * get a policy set.
	  *
	  * @param realm The realm to which the policy set belongs
	  * @param policySetId the id of the requested policy set
	  * @return Set with the given policySetId
	  */
	@WebMethod
	public PolicySetType retrievePolicySet (
			@WebParam(name = "realm")String realm, 
			@WebParam(name = "policySetId")String policySetId);

}
