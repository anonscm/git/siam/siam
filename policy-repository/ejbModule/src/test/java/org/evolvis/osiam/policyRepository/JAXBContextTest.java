/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.osiam.policyRepository;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

import org.junit.Assert;
import org.junit.Test;

public class JAXBContextTest {
	@Test
	public void testCreateJAXBContext() throws Exception {
		Assert.assertNotNull(JAXBContext.newInstance(PolicySetType.class));
	}

	@Test
	public void testUnMarshal() throws Exception {
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);

		StringWriter writer0 = new StringWriter();
		PolicySetType policySetType = new PolicySetType();
		policySetType.setDescription("testDescription");
		context.createMarshaller().marshal(new JAXBElement(
				  new QName("urn:oasis:names:tc:xacml:2.0:policy:schema:os","PolicySet"), PolicySetType.class, policySetType), writer0);
		
		// print XML
		System.out.println(writer0);
		Assert.assertNotNull(writer0.toString());
		
		JAXBElement<PolicySetType> policySetTypeJaxBElement = (JAXBElement<PolicySetType>) context.createUnmarshaller().unmarshal(new StringReader(writer0.toString()));
		PolicySetType policySetTypeFromXml = policySetTypeJaxBElement.getValue();
		Assert.assertEquals(policySetType.getDescription(), "testDescription"); 
	}
}
