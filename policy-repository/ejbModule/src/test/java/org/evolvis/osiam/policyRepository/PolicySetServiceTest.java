/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.osiam.policyRepository;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.NoResultException;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.osiam.policy_repo.PolicySetService;
import org.osiam.policy_repository.PSTRepresentationType;

public class PolicySetServiceTest  {

	protected Context jndiContext;

	@Before
	public void setUp() throws Exception {
		Properties properties = new Properties();

		properties.setProperty("openejb.altdd.prefix", "test");

		properties.setProperty("openejb.validation.output.level", "VERBOSE");

		properties.setProperty("policy_repo.hibernate.hbm2ddl.auto",
				"create-drop");
		properties.setProperty("policy_repo.hibernate.dialect",
				"org.hibernate.dialect.HSQLDialect");
		properties.setProperty("policy_repo.openjpa.Compatibility",
				"storeMapCollectionInEntityAsBlob=true");
		properties.setProperty("policy_repo.openjpa.jdbc.SynchronizeMappings",
				"buildSchema");
		properties.setProperty("policy_repo.openjpa.Log",
				"DefaultLevel=TRACE,SQL=TRACE");

		InitialContext localContext = null;
		try {
			localContext = new InitialContext(properties);
		} catch (NamingException e) {
			e.printStackTrace();
		}
		setJndiContext(localContext);

	}

	private void setJndiContext(InitialContext jndiContext) {
		this.jndiContext = jndiContext;
	}

	private PolicySetService myPolicySetService() throws Exception {
		Object obj = jndiContext.lookup("PolicySetServiceImpl/remote");

		Assert.assertNotNull(obj);
		// assertTrue(obj instanceof PolicySetServiceImpl);
		return (PolicySetService) obj;
	}

	@Test
	public void testAddPolicySet() throws Exception {
		PolicySetService policySetService = myPolicySetService();

		PSTRepresentationType pst = getTestPSTRepresentation();
		pst.setRealm("testrealm");
		String realm = pst.getRealm();
		PolicySetType policySetType = pst.getPolicySetType();
		policySetService.addPolicySet(realm, policySetType);

		// expects that retrievePolicySet works without errors
		PolicySetType policyActual = policySetService.retrievePolicySet(
				"testrealm", "PolicySetId");
		Assert.assertEquals(policySetType, policyActual);

	}

	@Test
	public void testRemovePolicySet() throws Exception {
		PolicySetService policySetService = myPolicySetService();
		// create Test PolicySet and add it to database
		PolicySetType policySet = getTestPolicySet();
		policySet.setPolicySetId("PolicySetId_TestRemove");
		policySetService.addPolicySet("realm_removetest", policySet);

		// Remove PolicySet
		policySetService.removePolicySet("realm_removetest",
				policySet.getPolicySetId());

		// check if PolicySet is still in database
		try {
			policySetService.retrievePolicySet("realm_removetest", "PolicySetId");
		} catch (UndeclaredThrowableException e) {
			//TODO make this nicer
			//the cause is a NoResultException
			Assert.assertTrue(e.getCause().getCause() instanceof NoResultException);
		}

	}

	@Test
	public void testRetrievePolicySet() throws Exception {
		PolicySetService policySetService = myPolicySetService();
		PSTRepresentationType pstRep = getTestPSTRepresentation();
		policySetService.addPolicySet(pstRep.getRealm(),
				pstRep.getPolicySetType());

		PolicySetType policySetTypeActual = policySetService.retrievePolicySet(
				pstRep.getRealm(), pstRep.getPolicySetType().getPolicySetId());

		Assert.assertEquals(pstRep.getPolicySetType(), policySetTypeActual);
	}

	@Test
	public void testRetrievePolicySetlist() throws Exception {
		PolicySetService policySetService = myPolicySetService();

		PSTRepresentationType pst = getTestPSTRepresentation();
		pst.setRealm("testRealm");

		ArrayList<PolicySetType> policyListExpected = new ArrayList<PolicySetType>();

		// create 5 different PolicySets
		for (int i = 0; i < 5; i++) {
			// create PolicySet
			PolicySetType policySet = getTestPolicySet();
			// add it to expected list
			policyListExpected.add(policySet);
			// persist it to the database
			policySetService.addPolicySet(pst.getRealm(),
					policyListExpected.get(i));
		}
		// get the list of the realm
		List<PolicySetType> policyListActual = policySetService
				.retrievePolicySetList(pst.getRealm());
		// compare expected list with the list, retrieved from the server
		Assert.assertEquals(policyListExpected, policyListActual);
	}

	@Test
	public void testUpdatePolicySet() throws Exception {
		PolicySetService policySetService = myPolicySetService();
		// create Test PSTRepresentation
		PSTRepresentationType pst = getTestPSTRepresentation();
		PolicySetType policySetExpected = pst.getPolicySetType();
		policySetExpected.setPolicySetId("PolicySetId_UpdateTest");
		// add it to the db
		policySetService.addPolicySet(pst.getRealm(), policySetExpected);

		// change policyset and update db
		policySetExpected.setDescription("Neue Description!");
		policySetService.updatePolicySet(pst.getRealm(),
				policySetExpected.getPolicySetId(), policySetExpected);
		// get policySet from DB
		PolicySetType policySetActual = policySetService.retrievePolicySet(
				pst.getRealm(), policySetExpected.getPolicySetId());

		// compare
		Assert.assertEquals(policySetExpected, policySetActual);
	}

	/**
	 * Create PolicySet for testing
	 * 
	 * @return PolicySetType
	 */
	private PolicySetType getTestPolicySet() {
		PolicySetType policySet = new PolicySetType();
		policySet.setDescription("Description");
		policySet.setPolicySetId("PolicySetId");
		return policySet;
	}

	/**
	 * Create PSTRepresentation for testing
	 * 
	 * @return PSTRepresentationType
	 */
	private PSTRepresentationType getTestPSTRepresentation() {
		PSTRepresentationType pstReturn = new PSTRepresentationType();
		pstReturn.setRealm("testrealm");
		PolicySetType policySet = getTestPolicySet();
		pstReturn.setPolicySetType(policySet);
		return pstReturn;
	}

}
