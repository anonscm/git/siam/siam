/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.policy_repo.ws_client;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.osiam.policy_repo.PolicySetService;

/**
 * policy repo client factory.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class PolicyRepoClientFactory {

	private static Logger logger = Logger.getLogger(PolicyRepoClientFactory.class.getName());

	private static final PolicyRepoClientFactory INSTANCE = new PolicyRepoClientFactory();

	private PolicyRepoClientFactory() {
	}

	public static PolicyRepoClientFactory getInstance() {
		return INSTANCE;
	}

	/**
	 * returns a port to a policy set service with the given endpoint.
	 * 
	 * @param endPoint the endpoint
	 * @return the port
	 * @throws MalformedURLException MalformedURLException
	 */
	public PolicySetService getPolicyRepoPort(String endPoint){

		logger.log(Level.INFO, "opening port to policy repo at '" + endPoint+ "'");
		
		//standard way to get a port causes an error when used in OpenAM
//		PolicySetService port;
//		URL wsdlUrl = new URL(endPoint + "?wsdl");
//		PolicyRepoService service = new PolicyRepoService(wsdlUrl);
//		port = service.getPolicySetServicePort();
//		((BindingProvider) port).getRequestContext().put(
//				BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
//		return port;
		
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
    	factory.setServiceClass(PolicySetService.class);
    	factory.setAddress(endPoint);
    	PolicySetService port = (PolicySetService) factory.create();
    	return port;

	}

}
