/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.policy_repo.ws_client.demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import org.osiam.policy_repo.PolicySetService;
import org.osiam.policy_repo.ws_client.PolicyRepoClientFactory;


import oasis.names.tc.xacml._2_0.policy.schema.os.ActionMatchType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeDesignatorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeSelectorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeValueType;
import oasis.names.tc.xacml._2_0.policy.schema.os.DefaultsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.EffectType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ObligationType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ObligationsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;
import oasis.names.tc.xacml._2_0.policy.schema.os.TargetType;

import oasis.names.tc.xacml._2_0.policy.schema.os.ObjectFactory;

/**
 * demo class for policy repo usage.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class DemoClient {
	
	private static final String XACML_NAMESPACE = "urn:oasis:names:tc:xacml:2.0:policy:schema:os";
	private static final String POLICYSET_LOCAL_NAME = "PolicySet";
	
	private static final String WS_ENDPOINT = "set_me";
	private static final String REALM = "/";

	private static Logger logger = Logger.getLogger(DemoClient.class.getName());
	
	private PolicySetService port;
	
	/**
	 * inits ws-port.
	 */
	public DemoClient(){
	    port = PolicyRepoClientFactory.getInstance().getPolicyRepoPort(WS_ENDPOINT);
	}

	/**
	 * demo main method.
	 * 
	 * @param args args
	 * @throws JAXBException JAXBException
	 */
	public static void main(String[] args) throws JAXBException{
		String policyName = "urn:oasis:names:tc:xacml:2.0:example:policysetid:1";
		String policyFileName ="src/main/resources/example-xacml-policySet.xml";
		DemoClient demo = new DemoClient();
		
//		demo.addPolicyFromFile(policyFileName, REALM);
//		demo.printPolicy(REALM, policyName);
//		demo.deletePolicySet(REALM, policyName);
//		demo.updatePolicyXacml();
		demo.printPoliciesForRealm(REALM);
	}

	/**
	 * update demo method.
	 * 
	 * @param policySetID policySetID
	 * @throws JAXBException JAXBException
	 */
	public void updatePolicyXacml(String policySetID) throws JAXBException{
		//Create a new policy set with the same id
		PolicySetType policyToAdd = getObject(policySetID);
		//Update 
		port.updatePolicySet("/", policySetID, policyToAdd);
		
	}
	
	private static PolicySetType getObject(String policySetID) {
		PolicySetType pst = new PolicySetType();
		pst.setDescription("policy set description");
		pst.setPolicySetId(policySetID);

		ObligationsType ots = new ObligationsType();

		ObligationType ot = new ObligationType();
		ot.setObligationId("otId");
		ot.setFulfillOn(EffectType.PERMIT);

		ots.getObligation().add(ot);

		pst.setObligations(ots);

		pst.setPolicyCombiningAlgId("combinId");

		DefaultsType dt = new DefaultsType();
		dt.setXPathVersion("xpath version");
		
		pst.setPolicySetDefaults(dt);

		TargetType tt = new TargetType();

		ActionsType ats = new ActionsType();
		
		ActionType at = new ActionType();
		
		ActionMatchType amt = new ActionMatchType();
		
		AttributeDesignatorType adt = new AttributeDesignatorType();
		adt.setAttributeId("attributeDesignatorId");
		adt.setDataType("dataType");
		adt.setIssuer("issuer");
		adt.setMustBePresent(true);
		amt.setActionAttributeDesignator(adt);
		
		AttributeSelectorType ast = new AttributeSelectorType();
		ast.setDataType("data type");
		ast.setMustBePresent(false);
		ast.setRequestContextPath("/");
		amt.setAttributeSelector(ast);
		
		AttributeValueType avt = new AttributeValueType();
		avt.setDataType("datatype");		
		amt.setAttributeValue(avt);
		
		amt.setMatchId("matchId");
		
		at.getActionMatch().add(amt);
		
		
		ats.getAction().add(at);
		tt.setActions(ats);
		pst.setTarget(tt);

		pst.setVersion("version");

		return pst;
	}

	/**
	 * deletes a policy set.
	 * 
	 * @param realm realm
	 * @param policySetId policy set id
	 */
	public void deletePolicySet(String realm,String policySetId ){
		port.removePolicySet(realm, policySetId);
	}

	/**
	 * print policy with given id.
	 * 
	 * @param policySetId policySetId
	 * @param realm realm
	 */
	public void printPolicy(String realm, String policySetId){
		logger.log(Level.INFO,"printing policy with id: "+policySetId+" for realm "+realm);
		PolicySetType policySet = port.retrievePolicySet(realm, policySetId);
		try {
			logger.log(Level.INFO,"policy for realm '"+realm+"': "+this.getStringFromPolicySet(policySet));	
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * print all policies for a given realm.
	 * 
	 * @param realm the realm
	 */
	public void printPoliciesForRealm(String realm){
		logger.log(Level.INFO,"printing policies for realm "+realm);
		List<PolicySetType> policySets = port.retrievePolicySetList(realm);
		try {
			for (PolicySetType policySet : policySets){
				logger.log(Level.INFO,"policy for realm '"+realm+"': "+this.getStringFromPolicySet(policySet));	
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * adds a policy given as file to a realm.
	 * 
	 * @param filename filename
	 * @param realm realm
	 */
	public void addPolicyFromFile(String filename, String realm){
		String policySetAsString ="";
		try {
			policySetAsString = DemoClient.readFileAsString(filename);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		PolicySetType policySet = null;
		try {
			policySet = this.getPolicySetFromString(policySetAsString);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		//wrap policySet with a wrapper policy set
//		String wrapperDescription = "description test";
//		PolicySetType wrapperPolicySet = this.wrapPolicy(policySet, policyWrapperName, wrapperDescription);
//		port.addPolicySet(realm, wrapperPolicySet);
		port.addPolicySet(realm, policySet);
	}
	
	/**
	 * wraps a given policy set.
	 * 
	 * @param innerPolicySet inner policy set
	 * @param wrapperPolicySetName name of wrapper policy set
	 * @param description description of wrapper policy set
	 * @return
	 */
	private PolicySetType wrapPolicy(PolicySetType innerPolicySet, String wrapperPolicySetName, String description){
		PolicySetType wrapperPolicySet = new PolicySetType();
		ObjectFactory objFactory = new ObjectFactory();
		JAXBElement<PolicySetType> jaxbPolicySetType = objFactory.createPolicySet(innerPolicySet);
		wrapperPolicySet.getPolicySetOrPolicyOrPolicySetIdReference().add(jaxbPolicySetType);
		wrapperPolicySet.setPolicySetId(wrapperPolicySetName);
		wrapperPolicySet.setDescription(description);
		return wrapperPolicySet;
	}
	
	private static String readFileAsString(String filePath)
    	throws java.io.IOException{
        StringBuffer fileContent = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buffer = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buffer)) != -1){
            String readData = String.valueOf(buffer, 0, numRead);
            fileContent.append(readData);
            buffer = new char[1024];
        }
        reader.close();
        return fileContent.toString();
    }

	/**
	 * convert xml-string to policy set.
	 * 
	 * @param policySetString
	 * @return
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	private PolicySetType getPolicySetFromString(String policySetString)
		throws JAXBException{
		JAXBElement<PolicySetType> policySetTypeJaxBElement;
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		policySetTypeJaxBElement = (JAXBElement<PolicySetType>) context.createUnmarshaller().unmarshal(new StringReader(policySetString));
		PolicySetType policySetTypeFromXml = policySetTypeJaxBElement.getValue();
		return policySetTypeFromXml;
	}
	
	/**
	 * convert policy set to string.
	 * 
	 * @param policySetType
	 * @return
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	private String getStringFromPolicySet(PolicySetType policySetType)
		throws JAXBException{
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		StringWriter writer0 = new StringWriter();
		context.createMarshaller().marshal(new JAXBElement(new QName(XACML_NAMESPACE,POLICYSET_LOCAL_NAME), PolicySetType.class, policySetType), writer0);
		return writer0.toString();
	}
	
}
