#!/bin/bash
#
# Copyright (C) 2011 tarent Solutions <info@tarent.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Script to make hjid XmlTransient in generated classes because the corresponding hyperjaxb3 feature does not work
# Author Jens Neumaier

# search for classes with hjid fields
for i in $( grep -r -l "Long hjid" target/generated-sources ); do
	echo "Processing $i..."
	# import XmlTransient and include XmlTransient annotation at hjid field
	sed 's/\(javax.xml.bind.annotation.XmlAttribute;\)/\1\nimport javax.xml.bind.annotation.XmlTransient;/' $i |
	sed 's/^\(.*\)\(@XmlAttribute(name = "Hjid")\)/\1@XmlTransient/g' >$i\temp; 
	# move temp copy to actual file position
	mv $i\temp $i;
	echo "Successfully made hjid XmlTransient for $i."
done

