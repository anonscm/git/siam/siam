/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Tests Resource, Action, Attribute and Environments shared and base functionality by examining Action
 * @author Fabian Foerster
 *
 */
public class ActionSubjectEnvironmentResourceTest {

	 private static Validator validator;
	    
	 
	 	
	    @BeforeClass
	    public static void init() {
	        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	        validator = factory.getValidator();
	    }
	    
	    private List<Attribute> getAttributesList(){
	    	List<Attribute> returnList=new ArrayList<Attribute>();
	    	returnList.add(null);
	    	returnList.add(new Attribute("a","b","c"));
	    	returnList.add(null);
	    	returnList.add(new Attribute("d","e","f"));
	    	return returnList;
	    }
	    
	    @Test(expected=IllegalArgumentException.class)
	    public void testIllegalConstruction(){
	    	ArrayList<Attribute> illegalList=new ArrayList<Attribute>();
	    	illegalList.add(null);
	    	new Action(illegalList);
	    }
	    
	    @Test
	    public void testConstruction(){
	    	List<Attribute> attributes = getAttributesList();
	    	Action action=new Action(attributes);
	    	int violation=validator.validate(action).size();
	    	
	    	//assert legal state
	    	Assert.assertEquals(0, violation);
	    	
	    	
	    	//verify, that the null elements have been removed from teh input list
	    	int size=action.getAttributes().size();
	    	Assert.assertEquals(2, size);
	    
	    	//check against modification
	    	action.getAttributes().clear();
	    	size=action.getAttributes().size();
	    	Assert.assertEquals(2, size);
	    
	    }
	
}
