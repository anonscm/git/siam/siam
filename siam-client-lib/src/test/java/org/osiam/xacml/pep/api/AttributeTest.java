/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AttributeTest {
    private static Validator validator;
    
    @BeforeClass
    public static void init() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

	@Test
	public void testConstruction(){
		int violations=0;
		Attribute attribute= new Attribute("a", "b", "c");
		validator.validate(attribute);
		Assert.assertEquals(0, violations);

		Assert.assertEquals("a",attribute.getId());
		Assert.assertEquals("b",attribute.getDataType());
		Assert.assertEquals("c",attribute.getValue());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIllegalConstruction(){
		new Attribute(null, null, null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIllegalConstruction1(){
		new Attribute(null, "b","c");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIllegalConstruction2(){
		new Attribute("a", null, "c");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIllegalConstruction3(){
		new Attribute("a", "b", null);
	}



}
