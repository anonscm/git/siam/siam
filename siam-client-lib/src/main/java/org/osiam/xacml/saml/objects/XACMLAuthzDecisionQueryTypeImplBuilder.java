package org.osiam.xacml.saml.objects;

import org.opensaml.common.impl.AbstractSAMLObjectBuilder;
import org.opensaml.xacml.XACMLObjectBuilder;
import org.osiam.xacml.saml.objects.XACMLAuthzDecisionQueryType;

public class XACMLAuthzDecisionQueryTypeImplBuilder extends AbstractSAMLObjectBuilder<XACMLAuthzDecisionQueryType>
implements XACMLObjectBuilder<XACMLAuthzDecisionQueryType>{

    /** Constructor. */
    public XACMLAuthzDecisionQueryTypeImplBuilder() {

    }

    /** {@inheritDoc} */
    public XACMLAuthzDecisionQueryType buildObject() {
        return null;
    }

    /** {@inheritDoc} */
    public XACMLAuthzDecisionQueryType buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new XACMLAuthzDecisionQueryTypeImpl(namespaceURI, localName, namespacePrefix);
    }
}
