/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import org.opensaml.saml2.core.impl.RequestAbstractTypeMarshaller;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.MarshallingException;
import org.w3c.dom.Element;


/**
 * Marshaller for Request Abstract
 *
 */
public class RequestAbstractMarshaller extends RequestAbstractTypeMarshaller{
	
    protected void marshallAttributes(XMLObject samlObject, Element domElement) throws MarshallingException {
        RequestAbstract query = (RequestAbstract) samlObject;

        if (query.getInputContextOnlyXSBooleanValue() != null) {
            domElement.setAttributeNS(null, RequestAbstract.INPUTCONTEXTONLY_ATTRIB_NAME, query
                    .getInputContextOnlyXSBooleanValue().toString());
        }

        if (query.getReturnContextXSBooleanValue() != null) {
            domElement.setAttributeNS(null, RequestAbstract.RETURNCONTEXT_ATTRIB_NAME, query
                    .getReturnContextXSBooleanValue().toString());
        }

        if (query.getType() != null) {
            domElement.setAttributeNS(SAMLConstants.XSI_NS , RequestAbstract.TYPE_ATTRIB_NAME, query
            		.getType().toString());
        }
        
        if (query.getID() != null) {
            domElement.setAttributeNS(null, RequestAbstract.ID_ATTRIB_NAME, query
            		.getID().toString());
        }
        
        if (query.getConsent() != null) {
            domElement.setAttributeNS(null, RequestAbstract.CONSENT_ATTRIB_NAME, query
            		.getConsent().toString());
        }
        
        if (query.getIssueInstant() != null) {
            domElement.setAttributeNS(null, RequestAbstract.ISSUE_INSTANT_ATTRIB_NAME, query
            		.getIssueInstant().toString());
        }
        
        if (query.getDestination() != null) {
            domElement.setAttributeNS(null, RequestAbstract.DESTINATION_ATTRIB_NAME, query
            		.getDestination().toString());
        }
        

        super.marshallAttributes(samlObject, domElement);
    }
}
