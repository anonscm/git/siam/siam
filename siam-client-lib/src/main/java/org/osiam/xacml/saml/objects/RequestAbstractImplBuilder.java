/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import org.opensaml.common.impl.AbstractSAMLObjectBuilder;
import org.opensaml.xacml.XACMLObjectBuilder;


/**
 * Builder for Request Abstract
 *
 */
public class RequestAbstractImplBuilder extends AbstractSAMLObjectBuilder<RequestAbstract>
		implements XACMLObjectBuilder<RequestAbstract>{
	
	
	public RequestAbstractImplBuilder (){
		
	}

	public RequestAbstract buildObject() {
		return null;
	}

	public RequestAbstract buildObject(String namespaceURI, String localName,String namespacePrefix) {
		return new RequestAbstractImpl(namespaceURI, localName, namespacePrefix);
	}

}
