package org.osiam.xacml.saml.objects;

import org.opensaml.saml2.core.impl.RequestAbstractTypeUnmarshaller;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xacml.policy.PolicySetType;
import org.opensaml.xacml.policy.PolicyType;
import org.opensaml.xacml.profile.saml.ReferencedPoliciesType;
import org.osiam.xacml.saml.objects.XACMLAuthzDecisionQueryType;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.schema.XSBooleanValue;
import org.w3c.dom.Attr;

public class XACMLAuthzDecisionQueryTypeUnmarshaller extends RequestAbstractTypeUnmarshaller{

    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentObject, XMLObject childObject) throws UnmarshallingException {
        XACMLAuthzDecisionQueryType xacmlauthzdecisionquery = (XACMLAuthzDecisionQueryType) parentObject;

        if (childObject instanceof RequestType) {
            xacmlauthzdecisionquery.setRequest((RequestType) childObject);
        } else if (childObject instanceof PolicyType) {
            xacmlauthzdecisionquery.getPolicies().add((PolicyType) childObject);
        } else if (childObject instanceof PolicySetType) {
            xacmlauthzdecisionquery.getPolicySets().add((PolicySetType) childObject);
        } else if (childObject instanceof ReferencedPoliciesType) {
            xacmlauthzdecisionquery.setReferencedPolicies((ReferencedPoliciesType) childObject);
        } else {
            super.processChildElement(parentObject, childObject);
        }
    }

    /** {@inheritDoc} */
    protected void processAttribute(XMLObject samlObject, Attr attribute) throws UnmarshallingException {
        XACMLAuthzDecisionQueryType authzDS = (XACMLAuthzDecisionQueryType) samlObject;

        if (attribute.getLocalName().equals(XACMLAuthzDecisionQueryType.INPUTCONTEXTONLY_ATTRIB_NAME)) {
            authzDS.setInputContextOnly(XSBooleanValue.valueOf(attribute.getValue()));
        }

        if (attribute.getLocalName().equals(XACMLAuthzDecisionQueryType.RETURNCONTEXT_ATTRIB_NAME)) {
            authzDS.setReturnContext(XSBooleanValue.valueOf(attribute.getValue()));
        }

        if (attribute.getLocalName().equals(XACMLAuthzDecisionQueryType.COMBINEPOLICIES_ATTRIB_NAME)) {
            authzDS.setCombinePolicies(XSBooleanValue.valueOf(attribute.getValue()));
        }

        super.processAttribute(samlObject, attribute);
    }
}
