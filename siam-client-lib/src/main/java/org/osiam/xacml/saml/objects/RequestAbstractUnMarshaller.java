/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import org.opensaml.saml2.core.impl.RequestAbstractTypeUnmarshaller;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.schema.XSBooleanValue;
import org.w3c.dom.Attr;


/**
 * Unmarshaller for Request Abstract
 *
 */
public class RequestAbstractUnMarshaller extends RequestAbstractTypeUnmarshaller{

	protected void processChildElement(XMLObject parentObject, XMLObject childObject) throws UnmarshallingException {
		RequestAbstract requestAbstract = (RequestAbstract) parentObject;

        if (childObject instanceof RequestType) {
        	requestAbstract.setRequest((RequestType) childObject);
        } 
        else {
            super.processChildElement(parentObject, childObject);
        }
    }

    /** {@inheritDoc} */
    protected void processAttribute(XMLObject samlObject, Attr attribute) throws UnmarshallingException {
    	RequestAbstract authzDS = (RequestAbstract) samlObject;

        if (attribute.getLocalName().equals(RequestAbstract.INPUTCONTEXTONLY_ATTRIB_NAME)) {
            authzDS.setInputContextOnly(XSBooleanValue.valueOf(attribute.getValue()));
        }

        if (attribute.getLocalName().equals(RequestAbstract.RETURNCONTEXT_ATTRIB_NAME)) {
            authzDS.setReturnContext(XSBooleanValue.valueOf(attribute.getValue()));
        }

        if (attribute.getLocalName().equals(RequestAbstract.TYPE_ATTRIB_NAME)) {
            authzDS.setType(String.valueOf(attribute.getValue()));
        }
        
        if (attribute.getLocalName().equals(RequestAbstract.ID_ATTRIB_NAME)) {
            authzDS.setType(String.valueOf(attribute.getValue()));
        }
        
        if (attribute.getLocalName().equals(RequestAbstract.CONSENT_ATTRIB_NAME)) {
            authzDS.setType(String.valueOf(attribute.getValue()));
        }
        
        if (attribute.getLocalName().equals(RequestAbstract.ISSUE_INSTANT_ATTRIB_NAME)) {
            authzDS.setType(String.valueOf(attribute.getValue()));
        }
        
        if (attribute.getLocalName().equals(RequestAbstract.DESTINATION_ATTRIB_NAME)) {
            authzDS.setType(String.valueOf(attribute.getValue()));
        }
        
        super.processAttribute(samlObject, attribute);
    }
}
