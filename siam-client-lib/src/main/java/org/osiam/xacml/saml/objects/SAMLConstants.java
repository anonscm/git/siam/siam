/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import org.opensaml.xacml.XACMLConstants;


/**
 * Class which provides some constants for SAML
 */
public class SAMLConstants extends XACMLConstants {

	public static final String SAML2_PREFIX = "samlp";

	public static final String SAML2_NS = "urn:oasis:names:tc:SAML:2.0:protocol";
	
	public static final String XSI_NS = "http://www.w3.org/2001/XMLSchema-instance";

	public static final String XSI_PREFIX = "xsi";
	
	public static final String XACML_QUERY_TYPE = "xacml-samlp:XACMLAuthzDecisionQuery";
	
	public static final String REQUEST_ABSTRACT_ID = "osiam";
	
	public static final String AUTH_DECISION_QUERY_NS = "urn:oasis:xacml:2.0:saml:protocol:schema:os";

}
