package org.osiam.xacml.saml.objects;

import org.opensaml.saml2.core.impl.RequestAbstractTypeMarshaller;
import org.osiam.xacml.saml.objects.XACMLAuthzDecisionQueryType;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.MarshallingException;
import org.w3c.dom.Element;

public class XACMLAuthzDecisionQueryTypeMarshaller extends RequestAbstractTypeMarshaller{

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject samlObject, Element domElement) throws MarshallingException {
        XACMLAuthzDecisionQueryType query = (XACMLAuthzDecisionQueryType) samlObject;

        if (query.getInputContextOnlyXSBooleanValue() != null) {
            domElement.setAttributeNS(null, XACMLAuthzDecisionQueryType.INPUTCONTEXTONLY_ATTRIB_NAME, query
                    .getInputContextOnlyXSBooleanValue().toString());
        }

        if (query.getReturnContextXSBooleanValue() != null) {
            domElement.setAttributeNS(null, XACMLAuthzDecisionQueryType.RETURNCONTEXT_ATTRIB_NAME, query
                    .getReturnContextXSBooleanValue().toString());
        }

        if (query.getCombinePoliciesXSBooleanValue() != null) {
            domElement.setAttributeNS(null, XACMLAuthzDecisionQueryType.COMBINEPOLICIES_ATTRIB_NAME, query
                    .getCombinePoliciesXSBooleanValue().toString());
        }

        super.marshallAttributes(samlObject, domElement);
    }
}
