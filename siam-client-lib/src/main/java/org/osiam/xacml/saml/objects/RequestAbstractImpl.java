/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.impl.RequestAbstractTypeImpl;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.XSBooleanValue;


/**
 * Implementation of Request Abstract
 * Provides some getter and setter for the attributes and values
 */
public class RequestAbstractImpl extends RequestAbstractTypeImpl implements RequestAbstract{
	
	
	private RequestType request;
	
	private Issuer issuer;
	
	private String type;
	
	private String ID;
	
	private String consent;
	
	private String destination;
	
	private DateTime issueInstant;
	
    private XSBooleanValue inputContextOnly;

    private XSBooleanValue returnContext;

    

    protected RequestAbstractImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);

    }

    
	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = prepareForAssignment(this.ID, ID);		
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = prepareForAssignment(this.type, type);		
	}

	public String getConsent() {
		return consent;
	}

	public void setConsent(String consent) {
		this.consent = prepareForAssignment(this.consent, consent);		
	}
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = prepareForAssignment(this.destination, destination);		
	}
	
	public DateTime getIssueInstant() {
		return issueInstant;
	}

	public void setIssueInstant(DateTime issueInstant) {
		this.issueInstant = prepareForAssignment(this.issueInstant, issueInstant);		
	}
	
	public Issuer getIssuer() {
		return issuer;
	}

	public void setIssuer(Issuer issuer) {
		this.issuer = prepareForAssignment(this.issuer, issuer);		
	}
	
    
	public RequestType getRequest() {
		return request;
	}

	public void setRequest(RequestType request) {
		this.request = prepareForAssignment(this.request, request);		
	}

	public XSBooleanValue getInputContextOnlyXSBooleanValue() {
		return inputContextOnly;
	}

	public XSBooleanValue getReturnContextXSBooleanValue() {
		return returnContext;
	}


	public void setInputContextOnly(XSBooleanValue inputContextOnly) {
        this.inputContextOnly = prepareForAssignment(this.inputContextOnly, inputContextOnly);		
	}
	
	public void setInputContextOnly(Boolean inputContextOnly) {
        if (inputContextOnly != null) {
            this.inputContextOnly = prepareForAssignment(this.inputContextOnly, new XSBooleanValue(inputContextOnly,
                    false));
        } else {
            this.inputContextOnly = prepareForAssignment(this.inputContextOnly, null);
        }
    }
	
	public void setReturnContext(Boolean returnContext) {
        if (returnContext != null) {
            this.returnContext = prepareForAssignment(this.returnContext, new XSBooleanValue(returnContext, false));
        } else {
            this.returnContext = prepareForAssignment(this.returnContext, null);
        }
    }

	public void setReturnContext(XSBooleanValue returnContext) {
		this.returnContext = prepareForAssignment(this.returnContext, returnContext);		
	}
	
	
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();


        if (issuer != null) {
            children.add(issuer);
        }
        
        if (super.getOrderedChildren() != null) {
            children.addAll(super.getOrderedChildren());
        }
        
        if (request != null) {
            children.add(request);
        }


        return Collections.unmodifiableList(children);
    }


    public Boolean isInputContextOnly() {
        if (inputContextOnly != null) {
            return inputContextOnly.getValue();
        }

        return Boolean.FALSE;
    }

    public Boolean isReturnContext() {
        if (returnContext != null) {
            return returnContext.getValue();
        }

        return Boolean.FALSE;
    }
}
