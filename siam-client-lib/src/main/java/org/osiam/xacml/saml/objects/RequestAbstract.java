/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.saml.objects;

import javax.xml.namespace.QName;

import org.joda.time.DateTime;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.RequestAbstractType;
import org.opensaml.xacml.XACMLObject;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xml.schema.XSBooleanValue;


/**
 * Interface for Request Abstract
 *
 */
public interface RequestAbstract extends RequestAbstractType, XACMLObject{

    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "RequestAbstract";

    public static final QName DEFAULT_ELEMENT_NAME_XACML20 = new QName(SAMLConstants.SAML2_NS,
            DEFAULT_ELEMENT_LOCAL_NAME, SAMLConstants.SAML2_PREFIX);
    
 	public static final String INPUTCONTEXTONLY_ATTRIB_NAME = "InputContextOnly";

	public static final String RETURNCONTEXT_ATTRIB_NAME = "ReturnContext";

	public static final String COMBINEPOLICIES_ATTRIB_NAME = "CombinePolicies";

	public static final String TYPE_ATTRIB_NAME = "xsi:type";
	
	public static final String ID_ATTRIB_NAME = "ID";
	
	public static final String ISSUE_INSTANT_ATTRIB_NAME = "IssueInstant";
	
	public static final String DESTINATION_ATTRIB_NAME = "Destination";
	
	public static final String CONSENT_ATTRIB_NAME = "Consent";

	

	
    public String getType();
    
    public void setType(String type);
    
    public String getID();
    
    public void setID(String ID);
    
    public DateTime getIssueInstant();
    
    public void setIssueInstant(DateTime issueInstant);
    
    public String getDestination();
    
    public void setDestination(String destination);
    
    public String getConsent();
    
    public void setConsent(String consent);
    
    public Issuer getIssuer();
    
    public void setIssuer(Issuer issuer);
    
    
	
    public RequestType getRequest();
    
    public void setRequest(RequestType request);
    
	public XSBooleanValue getInputContextOnlyXSBooleanValue();

	public XSBooleanValue getReturnContextXSBooleanValue();

	public void setInputContextOnly(XSBooleanValue valueOf);
	
    public void setInputContextOnly(Boolean inputContextOnly);

	public void setReturnContext(XSBooleanValue valueOf);
	
    public void setReturnContext(Boolean returnContext);

    public Boolean isInputContextOnly();

    public Boolean isReturnContext();

    
}
