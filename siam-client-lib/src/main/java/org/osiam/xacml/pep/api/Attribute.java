/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import javax.validation.constraints.NotNull;

import net.jcip.annotations.Immutable;

/**
 * Represents an Attribute for use in the API calls to the PEP functionality
 * Objects of this class are immutable
 * 
 * @author Fabian Foerster
 * 
 */
@Immutable
public final class Attribute{
	@NotNull
	private final String id;
	@NotNull
	private final String dataType;
	@NotNull
	private final String value;

	public String getId() {
		return this.id;
	}

	public String getDataType() {
		return this.dataType;
	}

	public String getValue() {
		return this.value;
	}

	public Attribute(String id, String dataType, String value) {
		if (id == null || dataType == null || value == null) {
			throw new IllegalArgumentException("Arguments must not be null");
		}
		this.id = id;
		this.dataType = dataType;
		this.value = value;
	}

	@Override
	public String toString() {
		return "id="+id+" dataType="+dataType+" value="+value;
	}
}
