/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import net.jcip.annotations.Immutable;
/**
 * Represents an Resource for use in the API calls to the PEP functionality
 * Objects of this class are immutable
 * 
 * @author Fabian Foerster
 *
 */
@Immutable
public class Subjects {
	private final Subject[] subjects;
	
	//avoids creation of a list containing null every time the constructor is called an needs a collection containing null in order to filter out 
	//null elements.
	private final static List<Subject> listContainingNull;
	static{
		listContainingNull= new ArrayList<Subject>();
		listContainingNull.add(null);
	}
	
	public Subjects(Subject initialSubject){
		if(initialSubject==null)
			throw new IllegalArgumentException("subject MUST NOT be null");

		ArrayList<Subject> subjectsList= new ArrayList<Subject>();
		subjectsList.add(initialSubject);
		this.subjects=subjectsList.toArray(new Subject[subjectsList.size()]);

	}
	
	public Subjects(Collection<Subject> subjects){
		//check for null
		if(subjects==null) {
			throw new IllegalArgumentException("subjecs MUST NOT be null");
		}
		
		//create a variable-lenght list from possibly fixed-length collection
		List<Subject> subjectsList=new ArrayList<Subject>(subjects);
		
		//eliminate any nulls in attributes
		subjectsList.removeAll(listContainingNull);
		
		if(subjectsList.size()==0) {
			throw new IllegalArgumentException("Subjects MUST NOT be empty nor contain only null");
		}
		
		this.subjects=subjectsList.toArray(new Subject[subjectsList.size()]);
	}
	
	
	/**
	 * returns a copy of the internal collection of resource. 
	 * Modifying this will not change the internal state of the object.  
	 */
	public List<Subject> getSubjects(){
		return new ArrayList<Subject>(Arrays.asList(subjects));
	}
}
