/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import org.w3c.dom.Element;



/**
 * Interface for the Policy Enforcement Point
 * @author Fabian Foerster
 *
 */
public interface PolicyEnforcementPoint {
	/**
	 * Causes the PolicyEnforcementPoint to query the configured PDP. Will throw an exception if no PdP has been configured  
	 * @param subjects 
	 * @param resources
	 * @param action
	 * @param environment
	 * @return result of the policy evaluation
	 */
	PolicyResult query(Subjects subjects, Resources resources, Action action, Environment environment);
	
	/**
	 * Causes the PolicyEnforcementPoint to query the configured PDP. Will throw an exception if no PdP has been configured  
	 * 
	 * @param request DOM element (request)
	 * @return result of the policy evaluation
	 */
	PolicyResult query(Element request);
	
	/**
	 * Gets the configured PDP Url, or an empty String, if none is configured
	 * @return
	 */
	String getConfiguredPdPUrl();
	
	
	/**
	 * Gets the configured Issuer, or an empty String, if none is configured
	 * @return
	 */
	String getConfiguredIssuer();
	

	
	/**
	 * Causes the PolicyEnforcementPoint to query the provided PDP
	 * @param subjects
	 * @param resources
	 * @param action
	 * @param environment
	 * @param pdpUrl must not be null, otherwise an IllegalArgumentException will be thrown
	 * @return
	 */
	PolicyResult query(Subjects subjects, Resources resources, Action action, Environment environment, String pdpUrl, String issuer);
}
