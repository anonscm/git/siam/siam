/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import java.util.List;

import net.jcip.annotations.Immutable;

/**
 * Represents an Action for use in the API calls to the PEP functionality
 * Objects of this class are immutable
 * 
 * @author Fabian Foerster
 *
 */
@Immutable
public final class Action extends AbstractPepApiInputArtifact{
	
	/**
	 * initializes a new Action object represented by a list of attributes
	 * @param attributes representation of the new Action object, must not be null
	 */
	public Action(List<Attribute> attributes){
		super(attributes);
	}
	
	/**
	 * Convenience Constructor for creation of an Action that is signified by but one Attribute.
	 * @param attribute single Attribute signifying the action to decide upon.
	 */
	public Action(Attribute attribute){
		super(attribute);
	}
	
}

