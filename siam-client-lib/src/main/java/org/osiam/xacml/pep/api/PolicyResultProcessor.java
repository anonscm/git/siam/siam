/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;



import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Statement;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.ws.soap.soap11.impl.FaultImpl;
import org.opensaml.xacml.ctx.DecisionType;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xacml.ctx.ResponseType;
import org.opensaml.xacml.ctx.ResultType;
import org.opensaml.xacml.ctx.StatusType;
import org.opensaml.xacml.policy.ObligationType;
import org.opensaml.xacml.policy.ObligationsType;
import org.opensaml.xacml.profile.saml.XACMLAuthzDecisionStatementType;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.util.XMLHelper;
import org.osiam.xacml.crypto.CryptoProvider;
import org.osiam.xacml.crypto.KeyStoreConfig;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class PolicyResultProcessor {
	
	private MarshallerFactory marshallerFactory = null;
	private javax.xml.parsers.DocumentBuilderFactory dbf = null;
	private javax.xml.parsers.DocumentBuilder db = null;
	
	private static Logger logger = Logger.getLogger(PolicyResultProcessor.class.getName());
	
	private KeyStoreConfig storeConfig;
	private CryptoProvider cryptoProvider;
	private boolean openAmRequestMode = false;
	
	
	private void init(){
		
		cryptoProvider = new CryptoProvider(storeConfig);
		
		marshallerFactory = org.opensaml.xml.Configuration
		.getMarshallerFactory();

		dbf = DocumentBuilderFactory.newInstance();
		
		dbf.setNamespaceAware(true);
		
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.log(Level.ERROR, e);
		}
	}

	
	public PolicyResultProcessor(KeyStoreConfig storeConfig, boolean openAmMode) {
		this.openAmRequestMode = openAmMode;
		this.storeConfig = storeConfig;
		init();
	}


	public PolicyResult processResult(Envelope soapResponse) {
		
		PolicyResult result = null;
		Object responseObj = soapResponse.getBody().getUnknownXMLObjects().get(0);
		Response response = null;
		if (responseObj instanceof Response){
			response = (Response)responseObj;
		}else if (responseObj instanceof FaultImpl){
			FaultImpl fault  = (FaultImpl)responseObj;
			System.out.println(fault.getMessage().getValue());
			//TODO throw custom exception
			throw new RuntimeException(fault.getMessage().getValue());
		}else{
			throw new RuntimeException("unknown response object");
		}
		
		if(openAmRequestMode){
			result = processRequestAbstract(response);
		}
		else{
			result = processAuthDecisionQuery(response);
		}
		
		return result;
	}

	
	private PolicyResult processRequestAbstract(Response response){
		
		PolicyResult result = null;
		
		if (response.isSigned()){
			if(cryptoProvider.validate(response)){
				if(!response.getEncryptedAssertions().isEmpty()){
					Assertion assertion = cryptoProvider.decrypt(response.getEncryptedAssertions().get(0));
					result = processRequestAbstractResponse(assertion);
				}
				else{
					Assertion assertion = response.getAssertions().get(0);
					result = processRequestAbstractResponse(assertion);

				}
			}
			else{
				logger.log(Level.ERROR, "Signature not valid");
			}
		}
		else {
			if(!response.getEncryptedAssertions().isEmpty()){
				Assertion assertion = cryptoProvider.decrypt(response.getEncryptedAssertions().get(0));
				result = processRequestAbstractResponse(assertion);
			}
			else{
				Assertion assertion = response.getAssertions().get(0);
				result = processRequestAbstractResponse(assertion);
			}
		}
		return result;
	}
	
	private PolicyResult processAuthDecisionQuery(Response response){
		
		PolicyResult result = null;
		
		if (response.isSigned()){
			if(cryptoProvider.validate(response)){
				if(!response.getEncryptedAssertions().isEmpty()){
					Assertion assertion = cryptoProvider.decrypt(response.getEncryptedAssertions().get(0));
					result = processAuthDecisionQueryResponse(assertion);
				}
				else{
					Assertion assertion = response.getAssertions().get(0);
					result = processAuthDecisionQueryResponse(assertion);

				}
			}
			else{
				logger.log(Level.ERROR, "Signature not valid");
			}
		}
		else {
			if(!response.getEncryptedAssertions().isEmpty()){
				Assertion assertion = cryptoProvider.decrypt(response.getEncryptedAssertions().get(0));
				result = processAuthDecisionQueryResponse(assertion);
			}
			else{
				Assertion assertion = response.getAssertions().get(0);
				result = processAuthDecisionQueryResponse(assertion);
			}
		}
		return result;
	}
	
	
	//TODO enhance this method
	private PolicyResult processRequestAbstractResponse(Assertion assertion){
		
		PolicyResult result = new PolicyResult();
		String desicionValue = null;
		String statusValue = null;

		Element elemResponse = assertion.getDOM();
		NodeList nodes_i = elemResponse.getChildNodes();
		
		for (int i = 0; i < nodes_i.getLength(); i++) {
			Node node_i = nodes_i.item(i);
			if (node_i.getNodeType() == Node.ELEMENT_NODE){
				Element elem_i = (Element) node_i;
				NodeList nodes_j = elem_i.getChildNodes();
				for (int j = 0; j < nodes_j.getLength(); j++) {
					  Node node_j = nodes_j.item(j);
					  if (node_j.getNodeType() == Node.ELEMENT_NODE) {
						  Element elem_j = (Element) node_j;
						  NodeList nodes_k = elem_j.getChildNodes();
						  for (int k = 0; k < nodes_k.getLength(); k++) {
							  Node node_k = nodes_k.item(k);
							  if (node_k.getNodeType() == Node.ELEMENT_NODE) {
								  Element elem_k = (Element) node_k;
								  NodeList nodes_l = elem_k.getChildNodes();
								  for (int l = 0; l<nodes_l.getLength(); l++){
									  Node node_l = nodes_l.item(l);
									  if (node_l.getNodeType() == Node.ELEMENT_NODE
											  && ((Element) node_l).getTagName().equals("xacml-context:Decision")){
										  Element elem_l = (Element) node_l;
										  desicionValue = elem_l.getTextContent();
										  result.setDecision(desicionValue);
									  }
									  else if (node_l.getNodeType() == Node.ELEMENT_NODE
											  && ((Element) node_l).getTagName().equals("xacml-context:Status")){
										  Element elem_l = (Element) node_l;
										  NodeList nodes_m = elem_l.getChildNodes();
										  for (int m = 0; m<nodes_m.getLength(); m++){
											  Node node_m = nodes_m.item(m);
											  if (node_m.getNodeType() == Node.ELEMENT_NODE
													  && ((Element) node_m).getTagName().equals("xacml-context:StatusCode")){
												  Element elem_m =(Element) node_m;
												  statusValue = elem_m.getAttribute("Value");
												  result.setStatus(statusValue);
											  }
										  }
									  }
								  }
							  }
						  }
					  }
				}
			}
		}

		return result;
	}
	
	//TODO enhance this method
	private PolicyResult processAuthDecisionQueryResponse(Assertion assertion){
		
		PolicyResult result = new PolicyResult();
		String desicionValue = null;
		String statusValue = null;

		Element elemResponse = assertion.getDOM();
		NodeList nodes_i = elemResponse.getChildNodes();
		
		for (int i = 0; i < nodes_i.getLength(); i++) {
			Node node_i = nodes_i.item(i);
			if (node_i.getNodeType() == Node.ELEMENT_NODE){
				Element elem_i = (Element) node_i;
				NodeList nodes_j = elem_i.getChildNodes();
				for (int j = 0; j < nodes_j.getLength(); j++) {
					  Node node_j = nodes_j.item(j);
					  if (node_j.getNodeType() == Node.ELEMENT_NODE) {
						  Element elem_j = (Element) node_j;
						  NodeList nodes_k = elem_j.getChildNodes();
						  for (int k = 0; k < nodes_k.getLength(); k++) {
							  Node node_k = nodes_k.item(k);
							  if (node_k.getNodeType() == Node.ELEMENT_NODE) {
								  Element elem_k = (Element) node_k;
								  NodeList nodes_l = elem_k.getChildNodes();
								  for (int l = 0; l<nodes_l.getLength(); l++){
									  Node node_l = nodes_l.item(l);
									  if (node_l.getNodeType() == Node.ELEMENT_NODE
											  && ((Element) node_l).getTagName().equals("ns7:Decision")){
										  Element elem_l = (Element) node_l;
										  desicionValue = elem_l.getTextContent();
										  result.setDecision(desicionValue);
									  }
									  else if (node_l.getNodeType() == Node.ELEMENT_NODE
											  && ((Element) node_l).getTagName().equals("ns7:Status")){
										  Element elem_l = (Element) node_l;
										  NodeList nodes_m = elem_l.getChildNodes();
										  for (int m = 0; m<nodes_m.getLength(); m++){
											  Node node_m = nodes_m.item(m);
											  if (node_m.getNodeType() == Node.ELEMENT_NODE
													  && ((Element) node_m).getTagName().equals("ns7:StatusCode")){
												  Element elem_m =(Element) node_m;
												  statusValue = elem_m.getAttribute("Value");
												  result.setStatus(statusValue);
											  }
										  }
									  }
								  }
							  }
						  }
					  }
				}
			}
		}

		return result;
	}
	
	
	@Deprecated
	//Don't use this method. Statement is null. Workaround with method processDecision()
	public PolicyResult processResponse(Assertion assertion){
		
		PolicyResult policyResult = new PolicyResult();
		
		XACMLAuthzDecisionStatementType statement =  (XACMLAuthzDecisionStatementType) assertion.getStatements().iterator().next();
		ResultType result = statement.getResponse().getResult();

		DecisionType decision = result.getDecision();
		policyResult.setDecision(decision.getDecision().toString());
		
		StatusType status = result.getStatus();
		policyResult.setStatus(status.getStatusCode().toString());
		
		if (result.getObligations() != null) {
			ObligationsType obligations = result.getObligations();
			ObligationType oblig = obligations.getObligations().get(0);
			policyResult.setObligation(oblig.getObligationId());
		}
		return policyResult;
	}
	
	
	//Helper method for printing node names
	@SuppressWarnings("unused")
	private void printDOM(Node rootNode){
	
		NodeList nodes = rootNode.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++ ){
				if (!nodes.item(i).getNodeName().trim().equals("#text")) {
					logger.log(Level.INFO, "DOM: " + nodes.item(i).getNodeName());
					printDOM(nodes.item(i));
				}
		}
	}

	//Helper method for printing XMLObject
	@SuppressWarnings("unused")
	private void marshallRequestType(XMLObject response) {

		try {
			Document doc = db.newDocument();
			Marshaller marshallerRequest = marshallerFactory
					.getMarshaller(RequestType.DEFAULT_ELEMENT_NAME);
			Element element = marshallerRequest.marshall(response, doc);
			String output = XMLHelper.prettyPrintXML(element);

			logger.log(Level.INFO, "XMLObject: " + output);

		} catch (MarshallingException e) {
			e.printStackTrace();
		}

	}
}
