/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.validation.constraints.NotNull;


/**
 * Base class providing the functionality for most PEP API artifacts
 * Classes extending this should be carefull to be immutable. 
 * This class only provides reading access to a copy of the collection of Attributes signifying the particular input artifact of the PEP API, e.g. an Action object.
 * @author Fabian Foerster
 *
 */
public abstract class AbstractPepApiInputArtifact{

	@NotNull
	private final Attribute[] attributes;
	
	
	//avoids creation of a list containing null every time the constructor is called an needs a collection containing null in order to filter out 
	//null elements.
	private final static List<Attribute> listContainingNull;
	static{
		listContainingNull= new ArrayList<Attribute>();
		listContainingNull.add(null);
	}
	
	/**
	 * initializes a new Artifact represented by a list of attributes
	 * @param attributes representation of the new  object, must not be null nor empty nor contain only null
	 */
	public AbstractPepApiInputArtifact(Collection<Attribute> attributes){
		//check for null
		if(attributes==null) {
			throw new IllegalArgumentException("attributes MUST NOT be null");
		}
		
		//create a variable-lenght list from possibly fixed-length collection
		List<Attribute> attributesList=new ArrayList<Attribute>(attributes);
		
		//eliminate any nulls in attributes
		attributesList.removeAll(listContainingNull);
		
		if(attributesList.size()==0) {
			throw new IllegalArgumentException("attributes MUST NOT be empty nor contain only null");
		}
		
		this.attributes=attributesList.toArray(new Attribute[attributesList.size()]);
	}
	
	
	/**
	 * initializes a new Action object represented by a list of attributes
	 * @param attributes representation of the new Action object, must not be null
	 */
	public AbstractPepApiInputArtifact(Attribute attribute){
		//Creates a non-fixed-length list from a fixed-length list representation of the array containing only one attribute
		this(new ArrayList<Attribute>(Arrays.asList(attribute)));
	}
	
	/**
	 * returns a copy of the internal collection of attributes. 
	 * Modifying this will not change the internal state of the object.  
	 */
	public List<Attribute> getAttributes(){
		return new ArrayList<Attribute>(Arrays.asList(attributes));
	}
	

}
