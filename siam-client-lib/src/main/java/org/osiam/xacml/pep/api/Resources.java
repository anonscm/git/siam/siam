/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.pep.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import net.jcip.annotations.Immutable;

/**
 * Represents an Resource for use in the API calls to the PEP functionality
 * Objects of this class are immutable
 * 
 * @author Fabian Foerster
 *
 */
@Immutable
public class Resources {
	private final Resource[] resources;
	
	//avoids creation of a list containing null every time the constructor is called an needs a collection containing null in order to filter out 
	//null elements.
	private final static List<Resource> listContainingNull;
	static{
		listContainingNull= new ArrayList<Resource>();
		listContainingNull.add(null);
	}
	
	
	public Resources(Collection<Resource> resources){
		//check for null
		if(resources==null) {
			throw new IllegalArgumentException("resources MUST NOT be null");
		}
		
		//create a variable-lenght list from possibly fixed-length collection
		List<Resource> resourcesList=new ArrayList<Resource>(resources);
		
		//eliminate any nulls in attributes
		resourcesList.removeAll(listContainingNull);
		
		if(resourcesList.size()==0) {
			throw new IllegalArgumentException("Resources MUST NOT be empty nor contain only null");
		}
		
		this.resources=resourcesList.toArray(new Resource[resourcesList.size()]);
	}
	/**
	 * returns a copy of the internal collection of resource. 
	 * Modifying this will not change the internal state of the object.  
	 */
	public List<Resource> getResources(){
		return new ArrayList<Resource>(Arrays.asList(resources));
	}
}
