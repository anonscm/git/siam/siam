/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLConfigurator;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.parse.BasicParserPool;
import org.osiam.xacml.crypto.KeyStoreConfig;
import org.osiam.xacml.pep.api.PolicyEnforcementPoint;
/**
 * Factory for PolicyEnforcementPoint
 * @author Fabian Foerster
 *
 */
public class PolicyEnforcementPointFactory {
	private static boolean initialized=false;
	private static XMLConfigurator xmlConf;
	private static XMLObjectBuilderFactory xmlObjectBuilderFactory = null;
	private static MarshallerFactory marshallerFactory;
	private static javax.xml.parsers.DocumentBuilderFactory documentBuilderFactory = null;
	private static BasicParserPool parserPool;

	
	/**
	 * initializes several static properties, allows exception handling
	 * the configuration file must be loaded first. before bootstrapping is done
	 */
	private static void init() throws ConfigurationException,
			ParserConfigurationException {
		if (!initialized) {
			
			//Configuration for RequestAbstract
//			InputStream in = PolicyEnforcementPointFactory.class.getClassLoader().getResourceAsStream("RequestAbstract_Config.xml");
			
			//Configuration for XACMLAuthzDecisionQueryType
			InputStream in = PolicyEnforcementPointFactory.class.getClassLoader().getResourceAsStream("XACMLAuthzDecisionQueryType_Config.xml");
			
			xmlConf = new XMLConfigurator();
			xmlConf.load(in);

			xmlObjectBuilderFactory = org.opensaml.xml.Configuration.getBuilderFactory();

			marshallerFactory = org.opensaml.xml.Configuration
					.getMarshallerFactory();

			documentBuilderFactory = DocumentBuilderFactory.newInstance();

			documentBuilderFactory.setNamespaceAware(true);

			parserPool = new BasicParserPool();
			parserPool.setNamespaceAware(true);

			try {
				DefaultBootstrap.bootstrap();
			} catch (ConfigurationException e) {
				e.printStackTrace();
			}
			initialized = true;
		}
	}
	
	/**
	 * Returns a policy Enforcment Point which queries the specified url and issues the request with the specified issuer
	 * @param pdp_url the url of the PDP to query.
	 * @param issuer the issuer of the PDP queries
	 * @return a properly configured PEP
	 * @throws ParserConfigurationException
	 * @throws ConfigurationException
	 */
	public static PolicyEnforcementPoint getPolicyEnforcementPoint(String pdp_url, String issuer, KeyStoreConfig storeConfig, boolean openAmMode) throws ParserConfigurationException, ConfigurationException{
		init();
		PolicyEnforcementPointImpl pep = new PolicyEnforcementPointImpl(pdp_url, issuer, storeConfig, openAmMode);
		pep.setDocumentBuilder(documentBuilderFactory.newDocumentBuilder());
		pep.setXmlObjectBuilderFactory(xmlObjectBuilderFactory);
		pep.setMarshallerFactory(marshallerFactory);
		pep.setBasicParserPool(parserPool);
		return pep;
	}
}
