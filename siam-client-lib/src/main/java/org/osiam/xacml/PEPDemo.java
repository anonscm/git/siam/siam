/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.osiam.xacml.crypto.KeyStoreConfig;
import org.osiam.xacml.pep.api.Action;
import org.osiam.xacml.pep.api.Attribute;
import org.osiam.xacml.pep.api.Environment;
import org.osiam.xacml.pep.api.PolicyEnforcementPoint;
import org.osiam.xacml.pep.api.PolicyResult;
import org.osiam.xacml.pep.api.Resource;
import org.osiam.xacml.pep.api.Resources;
import org.osiam.xacml.pep.api.Subject;
import org.osiam.xacml.pep.api.Subjects;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Demo class to create xacml requests.
 *
 */
public class PEPDemo {

	private static String PDP_URL = "http://localhost:8080/pdp_cepe/SOAPServlet";
	private static String ISSUER = "pepTestClient";
	private static String actionString = "read";
	private static String subjectString = "openam_username@test.com";
	private static String resourceString = "http://server.example.com/code/docs/developer-guide.html";
	
	private static String clientPrivateKeyResourceName = "sp1-keystore.jks";
	private static String clientPublicKeyResourceName = "sp1-truststore.jks";
	private static String privateKeyAlias ="pep-server-cert";
	private static String publicKeyAlias ="pdp-server-cert";
	private static char[] keyStorePassword = "changeit".toCharArray();
	private static char[] keyPassword = "changeit".toCharArray();
	
	private static boolean openAmMode = false;

	private static Logger logger = Logger.getLogger(PEPDemo.class.getName());

	private PolicyEnforcementPoint pep;
	
	public PEPDemo(boolean enableSignEnc) throws ParserConfigurationException, ConfigurationException{
		// new PolicyEnforcementPointFactory();
		KeyStoreConfig storeConfig = new KeyStoreConfig();
		storeConfig.setEnableSignatureAndEncryption(enableSignEnc);
		if (enableSignEnc){
			storeConfig.setKeyStoreFilename(clientPrivateKeyResourceName);
			storeConfig.setTrustStoreFilename(clientPublicKeyResourceName);
			storeConfig.setPrivateKeyAlias(privateKeyAlias);
			storeConfig.setPublicKeyAlias(publicKeyAlias);
			storeConfig.setKeystorePwd(keyStorePassword);
			storeConfig.setKeyPwd(keyPassword);
		}
		pep = PolicyEnforcementPointFactory.getPolicyEnforcementPoint(PDP_URL, ISSUER, storeConfig, openAmMode);
	}

	
	/**
	 * method to create a demo xacml request and fire it.
	 */
	public String buildAndFireManualQuery(){
		List<Subject> subjects=new ArrayList<Subject>();
		List<Resource> resources=new ArrayList<Resource>();
		List<Attribute> attributes =new ArrayList<Attribute>();
		String result;
		
		attributes.add(new Attribute("urn:oasis:names:tc:xacml:1.0:action:action-id", 
				"http://www.w3.org/2001/XMLSchema#string", 
				actionString));
		attributes.add(new Attribute("urn:oasis:names:tc:xacml:1.0:action:action-id", 
				"http://www.w3.org/2001/XMLSchema#string", 
				"write"));
		
		Action action=new Action(attributes);
		
//		Environment environment= new Environment(
//				new Attribute("urn:oasis:names:tc:xacml:1.0:environment:environment-id", 
//						"http://www.w3.org/2001/XMLSchema#string", 
//						"c"));
		
//		Action action = null;
		Environment environment = null;
		
		subjects.add(new Subject(
				new Attribute("urn:oasis:names:tc:xacml:1.0:subject:subject-id", 
						"http://www.w3.org/2001/XMLSchema#string",
						subjectString)));
		subjects.add(new Subject(
				new Attribute("urn:oasis:names:tc:xacml:1.0:subject:subject-id", 
						"http://www.w3.org/2001/XMLSchema#string",
						subjectString)));
		resources.add(new Resource(
				new Attribute("urn:oasis:names:tc:xacml:1.0:resource:resource-id", 
						"http://www.w3.org/2001/XMLSchema#anyURI",
						resourceString)));
		resources.add(new Resource(
				new Attribute("urn:oasis:names:tc:xacml:1.0:resource:resource-id", 
						"http://www.w3.org/2001/XMLSchema#anyURI",
						resourceString)));
		
		PolicyResult policyResult = pep.query(new Subjects(subjects), new Resources(resources), action, environment);
		result = "Decision:\n" + policyResult.getDecision()+ "\nStatus:\n" + policyResult.getStatus();
		logger.info( result );
		return result;
	}

	/**
	 * iterates over the files in the given path and fires a request for every policy found
	 * 
	 * @param path the path to look for policies
	 * @throws ConfigurationException
	 * @throws XMLParserException
	 * @throws FileNotFoundException
	 */
	public void fireRequestFromFilesIn(String path) throws ConfigurationException, XMLParserException, FileNotFoundException {
		for (String file : new File(path).list(new XMLFilenameFilter())) {
			logger.info("File:\n" + path + file);
			fireRequestFromFile(path + file);
		}
	}

	/**
	 * method to read a request from file and fire it
	 * 
	 * @param filePath path to request file 
	 * @throws ConfigurationException ConfigurationException
	 * @throws XMLParserException XMLParserException
	 * @throws FileNotFoundException FileNotFoundException
	 */
	public String fireRequestFromFile(String filePath) throws ConfigurationException, XMLParserException, FileNotFoundException{
		DefaultBootstrap.bootstrap();      
		BasicParserPool ppMgr = new BasicParserPool();
		ppMgr.setNamespaceAware(true); 
		InputStream in = new FileInputStream(filePath);
		Document inCommonMDDoc = ppMgr.parse(in);
		Element metadataRoot = inCommonMDDoc.getDocumentElement(); 		
		PolicyResult policyResult = pep.query(metadataRoot);
		String result = "Decision:\n" + policyResult.getDecision()+ "\nStatus:\n" + policyResult.getStatus() ;
		logger.info(result);
		
		return result;
	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException, ConfigurationException, XMLParserException {
		
		String path = "src/main/resources/demo/";
		boolean enableSignEnc = false;
		String requestFile = "src/main/resources/demo/request-1.xml";
		PEPDemo pepDemo = new PEPDemo(enableSignEnc);
//		pepDemo.buildAndFireManualQuery();

		pepDemo.fireRequestFromFilesIn(path);

//		pepDemo.fireRequestFromFile(requestFile);
	}

	public static String getPDP_URL() {
		return PDP_URL;
	}

	public static void setPDP_URL(String pdp_url) {
		PDP_URL = pdp_url;
	}

	public static String getISSUER() {
		return ISSUER;
	}

	public static void setISSUER(String issuer) {
		ISSUER = issuer;
	}

	public static String getActionString() {
		return actionString;
	}

	public static void setActionString(String actionString) {
		PEPDemo.actionString = actionString;
	}

	public static String getSubjectString() {
		return subjectString;
	}

	public static void setSubjectString(String subjectString) {
		PEPDemo.subjectString = subjectString;
	}

	public static String getResourceString() {
		return resourceString;
	}

	public static void setResourceString(String resourceString) {
		PEPDemo.resourceString = resourceString;
	}

	private static class XMLFilenameFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".xml");
		}		
	}
}