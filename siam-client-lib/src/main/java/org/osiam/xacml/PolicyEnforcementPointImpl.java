/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.impl.IssuerBuilder;
import org.opensaml.ws.security.SecurityPolicyException;
import org.opensaml.ws.soap.client.BasicSOAPMessageContext;
import org.opensaml.ws.soap.client.http.HttpClientBuilder;
import org.opensaml.ws.soap.client.http.HttpSOAPClient;
import org.opensaml.ws.soap.common.SOAPException;
import org.opensaml.ws.soap.soap11.Body;
import org.opensaml.ws.soap.soap11.Envelope;
import org.opensaml.xacml.XACMLConstants;
import org.opensaml.xacml.ctx.ActionType;
import org.opensaml.xacml.ctx.AttributeType;
import org.opensaml.xacml.ctx.AttributeValueType;
import org.opensaml.xacml.ctx.EnvironmentType;
import org.opensaml.xacml.ctx.RequestType;
import org.opensaml.xacml.ctx.ResourceType;
import org.opensaml.xacml.ctx.SubjectType;
import org.opensaml.xacml.ctx.impl.ActionTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.AttributeTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.AttributeValueTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.EnvironmentTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.RequestTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.ResourceTypeImplBuilder;
import org.opensaml.xacml.ctx.impl.SubjectTypeImplBuilder;
import org.opensaml.xacml.profile.saml.SAMLProfileConstants;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.Namespace;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.encryption.EncryptedData;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.util.XMLHelper;
import org.osiam.xacml.crypto.CryptoProvider;
import org.osiam.xacml.crypto.KeyStoreConfig;
import org.osiam.xacml.pep.api.AbstractPepApiInputArtifact;
import org.osiam.xacml.pep.api.Action;
import org.osiam.xacml.pep.api.Environment;
import org.osiam.xacml.pep.api.PolicyEnforcementPoint;
import org.osiam.xacml.pep.api.PolicyResult;
import org.osiam.xacml.pep.api.PolicyResultProcessor;
import org.osiam.xacml.pep.api.Resource;
import org.osiam.xacml.pep.api.Resources;
import org.osiam.xacml.pep.api.Subject;
import org.osiam.xacml.pep.api.Subjects;
import org.osiam.xacml.saml.objects.RequestAbstract;
import org.osiam.xacml.saml.objects.RequestAbstractImplBuilder;
import org.osiam.xacml.saml.objects.XACMLAuthzDecisionQueryType;
import org.osiam.xacml.saml.objects.XACMLAuthzDecisionQueryTypeImplBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Implementation of the policy enforcement point
 */
public class PolicyEnforcementPointImpl implements PolicyEnforcementPoint {
	private XMLObjectBuilderFactory builderFactory;
	private MarshallerFactory marshallerFactory;
	private BasicParserPool parserPool;
	private static Logger logger = Logger.getLogger(PolicyEnforcementPointImpl.class.getName());

	//switch to enable openam compatible requests -> using request abstract
	private boolean openAmRequestMode = false; 

	private String pdpUrl = "";
	private String issuer = "";
	private DocumentBuilder documentBuilder = null; 
	
	private AttributeTypeImplBuilder attributeBuilder;
	private AttributeValueTypeImplBuilder attributeValueBuilder;	
	private KeyStoreConfig storeConfig;
	private CryptoProvider cryptoProvider;
	
	/**
	 * Constructor
	 * 
	 * @param pdp_url
	 *            the handler class url
	 * @param issuer
	 *            the issuer for the request
	 * @throws ParserConfigurationException
	 * @throws ConfigurationException
	 */
	protected PolicyEnforcementPointImpl(String pdp_url, String issuer, KeyStoreConfig storeConfig)
			throws ParserConfigurationException, ConfigurationException {
		this(pdp_url, issuer, storeConfig, false);
	}
	/**
	 * Constructor
	 * 
	 * @param pdp_url
	 *            the handler class url
	 * @param issuer
	 *            the issuer for the request
	 * @throws ParserConfigurationException
	 * @throws ConfigurationException
	 */
	protected PolicyEnforcementPointImpl(String pdp_url, String issuer, KeyStoreConfig storeConfig, boolean openAmMode)
			throws ParserConfigurationException, ConfigurationException {
		this.setConfiguredPdpUrl(pdp_url);
		this.setConfiguredIssuer(issuer);
		this.setStoreConfig(storeConfig);
		this.setOpenAmRequestMode(openAmMode);
		cryptoProvider = new CryptoProvider(getStoreConfig());
	}

	protected void setBasicParserPool(BasicParserPool parserPool) {
		if (parserPool == null) {
			throw new IllegalArgumentException(
					"BasicParserPool must NOT be null!");
		}

		this.parserPool = parserPool;
	}

	protected void setMarshallerFactory(MarshallerFactory marshallerFactory) {
		if (marshallerFactory == null) {
			throw new IllegalArgumentException(
					"MarshallerFactory must NOT be null!");
		}

		this.marshallerFactory = marshallerFactory;
	}
	
	protected void setDocumentBuilder(DocumentBuilder documentBuilder) {
		if (documentBuilder == null) {
			throw new IllegalArgumentException(
					"DocumentBuilder must NOT be null!");
		}

		this.documentBuilder = documentBuilder;
	}

	protected void setXmlObjectBuilderFactory(
			XMLObjectBuilderFactory xmlObjectBuilderFactory) {
		if (xmlObjectBuilderFactory == null) {
			throw new IllegalArgumentException(
					"XMLObjectBuilderFactory must NOT be null!");
		}
		this.builderFactory = xmlObjectBuilderFactory;

		attributeValueBuilder = (AttributeValueTypeImplBuilder) builderFactory
				.getBuilder(AttributeValueType.DEFAULT_ELEMENT_NAME);

		attributeBuilder = (AttributeTypeImplBuilder) builderFactory
				.getBuilder(AttributeType.DEFAULT_ELEMENT_NAME);
	}


	@Override
	public PolicyResult query(Element requestElement) {
		UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory(); 
		Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(requestElement); 
		RequestType request;
		try {
			request = (RequestType)unmarshaller.unmarshall(requestElement);
		} catch (UnmarshallingException e) {
			throw new RuntimeException(e);
		}
		if (getConfiguredPdPUrl().length() == 0) {
			throw new IllegalStateException(
					"NO PDP_URL is set! either call setConfiguredPdPUrl prior to this method "
							+ "or call "
							+ "query(List<Subject> subjects, List<Resource> resources, Action action, Environment environment, String pdpUrl) ");
		}
		SignableSAMLObject samlRequest = null;
		if (this.isOpenAmRequestMode()){
			//TODO set correct time zone
			samlRequest = buildRequestAbstract(
				new DateTime(DateTimeZone.forOffsetHours(+1)), request, getConfiguredIssuer());
		}else{
			samlRequest = buildXacmlAuthzDecisionQuery(new DateTime(DateTimeZone.forOffsetHours(+1)), request, issuer);
		}
		return invoke(samlRequest, getConfiguredPdPUrl());
	}

	/** {@inheritDoc} */
	public PolicyResult query(Subjects subjects, Resources resources,
			Action action, Environment environment) {
		if (pdpUrl.length() == 0) {
			throw new IllegalStateException(
					"NO PDP_URL is set! either call setConfiguredPdPUrl prior to this method "
							+ "or call "
							+ "query(List<Subject> subjects, List<Resource> resources, Action action, Environment environment, String pdpUrl) ");
		}
		return query(subjects, resources, action, environment, getConfiguredPdPUrl(), getConfiguredIssuer());
	}

	/** {@inheritDoc} */
	public PolicyResult query(Subjects subjects, Resources resources,
			Action action, Environment environment, String pdpUrl, String issuer) {

		checkParameters(subjects, resources, pdpUrl, issuer);

		RequestType request = buildRequest(subjects, resources, action, environment);
		// Build the outgoing message structures
		SignableSAMLObject samlRequest = null;
		if (this.isOpenAmRequestMode()){
			//TODO set correct time zone
			samlRequest = buildRequestAbstract(new DateTime(DateTimeZone.forOffsetHours(+1)), request, getConfiguredIssuer());
		}else{
			samlRequest = buildXacmlAuthzDecisionQuery(new DateTime(DateTimeZone.forOffsetHours(+1)), request, getConfiguredIssuer());
		}		
		return invoke(samlRequest, pdpUrl);
	}

	/** {@inheritDoc} */
	public String getConfiguredPdPUrl() {
		return pdpUrl;
	}

	/**
	 * Sets the url for the PDP to be queried
	 */
	protected void setConfiguredPdpUrl(String url) {
		if (url == null) {
			throw new IllegalArgumentException("Url must not be null!");
		}

		this.pdpUrl = url;
	}

	/** {@inheritDoc} */
	public String getConfiguredIssuer() {
		return issuer;

	}
	
	
	public boolean isOpenAmRequestMode() {
		return openAmRequestMode;
	}

	protected void setOpenAmRequestMode(boolean openAmRequestMode) {
		this.openAmRequestMode = openAmRequestMode;
	}

	/**
	 * sets the issuer of the request
	 * 
	 * @param issuer
	 *            the issuer from whom the request originates.
	 */
	protected void setConfiguredIssuer(String issuer) {
		if (issuer == null) {
			throw new IllegalArgumentException("Issuer must not be null!");
		}

		this.issuer = issuer;
	}
	

	public KeyStoreConfig getStoreConfig() {
		return storeConfig;
	}

	private void setStoreConfig(KeyStoreConfig storeConfig) {
		if (storeConfig == null) {
			throw new IllegalArgumentException("StoreConfig must not be null!");
		}
		this.storeConfig = storeConfig;
	}

	/**
	 * Checks, if any parameter is null, contains null or is an empty
	 * Collection.
	 */
	private void checkParameters(Subjects subjects, Resources resources,
			String pdpUrl, String issuer) {
		if (subjects == null || resources == null || pdpUrl == null
				|| issuer == null) {
			throw new IllegalArgumentException("Parameters MUST NOT be null!");
		}

		if (subjects.getSubjects().size() == 0
				|| resources.getResources().size() == 0) {
			throw new IllegalArgumentException("Parameters MUST NOT be empty!");
		}

		if (subjects.getSubjects().contains(null)
				|| resources.getResources().contains(null)) {
			throw new IllegalArgumentException(
					"Parameters MUST NOT contain null!");
		}

	}


	// Process the request and returns the decision
	private PolicyResult invoke(SignableSAMLObject request, String pdp_url) {

		Envelope envelope = null;
		if(storeConfig.getEnableSignatureAndEncryption()){
			//sign and/or encrypt the request
			SignableSAMLObject signedRequest = cryptoProvider.signRequest(request);
			EncryptedData encryptedRequest = cryptoProvider.setEncryption(signedRequest);
			
			//build the soap envelope
			envelope = buildSOAP11Envelope(encryptedRequest);
		}
		else{
			envelope = buildSOAP11Envelope(request);
		}
		// SOAP context used by the SOAP client
		BasicSOAPMessageContext soapContext = new BasicSOAPMessageContext();
		soapContext.setOutboundMessage(envelope);
		// Build the SOAP client
		// HttpClientBuilder clientBuilder = new HttpClientBuilder();
		HttpClientBuilder clientBuilder = HttpClientBuilderFactory.newHttpClientBuilder();

		HttpSOAPClient soapClient = new HttpSOAPClient(
				clientBuilder.buildClient(), parserPool);
		;

		// Send the message
		try {

			soapClient.send(pdp_url, soapContext);

			// Access the SOAP response envelope
			Envelope soapResponse = (Envelope) soapContext.getInboundMessage();

			// Parse the response and get the decision
			marshallEnvelope(soapResponse);
			PolicyResultProcessor result = new PolicyResultProcessor(getStoreConfig(), openAmRequestMode);
			PolicyResult response = result.processResult(soapResponse);

			return response;

		} catch (SecurityException e) {
			logger.log(Level.ERROR, e);
		} catch (SecurityPolicyException e) {
			logger.log(Level.ERROR, e);
		} catch (SOAPException e) {
			logger.log(Level.ERROR, e);
		} catch (org.opensaml.xml.security.SecurityException e) {
			logger.log(Level.ERROR, e);
		}
		return null;
	}

	// Builds the SOAP envelope
	private Envelope buildSOAP11Envelope(XMLObject payload) {
		Envelope envelope = (Envelope) builderFactory.getBuilder(
				Envelope.DEFAULT_ELEMENT_NAME).buildObject(
				Envelope.DEFAULT_ELEMENT_NAME);
		Body body = (Body) builderFactory.getBuilder(Body.DEFAULT_ELEMENT_NAME)
				.buildObject(Body.DEFAULT_ELEMENT_NAME);

		body.getUnknownXMLObjects().add(payload);
		envelope.setBody(body);

		marshallEnvelope(envelope);

		return envelope;

	}
	
	// Build XACMLAuthzDecisionQuery
	private XACMLAuthzDecisionQueryType buildXacmlAuthzDecisionQuery(DateTime dateTime,
			RequestType request, String issuer) {
		
		
		XACMLAuthzDecisionQueryTypeImplBuilder xacmlDecisionQueryBuilder =
			(XACMLAuthzDecisionQueryTypeImplBuilder)
			builderFactory.getBuilder(XACMLAuthzDecisionQueryType.DEFAULT_ELEMENT_NAME_XACML20);

		XACMLAuthzDecisionQueryType xacmlAuthzDecisionQuery =  xacmlDecisionQueryBuilder.buildObject(
//		SAMLProfileConstants.SAML20XACML20P_NS,
//		FIXME PDP is expecting this NS:
				org.osiam.xacml.saml.objects.SAMLConstants.AUTH_DECISION_QUERY_NS,
//		see: https://bugs.internet2.edu/jira/browse/JOST-34
				XACMLAuthzDecisionQueryType.DEFAULT_ELEMENT_LOCAL_NAME,
			SAMLProfileConstants.SAML20XACMLPROTOCOL_PREFIX);
		//TODO generate ID
		xacmlAuthzDecisionQuery.setID(org.osiam.xacml.saml.objects.SAMLConstants.REQUEST_ABSTRACT_ID);
//			xacmlAuthzDecisionQuery.setDestination("localhost");
		xacmlAuthzDecisionQuery.setIssuer(buildIssuer(issuer));
		xacmlAuthzDecisionQuery.setVersion(org.opensaml.common.SAMLVersion.VERSION_20);
		xacmlAuthzDecisionQuery.setRequest(request);

		return xacmlAuthzDecisionQuery;
	}

	// Build the SAML request abstract
	private RequestAbstract buildRequestAbstract(DateTime dateTime,
			RequestType request, String issuer) {

		// Get the right builder for this element.
		RequestAbstractImplBuilder requestAbstractBuilder = (RequestAbstractImplBuilder) builderFactory
				.getBuilder(RequestAbstract.DEFAULT_ELEMENT_NAME_XACML20);

		// Build the object. The method needs input on what
		// XACML version we attend to use.
		RequestAbstract requestAbstract = requestAbstractBuilder.buildObject(
				org.osiam.xacml.saml.objects.SAMLConstants.SAML2_NS,
				RequestAbstract.DEFAULT_ELEMENT_LOCAL_NAME,
				org.osiam.xacml.saml.objects.SAMLConstants.SAML2_PREFIX);

		Namespace xsins = new
		Namespace(SAMLConstants.XSI_NS, SAMLConstants.XSI_PREFIX); 
		requestAbstract.addNamespace(xsins);
		//TODO generate ID
		requestAbstract.setID(org.osiam.xacml.saml.objects.SAMLConstants.REQUEST_ABSTRACT_ID);
		requestAbstract.setIssueInstant(dateTime);
		requestAbstract.setType(org.osiam.xacml.saml.objects.SAMLConstants.XACML_QUERY_TYPE);
		requestAbstract.setIssuer(buildIssuer(issuer));
		
		requestAbstract.setRequest(request);

		return requestAbstract;
	}

	// Parse the attribute value
	@SuppressWarnings("serial")
	private List<String> parseFirstAttributeValue(AbstractPepApiInputArtifact artifact) {
		List<String> values = new ArrayList<String>(){};
		if (artifact != null) {
			if (artifact.getAttributes().size() > 0) {
				for (int i=0; i<artifact.getAttributes().size(); i++){
					// is guaranteed never to return null
					values.add(artifact.getAttributes().get(i).getValue());
				}
			}
		}
		return values;
	}

	// Parse the attribute ID
	@SuppressWarnings("serial")
	private List<String> parseFirstAttributeID(AbstractPepApiInputArtifact artifact) {
		List<String> IDs = new ArrayList<String>(){};
		if (artifact != null) {
			if (artifact.getAttributes().size() > 0) {
				for (int i=0; i<artifact.getAttributes().size(); i++){
					// is guaranteed never to return null
					IDs.add(artifact.getAttributes().get(i).getId());
				}
			}
		}
		return IDs;
	}

	// Parse the attribute data types
	@SuppressWarnings("serial")
	private List<String> parseFirstAttributeDataType(AbstractPepApiInputArtifact artifact) {
		List<String> dataTypes = new ArrayList<String>(){};
		if (artifact != null) {
			if (artifact.getAttributes().size() > 0) {
				for (int i=0; i<artifact.getAttributes().size(); i++){
					// is guaranteed never to return null
					dataTypes.add(artifact.getAttributes().get(i).getDataType());
				}
			}
		}
		return dataTypes;
	}

	// Builds the issuer in the request
	private Issuer buildIssuer(String issuerValue) {
		IssuerBuilder issuerBuilder = (IssuerBuilder) builderFactory
				.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);

		Issuer issuer = issuerBuilder.buildObject(SAMLConstants.SAML20_NS,
				Issuer.DEFAULT_ELEMENT_LOCAL_NAME, SAMLConstants.SAML20_PREFIX);

		issuer.setValue(issuerValue);

		return issuer;
	}

	// Builds the XACML request
	private RequestType buildRequest(Subjects subjects,
			Resources resources, Action actionItem,
			Environment environmentItem) {

		boolean actionFlag = true;
		boolean envFlag = true;
		List<ResourceType> resourceList = new ArrayList<ResourceType>();
		List <SubjectType> subjectList = new ArrayList<SubjectType>();

		for (int i= 0; i<resources.getResources().size(); i++){
			resourceList.add(buildResource(resources.getResources().get(i)));
		}
		
		ActionType action = buildAction(actionFlag, actionItem);

		for (int i = 0; i<subjects.getSubjects().size(); i++){
			subjectList.add(buildSubject(subjects.getSubjects().get(i)));
		}
		
		EnvironmentType env = buildEnvironment(envFlag,environmentItem);

		return buildRequest(subjectList, action, resourceList, env);

	}

	private ResourceType buildResource(Resource resourceItem) {

		List<String> resourceString = parseFirstAttributeValue(resourceItem);	
		List<String> resourceID = parseFirstAttributeID(resourceItem);
		List<String> resourceType = parseFirstAttributeDataType(resourceItem);
		
		List<AttributeType> attributeList = new ArrayList<AttributeType>();

		ResourceTypeImplBuilder resourceBuilder = (ResourceTypeImplBuilder) builderFactory
				.getBuilder(ResourceType.DEFAULT_ELEMENT_NAME);

		ResourceType resource = resourceBuilder.buildObject();

		
		for (int i=0; i<resourceString.size(); i++){
			
			AttributeValueType resourceValue = attributeValueBuilder.buildObject(
					AttributeValueType.DEFAULT_ELEMENT_NAME,
					AttributeValueType.TYPE_NAME);

			AttributeType xacmlResourceAttribute = attributeBuilder.buildObject(
					XACMLConstants.XACML20CTX_NS,
					AttributeType.DEFAULT_ELEMENT_LOCAL_NAME,
					XACMLConstants.XACMLCONTEXT_PREFIX);
			
			resourceValue.setValue(resourceString.get(i));
			
			xacmlResourceAttribute.setAttributeID(resourceID.get(i));
			xacmlResourceAttribute.setDataType(resourceType.get(i));
			xacmlResourceAttribute.getAttributeValues().add(resourceValue);
			
			attributeList.add(xacmlResourceAttribute);
			
		}
		resource.getAttributes().addAll(attributeList);
		return resource;

	}

	private ActionType buildAction(boolean actionFlag, Action actionItem) {
		
		List<String> actionString = null;
		List<String> actionID = null;
		List<String> actionType = null;
		
		List<AttributeType> attributeList = new ArrayList<AttributeType>();

		if (actionItem != null) {
			actionFlag = false;
			actionString = parseFirstAttributeValue(actionItem);
			actionID = parseFirstAttributeID(actionItem);
			actionType = parseFirstAttributeDataType(actionItem);
		}
		
		ActionTypeImplBuilder actionBuilder = (ActionTypeImplBuilder) builderFactory
				.getBuilder(ActionType.DEFAULT_ELEMENT_NAME);

		ActionType action = actionBuilder.buildObject();

		if (actionFlag == false) {			
			for (int i =0; i<actionString.size(); i++){
				
				AttributeValueType actionValue = attributeValueBuilder.buildObject(
						AttributeValueType.DEFAULT_ELEMENT_NAME,
						AttributeValueType.TYPE_NAME);
				
				AttributeType xacmlActionAttribute = attributeBuilder.buildObject(
						XACMLConstants.XACML20CTX_NS,
						AttributeType.DEFAULT_ELEMENT_LOCAL_NAME,
						XACMLConstants.XACMLCONTEXT_PREFIX);
				
				// setting the value of the attribute value element
				actionValue.setValue(actionString.get(i));
	
				xacmlActionAttribute.setAttributeID(actionID.get(i));
				xacmlActionAttribute.setDataType(actionType.get(i));
				xacmlActionAttribute.getAttributeValues().add(actionValue);
	
				attributeList.add(xacmlActionAttribute);
			}
		}
		
		// adding all attributes
		action.getAttributes().addAll(attributeList);
		return action;
	}

	private SubjectType buildSubject(Subject subjectItem) {
		
		List<String> subjectString = parseFirstAttributeValue(subjectItem);
		List<String> subjectID = parseFirstAttributeID(subjectItem);
		List<String> subjectType = parseFirstAttributeDataType(subjectItem);
		
		List<AttributeType> attributeList = new ArrayList<AttributeType>();
		
		
		SubjectTypeImplBuilder subjectBuilder = (SubjectTypeImplBuilder) builderFactory
				.getBuilder(SubjectType.DEFAULT_ELEMENT_NAME);
		
		SubjectType subject = subjectBuilder.buildObject();
		
		
		for (int i=0; i<subjectString.size(); i++){
			
			AttributeType xacmlSubjectAttribute = attributeBuilder.buildObject(
					XACMLConstants.XACML20CTX_NS,
					AttributeType.DEFAULT_ELEMENT_LOCAL_NAME,
					XACMLConstants.XACMLCONTEXT_PREFIX);

			AttributeValueType subjectValue = attributeValueBuilder.buildObject(
					AttributeValueType.DEFAULT_ELEMENT_NAME,
					AttributeValueType.TYPE_NAME);
			
			subjectValue.setValue(subjectString.get(i));
			
			xacmlSubjectAttribute.setAttributeID(subjectID.get(i));
			xacmlSubjectAttribute.setDataType(subjectType.get(i));
			xacmlSubjectAttribute.getAttributeValues().add(subjectValue);
	
			attributeList.add(xacmlSubjectAttribute);
		}
		
		subject.getAttributes().addAll(attributeList);
		return subject;
	}

	private EnvironmentType buildEnvironment(boolean envFlag,
			Environment environmentItem) {
		
		List<String> environmentString = null;
		List<String> environmentID = null;
		List<String> environmentType = null;
		
		List<AttributeType> attributeList = new ArrayList<AttributeType>();
		

		if (environmentItem != null) {
			envFlag = false;
			environmentString = parseFirstAttributeValue(environmentItem);
			environmentID = parseFirstAttributeID(environmentItem);
			environmentType = parseFirstAttributeDataType(environmentItem);
		}
		
		EnvironmentTypeImplBuilder envBuilder = (EnvironmentTypeImplBuilder) builderFactory
				.getBuilder(EnvironmentType.DEFAULT_ELEMENT_NAME);

		EnvironmentType env = envBuilder.buildObject();

		if (envFlag == false) {
			
			for (int i=0; i<environmentString.size(); i++){
				
				AttributeValueType environmentValue = attributeValueBuilder
				.buildObject(AttributeValueType.DEFAULT_ELEMENT_NAME,
						AttributeValueType.TYPE_NAME);

				AttributeType xacmlEnvironmentAttribute = attributeBuilder
				.buildObject(XACMLConstants.XACML20CTX_NS, AttributeType.DEFAULT_ELEMENT_LOCAL_NAME,
						XACMLConstants.XACMLCONTEXT_PREFIX);
		
				environmentValue.setValue(environmentString.get(i));
	
				xacmlEnvironmentAttribute.setAttributeID(environmentID.get(i));
				xacmlEnvironmentAttribute.setDataType(environmentType.get(i));
				xacmlEnvironmentAttribute.getAttributeValues().add(environmentValue);
				
				attributeList.add(xacmlEnvironmentAttribute);
			}
		}
		
		env.getAttributes().addAll(attributeList);
		return env;

	}

	private RequestType buildRequest(List<SubjectType> subjectList, ActionType action,
			List<ResourceType> resourceList, EnvironmentType env) {
		RequestTypeImplBuilder requestBuilder = (RequestTypeImplBuilder) builderFactory
				.getBuilder(RequestType.DEFAULT_ELEMENT_NAME);

		RequestType request = requestBuilder.buildObject();

		for (int i = 0; i<subjectList.size(); i++){
			request.getSubjects().add(subjectList.get(i));
		}
		
		request.setAction(action);
		
		for (int i = 0; i<resourceList.size(); i++){
			request.getResources().add(resourceList.get(i));
		}
		
		request.setEnvironment(env);

		return request;

	}

	// Helper method for marshalling the XML object
	private void marshallEnvelope(Envelope request) {

		try {
			Document doc = documentBuilder.newDocument();
			Marshaller marshallerRequest = marshallerFactory
					.getMarshaller(Envelope.DEFAULT_ELEMENT_NAME);
			Element element = marshallerRequest.marshall(request, doc);
			String output = XMLHelper.prettyPrintXML(element);

			logger.log(Level.INFO, "Printing soap-Envelope: " + output);
			//TODO remove System.out
			System.out.println("Printing soap-Envelope: " + output);
		} catch (MarshallingException e) {
			logger.log(Level.ERROR, e);
		}

	}

}
