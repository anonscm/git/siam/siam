package org.osiam.xacml;

import org.opensaml.ws.soap.client.http.HttpClientBuilder;

/**
 * 
 * 
 * @author oweile
 */
public class HttpClientBuilderFactory {
	private static final String PROXY_HOST = "http.proxyHost";
	private static final String PROXY_PORT = "http.proxyPort";

	private HttpClientBuilderFactory() {
		throw new AssertionError();
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public static HttpClientBuilder newHttpClientBuilder() {
		HttpClientBuilder clientBuilder = new HttpClientBuilder();

		setProxyHost(clientBuilder);
		setProxyPort(clientBuilder);

		return clientBuilder;
	}

	private static void setProxyHost(HttpClientBuilder clientBuilder) {
		String proxyHost = System.getProperty("http.proxyHost");

		if (proxyHost != null) {
			clientBuilder.setProxyHost(proxyHost);
		}
	}

	private static void setProxyPort(HttpClientBuilder clientBuilder) {
		String proxyPort = System.getProperty("http.proxyPort");

		if (proxyPort != null) {
			clientBuilder.setProxyPort(Integer.parseInt(proxyPort));
		}
	}
}