/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.crypto;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.osiam.xacml.pep.api.PolicyResultProcessor;

public class ClientLibCredential {

	private static Logger logger = Logger.getLogger(PolicyResultProcessor.class.getName());

	 /**
	  * Returns credential, containing private key and pep public key (certificate)
	  * used for: PEP->PDP sign
	  * PDP->PEP decryption
	  * @param storeConfig key store configuration
	  * @return credentials
	  */
	public Credential getSignCredential(KeyStoreConfig storeConfig) {
        PrivateKey privateKey = null;
        X509Certificate cert = null;
        
        privateKey = (PrivateKey) getPrivateKey(storeConfig.getKeyStoreFilename(), storeConfig.getPrivateKeyAlias(),
        		storeConfig.getKeystorePwd(), storeConfig.getKeyPwd());
		cert = getCert(storeConfig.getKeyStoreFilename(), storeConfig.getPrivateKeyAlias(), storeConfig.getKeystorePwd());
        
		BasicX509Credential credential = new BasicX509Credential();
        credential.setEntityCertificate(cert);
        credential.setPrivateKey(privateKey);
        credential.setPublicKey(cert.getPublicKey());
        
        return credential;
    }
    
    /**
     * Returns credential, containing private key and pdp public key (certificate)
     * used for: PEP->PDP encryption
     * PDP->PEP signature validation
     * 
     * @param storeConfig key store configuration
     * @return credentials
     */
    public Credential getEncCredential(KeyStoreConfig storeConfig) {
    	
        PrivateKey privateKey = null;
        X509Certificate cert = null;
        
        privateKey = (PrivateKey) getPrivateKey(storeConfig.getKeyStoreFilename(), storeConfig.getPrivateKeyAlias(),
        		storeConfig.getKeystorePwd(), storeConfig.getKeyPwd());
		cert = getCert(storeConfig.getTrustStoreFilename(), storeConfig.getPublicKeyAlias(), storeConfig.getKeystorePwd());
        
		BasicX509Credential credential = new BasicX509Credential();
        credential.setEntityCertificate(cert);
        credential.setPrivateKey(privateKey);
        credential.setPublicKey(cert.getPublicKey());
        
        return credential;
    }
    
    
	/**
	 * Returns the private key from keystore. 
	 * @param filename Keystore file
	 * @param alias key alias
	 */
	private Key getPrivateKey(String filename, String alias,char[] keystorePwd, char[] keyPwd){
		
		PrivateKey priv = null;
		
		try {
			
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");
			InputStream ksfis = new FileInputStream(filename);
		    BufferedInputStream ksbufin = new BufferedInputStream(ksfis);  
		    
		    ks.load(ksbufin, keystorePwd);
		    priv = (PrivateKey) ks.getKey(alias, keyPwd);
		    
		} catch (KeyStoreException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (CertificateException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		}
		
		return priv;
	}

	
	/**
	 * Returns the certificate. 
	 * @param filename certificate file
	 * @param alias key alias
	 */
	private X509Certificate getCert(String filename, String alias, char[] keystorePwd){
		
		X509Certificate cert = null;
		
		try {
			
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");
			InputStream ksfis = new FileInputStream(filename);
		    BufferedInputStream ksbufin = new BufferedInputStream(ksfis);  
		    
		    ks.load(ksbufin, keystorePwd);
		    cert = (X509Certificate) ks.getCertificate(alias);
		    
		} catch (KeyStoreException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (CertificateException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		}
		
		return cert;
	}
}
