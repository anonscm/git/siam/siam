/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.crypto;

public class KeyStoreConfig {
	
	String keyStoreFilename;
	String trustStoreFilename;
	String privateKeyAlias; 
	String publicKeyAlias;
	Boolean enableSignatureAndEncryption;
	char[] keystorePwd;
	char[] keyPwd;
	
	public Boolean getEnableSignatureAndEncryption() {
		return enableSignatureAndEncryption;
	}
	public void setEnableSignatureAndEncryption(Boolean enableSignatureAndEncryption) {
		this.enableSignatureAndEncryption = enableSignatureAndEncryption;
	}
	public String getKeyStoreFilename() {
		return keyStoreFilename;
	}
	public void setKeyStoreFilename(String keyStoreFilename) {
		this.keyStoreFilename = keyStoreFilename;
	}
	public String getTrustStoreFilename() {
		return trustStoreFilename;
	}
	public void setTrustStoreFilename(String trustStoreFilename) {
		this.trustStoreFilename = trustStoreFilename;
	}
	public String getPrivateKeyAlias() {
		return privateKeyAlias;
	}
	public void setPrivateKeyAlias(String privateKeyAlias) {
		this.privateKeyAlias = privateKeyAlias;
	}
	public String getPublicKeyAlias() {
		return publicKeyAlias;
	}
	public void setPublicKeyAlias(String publicKeyAlias) {
		this.publicKeyAlias = publicKeyAlias;
	}
	public char[] getKeystorePwd() {
		return keystorePwd;
	}
	public void setKeystorePwd(char[] keystorePwd) {
		this.keystorePwd = keystorePwd;
	}
	public char[] getKeyPwd() {
		return keyPwd;
	}
	public void setKeyPwd(char[] keyPwd) {
		this.keyPwd = keyPwd;
	}

}
