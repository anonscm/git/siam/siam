package org.osiam.xacml.crypto;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.EncryptedAssertion;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.encryption.Decrypter;
import org.opensaml.saml2.encryption.Encrypter;
import org.opensaml.saml2.encryption.Encrypter.KeyPlacement;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.encryption.EncryptedData;
import org.opensaml.xml.encryption.EncryptionConstants;
import org.opensaml.xml.encryption.EncryptionException;
import org.opensaml.xml.encryption.EncryptionParameters;
import org.opensaml.xml.encryption.InlineEncryptedKeyResolver;
import org.opensaml.xml.encryption.KeyEncryptionParameters;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorFactory;
import org.opensaml.xml.security.keyinfo.StaticKeyInfoCredentialResolver;
import org.opensaml.xml.security.x509.X509KeyInfoGeneratorFactory;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.validation.ValidationException;

public class CryptoProvider {
	private static Logger logger = Logger.getLogger(CryptoProvider.class.getName());
	private KeyStoreConfig storeConfig;
	
	
	public CryptoProvider(KeyStoreConfig storeConfig) {
		this.storeConfig = storeConfig;
		
	}

	public EncryptedData setEncryption(SignableSAMLObject requestAbstract){
		
		ClientLibCredential cred = new ClientLibCredential();
		
		EncryptedData encRequest = null;
		
		//Private key
		Credential keyEncryptionCredential = cred.getEncCredential(storeConfig);
		
		EncryptionParameters encParams = new EncryptionParameters();
		encParams.setAlgorithm(EncryptionConstants.ALGO_ID_BLOCKCIPHER_AES128);
		
		KeyEncryptionParameters kekParams = new KeyEncryptionParameters();
		kekParams.setEncryptionCredential(keyEncryptionCredential);
		kekParams.setAlgorithm(EncryptionConstants.ALGO_ID_KEYTRANSPORT_RSAOAEP);
		
		KeyInfoGeneratorFactory kigf =
		    Configuration.getGlobalSecurityConfiguration()
		    .getKeyInfoGeneratorManager().getDefaultManager()
		    .getFactory(keyEncryptionCredential);
		
		kekParams.setKeyInfoGenerator(kigf.newInstance());

		Encrypter samlEncrypter = new Encrypter(encParams, kekParams);
		samlEncrypter.setKeyPlacement(KeyPlacement.PEER);

		try {
			encRequest = samlEncrypter.encryptElement(requestAbstract, encParams, kekParams);
		} catch (EncryptionException e) {
			logger.log(Level.ERROR, e);
		}
		return encRequest;
	}
	
	
	public SignableSAMLObject signRequest(SignableSAMLObject requestAbstract){
		
		ClientLibCredential cred = new ClientLibCredential();
		
		X509KeyInfoGeneratorFactory kiFactory = new X509KeyInfoGeneratorFactory();
		kiFactory.setEmitEntityCertificate(true);
		KeyInfoGenerator kiGenerator = kiFactory.newInstance();
		KeyInfo keyInfo = null;

		//Public key
		Credential signingCredential = cred.getSignCredential(storeConfig);
		
		Signature signature = (Signature) Configuration.getBuilderFactory()
        .getBuilder(Signature.DEFAULT_ELEMENT_NAME)
        .buildObject(Signature.DEFAULT_ELEMENT_NAME);

	
		signature.setSigningCredential(signingCredential);
		signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1);
		signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);

			try {
				keyInfo = kiGenerator.generate(signingCredential);
			} catch (org.opensaml.xml.security.SecurityException e) {
				logger.log(Level.ERROR, e);
			}

		signature.setKeyInfo(keyInfo);
		
		requestAbstract.setSignature(signature);
		
		
		try {
			Configuration.getMarshallerFactory().getMarshaller(requestAbstract).marshall(requestAbstract);
		} catch (MarshallingException e) {
			logger.log(Level.ERROR, e);
		}

		try {
			Signer.signObject(signature);
		} catch (SignatureException e) {
			logger.log(Level.ERROR, e);
		}
		
		return requestAbstract;
	}
	
	
	//Validate the signature
	public boolean validate(Response response){

		boolean valid = true;
		ClientLibCredential cred = new ClientLibCredential();
		
		Signature signatureToValidate = response.getSignature();
		SAMLSignatureProfileValidator profileValidator = new SAMLSignatureProfileValidator();
		
		try {
		    profileValidator.validate(signatureToValidate);
		} catch (ValidationException e) {
		    // Indicates signature did not conform to SAML Signature profile
			logger.log(Level.ERROR, e);
			valid = false;
		}
		
		Credential verificationCredential = cred.getEncCredential(storeConfig);
		SignatureValidator sigValidator = new SignatureValidator(verificationCredential);
		try {
		    sigValidator.validate(signatureToValidate);
		} catch (ValidationException e) {
		    // Indicates signature was not cryptographically valid, or possibly a processing error
			logger.log(Level.ERROR, e);
			valid = false;
		}
		
		return valid;
	}
	
	//Decrypt SAML Response
	public Assertion decrypt(EncryptedAssertion encryptedAssertion){

		ClientLibCredential cred = new ClientLibCredential();
		
		// This credential - contains the recipient's PrivateKey to be used for key decryption
		Credential decryptionCredential = cred.getSignCredential(storeConfig);

		StaticKeyInfoCredentialResolver skicr =
		  new StaticKeyInfoCredentialResolver(decryptionCredential);

		// The EncryptedKey is assumed to be contained within the 
		// EncryptedAssertion/EncryptedData/KeyInfo.       
		Decrypter samlDecrypter =
		  new Decrypter(null, skicr, new InlineEncryptedKeyResolver());
		
		Assertion assertion = null;
		try {
			assertion = samlDecrypter.decrypt(encryptedAssertion);
		} catch (DecryptionException e) {
			logger.log(Level.ERROR, e);
		}
		
		return assertion;
	}
}
