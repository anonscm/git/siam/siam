/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.pdp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.AccessController;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jboss.security.xacml.sunxacml.Indenter;
import org.jboss.security.xacml.sunxacml.ParsingException;
import org.jboss.security.xacml.sunxacml.ctx.RequestCtx;
import org.jboss.security.xacml.sunxacml.ctx.ResponseCtx;
import org.osiam.xacml.pdp.XacmlPdpConnector;
import org.osiam.xacml.pip.TarentIdmAttributeFinderConfiguration;
import org.osiam.xacml.policy_finder.PolicyRepoConfiguration;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.IdConstants;
import com.sun.identity.saml2.assertion.Assertion;
import com.sun.identity.saml2.assertion.AssertionFactory;
import com.sun.identity.saml2.assertion.Issuer;
import com.sun.identity.saml2.common.SAML2Exception;
import com.sun.identity.saml2.protocol.ProtocolFactory;
import com.sun.identity.saml2.protocol.RequestAbstract;
import com.sun.identity.saml2.soapbinding.RequestHandler;
import com.sun.identity.security.AdminTokenAction;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceConfig;
import com.sun.identity.sm.ServiceConfigManager;
import com.sun.identity.xacml.common.XACMLException;
import com.sun.identity.xacml.context.ContextFactory;
import com.sun.identity.xacml.context.Request;
import com.sun.identity.xacml.context.Response;
import com.sun.identity.xacml.saml2.XACMLAuthzDecisionQuery;
import com.sun.identity.xacml.saml2.XACMLAuthzDecisionStatement;

/**
 * This class is an implementation of SAML2 query RequestHandler to handle
 * XACMLAuthzDecisionQuery
 * 
 *  @author Christian Preilowski (c.preilowski@tarent.de)
 *  @author Jochen Todea
 * 
 */
public class XACMLAuthDecisionQueryHandlerSiamImpl implements RequestHandler {


	//configuration constants
	private static final String SUB_SCHEMA_IDM = "TarentIDM";
	private static final String SUB_SCHEMA_REPO = "PolicyRepo";
	private static final String POLICY_REPO_REALM = "realm";
	private static final String POLICY_REPO_ENDPOINT = "endpoint";
	private static final String POLICY_REPO_COMBINING_ALG = "combiningAlgorithm";
	private static final String IDM_ENDPOINT = "tarentIdmEndpointurl";
	private static final String IDM_CLIENTNAME = "tarentIdmClientName";
	
	private static Logger logger = Logger.getLogger(XACMLAuthDecisionQueryHandlerSiamImpl.class.getName());

	private PolicyRepoConfiguration policyRepoConfig;
	private TarentIdmAttributeFinderConfiguration tarentIdmAttrFinderConfig;


	/**
	 * This class is an implementation of SAML2 query RequestHandler to handle
	 * XACMLAuthzDecisionQuery
	 * 
	 * @throws SMSException SMSException
	 * @throws SSOException SSOException
	 * 
	 */
	@SuppressWarnings("unchecked")
	public XACMLAuthDecisionQueryHandlerSiamImpl() {
		
	}

	/**
	 * Processes an XACMLAuthzDecisionQuery and returns a SAML2 Response.
	 * 
	 * @param pdpEntityId  EntityID of PDP
	 * @param pepEntityId  EntityID of PEP
	 * @param samlpRequest SAML2 Request, an XAMLAuthzDecisionQuery
	 * @param soapMessage  SOAPMessage that carried the SAML2 Request
	 * @return SAML2 Response with an XAMLAuthzDecisionStatement
	 * @exception SAML2Exception  if the query can not be handled
	 */
	public com.sun.identity.saml2.protocol.Response handleQuery(
			String pdpEntityId, String pepEntityId,
			RequestAbstract samlpRequest, SOAPMessage soapMessage)
			throws SAML2Exception {

		logger.log(Level.INFO, "processing new xacml request");

		// Fetch the XACML-Request from SAMLP-Request and
		// transform in RequestCTX for the engine
		Request xacmlRequest = ((XACMLAuthzDecisionQuery) samlpRequest)
				.getRequest();

		String xacmlRequestString = xacmlRequest.toXMLString(false, false);

		RequestCtx request = null;
		try {
			InputStream is = new ByteArrayInputStream(xacmlRequestString.getBytes("UTF-8"));
			request = RequestCtx.getInstance(is);
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.ERROR, e);
			throw new RuntimeException(e);
		} catch (ParsingException e) {
			logger.log(Level.ERROR, e);
			throw new RuntimeException(e);
		}

		
		//Get the latest values for PolicyRepo and TarentIdm subSchemas from IdRepoService.xml
		try {
			getLatestSubSchemaValuesFromIdRepoService();
		} catch (SSOException e) {
			logger.log(Level.ERROR, e);
			throw new RuntimeException(e);
		} catch (SMSException e) {
			logger.log(Level.ERROR, e);
			throw new RuntimeException(e);
		}
		
		// Call the engine and get the Response
		XacmlPdpConnector pdpConnector = new XacmlPdpConnector(
				policyRepoConfig, tarentIdmAttrFinderConfig);
		ResponseCtx response = pdpConnector.callXacmlPdp(request);

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		response.encode(os, new Indenter());

		// Transform the ResponseCTX to Response
		Response resp = ContextFactory.getInstance().createResponse(
				os.toString());

		XACMLAuthzDecisionStatement statement = ContextFactory.getInstance()
				.createXACMLAuthzDecisionStatement();
		statement.setResponse(resp);

		// Generate SAMLP-Response
		com.sun.identity.saml2.protocol.Response samlpResponse = createSamlpResponse(
				statement, "");

		return samlpResponse;
	}

	private com.sun.identity.saml2.protocol.Response createSamlpResponse(
			XACMLAuthzDecisionStatement statement, String statusCodeValue)
			throws XACMLException, SAML2Exception {

		com.sun.identity.saml2.protocol.Response samlpResponse = ProtocolFactory
				.getInstance().createResponse();
		samlpResponse.setID("response-id:1");
		samlpResponse.setVersion("2.0");
		samlpResponse.setIssueInstant(new Date());

		com.sun.identity.saml2.protocol.StatusCode samlStatusCode = ProtocolFactory
				.getInstance().createStatusCode();
		samlStatusCode.setValue(statusCodeValue);
		com.sun.identity.saml2.protocol.Status samlStatus = ProtocolFactory
				.getInstance().createStatus();
		samlStatus.setStatusCode(samlStatusCode);
		samlpResponse.setStatus(samlStatus);

		Assertion assertion = AssertionFactory.getInstance().createAssertion();
		assertion.setVersion("2.0");
		assertion.setID("response-id:1");
		assertion.setIssueInstant(new Date());
		Issuer issuer = AssertionFactory.getInstance().createIssuer();
		issuer.setValue("issuer-1");
		assertion.setIssuer(issuer);
		List<String> statements = new ArrayList<String>();
		statements.add(statement.toXMLString(true, true));
		assertion.setStatements(statements);
		List<Assertion> assertions = new ArrayList<Assertion>();
		assertions.add(assertion);
		samlpResponse.setAssertion(assertions);
		return samlpResponse;
	}
	
	public String getPropertyStringValue(Map<String, Set<String>> configParams, String key) {
        String value = null;
        Set<String> valueSet = configParams.get(key);
        if (valueSet != null &&  valueSet.size()==1) {
            value = valueSet.iterator().next();
        } else {
                logger.error("error while getting config parameter for key" + key);
        }
        return value;
    }

	private void getLatestSubSchemaValuesFromIdRepoService() throws SMSException, SSOException{
		logger.log(Level.INFO,"initializing saml-xacml query handler.");
		
		// Initialize configuration from idRepoService.xml in Openam	
		SSOToken adminToken = (SSOToken) AccessController.doPrivileged(AdminTokenAction.getInstance());

		//TODO move configuration to federation pep/pdp config
		ServiceConfigManager idRepoServiceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, adminToken);
		ServiceConfig idRepoServiceConfig =idRepoServiceConfigManager.getOrganizationConfig("/", null);
		ServiceConfig subSchemaIDMConfig = idRepoServiceConfig.getSubConfig(SUB_SCHEMA_IDM);
		ServiceConfig subSchemaPRConfig =  idRepoServiceConfig.getSubConfig(SUB_SCHEMA_REPO);
		
		Map<String, Set<String>>  subSchemaIDMConfigParams = subSchemaIDMConfig.getAttributesForRead();
		String idmEndpoint = getPropertyStringValue(subSchemaIDMConfigParams, IDM_ENDPOINT);
		String idmClientName = getPropertyStringValue(subSchemaIDMConfigParams, IDM_CLIENTNAME);
		
		Map<String, Set<String>>  subSchemaPRParams = subSchemaPRConfig.getAttributesForRead();
		String policyRepoWSEndpoint = getPropertyStringValue(subSchemaPRParams, POLICY_REPO_ENDPOINT);
		String policyRepoRealm = getPropertyStringValue(subSchemaPRParams, POLICY_REPO_REALM);
		String policyRepoCombiningAlgorithm = getPropertyStringValue(subSchemaPRParams, POLICY_REPO_COMBINING_ALG);
		
		logger.log(Level.INFO, "CONFIG:\n idm-endpoint: '" + idmEndpoint
				+ "'\n idm client name: '" + idmClientName
				+ "'\n policy repo endpoint:'" + policyRepoWSEndpoint
				+ "'\n policy repo realm: '" + policyRepoRealm
				+ "'\n policy repo combining algorithm: '" + policyRepoCombiningAlgorithm + "'");
		
		if (idmEndpoint == null || idmClientName == null
				|| policyRepoWSEndpoint == null || policyRepoRealm == null) {
			throw new RuntimeException("one or more xacml engine configuration parameters are null!");
		}

		policyRepoConfig = new PolicyRepoConfiguration();
		policyRepoConfig.setRealm(policyRepoRealm);
		policyRepoConfig.setEndpoint(policyRepoWSEndpoint);
		policyRepoConfig.setDefaultCombiningAlgorithm( policyRepoCombiningAlgorithm );

		tarentIdmAttrFinderConfig = new TarentIdmAttributeFinderConfiguration();
		tarentIdmAttrFinderConfig.setRMIEndpoint(idmEndpoint);
		tarentIdmAttrFinderConfig.setClientName(idmClientName);

		
	}


}
