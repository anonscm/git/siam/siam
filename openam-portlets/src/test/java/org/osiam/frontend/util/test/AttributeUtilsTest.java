/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.util.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.osiam.frontend.util.AttributeUtils;

public class AttributeUtilsTest {

	@Test
	public void testRemoveIndexPrefix() {

		List<String> testList = new ArrayList<String>();
		testList.add("[3]=a3");
		testList.add("[0]=a0");
		testList.add("[2]=a2");
		testList.add("[4]=a4");
		testList.add("[1]=a1");

		final List<String> EXPECTED_LIST = new ArrayList<String>();
		EXPECTED_LIST.add("a0");
		EXPECTED_LIST.add("a1");
		EXPECTED_LIST.add("a2");
		EXPECTED_LIST.add("a3");
		EXPECTED_LIST.add("a4");

		List<String> returnedList = AttributeUtils.removeIndexPrefix(testList);

		Assert.assertEquals(EXPECTED_LIST, returnedList);

	}

	@Test
	public void testAddIndexPrefix() {

		List<String> testList = new ArrayList<String>();
		testList.add("a0");
		testList.add("a1");
		testList.add("a2");
		testList.add("a3");
		testList.add("a4");

		final List<String> EXPECTED_LIST = new ArrayList<String>();
		EXPECTED_LIST.add("[0]=a0");
		EXPECTED_LIST.add("[1]=a1");
		EXPECTED_LIST.add("[2]=a2");
		EXPECTED_LIST.add("[3]=a3");
		EXPECTED_LIST.add("[4]=a4");

		List<String> returnedList = AttributeUtils.addIndexPrefix(testList);

		Assert.assertEquals(EXPECTED_LIST, returnedList);

	}

}
