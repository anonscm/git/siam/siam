/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.util.test;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtilTest {

	@Test
	public void testToDOMDocumentString() throws UnsupportedEncodingException {

		String test = "<a attr=\"value\"><b>test</b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		validateDocument(document);

	}

	@Test
	public void testToDOMDocumentInputStream() throws UnsupportedEncodingException {

		String test = "<a attr=\"value\"><b>test</b></a>";

		Document document = XmlUtil.toDOMDocument(new ByteArrayInputStream(test.getBytes("UTF8")));

		validateDocument(document);

	}

	private void validateDocument(Document document) {
		Node root = document.getFirstChild();

		Assert.assertEquals("a", root.getNodeName());

		NamedNodeMap attributes = root.getAttributes();

		Assert.assertEquals(1, attributes.getLength());

		Node attribute = attributes.getNamedItem("attr");

		Assert.assertNotNull(attribute);
		Assert.assertEquals("value", attribute.getNodeValue());

		NodeList childList = root.getChildNodes();

		Assert.assertEquals(1, childList.getLength());

		Node node = childList.item(0);

		Assert.assertEquals("b", node.getNodeName());
	}

	@Test
	public void testGetNodeAttributeValue() throws UnsupportedEncodingException {

		String test = "<a attr=\"value\"></a>";

		Document document = XmlUtil.toDOMDocument(test);

		String attributeValue = XmlUtil.getNodeAttributeValue(document.getFirstChild(), "attr");

		Assert.assertEquals("value", attributeValue);

	}

	@Test
	public void testGetNodeAttributeValueNoAttribute() throws UnsupportedEncodingException {

		String test = "<a attr=\"value\"></a>";

		Document document = XmlUtil.toDOMDocument(test);

		String attributeValue = XmlUtil.getNodeAttributeValue(document.getFirstChild(), "az");

		Assert.assertNull(attributeValue);

	}

	@Test
	public void testGetChildNodes() throws UnsupportedEncodingException {

		String test = "<a><b></b><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		Set<Node> childSet = XmlUtil.getChildNodes(document.getFirstChild(), "b");

		Assert.assertEquals(2, childSet.size());

		Iterator<Node> iterator = childSet.iterator();
		Node node1 = iterator.next();
		Node node2 = iterator.next();

		Assert.assertEquals("b", node1.getNodeName());
		Assert.assertEquals("b", node2.getNodeName());

	}

	@Test
	public void testGetChildNodesInvalidChildName() throws UnsupportedEncodingException {

		String test = "<a><b></b><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		Set<Node> childSet = XmlUtil.getChildNodes(document.getFirstChild(), "bw");

		Assert.assertEquals(0, childSet.size());
	}

	@Test
	public void testGetChildNode() throws UnsupportedEncodingException {

		String test = "<a><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		Node child = XmlUtil.getChildNode(document.getFirstChild(), "b");

		Assert.assertEquals("b", child.getNodeName());
		Assert.assertEquals(0, child.getChildNodes().getLength());

	}

	@Test
	public void testGetChildNodeInvalidChildName() throws UnsupportedEncodingException {

		String test = "<a><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		try {
			XmlUtil.getChildNode(document.getFirstChild(), "c");

			Assert.fail("The method must throw IllegalArgumentException if the the name of the child is invalid");

		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testGetChildNodeNullChildName() throws UnsupportedEncodingException {

		String test = "<a><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		try {
			XmlUtil.getChildNode(document.getFirstChild(), null);

			Assert.fail("The method must throw NullPointerException if the the name of the child is invalid");

		} catch (NullPointerException e) {

		}

	}

	@Test
	public void testGetChildNodeEmptyChildName() throws UnsupportedEncodingException {

		String test = "<a><b></b></a>";

		Document document = XmlUtil.toDOMDocument(test);

		try {
			XmlUtil.getChildNode(document.getFirstChild(), "");

			Assert.fail("The method must throw IllegalArgumentException if the the name of the child is empty");

		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testGetAttributeValuePair() throws UnsupportedEncodingException {
		
		String test = "<AttributeValuePair><Attribute name=\"name\"/><Value>value1</Value><Value>value2</Value></AttributeValuePair>";
		Document document = XmlUtil.toDOMDocument(test);		
		
		AttributeValuePair pair = XmlUtil.getAttributeValuePair(document.getFirstChild());
		Assert.assertEquals("name", pair.getName());
		Assert.assertEquals(2, pair.getValueList().size());
		Assert.assertTrue(pair.getValueList().contains("value1"));
		Assert.assertTrue(pair.getValueList().contains("value2"));
	}

	@Test
	public void testGetAttributeValue() throws UnsupportedEncodingException {

		String test = "<Attribute name=\"name\"><Value>value</Value></Attribute>";

		Document document = XmlUtil.toDOMDocument(test);

		AttributeValuePair pair = XmlUtil.getAttributeValue(document.getFirstChild());

		Assert.assertNotNull(pair);

		Assert.assertEquals("name", pair.getName());
		Assert.assertEquals(1, pair.getValueList().size());
		Assert.assertEquals("value", pair.getValueList().get(0));

	}

	@Test
	public void testGetAttributeValueNoValue() throws UnsupportedEncodingException {

		String test = "<Attribute name=\"name\"><Value/></Attribute>";

		Document document = XmlUtil.toDOMDocument(test);

		AttributeValuePair pair = XmlUtil.getAttributeValue(document.getFirstChild());

		Assert.assertNotNull(pair);

		Assert.assertEquals("name", pair.getName());
		Assert.assertEquals(0, pair.getValueList().size());

	}

	@Test
	public void testGetAttributeValueMultipleValue() throws UnsupportedEncodingException {

		String test = "<Attribute name=\"name\"><Value>value1</Value><Value>value2</Value></Attribute>";

		Document document = XmlUtil.toDOMDocument(test);

		AttributeValuePair pair = XmlUtil.getAttributeValue(document.getFirstChild());

		Assert.assertNotNull(pair);

		Assert.assertEquals("name", pair.getName());
		Assert.assertEquals(2, pair.getValueList().size());
		Assert.assertTrue(pair.getValueList().contains("value1"));
		Assert.assertTrue(pair.getValueList().contains("value2"));

	}

	@Test
	public void testGetValueFromAttributeValue() throws UnsupportedEncodingException {

		String test = "<Attribute name=\"name\"><Value>value</Value></Attribute>";

		Document document = XmlUtil.toDOMDocument(test);

		String value = XmlUtil.getValueFromAttributeValue(document.getFirstChild());

		Assert.assertNotNull(value);

		Assert.assertEquals("value", value);

	}
}
