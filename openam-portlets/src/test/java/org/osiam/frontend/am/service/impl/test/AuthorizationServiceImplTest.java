/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.am.domain.authorization.AuthenticatedUsersSubject;
import org.osiam.frontend.am.domain.authorization.AuthenticationToRealmCondition;
import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.IdentityRepositoryResponseProvider;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResponseProvider;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.domain.authorization.Subject;
import org.osiam.frontend.am.domain.authorization.URLPolicyAgentRule;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.identity.sm.SMSException;

public class AuthorizationServiceImplTest extends BaseOSIAMTest {

	private final static String POLICY_NAME1 = "policy1";
	private final static String POLICY_NAME2 = "policy2";
	private final static String POLICY_TYPE1 = AuthorizationConstants.POLICY_TYPE_CDSSO;
	private final static String POLICY_TYPE2 = AuthorizationConstants.POLICY_TYPE_CDSSO;

	private final static String ENTITY_NAME1 = "entity1";
	private final static String ENTITY_NAME2 = "entity2";

	@Autowired
	private AuthorizationService service;

	@Before
	public void setUp() {
		List<Policy> policies = service.getPoliciesList(getTestSSOToken(), TEST_REALM);
		for (int i = 0; i < policies.size(); i++) {
			service.removePolicy(getTestSSOToken(), TEST_REALM, policies.get(i).getName());
		}
	}

	@Test
	public void testGetPoliciesListNullToken() {
		try {
			service.getPoliciesList(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetPoliciesListNullRealm() {
		try {
			service.getPoliciesList(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetPoliciesListWrongRealm() {
		try {
			service.getPoliciesList(getTestSSOToken(), WRONG_REALM);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetPoliciesCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Policy policy2 = new Policy(POLICY_NAME2, POLICY_TYPE2);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy2);
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}
		
		List<Policy> policies = service.getPoliciesList(getTestSSOToken(), TEST_REALM);
		Assert.assertNotNull("getPoliciesList must not return null values.", policies);
		Assert.assertEquals(
				String.format("getPoliciesList failed. Must be exactly 2 policies, instead are % d.", policies.size()),
				2, policies.size());
		Assert.assertTrue("One of the policies was not persisted", policies.contains(policy1));
		Assert.assertTrue("One of the policies was not persisted", policies.contains(policy2));

	}

	@Test
	public void testGetRulesListNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRulesListForPolicy(null, TEST_REALM, POLICY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRulesListNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRulesListForPolicy(getTestSSOToken(), null, POLICY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRulesListWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRulesListForPolicy(getTestSSOToken(), WRONG_REALM, POLICY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRulesListCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Rule rule1 = new URLPolicyAgentRule(ENTITY_NAME1, "true", "true", "res");
		Rule rule2 = new URLPolicyAgentRule(ENTITY_NAME2, "true", "true", "res");
		List<Rule> rules = new ArrayList<Rule>();
		rules.add(rule1);
		rules.add(rule2);
		policy1.setRules(rules);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}
		//
		// List<BaseEntity> returnRules =
		// service.getRulesListForPolicy(getTestSSOToken(), TEST_REALM,
		// POLICY_NAME1);
		// Assert.assertNotNull("getPoliciesList must not return null values.",
		// returnRules);
		// Assert.assertEquals(String.format("getRulesListForPolicy failed. Must be exactly 2 rules, instead are % d.",
		// returnRules.size()), 2, returnRules.size());
		// Assert.assertTrue("Rule not persisted correctly.",
		// containsBaseEntityByName(returnRules, ENTITY_NAME1));
		// Assert.assertTrue("Rule not persisted correctly.",
		// containsBaseEntityByName(returnRules, ENTITY_NAME2));
	}

	@Test
	public void testGetSubjectsListNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubjectListForPolicy(null, TEST_REALM, POLICY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectListNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubjectListForPolicy(getTestSSOToken(), null, POLICY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectListWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubjectListForPolicy(getTestSSOToken(), WRONG_REALM, POLICY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectListCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Subject entity1 = new AuthenticatedUsersSubject(ENTITY_NAME1);
		Subject entity2 = new AuthenticatedUsersSubject(ENTITY_NAME2);
		List<Subject> entities = new ArrayList<Subject>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setSubjects(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		List<BaseEntity> returnEntities = service.getSubjectListForPolicy(getTestSSOToken(), TEST_REALM, POLICY_NAME1);
		Assert.assertNotNull("getSubjectListForPolicy must not return null values.", returnEntities);
		Assert.assertEquals(String.format(
				"getSubjectListForPolicy failed. Must be exactly 2 subjects, instead are % d.", returnEntities.size()),
				2, returnEntities.size());
		Assert.assertTrue("Subject not persisted correctly.", containsBaseEntityByName(returnEntities, ENTITY_NAME1));
		Assert.assertTrue("Subject not persisted correctly.", containsBaseEntityByName(returnEntities, ENTITY_NAME2));
	}

	@Test
	public void testGetConditionsListNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getConditionListForPolicy(null, TEST_REALM, POLICY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionListNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getConditionListForPolicy(getTestSSOToken(), null, POLICY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionListWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getConditionListForPolicy(getTestSSOToken(), WRONG_REALM, POLICY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionListCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Condition entity1 = new AuthenticationToRealmCondition(ENTITY_NAME1, TEST_REALM);
		Condition entity2 = new AuthenticationToRealmCondition(ENTITY_NAME2, TEST_REALM);
		List<Condition> entities = new ArrayList<Condition>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setConditions(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		List<BaseEntity> returnEntities = service
				.getConditionListForPolicy(getTestSSOToken(), TEST_REALM, POLICY_NAME1);
		Assert.assertNotNull("getConditionListForPolicy must not return null values.", returnEntities);
		Assert.assertEquals(
				String.format("getConditionListForPolicy failed. Must be exactly 2 subjects, instead are % d.",
						returnEntities.size()), 2, returnEntities.size());
		Assert.assertTrue("Condition not persisted correctly.", containsBaseEntityByName(returnEntities, ENTITY_NAME1));
		Assert.assertTrue("Condition not persisted correctly.", containsBaseEntityByName(returnEntities, ENTITY_NAME2));
	}

	@Test
	public void testGetResponseProvidersListNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProviderListForPolicy(null, TEST_REALM, POLICY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderListNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProviderListForPolicy(getTestSSOToken(), null, POLICY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderListWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProviderListForPolicy(getTestSSOToken(), WRONG_REALM, POLICY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderListCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		ResponseProvider entity1 = new IdentityRepositoryResponseProvider(ENTITY_NAME1);
		ResponseProvider entity2 = new IdentityRepositoryResponseProvider(ENTITY_NAME2);
		List<ResponseProvider> entities = new ArrayList<ResponseProvider>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setResponseProviders(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		List<BaseEntity> returnEntities = service.getResponseProviderListForPolicy(getTestSSOToken(), TEST_REALM,
				POLICY_NAME1);
		Assert.assertNotNull("getResponseProviderListForPolicy must not return null values.", returnEntities);
		Assert.assertEquals(String.format(
				"getResponseProviderListForPolicy failed. Must be exactly 2 subjects, instead are % d.",
				returnEntities.size()), 2, returnEntities.size());
		Assert.assertTrue("ResponseProvider not persisted correctly.",
				containsBaseEntityByName(returnEntities, ENTITY_NAME1));
		Assert.assertTrue("ResponseProvider not persisted correctly.",
				containsBaseEntityByName(returnEntities, ENTITY_NAME2));
	}

	@Test
	public void testGetConditionNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getCondition(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getCondition(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getCondition(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getCondition(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionWrongPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getCondition(getTestSSOToken(), TEST_REALM, WRONG_REALM, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetConditionCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Condition entity1 = new AuthenticationToRealmCondition(ENTITY_NAME1, TEST_REALM);
		Condition entity2 = new AuthenticationToRealmCondition(ENTITY_NAME2, TEST_REALM);
		List<Condition> entities = new ArrayList<Condition>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setConditions(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		Condition retCondition = service.getCondition(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNotNull("getCondition must not return null values.", retCondition);
		Assert.assertTrue("getCondition not returned correctly.", retCondition.getName().equals(ENTITY_NAME1));
	}

	@Test
	public void testGetSubjectNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectWrongPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(getTestSSOToken(), TEST_REALM, WRONG_REALM, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectNullRuleName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetSubjectCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Subject entity1 = new AuthenticatedUsersSubject(ENTITY_NAME1);
		Subject entity2 = new AuthenticatedUsersSubject(ENTITY_NAME2);
		List<Subject> entities = new ArrayList<Subject>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setSubjects(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		Subject retEntity = service.getSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNotNull("getSubject must not return null values.", retEntity);
		Assert.assertTrue("getCondition not returned correctly.", retEntity.getName().equals(ENTITY_NAME1));
	}

	@Test
	public void testGetRuleNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleWrongPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(getTestSSOToken(), TEST_REALM, WRONG_REALM, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleNullRuleName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getRule(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetRuleCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Rule entity1 = new URLPolicyAgentRule(ENTITY_NAME1, "true", "true", "res");
		Rule entity2 = new URLPolicyAgentRule(ENTITY_NAME2, "true", "true", "res");
		List<Rule> entities = new ArrayList<Rule>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setRules(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		Rule retEntity = service.getRule(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNotNull("getCondition must not return null values.", retEntity);
		Assert.assertTrue("getCondition not returned correctly.", retEntity.getName().equals(ENTITY_NAME1));
	}

	@Test
	public void testGetRuleResponseProviderNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderWrongPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(getTestSSOToken(), TEST_REALM, WRONG_REALM, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderNullRuleName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.getResponseProvider(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetResponseProviderCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		ResponseProvider entity1 = new IdentityRepositoryResponseProvider(ENTITY_NAME1);
		ResponseProvider entity2 = new IdentityRepositoryResponseProvider(ENTITY_NAME2);
		List<ResponseProvider> entities = new ArrayList<ResponseProvider>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setResponseProviders(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		ResponseProvider retEntity = service.getResponseProvider(getTestSSOToken(), TEST_REALM, POLICY_NAME1,
				ENTITY_NAME1);
		Assert.assertNotNull("getResponseProvider must not return null values.", retEntity);
		Assert.assertTrue("getResponseProvider not returned correctly.", retEntity.getName().equals(ENTITY_NAME1));
	}

	@Test
	public void testRemoveRuleNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeRule(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveRuleNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeRule(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveRuleWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeRule(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveRuleNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeRule(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveRuleNullRuleName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeRule(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveRuleCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Rule entity1 = new URLPolicyAgentRule(ENTITY_NAME1, "true", "true", "res");
		Rule entity2 = new URLPolicyAgentRule(ENTITY_NAME2, "true", "true", "res");
		List<Rule> entities = new ArrayList<Rule>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setRules(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		service.removeRule(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Rule retEntity = service.getRule(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNull("removeRule did not delete.", retEntity);

	}

	@Test
	public void testRemoveSubjectNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveSubjectNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveSubjectWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveSubjectNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveSubjectNullRuleName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveSubjectCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Subject entity1 = new AuthenticatedUsersSubject(ENTITY_NAME1);
		Subject entity2 = new AuthenticatedUsersSubject(ENTITY_NAME2);
		List<Subject> entities = new ArrayList<Subject>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setSubjects(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);

		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		service.removeSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Subject retEntity = service.getSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNull("removeSubject did not delete.", retEntity);

	}

	@Test
	public void testRemoveConditionNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeCondition(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveConditionNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeCondition(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveConditionWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeCondition(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveConditionNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeCondition(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveConditionNullConditionName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeSubject(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveConditionCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Condition entity1 = new AuthenticationToRealmCondition(ENTITY_NAME1, TEST_REALM);
		Condition entity2 = new AuthenticationToRealmCondition(ENTITY_NAME2, TEST_REALM);
		List<Condition> entities = new ArrayList<Condition>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setConditions(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		service.removeCondition(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Condition retEntity = service.getCondition(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		Assert.assertNull("removeSubject did not delete.", retEntity);

	}

	@Test
	public void testRemoveResponseProviderNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeResponseProvider(null, TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveResponseProviderNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeResponseProvider(getTestSSOToken(), null, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveResponseProviderWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeResponseProvider(getTestSSOToken(), WRONG_REALM, POLICY_NAME1, ENTITY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveResponseProviderNullPolicyName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeResponseProvider(getTestSSOToken(), TEST_REALM, null, ENTITY_NAME1);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveResponseProviderNullResponseProviderName() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.removeResponseProvider(getTestSSOToken(), TEST_REALM, POLICY_NAME1, null);
			Assert.fail(String.format("If the policy name is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveResponseProviderCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		ResponseProvider entity1 = new IdentityRepositoryResponseProvider(ENTITY_NAME1);
		ResponseProvider entity2 = new IdentityRepositoryResponseProvider(ENTITY_NAME2);
		List<ResponseProvider> entities = new ArrayList<ResponseProvider>();
		entities.add(entity1);
		entities.add(entity2);
		policy1.setResponseProviders(entities);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		service.removeResponseProvider(getTestSSOToken(), TEST_REALM, POLICY_NAME1, ENTITY_NAME1);
		ResponseProvider retEntity = service.getResponseProvider(getTestSSOToken(), TEST_REALM, POLICY_NAME1,
				ENTITY_NAME1);
		Assert.assertNull("removeSubject did not delete.", retEntity);

	}

	@Test
	public void testAddPolicyListNullToken() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(null, TEST_REALM, policy1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddPolicyListNullRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), null, policy1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddPolicyListNullPolicy() {
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddPolicyListWrongRealm() {
		try {
			Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
			service.addPolicy(getTestSSOToken(), WRONG_REALM, policy1);

			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddPoliciesCorrect() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		Policy policy2 = new Policy(POLICY_NAME2, POLICY_TYPE2);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy2);
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}
		
		List<Policy> policies = service.getPoliciesList(getTestSSOToken(), TEST_REALM);
		Assert.assertNotNull("addPolicy must not return null values.", policies);
		Assert.assertEquals(
				String.format("addPolicy failed. Must be exactly 2 policies, instead are % d.", policies.size()), 2,
				policies.size());
		Assert.assertTrue("One of the policies was not persisted", policies.contains(policy1));
		Assert.assertTrue("One of the policies was not persisted", policies.contains(policy2));

	}

	@Test
	public void testPolicyNameAlreadyExistNullToken() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		} catch (SMSException e1) {
			System.out.println("Error saving policy");
		}
		try {
			service.policyNameAlreadyExist(null, TEST_REALM, POLICY_NAME1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testPolicyNameAlreadyExistNullRealm() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e1) {
			System.out.println("Error saving policy");
		}
		try {
			service.policyNameAlreadyExist(getTestSSOToken(), null, POLICY_NAME1);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testPolicyNameAlreadyExistWrongRealm() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e1) {
			System.out.println("Error saving policy");
		}
		try {
			service.policyNameAlreadyExist(getTestSSOToken(), WRONG_REALM, POLICY_NAME1);
			Assert.fail(String.format("If the realm is wrong, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testPolicyNameAlreadyExistExists() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
	
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		boolean exists = service.policyNameAlreadyExist(getTestSSOToken(), TEST_REALM, POLICY_NAME1);
		Assert.assertTrue("policy must be there", exists);

	}

	@Test
	public void testPolicyNameAlreadyExistNotExists() {
		Policy policy1 = new Policy(POLICY_NAME1, POLICY_TYPE1);
		try {
			service.addPolicy(getTestSSOToken(), TEST_REALM, policy1);
		
		} catch (SMSException e) {
			System.out.println("Error saving policy");
		}

		boolean exists = service.policyNameAlreadyExist(getTestSSOToken(), TEST_REALM, POLICY_NAME1 + "blah");
		Assert.assertFalse("policy must not be there", exists);

	}

	private boolean containsBaseEntityByName(List<BaseEntity> entities, String name) {
		for (int i = 0; i < entities.size(); i++) {
			BaseEntity baseEntity = entities.get(i);
			if (baseEntity.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

}