/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.osiam.frontend.configuration.domain.datastores.DataStore;
import org.osiam.frontend.configuration.exceptions.DataStoresException;
import org.osiam.frontend.configuration.service.DataStoresConstants;
import org.osiam.frontend.configuration.service.DataStoresService;
import org.springframework.beans.factory.annotation.Autowired;

public class DataStoresServiceImplTest extends BaseOSIAMTest {

	private static final String A_DATA_STORE = "aDataStore";

	@Autowired
	private DataStoresService service;

	@Before
	public void setUp() {
		// delete all data stores in the current realm
		Set<String> dataStoreNameSet = service.getDataStoreNameSet(getTestSSOToken(), TEST_REALM);
		service.deleteDataStore(getTestSSOToken(), TEST_REALM, dataStoreNameSet);
	}

	@Test
	public void getDataStoreListCorrect() {

		final String[] DATA_STORE_NAMES = new String[] { "dataStore1", "dataStore2" };

		final String[] DATA_STORE_TYPES = new String[] { DataStoresConstants.STORE_TYPE_DATABASE,
				DataStoresConstants.STORE_TYPE_LDAP_FOR_AD };

		final DataStore DATA_STORE1 = new DataStore();
		final DataStore DATA_STORE2 = new DataStore();
		DATA_STORE1.setName(DATA_STORE_NAMES[0]);
		DATA_STORE2.setName(DATA_STORE_NAMES[1]);
		DATA_STORE1.setType(DATA_STORE_TYPES[0]);
		DATA_STORE2.setType(DATA_STORE_TYPES[1]);

		final DataStore[] EXPECTED_LIST = new DataStore[] { DATA_STORE1, DATA_STORE2 };

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();

		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE_NAMES[0], DATA_STORE_TYPES[0], attributeMap);
		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE_NAMES[1], DATA_STORE_TYPES[1], attributeMap);

		List<DataStore> dataStoreList = service.getDataStoresList(getTestSSOToken(), TEST_REALM);

		Assert.assertTrue("data store list is not properly returned by method getDataStoresList", dataStoreList != null);
		Assert.assertTrue("data store list is not properly returned by method getDataStoresList",
				dataStoreList.size() == EXPECTED_LIST.length);

		for (int i = 0; i < EXPECTED_LIST.length; i++) {
			DataStore actualDataStore = dataStoreList.get(i);
			Assert.assertTrue("data store list is not properly returned by method getDataStoresList",
					EXPECTED_LIST[i].equals(actualDataStore));
		}

	}

	@Test
	public void getDataStoreListNullToken() {
		try {
			service.getDataStoresList(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void getDataStoreListNullRealm() {
		try {
			service.getDataStoresList(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void getDataStoreNameListCorrect() {

		final String DATA_STORE1 = "dataStore1";
		final String DATA_STORE2 = "dataStore2";

		final Set<String> EXPECTED_SET = new HashSet<String>(2);
		EXPECTED_SET.add(DATA_STORE1);
		EXPECTED_SET.add(DATA_STORE2);

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();

		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, "Database", attributeMap);
		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE2, "Database", attributeMap);

		Set<String> dataStoreSet = service.getDataStoreNameSet(getTestSSOToken(), TEST_REALM);
		Assert.assertTrue("data store name set is not properly returned by method getDataStoreNameList",
				EXPECTED_SET.equals(dataStoreSet));

	}

	@Test
	public void getDataStoreNameListNullToken() {
		try {
			service.getDataStoreNameSet(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void getDataStoreNameListNullRealm() {
		try {
			service.getDataStoreNameSet(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainNameSetWrongRealm() {
		try {
			service.getDataStoreNameSet(getTestSSOToken(), WRONG_REALM);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (DataStoresException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDefaultAttributeValues() {
		Map<String, Set<String>> attributes = service.getDefaultAttributeValues(getTestSSOToken(),
				DataStoresConstants.STORE_TYPE_LDAP_FOR_AD);
		Assert.assertNotNull("getDefaultAttributeValues must return a not null value", attributes);
	}
	
	@Test
	public void testGetDefaultAttributeValuesNullToken() {
		try {
			service.getDefaultAttributeValues(null, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDefaultAttributeValuesNullType() {
		try {
			service.getDefaultAttributeValues(getTestSSOToken(), null);
			Assert.fail("If type param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreCorrect() {

		final String DATA_STORE1 = "dataStore1";
		final String DATA_STORE2 = "dataStore2";

		final Set<String> EXPECTED_SET = new HashSet<String>(2);
		EXPECTED_SET.add(DATA_STORE1);
		EXPECTED_SET.add(DATA_STORE2);

		final Set<String> TESTED_VALUE1_SET = asSet("1234");
		final Set<String> TESTED_VALUE2_SET = asSet("LDAP_BIND_DN");

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();

		attributeMap.put(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS, TESTED_VALUE1_SET);
		attributeMap.put(DataStoresConstants.DELAY_BETWEEN_RETRIES, asSet("567"));
		attributeMap.put(DataStoresConstants.LDAP_BIND_DN, TESTED_VALUE2_SET);
		attributeMap.put(DataStoresConstants.LDAP_ORGANIZATION_DN, asSet("LDAP_ORGANIZATION_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_BASE_DN, asSet("PERSISTENT_SEARCH_BASE_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_FILTER, asSet("PERSISTENT_SEARCH_FILTER"));
		attributeMap.put(DataStoresConstants.ATTRIBUTE_NAME_OF_USER_STATUS, asSet("ATTRIBUTE_NAME_OF_USER_STATUS"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MAXIMUM_SIZE, asSet("999"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MINIMUM_SIZE, asSet("121"));
		attributeMap.put(DataStoresConstants.LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME,
				asSet("LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME"));
		attributeMap.put(DataStoresConstants.IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED, asSet("User"));

		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);
		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE2, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);

		Set<String> dataStoreNameSet = service.getDataStoreNameSet(getTestSSOToken(), TEST_REALM);
		Assert.assertTrue("data store name set is not properly returned by method getDataStoreNameList",
				EXPECTED_SET.equals(dataStoreNameSet));

		Map<String, Set<String>> dataStoreAttributes = service.getDataStoreAttributes(getTestSSOToken(), TEST_REALM,
				DATA_STORE1);

		Set<String> actualValue1 = dataStoreAttributes.get(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.",
				DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS), TESTED_VALUE1_SET.equals(actualValue1));

		Set<String> actualValue2 = dataStoreAttributes.get(DataStoresConstants.LDAP_BIND_DN);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.", DataStoresConstants.LDAP_BIND_DN),
				TESTED_VALUE2_SET.equals(actualValue2));

	}

	@Test
	public void testAddDataStoreNullToken() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(null, TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreNullRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), null, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreWrongRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), WRONG_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (DataStoresException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreNullName() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, null, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreNullType() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, null, attributeMap);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddDataStoreNullAttributes() {
		try {
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					null);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEditDataStoreCorrect() {

		final String DATA_STORE1 = "dataStore1";

		final Set<String> EXPECTED_SET = new HashSet<String>(2);
		EXPECTED_SET.add(DATA_STORE1);

		// values initially saved
		final Set<String> INITIAL_VALUE1_SET = asSet("1234");
		final Set<String> INITIAL_VALUE2_SET = asSet("LDAP_BIND_DN");

		// values altered and checked as assert
		final Set<String> TESTED_VALUE1_SET = asSet("3212");
		final Set<String> TESTED_VALUE2_SET = asSet("NEW_LDAP_BIND_DN");

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();

		attributeMap.put(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS, INITIAL_VALUE1_SET);
		attributeMap.put(DataStoresConstants.DELAY_BETWEEN_RETRIES, asSet("567"));
		attributeMap.put(DataStoresConstants.LDAP_BIND_DN, INITIAL_VALUE2_SET);
		attributeMap.put(DataStoresConstants.LDAP_ORGANIZATION_DN, asSet("LDAP_ORGANIZATION_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_BASE_DN, asSet("PERSISTENT_SEARCH_BASE_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_FILTER, asSet("PERSISTENT_SEARCH_FILTER"));
		attributeMap.put(DataStoresConstants.ATTRIBUTE_NAME_OF_USER_STATUS, asSet("ATTRIBUTE_NAME_OF_USER_STATUS"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MAXIMUM_SIZE, asSet("999"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MINIMUM_SIZE, asSet("121"));
		attributeMap.put(DataStoresConstants.LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME,
				asSet("LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME"));
		attributeMap.put(DataStoresConstants.IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED, asSet("User"));

		// insert a data store with initial values
		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);

		// altering two of the attributes
		attributeMap.put(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS, TESTED_VALUE1_SET);
		attributeMap.put(DataStoresConstants.LDAP_BIND_DN, TESTED_VALUE2_SET);

		// altering the saved data store
		service.editDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, attributeMap);

		Set<String> dataStoreNameSet = service.getDataStoreNameSet(getTestSSOToken(), TEST_REALM);
		Assert.assertTrue("data store name set is not properly returned by method getDataStoreNameList",
				EXPECTED_SET.equals(dataStoreNameSet));

		Map<String, Set<String>> dataStoreAttributes = service.getDataStoreAttributes(getTestSSOToken(), TEST_REALM,
				DATA_STORE1);

		Set<String> actualValue1 = dataStoreAttributes.get(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.",
				DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS), TESTED_VALUE1_SET.equals(actualValue1));

		Set<String> actualValue2 = dataStoreAttributes.get(DataStoresConstants.LDAP_BIND_DN);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.", DataStoresConstants.LDAP_BIND_DN),
				TESTED_VALUE2_SET.equals(actualValue2));

	}

	@Test
	public void testEditDataStoreNullToken() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.editDataStore(null, TEST_REALM, A_DATA_STORE, attributeMap);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEditDataStoreNullRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.editDataStore(getTestSSOToken(), null, A_DATA_STORE, attributeMap);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEditDataStoreWrongRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.editDataStore(getTestSSOToken(), WRONG_REALM, A_DATA_STORE, attributeMap);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (DataStoresException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEditDataStoreNullName() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.editDataStore(getTestSSOToken(), TEST_REALM, null, attributeMap);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEditDataStoreNullAttributes() {
		try {
			service.editDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, null);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteDataStoreNullToken() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			service.deleteDataStore(null, TEST_REALM, asSet(A_DATA_STORE));
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteDataStoreNullRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			service.deleteDataStore(getTestSSOToken(), null, asSet(A_DATA_STORE));
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteDataStoreWrongRealm() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			service.deleteDataStore(getTestSSOToken(), WRONG_REALM, asSet(A_DATA_STORE));
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (DataStoresException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteDataStoreNullNameSet() {
		try {
			Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
			service.addDataStore(getTestSSOToken(), TEST_REALM, A_DATA_STORE, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
					attributeMap);
			service.deleteDataStore(getTestSSOToken(), TEST_REALM, null);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteDataStoreCorrect() {
		final String DATA_STORE1 = "dataSet1";
		final String DATA_STORE2 = "dataSet2";
		final byte NUMBER_OF_DATA_SETS_AFTER_DELETE = 1;

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();
		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);

		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE2, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);

		service.deleteDataStore(getTestSSOToken(), TEST_REALM, asSet(DATA_STORE1));

		Set<String> dataStoreNameSet = service.getDataStoreNameSet(getTestSSOToken(), TEST_REALM);
		int dataStoreSetSize = dataStoreNameSet.size();

		Assert.assertEquals(
				String.format(
						"If the dataStore is not found, the method must raise a warning and delete nothing. However, instead of one dataStore there are only %d data stores now.",
						dataStoreSetSize), NUMBER_OF_DATA_SETS_AFTER_DELETE, dataStoreSetSize);

		Assert.assertTrue("After the delete operation, the data store'" + DATA_STORE2
				+ "' must not be deleted. However, it seems it was deleted.", dataStoreNameSet.contains(DATA_STORE2));

		Assert.assertFalse("After the delete operation, the data store'" + DATA_STORE1
				+ "' must be deleted. However, it seems it was NOT deleted.", dataStoreNameSet.contains(DATA_STORE1));

	}

	@Test
	public void testGetDataStoreAttributesNullToken() {
		try {
			service.getDataStoreAttributes(null, TEST_REALM, A_DATA_STORE);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDataStoreAttributesNullRealm() {
		try {
			service.getDataStoreAttributes(getTestSSOToken(), null, A_DATA_STORE);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDataStoreAttributesWrongRealm() {
		try {
			service.getDataStoreAttributes(getTestSSOToken(), WRONG_REALM, A_DATA_STORE);
			Assert.fail(String.format("If the realm is wrong, method must raise DataStoresException."));
		} catch (DataStoresException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDataStoreAttributesNullName() {
		try {
			service.getDataStoreAttributes(getTestSSOToken(), TEST_REALM, null);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetDataStoreAttributesCorrect() {

		final String DATA_STORE1 = "dataStore1";

		final Set<String> TESTED_VALUE1_SET = asSet("1234");
		final Set<String> TESTED_VALUE2_SET = asSet("LDAP_BIND_DN");

		Map<String, Set<String>> attributeMap = new HashMap<String, Set<String>>();

		attributeMap.put(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS, TESTED_VALUE1_SET);
		attributeMap.put(DataStoresConstants.DELAY_BETWEEN_RETRIES, asSet("567"));
		attributeMap.put(DataStoresConstants.LDAP_BIND_DN, TESTED_VALUE2_SET);
		attributeMap.put(DataStoresConstants.LDAP_ORGANIZATION_DN, asSet("LDAP_ORGANIZATION_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_BASE_DN, asSet("PERSISTENT_SEARCH_BASE_DN"));
		attributeMap.put(DataStoresConstants.PERSISTENT_SEARCH_FILTER, asSet("PERSISTENT_SEARCH_FILTER"));
		attributeMap.put(DataStoresConstants.ATTRIBUTE_NAME_OF_USER_STATUS, asSet("ATTRIBUTE_NAME_OF_USER_STATUS"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MAXIMUM_SIZE, asSet("999"));
		attributeMap.put(DataStoresConstants.LDAP_CONNECTION_POOL_MINIMUM_SIZE, asSet("121"));
		attributeMap.put(DataStoresConstants.LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME,
				asSet("LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME"));
		attributeMap.put(DataStoresConstants.IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED, asSet("User"));

		service.addDataStore(getTestSSOToken(), TEST_REALM, DATA_STORE1, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD,
				attributeMap);

		Map<String, Set<String>> dataStoreAttributes = service.getDataStoreAttributes(getTestSSOToken(), TEST_REALM,
				DATA_STORE1);

		Set<String> actualValue1 = dataStoreAttributes.get(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.",
				DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS), TESTED_VALUE1_SET.equals(actualValue1));

		Set<String> actualValue2 = dataStoreAttributes.get(DataStoresConstants.LDAP_BIND_DN);
		Assert.assertTrue(String.format("Attribute %s was not properly persisted.", DataStoresConstants.LDAP_BIND_DN),
				TESTED_VALUE2_SET.equals(actualValue2));

	}

	private Set<String> asSet(String value) {
		Set<String> set1 = new HashSet<String>();
		set1.add(value);

		return set1;
	}

}
