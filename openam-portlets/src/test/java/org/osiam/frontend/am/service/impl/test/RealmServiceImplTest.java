/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import org.junit.Assert;
import org.junit.Test;
import org.osiam.frontend.common.exceptions.OSIAMException;
import org.osiam.frontend.common.service.RealmService;
import org.osiam.frontend.common.service.impl.OpenSSOServiceImpl;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.osiam.frontend.debug.XMLDebug;
import org.springframework.beans.factory.annotation.Autowired;

import com.iplanet.sso.SSOToken;

public class RealmServiceImplTest extends BaseOSIAMTest {

	@Autowired
	private RealmService service;

	@Test
	public void testLoginFailed() {

		OpenSSOServiceImpl ossos = new OpenSSOServiceImpl();
		ossos.setUsername("wrongusername");
		ossos.setPassword("wrongpassword");
		try {
			SSOToken token = ossos.authenticate("/");
			System.out.println(token);
			Assert.fail("Exception should be thrown.");
		} catch (OSIAMException e) {

		}
	}

	@Test
	public void testRealmsList() {
		System.out.println(service.getRealmSet(getTestSSOToken()));
	}

	@Test
	public void testRealmsList2() {
		System.out.println(service.getRealmSet(getTestSSOToken(), "test"));
	}
	
	@Test
	public void testRealmSiblingsSet() {
		XMLDebug.printXML(service.getRealmSiblingsSet(getTestSSOToken(), TEST_REALM,"*"));
	}
	
	@Test
	public void testRealmChildrenSet() {
		XMLDebug.printXML(service.getRealmChildrenSet(getTestSSOToken(), TEST_REALM,"*"));
	}
}
