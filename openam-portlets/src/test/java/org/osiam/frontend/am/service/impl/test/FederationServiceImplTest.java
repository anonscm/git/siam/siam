/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.EntityProviderDescriptor;
import org.osiam.frontend.configuration.domain.federation.SAMLV2XACMLPDPAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.springframework.beans.factory.annotation.Autowired;

public class FederationServiceImplTest extends BaseOSIAMTest {

	private static final String NAME1 = "circle_of_trust_name_that_is_only_used_for_unit_testing";

	@Autowired
	private FederationService service;

	@Before
	public void setUp() {
	}

	@Test
	public void testAddCircleOfTrustCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		CircleOfTrust savedCircleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertNotNull("Entity not persisted", savedCircleOfTrust);
		Assert.assertTrue("Name not correctly persisted", NAME1.equals(savedCircleOfTrust.getName()));
	}

	@Test
	public void testAddCircleOfTrustNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(null, TEST_REALM, circleOfTrust);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddCircleOfTrustNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), null, circleOfTrust);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddCircleOfTrustWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), WRONG_REALM, circleOfTrust);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddCircleOfTrustNullCircle() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			service.addCircleOfTrust(getTestSSOToken(), WRONG_REALM, null);

			Assert.fail("If circleOfTrust param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If circleOfTrust param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		CircleOfTrust savedCircleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertNotNull("Entity not correctly returned", savedCircleOfTrust);
		Assert.assertTrue("Name not correctly returned", NAME1.equals(savedCircleOfTrust.getName()));
	}

	@Test
	public void testGetCircleOfTrustNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrust(null, TEST_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrust(getTestSSOToken(), null, NAME1);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrust(getTestSSOToken(), WRONG_REALM, NAME1);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustNullCircle() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrust(getTestSSOToken(), WRONG_REALM, null);

			Assert.fail("If circleOfTrust param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If circleOfTrust param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustListCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		List<CircleOfTrust> savedCircleOfTrust = service.getCircleOfTrustList(getTestSSOToken(), TEST_REALM);
		Assert.assertNotNull("List cannot be null.", savedCircleOfTrust);

	}

	@Test
	public void testGetCircleOfTrustListNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrustList(null, TEST_REALM);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustListNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrustList(getTestSSOToken(), null);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetCircleOfTrustListWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.getCircleOfTrustList(getTestSSOToken(), WRONG_REALM);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveCircleOfTrustCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		service.deleteCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);

		CircleOfTrust savedCircleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertNull("Entity not deleted.", savedCircleOfTrust);
	}

	@Test
	public void testRemoveEntityProviderNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(null, TEST_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveEntityProviderNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(getTestSSOToken(), null, NAME1);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveEntityProviderWrongRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(getTestSSOToken(), WRONG_REALM, NAME1);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveEntityProviderNullEntityProvider() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, null);

			Assert.fail("If circleOfTrust param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If circleOfTrust param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveEntityProviderCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, NAME1);

		CircleOfTrust savedCircleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertNull("Entity not deleted.", savedCircleOfTrust);
	}

	@Test
	public void testRemoveCircleOfTrustNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.deleteCircleOfTrust(null, TEST_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveCircleOfTrustNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.deleteCircleOfTrust(getTestSSOToken(), null, NAME1);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveCircleOfTrustWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.deleteCircleOfTrust(getTestSSOToken(), WRONG_REALM, NAME1);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testRemoveCircleOfTrustNullCircle() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.deleteCircleOfTrust(getTestSSOToken(), WRONG_REALM, null);

			Assert.fail("If circleOfTrust param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If circleOfTrust param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProviderListCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		List<EntityProvider> savedEntityProviders = service.getEntityProviderList(getTestSSOToken(), TEST_REALM);
		Assert.assertNotNull("List cannot be null.", savedEntityProviders);

	}

	@Test
	public void testGetEntityProviderListNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.getEntityProviderList(null, TEST_REALM);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProviderListNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.getEntityProviderList(getTestSSOToken(), null);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProviderListWrongRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.getEntityProviderList(getTestSSOToken(), WRONG_REALM);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProvidersForGivenTypeAndGivenNameCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		EntityProvider savedEntityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
				TEST_REALM, FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

		service.saveAttributesEntityProvider(getTestSSOToken(), TEST_REALM, savedEntityProvider);

		Assert.assertNotNull("entity provider not persisted.", savedEntityProvider);

	}

	@Test
	public void testGetEntityProvidersForGivenTypeAndGivenNameNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			EntityProvider savedEntityProvider = service.getEntityProvidersForGivenTypeAndGivenName(null, TEST_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProvidersForGivenTypeAndGivenNameNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			EntityProvider savedEntityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
					null, FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetEntityProvidersForGivenTypeAndGivenNameWrongRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			EntityProvider savedEntityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
					null, FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateCircleOfTrustCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);
		circleOfTrust.setDescription("description");

		service.updateCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		CircleOfTrust savedCircleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertNotNull("Entity not persisted", savedCircleOfTrust);
		Assert.assertTrue("Name not correctly persisted", NAME1.equals(savedCircleOfTrust.getName()));
	}

	@Test
	public void testUpdateCircleOfTrustNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.updateCircleOfTrust(null, TEST_REALM, circleOfTrust);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateCircleOfTrustNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.updateCircleOfTrust(getTestSSOToken(), null, circleOfTrust);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateCircleOfTrustWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);
			circleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
			service.updateCircleOfTrust(getTestSSOToken(), WRONG_REALM, circleOfTrust);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateCircleOfTrustNullCircle() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);
			circleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
			service.updateCircleOfTrust(getTestSSOToken(), TEST_REALM, null);

			Assert.fail("If circleOfTrust param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If circleOfTrust param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddEntityProviderCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		EntityProvider savedEntityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
				TEST_REALM, FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);
		Assert.assertNotNull("Entity not persisted", savedEntityProvider);
		Assert.assertTrue("Name not correctly persisted", NAME1.equals(savedEntityProvider.getEntityIdentifier()));
	}

	@Test
	public void testAddEntityProviderNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(null, TEST_REALM, entityProvider);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddEntityProviderNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), null, entityProvider);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddEntityProviderWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), WRONG_REALM, entityProvider);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddEntityProviderNullCircle() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			service.addEntityProvider(getTestSSOToken(), WRONG_REALM, null);

			Assert.fail("If entityProvider param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If entityProvider param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteEntityProviderCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, NAME1);

	}

	@Test
	public void testDeleteEntityProviderNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(null, WRONG_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteEntityProviderNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(getTestSSOToken(), null, NAME1);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteEntityProviderWrongRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.deleteEntityProvider(getTestSSOToken(), WRONG_REALM, NAME1);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testImportEntityProviderCorrect() {
		service.deleteEntityProvider(getTestSSOToken(), "/", "123");
		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, "123");
		String metadata = "<EntityDescriptor xmlns=\"urn:oasis:names:tc:SAML:2.0:metadata\" entityID=\"123\"><AuthnAuthorityDescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\"><AuthnQueryService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AuthnQueryServiceSoap/metaAlias/Authentication Authority\"/><AssertionIDRequestService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AIDReqSoap/AuthnAuthRole/metaAlias/Authentication Authority\"/><AssertionIDRequestService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:URI\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AIDReqUri/AuthnAuthRole/metaAlias/Authentication Authority\"/></AuthnAuthorityDescriptor></EntityDescriptor>";
		String config = "<EntityConfig xmlns=\"urn:sun:fm:SAML:2.0:entityconfig\" entityID=\"123\" hosted=\"true\"><XACMLAuthzDecisionQueryConfig metaAlias=\"/XACML Policy Enforcement Point\"><Attribute name=\"signingCertAlias\"><Value/></Attribute><Attribute name=\"encryptionCertAlias\"><Value/></Attribute><Attribute name=\"basicAuthUser\"><Value/></Attribute><Attribute name=\"basicAuthPassword\"><Value/></Attribute><Attribute name=\"basicAuthOn\"><Value>false</Value></Attribute><Attribute name=\"wantXACMLAuthzDecisionResponseSigned\"><Value>false</Value></Attribute><Attribute name=\"wantAssertionEncrypted\"><Value>false</Value></Attribute></XACMLAuthzDecisionQueryConfig><AuthnAuthorityConfig metaAlias=\"/Authentication Authority\"><Attribute name=\"signingCertAlias\"><Value/></Attribute><Attribute name=\"encryptionCertAlias\"><Value/></Attribute><Attribute name=\"assertionIDRequestMapper\"><Value>com.sun.identity.saml2.plugins.DefaultAssertionIDRequestMapper</Value></Attribute></AuthnAuthorityConfig></EntityConfig>";
		service.importEntityProvider(getTestSSOToken(), TEST_REALM, config, metadata);

	}

	@Test
	public void testImportEntityProviderOneNullFile() {
		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, "123");
		String metadata = "<EntityDescriptor xmlns=\"urn:oasis:names:tc:SAML:2.0:metadata\" entityID=\"123\"><AuthnAuthorityDescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\"><AuthnQueryService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AuthnQueryServiceSoap/metaAlias/Authentication Authority\"/><AssertionIDRequestService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AIDReqSoap/AuthnAuthRole/metaAlias/Authentication Authority\"/><AssertionIDRequestService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:URI\" Location=\"http://siam-backend-dev.bonn.tarent.de:8080/opensso/AIDReqUri/AuthnAuthRole/metaAlias/Authentication Authority\"/></AuthnAuthorityDescriptor></EntityDescriptor>";
		String config = "<EntityConfig xmlns=\"urn:sun:fm:SAML:2.0:entityconfig\" entityID=\"123\" hosted=\"true\"><XACMLAuthzDecisionQueryConfig metaAlias=\"/XACML Policy Enforcement Point\"><Attribute name=\"signingCertAlias\"><Value/></Attribute><Attribute name=\"encryptionCertAlias\"><Value/></Attribute><Attribute name=\"basicAuthUser\"><Value/></Attribute><Attribute name=\"basicAuthPassword\"><Value/></Attribute><Attribute name=\"basicAuthOn\"><Value>false</Value></Attribute><Attribute name=\"wantXACMLAuthzDecisionResponseSigned\"><Value>false</Value></Attribute><Attribute name=\"wantAssertionEncrypted\"><Value>false</Value></Attribute></XACMLAuthzDecisionQueryConfig><AuthnAuthorityConfig metaAlias=\"/Authentication Authority\"><Attribute name=\"signingCertAlias\"><Value/></Attribute><Attribute name=\"encryptionCertAlias\"><Value/></Attribute><Attribute name=\"assertionIDRequestMapper\"><Value>com.sun.identity.saml2.plugins.DefaultAssertionIDRequestMapper</Value></Attribute></AuthnAuthorityConfig></EntityConfig>";
		service.importEntityProvider(getTestSSOToken(), TEST_REALM, config, null);
		service.getEntityProviderList(getTestSSOToken(), TEST_REALM);

	}

	@Test
	public void testUpdateEntityProviderDescriptorCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();

		EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
		entityProviderDescriptor.setMetaAlias("met");
		entityProvider.getDescriptorMap().put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT,
				entityProviderDescriptor);

		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), TEST_REALM,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

		SAMLV2XACMLPDPAttributesBean newEntityProviderDescriptor = (SAMLV2XACMLPDPAttributesBean) entityProvider
				.getDescriptorMap().get(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);

		final String EXPECTED = "NEW_LOCATION";
		newEntityProviderDescriptor.setAuthorizationServiceLocation(EXPECTED);

		service.updateEntityProviderDescriptor(getTestSSOToken(), TEST_REALM,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1, newEntityProviderDescriptor);

		entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), TEST_REALM,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

		SAMLV2XACMLPDPAttributesBean afterSaveEntityProviderDescriptor = (SAMLV2XACMLPDPAttributesBean) entityProvider
				.getDescriptorMap().get(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);

		Assert.assertEquals("Field not correctly persisted", EXPECTED,
				afterSaveEntityProviderDescriptor.getAuthorizationServiceLocation());
	}

	@Test
	public void testUpdateEntityProviderDescriptorNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();

			EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();
			entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
			entityProviderDescriptor.setMetaAlias("met");
			entityProvider.getDescriptorMap().put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT,
					entityProviderDescriptor);

			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), TEST_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			SAMLV2XACMLPDPAttributesBean newEntityProviderDescriptor = (SAMLV2XACMLPDPAttributesBean) entityProvider
					.getDescriptorMap().get(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);

			final String EXPECTED = "NEW_LOCATION";
			newEntityProviderDescriptor.setAuthorizationServiceLocation(EXPECTED);

			service.updateEntityProviderDescriptor(null, TEST_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1, newEntityProviderDescriptor);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateEntityProviderNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();

			EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();
			entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
			entityProviderDescriptor.setMetaAlias("met");
			entityProvider.getDescriptorMap().put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT,
					entityProviderDescriptor);

			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), TEST_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			SAMLV2XACMLPDPAttributesBean newEntityProviderDescriptor = (SAMLV2XACMLPDPAttributesBean) entityProvider
					.getDescriptorMap().get(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);

			final String EXPECTED = "NEW_LOCATION";
			newEntityProviderDescriptor.setAuthorizationServiceLocation(EXPECTED);

			service.updateEntityProviderDescriptor(getTestSSOToken(), null,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1, newEntityProviderDescriptor);

			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testUpdateEntityProviderWrongRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();

			EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();
			entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
			entityProviderDescriptor.setMetaAlias("met");
			entityProvider.getDescriptorMap().put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT,
					entityProviderDescriptor);

			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), TEST_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1);

			SAMLV2XACMLPDPAttributesBean newEntityProviderDescriptor = (SAMLV2XACMLPDPAttributesBean) entityProvider
					.getDescriptorMap().get(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);

			final String EXPECTED = "NEW_LOCATION";
			newEntityProviderDescriptor.setAuthorizationServiceLocation(EXPECTED);

			service.updateEntityProviderDescriptor(getTestSSOToken(), WRONG_REALM,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, NAME1, newEntityProviderDescriptor);

			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testCircleOfTrustNameAlreadyExistNullToken() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.circleOfTrustNameAlreadyExist(null, TEST_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testCircleOfTrustNameAlreadyExistNullRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

			service.circleOfTrustNameAlreadyExist(getTestSSOToken(), null, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testCircleOfTrustNameAlreadyExistWrongRealm() {
		try {
			removeCircleOfTrustIfThere(NAME1);
			CircleOfTrust circleOfTrust = createCircleOfTrust();
			service.addCircleOfTrust(getTestSSOToken(), WRONG_REALM, circleOfTrust);

			service.circleOfTrustNameAlreadyExist(getTestSSOToken(), TEST_REALM, NAME1);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testCircleOfTrustNameAlreadyExistCorrect() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		boolean exists = service.circleOfTrustNameAlreadyExist(getTestSSOToken(), TEST_REALM, NAME1);
		Assert.assertTrue("circle of trust must be there", exists);

	}

	@Test
	public void testCircleOfTrustNameAlreadyExistCorrectNotExists() {

		removeCircleOfTrustIfThere(NAME1);
		CircleOfTrust circleOfTrust = createCircleOfTrust();
		service.addCircleOfTrust(getTestSSOToken(), TEST_REALM, circleOfTrust);

		boolean exists = service.circleOfTrustNameAlreadyExist(getTestSSOToken(), TEST_REALM, NAME1 + "_blah");
		Assert.assertFalse("circle of trust must NOT be there", exists);

	}

	@Test
	public void testEntityProviderAlreadyExistNullToken() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.entityProviderNameAlreadyExist(null, TEST_REALM, NAME1,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEntityProviderAlreadyExistNullRealm() {
		try {
			removeEntityProviderIfThere(NAME1);
			EntityProvider entityProvider = createEntityProvider();
			service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

			service.entityProviderNameAlreadyExist(getTestSSOToken(), null, NAME1,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testEntityProviderAlreadyExistCorrect() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		boolean exists = service.entityProviderNameAlreadyExist(getTestSSOToken(), TEST_REALM, NAME1,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		Assert.assertTrue("entity provider must be there", exists);

	}

	@Test
	public void testEntityProviderAlreadyExistCorrectNotExists() {

		removeEntityProviderIfThere(NAME1);
		EntityProvider entityProvider = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), TEST_REALM, entityProvider);

		boolean exists = service.entityProviderNameAlreadyExist(getTestSSOToken(), TEST_REALM, NAME1 + "_blah",
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		Assert.assertFalse("entity provider must NOT be there", exists);

	}

	private void removeCircleOfTrustIfThere(String name) {
		CircleOfTrust circleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, name);
		if (circleOfTrust != null) {
			service.deleteCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		}
	}

	private void removeEntityProviderIfThere(String name) {
		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, name);

	}

	private CircleOfTrust createCircleOfTrust() {
		CircleOfTrust circleOfTrust = new CircleOfTrust();
		circleOfTrust.setName(NAME1);
		circleOfTrust.setDescription("");
		circleOfTrust.setIdffReaderServiceURL("http:www.reader.com");
		circleOfTrust.setIdffWriterServiceURL("");
		circleOfTrust.setSaml2ReaderServiceURL("");
		circleOfTrust.setSaml2WriterServiceURL("http:www.google.com");
		circleOfTrust.setStatus("active");
		circleOfTrust.setTrustedProviders(new ArrayList<String>());
		return circleOfTrust;
	}

	private EntityProvider createEntityProvider() {
		EntityProvider entityProvider = new EntityProvider();
		entityProvider.setEntityIdentifier(NAME1);
		entityProvider.setProtocol(FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		Map<String, EntityProviderDescriptor> descriptorMap = new HashMap<String, EntityProviderDescriptor>();
		EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();		
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER);
		entityProviderDescriptor.setMetaAlias("met");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER, entityProviderDescriptor);
		entityProvider.setDescriptorMap(descriptorMap);

		return entityProvider;
	}

}
