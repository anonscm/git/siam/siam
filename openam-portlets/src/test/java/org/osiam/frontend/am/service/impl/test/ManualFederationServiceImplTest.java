/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.EntityProviderDescriptor;
import org.osiam.frontend.configuration.service.AgentsService;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.springframework.beans.factory.annotation.Autowired;

public class ManualFederationServiceImplTest extends BaseOSIAMTest {

	private static final String NAME1 = "circle_of_trust_name_that_is_only_used_for_unit_testing";

	@Autowired
	private FederationService service;

	@Autowired
	private AgentsService agentsService;

	@Before
	public void setUp() {
	}

	private String readFile(String file) throws IOException {
		StringBuffer contentBuffer = new StringBuffer();

		FileReader reader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(reader);

		String line = "";
		while (line != null) {
			line = bufferedReader.readLine();
			if (line != null) {
				contentBuffer.append(line);
			}
		}

		return contentBuffer.toString();
	}

	// //@Test
	public void test1() throws Exception {

		// removeEntityProviderIfThere(NAME1,
		// FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		// EntityProvider entityProvider = createEntityProvider();
		// service.addEntityProvider(getTestSSOToken(), TEST_REALM,
		// entityProvider);

		// service.deleteEntityProvider(getTestSSOToken(), "/", "qqq");
		// service.getEntityProviderList(getTestSSOToken(), "/");
		/*
		 * EntityProvider entityProvider =
		 * service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
		 * "/", FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, "qqq");
		 */

		/*
		 * service.saveAttributesEntityProvider(getTestSSOToken(), "/",
		 * entityProvider);
		 */

		/*
		 * service.deleteEntityProvider(getTestSSOToken(), "/", "123"); String
		 * config =
		 * readFile("/home/viorel/workspace/codeGenerator/opensso1.xml"); String
		 * metadata =
		 * readFile("/home/viorel/workspace/codeGenerator/opensso2.xml");
		 * service.importEntityProvider(getTestSSOToken(), "/", config,
		 * metadata);
		 */
		// EntityProvider entityProvider =
		// service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(),
		// "/", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, "all");
		// System.out.println("");

		EntityProvider entityProvider = new EntityProvider(FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, "/",
				FederationConstants.REMOTE_VALUE);

		service.deleteEntityProvider(getTestSSOToken(), "/", "ABCDEFG2");
		entityProvider.setEntityIdentifier("ABCDEFG2");

		Map<String, EntityProviderDescriptor> descriptorMap = new HashMap<String, EntityProviderDescriptor>();

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT, new EntityProviderDescriptor(
				EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT, "metaalis", "", ""));

		entityProvider.setDescriptorMap(descriptorMap);
		service.addEntityProvider(getTestSSOToken(), "/", entityProvider);

	}

	// @Test
	public void test0() throws Exception {

		EntityProvider entityProvider = new EntityProvider(FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, "/",
				FederationConstants.REMOTE_VALUE);

		service.deleteEntityProvider(getTestSSOToken(), "/", "ABCDEFG3");
		entityProvider.setEntityIdentifier("ABCDEFG3");

		Map<String, EntityProviderDescriptor> descriptorMap = new HashMap<String, EntityProviderDescriptor>();

		descriptorMap.put(EntityProviderDescriptor.WSFED_GENERAL, new EntityProviderDescriptor(
				EntityProviderDescriptor.WSFED_GENERAL, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.WSFED_IDENTITY_PROVIDER, new EntityProviderDescriptor(
				EntityProviderDescriptor.WSFED_IDENTITY_PROVIDER, "metaalis", "", ""));

		descriptorMap.put(EntityProviderDescriptor.WSFED_SERVICE_PROVIDER, new EntityProviderDescriptor(
				EntityProviderDescriptor.WSFED_SERVICE_PROVIDER, "metaalis", "", ""));

		entityProvider.setDescriptorMap(descriptorMap);
		service.addEntityProvider(getTestSSOToken(), "/", entityProvider);

	}

	//@Test
	public void test3() {
		
		EntityProvider entityProvider = service.getEntityProvidersForGivenTypeAndGivenName(getTestSSOToken(), "/",
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, "aaa");
		System.out.println("");
		//service.saveAttributesEntityProvider(getTestSSOToken(), "/", entityProvider);


	}
	
	@Test
	public void test2() {
	
		EntityProvider entityProvider  = createEntityProvider();
		service.addEntityProvider(getTestSSOToken(), "/", entityProvider);
		
		System.out.println("");

	}

	private void removeCircleOfTrustIfThere(String name) {
		CircleOfTrust circleOfTrust = service.getCircleOfTrust(getTestSSOToken(), TEST_REALM, name);
		if (circleOfTrust != null) {
			service.deleteCircleOfTrust(getTestSSOToken(), TEST_REALM, NAME1);
		}
	}

	private void removeEntityProviderIfThere(String name, String type) {

		service.deleteEntityProvider(getTestSSOToken(), TEST_REALM, NAME1);

	}

	private CircleOfTrust createCircleOfTrust() {
		CircleOfTrust circleOfTrust = new CircleOfTrust();
		circleOfTrust.setName(NAME1);
		circleOfTrust.setDescription("");
		circleOfTrust.setIdffReaderServiceURL("http:www.reader.com");
		circleOfTrust.setIdffWriterServiceURL("");
		circleOfTrust.setSaml2ReaderServiceURL("");
		circleOfTrust.setSaml2WriterServiceURL("http:www.google.com");
		circleOfTrust.setStatus("active");
		circleOfTrust.setTrustedProviders(new ArrayList<String>());
		return circleOfTrust;
	}

	private EntityProvider createEntityProvider() {
		EntityProvider entityProvider = new EntityProvider();
		entityProvider.setEntityIdentifier("a1");
		entityProvider.setProtocol(FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);

		Map<String, EntityProviderDescriptor> descriptorMap = new HashMap<String, EntityProviderDescriptor>();
		
		
		EntityProviderDescriptor entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY, entityProviderDescriptor);
		
		
		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER, entityProviderDescriptor);
		
		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT, entityProviderDescriptor);
		
		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT, entityProviderDescriptor);

		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY, entityProviderDescriptor);
		
		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER, entityProviderDescriptor);
		
		entityProviderDescriptor = new EntityProviderDescriptor();
		entityProviderDescriptor.setType(EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER);
		entityProviderDescriptor.setMetaAlias("abc");
		descriptorMap.put(EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER, entityProviderDescriptor);
		
		
		entityProvider.setDescriptorMap(descriptorMap);
		

		return entityProvider;
	}
}
