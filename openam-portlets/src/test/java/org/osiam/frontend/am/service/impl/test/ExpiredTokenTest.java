/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.springframework.beans.factory.annotation.Autowired;

import com.iplanet.sso.SSOToken;

/**
 * 
 * This class contains some code useful for manual testing what is happening
 * when the SSOToken got expired. This class is not included in the test suite.
 * 
 */

public class ExpiredTokenTest extends BaseOSIAMTest {

	@Autowired
	private AuthenticationService service;

	@Before
	public void setUp() {
		// cleaning up the test realm, so that it has no chains attached
		Set<String> chainNameSet = service.getChainNameSet(getTestSSOToken(), TEST_REALM);
		Iterator<String> chainNameSetIterator = chainNameSet.iterator();
		while (chainNameSetIterator.hasNext()) {
			service.deleteChain(getTestSSOToken(), TEST_REALM, chainNameSetIterator.next());
		}
	}

	/**
	 * 
	 * This method contains some code useful for manual testing what is
	 * happening when the SSOToken got expired. This test is not included in the
	 * test suite (e.g. add the "Test" annotation only when needed and do not
	 * commit)
	 * @throws InterruptedException 
	 * 
	 */
	@Test
	public void testGetChainNameSetExpiredToken() throws InterruptedException {

		final String CHAIN1 = "chain1";
		final String CHAIN2 = "chain2";

		SSOToken token = getTestSSOToken();

		final Set<String> EXPECTED_SET = new HashSet<String>(2);
		EXPECTED_SET.add(CHAIN1);
		EXPECTED_SET.add(CHAIN2);

		service.addNewChain(token, TEST_REALM, CHAIN1);
		service.addNewChain(token, TEST_REALM, CHAIN2);

		System.out.println("NOW Sleeping");
		Thread.sleep(60 * 10);
//		Thread.sleep(60 * 1000);
		// stop the Tomcat server here so that every token expires NOW
		System.out.println("AWAKE !");

		if (!tokenIsValid(token)) {
			System.out.println("INVALID !");
		}

		Set<String> chainSet = service.getChainNameSet(token, TEST_REALM);
		Assert.assertTrue("chain name set is not properly returned by method getChainNameSet",
				EXPECTED_SET.equals(chainSet));

	}

	private boolean tokenIsValid(SSOToken token) {
		return token != null;
	}

}
