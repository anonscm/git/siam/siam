/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.osiam.frontend.configuration.domain.agents.Agent;
import org.osiam.frontend.configuration.domain.agents.J2EEAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.J2EEApplicationBean;
import org.osiam.frontend.configuration.domain.agents.J2EEGlobalBean;
import org.osiam.frontend.configuration.domain.agents.J2EEMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.J2EEOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.J2EESSOBean;
import org.osiam.frontend.configuration.domain.agents.LocalAgent;
import org.osiam.frontend.configuration.domain.agents.WebAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.WebApplicationBean;
import org.osiam.frontend.configuration.domain.agents.WebGlobalBean;
import org.osiam.frontend.configuration.domain.agents.WebMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.WebOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.WebSSOBean;
import org.osiam.frontend.configuration.exceptions.AgentsException;
import org.osiam.frontend.configuration.service.AgentsConstants;
import org.osiam.frontend.configuration.service.AgentsService;
import org.springframework.beans.factory.annotation.Autowired;

public class AgentsServiceImplTest extends BaseOSIAMTest {

	private final String[] AGENT_NAMES = new String[] { "a" + (new Date()).getTime(), "b" + (new Date()).getTime() };
	private final String[] AGENT_TYPES = new String[] { AgentsConstants.AGENT_TYPE_WEB, AgentsConstants.AGENT_TYPE_WEB };

	@Autowired
	private AgentsService service;

	@Before
	public void setUp() {
		// delete all data stores in the current realm
		List<Agent> agents = service.getAgentsList(getTestSSOToken(), TEST_REALM, Integer.MAX_VALUE);
		Iterator<Agent> agentsIterator = agents.iterator();
		while (agentsIterator.hasNext()) {
			service.deleteAgent(getTestSSOToken(), TEST_REALM, agentsIterator.next().getName());
		}
	}

	@Test
	public void testGetAgentsListCorrect() {

		final Agent[] EXPECTED_LIST = createTestAgents();

		service.addAgent(getTestSSOToken(), TEST_REALM, EXPECTED_LIST[0]);
		service.addAgent(getTestSSOToken(), TEST_REALM, EXPECTED_LIST[1]);

		List<Agent> agentList = service.getAgentsList(getTestSSOToken(), TEST_REALM, Integer.MAX_VALUE);

		Assert.assertTrue("agent list is not properly returned by method getAgentList", agentList != null);
		Assert.assertTrue("agent list is not properly returned by method getAgentList",
				agentList.size() == EXPECTED_LIST.length);

	}

	@Test
	public void testGetAgentListNullToken() {
		try {
			service.getAgentsList(null, TEST_REALM, Integer.MAX_VALUE);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAgentListNullRealm() {
		try {
			service.getAgentsList(getTestSSOToken(), null, Integer.MAX_VALUE);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {
			return;
		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
		Assert.fail("There should be an exception thrown");
	}

	@Test
	public void testGetAgentListWrongRealm() {
		try {
			service.getAgentsList(getTestSSOToken(), WRONG_REALM, Integer.MAX_VALUE);
			Assert.fail(String.format("If the realm is wrong, method must raise AgentsException."));
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAgentsListNegativeCount() {
		try {
			service.getAgentsList(getTestSSOToken(), TEST_REALM, -1);

			Assert.fail(String.format("If the count param is negative, method must raise IllegalArgumentException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the maxResults parameter is negative, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddAgentCorrect() {

		final Agent[] EXPECTED_LIST = createTestAgents();

		service.addAgent(getTestSSOToken(), TEST_REALM, EXPECTED_LIST[0]);
		service.addAgent(getTestSSOToken(), TEST_REALM, EXPECTED_LIST[1]);

		List<Agent> agentList = service.getAgentsList(getTestSSOToken(), TEST_REALM, Integer.MAX_VALUE);

		Assert.assertTrue("agent list is not properly returned by method getAgentList", agentList != null);
		Assert.assertTrue("agent list is not properly returned by method getAgentList",
				agentList.size() == EXPECTED_LIST.length);
	}

	@Test
	public void testAddAgentNullToken() {
		try {
			final Agent[] AGENTS = createTestAgents();
			service.addAgent(null, TEST_REALM, AGENTS[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddAgentNullRealm() {
		try {
			final Agent[] AGENTS = createTestAgents();
			service.addAgent(getTestSSOToken(), null, AGENTS[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddAgentWrongRealm() {
		try {
			final Agent[] AGENTS = createTestAgents();
			service.addAgent(getTestSSOToken(), WRONG_REALM, AGENTS[0]);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteAgentCorrect() {

		final Agent[] AGENTS = createTestAgents();
		final int NUMBER_OF_AGENTS_AFTER_DELETE = 1;

		service.addAgent(getTestSSOToken(), TEST_REALM, AGENTS[0]);
		service.addAgent(getTestSSOToken(), TEST_REALM, AGENTS[1]);

		service.deleteAgent(getTestSSOToken(), TEST_REALM, AGENTS[0].getName());

		List<Agent> agentList = service.getAgentsList(getTestSSOToken(), TEST_REALM, Integer.MAX_VALUE);

		int agentListSize = agentList.size();

		AGENTS[0].setPassword(null);
		AGENTS[0].setPassword(null);
		Assert.assertEquals(
				String.format(
						"If the agent is not found, the method must raise a warning and delete nothing. However, instead of one agent there are only %d agents now.",
						agentListSize), NUMBER_OF_AGENTS_AFTER_DELETE, agentListSize);

		Assert.assertTrue("After the delete operation, the agent'" + AGENTS[1].getName()
				+ "' must not be deleted. However, it seems it was deleted.",
				agentList.get(0).getName().equals(AGENTS[1].getName()));

		Assert.assertFalse("After the delete operation, the agent'" + AGENTS[0].getName()
				+ "' must be deleted. However, it seems it was NOT deleted.",
				agentList.get(0).getName().equals(AGENTS[0].getName()));

	}

	@Test
	public void testDeleteAgentNullToken() {
		try {
			service.deleteAgent(null, TEST_REALM, "agent2");
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteAgentNullRealm() {
		try {
			service.deleteAgent(getTestSSOToken(), null, "agent2");
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteAgentNullAgentName() {
		try {
			service.deleteAgent(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If agentName param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If agentName param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAgentGroupsListNullToken() {
		try {
			service.getAgentsList(null, TEST_REALM, 1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAgentGroupsListNullRealm() {
		try {
			service.getAgentsList(getTestSSOToken(), null, 1);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAgentGroupsListWrongRealm() {
		try {
			service.getAgentsList(getTestSSOToken(), WRONG_REALM, 1);
			Assert.fail("If the realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm param is wrong,  method must raise AgentsException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForLocalAgent() {
		final LocalAgent[] AGENTS = createTestLocalAgents();

		service.saveLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[0]);
		service.saveLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[1]);

		LocalAgent localAgent = service.getAttributesForLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[0].getName());

		AGENTS[0].setPassword(null);
		localAgent.setPassword(null);
		AGENTS[0].setPasswordConfirm(null);
		localAgent.setPasswordConfirm(null);

		Assert.assertTrue("Get operation failed, wrong fields.", localAgent.equals(AGENTS[0]));

	}

	@Test
	public void testSaveAttributesForWebGlobal() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebGlobalBean webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		service.saveWebGlobal(getTestSSOToken(), TEST_REALM, webGlobalBean);

		webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webGlobalBean);

	}

	@Test
	public void testSaveAttributesForWebApplication() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebApplicationBean webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = ",";
		webApplicationBean.setAttributeMultiValueSeparator(EXPECTED_FIELD);
		service.saveWebApplication(getTestSSOToken(), TEST_REALM, webApplicationBean);

		webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webApplicationBean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD,
				webApplicationBean.getAttributeMultiValueSeparator());
	}

	@Test
	public void testSaveAttributesForWebSSO() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebSSOBean webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final String EXPECTED_COOKIE_NAME = "abc";
		webSSOBean.setCookieName(EXPECTED_COOKIE_NAME);
		service.saveWebSSO(getTestSSOToken(), TEST_REALM, webSSOBean);

		webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webSSOBean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_COOKIE_NAME, webSSOBean.getCookieName());
	}

	@Test
	public void testSaveAttributesForWebOpenSSOServices() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebOpenSSOServicesBean bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setLogoutRedirectURL(EXPECTED_FIELD);
		service.saveWebOpenSSOServices(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getLogoutRedirectURL());
	}

	@Test
	public void testSaveAttributesForWebMiscellaneous() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebMiscellaneousBean bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setAgentLocale(EXPECTED_FIELD);
		service.saveWebMiscellaneous(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getAgentLocale());
	}

	@Test
	public void testSaveAttributesForWebAdvanced() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebAdvancedBean bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setAuthenticationType(EXPECTED_FIELD);
		service.saveWebAdvanced(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getAuthenticationType());
	}

	@Test
	public void testSaveAttributesForJ2EEGlobal() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEGlobalBean bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		bean.setAgentFilterMode(Arrays.asList("ALL"));
		bean.setAgentDebugLevel("message");
		service.saveJ2EEGlobal(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testSaveAttributesForJ2EEApplication() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEApplicationBean bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		service.saveJ2EEApplication(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
	}

	@Test
	public void testSaveAttributesForJ2EESSO() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EESSOBean bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		service.saveJ2EESSO(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testSaveAttributesForJ2EEOpenSSOServices() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEOpenSSOServicesBean bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		service.saveJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testSaveAttributesForJ2EEMiscellaneousBean() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEMiscellaneousBean bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		service.saveJ2EEMiscellaneous(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testSaveAttributesForJ2EEAdvancedBean() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEAdvancedBean bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		service.saveJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testGetAttributesForLocalAgent() {
		final LocalAgent[] AGENTS = createTestLocalAgents();

		service.saveLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[0]);
		service.saveLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[1]);

		LocalAgent localAgent = service.getAttributesForLocalAgent(getTestSSOToken(), TEST_REALM, AGENTS[0].getName());

		AGENTS[0].setPassword(null);
		localAgent.setPassword(null);
		AGENTS[0].setPasswordConfirm(null);
		localAgent.setPasswordConfirm(null);

		Assert.assertTrue("Get operation failed, wrong fields.", localAgent.equals(AGENTS[0]));

	}

	@Test
	public void testGetAttributesForWebGlobal() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebGlobalBean webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final Integer EXPECTED_AGENT_DEBUG_FILE_SIZE = new Integer(123);
		webGlobalBean.setAgentDebugFileSize(EXPECTED_AGENT_DEBUG_FILE_SIZE);
		service.saveWebGlobal(getTestSSOToken(), TEST_REALM, webGlobalBean);

		webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webGlobalBean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_AGENT_DEBUG_FILE_SIZE,
				webGlobalBean.getAgentDebugFileSize());
	}

	@Test
	public void testGetAttributesForWebGlobalNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebGlobal(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebGlobalNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebGlobal(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebGlobalWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebGlobal(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is wrong, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebApplication() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebApplicationBean webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = ",";
		webApplicationBean.setAttributeMultiValueSeparator(EXPECTED_FIELD);
		service.saveWebApplication(getTestSSOToken(), TEST_REALM, webApplicationBean);

		webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webApplicationBean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD,
				webApplicationBean.getAttributeMultiValueSeparator());
	}

	@Test
	public void testGetAttributesForWebApplicationNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebApplication(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebApplicationNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebApplication(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebApplicationWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebApplication(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebSSO() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebSSOBean webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final String EXPECTED_COOKIE_NAME = "abc";
		webSSOBean.setCookieName(EXPECTED_COOKIE_NAME);
		service.saveWebSSO(getTestSSOToken(), TEST_REALM, webSSOBean);

		webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(webSSOBean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_COOKIE_NAME, webSSOBean.getCookieName());
	}

	@Test
	public void testGetAttributesForWebSSONullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebSSO(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebSSONullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebSSO(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebSSOWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebSSO(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebOpenSSOServices() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebOpenSSOServicesBean bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setLogoutRedirectURL(EXPECTED_FIELD);
		service.saveWebOpenSSOServices(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getLogoutRedirectURL());
	}

	@Test
	public void testGetAttributesForWebOpenSSOServicesNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {

			service.getAttributesForWebOpenSSOServices(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebOpenSSOServicesNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebOpenSSOServices(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebOpenSSOServicesWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebOpenSSOServices(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebMiscellaneous() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebMiscellaneousBean bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setAgentLocale(EXPECTED_FIELD);
		service.saveWebMiscellaneous(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getAgentLocale());
	}

	@Test
	public void testGetAttributesForWebMiscellaneousNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebMiscellaneous(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebMiscellaneousNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebMiscellaneous(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebMiscellaneousWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebMiscellaneous(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebAdvanced() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebAdvancedBean bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setAuthenticationType(EXPECTED_FIELD);
		service.saveWebAdvanced(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getAuthenticationType());
	}

	@Test
	public void testGetAttributesForWebAdvancedNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebAdvanced(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebAdvancedNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebAdvanced(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForWebAdvancedWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		try {
			service.getAttributesForWebAdvanced(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEGlobal() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEGlobalBean bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		bean.setAgentFilterMode(Arrays.asList("ALL"));
		bean.setAgentDebugLevel("message");
		service.saveJ2EEGlobal(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testGetAttributesForJ2EEGlobalNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEGlobal(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEGlobalNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));
		try {
			service.getAttributesForJ2EEGlobal(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEGlobalWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEGlobal(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEApplication() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEApplicationBean bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setCookieSeparatorCharacter(EXPECTED_FIELD);
		service.saveJ2EEApplication(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getCookieSeparatorCharacter());
	}

	@Test
	public void testGetAttributesForJ2EEApplicationNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEApplication(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEApplicationNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEApplication(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEApplicationWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEApplication(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EESSO() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EESSOBean bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		service.saveJ2EESSO(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
	}

	@Test
	public void testGetAttributesForJ2EESSONullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EESSO(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EESSONullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EESSO(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EESSOWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EESSO(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException exception raised is %s", e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEOpenSSOServices() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEOpenSSOServicesBean bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		service.saveJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);

	}

	@Test
	public void testGetAttributesForJ2EEOpenSSOServicesNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEOpenSSOServices(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEOpenSSOServicesNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEOpenSSOServicesWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEMiscellaneousBean() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEMiscellaneousBean bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setEncryptionProvider(EXPECTED_FIELD);
		service.saveJ2EEMiscellaneous(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getEncryptionProvider());
	}

	@Test
	public void testGetAttributesForJ2EEMiscellaneousBeanNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEMiscellaneousBean(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEMiscellaneousBeanNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEMiscellaneousBeanWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEAdvancedBean() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEAdvancedBean bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final String EXPECTED_FIELD = "abc";
		bean.setAlternativeAgentHostName(EXPECTED_FIELD);
		bean.setWebServiceEndPoints(new ArrayList<String>());
		service.saveJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, bean);

		bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertNotNull(bean);
		Assert.assertEquals("Field not correctly returned.", EXPECTED_FIELD, bean.getAlternativeAgentHostName());
	}

	@Test
	public void testGetAttributesForJ2EEAdvancedBeanNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEAdvancedBean(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEAdvancedBeanNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAttributesForJ2EEAdvancedBeanWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), WRONG_REALM, AGENT_NAMES[0]);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebGlobalNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		WebGlobalBean webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final Integer EXPECTED_AGENT_DEBUG_FILE_SIZE = new Integer(123);
		webGlobalBean.setAgentDebugFileSize(EXPECTED_AGENT_DEBUG_FILE_SIZE);

		try {
			service.saveWebGlobal(null, TEST_REALM, webGlobalBean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebGlobalNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebGlobalBean webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final Integer EXPECTED_AGENT_DEBUG_FILE_SIZE = new Integer(123);
		webGlobalBean.setAgentDebugFileSize(EXPECTED_AGENT_DEBUG_FILE_SIZE);

		try {
			service.saveWebGlobal(getTestSSOToken(), null, webGlobalBean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebGlobalWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebGlobalBean webGlobalBean = service.getAttributesForWebGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		final Integer EXPECTED_AGENT_DEBUG_FILE_SIZE = new Integer(123);
		webGlobalBean.setAgentDebugFileSize(EXPECTED_AGENT_DEBUG_FILE_SIZE);

		try {
			service.saveWebGlobal(getTestSSOToken(), WRONG_REALM, webGlobalBean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebApplicationNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebApplicationBean webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebApplication(null, TEST_REALM, webApplicationBean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebApplicationNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebApplicationBean webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebApplication(getTestSSOToken(), null, webApplicationBean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebApplicationWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebApplicationBean webApplicationBean = service.getAttributesForWebApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebApplication(getTestSSOToken(), WRONG_REALM, webApplicationBean);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebSSONullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebSSOBean webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebSSO(null, TEST_REALM, webSSOBean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebSSONullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebSSOBean webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebSSO(getTestSSOToken(), null, webSSOBean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebSSOWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebSSOBean webSSOBean = service.getAttributesForWebSSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebSSO(getTestSSOToken(), WRONG_REALM, webSSOBean);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebOpenSSOServicesNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebOpenSSOServicesBean bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebOpenSSOServices(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebOpenSSOServicesNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebOpenSSOServicesBean bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebOpenSSOServices(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebOpenSSOServicesWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebOpenSSOServicesBean bean = service.getAttributesForWebOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebOpenSSOServices(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebMiscellaneousNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebMiscellaneousBean bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebMiscellaneous(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebMiscellaneousNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebMiscellaneousBean bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebMiscellaneous(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebMiscellaneousWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebMiscellaneousBean bean = service.getAttributesForWebMiscellaneous(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveWebMiscellaneous(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebAdvancedNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebAdvancedBean bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebAdvanced(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebAdvancedNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebAdvancedBean bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebAdvanced(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForWebAdvancedWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM, createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_WEB));

		WebAdvancedBean bean = service.getAttributesForWebAdvanced(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveWebAdvanced(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise IllegalArgumentException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEGlobalNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEGlobalBean bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EEGlobal(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEGlobalNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEGlobalBean bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EEGlobal(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEGlobalWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEGlobalBean bean = service.getAttributesForJ2EEGlobal(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		bean.setAgentFilterMode(Arrays.asList("ALL"));
		bean.setAgentDebugLevel("message");
		try {
			service.saveJ2EEGlobal(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEApplicationNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEApplicationBean bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		try {
			service.saveJ2EEApplication(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEApplicationNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEApplicationBean bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		try {
			service.saveJ2EEApplication(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEApplicationWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEApplicationBean bean = service.getAttributesForJ2EEApplication(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);
		try {
			service.saveJ2EEApplication(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EESSONullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EESSOBean bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EESSO(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EESSONullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EESSOBean bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EESSO(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	// @Test
	// TODO: test fails, exception not thrown
	public void testSaveAttributesForJ2EESSOWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EESSOBean bean = service.getAttributesForJ2EESSO(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EESSO(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEOpenSSOServicesNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEOpenSSOServicesBean bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEOpenSSOServices(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEOpenSSOServicesNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEOpenSSOServicesBean bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEOpenSSOServices(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEOpenSSOServicesWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEOpenSSOServicesBean bean = service.getAttributesForJ2EEOpenSSOServices(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEOpenSSOServices(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEMiscellaneousBeanNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEMiscellaneousBean bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEMiscellaneous(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEMiscellaneousBeanNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEMiscellaneousBean bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEMiscellaneous(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEMiscellaneousBeanWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEMiscellaneousBean bean = service.getAttributesForJ2EEMiscellaneousBean(getTestSSOToken(), TEST_REALM,
				AGENT_NAMES[0]);

		try {
			service.saveJ2EEMiscellaneous(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEAdvancedBeanNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEAdvancedBean bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EEAdvancedBean(null, TEST_REALM, bean);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEAdvancedBeanNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEAdvancedBean bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EEAdvancedBean(getTestSSOToken(), null, bean);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAttributesForJ2EEAdvancedBeanWrongRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		J2EEAdvancedBean bean = service.getAttributesForJ2EEAdvancedBean(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);

		try {
			service.saveJ2EEAdvancedBean(getTestSSOToken(), WRONG_REALM, bean);
			Assert.fail("If realm param is wrong, method must raise AgentsException.");
		} catch (AgentsException e) {

		} catch (Exception e) {
			Assert.fail(String.format(
					"If realm param is wrong, method must raise AgentsException. The actual exception raised is %s",
					e.toString()));
		}
	}

	@Test
	public void testAgentNameAlreadyExistNullToken() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.agentNameAlreadyExist(null, TEST_REALM, AGENT_NAMES[0]);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAgentNameAlreadyExistNullRealm() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		try {
			service.agentNameAlreadyExist(getTestSSOToken(), null, AGENT_NAMES[0]);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	

	@Test
	public void testAgentNameAlreadyExistExists() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		boolean exists = service.agentNameAlreadyExist(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0]);
		Assert.assertTrue("agent must be there", exists);
	}

	@Test
	public void testAgentNameAlreadyExistNotExists() {

		service.addAgent(getTestSSOToken(), TEST_REALM,
				createTestAgent(AGENT_NAMES[0], AgentsConstants.AGENT_TYPE_J2EE));

		boolean exists = service.agentNameAlreadyExist(getTestSSOToken(), TEST_REALM, AGENT_NAMES[0] + "blah");
		Assert.assertFalse("agent must not be there", exists);
	}

	private LocalAgent[] createTestLocalAgents() {
		final LocalAgent AGENT1 = new LocalAgent();
		final LocalAgent AGENT2 = new LocalAgent();
		AGENT1.setName(AGENT_NAMES[0]);
		AGENT2.setName(AGENT_NAMES[1]);
		AGENT1.setPassword("password");
		AGENT2.setPassword("password");
		AGENT1.setType(AGENT_TYPES[0]);
		AGENT2.setType(AGENT_TYPES[1]);
		AGENT1.setStatus("active");
		AGENT2.setStatus("active");
		AGENT1.setAgentRootURLforCDSSO(Arrays.asList("http://www.google.com:8080/"));
		AGENT2.setAgentRootURLforCDSSO(Arrays.asList("http://www.google.com:8080/"));

		return new LocalAgent[] { AGENT1, AGENT2 };
	}

	private Agent[] createTestAgents() {
		Agent[] agents = new Agent[2];
		agents[0] = createTestAgent(AGENT_NAMES[0], AGENT_TYPES[0]);
		agents[1] = createTestAgent(AGENT_NAMES[1], AGENT_TYPES[1]);
		return agents;
	}

	private Agent createTestAgent(String name, String type) {
		final Agent AGENT1 = new Agent();
		AGENT1.setName(name);
		AGENT1.setPassword("password");
		AGENT1.setType(type);
		if (AgentsConstants.AGENT_TYPE_WEB.equals(type)) {
			AGENT1.setAgentURL("http://google.com:9090/");
		} else {
			AGENT1.setAgentURL("http://google.com:9090/blahblah");
		}
		AGENT1.setServerURL("http://google.com:9090/blah");
		AGENT1.setConfiguration(AgentsConstants.AGENT_CONFIGURATION_CENTRALIZED);

		return AGENT1;
	}
}
