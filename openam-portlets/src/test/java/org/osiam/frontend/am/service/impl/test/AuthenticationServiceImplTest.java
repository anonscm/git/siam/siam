/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.osiam.frontend.am.domain.authentication.ChainProperty;
import org.osiam.frontend.am.exceptions.AuthenticationException;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.service.impl.test.BaseOSIAMTest;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthenticationServiceImplTest extends BaseOSIAMTest {

	@Autowired
	private AuthenticationService service;

	@Before
	public void setUp() {
		// cleaning up the test realm, so that it has no chains attached
		Set<String> chainNameSet = service.getChainNameSet(getTestSSOToken(), TEST_REALM);
		Iterator<String> chainNameSetIterator = chainNameSet.iterator();
		while (chainNameSetIterator.hasNext()) {
			service.deleteChain(getTestSSOToken(), TEST_REALM, chainNameSetIterator.next());
		}
	}

	@Test
	public void testGetChainNameSet() {

		final String CHAIN1 = "chain1";
		final String CHAIN2 = "chain2";

		final Set<String> EXPECTED_SET = new HashSet<String>(2);
		EXPECTED_SET.add(CHAIN1);
		EXPECTED_SET.add(CHAIN2);

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN1);
		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN2);

		Set<String> chainSet = service.getChainNameSet(getTestSSOToken(), TEST_REALM);
		Assert.assertTrue("chain name set is not properly returned by method getChainNameSet",
				EXPECTED_SET.equals(chainSet));

	}

	@Test
	public void testGetChainNameSetNullToken() {
		try {
			service.getChainNameSet(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainNameSetNullRealm() {
		try {
			service.getChainNameSet(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainNameSetWrongRealm() {
		try {
			service.getChainNameSet(getTestSSOToken(), WRONG_REALM);

			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddNewChainNullToken() {
		try {
			service.addNewChain(null, TEST_REALM, A_CHAIN);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddNewChainNullRealm() {
		try {
			service.addNewChain(getTestSSOToken(), null, A_CHAIN);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddNewChainNullChain() {
		try {
			service.addNewChain(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddNewChainWrongRealm() {
		try {
			service.addNewChain(getTestSSOToken(), WRONG_REALM, A_CHAIN);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testAddNewChainCorrect() {
		final String CHAIN1 = "chain1";
		final byte NUMBER_OF_CHAINS_AFTER_ADD = 1;

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN1);

		Set<String> chainSet = service.getChainNameSet(getTestSSOToken(), TEST_REALM);
		int chainSetSize = chainSet.size();

		Assert.assertEquals(
				String.format(
						"If the chain is not found, the method must raise a warning and delete nothing. However, instead of one chain there are only %d chains now.",
						chainSetSize), NUMBER_OF_CHAINS_AFTER_ADD, chainSetSize);

		Assert.assertTrue("After the delete operation, the chain'" + CHAIN1
				+ "' must be present. However, it seems it is not there.", chainSet.contains(CHAIN1));

	}

	@Test
	public void testGetOrganizationChainNullToken() {
		try {
			service.getOrganizationChain(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetOrganizationChainNullRealm() {
		try {
			service.getOrganizationChain(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetOrganizationChainWrongRealm() {
		try {
			service.getOrganizationChain(getTestSSOToken(), WRONG_REALM);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetOrganizationChainCorrect() {

		final String CHAIN_ORGANIZATION = "chainOrganization";
		final String CHAIN_ADMINISTRATOR = "chainAdministrator";

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION);
		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ADMINISTRATOR);

		service.saveAuthenticationConfiguration(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION, CHAIN_ADMINISTRATOR);

		String organization = service.getOrganizationChain(getTestSSOToken(), TEST_REALM);
		Assert.assertEquals(
				String.format("Organization must have been %s. However, it is %s", CHAIN_ORGANIZATION, organization),
				CHAIN_ORGANIZATION, organization);
	}

	@Test
	public void testGetAdministratorChainWrongRealm() {
		try {
			service.getAdministratorChain(getTestSSOToken(), WRONG_REALM);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAdministratorChainNullToken() {
		try {
			service.getAdministratorChain(null, TEST_REALM);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAdministratorChainNullRealm() {
		try {
			service.getAdministratorChain(getTestSSOToken(), null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetAdministratorChainCorrect() {

		final String CHAIN_ORGANIZATION = "chainOrganization";
		final String CHAIN_ADMINISTRATOR = "chainAdministrator";

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION);
		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ADMINISTRATOR);

		service.saveAuthenticationConfiguration(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION, CHAIN_ADMINISTRATOR);

		String administrator = service.getAdministratorChain(getTestSSOToken(), TEST_REALM);
		Assert.assertEquals(
				String.format("Administrator must have been %s. However, it is %s", CHAIN_ADMINISTRATOR, administrator),
				CHAIN_ADMINISTRATOR, administrator);
	}

	@Test
	public void testDeleteChainNullToken() {
		try {
			service.deleteChain(null, TEST_REALM, A_CHAIN);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteChainNullRealm() {
		try {
			service.deleteChain(getTestSSOToken(), null, A_CHAIN);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteChainNullChain() {
		try {
			service.deleteChain(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteChainWrongRealm() {
		try {
			service.deleteChain(getTestSSOToken(), WRONG_REALM, A_CHAIN);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testDeleteChainWrongChain() {
		final String CHAIN1 = "chain1";
		final byte NUMBER_OF_CHAINS_INSERTED = 1;

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN1);

		service.deleteChain(getTestSSOToken(), TEST_REALM, WRONG_CHAIN);
		int chainSetSize = service.getChainNameSet(getTestSSOToken(), TEST_REALM).size();

		Assert.assertEquals(
				String.format(
						"If the chain is not found, the method must raise a warning and delete nothing. However, instead of one chain there are only %d chains now.",
						chainSetSize), NUMBER_OF_CHAINS_INSERTED, chainSetSize);
	}

	@Test
	public void testDeleteChainCorrect() {
		final String CHAIN1 = "chain1";
		final String CHAIN2 = "chain2";
		final byte NUMBER_OF_CHAINS_AFTER_DELETE = 1;

		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN1);
		service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN2);

		service.deleteChain(getTestSSOToken(), TEST_REALM, CHAIN1);
		Set<String> chainSet = service.getChainNameSet(getTestSSOToken(), TEST_REALM);
		int chainSetSize = chainSet.size();

		Assert.assertEquals(
				String.format(
						"If the chain is not found, the method must raise a warning and delete nothing. However, instead of one chain there are only %d chains now.",
						chainSetSize), NUMBER_OF_CHAINS_AFTER_DELETE, chainSetSize);

		Assert.assertTrue("After the delete operation, the chain'" + CHAIN2
				+ "' must not be deleted. However, it seems it was deleted.", chainSet.contains(CHAIN2));

		Assert.assertFalse("After the delete operation, the chain'" + CHAIN1
				+ "' must be deleted. However, it seems it was NOT deleted.", chainSet.contains(CHAIN1));

	}

	@Test
	public void testSaveAuthenticationConfigurationNullToken() {
		try {

			final String CHAIN_ORGANIZATION = "chainOrganization";
			final String CHAIN_ADMINISTRATOR = "chainAdministrator";

			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION);
			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ADMINISTRATOR);

			service.saveAuthenticationConfiguration(null, TEST_REALM, CHAIN_ORGANIZATION, CHAIN_ADMINISTRATOR);

			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAuthenticationConfigurationNullRealm() {
		try {
			final String CHAIN_ORGANIZATION = "chainOrganization";
			final String CHAIN_ADMINISTRATOR = "chainAdministrator";

			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION);
			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ADMINISTRATOR);

			service.saveAuthenticationConfiguration(getTestSSOToken(), null, CHAIN_ORGANIZATION, CHAIN_ADMINISTRATOR);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveAuthenticationConfigurationWrongRealm() {
		try {
			final String CHAIN_ORGANIZATION = "chainOrganization";
			final String CHAIN_ADMINISTRATOR = "chainAdministrator";

			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ORGANIZATION);
			service.addNewChain(getTestSSOToken(), TEST_REALM, CHAIN_ADMINISTRATOR);

			service.saveAuthenticationConfiguration(getTestSSOToken(), WRONG_REALM, CHAIN_ORGANIZATION,
					CHAIN_ADMINISTRATOR);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainPropertyListNullToken() {
		try {
			service.getChainPropertyList(null, TEST_REALM, A_CHAIN);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainPropertyListNullRealm() {
		try {
			service.getChainPropertyList(getTestSSOToken(), null, A_CHAIN);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainPropertyListNullChain() {
		try {
			service.getChainPropertyList(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainPropertyListWrongRealm() {
		try {
			service.getChainPropertyList(getTestSSOToken(), WRONG_REALM, A_CHAIN);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testGetChainPropertyListCorrect() {

		final byte CHAIN_PROPERTIES_COUNT = 2;

		final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(CHAIN_PROPERTIES_COUNT);
		final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
		CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
		CHAIN_PROPERTY1.setInstance("instance1");
		CHAIN_PROPERTY1.setOptions("option1");
		CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

		final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
		CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
		CHAIN_PROPERTY2.setInstance("instance2");
		CHAIN_PROPERTY2.setOptions("option2");
		CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

		service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

		service.saveChainPropertyList(getTestSSOToken(), TEST_REALM, A_CHAIN, CHAIN_PROPERTY_LIST);
		List<ChainProperty> chainPropertyList = service.getChainPropertyList(getTestSSOToken(), TEST_REALM, A_CHAIN);

		Assert.assertEquals(String.format(
				"There must be %d properties after the save operation. Instead, there are %d.", CHAIN_PROPERTIES_COUNT,
				chainPropertyList.size()), CHAIN_PROPERTIES_COUNT, chainPropertyList.size());

		Assert.assertTrue("CHAIN_PROPERTY1 was not saved.", chainPropertyList.contains(CHAIN_PROPERTY1));
		Assert.assertTrue("CHAIN_PROPERTY1 was not saved.", chainPropertyList.contains(CHAIN_PROPERTY2));
	}

	@Test
	public void testSaveChainPropertyListNullToken() {
		try {
			final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(2);
			final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
			CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY1.setInstance("instance1");
			CHAIN_PROPERTY1.setOptions("option1");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

			final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
			CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY2.setInstance("instance2");
			CHAIN_PROPERTY2.setOptions("option2");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

			service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

			service.saveChainPropertyList(null, TEST_REALM, A_CHAIN, CHAIN_PROPERTY_LIST);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveChainPropertyListNullRealm() {
		try {
			final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(2);
			final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
			CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY1.setInstance("instance1");
			CHAIN_PROPERTY1.setOptions("option1");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

			final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
			CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY2.setInstance("instance2");
			CHAIN_PROPERTY2.setOptions("option2");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

			service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

			service.saveChainPropertyList(getTestSSOToken(), null, A_CHAIN, CHAIN_PROPERTY_LIST);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveChainPropertyListNullChain() {
		try {
			final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(2);
			final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
			CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY1.setInstance("instance1");
			CHAIN_PROPERTY1.setOptions("option1");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

			final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
			CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY2.setInstance("instance2");
			CHAIN_PROPERTY2.setOptions("option2");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

			service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

			service.saveChainPropertyList(getTestSSOToken(), TEST_REALM, null, CHAIN_PROPERTY_LIST);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveChainPropertyListWrongRealm() {
		try {
			final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(2);
			final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
			CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY1.setInstance("instance1");
			CHAIN_PROPERTY1.setOptions("option1");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

			final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
			CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY2.setInstance("instance2");
			CHAIN_PROPERTY2.setOptions("option2");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

			service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

			service.saveChainPropertyList(getTestSSOToken(), WRONG_REALM, A_CHAIN, CHAIN_PROPERTY_LIST);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveChainPropertyListNullList() {
		try {
			final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(2);
			final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
			CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY1.setInstance("instance1");
			CHAIN_PROPERTY1.setOptions("option1");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

			final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
			CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
			CHAIN_PROPERTY2.setInstance("instance2");
			CHAIN_PROPERTY2.setOptions("option2");
			CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

			service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

			service.saveChainPropertyList(getTestSSOToken(), TEST_REALM, A_CHAIN, null);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testSaveChainPropertyListCorrect() {

		final byte CHAIN_PROPERTIES_COUNT = 2;

		final List<ChainProperty> CHAIN_PROPERTY_LIST = new ArrayList<ChainProperty>(CHAIN_PROPERTIES_COUNT);
		final ChainProperty CHAIN_PROPERTY1 = new ChainProperty();
		CHAIN_PROPERTY1.setCriteria(service.getCriteriaList().get(0));
		CHAIN_PROPERTY1.setInstance("instance1");
		CHAIN_PROPERTY1.setOptions("option1");
		CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY1);

		final ChainProperty CHAIN_PROPERTY2 = new ChainProperty();
		CHAIN_PROPERTY2.setCriteria(service.getCriteriaList().get(0));
		CHAIN_PROPERTY2.setInstance("instance2");
		CHAIN_PROPERTY2.setOptions("option2");
		CHAIN_PROPERTY_LIST.add(CHAIN_PROPERTY2);

		service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

		service.saveChainPropertyList(getTestSSOToken(), TEST_REALM, A_CHAIN, CHAIN_PROPERTY_LIST);
		List<ChainProperty> chainPropertyList = service.getChainPropertyList(getTestSSOToken(), TEST_REALM, A_CHAIN);

		Assert.assertEquals(String.format(
				"There must be %d properties after the save operation. Instead, there are %d.", CHAIN_PROPERTIES_COUNT,
				chainPropertyList.size()), CHAIN_PROPERTIES_COUNT, chainPropertyList.size());

		Assert.assertTrue("CHAIN_PROPERTY1 was not saved.", chainPropertyList.contains(CHAIN_PROPERTY1));
		Assert.assertTrue("CHAIN_PROPERTY1 was not saved.", chainPropertyList.contains(CHAIN_PROPERTY2));
	}

	@Test
	public void testChainNameAlreadyExistNullToken() {
		try {
			service.chainNameAlreadyExist(null, TEST_REALM, A_CHAIN);
			Assert.fail("If token param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If token param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testChainNameAlreadyExistNullRealm() {
		try {
			service.chainNameAlreadyExist(getTestSSOToken(), null, A_CHAIN);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testChainNameAlreadyExistNullChain() {
		try {
			service.chainNameAlreadyExist(getTestSSOToken(), TEST_REALM, null);
			Assert.fail("If realm param is null, method must raise IllegalArgumentException.");
		} catch (IllegalArgumentException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If realm param is null, method must raise IllegalArgumentException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testChainNameAlreadyExistWrongRealm() {
		try {
			service.chainNameAlreadyExist(getTestSSOToken(), WRONG_REALM, A_CHAIN);
			Assert.fail(String.format("If the realm is wrong, method must raise AuthenticationException."));
		} catch (AuthenticationException e) {

		} catch (Exception e) {
			Assert.fail(String
					.format("If the realm is wrong, method must raise AuthenticationException. The actual exception raised is %s",
							e.toString()));
		}
	}

	@Test
	public void testChainNameAlreadyExistExists() {
		service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

		boolean exists = service.chainNameAlreadyExist(getTestSSOToken(), TEST_REALM, A_CHAIN);
		Assert.assertTrue("chain must be there", exists);

	}

	@Test
	public void testChainNameAlreadyExistNotExists() {
		service.addNewChain(getTestSSOToken(), TEST_REALM, A_CHAIN);

		boolean exists = service.chainNameAlreadyExist(getTestSSOToken(), TEST_REALM, A_CHAIN + "blah");
		Assert.assertFalse("chain must not be there", exists);

	}

}
