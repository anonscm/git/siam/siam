/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service.impl.test;

import org.osiam.frontend.common.service.OpenSSOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.iplanet.sso.SSOToken;

@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public abstract class BaseOSIAMTest extends AbstractJUnit4SpringContextTests {
	
	public static final String TEST_REALM = "/test";
	
	public static final String WRONG_REALM = "_this_is_a_realm_that_hoppefully_nobody_will_ever_create_in_the_test_machine";
	
	public static final String WRONG_CHAIN = "_this_is_a_chain_that_hoppefully_nobody_will_ever_create_in_the_test_machine";
	
	public static final String A_CHAIN = "aChain";
	
	@Autowired
	private OpenSSOService openSSOService;
	
	private SSOToken testToken;

	protected SSOToken getTestSSOToken() {
		if (testToken == null) {
			testToken = openSSOService.authenticate("/");
		}
		return testToken;
	}

}
