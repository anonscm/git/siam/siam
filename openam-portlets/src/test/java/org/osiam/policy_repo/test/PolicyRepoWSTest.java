/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.policy_repo.test;

import oasis.names.tc.xacml._2_0.policy.schema.os.ActionMatchType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeDesignatorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeSelectorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeValueType;
import oasis.names.tc.xacml._2_0.policy.schema.os.DefaultsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.EffectType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ObligationType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ObligationsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;
import oasis.names.tc.xacml._2_0.policy.schema.os.TargetType;

import org.junit.Test;
import org.osiam.policy_repo.PolicySetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = { "classpath:applicationContextWS.xml" })
public class PolicyRepoWSTest extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	private PolicySetService pss;
	
	@Test
	public void testAdd() {
		PolicySetType pst = new PolicySetType();
		pst.setDescription("descriere");

		ObligationsType ots = new ObligationsType();

		ObligationType ot = new ObligationType();
		ot.setObligationId("otId");
		ot.setFulfillOn(EffectType.PERMIT);

		ots.getObligation().add(ot);

		pst.setObligations(ots);

		pst.setPolicyCombiningAlgId("combinId");

		DefaultsType dt = new DefaultsType();
		dt.setXPathVersion("xpath version");
		
		pst.setPolicySetDefaults(dt);

		pst.setPolicySetId("psId");

		TargetType tt = new TargetType();

		ActionsType ats = new ActionsType();
		
		ActionType at = new ActionType();
		
		ActionMatchType amt = new ActionMatchType();
		
		AttributeDesignatorType adt = new AttributeDesignatorType();
		adt.setAttributeId("attributeDesignatorId");
		adt.setDataType("dataType");
		adt.setIssuer("issuer");
		adt.setMustBePresent(true);
		amt.setActionAttributeDesignator(adt);
		
		AttributeSelectorType ast = new AttributeSelectorType();
		ast.setDataType("data type");
		ast.setMustBePresent(false);
		ast.setRequestContextPath("/");
		amt.setAttributeSelector(ast);
		
		AttributeValueType avt = new AttributeValueType();
		avt.setDataType("datatype");		
		amt.setAttributeValue(avt);
		
		amt.setMatchId("matchId");
		
		at.getActionMatch().add(amt);
		
		
		ats.getAction().add(at);
		tt.setActions(ats);
		pst.setTarget(tt);

		pst.setVersion("version");

		pss.addPolicySet("/", pst);
	}

}
