<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="datastoresUrl"></portlet:renderURL>
		<a href="${datastoresUrl}"><spring:message code="breadcrumb.datastores" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.ldapv3_for_adam" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="dataStoreAtt">
		<input type="hidden" name="ctx" value="LDAPv3ForADAMController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="name" value="${dataStoreAtt.name}" />
		
		<div class="amDivButtons"> 
			<input type="submit" name="save"  value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.active_directory_application_mode" /></h3>
		<div>
			<label>*<spring:message code="label.name" />:</label>
			${dataStoreAtt.name}
		</div>
		<div>
			<label for="loadSchemaWhenFinished"><spring:message code="label.load_schema_when_finished" />:</label>
			<input name="loadSchemaWhenFinished" type="checkbox" value="true" id="loadSchemaWhenFinished" 
				<c:choose>
					<c:when test="${dataStoreAtt.loadSchemaWhenFinished eq valueTrue}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> />
		</div>
		<hr>
		<div>
			<label class="amLabelTitle">*<spring:message code="label.ldap_server" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label><spring:message code="lable.current_values" /></label>
					<c:forEach var="ls" items="${dataStoreAtt.ldapServer}">
						<input type="hidden" name="ldapServer" value="${ls}" />
					</c:forEach>	
					<select name="ldapServerDeleteValues" multiple="multiple" >
						<c:forEach var="lsv" items="${dataStoreAtt.ldapServer}">
							<option value="${lsv}">${lsv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteLdapServer" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="ldapServerAddValue"><spring:message code="label.new_value" /></label>
					<input type="text" name="ldapServerAddValue" id="ldapServerAddValue">
					<input type="submit" name="addLdapServer" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.ldap_server" />
			</div>
			<form:errors path="ldapServer" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label for="ldapBindDn"><spring:message code="label.ldap_bind_dn" />:</label>
			<input name="ldapBindDn" type="text" value="${dataStoreAtt.ldapBindDn}" id="ldapBindDn">
			<div class="amNote">
				<spring:message code="note.ldap_bind_dn" />
			</div>		
		</div>
		<div>
			<label for="ldapBindPassword"><spring:message code="label.ldap_bind_password" />:</label>
			<input name="ldapBindPassword" type="password" value="${dataStoreAtt.ldapBindPassword}" id="ldapBindPassword">
		</div>
		<div>
			<label for="ldapBindPasswordConfirm"><spring:message code="label.ldap_bind_password_confirm" />:</label>
			<input name="ldapBindPasswordConfirm" type="password" value="${dataStoreAtt.ldapBindPasswordConfirm}" id="ldapBindPasswordConfirm">
			<form:errors path="ldapBindPasswordConfirm" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label for="ldapOrganizationDn">*<spring:message code="label.ldap_organization_dn" />:</label>
			<input name="ldapOrganizationDn" type="text" value="${dataStoreAtt.ldapOrganizationDn}" id="ldapOrganizationDn">
			<form:errors path="ldapOrganizationDn" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label><spring:message code="label.ldap_ssl_tls" />:</label>
			<input name="ldapSslTls" type="checkbox" value="true" id="ldapSslTls" 
				<c:choose>
					<c:when test="${dataStoreAtt.ldapSslTls eq valueTrue}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>  />
			<label class="amInputLabel" for="ldapSslTls">
				<spring:message code="label.enabled" />
			</label>
		</div>
		<div>
			<label for="ldapConnectionPoolMinimumSize"><spring:message code="label.ldap_connection_pool_minimum_size" />:</label>
			<input name="ldapConnectionPoolMinimumSize" type="text" value="${dataStoreAtt.ldapConnectionPoolMinimumSize}" id="ldapConnectionPoolMinimumSize">
			<form:errors path="ldapConnectionPoolMinimumSize" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label for="ldapConnectionPoolMaximumSize"><spring:message code="label.ldap_connection_pool_maximum_size" />:</label>
			<input name="ldapConnectionPoolMaximumSize" type="text" value="${dataStoreAtt.ldapConnectionPoolMaximumSize}" id="ldapConnectionPoolMaximumSize">
			<form:errors path="ldapConnectionPoolMaximumSize" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="maximumResultsReturnedFromSearch"><spring:message code="label.maximum_results_returned_from_search" />:</label>
			<input name="maximumResultsReturnedFromSearch" type="text" value="${dataStoreAtt.maximumResultsReturnedFromSearch}" id="maximumResultsReturnedFromSearch">
			<form:errors path="maximumResultsReturnedFromSearch" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label for="searchTimeout"><spring:message code="label.search_timeout" />:</label>
			<input name="searchTimeout" type="text" value="${dataStoreAtt.searchTimeout}" id="searchTimeout">
			<div class="amNote">
				<spring:message code="note.search_timeout" />
			</div>
			<form:errors path="searchTimeout" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="ldapFollowsReferral"><spring:message code="label.ldap_follows_referral" />:</label>
			<input name="ldapFollowsReferral" type="checkbox" value="true" id="ldapFollowsReferral" 
				<c:choose>
					<c:when test="${dataStoreAtt.ldapFollowsReferral eq valueTrue}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> />
		</div>
		<div>
			<label for="ldapv3RepositoryPlugInClassName">*<spring:message code="label.ldapv3_repository_plug_in_class_name" />:</label>
			<input name="ldapv3RepositoryPlugInClassName" type="text" value="${dataStoreAtt.ldapv3RepositoryPlugInClassName}" id="ldapv3RepositoryPlugInClassName">
			<form:errors path="ldapv3RepositoryPlugInClassName" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.attribute_name_mapping" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeNameMappingDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="anm" items="${dataStoreAtt.attributeNameMapping}">
						<input type="hidden" name="attributeNameMapping" value="${anm}" />
					</c:forEach>	
					<select name="attributeNameMappingDeleteValues" multiple="multiple" id="attributeNameMappingDeleteValues">
						<c:forEach var="anmv" items="${dataStoreAtt.attributeNameMapping}">
							<option value="${anmv}">${anmv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteAttributeNameMapping" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="attributeNameMappingAddValue"><spring:message code="label.new_value" /></label>
					<input name="attributeNameMappingAddValue" type="text" id="attributeNameMappingAddValue">
					<input type="submit" name="addAttributeNameMapping" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldapv3_plug_in_supported_types_and_operations" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapv3PlugInSupportedTypesAndOperationsDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="pisto" items="${dataStoreAtt.ldapv3PlugInSupportedTypesAndOperations}">
						<input type="hidden" name="ldapv3PlugInSupportedTypesAndOperations" value="${pisto}" />
					</c:forEach>	
					<select name="ldapv3PlugInSupportedTypesAndOperationsDeleteValues" multiple="multiple" id="ldapv3PlugInSupportedTypesAndOperationsDeleteValues">
						<c:forEach var="pistov" items="${dataStoreAtt.ldapv3PlugInSupportedTypesAndOperations}">
							<option value="${pistov}">${pistov}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteLdapPlugInSupportedTypesOperations" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="ldapv3PlugInSupportedTypesAndOperationsAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapv3PlugInSupportedTypesAndOperationsAddValue" type="text" id="ldapv3PlugInSupportedTypesAndOperationsAddValue">
					<input type="submit" name="addLdapPlugInSupportedTypesOperations" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label><spring:message code="label.ldapv3_plug_in_search_scope" />:</label>
			<div class="amFormMultipleInputs">
				<input type="radio" name="ldapv3PlugInSearchScope" value="SCOPE_BASE" id="ldapv3PlugInSearchScopeBase" 
					<c:choose>
						<c:when test="${dataStoreAtt.ldapv3PlugInSearchScope eq 'SCOPE_BASE'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="ldapv3PlugInSearchScopeBase"><spring:message code="label.scope_base" /></label>
				<br>
				<input type="radio" name="ldapv3PlugInSearchScope" value="SCOPE_ONE" id="ldapv3PlugInSearchScopeOne" 
					<c:choose>
						<c:when test="${dataStoreAtt.ldapv3PlugInSearchScope eq 'SCOPE_ONE'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="ldapv3PlugInSearchScopeOne"><spring:message code="label.scope_one" /></label>
				<br>
				<input type="radio" name="ldapv3PlugInSearchScope" value="SCOPE_SUB" id="ldapv3PlugInSearchScopeSub"
					<c:choose>
						<c:when test="${dataStoreAtt.ldapv3PlugInSearchScope eq 'SCOPE_SUB'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="ldapv3PlugInSearchScopeSub"><spring:message code="label.scope_sub" /></label>
			</div>
		</div>
		<div>
			<label for="ldapUsersSearchAttribute"><spring:message code="label.ldap_users_search_attribute" />:</label>
			<input name="ldapUsersSearchAttribute" type="text" value="${dataStoreAtt.ldapUsersSearchAttribute}" id="ldapUsersSearchAttribute">
		</div>
		<div>
			<label for="ldapUsersSearchFilter"><spring:message code="label.ldap_users_search_filter" />:</label>
			<input name="ldapUsersSearchFilter" type="text" value="${dataStoreAtt.ldapUsersSearchFilter}" id="ldapUsersSearchFilter">
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldap_user_object_class" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapUserObjectClassDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="luoc" items="${dataStoreAtt.ldapUserObjectClass}">
						<input type="hidden" name="ldapUserObjectClass" value="${luoc}" />
					</c:forEach>	
					<select name="ldapUserObjectClassDeleteValues" multiple="multiple" id="ldapUserObjectClassDeleteValues">
						<c:forEach var="luocv" items="${dataStoreAtt.ldapUserObjectClass}">
							<option value="${luocv}">${luocv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteLdapUserObjectClass" value="<spring:message code="action.delete" />">	
				</div>
				<div>
					<label for="ldapUserObjectClassAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapUserObjectClassAddValue" type="text" id="ldapUserObjectClassAddValue">
					<input type="submit" name="addLdapUserObjectClass" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldap_user_attributes" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapUserAttributesDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="lua" items="${dataStoreAtt.ldapUserAttributes}">
						<input type="hidden" name="ldapUserAttributes" value="${lua}" />
					</c:forEach>	
					<select name="ldapUserAttributesDeleteValues" multiple="multiple" id="ldapUserAttributesDeleteValues">
						<c:forEach var="luav" items="${dataStoreAtt.ldapUserAttributes}">
							<option value="${luav}">${luav}</option>
						</c:forEach>
						<option>________________________</option>
					</select>	
					<input type="submit" name="deleteLdapUserAttributes" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="ldapUserAttributesAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapUserAttributesAddValue" type="text" id="ldapUserAttributesAddValue">
					<input type="submit" name="addLdapUserAttributes" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.create_user_attribute_mapping" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="createUserAttributeMappingDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="cuam" items="${dataStoreAtt.createUserAttributeMapping}">
						<input type="hidden" name="createUserAttributeMapping" value="${cuam}" />
					</c:forEach>	
					<select name="createUserAttributeMappingDeleteValues" multiple="multiple" id="createUserAttributeMappingDeleteValues">
						<c:forEach var="cuamv" items="${dataStoreAtt.createUserAttributeMapping}">
							<option value="${cuamv}">${cuamv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>	
					<input type="submit" name="deleteCreateUserAttributeMapping" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="createUserAttributeMappingAddValue"><spring:message code="label.new_value" /></label>
					<input name="createUserAttributeMappingAddValue" type="text" id="createUserAttributeMappingAddValue">
					<input type="submit" name="addCreateUserAttributeMapping" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.create_user_attribute_mapping" />
			</div>
		</div>
		<div>
			<label for="attributeNameOfUserStatus"><spring:message code="label.attribute_name_of_user_status" />:</label>
			<input name="attributeNameOfUserStatus" type="text" value="${dataStoreAtt.attributeNameOfUserStatus}" id="attributeNameOfUserStatus">
		</div>
		<div>
			<label for="userStatusActiveValue"><spring:message code="label.user_status_active_value" />:</label>
			<input name="userStatusActiveValue" type="text" value="${dataStoreAtt.userStatusActiveValue}" id="userStatusActiveValue">
		</div>
		<div>
			<label for="userStatusInactiveValue"><spring:message code="label.user_status_inactive_value" />:</label>
			<input name="userStatusInactiveValue" type="text" value="${dataStoreAtt.userStatusInactiveValue}" id="userStatusInactiveValue">
		</div>
		<div>
			<label for="ldapGroupsSearchAttribute"><spring:message code="label.ldap_groups_search_attribute" />:</label>
			<input name="ldapGroupsSearchAttribute" type="text" value="${dataStoreAtt.ldapGroupsSearchAttribute}" id="ldapGroupsSearchAttribute">
		</div>
		<div>
			<label for="ldapGroupsSearchFilter"><spring:message code="label.ldap_groups_search_filter" />:</label>
			<input name="ldapGroupsSearchFilter" type="text" value="${dataStoreAtt.ldapGroupsSearchFilter}" id="ldapGroupsSearchFilter">
		</div>
		<div>
			<label for="ldapGroupsContainerNamingAttribute"><spring:message code="label.ldap_groups_container_naming_attribute" />:</label>
			<input name="ldapGroupsContainerNamingAttribute" type="text" value="${dataStoreAtt.ldapGroupsContainerNamingAttribute}" id="ldapGroupsContainerNamingAttribute">
		</div>
		<div>
			<label for="ldapGroupsContainerValue"><spring:message code="label.ldap_groups_container_value" />:</label>
			<input name="ldapGroupsContainerValue" type="text" value="${dataStoreAtt.ldapGroupsContainerValue}" id="ldapGroupsContainerValue">
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldap_groups_object_class" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapGroupsObjectClassDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="lgoc" items="${dataStoreAtt.ldapGroupsObjectClass}">
						<input type="hidden" name="ldapGroupsObjectClass" value="${lgoc}" />
					</c:forEach>	
					<select name="ldapGroupsObjectClassDeleteValues" multiple="multiple" id="ldapGroupsObjectClassDeleteValues">
						<c:forEach var="lgocv" items="${dataStoreAtt.ldapGroupsObjectClass}">
							<option value="${lgocv}">${lgocv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>	
					<input type="submit" name="deleteLdapGroupsObjectClass" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="ldapGroupsObjectClassAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapGroupsObjectClassAddValue" type="text" id="ldapGroupsObjectClassAddValue">
					<input type="submit" name="addLdapGroupsObjectClass" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldap_groups_attributes" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapGroupsAttributesDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="lga" items="${dataStoreAtt.ldapGroupsAttributes}">
						<input type="hidden" name="ldapGroupsAttributes" value="${lga}" />
					</c:forEach>	
					<select name="ldapGroupsAttributesDeleteValues" multiple="multiple" id="ldapGroupsAttributesDeleteValues">
						<c:forEach var="lgav" items="${dataStoreAtt.ldapGroupsAttributes}">
							<option value="${lgav}">${lgav}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteLdapGroupsAttributes" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="ldapGroupsAttributesAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapGroupsAttributesAddValue" type="text" id="ldapGroupsAttributesAddValue">
					<input type="submit" name="addLdapGroupsAttributes" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label for="attributeNameForGroupMembership"><spring:message code="label.attribute_name_for_group_membership" />:</label>
			<input name="attributeNameForGroupMembership" type="text" value="${dataStoreAtt.attributeNameForGroupMembership}" id="attributeNameForGroupMembership">
		</div>
		<div>
			<label for="attributeNameOfUnqiueMember"><spring:message code="label.attribute_name_of_unqiue_member" />:</label>
			<input name="attributeNameOfUnqiueMember" type="text" value="${dataStoreAtt.attributeNameOfUnqiueMember}" id="attributeNameOfUnqiueMember">
		</div>
		<div>
			<label for="ldapPeopleContainerNamingAttribute"><spring:message code="label.ldap_people_container_naming_attribute" />:</label>
			<input name="ldapPeopleContainerNamingAttribute" type="text" value="${dataStoreAtt.ldapPeopleContainerNamingAttribute}" id="ldapPeopleContainerNamingAttribute">
		</div>
		<div>
			<label for="ldapPeopleContainerValue"><spring:message code="label.ldap_people_container_value" />:</label>
			<input name="ldapPeopleContainerValue" type="text" value="${dataStoreAtt.ldapPeopleContainerValue}" id="ldapPeopleContainerValue">
		</div>
		<div>
			<label><spring:message code="label.identity_types_that_can_be_authenticated" />:</label>
			<input name="identityTypesThatCanBeAuthenticated" type="checkbox" value="User" id="identityTypesThatCanBeAuthenticated" 
				<c:choose>
					<c:when test="${dataStoreAtt.identityTypesThatCanBeAuthenticated eq 'User'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="identityTypesThatCanBeAuthenticated">
				<spring:message code="label.identity_types_that_can_be_authenticated.user" />
			</label>
		</div>
		<div>
			<label for="authenticationNamingAttribute"><spring:message code="label.authentication_naming_attribute" />:</label>
			<input name="authenticationNamingAttribute" type="text" value="${dataStoreAtt.authenticationNamingAttribute}" id="authenticationNamingAttribute">
		</div>
		<div>
			<label for="persistentSearchBaseDn"><spring:message code="label.persistent_search_base_dn" />:</label>
			<input name="persistentSearchBaseDn" type="text" value="${dataStoreAtt.persistentSearchBaseDn}" id="persistentSearchBaseDn">
		</div>
		<div>
			<label for="persistentSearchFilter"><spring:message code="label.persistent_search_filter" />:</label>
			<input name="persistentSearchFilter" type="text" value="${dataStoreAtt.persistentSearchFilter}" id="persistentSearchFilter">  
		</div>
		<div>
			<label><spring:message code="label.persistent_search_scope" />:</label>
			<div class="amFormMultipleInputs">
				<input type="radio" name="persistentSearchScope" value="SCOPE_BASE" id="persistentSearchScopeBse" 
					<c:choose>
						<c:when test="${dataStoreAtt.persistentSearchScope eq 'SCOPE_BASE'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="persistentSearchScopeBse"><spring:message code="label.scope_base" /></label>
				<br>
				<input type="radio" name="persistentSearchScope" value="SCOPE_ONE" id="persistentSearchScopeOne" 
					<c:choose>
						<c:when test="${dataStoreAtt.persistentSearchScope eq 'SCOPE_ONE'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="persistentSearchScopeOne"><spring:message code="label.scope_one" /></label>
				<br>
				<input type="radio" name="persistentSearchScope" value="SCOPE_SUB" id="persistentSearchScopeSub" 
					<c:choose>
						<c:when test="${dataStoreAtt.persistentSearchScope eq 'SCOPE_SUB'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="persistentSearchScopeSub"><spring:message code="label.scope_sub" /></label>
			</div>
		</div>
		<div>
			<label for="persistentSearchMaximumIdleTimeBeforeRestart"><spring:message code="label.persistent_search_maximum_idle_time_before_restart" />:</label>
			<input name="persistentSearchMaximumIdleTimeBeforeRestart" type="text" value="${dataStoreAtt.persistentSearchMaximumIdleTimeBeforeRestart}" id="persistentSearchMaximumIdleTimeBeforeRestart">
			<div class="amNote">
				<spring:message code="note.persistent_search_maximum_idle_time_before_restart" />
			</div>		
			<form:errors path="persistentSearchMaximumIdleTimeBeforeRestart" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="maximumNumberOfRetriesAfterErrorCodes"><spring:message code="label.maximum_number_of_retries_after_error_codes" />:</label>
			<input name="maximumNumberOfRetriesAfterErrorCodes" type="text" value="${dataStoreAtt.maximumNumberOfRetriesAfterErrorCodes}" id="maximumNumberOfRetriesAfterErrorCodes">
			<form:errors path="maximumNumberOfRetriesAfterErrorCodes" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="theDelayTimeBetweenRetries"><spring:message code="label.the_delay_time_between_retries" />:</label>
			<input name="theDelayTimeBetweenRetries" type="text" value="${dataStoreAtt.theDelayTimeBetweenRetries}" id="theDelayTimeBetweenRetries">
			<div class="amNote">
				<spring:message code="note.the_delay_time_between_retries" />
			</div>		
			<form:errors path="theDelayTimeBetweenRetries" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.ldap_exception_error_codes_to_retry_on" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="ldapExceptionErrorCodesToRetryOnDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="leectro" items="${dataStoreAtt.ldapExceptionErrorCodesToRetryOn}">
						<input type="hidden" name="ldapExceptionErrorCodesToRetryOn" value="${leectro}" />
					</c:forEach>	
					<select name="ldapExceptionErrorCodesToRetryOnDeleteValues" multiple="multiple" id="ldapExceptionErrorCodesToRetryOnDeleteValues">
						<c:forEach var="leectrov" items="${dataStoreAtt.ldapExceptionErrorCodesToRetryOn}">
							<option value="${leectrov}">${leectrov}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteLdapExceptionErrorCodesToRetryOn" value="<spring:message code="action.delete" />">	
				</div>
				<div>
					<label for="ldapExceptionErrorCodesToRetryOnAddValue"><spring:message code="label.new_value" /></label>
					<input name="ldapExceptionErrorCodesToRetryOnAddValue" type="text" id="ldapExceptionErrorCodesToRetryOnAddValue">
					<input type="submit" name="addLdapExceptionErrorCodesToRetryOn" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label><spring:message code="label.caching" />:</label>
			<input name="caching" type="checkbox" value="true" id="caching" 
				<c:choose>
					<c:when test="${dataStoreAtt.caching eq valueTrue}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="caching">
				<spring:message code="label.enabled" />
			</label>
		</div>
		<div>
			<label for="maximumAgeOfCachedItems"><spring:message code="label.maximum_age_of_cached_items" />:</label>
			<input name="maximumAgeOfCachedItems" type="text" value="${dataStoreAtt.maximumAgeOfCachedItems}" id="maximumAgeOfCachedItems">
			<div class="amNote">
				<spring:message code="note.maximum_age_of_cached_items" />
			</div>		
			<form:errors path="maximumAgeOfCachedItems" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="maximumSizeOfTheCache"><spring:message code="label.maximum_size_of_the_cache" />:</label>
			<input name="maximumSizeOfTheCache" type="text" value="${dataStoreAtt.maximumSizeOfTheCache}" id="maximumSizeOfTheCache">
			<div class="amNote">
				<spring:message code="note.maximum_size_of_the_cache" />
			</div>	
			<form:errors path="maximumSizeOfTheCache" cssClass="portlet-msg-error"></form:errors>
		</div>
	</form:form>
</div>