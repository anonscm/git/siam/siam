<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="datastoresUrl"></portlet:renderURL>
		<a href="${datastoresUrl}"><spring:message code="breadcrumb.datastores" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.database" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="dataStoreAtt">
		<input type="hidden" name="ctx" value="DatabaseController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="name" value="${dataStoreAtt.name}" />
		
		<div class="amDivButtons"> 
			<input type="submit" name="save"  value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.database_repository_early_access" /></h3>
		<div>
			<label >*<spring:message code="label.store_name" />:</label>
			${dataStoreAtt.name}
		</div>
		<div>
			<label for="loadSchemaWhenFinished"><spring:message code="label.load_schema_when_finished" />:</label>
			<input name="loadSchemaWhenFinished" type="checkbox" value="true" id="loadSchemaWhenFinished" 
				<c:choose>
					<c:when test="${dataStoreAtt.loadSchemaWhenFinished eq valueTrue}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
		</div>
		<hr>
		
		<div>
			<label class="amLabelTitle"><spring:message code="label.attribute_name_mapping" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeNameMappingDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="anm" items="${dataStoreAtt.attributeNameMapping}">
						<input type="hidden" name="attributeNameMapping" value="${anm}" />
					</c:forEach>	
					<select name="attributeNameMappingDeleteValues" multiple="multiple" id="attributeNameMappingDeleteValues">
						<c:forEach var="anmv" items="${dataStoreAtt.attributeNameMapping}">
							<option value="${anmv}">${anmv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteAttributeNameMapping" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="attributeNameMappingAddValue"><spring:message code="label.new_value" /></label>
					<input type="text" name="attributeNameMappingAddValue" id="attributeNameMappingAddValue">
					<input type="submit" name="addAttributeNameMapping" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label for="databaseRepositoryPluginClassName">*<spring:message code="label.database_repository_plugin_class_name" />:</label>
			<input name="databaseRepositoryPluginClassName" type="text" value="${dataStoreAtt.databaseRepositoryPluginClassName}" id="databaseRepositoryPluginClassName">
			<form:errors path="databaseRepositoryPluginClassName" cssClass="portlet-msg-error"></form:errors>		
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.database_plug_in_supported_types_and_operations" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="databasePlugInSupportedTypesAndOperationsDeleteValues"><spring:message code="lable.current_values" /></label>	
					<c:forEach var="pistao" items="${dataStoreAtt.databasePlugInSupportedTypesAndOperations}">
						<input type="hidden" name="databasePlugInSupportedTypesAndOperations" value="${pistao}" />
					</c:forEach>	
					<select name="databasePlugInSupportedTypesAndOperationsDeleteValues" multiple="multiple" id="databasePlugInSupportedTypesAndOperationsDeleteValues" >
						<c:forEach var="pistaov" items="${dataStoreAtt.databasePlugInSupportedTypesAndOperations}">
							<option value="${pistaov}">${pistaov}</option>
						</c:forEach>
						<option>________________________</option>
					</select>	
					<input type="submit" name="deleteLdapPlugInSupportedTypesOperations" value="<spring:message code="action.delete" />">		
				</div>
				<div>
					<label for="databasePlugInSupportedTypesAndOperationsAddValue"><spring:message code="label.new_value" /></label>
					<input type="text" name="databasePlugInSupportedTypesAndOperationsAddValue" id="databasePlugInSupportedTypesAndOperationsAddValue">
					<input type="submit" name="addLdapPlugInSupportedTypesOperations" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
		<div>
			<label for="databaseDataAccessObjectPluginClassName">*<spring:message code="label.database_data_access_object_plugin_class_name" />:</label>	
			<input name="databaseDataAccessObjectPluginClassName" type="text" value="${dataStoreAtt.databaseDataAccessObjectPluginClassName}" id="databaseDataAccessObjectPluginClassName">
			<form:errors path="databaseDataAccessObjectPluginClassName" cssClass="portlet-msg-error"></form:errors>		
		</div>
		<div>
			<label><spring:message code="label.connection_type" />:</label>
			<div class="amFormMultipleInputs">
				<input name="connectionType" type="radio" value="JDBC" id="connectionTypeJDBC"
					<c:choose>
						<c:when test="${dataStoreAtt.connectionType eq 'JDBC'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="connectionTypeJDBC">
					<spring:message code="label.connection_type.connection_is_retrieved_via_programmatic_connection" />
				</label>
				<br>
				<input name="connectionType" type="radio" value="JNDI" id="connectionTypeJNDI"
					<c:choose>
						<c:when test="${dataStoreAtt.connectionType eq 'JNDI'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="connectionTypeJNDI">
					<spring:message code="label.connection_type.connection_pool_is_retrieved_via_jndi_datasource" />
				</label>	
			</div>
		</div>
		<div>
			<label for="databaseDatasourceName">*<spring:message code="label.database_datasource_name" />:</label>
			<input name="databaseDatasourceName" type="text" value="${dataStoreAtt.databaseDatasourceName}" id="databaseDatasourceName">
			<div class="amNote">
				<spring:message code="note.database_datasource_name" />
			</div>
			<form:errors path="databaseDatasourceName" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="jdbcDriverClassName"><spring:message code="label.jdbc_driver_class_name" />:</label>
			<input name="jdbcDriverClassName" type="text" value="${dataStoreAtt.jdbcDriverClassName}" id="jdbcDriverClassName"> 
			<div class="amNote">
				<spring:message code="note.jdbc_driver_class_name" />
			</div>
		</div>
		<div>
			<label for="passwordForConnectingToDatabase"><spring:message code="label.password_for_connecting_to_database" />:</label>
			<input name="passwordForConnectingToDatabase" type="password" value="${dataStoreAtt.passwordForConnectingToDatabase}" id="passwordForConnectingToDatabase">
			<div class="amNote">
				<spring:message code="note.password_for_connecting_to_database" />
			</div>
		</div>
		<div>
			<label for="passwordForConnectingToDatabaseConfirm"><spring:message code="label.password_for_onnecting_to_database_confirm" />:</label>
			<input name="passwordForConnectingToDatabaseConfirm" type="password" value="${dataStoreAtt.passwordForConnectingToDatabaseConfirm}" id="passwordForConnectingToDatabaseConfirm">
			<form:errors path="passwordForConnectingToDatabaseConfirm" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="jdbcDriverUrl"><spring:message code="label.jdbc_driver_url" />:</label>
			<input name="jdbcDriverUrl" type="text" value="${dataStoreAtt.jdbcDriverUrl}" id="jdbcDriverUrl">
			<div class="amNote">
				<spring:message code="note.jdbc_driver_url" />
			</div>
		</div>
		<div>
			<label for="connectThisUserToDatabase"><spring:message code="label.connect_this_user_to_database" />:</label>
			<input name="connectThisUserToDatabase" type="text" value="${dataStoreAtt.connectThisUserToDatabase}" id="connectThisUserToDatabase">
			<div class="amNote">
				<spring:message code="note.connect_this_user_to_database" />
			</div>
		</div>
		<div>
			<label for="databaseUserTableName">*<spring:message code="label.database_user_table_name" />:</label>
			<input name="databaseUserTableName" type="text" value="${dataStoreAtt.databaseUserTableName}" id="databaseUserTableName">
			<form:errors path="databaseUserTableName" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label class="amLabelTitle">*<spring:message code="label.list_of_user_attributes_names_in_database" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="listOfUserAttributesNamesInDatabaseDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="louanin" items="${dataStoreAtt.listOfUserAttributesNamesInDatabase}">
						<input type="hidden" name="listOfUserAttributesNamesInDatabase" value="${louanin}" />
					</c:forEach>	
					<select name="listOfUserAttributesNamesInDatabaseDeleteValues" multiple="multiple" id="listOfUserAttributesNamesInDatabaseDeleteValues">
						<c:forEach var="louaninv" items="${dataStoreAtt.listOfUserAttributesNamesInDatabase}">
							<option value="${louaninv}">${louaninv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteListOfUserAttributesNamesInDatabase" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="listOfUserAttributesNamesInDatabaseAddValue"><spring:message code="label.new_value" /></label>
					<input type="text" name="listOfUserAttributesNamesInDatabaseAddValue" id="listOfUserAttributesNamesInDatabaseAddValue">
					<input type="submit" name="addListOfUserAttributesNamesInDatabase" value="<spring:message code="action.add" />">
				</div>
			</div>
			<form:errors path="listOfUserAttributesNamesInDatabase" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="userPasswordAttributeName">*<spring:message code="label.user_password_attribute_name" />:</label>
			<input name="userPasswordAttributeName" type="text" value="${dataStoreAtt.userPasswordAttributeName}" id="userPasswordAttributeName">
			<form:errors path="userPasswordAttributeName" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label for="userIdAttributeName">*<spring:message code="label.user_id_attribute_name" />:</label>
			<input name="userIdAttributeName" type="text" value="${dataStoreAtt.userIdAttributeName}" id="userIdAttributeName">
			<div class="amNote">
				<spring:message code="note.user_id_attribute_name" />
			</div>
			<form:errors path="userIdAttributeName" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="attributeNameOfUserStatus">*<spring:message code="label.attribute_name_of_user_status" />:</label>
			<input name="attributeNameOfUserStatus" type="text" value="${dataStoreAtt.attributeNameOfUserStatus}" id="attributeNameOfUserStatus">
			<div class="amNote">
				<spring:message code="note.attribute_name_of_user_status" />
			</div>	
			<form:errors path="attributeNameOfUserStatus" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="userStatusActiveValue">*<spring:message code="label.user_status_active_value" />:</label>
			<input name="userStatusActiveValue" type="text" value="${dataStoreAtt.userStatusActiveValue}" id="userStatusActiveValue">
			<div class="amNote">
				<spring:message code="note.user_status_active_value" />
			</div>
			<form:errors path="userStatusActiveValue" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="userStatusInActiveValue"><spring:message code="label.user_status_in_active_value" />:</label>
			<input name="userStatusInActiveValue" type="text" value="${dataStoreAtt.userStatusInActiveValue}" id="userStatusInActiveValue">
			<div class="amNote">
				<spring:message code="note.user_status_in_active_value" />
			</div>		
			<form:errors path="userStatusInActiveValue" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="maximumResultsReturnedFromSearch"><spring:message code="label.maximum_results_returned_from_search" />:</label>
			<input name="maximumResultsReturnedFromSearch" type="text" value="${dataStoreAtt.maximumResultsReturnedFromSearch}" id="maximumResultsReturnedFromSearch">
			<div class="amNote">
				<spring:message code="note.maximum_results_returned_from_search" />
			</div>
			<form:errors path="maximumResultsReturnedFromSearch" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="usersSearchAttributeInDatabase"><spring:message code="label.users_search_attribute_in_database" />:</label>
			<input name="usersSearchAttributeInDatabase" type="text" value="${dataStoreAtt.usersSearchAttributeInDatabase}" id="usersSearchAttributeInDatabase">
			<div class="amNote">
				<spring:message code="note.users_search_attribute_in_database" />
			</div>
		</div>
		<div>
			<label for="databaseMembershipTableName"><spring:message code="label.database_membership_table_name" />:</label>
			<input name="databaseMembershipTableName" type="text" value="${dataStoreAtt.databaseMembershipTableName}" id="databaseMembershipTableName">
		</div>
		<div>
			<label for="membershipIdAttributeName">*<spring:message code="label.membership_id_attribute_name" />:</label>
			<input name="membershipIdAttributeName" type="text" value="${dataStoreAtt.membershipIdAttributeName}" id="membershipIdAttributeName">
			<div class="amNote">
				<spring:message code="note.membership_id_attribute_name" />
			</div>		
			<form:errors path="membershipIdAttributeName" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="membershipSearchAttributeInDatabase"><spring:message code="label.membership_search_attribute_in_database" />:</label>
			<input name="membershipSearchAttributeInDatabase" type="text" value="${dataStoreAtt.membershipSearchAttributeInDatabase}" id="membershipSearchAttributeInDatabase">
			<div class="amNote">
				<spring:message code="note.membership_search_attribute_in_database" />
			</div>
		</div>
	</form:form>	

</div>
