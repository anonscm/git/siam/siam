<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.DataStoresConstants"%>
<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<spring:message code="breadcrumb.datastores" />
	</div>

	<c:set var="typeLdapForAd" value="<%= DataStoresConstants.STORE_TYPE_LDAP_FOR_AD %>"/>
	<c:set var="typeLdapForAdam" value="<%= DataStoresConstants.STORE_TYPE_LDAP_FOR_ADAM %>"/>
	<c:set var="typeDatabase" value="<%= DataStoresConstants.STORE_TYPE_DATABASE %>"/>
	<c:set var="typeLdapForOpenDs" value="<%= DataStoresConstants.STORE_TYPE_LDAP_FOR_OPEN_DS %>"/>
	<c:set var="typeLdapForAmds" value="<%= DataStoresConstants.STORE_TYPE_LDAP_FOR_AMDS %>"/>
	<c:set var="typeLdapForTivoli" value="<%= DataStoresConstants.STORE_TYPE_LDAP_FOR_TIVOLI %>"/>
	
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="entity">
		<input type="hidden" name="action" value="doCreateDataStore" />
		
		<h3> <spring:message code="title.new_data_store" /></h3>
		<div class="amFormCell">
			<label for="name"><spring:message code="label.store_name" />:</label>
			<input type="text" name="name" id="name">
		</div>
		<div class="amFormCell">
			<label for="type"><spring:message code="label.type" />:</label>
			<select name="type" id="type">
				<option value="${typeLdapForAd}"><spring:message code="label.data_store_type.${typeLdapForAd}" /></option>
				<option value="${typeLdapForAdam}"><spring:message code="label.data_store_type.${typeLdapForAdam}" /></option>
				<option value="${typeDatabase}"><spring:message code="label.data_store_type.${typeDatabase}" /></option>
				<option value="${typeLdapForOpenDs}"><spring:message code="label.data_store_type.${typeLdapForOpenDs}" /></option>
				<option value="${typeLdapForAmds}"><spring:message code="label.data_store_type.${typeLdapForAmds}" /></option>
				<option value="${typeLdapForTivoli}"><spring:message code="label.data_store_type.${typeLdapForTivoli}" /></option>
			</select>
		</div>
		<input type="submit" value="<spring:message code="action.create" />">
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		<form:errors path="type" cssClass="portlet-msg-error"></form:errors>
	</form:form>

	<h3> <spring:message code="title.configured_data_stores" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form action="${formAction}" id="${ns}pageForm_deleteDataStores" method="post">
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllDataStores eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllDataStores" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllDataStores" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th ><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteDataStores" value="<spring:message code="action.delete_selected"/>" />
					</div>
				</th>
			</tr>
			<c:forEach var="ds" items="${dataStoresList}">
				<tr>
					<td class="amInputCell">
			   			<input type="checkbox" name="deleteDataStoresList" value="${ds.name}"
				   			<c:choose>
								<c:when test="${checkAllDataStores eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
			   		</td>
			   		<td><spring:message code="label.data_store_type.${ds.type}" /></td>
					<td>
						<portlet:renderURL var="editDataStoreURL">
							<portlet:param name="ctx" value="${ds.type}Controller"/>
							<portlet:param name="name" value="${ds.name}"/>
							<portlet:param name="action" value="<%= CommonConstants.ACTION_UPDATE %>"/>
						</portlet:renderURL>
						<a href="${editDataStoreURL}">${ds.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</form>				
</div>
