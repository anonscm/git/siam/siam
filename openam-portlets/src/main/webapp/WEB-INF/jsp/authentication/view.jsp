<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<div class="amFrontendCSSWrapper">
	<div class="amBreadcrumb">
		<spring:message code="breadcrumb.authentication" />
	</div>

	<h3><spring:message code="title.new_chain"/></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_saveChain" method="post" commandName="chain">
		<input type="hidden" name="action" value="doSaveChain" />
		<div>
			<label for="name"><spring:message code="label.chain_name"/>:</label>
			<input type="text" name="name" id="name"> 
			<input type="submit" value="<spring:message code="action.create"/>">
			<form:errors path="name" cssClass="portlet-msg-error"/>
		</div>
	</form:form>

	<h3><spring:message code="title.configured_chain" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_updateConfigChain" method="post">
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllChains eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllChains" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllChains" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.org_auth"/></th>
				<th><spring:message code="label.admin_auth"/></th>
				<th>
					<spring:message code="label.name"/>
					<div class="amDivButtons">
						<input type="submit" name="updateConfigChain" value="<spring:message code="action.save"/>">
						<input type="reset" value="<spring:message code="action.reset"/>">
						<input type="submit" name="deleteChain" value="<spring:message code="action.delete_selected"/>"/>
					</div>
				</th>
			</tr>
			<c:forEach var="chain" items="${chainNamesList}">
				<tr>
					<td class="amInputCell">
			   			<input type="checkbox" name="deleteChainList" value="${chain}"
				   			<c:choose>
								<c:when test="${checkAllChains eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
			   		</td>
			   		<td class="amInputCell">
						<input type="radio" name="orgAuth" value="${chain}" 
							<c:choose>
								<c:when test="${orgAuth eq chain}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td class="amInputCell">
						<input type="radio" name="adminAuth" value="${chain}"
							<c:choose>
								<c:when test="${adminAuth eq chain}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td>
						<portlet:renderURL var="editChainURL">
							<portlet:param name="ctx" value="ChainPropertiesController"/>
							<portlet:param name="name" value="${chain}"/>
							<portlet:param name="action" value="<%= CommonConstants.ACTION_UPDATE %>"/>
						</portlet:renderURL>
						<a href="${editChainURL}">${chain}</a>
					</td>
				</tr>
			</c:forEach>
		</table>	
	</form:form>
</div>