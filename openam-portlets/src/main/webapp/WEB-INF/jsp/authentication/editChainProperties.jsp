<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<div class="amFrontendCSSWrapper">
	
	<div class="amBreadcrumb">
		<portlet:renderURL var="authenticationUrl"></portlet:renderURL>
		<a href="${authenticationUrl}">
			<spring:message code="breadcrumb.authentication" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.chain_properties" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="chain">
		<input type="hidden" name="ctx" value="ChainPropertiesController" />
		<input type="hidden" name="name" value="${chain.name}" />
		<input type="hidden" name="action" value="${actionValue}" />
	
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	
		<h3> ${chain.name} - <spring:message code="title.chain" /></h3>	
	
		<h3> <spring:message code="title.new_properties"/> </h3>
		<div class="amFormCell">
			<label for="instance"><spring:message code="label.instance"/>:</label>
			<select name="instance" id="instance">
				<c:forEach var="instance" items="${instanceList}">
					<option value="${instance}">${instance}</option>
				</c:forEach>
			</select>
		</div>
		<div class="amFormCell">
			<label for="criteria"><spring:message code="label.criteria"/>:</label>
			<select name="criteria" id="criteria">
				<c:forEach var="criteria" items="${criteriaList}">
					<option value="${criteria}">${criteria}</option>
				</c:forEach>
			</select>
		</div>
		<div class="amFormCell">
			<label for="options"><spring:message code="label.options"/>:</label>
			<input type="text" name="options" id="options">
		</div>
		<input type="submit" name="addChainProperties" value="<spring:message code="action.add"/>">
	
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllProperties eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllProperties" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllProperties" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.instance"/></th>
				<th><spring:message code="label.criteria"/></th>
				<th><spring:message code="label.options"/></th>
				<th colspan="2">
					<div class="amDivButtons">
					 	<input type="submit" name="deleteProperties" value="<spring:message code="action.delete_selected"/>"/>
					</div>	
				</th>
			</tr>
			<c:forEach var="prop" items="${chain.propertiesList}" varStatus="status">
				<tr>
					<td class="amInputCell">
			   			<input type="checkbox" name="deletePropertiesList" value="${status.index}"
				   			<c:choose>
								<c:when test="${checkAllProperties eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
			   		</td>
			   		<td>
						<c:set var="currentInstance" value="${prop.instance}"/>
						<select name="propertiesList[${status.index}].instance">
							<c:forEach var="instance" items="${instanceList}">
								<option value="${instance}"
									<c:choose>
										<c:when test="${currentInstance eq instance}">
											selected="selected"
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${instance}</option>
							</c:forEach>
						</select>	
					</td>
					<td>
						<c:set var="currentCriteria" value="${prop.criteria}"/>
						<select name="propertiesList[${status.index}].criteria">
							<c:forEach var="criteria" items="${criteriaList}">
								<option value="${criteria}"
									<c:choose>
										<c:when test="${currentCriteria eq criteria}">
											selected="selected"
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${criteria}</option>
							</c:forEach>
						</select>	
					</td>
					<td>
						<input type="text" name="propertiesList[${status.index}].options" value="${prop.options}">
					</td>
					<td class="amInputCell">
						<c:if test="${!status.first}">
							<input type="submit" name="up_${status.index}" value="<spring:message code="action.up" />"/>
						</c:if>	
					</td>
					<td class="amInputCell">
						<c:if test="${!status.last}">
							<input type="submit" name="down_${status.index}" value="<spring:message code="action.down" />"/>
						</c:if>	
					</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>
</div>
