<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_xacml_policy_decision_point" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="xacmlPdp">
		<input type="hidden" name="ctx" value="samlv2xacml_policy_decision_pointController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="${xacmlPdp.metaAlias}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>  - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
		
		<div>
			<label><spring:message code="label.protocol_support_enumeration" />:</label>
			${xacmlPdp.protocolSupportEnumeration}
			<div class="amNote">
				<spring:message code="note.protocol_support_enumeration" />
			</div>
		</div>
		<div>
			<label for="signingCertificateAlias"><spring:message code="label.signing_key_alias" />:</label>
			<input type="text" name="signingCertificateAlias" value="${xacmlPdp.signingCertificateAlias}" id="signingCertificateAlias">
			<div class="amNote">
				<spring:message code="note.signing_key_alias" />
			</div>
		</div>
		<div>
			<label for="encryptionCertificateAlias"><spring:message code="label.encryption_key_alias" />:</label>
			<input type="text" name="encryptionCertificateAlias" value="${xacmlPdp.encryptionCertificateAlias}" id="encryptionCertificateAlias">
			<div class="amNote">
				<spring:message code="note.encryption_key_alias" />
			</div>
		</div >
		<div>
			<label class="amLabelTitle"><spring:message code="label.basic_suthorization" /></label>
			<div class="amNote">
				<spring:message code="note.basic_suthorization" />
			</div>
			<div class="amFormMultipleValues">
				<div>
					<label for="basicAuthorizationEnabled"><spring:message code="label.enabled" />:</label>
					<input type="checkbox" value="true" name="basicAuthorizationEnabled" id="basicAuthorizationEnabled" 
					<c:choose>
						<c:when test="${xacmlPdp.basicAuthorizationEnabled eq 'true'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				</div>
				<div>
					<label for="basicAuthorizationUser"><spring:message code="label.user_name" />:</label>
					<input type="text" name="basicAuthorizationUser" value="${xacmlPdp.basicAuthorizationUser}" id="basicAuthorizationUser">
				</div>
				<div>
					<label for="basicAuthorizationPassword"><spring:message code="label.password" />:</label>
					<input type="text" name="basicAuthorizationPassword" value="${xacmlPdp.basicAuthorizationPassword}" id="basicAuthorizationPassword">
				</div>
			</div>
		</div>
		<div>
			<label><spring:message code="label.authorization_decision_query_signed" />:</label>
			<input type="checkbox" value="true" name="authorizationDecisionQuerySigned" id="authorizationDecisionQuerySigned" 
				<c:choose>
					<c:when test="${xacmlPdp.authorizationDecisionQuerySigned eq 'true'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="authorizationDecisionQuerySigned"><spring:message code="label.signed" /></label>
			<div class="amNote">
				<spring:message code="note.authorization_decision_response_signed" />
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="title.authorization_service" />:</label>
			<div class="amNote">
				<spring:message code="note.assertion_encrypted" />
			</div>
			<div class="amFormMultipleValues">
				<div>
					<label><spring:message code="label.binding" />:</label>
					${xacmlPdp.authorizationServiceBinding}
					<input type="hidden" name="authorizationServiceBinding" value="${xacmlPdp.authorizationServiceBinding}">
				</div>
				<div>
					<label for="authorizationServiceLocation"><spring:message code="label.location" />:</label>
					<input type="text" name="authorizationServiceLocation" value="${xacmlPdp.authorizationServiceLocation}" id="authorizationServiceLocation">
				</div>
				
			</div>
		</div>
	
	</form:form>
		
</div>
		