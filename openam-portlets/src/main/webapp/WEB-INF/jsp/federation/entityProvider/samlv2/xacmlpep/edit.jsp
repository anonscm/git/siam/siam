<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_xacml_policy_enforcement_point" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="xacmlPep">
		<input type="hidden" name="ctx" value="samlv2xacml_policy_enforcement_pointController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="${xacmlPep.metaAlias}">
		<input type="hidden" name="base64SigningCertificate" value="${xacmlPep.base64SigningCertificate}">
		<input type="hidden" name="base64EncryptionCertificate" value="${xacmlPep.base64EncryptionCertificate}">
		<input type="hidden" name="algorithm" value="${xacmlPep.algorithm}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>  - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
		
		<div>
			<label ><spring:message code="label.protocol_support_enumeration" />:</label>
			${xacmlPep.protocolSupportEnumeration}
			<input type="hidden" name="protocolSupportEnumeration" value="${xacmlPep.protocolSupportEnumeration}">
			<div class="amNote">
				<spring:message code="note.protocol_support_enumeration_pep" />
			</div>
			<c:set var="remoteLocation"><%=FederationConstants.REMOTE_VALUE%></c:set>
			<div class="amFormMultipleValues">
				<div>
					<label for="signingCertificateAlias"><spring:message code="label.signing_key_alias" />:</label>
						<c:choose>
							<c:when test="${entityLocation eq remoteLocation}">
								${xacmlPep.signingCertificateAlias}
							</c:when>
							<c:otherwise>
								<input type="text"  name="signingCertificateAlias" value="${xacmlPep.signingCertificateAlias}" id="signingCertificateAlias">
							</c:otherwise>
						</c:choose>
						
					
					<div class="amNote">
						<spring:message code="note.signing_key_alias" />
					</div> 
				</div>
				<div>
					<label for="encryptionCertificateAlias"><spring:message code="label.encryption_key_alias" />:</label>
					<c:choose>
						<c:when test="${entityLocation eq remoteLocation}">
							${xacmlPep.encryptionCertificateAlias}
						</c:when>
						<c:otherwise>
							<input type="text"  name="encryptionCertificateAlias" value="${xacmlPep.encryptionCertificateAlias}" id="encryptionCertificateAlias">
						</c:otherwise>
					</c:choose>		
					
					<div class="amNote">
						<spring:message code="note.encryption_key_alias" />
					</div>
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.basic_suthorization" /></label>
			<div class="amNote">
				<spring:message code="note.basic_suthorization" />
			</div>
			<div class="amFormMultipleValues">
				<div>
					<label for="basicAuthorizationEnabled"><spring:message code="label.enabled" />:</label>
					<input type="checkbox" value="true" name="basicAuthorizationEnabled" id="basicAuthorizationEnabled" 
						<c:choose>
							<c:when test="${xacmlPep.basicAuthorizationEnabled eq 'true'}">checked="checked"</c:when>
							<c:otherwise></c:otherwise>
						</c:choose> >
				</div>
				<div>
					<label for="basicAuthorizationUser"><spring:message code="label.user_name" />:</label>
					<input type="text" name="basicAuthorizationUser" value="${xacmlPep.basicAuthorizationUser}" id="basicAuthorizationUser">
				</div>
				<div>
					<label for="basicAuthorizationPassword"><spring:message code="label.password" />:</label>
					<input type="text" name="basicAuthorizationPassword" value="${xacmlPep.basicAuthorizationPassword}" id="basicAuthorizationPassword">
				</div>
			</div>
		</div>
		<div>
			<label><spring:message code="label.authorization_decision_query_signed" />:</label>
			<input type="checkbox" value="true" name="authorizationDecisionResponseSigned" id="authorizationDecisionResponseSigned" 
				<c:choose>
					<c:when test="${xacmlPep.authorizationDecisionResponseSigned eq 'true'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="authorizationDecisionResponseSigned"><spring:message code="label.signed" /></label>
			<div class="amNote">
				<spring:message code="note.authorization_decision_query_signed" />
			</div>
		</div>
		<div>
			<label><spring:message code="label.assertion_encrypted" />:</label>
			<input type="checkbox" value="true" name="assertionEncrypted" id="assertionEncrypted" 
				<c:choose>
					<c:when test="${xacmlPep.assertionEncrypted eq 'true'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="assertionEncrypted"><spring:message code="label.yes" /></label>
			<div class="amNote">
				<spring:message code="note.assertion_encrypted.pep" />
			</div>
		</div>
	</form:form>

</div>				