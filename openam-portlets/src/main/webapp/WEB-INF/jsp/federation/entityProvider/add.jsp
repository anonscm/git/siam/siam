<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.add_entity_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal" />
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="eentityProviderService">
		<input type="hidden" name="ctx" value="EntityProviderAddController" />
		<input type="hidden" name="entityIdentifier" value="${entityProvider.entityIdentifier}" />
		<input type="hidden" name="protocol" value="${entityProvider.protocol}" />
		
		<c:if test="${not empty error}">
			<span class="portlet-msg-error"><spring:message code="${error}"/></span>
		</c:if>
		
		<div class="amDivButtons">
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="cancel" value="<spring:message code="action.cancel" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: 
			<spring:message code="label.${entityProvider.protocol}" />
			${entityProvider.entityIdentifier}
		</h3>
	
		<h3><spring:message code="title.add_service" /></h3>
		<div>
			<label for="metaAlias"><spring:message code="label.meta_alias" />:</label>
			<form:input path="metaAlias" id="metaAlias" /> 
			<form:errors path="metaAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label for="type"><spring:message code="label.service" />:</label>
			<form:select path="type" id="type">
				<c:forEach var="type" items="${serviceTypes}">
					<form:option value="${type}">
						<spring:message code="title.service.${type}" />
					</form:option>
				</c:forEach>
			</form:select>
		</div>
		<div>
			<label for="signingCertificateAlias"><spring:message code="label.signing_certificate_alias" /></label>
			<form:input path="signingCertificateAlias" id="signingCertificateAlias" />
			<form:errors path="signingCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label for="encryptionCertificateAlias"><spring:message code="label.encryption_certificate_alias" /></label>
			<form:input path="encryptionCertificateAlias" id="encryptionCertificateAlias"/>
			<form:errors path="encryptionCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label>&nbsp;</label>
			<input type="submit" name="addService" value="<spring:message code="action.add" />" />
		</div>
		
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllServices eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllServices" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllServices" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.service" /></th>
				<th>
					<spring:message code="label.meta_alias" />
					<div class="amDivButtons">
						<input type="submit" value="<spring:message code="action.delete_selected" />" name="deleteServices">
					</div>
				</th>
			</tr>
			<c:forEach var="service" items="${entityProvider.descriptorMap}">
				<tr>
					<td class="amInputCell">
						<input name="deleteServiceList" value="${service.value.type}" type="checkbox"
							<c:choose>
								<c:when test="${checkAllServices eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<input type="hidden" name="descriptorMap['${service.value.type}'].type" value="${entityProvider.descriptorMap[service.value.type].type}" /> 
						<input type="hidden" name="descriptorMap['${service.value.type}'].metaAlias" value="${entityProvider.descriptorMap[service.value.type].metaAlias}" />
						<input type="hidden" name="descriptorMap['${service.value.type}'].signingCertificateAlias" value="${entityProvider.descriptorMap[service.value.type].signingCertificateAlias}" /> 
						<input type="hidden" name="descriptorMap['${service.value.type}'].encryptionCertificateAlias" value="${entityProvider.descriptorMap[service.value.type].encryptionCertificateAlias}" />
					</td>
					<td><spring:message code="title.service.${service.value.type}" /></td>
					<td>${service.value.metaAlias}</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>

</div>