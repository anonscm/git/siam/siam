<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.import_entity_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal" />
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="importEntityProvider" enctype="multipart/form-data" >
		<input type="hidden" name="ctx" value="ImportEntityProviderController" />
		
		<c:if test="${not empty error}">
			<span class="portlet-msg-error"><spring:message code="error.invalid_xml"/> ${error}</span>
		</c:if>
		
		<div class="amDivButtons">
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="cancel" value="<spring:message code="action.cancel" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.import_entity_provider" /></h3>
		<div class="amNote">
			<spring:message code="note.import_entity_provider" /> 
		</div>
		
		<div>
			<label><spring:message code="label.realm_name" />:</label>
			${realm}
		</div>
		<div>
			<div class="amFormCell">
				<label><spring:message code="label.metadata.type" /> </label>
				<div>
					<input type="radio" name="metadataFileLocationType" value="URL" id="metadataFileLocationType_url" checked="checked">
					<label for="metadataFileLocationType_url"><%=FederationConstants.LOCATION_TYPE_URL %></label>
				</div>
				<div>
					<input type="radio" name="metadataFileLocationType" value="File" id="metadataFileLocationType_file" >
					<label for="metadataFileLocationType_file"><%=FederationConstants.LOCATION_TYPE_FILE %></label>
				</div>	
			</div>
			<div class="amFormCell">
				<label>*<spring:message code="label.metadata.location" />:</label>
				<div>
					<input type="text" name="metadataFileURLLocation">
				</div>
				<div>
					<input type="file" name="metadataFileLocation">
				</div>
			</div>
			<form:errors path="metadataFileLocationType"  cssClass="portlet-msg-error"/>
			<form:errors path="metadataFileURLLocation"  cssClass="portlet-msg-error"/>
			<form:errors path="metadataFileLocation"  cssClass="portlet-msg-error"/>
		</div>
		
		<div>
			<div class="amFormCell">
				<label> <spring:message code="label.extended_data.type" /> </label>
				<div>
					<input type="radio" name="extendedDataFileLocationType" value="URL" id="extendedDataFileLocationType_url"  checked="checked">
		 			<label for="extendedDataFileLocationType_url"><%=FederationConstants.LOCATION_TYPE_URL %></label>
				</div>
				<div>
					<input type="radio" name="extendedDataFileLocationType" value="File" id="extendedDataFileLocationType_file" >
		 			<label for="extendedDataFileLocationType_file"><%=FederationConstants.LOCATION_TYPE_FILE %></label>
	 			</div>
			</div>
			<div class="amFormCell">
				<label> <spring:message code="label.extended_data.location" /> :</label>
				<div>
				<input type="text" name="extendedDataFileURLLocation">
				</div>
				<div>
					<input type="file"" name="extendedDataFileLocation">
				</div>
			</div>
		</div>
	</form:form>

</div>