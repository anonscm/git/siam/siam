<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_idp_advanced" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2identity_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2IDPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2IDPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2IDPServicesURL">
					<portlet:param name="ctx" value="Samlv2IDPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2IDPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2IDPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><spring:message code="label.menu_left.assertion_content" /></a></li>
					<li><a href="${editSamlv2IDPAssertionProcessingURL}"><spring:message code="label.menu_left.assertion_processing" /></a></li>
					<li><a href="${editSamlv2IDPServicesURL}"><spring:message code="label.menu_left.services" /></a></li>
					<li><a href="${editSamlv2IDPAdvanceddURL}"><b><spring:message code="label.menu_left.advanced" /></b></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="idpAdvanced">
					<input type="hidden" name="ctx" value="Samlv2IDPAdvancedController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden"name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.sae_configuration" /></h3>
					<div>
						<label for="saeConfigurationIDPURL"><spring:message code="label.idp_url" />:</label>
						<input type="text" name="saeConfigurationIDPURL" value="${idpAdvanced.saeConfigurationIDPURL}" id="saeConfigurationIDPURL">
						<div class="amNote">
							<spring:message code="note.idp_url" /> 
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.application_security_configuration" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="item" items="${idpAdvanced.saeConfigurationApplicationSecurityConfigurationCurrentValues}">
									<input type="hidden" name="saeConfigurationApplicationSecurityConfigurationCurrentValues" value="${item}" />
								</c:forEach>	
								<select name="saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues" multiple="multiple" id="saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues">
									<c:forEach var="itemv" items="${idpAdvanced.saeConfigurationApplicationSecurityConfigurationCurrentValues}">
										<option value="${itemv}">${itemv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteSaeConfigurationApplicationSecurityConfigurationCurrentValues" value="<spring:message code="action.delete" />">
							</div>						
							<div>
								<label for="saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue" type="text" id="saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue">
								<input type="submit" name="addSaeConfigurationApplicationSecurityConfigurationCurrentValues" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.application_security_configuration" />
						</div>
					</div>
					<hr>
					
					<h3> <spring:message code="title.ecp_configuration" /></h3>
					<div>
						<label for="ecpConfigurationIDPSessionMapper"><spring:message code="label.idp_session_mapper" />:</label>
						<input type="text" name="ecpConfigurationIDPSessionMapper" value="${idpAdvanced.ecpConfigurationIDPSessionMapper}" id="ecpConfigurationIDPSessionMapper">
						<div class="amNote">
							<spring:message code="note.idp_session_mapper" /> 
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.session_synchronization" /></h3>
					<div>
						<label>&nbsp;</label>
						<input type="checkbox" name="sessionSynchronizationEnabled" value="true" id="sessionSynchronizationEnabled" 
							<c:choose>
								<c:when test="${idpAdvanced.sessionSynchronizationEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="sessionSynchronizationEnabled"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.session_synchronization" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.relay_state_url_list" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.relay_state_url_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="relayStateURLListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="item" items="${idpAdvanced.relayStateURLList}">
									<input type="hidden" name="relayStateURLList" value="${item}" />
								</c:forEach>	
								<select name="relayStateURLListDeleteValues" multiple="multiple" id="relayStateURLListDeleteValues">
									<c:forEach var="itemv" items="${idpAdvanced.relayStateURLList}">
										<option value="${itemv}">${itemv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteRelayStateURLList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="relayStateURLListAddValue"><spring:message code="label.new_value" /></label>
								<input name="relayStateURLListAddValue" type="text" id="relayStateURLListAddValue">
								<input type="submit" name="addRelayStateURLList" value="<spring:message code="action.add" />">
							</div>
						</div>
					</div>
					
				</form:form>
			</div>
		</div>
	</div>
</div>