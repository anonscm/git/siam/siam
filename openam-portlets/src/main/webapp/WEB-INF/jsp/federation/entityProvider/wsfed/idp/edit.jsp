<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.wsfed_identity_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="idp">
		<input type="hidden" name="ctx" value="wsfedidentity_providerController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="${idp.metaAlias}">
		<input type="hidden" name="encryptionCertificateAlias" value="${idp.encryptionCertificateAlias}">
		<input type="hidden" name="claimTypeUri" value="${idp.claimTypeUri}">
		<input type="hidden" name="assertionNotBeforeTimeSkew" value="${idp.assertionNotBeforeTimeSkew}">
		<input type="hidden" name="authnContextMapper" value="${idp.authnContextMapper}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %> - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
		
		<h3><spring:message code="title.signing_cert_alias" /></h3>
		<div>
			<label for="signingCertificateAlias"><spring:message code="label.alias" />:</label>
			<input type="text" name="signingCertificateAlias" value="${idp.signingCertificateAlias}" id="signingCertificateAlias">
			<form:errors path="signingCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<hr>
		
		<h3><spring:message code="title.claim_types" /></h3>
		<div>
			<label for="claimTypeDisplayName"><spring:message code="title.claim_types" />:</label>
			<select name="claimTypeDisplayName" id="claimTypeDisplayName">
				<c:forEach var="item" items="${typeList}">
					<option value="${item}"
				 	<c:choose>
						<c:when test="${idp.claimTypeDisplayName eq item }">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				 >${item}</option>
				</c:forEach>
			</select>
		</div>
		<hr>
		
		<h3><spring:message code="title.name_identifier_and_mappings" /></h3>
		<div>
			<label for="nameFormatID"><spring:message code="label.name_id_format" />:</label>
			<select name="nameFormatID" id="nameFormatID">
				<c:forEach var="item" items="${nameFormatList}">
					<option value="${item}"
				 	<c:choose>
						<c:when test="${idp.nameFormatID eq item }">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				 >${item}</option>
				</c:forEach>
			</select>
			<div class="amNote">
				<spring:message code="note.name_id_format" />
			</div>
		</div>
		<div>
			<label for="nameIDAttribute" ><spring:message code="label.name_id_attribute" />:</label>
			<input type="text" name="nameIDAttribute" value="${idp.nameIDAttribute}" id="nameIDAttribute">
			<div class="amNote">
				<spring:message code="note.name_id_attribute" />
			</div>
		</div>
		<div>
			<label for="nameIncludesDomain"><spring:message code="label.name_includes_domain" />:</label>
			<input type="checkbox" value="true" name="nameIncludesDomain" id="nameIncludesDomain" 
				<c:choose>
					<c:when test="${idp.nameIncludesDomain eq 'true' }">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>  >
			<div class="amNote">
				<spring:message code="note.name_includes_domain" />
			</div>
		</div>
		<div>
			<label for="domainAttribute"><spring:message code="label.domain_attribute" />:</label>
			<input type="text" name="domainAttribute" value="${idp.domainAttribute}" id="domainAttribute">
			<div class="amNote">
				<spring:message code="note.domain_attribute" />
			</div>
		</div>
		<div>
			<label for="upnDomain"><spring:message code="label.upn_domain" />:</label>
			<input type="text" name="upnDomain" value="${idp.upnDomain}" id="upnDomain">
			<div class="amNote">
				<spring:message code="note.upn_domain" />
			</div>
		</div>
		<hr>
		
		<h3><spring:message code="title.account_mapper" /></h3>
		<div>
			<label for="accountMapper"><spring:message code="title.account_mapper" />:</label>
			<input type="text" name="accountMapper" value="${idp.accountMapper}" id="accountMapper">
			<div class="amNote">
				<spring:message code="note.account_mapper" />
			</div>
		</div>
		<hr>
		
		<h3><spring:message code="title.attribute_mapper" /></h3>
		<div>
			<label for="attributeMapper"><spring:message code="title.attribute_mapper" />:</label>
			<input type="text" name="attributeMapper" value="${idp.attributeMapper}" id="attributeMapper">
			<div class="amNote">
				<spring:message code="note.attribute_mapper" />
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.attribute_map" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeMapCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="item" items="${idp.attributeMapCurrentValues}">
						<input type="hidden" name="attributeMapCurrentValues" value="${item}" />
					</c:forEach>	
					<select name="attributeMapCurrentValuesDeleteValues" multiple="multiple" id="attributeMapCurrentValuesDeleteValues">
						<c:forEach var="itemv" items="${idp.attributeMapCurrentValues}">
							<option value="${itemv}">${itemv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteAttributeMapCurrentValues" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="attributeMapCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
					<input name="attributeMapCurrentValuesAddValue" type="text" id="attributeMapCurrentValuesAddValue">
					<input type="submit" name="addAttributeMapCurrentValues" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.attribute_map" />
			</div>
		</div>
		<hr>
		
		<h3><spring:message code="title.assertion_effective_time" /></h3>
		<div>
			<label for="assertionEffectiveTime"><spring:message code="title.assertion_effective_time" />:</label>
			<input type="text" name="assertionEffectiveTime" value="${idp.assertionEffectiveTime}" id="assertionEffectiveTime">
			<div class="amNote">
				<spring:message code="note.assertion_effective_time" />
			</div>
			<form:errors path="assertionEffectiveTime" cssClass="portlet-msg-error"></form:errors>	
		</div>
	</form:form>	

</div>
