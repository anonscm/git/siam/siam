<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_idp_services" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2identity_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2IDPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2IDPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2IDPServicesURL">
					<portlet:param name="ctx" value="Samlv2IDPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2IDPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2IDPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><spring:message code="label.menu_left.assertion_content" /></a></li>
					<li><a href="${editSamlv2IDPAssertionProcessingURL}"><spring:message code="label.menu_left.assertion_processing" /></a></li>
					<li><a href="${editSamlv2IDPServicesURL}"><b><spring:message code="label.menu_left.services" /></b></a></li>
					<li><a href="${editSamlv2IDPAdvanceddURL}"><spring:message code="label.menu_left.advanced" /></a></li>
				</ul>	
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="idpServices">
					<input type="hidden" name="ctx" value="Samlv2IDPServicesController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.metaalias" /></h3>
					<div>
						<label><spring:message code="title.metaalias" />:</label>
						${metaAlias}
						<div class="amNote">
							<spring:message code="note.metaalias" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.idp_service_attributes" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.artifact_resolution_service" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="idpServiceAttributesArtifactResolutionServiceDefault"><spring:message code="label.default" /></label>
								<input type="checkbox" value="true" name="idpServiceAttributesArtifactResolutionServiceDefault" id="idpServiceAttributesArtifactResolutionServiceDefault" 
									<c:choose>
										<c:when test="${idpServices.idpServiceAttributesArtifactResolutionServiceDefault eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="idpServiceAttributesArtifactResolutionServiceLocation"><spring:message code="label.location" />:</label>
								<input type="text" name="idpServiceAttributesArtifactResolutionServiceLocation" value="${idpServices.idpServiceAttributesArtifactResolutionServiceLocation}" id="idpServiceAttributesArtifactResolutionServiceLocation">
							</div>
							<div>
								<label for="idpServiceAttributesArtifactResolutionServiceIndex"><spring:message code="label.index" />:</label>
								<input type="text" name="idpServiceAttributesArtifactResolutionServiceIndex" value="${idpServices.idpServiceAttributesArtifactResolutionServiceIndex}" id="idpServiceAttributesArtifactResolutionServiceIndex">
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.single_logout_service" /> </label>
						<div class="amFormMultipleValues">
							<div>
								<label for="idpServiceAttributesSingleLogoutServiceDefault"><spring:message code="label.default" />:</label>
								<select name="idpServiceAttributesSingleLogoutServiceDefault" id="idpServiceAttributesSingleLogoutServiceDefault">
									<option value="none"><spring:message code="label._none_" /></option>
									<c:forEach var="item" items="${singleLogoutService}">
										<option value="${item}"
											<c:choose>
												<c:when test="${idpServices.idpServiceAttributesSingleLogoutServiceDefault eq item}">
													selected="selected"</c:when>
												<c:otherwise></c:otherwise>
											</c:choose> 
										>${item}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.http_redirect" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation" value="${idpServices.idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation}" id="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation">
									</div>
									<div>
										<label for="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation"><spring:message code="label.response_location" />:</label>
										<input type="text" name="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation" value="${idpServices.idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation}" id="idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="lebel.post" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleLogoutServicePOSTLocation"><spring:message code="label.location" /></label>
										<input type="text" name="idpServiceAttributesSingleLogoutServicePOSTLocation" value="${idpServices.idpServiceAttributesSingleLogoutServicePOSTLocation}" id="idpServiceAttributesSingleLogoutServicePOSTLocation">
									</div>
									<div>
										<label for="idpServiceAttributesSingleLogoutServicePOSTResponseLocation"><spring:message code="label.response_location" />:</label>
										<input type="text" name="idpServiceAttributesSingleLogoutServicePOSTResponseLocation" value="${idpServices.idpServiceAttributesSingleLogoutServicePOSTResponseLocation}" id="idpServiceAttributesSingleLogoutServicePOSTResponseLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.soap" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleLogoutServiceSOAPlocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesSingleLogoutServiceSOAPlocation" value="${idpServices.idpServiceAttributesSingleLogoutServiceSOAPlocation}" id="idpServiceAttributesSingleLogoutServiceSOAPlocation">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.manage_name_id_service" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault"><spring:message code="label.default" />:</label>
								<select name="idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault" id="idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault">
									<option value="none"><spring:message code="label._none_" /></option>
									<c:forEach var="item" items="${singleLogoutService}">
										<option value="${item}"
											<c:choose>
												<c:when test="${idpServices.idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault eq item}">
													selected="selected"</c:when>
												<c:otherwise></c:otherwise>
											</c:choose> 
										>${item}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.http_redirect" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation" value="${idpServices.idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation}" id="idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation">
									</div>
									<div>
										<label for="idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation"><spring:message code="label.response_location" />:</label>
										<input type="text" name="idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation" value="${idpServices.idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation}" id="idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="lebel.post" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesManageNameIDServicePOSTLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesManageNameIDServicePOSTLocation" value="${idpServices.idpServiceAttributesManageNameIDServicePOSTLocation}" id="idpServiceAttributesManageNameIDServicePOSTLocation">
									</div>
									<div>
										<label for="idpServiceAttributesManageNameIDServicePOSTResponseLocation"><spring:message code="label.response_location" />:</label>
										<input type="text" name="idpServiceAttributesManageNameIDServicePOSTResponseLocation" value="${idpServices.idpServiceAttributesManageNameIDServicePOSTResponseLocation}" id="idpServiceAttributesManageNameIDServicePOSTResponseLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.soap" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesManageNameIDServiceSOAPLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesManageNameIDServiceSOAPLocation" value="${idpServices.idpServiceAttributesManageNameIDServiceSOAPLocation}" id="idpServiceAttributesManageNameIDServiceSOAPLocation">
									</div>
								</div>
							</div>
					
						</div>
					</div>	
					<div>
						<label class="amLabelTitle"><spring:message code="label.single_sign_on_service" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label class="amLabelTitle"><spring:message code="label.http_redirect" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation" value="${idpServices.idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation}" id="idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="lebel.post" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleSignOnServicePOSTLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesSingleSignOnServicePOSTLocation" value="${idpServices.idpServiceAttributesSingleSignOnServicePOSTLocation}" id="idpServiceAttributesSingleSignOnServicePOSTLocation">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.soap" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="idpServiceAttributesSingleSignOnServiceSOAPLocation"><spring:message code="label.location" />:</label>
										<input type="text" name="idpServiceAttributesSingleSignOnServiceSOAPLocation" value="${idpServices.idpServiceAttributesSingleSignOnServiceSOAPLocation}" id="idpServiceAttributesSingleSignOnServiceSOAPLocation">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.name_id_mapping" /></h3>
					<div>
						<label>&nbsp;</label>
						<input type="text" name="nameIDMapping" value="${idpServices.nameIDMapping}">
					</div>
					
				</form:form>
			</div>
		</div>
	</div>
</div>