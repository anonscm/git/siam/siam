<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.entity_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal" />
	<form:form action="${formAction}" id="${ns}pageForm" method="post">
		<input type="hidden" name="ctx" value="EntityProviderEditController" />
		<div class="amDivButtons">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	</form:form>
	
	<h3> <spring:message code="title.entity_provider" />: ${entityProvider.protocol} ${entityProvider.entityIdentifier} </h3>

	<h3> <spring:message code="title.configured_entities" /> </h3>
	<table>
		<tr class="odd">
			<th><spring:message code="label.services" /></th>
			<th><spring:message code="label.meta_alias" /></th>
		</tr>
		<c:forEach var="service" items="${entityProvider.descriptorMap}">
			<c:set var="wsfedProtocol"><%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL%></c:set>
			<tr>
				<td>
					<portlet:renderURL var="editServicesURL">
						<portlet:param name="ctx" value="${entityProvider.protocol}${service.key}Controller" />
						<portlet:param name="entityIdentifier" value="${entityProvider.entityIdentifier}" />
						<portlet:param name="serviceType" value="${service.key}" />
					</portlet:renderURL> 
					<a href="${editServicesURL}">
						<spring:message code="title.service.${service.key}.short" />
					</a>
				</td>
				<td>${service.value.metaAlias}</td>
			</tr>
		</c:forEach>
	</table>

</div>
