<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_attribute_query_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="attributeQuery">
		<input type="hidden" name="ctx" value="samlv2attribute_query_providerController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="${attributeQuery.metaAlias}">
	
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>  - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
	
		<div>
			<h3><spring:message code="title.name_id_format" /></h3>
			<div class="amFormMultipleValues">
				<div>
					<label for="nameIDFormatCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="item" items="${attributeQuery.nameIDFormatCurrentValues}">
					<input type="hidden" name="nameIDFormatCurrentValues" value="${item}" />
					</c:forEach>	
					<select name="nameIDFormatCurrentValuesDeleteValues" multiple="multiple" id="nameIDFormatCurrentValuesDeleteValues">
						<c:forEach var="itemv" items="${attributeQuery.nameIDFormatCurrentValues}">
							<option value="${itemv}">${itemv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteNameIDFormatCurrentValues" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="nameIDFormatCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
					<input name="nameIDFormatCurrentValuesAddValue" type="text" id="nameIDFormatCurrentValuesAddValue">
					<input type="submit" name="addNameIDFormatCurrentValues" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.name_id_format.attr_query" />
			</div>
		</div>
	
		<h3><spring:message code="title.cert_aliases_signing_and_encrytpion" /></h3>
		<div>
			<label for="signingCertificateAlias"><spring:message code="lable.signing" />:</label>
			<input type="text" name="signingCertificateAlias" value="${attributeQuery.signingCertificateAlias}" id="signingCertificateAlias">
			<form:errors path="signingCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label for="encryptionCertificateAlias"><spring:message code="label.encryption" />:</label>
			<input type="text" name="encryptionCertificateAlias" value="${attributeQuery.encryptionCertificateAlias}" id="encryptionCertificateAlias">
			<form:errors path="encryptionCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label><spring:message code="label.key_size" />:</label>
		</div>
		<div>
			<label><spring:message code="label.algorithm" />:</label>
		</div>
	
	</form:form>	

</div>