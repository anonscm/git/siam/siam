<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_sp_assertion_processing" />
	</div>

	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2service_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2SPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2SPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2SPServicesURL">
					<portlet:param name="ctx" value="Samlv2SPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2SPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2SPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><spring:message code="label.menu_left.assertion_content" /></a></li>
					<li><a href="${editSamlv2SPAssertionProcessingURL}"><b><spring:message code="label.menu_left.assertion_processing" /></b></a></li>
					<li><a href="${editSamlv2SPServicesURL}"><spring:message code="label.menu_left.services" /></a></li>
					<li><a href="${editSamlv2SPAdvanceddURL}"><spring:message code="label.menu_left.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="spAssertionProcessing">
					<input type="hidden" name="ctx" value="Samlv2SPAssertionProcessingController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.attribute_mapper" /></h3>
					<div>
						<label for="attributeMapper"><spring:message code="title.attribute_mapper" />:</label>
						<input type="text" name="attributeMapper" value="${spAssertionProcessing.attributeMapper}" id="attributeMapper">
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.attribute_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="attributeMapCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="amcv" items="${spAssertionProcessing.attributeMapCurrentValues}">
									<input type="hidden" name="attributeMapCurrentValues" value="${amcv}" />
								</c:forEach>	
								<select name="attributeMapCurrentValuesDeleteValues" multiple="multiple" id="attributeMapCurrentValuesDeleteValues">
									<c:forEach var="amcvv" items="${spAssertionProcessing.attributeMapCurrentValues}">
										<option value="${amcvv}">${amcvv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteAttributeMapCurrentValues" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="attributeMapCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="attributeMapCurrentValuesAddValue" type="text" id="attributeMapCurrentValuesAddValue">
								<input type="submit" name="addAttributeMapCurrentValues" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.attribute_map" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="label.auto_federation" /></h3>
					<div>
						<label for="autoFederationEnabled"><spring:message code="label.enabled" />:</label>
						<input type="checkbox" value="true" name="autoFederationEnabled" id="autoFederationEnabled"  
							<c:choose>
								<c:when test="${spAssertionProcessing.autoFederationEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<div class="amNote">
							<spring:message code="note.auto_federation.enable" />
						</div>
					</div>
					<div>
						<label for="autoFederationAttribute"><spring:message code="label.attribute" />:</label>
						<input type="text" name="autoFederationAttribute" value="${spAssertionProcessing.autoFederationAttribute}" id="autoFederationAttribute">
						<div class="amNote">
							<spring:message code="note.attribute" />
						</div> 
					</div>
					<hr>
					
					<h3><spring:message code="title.account_mapper" /></h3>
					<div>
						<label for="accountMapper"><spring:message code="title.account_mapper" />:</label>
						<input type="text" name="accountMapper" value="${spAssertionProcessing.accountMapper}" id="accountMapper">
						<div class="amNote">
							<spring:message code="note.account_mapper.sp" />
						</div>
					</div>
					<div>
						<label for="useNameIDasUserIDEnabled"><spring:message code="label.use_name_id_as_user_id" />:</label>
						<input type="checkbox" value="true" name="useNameIDasUserIDEnabled" id="useNameIDasUserIDEnabled"
							<c:choose>
								<c:when test="${spAssertionProcessing.useNameIDasUserIDEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<div class="amNote">
							<spring:message code="note.use_name_id_as_user_id" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="label.artifact_message_encoding" /></h3>
					<div>
						<label for="artifactMessageEncoding"><spring:message code="label.encoding" />:</label>
						<select name="artifactMessageEncoding" id="artifactMessageEncoding">
							<option value="URI"
								<c:choose>
									<c:when test="${spAssertionProcessing.artifactMessageEncoding eq 'URI'}">selected="selected"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
								<spring:message code="label.encoding.uri" />
							</option>
							<option value="FORM"
								<c:choose>
									<c:when test="${spAssertionProcessing.artifactMessageEncoding eq 'FORM'}">selected="selected"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
								<spring:message code="label.encoding.form" />
							</option>
						</select>
					</div>
					<hr>
					
					<h3><spring:message code="title.transient_user" /></h3>
					<div>
						<label>&nbsp;</label>
						<input type="text" name="transientUser" value="${spAssertionProcessing.transientUser}">
						<div class="amNote">
							<spring:message code="note.transient_user" />
						</div>
					</div>
					
					<h3><spring:message code="title.url" /></h3>
					<div>
						<label for="localAuthenticationUrl"><spring:message code="label.local_authentication_url" />:</label>
						<input type="text" name="localAuthenticationUrl" value="${spAssertionProcessing.localAuthenticationUrl}" id="localAuthenticationUrl">
						<div class="amNote">
							<spring:message code="note.local_authentication_url" />
						</div> 
					</div>
					<div>
						<label for="intermediateUrl"><spring:message code="label.intermediate_url" />:</label>
						<input type="text" name="intermediateUrl" value="${spAssertionProcessing.intermediateUrl}" id="intermediateUrl">
						<div class="amNote">
							<spring:message code="note.intermediate_url" />
						</div>
					</div>
					<div>
						<label for="externalApplicationLogoutURL"><spring:message code="label.external_application_logout_url" />:</label>
						<input type="text" name="externalApplicationLogoutURL" value="${spAssertionProcessing.externalApplicationLogoutURL}" id="externalApplicationLogoutURL">
						<div class="amNote">
							<spring:message code="note.external_application_logout_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="label.default_relay_state_url" /></h3>
					<div>
						<label>&nbsp;</label>
						<input type="text" name="defaultRelayStateURL" value="${spAssertionProcessing.defaultRelayStateURL}">
						<div class="amNote">
							<spring:message code="note.default_relay_state_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.adapter" /></h3>
					<div>
						<label for="adapter"><spring:message code="title.adapter" />:</label>
						<input type="text" name="adapter" value="${spAssertionProcessing.adapter}" id="adapter">
						<div class="amNote">
							<spring:message code="note.adapter" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.adapter_environment" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="adapterEnvironmentCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="amcv" items="${spAssertionProcessing.adapterEnvironmentCurrentValues}">
									<input type="hidden" name="adapterEnvironmentCurrentValues" value="${amcv}" />
								</c:forEach>	
								<select name="adapterEnvironmentCurrentValuesDeleteValues" multiple="multiple" id="adapterEnvironmentCurrentValuesDeleteValues">
									<c:forEach var="amcvv" items="${spAssertionProcessing.adapterEnvironmentCurrentValues}">
										<option value="${amcvv}">${amcvv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteAdapterEnvironmentCurrentValues" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="adapterEnvironmentCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="adapterEnvironmentCurrentValuesAddValue" type="text" id="adapterEnvironmentCurrentValuesAddValue">
								<input type="submit" name="addAdapterEnvironmentCurrentValues" value="<spring:message code="action.add" />">
							</div>
						</div>
					</div>
					
				
				</form:form>
			
			</div>
		</div>
	</div>

</div>			