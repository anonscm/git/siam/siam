<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_sp_services" />
	</div>

	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2service_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2SPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2SPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2SPServicesURL">
					<portlet:param name="ctx" value="Samlv2SPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2SPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2SPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><spring:message code="label.menu_left.assertion_content" /></a></li>
					<li><a href="${editSamlv2SPAssertionProcessingURL}"><spring:message code="label.menu_left.assertion_processing" /></a></li>
					<li><a href="${editSamlv2SPServicesURL}"><b><spring:message code="label.menu_left.services" /></b></a></li>
					<li><a href="${editSamlv2SPAdvanceddURL}"><spring:message code="label.menu_left.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="spServices">
					<input type="hidden" name="ctx" value="Samlv2SPServicesController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					<input type="hidden" name="assertionServiceHTTPARTIFACTResponseLocationServices" value="${spServices.assertionServiceHTTPARTIFACTResponseLocationServices}">
					<input type="hidden" name="assertionServicePOSTResponseLocationServices" value="${spServices.assertionServicePOSTResponseLocationServices}">
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.metaalias" /></h3>
					<div>
						<label><spring:message code="title.metaalias" />:</label>
						${metaAlias}
						<div class="amNote">
							<spring:message code="note.metaalias" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.sp_service_attributes" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.single_logout_service" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="singleLogoutServiceDefaultServices"><spring:message code="label.default" />:</label>
								<select name="singleLogoutServiceDefaultServices" id="singleLogoutServiceDefaultServices">
									<option value="none"><spring:message code="label._none_" /></option>
									<c:forEach var="item" items="${singleLogoutService}">
										<option value="${item}"
											<c:choose>
												<c:when test="${spServices.singleLogoutServiceDefaultServices eq item}">
													selected="selected"</c:when>
												<c:otherwise></c:otherwise>
											</c:choose> 
										>${item}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.http_redirect" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="singleLogoutServiceHTTPREDIRECTLocationServices"><spring:message code="label.location" />:</label>
										<input type="text" name="singleLogoutServiceHTTPREDIRECTLocationServices" value="${spServices.singleLogoutServiceHTTPREDIRECTLocationServices}" id="singleLogoutServiceHTTPREDIRECTLocationServices">
									</div>
									<div>
										<label for="singleLogoutServiceHTTPREDIRECTResponseLocationServices"><spring:message code="label.response_location" />:</label>
										<input type="text" name="singleLogoutServiceHTTPREDIRECTResponseLocationServices" value="${spServices.singleLogoutServiceHTTPREDIRECTResponseLocationServices}" id="singleLogoutServiceHTTPREDIRECTResponseLocationServices">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="lebel.post" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="singleLogoutServicePOSTLocationServices"><spring:message code="label.location" /></label>
										<input type="text" name="singleLogoutServicePOSTLocationServices" value="${spServices.singleLogoutServicePOSTLocationServices}" id="singleLogoutServicePOSTLocationServices">
									</div>
									<div>
										<label for="singleLogoutServicePOSTResponseLocationServices"><spring:message code="label.response_location" />:</label>
										<input type="text" name="singleLogoutServicePOSTResponseLocationServices" value="${spServices.singleLogoutServicePOSTResponseLocationServices}" id="singleLogoutServicePOSTResponseLocationServices">
									</div>
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.soap" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="singleLogoutServiceSOAPLocationServices"><spring:message code="label.location" />:</label>
										<input type="text" name="singleLogoutServiceSOAPLocationServices" value="${spServices.singleLogoutServiceSOAPLocationServices}" id="singleLogoutServiceSOAPLocationServices">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.manage_name_id_service" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="manageNameIDServiceDefaultServices"><spring:message code="label.default" />:</label>
								<select name="manageNameIDServiceDefaultServices" id="manageNameIDServiceDefaultServices">
									<option value="none"><spring:message code="label._none_" /></option>
									<c:forEach var="item" items="${singleLogoutService}">
										
										<option value="${item}"
											<c:choose>
												<c:when test="${spServices.manageNameIDServiceDefaultServices eq item}">
													selected="selected"</c:when>
												<c:otherwise></c:otherwise>
											</c:choose> 
										>${item}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.http_redirect" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="manageNameIDServiceHTTPREDIRECTLocationServices"><spring:message code="label.location" />:</label>
										<input type="text" name="manageNameIDServiceHTTPREDIRECTLocationServices" value="${spServices.manageNameIDServiceHTTPREDIRECTLocationServices}" id="manageNameIDServiceHTTPREDIRECTLocationServices">
									</div>
									<div>
										<label for="manageNameIDServiceHTTPREDIRECTResponseLocationServices"><spring:message code="label.response_location" />:</label>
										<input type="text" name="manageNameIDServiceHTTPREDIRECTResponseLocationServices" value="${spServices.manageNameIDServiceHTTPREDIRECTResponseLocationServices}" id="manageNameIDServiceHTTPREDIRECTResponseLocationServices">
									</div>
									
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="lebel.post" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="manageNameIDServicePOSTLocationServices"><spring:message code="label.location" />:</label>
										<input type="text" name="manageNameIDServicePOSTLocationServices" value="${spServices.manageNameIDServicePOSTLocationServices}" id="manageNameIDServicePOSTLocationServices">
									</div>
									<div>
										<label for="manageNameIDServicePOSTResponseLocationServices"><spring:message code="label.response_location" />:</label>
										<input type="text" name="manageNameIDServicePOSTResponseLocationServices" value="${spServices.manageNameIDServicePOSTResponseLocationServices}" id="manageNameIDServicePOSTResponseLocationServices">
									</div>
									
								</div>
							</div>
							<div>
								<label class="amLabelTitle"><spring:message code="label.soap" /></label>
								<div class="amFormMultipleValues">
									<div>
										<label for="manageNameIDServiceSOAPLocationServices"><spring:message code="label.location" />:</label>
										<input type="text" name="manageNameIDServiceSOAPLocationServices" value="${spServices.manageNameIDServiceSOAPLocationServices}" id="manageNameIDServiceSOAPLocationServices">
									</div>
									<div>
										<label for="manageNameIDServiceSOAPResponseLocationServices"><spring:message code="label.response_location" />:</label>
										<input type="text" name="manageNameIDServiceSOAPResponseLocationServices" value="${spServices.manageNameIDServiceSOAPResponseLocationServices}" id="manageNameIDServiceSOAPResponseLocationServices">
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.assertion_consumer_service" /> </h3>
					<table>
						<tr class="odd">
							<th><spring:message code="label.default" /></th>
							<th><spring:message code="label.type" /></th>
							<th><spring:message code="label.location" /></th>
							<th><spring:message code="label.index" /></th>
						</tr>
						<tr>
							<td>
								<input name="assertionServiceHTTPARTIFACTDefault" type="checkbox" value="true"
								 <c:choose>
									<c:when test="${spServices.assertionServiceHTTPARTIFACTDefault eq 'true'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> 
								>
							</td>
							<td><spring:message code="label.http-artifact" /></td>
							<td><input type="text" name="assertionServiceHTTPARTIFACTLocationServices" value="${spServices.assertionServiceHTTPARTIFACTLocationServices}" ></td>
							<td><input type="text" name="assertionServiceHTTPARTIFACTIndex" value="${spServices.assertionServiceHTTPARTIFACTIndex}" ></td>
						</tr>
						<tr>
							<td>
								<input name="assertionServicePOSTDefault" type="checkbox" value="true"
								 <c:choose>
									<c:when test="${spServices.assertionServicePOSTDefault eq 'true'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> 
								>
							</td>
							<td><spring:message code="label.http-post" /> </td>
							<td><input type="text" name="assertionServicePOSTLocationServices" value="${spServices.assertionServicePOSTLocationServices}" ></td>
							<td><input type="text" name="assertionServicePOSTIndex" value="${spServices.assertionServicePOSTIndex}" ></td>
						</tr>
						<tr>
							<td>
								<input name="assertionServicePAOSDefault" type="checkbox" value="true"
								 <c:choose>
									<c:when test="${spServices.assertionServicePAOSDefault eq 'true'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> 
								>
							</td>
							<td><spring:message code="lebel.post" /> </td>
							<td><input type="text" name="assertionServicePAOSLocationServices" value="${spServices.assertionServicePAOSLocationServices}" ></td>
							<td><input type="text" name="assertionServicePAOSIndex" value="${spServices.assertionServicePAOSIndex}" ></td>
						</tr>
					</table>
					<div class="amNote">
						<spring:message code="note.assertion_consumer_service" />
					</div>
				
				</form:form>
			
			</div>
		</div>
	</div>
		

</div>	