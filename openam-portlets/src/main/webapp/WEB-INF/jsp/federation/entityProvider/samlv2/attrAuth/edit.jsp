<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_attribute_authority" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="attrAuth">
		<input type="hidden" name="ctx" value="samlv2attribute_authorityController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="${attrAuth.metaAlias}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %> -  ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
		
		<h3><spring:message code="title.signing_and_encryption" /></h3>
		<div>
			<label><spring:message code="label.key_size" />:</label>
		</div>
		<div>
			<label><spring:message code="label.algorithm" />:</label>
		</div>
		<hr>
	
		<h3><spring:message code="title.attribute_service" /></h3>
		<div>
			<label class="amLabelTitle"><spring:message code="labe.basic" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeServiceBasicLocation"><spring:message code="label.location" />:</label>
					<input type="text" name="attributeServiceBasicLocation" value="${attrAuth.attributeServiceBasicLocation}" id="attributeServiceBasicLocation">
				</div>
				<div>
					<label for="attributeServiceBasicDefaultMapper"><spring:message code="label.default_mapper" />:</label>
					<input type="text"  name="attributeServiceBasicDefaultMapper" value="${attrAuth.attributeServiceBasicDefaultMapper}" id="attributeServiceBasicDefaultMapper">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="title.x_509" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeServiceX509SupportsService"><spring:message code="label.supports_x_509" />:</label>
					<input type="checkbox" name="attributeServiceX509SupportsService" value="true" id="attributeServiceX509SupportsService" 
						<c:choose>
							<c:when test="${attrAuth.attributeServiceX509SupportsService eq 'true'}">checked="checked"</c:when>
							<c:otherwise></c:otherwise>
						</c:choose> >
				</div>
				<div>
					<label for="attributeServiceX509Location"><spring:message code="label.location" />:</label>
					<input type="text" name="attributeServiceX509Location" value="${attrAuth.attributeServiceX509Location}" id="attributeServiceX509Location">
				</div>
				<div>
					<label for="attributeServiceX509X509Mapper"><spring:message code="label.x_509_mapper" />:</label>
					<input type="text" name="attributeServiceX509X509Mapper" value="${attrAuth.attributeServiceX509X509Mapper}" id="attributeServiceX509X509Mapper">
				</div>
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.assertion_id_request" /></h3>
		<div>
			<label class="amLabelTitle"><spring:message code="label.soap" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="assertionIDRequestSoapLocation"><spring:message code="label.location" />:</label>
					<input type="text" name="assertionIDRequestSoapLocation" value="${attrAuth.assertionIDRequestSoapLocation}" id="assertionIDRequestSoapLocation">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.uri" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="assertionIDRequestURILocation"><spring:message code="label.location" />:</label>
					<input type="text" name="assertionIDRequestURILocation" value="${attrAuth.assertionIDRequestURILocation}" id="assertionIDRequestURILocation">
				</div>
			</div>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.mapper" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="assertionIDRequestMapper"><spring:message code="label.location" />:</label>
					<input type="text" name="assertionIDRequestMapper" value="${attrAuth.assertionIDRequestMapper}" id="assertionIDRequestMapper">
					<div class="amNote">
						<spring:message code="note.mapper" />
					</div>
				</div>
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.attribute_profile" /></h3>
		<div>
			<label>&nbsp;</label>
			<input type="text" name="attributeProfile" value="${attrAuth.attributeProfile}">
			<div class="amNote">
				<spring:message code="note.attribute_profile" />
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.cert_aliases_signing_and_encrytpion" /></h3>
		<div>
			<label for="signingCertificateAlias"><spring:message code="lable.signing" /></label>
			<input type="text" name="signingCertificateAlias" value="${attrAuth.signingCertificateAlias}" id="signingCertificateAlias">
			<form:errors path="signingCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label for="encryptionCertificateAlias"><spring:message code="label.encryption" />:</label>
			<input type="text" name="encryptionCertificateAlias" value="${attrAuth.encryptionCertificateAlias}" id="encryptionCertificateAlias">
			<form:errors path="encryptionCertificateAlias"  cssClass="portlet-msg-error"/>
		</div>
		<div>
			<label><spring:message code="label.key_size" />:</label>
		</div>
		<div>
			<label><spring:message code="label.algorithm" />:</label>
		</div>
		<hr>
	
		<h3><spring:message code="title.subject_data_store" /></h3>
		<div>
			<label>&nbsp;</label>
			<input type="text" name="subjectDataStore" value="${attrAuth.subjectDataStore}">
			<div class="amNote">
				<spring:message code="note.subject_data_store" />
			</div>
		</div>
	</form:form>

</div>