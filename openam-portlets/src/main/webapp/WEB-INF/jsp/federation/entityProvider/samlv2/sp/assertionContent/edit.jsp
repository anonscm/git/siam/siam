<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper"> 

	<div class="amBreadcrumb" >
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_sp_assertion_content" />
		 
	</div>

	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2service_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2SPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2SPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2SPServicesURL">
					<portlet:param name="ctx" value="Samlv2SPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2SPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2SPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><b><spring:message code="label.menu_left.assertion_content" /></b></a></li>
					<li><a href="${editSamlv2SPAssertionProcessingURL}"><spring:message code="label.menu_left.assertion_processing" /></a></li>
					<li><a href="${editSamlv2SPServicesURL}"><spring:message code="label.menu_left.services" /></a></li>
					<li><a href="${editSamlv2SPAdvanceddURL}"><spring:message code="label.menu_left.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="spAssertionContent">
					<input type="hidden" name="ctx" value="samlv2service_providerController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.signing_and_encryption" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.request_response_signing" />:</label>
						<div class="amNote">
							<spring:message code="note.request_response_signing" />
						</div>
						<div class="amFormMultipleValues">
							<div>
								<label for="signingandEncryptionAuthenticationRequestsSigned"><spring:message code="label.authentication_requests_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionAuthenticationRequestsSigned" id="signingandEncryptionAuthenticationRequestsSigned" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionAuthenticationRequestsSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionAuthenticationAssertionsSigned"><spring:message code="label.assertions_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionAuthenticationAssertionsSigned" id="signingandEncryptionAuthenticationAssertionsSigned" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionAuthenticationAssertionsSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionPostResponseSigned"><spring:message code="label.post_response_signed" /> :</label>
								<input type="checkbox" value="true" name="signingandEncryptionPostResponseSigned" id="signingandEncryptionPostResponseSigned"
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionPostResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionArtifactResponseSigned"><spring:message code="label.artifact_response_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionArtifactResponseSigned" id="signingandEncryptionArtifactResponseSigned"
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionArtifactResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionLogoutRequestSigned"><spring:message code="label.logout_request_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionLogoutRequestSigned" id="signingandEncryptionLogoutRequestSigned" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionLogoutRequestSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionLogoutResponseSigned"><spring:message code="label.logout_response_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionLogoutResponseSigned" id="signingandEncryptionLogoutResponseSigned"
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionLogoutResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionManageNameIDRequestSigned" ><spring:message code="label.manage_name_id_request_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionManageNameIDRequestSigned" id="signingandEncryptionManageNameIDRequestSigned" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionManageNameIDRequestSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionManageNameIDResponseSigned"><spring:message code="label.manage_name_id_response_signed" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionManageNameIDResponseSigned" id="signingandEncryptionManageNameIDResponseSigned" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionManageNameIDResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.encryption" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="signingandEncryptionEncryptionAttribute"><spring:message code="label.attribute" /> :</label>
								<input type="checkbox" value="true" name="signingandEncryptionEncryptionAttribute" id="signingandEncryptionEncryptionAttribute"
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionEncryptionAttribute eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionAssertionAttribute"><spring:message code="label.assertion" />:</label>
								<input type="checkbox" value="true" name="signingandEncryptionAssertionAttribute" id="signingandEncryptionAssertionAttribute" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionAssertionAttribute eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="signingandEncryptionNameIDAttribute"><spring:message code="label.name_id" /> :</label>
								<input type="checkbox" value="true" name="signingandEncryptionNameIDAttribute" id="signingandEncryptionNameIDAttribute" 
									<c:choose>
										<c:when test="${spAssertionContent.signingandEncryptionNameIDAttribute eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.certificate_aliases" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="signingandEncryptionCertificateAliasesSigning"><spring:message code="lable.signing" /></label>
								<input type="text" name="signingandEncryptionCertificateAliasesSigning" value="${spAssertionContent.signingandEncryptionCertificateAliasesSigning}" id="signingandEncryptionCertificateAliasesSigning">
								<form:errors path="signingandEncryptionCertificateAliasesSigning"  cssClass="portlet-msg-error"/>
							</div>
							<div>
								<label for="signingandEncryptionCertificateAliasesEncryption"><spring:message code="label.encryption" />:</label>
								<input type="text" name="signingandEncryptionCertificateAliasesEncryption" value="${spAssertionContent.signingandEncryptionCertificateAliasesEncryption}" id="signingandEncryptionCertificateAliasesEncryption">
								<form:errors path="signingandEncryptionCertificateAliasesEncryption"  cssClass="portlet-msg-error"/>
							</div>
							<div>
								<label for="signingandEncryptionCertificateAliasesKeySize"><spring:message code="label.key_size" />:</label>
								<input type="text" name="signingandEncryptionCertificateAliasesKeySize" value="${spAssertionContent.signingandEncryptionCertificateAliasesKeySize}" id="signingandEncryptionCertificateAliasesKeySize">
							</div>
							<div>
								<label for="signingandEncryptionCertificateAliasesAlgorithm"><spring:message code="label.algorithm" />:</label>
								<input type="text" name="signingandEncryptionCertificateAliasesAlgorithm" value="${spAssertionContent.signingandEncryptionCertificateAliasesAlgorithm}" id="signingandEncryptionCertificateAliasesAlgorithm">
							</div>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.name_id_format" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.name_id_format_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="nameIDFormatListCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="nidvmcv" items="${spAssertionContent.nameIDFormatListCurrentValues}">
									<input type="hidden" name="nameIDFormatListCurrentValues" value="${nidvmcv}" />
								</c:forEach>	
								<select name="nameIDFormatListCurrentValuesDeleteValues" multiple="multiple" id="nameIDFormatListCurrentValuesDeleteValues">
									<c:forEach var="nidvmcvv" items="${spAssertionContent.nameIDFormatListCurrentValues}">
										<option value="${nidvmcvv}">${nidvmcvv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNameIDFormatListCurrentValues" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="nameIDFormatListCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="nameIDFormatListCurrentValuesAddValue" type="text" id="nameIDFormatListCurrentValuesAddValue">
								<input type="submit" name="addNameIDFormatListCurrentValues" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.name_id_format_list" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.authentication_context" /></h3>
					<div>
						<label for="authenticationContextMapper"><spring:message code="label.mapper" />:</label>
						<input type="text" name="authenticationContextMapper" value="${spAssertionContent.authenticationContextMapper}" id="authenticationContextMapper">
					</div>
					<div>
						<label for="authenticationContextDefaultAuthenticationContext">*<spring:message code="label.authentication_context" />:</label>
						<select name="authenticationContextDefaultAuthenticationContext" id="authenticationContextDefaultAuthenticationContext">
							<c:forEach var="element" items="${spAssertionContent.samlv2AuthContextList}" varStatus="status" >
								<option value="${element.name}" 
									<c:choose>
										<c:when test="${spAssertionContent.authenticationContextDefaultAuthenticationContext eq element.name}">selected="selected"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${authenticationContextMap[element.name]}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.authentication_context" /></label>
						<table>
							<tr class="odd">
								<th><spring:message code="label.authentication_context.supported" /></th>
								<th><spring:message code="label.authentication_context.context_reference" /></th>
								<th><spring:message code="label.authentication_context.level" /></th>
							</tr>
							<c:forEach var="element" items="${spAssertionContent.samlv2AuthContextList}" varStatus="status" >
								<tr>
									<td>
										<input type="checkbox" value="true" name="samlv2AuthContextList[${status.index}].supported"
										 <c:choose>
											<c:when test="${element.supported eq 'true'}">checked="checked"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose> 
										>
									</td>
									<td>
										${authenticationContextMap[element.name]}
										<input type="hidden" name="samlv2AuthContextList[${status.index}].name" value="${element.name}">
									</td>
									<td><input type="text" value="${element.level}" name="samlv2AuthContextList[${status.index}].level"></td>
								</tr>
							</c:forEach>
						</table>	
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="note.authentication_context" /> </label>
						<div class="amFormMultipleValues">
							<div>
								<label for="comparisonType"><spring:message code="label.comparison_type" />:</label>
								<select name="comparisonType" id="comparisonType">
									<option value="ignore"><spring:message code="label._none_" /></option>
									<option value="exact"
										<c:choose>
											<c:when test="${spAssertionContent.comparisonType eq 'exact'}">selected="selected"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose> >
										<spring:message code="label.comparison_type_exact" />
									</option>
									<option value="minimum"
										<c:choose>
											<c:when test="${spAssertionContent.comparisonType eq 'minimum'}">selected="selected"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>  >
										<spring:message code="label.comparison_type_minimum" />
									</option>
									<option value="maximum"
										<c:choose>
											<c:when test="${spAssertionContent.comparisonType eq 'maximum'}">selected="selected"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose> >
										<spring:message code="label.comparison_type_maximum" />
									</option>
									<option value="better"
										<c:choose>
											<c:when test="${spAssertionContent.comparisonType eq 'better'}">selected="selected"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>  >
										<spring:message code="label.comparison_type_better" />
									</option>
								</select>
							</div>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="label.assertion_time_skew" /></h3>
					<div>
						<label>&nbsp;</label>
						<input type="text" name="assertionTimeSkew" value="${spAssertionContent.assertionTimeSkew}">
						<div class="amNote">
							<spring:message code="note.not_before_time_skew" />
						</div>
						<form:errors path="assertionTimeSkew"  cssClass="portlet-msg-error"/>
					</div>
					<hr>
					
					<h3><spring:message code="title.basic_authentication" /></h3>
					<div>
						<label for="amLabelTitle"><spring:message code="note.basic_authentication" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="basicAuthenticationEnabled"><spring:message code="label.enabled" />:</label>
								<input type="checkbox" value="true" name="basicAuthenticationEnabled" id="basicAuthenticationEnabled" 
									<c:choose>
										<c:when test="${spAssertionContent.basicAuthenticationEnabled eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="basicAuthenticationUserName"><spring:message code="label.user_name" />:</label>
								<input type="text" name="basicAuthenticationUserName" value="${spAssertionContent.basicAuthenticationUserName}" id="basicAuthenticationUserName">
							</div>
							<div>
								<label for="basicAuthenticationPassword"><spring:message code="label.basic_authentication_password" />:</label>
								<input type="text" name="basicAuthenticationPassword" value="${spAssertionContent.basicAuthenticationPassword}" id="basicAuthenticationPassword">
							</div>
						</div>
					</div>
			
				</form:form>
			</div>
		</div>
	</div>

</div>