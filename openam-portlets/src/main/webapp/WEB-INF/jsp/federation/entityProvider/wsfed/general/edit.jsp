<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.wsfed_general" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="general">
		<input type="hidden" name="ctx" value="wsfedgeneralController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>">
		<input type="hidden" name="metaAlias" value="ignore">
		<input type="hidden" name="singleSignOutNotificationEndpoint" value="${general.singleSignOutNotificationEndpoint}">
		<c:forEach var="item" items="${general.tokenTypesOffered}">
			<input type="hidden" name="tokenTypesOffered" value="${item}">
		</c:forEach>
		<input type="hidden" name="uriNamedClaimTypesOfferedDisplay" value="${general.uriNamedClaimTypesOfferedDisplay}">
		<input type="hidden" name="uriNamedClaimTypesOfferedURL" value="${general.uriNamedClaimTypesOfferedURL}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %> - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
		
		<div>
			<label><spring:message code="label.name" />:</label>
			${entityIdentifier}
		</div>
		<c:if test="${general.spDisplayName!=null}">
			<div>
				<label for="spDisplayName"><spring:message code="label.sp_display_name" />:</label>
				<input type="text" name="spDisplayName" value="${general.spDisplayName}" id="spDisplayName">
			</div>
		</c:if>
		<c:if test="${general.idpDisplayName!=null}">
			<div>
				<label for="idpDisplayName"><spring:message code="label.idp_display_name" />:</label>
				<input type="text" name="idpDisplayName" value="${general.idpDisplayName}" id="idpDisplayName">
			</div>
		</c:if>
		
		<div>
			<label><spring:message code="label.realm" />:</label>
			${realm}
		</div>
		<div>
			<label for="tokenIssuerNameList"><spring:message code="label.token_issuer_name" />:</label>
			<input type="text" name="tokenIssuerNameList" value="${general.tokenIssuerNameList}" id="tokenIssuerNameList">
		</div>
		<div>
			<label for="tokenIssuerEndpointList"><spring:message code="label.token_issuer_endpoint" />:</label>
			<input type="text" name="tokenIssuerEndpointList" value="${general.tokenIssuerEndpointList}" id="tokenIssuerEndpointList">
		</div>
		
	</form:form>	

</div>