<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a class="link_button" href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>"/>
		</portlet:renderURL>
		<a class="link_button" href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.wsfed_service_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="sp">
		<input type="hidden" name="ctx" value="wsfedservice_providerController" />
		<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
		<input type="hidden" name="serviceType" value="${serviceType}">
		<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %>">
		
		<input type="hidden" name="metaAlias" value="${sp.metaAlias}">
		<input type="hidden" name="encryptionCertificateAlias" value="${sp.encryptionCertificateAlias}">
		<input type="hidden" name="signingCertificateAlias" value="${sp.signingCertificateAlias}">
		
		<input type="hidden" name="saml2AuthModuleName" value="${sp.saml2AuthModuleName}">
		<input type="hidden" name="authncontextMapper" value="${sp.authncontextMapper}">
		<input type="hidden" name="authncontextClassRefMapping" value="${sp.authncontextClassRefMapping}">
		<input type="hidden" name="authncontextComparisonType" value="${sp.authncontextComparisonType}">
		<input type="hidden" name="assertionCacheEnabled" value="${sp.assertionCacheEnabled}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3>
			<spring:message code="title.entity_provider" />: <%=FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL %> - ${entityIdentifier}, 
			<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
		</h3>
	
		<h3><spring:message code="title.assertion_signed" /></h3>
		<div>
			<label><spring:message code="title.assertion_signed" />:</label>
			<input type="checkbox" value="true" name="assertionSigned"
				<c:choose>
					<c:when test="${sp.assertionSigned eq 'true' }">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
		</div>
		<hr>
	
		<h3><spring:message code="title.account_mapper" /></h3>
		<div>
			<label for="accountMapper"><spring:message code="title.account_mapper" />:</label>
			<input type="text" name="accountMapper" value="${sp.accountMapper}" id="accountMapper">
		</div>
		<hr>
	
		<h3><spring:message code="title.attribute_mapper" /></h3>
		<div>
			<label for="attributeMapper"><spring:message code="title.attribute_mapper" />:</label>
			<input type="text" name="attributeMapper" value="${sp.attributeMapper}" id="attributeMapper">
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.attribute_map" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="attributeMapCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="item" items="${sp.attributeMapCurrentValues}">
						<input type="hidden" name="attributeMapCurrentValues" value="${item}" />
					</c:forEach>	
					<select name="attributeMapCurrentValuesDeleteValues" multiple="multiple" id="attributeMapCurrentValuesDeleteValues">
						<c:forEach var="itemv" items="${sp.attributeMapCurrentValues}">
							<option value="${itemv}">${itemv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteAttributeMapCurrentValues" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="attributeMapCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
					<input name="attributeMapCurrentValuesAddValue" type="text" id="attributeMapCurrentValuesAddValue">
					<input type="submit" name="addAttributeMapCurrentValues" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.attribute_map" />
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.assertion_effective_time" /></h3>
		<div>
			<label for="assertionEffectiveTime"><spring:message code="title.assertion_effective_time" />:</label>
			<input type="text" name="assertionEffectiveTime" value="${sp.assertionEffectiveTime}" id="assertionEffectiveTime">
			<div class="amNote">
				<spring:message code="note.assertion_effective_time.wd_fed.sp" />
			</div>
			<form:errors path="assertionEffectiveTime" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<hr>
	
		<h3><spring:message code="title.assertion_skew_time" /></h3>
		<div>
			<label for="assertionTimeSkew"><spring:message code="title.assertion_skew_time" />:</label>
			<input type="text" name="assertionTimeSkew" value="${sp.assertionTimeSkew }" id="assertionTimeSkew">
			<div class="amNote">
				<spring:message code="note.assertion_skew_time" />
			</div>
			<form:errors path="assertionTimeSkew" cssClass="portlet-msg-error"></form:errors>
		</div>
		<hr>
	
		<h3><spring:message code="title.default_relay_state" /></h3>
		<div>
			<label for="defaultRelayState"><spring:message code="title.default_relay_state" />:</label>
			<input type="text" name="defaultRelayState" value="${sp.defaultRelayState}" id="defaultRelayState">
			<div class="amNote">
				<spring:message code="note.default_relay_state" />
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.home_realm_discovery" /></h3>
		<div>
			<label for="homeRealmDiscoveryService"><spring:message code="label.home_realm_discovery_service" />:</label>
			<input type="text" name="homeRealmDiscoveryService" value="${sp.homeRealmDiscoveryService}" id="homeRealmDiscoveryService">
			<div class="amNote">
				<spring:message code="note.home_realm_discovery_service" />
			</div>
		</div>
		<hr>
	
		<h3><spring:message code="title.account_realm_selection" /></h3>
		<div class="amNote">
			<spring:message code="note.account_realm_selection" /> 
		</div>
		<div>
			<div class="amFormCell">
				<input type="radio" name="accountRealmSelection" value="cookie" id="accountRealmSelection" 
					<c:choose>
						<c:when test="${sp.accountRealmSelection eq 'cookie' }">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>  > 
				<label class="amInputLabel" for="accountRealmSelection"><spring:message code="label.cookie" /></label>
			</div>
			<div class="amFormCell">
				<label for="cookieName"><spring:message code="label.name" /></label>
				<input type="text" name="cookieName" value="${sp.cookieName}" class="cookieName"> 
			</div>
		</div>
		<div>
			<div class="amFormCell">
				<input type="radio" name="accountRealmSelection" value="UserAgentKey" id="accountRealmSelection"
					<c:choose>
						<c:when test="${sp.accountRealmSelection eq 'UserAgentKey' }">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>  > 
				<label class="amInputLabel" for="accountRealmSelection"><spring:message code="label.user_agent_key" />  </label>
			</div>
			<div class="amFormCell">
				<label for="userAgentKey"><spring:message code="label.key" /></label>
				<input type="text" name="userAgentKey" value="${sp.userAgentKey}" id="userAgentKey">
			</div>
		</div>
		
	</form:form>	

</div>
