<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.FederationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="entityProviderUrl">
			<portlet:param name="ctx" value="EntityProviderEditController"/>
			<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
			<portlet:param name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>"/>
		</portlet:renderURL>
		<a href="${entityProviderUrl}">
			<spring:message code="breadcrumb.entity_provider" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.samlv2_idp_assertion_content" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="title.entity_provider" />: ${entityIdentifier}, 
					<spring:message code="label.entity" />: ${metaAlias}, 
					<spring:message code="label.type" />: <spring:message code="title.service.${serviceType}.short" />
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editSamlv2AssertionContentURL">
					<portlet:param name="ctx" value="samlv2identity_providerController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editSamlv2IDPAssertionProcessingURL">
					<portlet:param name="ctx" value="Samlv2IDPAssertionProcessingController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editSamlv2IDPServicesURL">
					<portlet:param name="ctx" value="Samlv2IDPServicesController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editSamlv2IDPAdvanceddURL">
					<portlet:param name="ctx" value="Samlv2IDPAdvancedController"/>
					<portlet:param name="entityIdentifier" value="${entityIdentifier}"/>
					<portlet:param name="serviceType" value="${serviceType}"/>
				</portlet:renderURL>	
				
				<ul class="lfr-menu-list">
					<li><a href="${editSamlv2AssertionContentURL}"><b><spring:message code="label.menu_left.assertion_content" /></b></a></li>
					<li><a href="${editSamlv2IDPAssertionProcessingURL}"><spring:message code="label.menu_left.assertion_processing" /></a></li>
					<li><a href="${editSamlv2IDPServicesURL}"><spring:message code="label.menu_left.services" /></a></li>
					<li><a href="${editSamlv2IDPAdvanceddURL}"><spring:message code="label.menu_left.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="idpAssertionContent">
					<input type="hidden" name="ctx" value="samlv2identity_providerController" />
					<input type="hidden" name="entityIdentifier" value="${entityIdentifier}">
					<input type="hidden" name="serviceType" value="${serviceType}">
					<input type="hidden" name="protocol" value="<%=FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL %>">
					<input type="hidden" name="nameIDFormatNameIDFormatList" value="${idpAssertionContent.nameIDFormatNameIDFormatList}">
					
					<c:forEach var="item" items="${idpAssertionContent.relayStateUrlListCurrentValues}">
						<input type="hidden" name="relayStateUrlListCurrentValues" value="${item}">
					</c:forEach>
				
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<h3><spring:message code="title.signing_and_encryption" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.request_response_signing" />:</label>
						<div class="amNote">
							<spring:message code="note.request_response_signing" />
						</div>
						<div class="amFormMultipleValues">				
							<div>
								<label for="authenticationRequestSigned"><spring:message code="label.authentication_request" />:</label>
								<input type="checkbox" value="true" name="authenticationRequestSigned" id="authenticationRequestSigned" 
									<c:choose>
										<c:when test="${idpAssertionContent.authenticationRequestSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="artifactResolveSigned"><spring:message code="label.artifact_resolve" />:</label>
								<input type="checkbox" value="true" name="artifactResolveSigned" id="artifactResolveSigned"
									<c:choose>
										<c:when test="${idpAssertionContent.artifactResolveSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="logoutRequestSigned"><spring:message code="label.logout_request" />:</label>
								<input type="checkbox" value="true" name="logoutRequestSigned" id="logoutRequestSigned"
									<c:choose>
										<c:when test="${idpAssertionContent.logoutRequestSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="logoutResponseSigned"><spring:message code="label.logout_response" /> :</label>
								<input type="checkbox" value="true" name="logoutResponseSigned" id="logoutResponseSigned"
									<c:choose>
										<c:when test="${idpAssertionContent.logoutResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="manageNameIDRequestSigned"><spring:message code="label.manage_name_id_request" />:</label>
								<input type="checkbox" value="true" name="manageNameIDRequestSigned" id="manageNameIDRequestSigned" 
									<c:choose>
										<c:when test="${idpAssertionContent.manageNameIDRequestSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label for="manageNameIDResponseSigned"><spring:message code="label.manage_name_id_response" />:</label>
								<input type="checkbox" value="true" name="manageNameIDResponseSigned" id="manageNameIDResponseSigned" 
									<c:choose>
										<c:when test="${idpAssertionContent.manageNameIDResponseSigned eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
						</div>	
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.encryption" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="wantNameIDEncrypted"><spring:message code="label.name_id_encryption" />: </label>
								<input type="checkbox" value="true" name="wantNameIDEncrypted" id="wantNameIDEncrypted" 
									<c:choose>
										<c:when test="${idpAssertionContent.wantNameIDEncrypted eq 'true'}">checked="checked"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose> >
							</div>
							<div>
								<label></label>
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.certificate_aliases" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="certificateAliasesSigning"><spring:message code="lable.signing" /></label>
								<input type="text" name="certificateAliasesSigning" value="${idpAssertionContent.certificateAliasesSigning}" id="certificateAliasesSigning">
								<form:errors path="certificateAliasesSigning"  cssClass="portlet-msg-error"/>
							</div>
							<div>
								<label for="certificateAliasesEncryption"><spring:message code="label.encryption" />:</label>
								<input type="text" name="certificateAliasesEncryption" value="${idpAssertionContent.certificateAliasesEncryption}" id="certificateAliasesEncryption">
								<form:errors path="certificateAliasesEncryption"  cssClass="portlet-msg-error"/>
							</div>
							<div>
								<label for="certificateAliasesKeySize"><spring:message code="label.key_size" />:</label>
								<input type="text" name="certificateAliasesKeySize" value="${idpAssertionContent.certificateAliasesKeySize}" id="certificateAliasesKeySize">
							</div>
							<div>
								<label for="certificateAliasesAlgorithm"><spring:message code="label.algorithm" />:</label>
								<input type="text" name="certificateAliasesAlgorithm" value="${idpAssertionContent.certificateAliasesAlgorithm}" id="certificateAliasesAlgorithm">
							</div>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.name_id_format" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.name_id_format_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="nameIDValueMapFormatValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="nidvmcv" items="${idpAssertionContent.nameIDValueMapFormatValues}">
									<input type="hidden" name="nameIDValueMapFormatValues" value="${nidvmcv}" />
								</c:forEach>	
								<select name="nameIDValueMapFormatValuesDeleteValues" multiple="multiple" id="nameIDValueMapFormatValuesDeleteValues">
									<c:forEach var="nidvmcvv" items="${idpAssertionContent.nameIDValueMapFormatValues}">
										<option value="${nidvmcvv}">${nidvmcvv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNameIDValueMapFormatValues" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="nameIDValueMapFormatValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="nameIDValueMapFormatValuesAddValue" type="text" id="nameIDValueMapFormatValuesAddValue">
								<input type="submit" name="addNameIDValueMapFormatValues" value="<spring:message code="action.add" />">
							</div>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.name_id_value_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="nameIDValueMapCurrentValuesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="nidvmfv" items="${idpAssertionContent.nameIDValueMapCurrentValues}">
									<input type="hidden" name="nameIDValueMapCurrentValues" value="${nidvmfv}" />
								</c:forEach>	
								<select name="nameIDValueMapCurrentValuesDeleteValues" multiple="multiple" id="nameIDValueMapCurrentValuesDeleteValues">
									<c:forEach var="nidvmfvv" items="${idpAssertionContent.nameIDValueMapCurrentValues}">
										<option value="${nidvmfvv}">${nidvmfvv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNameIDValueMapCurrentValues" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="nameIDValueMapCurrentValuesAddValue"><spring:message code="label.new_value" /></label>
								<input name="nameIDValueMapCurrentValuesAddValue" type="text" id="nameIDValueMapCurrentValuesAddValue">
								<input type="submit" name="addNameIDValueMapCurrentValues" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.name_id_value_map" /> 
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.authentication_context" /></h3>
					<div>
						<label for="authenticationContextMapper"><spring:message code="label.mapper" />:</label>
						<input type="text" name="authenticationContextMapper" value="${idpAssertionContent.authenticationContextMapper}" id="authenticationContextMapper">
					</div>
					<div>
						<label for="defaultAuthenticationContext">*<spring:message code="label.authentication_context" />:</label>
						<select name="defaultAuthenticationContext" id="defaultAuthenticationContext">
							<c:forEach var="element" items="${idpAssertionContent.samlv2AuthContextList}" varStatus="status" >
								<option value="${element.name}" 
									<c:choose>
										<c:when test="${idpAssertionContent.defaultAuthenticationContext eq element.name}">selected="selected"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${authenticationContextMap[element.name]}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label><spring:message code="label.authentication_context" /></label>
						<table>
							<tr class="odd">
								<th><spring:message code="label.authentication_context.supported" /></th>
								<th><spring:message code="label.authentication_context.context_reference" /></th>
								<th><spring:message code="label.authentication_context.key" /></th>
								<th><spring:message code="label.authentication_context.value" /></th>
								<th><spring:message code="label.authentication_context.level" /></th>
							</tr>
							<c:forEach var="element" items="${idpAssertionContent.samlv2AuthContextList}" varStatus="status" >
								<tr>
									<td>
										<input type="checkbox" value="true" name="samlv2AuthContextList[${status.index}].supported"
										 <c:choose>
											<c:when test="${element.supported eq 'true'}">checked="checked"</c:when>
											<c:otherwise></c:otherwise>
										</c:choose> 
										>
									</td>
									<td>
										${authenticationContextMap[element.name]}
										<input type="hidden" name="samlv2AuthContextList[${status.index}].name" value="${element.name}">
									</td>
									<td>
										<select name="samlv2AuthContextList[${status.index}].key">
											<option value="none"><spring:message code="label.none" /></option>
											<c:forEach var="item" items="${keyList}">
												<option value="${item}"
													<c:choose>
														<c:when test="${element.key eq item}">selected="selected"</c:when>
														<c:otherwise></c:otherwise>
													</c:choose>  
												>${item}</option>
											</c:forEach>
											
										</select>
									</td>
									<td><input type="text" value="${element.value}" name="samlv2AuthContextList[${status.index}].value" ></td>
									<td><input type="text" value="${element.level}" name="samlv2AuthContextList[${status.index}].level"></td>
								</tr>
							</c:forEach>
						</table>	
						<div class="amNote">
							<spring:message code="note.authentication_context" /> 
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.assertion_time" /></h3>
					<div>
						<label for="notBeforeTimeSkew"><spring:message code="label.not_before_time_skew" />:</label>
						<input type="text" name="notBeforeTimeSkew" value="${idpAssertionContent.notBeforeTimeSkew}" id="notBeforeTimeSkew">
						<div class="amNote">
							<spring:message code="note.not_before_time_skew" />
						</div>
					</div>
					<div>
						<label for="effectiveTime"><spring:message code="label.effective_time" />:</label>
						<input type="text" name="effectiveTime" value="${idpAssertionContent.effectiveTime}" id="effectiveTime">
						<div class="amNote">
							<spring:message code="note.effective_time" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.basic_authentication" /></h3>
					<div class="amNote">
						<spring:message code="note.basic_authentication" />
					</div>
					<div>
						<label for="basicauthenticationsettingforSoapbasedbindingEnabled"><spring:message code="label.enabled" />:</label>
						<input type="checkbox" value="true" name="basicauthenticationsettingforSoapbasedbindingEnabled" id="basicauthenticationsettingforSoapbasedbindingEnabled" 
							<c:choose>
								<c:when test="${idpAssertionContent.basicauthenticationsettingforSoapbasedbindingEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</div>
					<div>
						<label for="basicauthenticationsettingforSoapbasedbindingEnabledUserName"><spring:message code="label.user_name" />:</label>
						<input type="text" name="basicauthenticationsettingforSoapbasedbindingEnabledUserName" value="${idpAssertionContent.basicauthenticationsettingforSoapbasedbindingEnabledUserName}" id="basicauthenticationsettingforSoapbasedbindingEnabledUserName">
					</div>
					<div>
						<label for="basicauthenticationsettingforSoapbasedbindingEnabledPassword"><spring:message code="label.basic_authentication_password" />:</label>
						<input type="text" name="basicauthenticationsettingforSoapbasedbindingEnabledPassword" value="${idpAssertionContent.basicauthenticationsettingforSoapbasedbindingEnabledPassword}" id="basicauthenticationsettingforSoapbasedbindingEnabledPassword">
					</div>
					<hr>
					
					<h3><spring:message code="label.cache" /> </h3>
					<div>
						<label>&nbsp;</label>
						<input type="checkbox" value="true" name="assertionCacheEnabled" id="assertionCacheEnabled"  
							<c:choose>
								<c:when test="${idpAssertionContent.assertionCacheEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="assertionCacheEnabled"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cache" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.bootstrapping" /></h3>
					<div>
						<label><spring:message code="title.bootstrapping" /></label>
						<input type="checkbox" value="true" name="discoveryBootstrappingEnabled" id="discoveryBootstrappingEnabled"
							<c:choose>
								<c:when test="${idpAssertionContent.discoveryBootstrappingEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="discoveryBootstrappingEnabled"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.bootstrapping" />
						</div>
					</div>
					
				</form:form>
			</div>
		</div>
	</div>
</div>
		