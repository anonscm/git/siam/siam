<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<spring:message code="breadcrumb.federation" />
	</div>

	<h3><spring:message code="title.new_entity_provider" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="entityP">
		<input type="hidden" name="action" value="doAddEntityProvider" />
		
		<div class="amFormCell">
			<label for="name"><spring:message code="label.entity_provider_name" />:</label>
			 <input type="text" name="name" id="name">
		</div>
		<div class="amFormCell">
			<label for="type"><spring:message code="label.entity_provider_type" /></label>
			<select name="type" id="type">
				<c:forEach var="protocol" items="${entityProviderProtocolList}">
					<option value="${protocol}"><spring:message code="label.${protocol}"/></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" value="<spring:message code="action.create" />">
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
	</form:form>

	<h3><spring:message code="title.configured_entity_providers" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_delete" method="post">
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllEntityProviders eq checkAll}">
							<input type="submit" name="checkForDeleteEntityProviders" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllEntityProviders" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteEntityProviders" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllEntityProviders" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.protocol" /></th>
				<th><spring:message code="label.type" /></th>
				<th><spring:message code="label.location" /></th>
				<th>
					<spring:message code="label.name" />
				</th>
				<th>	
					<div class="amDivButtons">
						<input type="submit" name="deleteEntityProvider" value="<spring:message code="action.delete_selected" />">
						<input type="submit" name="importEntityProvider" value="<spring:message code="action.import" />">
					</div>
				</th>
			</tr>
			<c:forEach var="entityProvider" items="${entityProvidersList}">
				<tr>
					<td class="amInputCell">
						<input name="deleteEntityProviderList" value="${entityProvider.entityIdentifier}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllEntityProviders eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
						>
					</td>
					<td>${entityProvider.protocol}</td>
					<td>
						<c:forEach var="service" items="${entityProvider.descriptorMap}" varStatus="loop">
							<c:if test="${service.key ne 'general'}">
								<c:choose>
									<c:when test="${!loop.first}">, </c:when>
								</c:choose>
								<spring:message code="title.service.${service.key}.short" />
							</c:if>
						</c:forEach>
					</td>
					<td>${entityProvider.location}</td>
					<td>
						<portlet:renderURL var="editEntityProviderURL">
							<portlet:param name="ctx" value="EntityProviderEditController"/>
							<portlet:param name="entityIdentifier" value="${entityProvider.entityIdentifier}"/>
							<portlet:param name="protocol" value="${entityProvider.protocol}"/>
						</portlet:renderURL>
						<c:if test="${not empty entityProvider.entityIdentifier}">
							<a href="${editEntityProviderURL}">${entityProvider.entityIdentifier}</a>
						</c:if>
					</td>
					<td>
						<portlet:resourceURL var="exportEntityProviderMetadataURL" id="exportEntityProviderMetadata" >
							<portlet:param name="entityName" value="${entityProvider.entityIdentifier}"/>
							<portlet:param name="entityType" value="${entityProvider.protocol}"/>
						</portlet:resourceURL>
						<portlet:resourceURL var="exportEntityProviderExtendedDataURL" id="exportEntityProviderExtendedData" >
							<portlet:param name="entityName" value="${entityProvider.entityIdentifier}"/>
							<portlet:param name="entityType" value="${entityProvider.protocol}"/>
						</portlet:resourceURL>
						<spring:message code="action.export" />: &nbsp;
						<a href="${exportEntityProviderMetadataURL}"><spring:message code="action.export_metadata" /></a>&nbsp;
						<a href="${exportEntityProviderExtendedDataURL}"><spring:message code="action.export_extended_data" /></a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>	
	<hr>

	<h3><spring:message code="title.new_circle_of_trust" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="entity">
		<input type="hidden" name="action" value="doAddCircleOfTrust" />
	
		<div class="amFormCell">
			<label for="circle_of_trust_name"><spring:message code="label.circle_of_trust_name" />: </label>
			<input type="text" name="name" id="circle_of_trust_name">
		</div>
		<input type="hidden" name="type" value="ignorMe">
		<input type="submit" value="<spring:message code="action.create" />">
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
	</form:form>

	<h3><spring:message code="label.configured_circles_of_trust" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_delete" method="post">
		<table >
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllCircleOfTrust eq checkAll}">
							<input type="submit" name="checkForDeleteCircleOfTrust" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllCircleOfTrust" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteCircleOfTrust" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllCircleOfTrust" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.name" /></th>
				<th><spring:message code="label.status" /></th>
				<th>
					<spring:message code="label.entities" />
					<div class="amDivButtons">
						<input type="submit" name="deleteCircleOfTrust" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="circleOfTrust" items="${circleOfTrustList}">
				<tr>
					<td class="amInputCell">
						<input name="deleteCircleOfTrustList" value="${circleOfTrust.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllCircleOfTrust eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td>
						<portlet:renderURL var="editCircleOfTrustURL">
							<portlet:param name="ctx" value="CircleOfTrustController"/>
							<portlet:param name="circleOfTrustName" value="${circleOfTrust.name}"/>
							<portlet:param name="action" value="<%= CommonConstants.ACTION_UPDATE %>"/>
						</portlet:renderURL>
						<a href="${editCircleOfTrustURL}">${circleOfTrust.name}</a>
					</td>
					<td>${circleOfTrust.status}</td>
					<td>
						<c:forEach var="entitie" items="${circleOfTrust.trustedProviders}">
							${entitie}<br>
						</c:forEach>
					</td>
				</tr>
			</c:forEach>
		</table>

	</form:form>	

</div>
