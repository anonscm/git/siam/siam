<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="federationUrl"></portlet:renderURL>
		<a href="${federationUrl}"><spring:message code="breadcrumb.federation" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.circle_of_trust" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="circleOfTrust">
		<input type="hidden" name="ctx" value="CircleOfTrustController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="circleOfTrustName" value="${circleOfTrustName}" />
		
		<div class="amDivButtons"> 
			<input type="submit" name="save"  value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.circle_of_trust" /></h3>
		<div>
			<label for="name">*<spring:message code="label.name" />:</label>
			<c:set var="actionUpdate"><%= CommonConstants.ACTION_UPDATE %></c:set>
			<c:choose>
				<c:when test="${actionValue eq actionUpdate}">
					${circleOfTrust.name}
					<input type="hidden" name="name" value="${circleOfTrust.name}">
				</c:when>
				<c:otherwise>
					<input type="text" name="name" value="${circleOfTrust.name}" id="name">
				</c:otherwise>
			</c:choose>
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="description"><spring:message code="label.circle_description" />:</label>
			<input type="text" name="description" value="${circleOfTrust.description}" id="description">
		</div>
		<div>
			<label for="idffWriterServiceURL"><spring:message code="label.idff_writer_service_url" />:</label>
			<input type="text" name="idffWriterServiceURL" value="${circleOfTrust.idffWriterServiceURL}" id="idffWriterServiceURL">
			<div class="amNote">
				<spring:message code="note.idff_writer_service_url" />
			</div>
			<form:errors path="idffWriterServiceURL" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="idffReaderServiceURL"><spring:message code="label.idff_reader_service_url" />:</label>
			<input type="text" name="idffReaderServiceURL" value="${circleOfTrust.idffReaderServiceURL}" id="idffReaderServiceURL">
			<div class="amNote">
				<spring:message code="note.idff_reader_service_url" />
			</div>	
			<form:errors path="idffReaderServiceURL" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="saml2WriterServiceURL"><spring:message code="label.saml2_writer_service_url" />:</label>
			<input type="text" name="saml2WriterServiceURL" value="${circleOfTrust.saml2WriterServiceURL}" id="saml2WriterServiceURL">
			<div class="amNote">
				<spring:message code="note.saml2_writer_service_url" />
			</div>	
			<form:errors path="saml2WriterServiceURL" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="saml2ReaderServiceURL"><spring:message code="label.saml2_reader_service_url" />:</label>
			<input type="text" name="saml2ReaderServiceURL" value="${circleOfTrust.saml2ReaderServiceURL}" id="saml2ReaderServiceURL">
			<div class="amNote">
				<spring:message code="note.saml2_reader_service_url" />
			</div>	
			<form:errors path="saml2ReaderServiceURL" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label><spring:message code="label.status" /></label>
			<div class="amFormMultipleInputs">
				<input type="radio" value="active" name="status" id="statusActive"
					<c:choose>
						<c:when test="${circleOfTrust.status eq 'active'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>  >
				<label class="amInputLabel" for="statusActive"><spring:message code="label.active" /></label>
				<br>
				<input type="radio" value="inactive" name="status" id="statusInactive"
					<c:choose>
						<c:when test="${circleOfTrust.status eq 'inactive'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="statusInactive"><spring:message code="label.inactive" /></label>
			</div>
		</div>
		<div>
			<label><spring:message code="label.realm" />:</label>
			${circleOfTrust.realm}
			<input type="hidden" name="realm" value="${circleOfTrust.realm}"/>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="title.entity_provider" /> </label>
			<div class="amNote">
				<spring:message code="note.cirle_entity_providers" />
			</div>
		</div>
		<div class="amFormCell">
			<label for="availableEntitiesSelected"><spring:message code="label.available" /></label>
			<br>
			<c:forEach var="ae" items="${availabelEntities}">
				<input type="hidden" name=allAvailabelEntities value="${ae}" />
			</c:forEach>
			<select multiple="multiple" name="availableEntitiesSelected" id="availableEntitiesSelected">
				<c:forEach var="aev" items="${availabelEntities}">
					<option value="${aev}">${aev}</option>
				</c:forEach>
				<option value="ignoreMe">________________________</option>
			</select>
		</div>
		<div class="amFormCell">
			<br>
			<input type="submit" name="addEntities" value="<spring:message code="action.add" />"/><br>
			<input type="submit" name="addAllEntities" value="<spring:message code="action.add_all" />"/><br>
			<input type="submit" name="removeEntities" value="<spring:message code="action.remove" />"/><br>
			<input type="submit" name="removeAllEntities" value="<spring:message code="action.remove_all" />"/><br>
			<input type="submit" name="moveUpEntities" value="<spring:message code="action.up" />"/><br>
			<input type="submit" name="moveDownEntities" value="<spring:message code="action.down" />"/>
		</div>
		<div class="amFormCell">
			<label for="selectedEntities"><spring:message code="label.selected" /></label>
			<br>
			<c:forEach var="tp" items="${circleOfTrust.trustedProviders}">
				<input type="hidden" name="trustedProviders" value="${tp}" />
			</c:forEach>
			<select multiple="multiple" name="selectedEntities" id="selectedEntities">
				<c:forEach var="value" items="${circleOfTrust.trustedProviders}">
					<option value="${value}">${value}</option>
				</c:forEach>
				<option value="ignoreMe">________________________</option>
			</select>
		</div>
	</form:form>

</div>
