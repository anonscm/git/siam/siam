<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.edit_local_agent" />
	</div>

	<h3> <spring:message code="title.edit_agent" />: ${name} </h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="localAgent">
		<input type="hidden" name="ctx" value="EditLocalController" />
		<input type="hidden" name="name" value="${name}">
		<input type="hidden" name="type" value="${localAgent.type}">
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		<div>
			<label for="password"><spring:message code="label.password" />:</label>
			<input name="password" value="${localAgent.password}" type="password" id="password"> 
			<form:errors path="password" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="passwordConfirm"><spring:message code="label.password_confirm" />:</label>
			<input name="passwordConfirm" value="${localAgent.passwordConfirm}" type="password" id="passwordConfirm"> 
			<form:errors path="passwordConfirm" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label><spring:message code="label.status" />:</label>
			<input name="status" type="radio" value="Active" id="statusActive"
				<c:choose>
					<c:when test="${localAgent.status eq 'Active'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="statusActive">
				<spring:message code="label.active" />
			</label>
			&nbsp;
			<input name="status" type="radio" value="Inactive" id="statusInactive"
				<c:choose>
					<c:when test="${localAgent.status eq 'Inactive'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="statusInactive">
				<spring:message code="label.inactive" />
			</label> 
			<form:errors path="status" cssClass="portlet-msg-error"></form:errors>	
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.agent_root_url_for_cdsso" /></label>
			<div class="amFormMultipleValues">
				<div>
					<label for="agentRootURLforCDSSODeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="arufc" items="${localAgent.agentRootURLforCDSSO}">
						<input type="hidden" name="agentRootURLforCDSSO" value="${arufc}" />
					</c:forEach>	
					<select name="agentRootURLforCDSSODeleteValues" multiple="multiple" id="agentRootURLforCDSSODeleteValues"> 
						<c:forEach var="arufcv" items="${localAgent.agentRootURLforCDSSO}">
							<option value="${arufcv}">${arufcv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteAgentRootURLforCDSSO" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="agentRootURLforCDSSOAddValue"><spring:message code="label.new_value" /></label>
					<input name="agentRootURLforCDSSOAddValue" type="text" id="agentRootURLforCDSSOAddValue">
					<input type="submit" name="addAgentRootURLforCDSSO" value="<spring:message code="action.add" />">
				</div>
			</div>
			<div class="amNote">
				<spring:message code="note.the_agent_root_url_for_cdsso" />
			</div>
			<form:errors path="agentRootURLforCDSSO" cssClass="portlet-msg-error"></form:errors>
		</div>
	</form:form>	

</div>
