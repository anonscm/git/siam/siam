<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.add_agent" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="agent">
		<input type="hidden" name="ctx" value="AddAgentController" />
		<input type="hidden" name="type" value="${type}">
		
		<c:if test="${not empty errorKey}">
			<div class="portlet-msg-error"><spring:message code="${errorKey}"/></div>
		</c:if>
		
		<div class="amDivButtons"> 
			<input type="submit" name="addAgent" value="<spring:message code="action.save" />">
			<input type="submit" name="cancelAgent" value="<spring:message code="action.cancel" />">
		</div>
	
		<h3><spring:message code="title.new_agent" /> - ${type} </h3>
		<div>
			<label for="name">*<spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${name}" id="name">
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="password">*<spring:message code="label.password" />:</label>
			<input type="password" name="password" value="${agent.password}" id="password">
			<form:errors path="password" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="reEnterPassword">*<spring:message code="label.password_confirm" />:</label>
			<input type="password" name="reEnterPassword" value="${agent.reEnterPassword}" id="reEnterPassword"> 
			<form:errors path="reEnterPassword" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label><spring:message code="label.configuration" />:</label>
			<div class="amFormMultipleInputs">
				<input type="radio" id="configLocal" name="configuration" value="local" 
					<c:choose>
						<c:when test="${agent.configuration eq 'local'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="configLocal"><spring:message code="label.configuration_local" /></label>
				&nbsp;
				<input type="radio" id="configCentralized" name="configuration" value="centralized"
					<c:choose>
						<c:when test="${agent.configuration ne 'local'}">checked="checked"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
				<label class="amInputLabel" for="configCentralized"><spring:message code="label.configuration_centralized" /></label>
			</div>
			<div class="amNote">
				<spring:message code="note.configuration" />
			</div>
			<form:errors path="configuration" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="serverURL"><spring:message code="label.server_url" />:</label>
			<input type="text" name="serverURL" value="${agent.serverURL}" id="serverURL">
			<div class="amNote">
				<spring:message code="note.server_url" />
			</div>
			<form:errors path="serverURL" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="agentURL">*<spring:message code="label.agent_url" />:</label>
			<input type="text" name="agentURL" value="${agent.agentURL}" id="agentURL">
			<div class="amNote">
				<spring:message code="note.agent_url.${type}" />
			</div>
			<form:errors path="agentURL" cssClass="portlet-msg-error"></form:errors>
		</div>
	</form:form>	

</div>