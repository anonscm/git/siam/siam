<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_advanced" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
					<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editWebAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editWebAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><b><spring:message code="label.menu.advanced" /></b></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="webAgent">
					<input type="hidden" name="ctx" value="WebAgentAdvancedController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.client_identification" /></h3>
					<div>
						<label for="clientIPAddressHeader"><spring:message code="label.client_iP_address_header" />:</label>
						<input name="clientIPAddressHeader" value="${webAgent.clientIPAddressHeader}" type="text" id="clientIPAddressHeader">
						<div class="amNote">
							<spring:message code="note.client_iP_address_header" />
						</div>
					</div>
					<div>
						<label for="clientHostnameHeader"><spring:message code="label.client_hostname_header" />:</label>
						<input name="clientHostnameHeader" value="${webAgent.clientHostnameHeader}" type="text" id="clientHostnameHeader">
						<div class="amNote">
							<spring:message code="note.client_hostname_header" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.load_balancer" /></h3>
					<div>
						<label><spring:message code="label.load_balancer_setup" />:</label>
						<input name="loadBalancerSetup" value="true" type="checkbox" id="loadBalancerSetup" 
							<c:choose>
								<c:when test="${webAgent.loadBalancerSetup eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="loadBalancerSetup"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.load_balancer_setup" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.override_request_url_protocol" />:</label>
						<input name="overrideRequestURLProtocol" value="true" type="checkbox" id="overrideRequestURLProtocol"
							<c:choose>
								<c:when test="${webAgent.overrideRequestURLProtocol eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="overrideRequestURLProtocol"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.override_request_url_protocol" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.override_request_url_host" />:</label>
						<input name="overrideRequestURLHost" value="true" type="checkbox" id="overrideRequestURLHost" 
							<c:choose>
								<c:when test="${webAgent.overrideRequestURLHost eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="overrideRequestURLHost"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.override_request_url_host" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.override_request_url_port" />:</label>
						<input name="overrideRequestURLPort" value="true" type="checkbox" id="overrideRequestURLPort" 
							<c:choose>
								<c:when test="${webAgent.overrideRequestURLPort eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="overrideRequestURLPort"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.override_request_url_port" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.override_notification_url" />:</label>
						<input name="overrideNotificationURL" value="true" type="checkbox" id="overrideNotificationURL" 
							<c:choose>
								<c:when test="${webAgent.overrideNotificationURL eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="overrideNotificationURL"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.override_notification_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.post_data_preservation" /></h3>
					<div>
						<label><spring:message code="label.post_data_preservation" />:</label>
						<input name="amInputLabel" value="true" type="checkbox" id="amInputLabel"
							<c:choose>
								<c:when test="${webAgent.postDataPreservation eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="amInputLabel"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.post_data_preservation" />
						</div>
					</div>
					<div>
						<label for="postDataEntriesCachePeriod"><spring:message code="label.post_data_entries_cache_period" />:</label>
						<input name="postDataEntriesCachePeriod" value="${webAgent.postDataEntriesCachePeriod}" type="text" id="postDataEntriesCachePeriod">
						<div class="amNote">
							<spring:message code="note.post_data_entries_cache_period" />
						</div>
						<form:errors path="postDataEntriesCachePeriod" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.sun_java_system_proxy_server" /></h3>
					<div>
						<label><spring:message code="label.override_proxy_server_s_host_and_port" />:</label>
						<input name="overrideProxyServerHostAndPort" value="true" type="checkbox" id="overrideProxyServerHostAndPort" 
							<c:choose>
								<c:when test="${webAgent.overrideProxyServerHostAndPort eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="overrideProxyServerHostAndPort"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.override_proxy_server_s_host_and_port" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.microsoft_iis_server" /></h3>
					<div>
						<label for="authenticationType"><spring:message code="label.authentication_type" /> :</label>
						<input name="authenticationType" value="${webAgent.authenticationType}" type="text" id="authenticationType">
						<div class="amNote">
							<spring:message code="note.authentication_type" />
						</div>		
					</div>
					<div>
						<label for="replayPasswordKey"><spring:message code="label.replay_password_key" />:</label>
						<input name="replayPasswordKey" value="${webAgent.replayPasswordKey}" type="text" id="replayPasswordKey">
						<div class="amNote">
							<spring:message code="note.replay_password_key" />
						</div>	
					</div>
					<div>
						<label><spring:message code="label.filter_priority" />:</label>
						<div class="amFormMultipleInputs">
							<input  name="filterPriority" value="DEFAULT" type="radio" id="filterPriorityDefautl" 
								<c:choose>
									<c:when test="${webAgent.filterPriority eq 'DEFAULT'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="filterPriorityDefautl"><spring:message code="label.default" /></label>
							<br>
							<input  name="filterPriority" value="HIGH" type="radio" id="filterPriorityHigh" 
								<c:choose>
									<c:when test="${webAgent.filterPriority eq 'HIGH'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="filterPriorityHigh"><spring:message code="label.high" /></label>
							<br>
							<input  name="filterPriority" value="LOW" type="radio" id="filterPriorityLow" 
								<c:choose>
									<c:when test="${webAgent.filterPriority eq 'LOW'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="filterPriorityLow"><spring:message code="label.low" /></label>
							<br>
							<input  name="filterPriority" value="MEDIUM" type="radio" id="filterPriorityMedium" 
								<c:choose>
									<c:when test="${webAgent.filterPriority eq 'MEDIUM'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="filterPriorityMedium"><spring:message code="label.medium" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.filter_priority" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.filter_configured_with_owa" /> :</label>
						<input  name="filterconfiguredWithOWA" value="true" type="checkbox" id="filterconfiguredWithOWA" 
							<c:choose>
								<c:when test="${webAgent.filterconfiguredWithOWA eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="filterconfiguredWithOWA"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.filter_configured_with_owa" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.change_url_protocol_to_https" /> :</label>
						<input  name="changeURLProtocolToHttps" value="true" type="checkbox" id="changeURLProtocolToHttps" 
							<c:choose>
								<c:when test="${webAgent.changeURLProtocolToHttps eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="changeURLProtocolToHttps"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.change_url_protocol_to_https" />
						</div>
					</div>
					<div>
						<label for="idleSessionTimeoutPageURL"><spring:message code="label.idle_session_timeout_page_url" />:</label>
						<input name="idleSessionTimeoutPageURL" value="${webAgent.idleSessionTimeoutPageURL}" type="text" id="idleSessionTimeoutPageURL">
						<div class="amNote">
							<spring:message code="note.idle_session_timeout_page_url" />
						</div> 
					</div>
					<hr>
					
					<h3><spring:message code="title.ibm_lotus_domino_server" /></h3>
					<div>
						<label><spring:message code="label.check_user_in_domino_database" />:</label>
						<input  name="checkUserInDominoDatabase" value="true" type="checkbox" id="checkUserInDominoDatabase" 
							<c:choose>
								<c:when test="${webAgent.checkUserInDominoDatabase eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="checkUserInDominoDatabase"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.check_user_in_domino_database" />
						</div> 
					</div>
					<div>
						<label><spring:message code="label.use_ltpa_token" />:</label>
						<input  name="useLTPAToken" value="true" type="checkbox" id="useLTPAToken" 
							<c:choose>
								<c:when test="${webAgent.useLTPAToken eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="useLTPAToken"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.use_ltpa_token" />
						</div>
					</div>
					<div>
						<label for="ltpaTokenCookieName"><spring:message code="label.ltpa_token_cookie_name" />:</label>
						<input name="ltpaTokenCookieName" value="${webAgent.ltpaTokenCookieName}" type="text" id="ltpaTokenCookieName">
						<div class="amNote">
							<spring:message code="note.ltpa_token_cookie_name" />
						</div> 
					</div>
					<div>
						<label for="ltpaTokenConfigurationName"><spring:message code="label.ltpa_token_configuration_name" />:</label>
						<input name="ltpaTokenConfigurationName" value="${webAgent.ltpaTokenConfigurationName}" type="text" id="ltpaTokenConfigurationName">
						<div class="amNote">
							<spring:message code="note.ltpa_token_configuration_name" />
						</div> 
					</div>
					<div>
						<label for="ltpaTokenOrganizationName"><spring:message code="label.ltpa_token_organization_name" />:</label>
						<input name="ltpaTokenOrganizationName" value="${webAgent.ltpaTokenOrganizationName}" type="text" id="ltpaTokenOrganizationName">
						<div class="amNote">
							<spring:message code="note.ltpa_token_organization_name" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.custom_properties" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_properties" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customPropertiesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="arufc" items="${webAgent.customProperties}">
									<input type="hidden" name="customProperties" value="${arufc}" />
								</c:forEach>	
								<select name="customPropertiesDeleteValues" multiple="multiple" id="customPropertiesDeleteValues">
									<c:forEach var="arufcv" items="${webAgent.customProperties}">
										<option value="${arufcv}">${arufcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomProperties" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="customPropertiesAddValue"><spring:message code="label.new_value" /></label>
								<input name="customPropertiesAddValue" type="text" id="customPropertiesAddValue">
								<input type="submit" name="addCustomProperties" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_properties" />
						</div>
					</div>
					
				</form:form>	
			</div>
		</div>	
	</div>
</div>








