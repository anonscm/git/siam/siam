<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div  class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_open_sso_services" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
					<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editWebAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editWebAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><b><spring:message code="label.menu.opensso_services" /></b></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="webAgent">
					<input type="hidden" name="ctx" value="WebAgentOpenSSOServicesController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.login_url" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.opensso_login_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="openSSOLoginURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ossoliurl" items="${webAgent.openSSOLoginURL}">
									<input type="hidden" name="openSSOLoginURL" value="${ossoliurl}" />
								</c:forEach>	
								<select name="openSSOLoginURLSelectValues" multiple="multiple" id="openSSOLoginURLSelectValues">
									<c:forEach var="ossoliurlv" items="${webAgent.openSSOLoginURL}">
										<option value="${ossoliurlv}">${ossoliurlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpOpenSSOLoginURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownOpenSSOLoginURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopOpenSSOLoginURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomOpenSSOLoginURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deleteOpenSSOLoginURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="openSSOLoginURLAddValue"><spring:message code="label.new_value" /></label>
								<input name="openSSOLoginURLAddValue" type="text" id="openSSOLoginURLAddValue">
								<input type="submit" name="addOpenSSOLoginURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.opensso_login_url" />
						</div>
					</div>
					<div>
						<label for="agentConnectionTimeout"><spring:message code="label.agent_connection_timeout" />:</label>
						<input name="agentConnectionTimeout" value="${webAgent.agentConnectionTimeout}" type="text" id="agentConnectionTimeout">
						<div class="amNote">
							<spring:message code="note.agent_connection_timeout" />
						</div>	
						<form:errors path="agentConnectionTimeout" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="pollingPeriodForPrimaryServer"><spring:message code="label.polling_period_for_primary_server" />:</label>
						<input name="pollingPeriodForPrimaryServer" value="${webAgent.pollingPeriodForPrimaryServer}" type="text" id="pollingPeriodForPrimaryServer">
						<div class="amNote">
							<spring:message code="note.polling_period_for_primary_server" />
						</div>
						<form:errors path="pollingPeriodForPrimaryServer" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.logout_url" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.opensso_logout_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="openSSOLogoutURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ossolourl" items="${webAgent.openSSOLogoutURL}">
									<input type="hidden" name="openSSOLogoutURL" value="${ossolourl}" />
								</c:forEach>	
								<select name="openSSOLogoutURLSelectValues" multiple="multiple" id="openSSOLogoutURLSelectValues">
									<c:forEach var="ossolourlv" items="${webAgent.openSSOLogoutURL}">
										<option value="${ossolourlv}">${ossolourlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpOpenSSOLogoutURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownOpenSSOLogoutURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopOpenSSOLogoutURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomOpenSSOLogoutURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deleteOpenSSOLogoutURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="openSSOLogoutURLAddValue"><spring:message code="label.new_value" /></label>
								<input name="openSSOLogoutURLAddValue" type="text" id="openSSOLogoutURLAddValue">
								<input type="submit" name="addOpenSSOLogoutURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.opensso_logout_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.agent_logout_url" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.logout_url_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="logoutURLListSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="lurll" items="${webAgent.logoutURLList}">
									<input type="hidden" name="logoutURLList" value="${lurll}" />
								</c:forEach>	
								<select name="logoutURLListSelectValues" multiple="multiple" id="logoutURLListSelectValues">
									<c:forEach var="lurllv" items="${webAgent.logoutURLList}">
										<option value="${lurllv}">${lurllv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpLogoutURLList" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownLogoutURLList" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopLogoutURLList" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomLogoutURLList" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deleteLogoutURLList" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="logoutURLListAddValue"><spring:message code="label.new_value" /></label>
								<input name="logoutURLListAddValue" type="text" id="logoutURLListAddValue">
								<input type="submit" name="addLogoutURLList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.logout_url_list" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.logout_cookies_list_for_reset" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="logoutCookiesListForResetDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="lclfr" items="${webAgent.logoutCookiesListForReset}">
									<input type="hidden" name="logoutCookiesListForReset" value="${lclfr}" />
								</c:forEach>	
								<select name="logoutCookiesListForResetDeleteValues" multiple="multiple" id="logoutCookiesListForResetDeleteValues">
									<c:forEach var="lclfrv" items="${webAgent.logoutCookiesListForReset}">
										<option value="${lclfrv}">${lclfrv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLogoutCookiesListForReset" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="logoutCookiesListForResetAddValue"><spring:message code="label.new_value" /></label>
								<input name="logoutCookiesListForResetAddValue" type="text" id="logoutCookiesListForResetAddValue">
								<input type="submit" name="addLogoutCookiesListForReset" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.logout_cookies_list_for_reset" />
						</div>
					</div>
					<div>
						<label for="logoutRedirectURL"><spring:message code="label.logout_redirect_url" />:</label>
						<input name="logoutRedirectURL" value="${webAgent.logoutRedirectURL}"  type="text" id="logoutRedirectURL">
						<div class="amNote">
							<spring:message code="note.logout_redirect_url" />
						</div>	
					</div>
					<hr>
					
					<h3><spring:message code="title.policy_client_service" /></h3>
					<div>
						<label for="policyCachePollingPeriod"><spring:message code="label.policy_cache_polling_period" />:</label>
						<input name="policyCachePollingPeriod" value="${webAgent.policyCachePollingPeriod}"  type="text" id="policyCachePollingPeriod">
						<div class="amNote">
							<spring:message code="note.policy_cache_polling_period" />
						</div>	
						<form:errors path="policyCachePollingPeriod" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="ssoCachePollingPeriod"><spring:message code="label.sso_cache_polling_period" />:</label>
						<input name="ssoCachePollingPeriod" value="${webAgent.ssoCachePollingPeriod}"  type="text" id="ssoCachePollingPeriod">
						<div class="amNote">
							<spring:message code="note.sso_cache_polling_period" />
						</div>
						<form:errors path="ssoCachePollingPeriod" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="userIDParameter"><spring:message code="label.user_id_parameter" />:</label>
						<input name="userIDParameter" value="${webAgent.userIDParameter}"  type="text" id="userIDParameter">
						<div class="amNote">
							<spring:message code="note.user_id_parameter" />
						</div>	
					</div>
					<div>
						<label for="userIDParameterType"><spring:message code="label.user_id_parameter_type" />:</label>
						<input name="userIDParameterType" value="${webAgent.userIDParameterType}"  type="text" id="userIDParameterType">
						<div class="amNote">
							<spring:message code="note.user_id_parameter_type" />
						</div>	
					</div>
					<div>
						<label><spring:message code="label.fetch_policies_from_root_resource" />:</label>
						<input name="fetchPoliciesFromRootResource" value="true"  type="checkbox" id="fetchPoliciesFromRootResource" 
							<c:choose>
								<c:when test="${webAgent.fetchPoliciesFromRootResource eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  > 
						<label class="amInputLabel" for="fetchPoliciesFromRootResource"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.fetch_policies_from_root_resource" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.retrieve_client_hostname" />:</label>
						<input name="retrieveClientHostname" value="true"  type="checkbox" id="retrieveClientHostname" 
							<c:choose>
								<c:when test="${webAgent.retrieveClientHostname eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> > 
						<label class="amInputLabel" for="retrieveClientHostname"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.retrieve_client_hostname" />
						</div>	
					</div>
					<div>
						<label for="policyClockSkew"><spring:message code="label.policy_clock_skew" />:</label>
						<input name="policyClockSkew" value="${webAgent.policyClockSkew}"  type="text" id="policyClockSkew"> 
						<div class="amNote">
							<spring:message code="note.policy_clock_skew" />
						</div>	
						<form:errors path="policyClockSkew" cssClass="portlet-msg-error"></form:errors>
					</div>
				
				</form:form>
			</div>
		</div>
	</div>

</div>

