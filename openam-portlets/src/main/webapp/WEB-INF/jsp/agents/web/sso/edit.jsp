<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_sso" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>	
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
					<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editWebAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editWebAgentSSOURL}"><b><spring:message code="label.menu.sso" /></b></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="webAgent">
					<input type="hidden" name="ctx" value="WebAgentSSOController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.cookie" /></h3>
					<div>
						<label for="cookieName"><spring:message code="label.cookie_name" />:</label>
						<input name="cookieName" value="${webAgent.cookieName}" type="text" id="cookieName">
						<div class="amNote">
							<spring:message code="note.cookie_name" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.cookie_security" />:</label>
						<input name="cookieSecurity" value="true" type="checkbox" id="cookieSecurity" 
							<c:choose>
								<c:when test="${webAgent.cookieSecurity eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="cookieSecurity"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cookie_security" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.cross_domain_sso" /></h3>
					<div>
						<label><spring:message code="label.cross_domain_sso" />:</label>
						<input name="crossDomainSSO" value="true" type="checkbox" id="crossDomainSSO" 
							<c:choose>
								<c:when test="${webAgent.crossDomainSSO eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="crossDomainSSO"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cross_domain_sso" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cdsso_servlet_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cdssoServletURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="csu" items="${webAgent.cdssoServletURL}">
									<input type="hidden" name="cdssoServletURL" value="${csu}" />
								</c:forEach>	
								<select name="cdssoServletURLSelectValues" multiple="multiple" id="cdssoServletURLSelectValues">
									<c:forEach var="csuv" items="${webAgent.cdssoServletURL}">
										<option value="${csuv}">${csuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpCdssoServletURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownCdssoServletURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopCdssoServletURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomCdssoServletURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deleteCdssoServletURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="cdssoServletURLAddValue"><spring:message code="label.new_value" /></label>
								<input name="cdssoServletURLAddValue" type="text" id="cdssoServletURLAddValue">
								<input type="submit" name="addCdssoServletURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cdsso_servlet_url" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cookies_domain_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cookiesDomainListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="cdl" items="${webAgent.cookiesDomainList}">
									<input type="hidden" name="cookiesDomainList" value="${cdl}" />
								</c:forEach>	
								<select name="cookiesDomainListDeleteValues" multiple="multiple" id="cookiesDomainListDeleteValues">
									<c:forEach var="cdlv" items="${webAgent.cookiesDomainList}">
										<option value="${cdlv}">${cdlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCookiesDomainList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="cookiesDomainListAddValue"><spring:message code="label.new_value" /></label>
								<input name="cookiesDomainListAddValue" type="text" id="cookiesDomainListAddValue">
								<input type="submit" name="addCookiesDomainList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cookies_domain_list" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.cookie_reset" /></h3>
					<div>
						<label><spring:message code="label.cookie_reset" />:</label>
						<input name="cookieReset" value="true" type="checkbox" id="cookieReset" 
							<c:choose>
								<c:when test="${webAgent.cookieReset eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="cookieReset"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cookie_reset" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cookies_reset_name_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cookiesResetNameListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="crnl" items="${webAgent.cookiesResetNameList}">
									<input type="hidden" name="cookiesResetNameList" value="${crnl}" />
								</c:forEach>	
								<select name="cookiesResetNameListDeleteValues" multiple="multiple" id="cookiesResetNameListDeleteValues">
									<c:forEach var="crnlv" items="${webAgent.cookiesResetNameList}">
										<option value="${crnlv}">${crnlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCookiesResetNameList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="cookiesResetNameListAddValue"><spring:message code="label.new_value" /></label>
								<input name="cookiesResetNameListAddValue" type="text" id="cookiesResetNameListAddValue">
								<input type="submit" name="addCookiesResetNameList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cookies_reset_name_list" />
						</div>
					</div>
					
				</form:form>	
			</div>
		</div>	
	</div>
</div>