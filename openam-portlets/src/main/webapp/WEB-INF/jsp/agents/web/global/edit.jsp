<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_global" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
					<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><b><spring:message code="label.menu.global" /></b></a></li>
					<li><a href="${editWebAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editWebAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="webAgent">
					<input type="hidden" name="ctx" value="WebAgentGlobalController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.profile" /></h3>
					<div>
						<label for="group"><spring:message code="label.group" />:</label>
						<select name="group" id="group">
							<option value=""><spring:message code="action.none" /></option>
							<c:forEach var="item" items="${groupsList}">
								<option value="${item}"
									<c:choose>
										<c:when test="${item eq webAgent.group}">selected="selected"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${item}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label for="password">*<spring:message code="label.password" />:</label>
						<input name="password" value="${webAgent.password}" type="password" id="password"> 
						<form:errors path="password" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="passwordConfirm">*<spring:message code="label.password_confirm" />:</label>
						<input name="passwordConfirm" value="${webAgent.passwordConfirm}" type="password" id="passwordConfirm"> 
						<form:errors path="passwordConfirm" cssClass="portlet-msg-error"></form:errors>	
					</div>
					<div>
						<label>*<spring:message code="label.status" />:</label>
						<div class="amFormMultipleInputs">
							<input name="status" type="radio" value="Active" id="statusActive" 
								<c:choose>
									<c:when test="${webAgent.status eq 'Active'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="statusActive"><spring:message code="label.active" /></label>
							<br>
							<input name="status" type="radio" value="Inactive" id="statusInactive" 
								<c:choose>
									<c:when test="${webAgent.status eq 'Inactive'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="statusInactive"><spring:message code="label.inactive" /></label>
						</div>
						<form:errors path="status" cssClass="portlet-msg-error"></form:errors>	
					</div>
					<div>
						<label><spring:message code="label.location_of_agent_configuration_repository" />:</label>
						<div class="amFormMultipleInputs">
							<input name="locationOfAgentConfigurationRepository" type="radio" value="local" id="locationOfAgentConfigurationRepositoryLocal" 
								<c:choose>
									<c:when test="${webAgent.locationOfAgentConfigurationRepository eq 'local'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="locationOfAgentConfigurationRepositoryLocal"><spring:message code="label.local" /></label>
							<br>
							<input name="locationOfAgentConfigurationRepository" type="radio" value="centralized" id="locationOfAgentConfigurationRepositoryCentralized" 
								<c:choose>
									<c:when test="${webAgent.locationOfAgentConfigurationRepository eq 'centralized'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="locationOfAgentConfigurationRepositoryCentralized"><spring:message code="label.centralized" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.location_of_agent_configuration_repository" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.agent_configuration_change_notification" />:</label>
						<input name="agentConfigurationChangeNotification" value="true" type="checkbox" id="agentConfigurationChangeNotification"
								<c:choose>
									<c:when test="${webAgent.agentConfigurationChangeNotification eq 'true'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
						<label class="amInputLabel" for="agentConfigurationChangeNotification"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.agent_configuration_change_notification" />
						</div>	
					</div>
					<div>
						<label><spring:message code="label.enable_notifications" />:</label>
						<input name="enableNotifications" value="true" type="checkbox" id="enableNotifications"
							<c:choose>
								<c:when test="${webAgent.enableNotifications eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> > 
						<label class="amInputLabel" for="enableNotifications"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.enable_notifications" />
						</div>
					</div>
					<div>
						<label for="agentNotificationURL"><spring:message code="label.agent_notification_url" />:</label>
						<input name="agentNotificationURL" value="${webAgent.agentNotificationURL}" type="text" id="agentNotificationURL">
						<div class="amNote">
							<spring:message code="note.agent_notification_url" />
						</div>
					</div>
					<div>
						<label for="agentDeploymentURIPrefix"><spring:message code="label.agent_deployment_uri_prefix" />:</label>
						<input name="agentDeploymentURIPrefix" value="${webAgent.agentDeploymentURIPrefix}" type="text" id="agentDeploymentURIPrefix">
						<div class="amNote">
							<spring:message code="note.agent_deployment_uri_prefix" />
						</div>
					</div>
					<div>
						<label for="configurationReloadInterval"><spring:message code="label.configuration_reload_interval" />:</label>
						<input name="configurationReloadInterval" value="${webAgent.configurationReloadInterval}" type="text" id="configurationReloadInterval">
						<div class="amNote">
							<spring:message code="note.configuration_reload_interval" />
						</div>
						<form:errors path="configurationReloadInterval" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="configurationCleanupInterval"><spring:message code="label.configuration_cleanup_interval" />:</label>
						<input name="configurationCleanupInterval" value="${webAgent.configurationCleanupInterval}" type="text" id="configurationCleanupInterval">
						<div class="amNote">
							<spring:message code="note.configuration_cleanup_interval" />
						</div>
						<form:errors path="configurationCleanupInterval" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.agent_root_url_for_cdsso" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="agentRootURLforCDSSODeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="arufc" items="${webAgent.agentRootURLforCDSSO}">
									<input type="hidden" name="agentRootURLforCDSSO" value="${arufc}" />
								</c:forEach>	
								<select name="agentRootURLforCDSSODeleteValues" multiple="multiple" id="agentRootURLforCDSSODeleteValues">
									<c:forEach var="arufcv" items="${webAgent.agentRootURLforCDSSO}">
										<option value="${arufcv}">${arufcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteAgentRootURLforCDSSO" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="agentRootURLforCDSSOAddValue"><spring:message code="label.new_value" /></label>
								<input name="agentRootURLforCDSSOAddValue" type="text" id="agentRootURLforCDSSOAddValue">
								<input type="submit" name="addAgentRootURLforCDSSO" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.the_agent_root_url_for_cdsso" />
						</div>
						<form:errors path="agentRootURLforCDSSO" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.general" /></h3>
					<div>
						<label><spring:message code="label.sso_only_mode" />:</label>
						<input name="ssoOnlyMode" value="true" type="checkbox" id="ssoOnlyMode" 
							<c:choose>
								<c:when test="${webAgent.ssoOnlyMode eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  >
						<label class="amInputLabel" for="ssoOnlyMode"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.sso_only_mode" />
						</div>
					</div>
					<div>
						<label for="resourcesAccessDeniedURL"><spring:message code="label.resources_access_denied_url" />:</label>
						<input name="resourcesAccessDeniedURL" value="${webAgent.resourcesAccessDeniedURL}" type="text" id="resourcesAccessDeniedURL">
						<div class="amNote">
							<spring:message code="note.resources_access_denied_url" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.agent_debug_level" />:</label>
						<div class="amFormMultipleInputs">
							<input name="agentDebugLevel" value="All" type="radio" id="agentDebugLevelAll" 
								<c:choose>
									<c:when test="${webAgent.agentDebugLevel eq 'All'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="agentDebugLevelAll"><spring:message code="label.agent_debug_level.all" /></label>
							<br>
							<input name="agentDebugLevel" value="Error" type="radio" id="agentDebugLevelError" 
								<c:choose>
									<c:when test="${webAgent.agentDebugLevel eq 'Error'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevelError"><spring:message code="label.agent_debug_level.error" /></label>
							<br>
							<input name="agentDebugLevel" value="Info" type="radio" id="agentDebugLevelInfo" 
								<c:choose>
									<c:when test="${webAgent.agentDebugLevel eq 'Info'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevelInfo"><spring:message code="label.agent_debug_level.info" /></label>
							<br>
							<input name="agentDebugLevel" value="Message" type="radio" id="agentDebugLevelMessage" 
								<c:choose>
									<c:when test="${webAgent.agentDebugLevel eq 'Message'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevelMessage"><spring:message code="label.agent_debug_level.message" /></label>
							<br>
							<input name="agentDebugLevel" value="Warning" type="radio" id="agentDebugLevelWarning" 
								<c:choose>
									<c:when test="${webAgent.agentDebugLevel eq 'Warning'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevelWarning"><spring:message code="label.agent_debug_level.warning" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.agent_debug_level" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.agent_debug_file_rotation" />:</label>
						<input name="agentDebugFileRotation" value="true" type="checkbox" id="agentDebugFileRotation" 
							<c:choose>
								<c:when test="${webAgent.agentDebugFileRotation eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  >
						<label class="amInputLabel" for="agentDebugFileRotation"><spring:message code="label.enabled" /></label>	
						<div class="amNote">
							<spring:message code="note.agent_debug_file_rotation" />
						</div>
					</div>
					<div>
						<label for="agentDebugFileSize"><spring:message code="label.agent_debug_file_size" />:</label>
						<input name="agentDebugFileSize" value="${webAgent.agentDebugFileSize}" type="text" id="agentDebugFileSize">
						<div class="amNote">
							<spring:message code="note.agent_debug_file_size" />
						</div>
						<form:errors path="agentDebugFileSize" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.audit" /></h3>
					<div>
						<label><spring:message code="label.audit_access_types" />:</label>
						<div class="amFormMultipleInputs">
							<input name="auditAccessTypes" value="LOG_ALLOW" type="radio" id="auditAccessTypeslogAllow" 
								<c:choose>
									<c:when test="${webAgent.auditAccessTypes eq 'LOG_ALLOW'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="auditAccessTypeslogAllow"><spring:message code="label.log_allow" /></label>
							<br>
							<input name="auditAccessTypes" value="LOG_BOTH" type="radio" id="auditAccessTypesLogBoth" 
								<c:choose>
									<c:when test="${webAgent.auditAccessTypes eq 'LOG_BOTH'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="auditAccessTypesLogBoth"><spring:message code="label.log_both" /></label>
							<br>
							<input name="auditAccessTypes" value="LOG_DENY" type="radio" id="auditAccessTypesLogDeny" 
								<c:choose>
									<c:when test="${webAgent.auditAccessTypes eq 'LOG_DENY'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditAccessTypesLogDeny"><spring:message code="label.log_deny" /></label>
							<br>
							<input name="auditAccessTypes" value="LOG_NONE" type="radio" id="auditAccessTypesLogName" 
								<c:choose>
									<c:when test="${webAgent.auditAccessTypes eq 'LOG_NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="auditAccessTypesLogName"><spring:message code="label.log_alone" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.audit_access_types" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.audit_log_location" />:</label>
						<div class="amFormMultipleInputs">
							<input name="auditLogLocation" value="ALL" type="radio" id="auditLogLocationAll" 
								<c:choose>
									<c:when test="${webAgent.auditLogLocation eq 'ALL'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="auditLogLocationAll"><spring:message code="label.audit_log_location.all" /></label>
							<br>
							<input name="auditLogLocation" value="LOCAL" type="radio" id="auditLogLocationLocal" 
								<c:choose>
									<c:when test="${webAgent.auditLogLocation eq 'LOCAL'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditLogLocationLocal"><spring:message code="label.audit_log_location.local" /></label>
							<br>
							<input name="auditLogLocation" value="REMOTE" type="radio" id="auditLogLocationRemote" 
								<c:choose>
									<c:when test="${webAgent.auditLogLocation eq 'REMOTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>  >
							<label class="amInputLabel" for="auditLogLocationRemote"><spring:message code="label.audit_log_location.remote" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.audit_log_location" />
						</div>
					</div>
					<div>
						<label for="remoteLogFilename"><spring:message code="label.remote_log_filename" />:</label>
						<input name="remoteLogFilename" value="${webAgent.remoteLogFilename}" type="text" id="remoteLogFilename">
						<div class="amNote">
							<spring:message code="note.remote_log_filename" />
						</div>
					</div>
					<div>
						<label for="remoteAuditLogInterval"><spring:message code="label.remote_audit_log_interval" />:</label>
						<input name="remoteAuditLogInterval" value="${webAgent.remoteAuditLogInterval}" type="text" id="remoteAuditLogInterval">
						<div class="amNote">
							<spring:message code="note.remote_audit_log_interval" />
						</div>
						<form:errors path="remoteAuditLogInterval" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.rotate_local_audit_log" />:</label>
						<input name="rotateLocalAuditLog" value="true" type="checkbox" id="rotateLocalAuditLog" 
							<c:choose>
								<c:when test="${webAgent.rotateLocalAuditLog eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  >
						<label class="amInputLabel" for="rotateLocalAuditLog"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.rotate_local_audit_log" />
						</div>	
					</div>
					<div>
						<label for="localAuditLogRotationSize"><spring:message code="label.local_audit_log_rotation_size" />:</label>
						<input name="localAuditLogRotationSize" value="${webAgent.localAuditLogRotationSize}" type="text" id="localAuditLogRotationSize">
						<div class="amNote">
							<spring:message code="note.local_audit_log_rotation_size" />
						</div>
						<form:errors path="localAuditLogRotationSize" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.fully_qualified_domain_name_checking" /></h3>
					<div>
						<label><spring:message code="label.fqdn_check" />:</label>
						<input name="fqdnCheck" value="true" type="checkbox" id="fqdnCheck" 
							<c:choose>
								<c:when test="${webAgent.fqdnCheck eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="fqdnCheck"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.fqdn_check" />
						</div>
					</div>
					<div>
						<label for="fqdnDefault"><spring:message code="label.fqdn_default" />:</label>
						<input name="fqdnDefault" value="${webAgent.fqdnDefault}" type="text" id="fqdnDefault">
						<div class="amNote">
							<spring:message code="note.fqdn_default" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.fqdn_virtual_host_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="fqdnVirtualHostMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="fvhm" items="${webAgent.fqdnVirtualHostMap}">
									<input type="hidden" name="fqdnVirtualHostMap" value="${fvhm}" />
								</c:forEach>	
								<select name="fqdnVirtualHostMapDeleteValues" multiple="multiple" id="fqdnVirtualHostMapDeleteValues">
									<c:forEach var="fvhmv" items="${webAgent.fqdnVirtualHostMap}">
										<option value="${fvhmv}">${fvhmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteFqdnVirtualHostMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="fqdnVirtualHostMapKey"><spring:message code="label.map_key" />:</label>
									<input name="fqdnVirtualHostMapKey" type="text" id="fqdnVirtualHostMapKey">
								</div>
								<div class="amFormCell">
									<label for="fqdnVirtualHostMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="fqdnVirtualHostMapValue" type="text" id="fqdnVirtualHostMapValue">
								</div>
								<input type="submit" name="addFqdnVirtualHostMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.fqdn_virtual_host_map" />
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
