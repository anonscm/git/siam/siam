<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_miscellaneous" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
					<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editWebAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editWebAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><b><spring:message code="label.menu.miscellaneous" /></b></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="webAgent">
					<input type="hidden" name="ctx" value="WebAgentMiscellaneousController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.locale" /></h3>
					<div>
						<label for="agentLocale"><spring:message code="label.agent_locale" />:</label>
						<input name="agentLocale" value="${webAgent.agentLocale}" type="text" id="agentLocale">
						<div class="amNote">
							<spring:message code="note.agent_locale" />
						</div>	
					</div>
					<hr>
					
					<h3><spring:message code="title.anonymous_user" /></h3>
					<div>
						<label><spring:message code="label.anonymous_user" />:</label>
						<input name="anonymousUser" value="true" type="checkbox" id="anonymousUser"
							<c:choose>
								<c:when test="${webAgent.anonymousUser eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="anonymousUser"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.anonymous_user" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.cookie_processing" /></h3>
					<div>
						<label for="profileAttributesCookiePrefix"><spring:message code="label.profile_attributes_cookie_prefix" />:</label>
						<input name="profileAttributesCookiePrefix" value="${webAgent.profileAttributesCookiePrefix}" type="text" id="profileAttributesCookiePrefix">
						<div class="amNote">
							<spring:message code="note.profile_attributes_cookie_prefix" />
						</div>
					</div>
					<div>
						<label for="profileAttributesCookieMaxage"><spring:message code="label.profile_attributes_cookie_maxage" />:</label>
						<input name="profileAttributesCookieMaxage" value="${webAgent.profileAttributesCookieMaxage}" type="text" id="profileAttributesCookieMaxage">
						<div class="amNote">
							<spring:message code="note.profile_attributes_cookie_maxage" />
						</div>
						<form:errors path="profileAttributesCookieMaxage" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.url_handling" /></h3>
					<div>
						<label><spring:message code="label.url_comparison_case_sensitivity_check" />:</label>
						<input name="urlComparisonCaseSensitivityCheck" value="true" type="checkbox" id="urlComparisonCaseSensitivityCheck" 
							<c:choose>
								<c:when test="${webAgent.urlComparisonCaseSensitivityCheck eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="urlComparisonCaseSensitivityCheck"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.url_comparison_case_sensitivity_check" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.encode_url_s_special_characters" />:</label>
						<input name="encodeURLSpecialCharacters" value="true" type="checkbox" id="encodeURLSpecialCharacters" 
							<c:choose>
								<c:when test="${webAgent.encodeURLSpecialCharacters eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="encodeURLSpecialCharacters"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.encode_url_s_special_characters" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.ignore_naming_url" /> </h3>
					<div>
						<label><spring:message code="label.ignore_preferred_naming_url_in_naming_request" />:</label>
						<input name="ignorePreferredNamingURLInNamingRequest" value="true" type="checkbox" id="ignorePreferredNamingURLInNamingRequest" 
							<c:choose>
								<c:when test="${webAgent.ignorePreferredNamingURLInNamingRequest eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="ignorePreferredNamingURLInNamingRequest"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.ignore_preferred_naming_url_in_naming_request" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.ignore_server_check" /></h3>
					<div>
						<label><spring:message code="label.ignore_server_check" />:</label>
						<input name="ignoreServerCheck" value="true" type="checkbox" id="ignoreServerCheck" 
							<c:choose>
								<c:when test="${webAgent.ignoreServerCheck eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="ignoreServerCheck"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.ignore_server_check" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.ignore_path_info" /></h3>
					<div>
						<label><spring:message code="label.ignore_path_info_in_request_url" />:</label>
						<input name="ignorePathInfoInRequestURL" value="true" type="checkbox" id="ignorePathInfoInRequestURL" 
							<c:choose>
								<c:when test="${webAgent.ignorePathInfoInRequestURL eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  >
						<label class="amInputLabel" for="ignorePathInfoInRequestURL"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.ignore_path_info_in_request_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.multi_byte_enable" /></h3>
					<div>
						<label><spring:message code="label.native_encoding_of_profile_attributes" />:</label>
						<input name="nativeEncodingOfProfileAttributes" value="true" type="checkbox" id="nativeEncodingOfProfileAttributes"
							<c:choose>
								<c:when test="${webAgent.nativeEncodingOfProfileAttributes eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>  >
						<label class="amInputLabel" for="nativeEncodingOfProfileAttributes"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.native_encoding_of_profile_attributes" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.goto_parameter_name" /></h3>
					<div>
						<label for="gotoParameterName"><spring:message code="label.goto_parameter_name" />:</label>
						<input name="gotoParameterName" value="${webAgent.gotoParameterName}" type="text" id="gotoParameterName">
						<div class="amNote">
							<spring:message code="note.goto_parameter_name" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.deprecated_agent_properties" /></h3>
					<div>
						<label for="anonymousUserDefaultValue"><spring:message code="label.anonymous_user_default_value" />:</label>
						<input name="anonymousUserDefaultValue" value="${webAgent.anonymousUserDefaultValue}" type="text" id="anonymousUserDefaultValue">
						<div class="amNote">
							<spring:message code="note.anonymous_user_default_value" />
						</div>
					</div>
				</form:form>	
			
			</div>
		</div>
	</div>

</div>