<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.web_agent_application" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_WEB %>
				</h2>
			</div>
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editWebAgentGlobalURL">
						<portlet:param name="ctx" value="WebAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editWebAgentApplicationURL">
					<portlet:param name="ctx" value="WebAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentSSOURL">
					<portlet:param name="ctx" value="WebAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editWebAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="WebAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentAdvancedURL">
					<portlet:param name="ctx" value="WebAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editWebAgentMiscellaneousURL">
					<portlet:param name="ctx" value="WebAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editWebAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editWebAgentApplicationURL}"><b><spring:message code="label.menu.application" /></b></a></li>
					<li><a href="${editWebAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editWebAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editWebAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editWebAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post">
					<input type="hidden" name=ctx value="WebAgentApplicationController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.not_enforced_url_processing" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.not_enforced_urls" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="notEnforcedURLsDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="neu" items="${webAgent.notEnforcedURLs}">
									<input type="hidden" name="notEnforcedURLs" value="${neu}" />
								</c:forEach>	
								<select name="notEnforcedURLsDeleteValues" multiple="multiple" id="notEnforcedURLsDeleteValues">
									<c:forEach var="neuv" items="${webAgent.notEnforcedURLs}">
										<option value="${neuv}">${neuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNotEnforcedURLs" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="notEnforcedURLsAddValue"><spring:message code="label.new_value" /></label>
								<input name="notEnforcedURLsAddValue" type="text" id="notEnforcedURLsAddValue">
								<input type="submit" name="addNotEnforcedURLs" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.not_enforced_urls" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.invert_not_enforced_urls" />:</label>
						<input name="invertNotEnforcedURLs" value="true" type="checkbox" id="invertNotEnforcedURLs" 
							<c:choose>
								<c:when test="${webAgent.invertNotEnforcedURLs eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="invertNotEnforcedURLs"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.invert_not_enforced_urls" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.fetch_attributes_for_not_enforced_urls" />:</label>
						<input name="fetchAttributesForNotEnforcedURLs" value="true" type="checkbox" id="fetchAttributesForNotEnforcedURLs" 
							<c:choose>
								<c:when test="${webAgent.fetchAttributesForNotEnforcedURLs eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="fetchAttributesForNotEnforcedURLs"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.fetch_attributes_for_not_enforced_urls" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.not_enforced_ip_processing" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.not_enforced_client_ip_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="notEnforcedClientIPListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="necipl" items="${webAgent.notEnforcedClientIPList}">
									<input type="hidden" name="notEnforcedClientIPList" value="${necipl}" />
								</c:forEach>	
								<select name="notEnforcedClientIPListDeleteValues" multiple="multiple" id="notEnforcedClientIPListDeleteValues">
									<c:forEach var="neciplv" items="${webAgent.notEnforcedClientIPList}">
										<option value="${neciplv}">${neciplv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNotEnforcedClientIPList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="notEnforcedClientIPListAddValue"><spring:message code="label.new_value" /></label>
								<input name="notEnforcedClientIPListAddValue" type="text" id="notEnforcedClientIPListAddValue">
								<input type="submit" name="addNotEnforcedClientIPList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.not_enforced_client_ip_list" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.client_ip_validation" />:</label>
						<input name="clientIPValidation" value="true" type="checkbox" id="clientIPValidation" 
							<c:choose>
								<c:when test="${webAgent.clientIPValidation eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="clientIPValidation"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.client_ip_validation" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.profile_attributes_processing" /></h3>
					<div>
						<label><spring:message code="label.profile_attribute_fetch_mode" />:</label>
						<div class="amFormMultipleInputs">
							<input name="profileAttributeFetchMode" value="HTTP_COOKIE" type="radio" id="profileAttributeFetchModeHttpCookie" 
								<c:choose>
									<c:when test="${webAgent.profileAttributeFetchMode eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeHttpCookie"><spring:message code="label.http_cookie" /></label>
							<br>
							<input name="profileAttributeFetchMode" value="HTTP_HEADER" type="radio" id="profileAttributeFetchModeHttpHeader" 
								<c:choose>
									<c:when test="${webAgent.profileAttributeFetchMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeHttpHeader"><spring:message code="label.http_header" /></label>
							<br>
							<input name="profileAttributeFetchMode" value="NONE" type="radio" id="profileAttributeFetchModeNone" 
								<c:choose>
									<c:when test="${webAgent.profileAttributeFetchMode eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeNone"><spring:message code="label.none" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_fetch_mode" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="lable.profile_attribute_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="profileAttributeMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="pam" items="${webAgent.profileAttributeMap}">
									<input type="hidden" name="profileAttributeMap" value="${pam}" />
								</c:forEach>	
								<select name="profileAttributeMapDeleteValues" multiple="multiple" id="profileAttributeMapDeleteValues">
									<c:forEach var="pamv" items="${webAgent.profileAttributeMap}">
										<option value="${pamv}">${pamv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteProfileAttributeMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="profileAttributeMapKey"><spring:message code="label.map_key" />:</label>
									<input name="profileAttributeMapKey" type="text" id="profileAttributeMapKey">
								</div>
								<div class="amFormCell">
									<label for="profileAttributeMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="profileAttributeMapValue" type="text" id="profileAttributeMapValue">
								</div>
								<input type="submit" name="addProfileAttributeMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_map" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.respunse_attributes_processing" /></h3>
					<div>
						<label><spring:message code="label.response_attribute_fetch_mode" />:</label>
						<div class="amFormMultipleInputs">
							<input name="responseAttributeFetchMode" value="HTTP_COOKIE" type="radio" id="responseAttributeFetchModehttpCookie" 
								<c:choose>
									<c:when test="${webAgent.responseAttributeFetchMode eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModehttpCookie"><spring:message code="label.http_cookie" /></label>
							<br>
							<input name="responseAttributeFetchMode" value="HTTP_HEADER" type="radio" id="responseAttributeFetchModeHttpHeader" 
								<c:choose>
									<c:when test="${webAgent.responseAttributeFetchMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModeHttpHeader"><spring:message code="label.http_header" /></label>
							<br>
							<input name="responseAttributeFetchMode" value="NONE" type="radio" id="responseAttributeFetchModeNone" 
								<c:choose>
									<c:when test="${webAgent.responseAttributeFetchMode eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModeNone"><spring:message code="label.none" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_fetch_mode" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.response_attribute_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="responseAttributeMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ram" items="${webAgent.responseAttributeMap}">
									<input type="hidden" name="responseAttributeMap" value="${ram}" />
								</c:forEach>	
								<select name="responseAttributeMapDeleteValues" multiple="multiple" id="responseAttributeMapDeleteValues">
									<c:forEach var="ramv" items="${webAgent.responseAttributeMap}">
										<option value="${ramv}">${ramv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteResponseAttributeMap" value="<spring:message code="action.delete" />">
							</div>				
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="responseAttributeMapKey"><spring:message code="label.map_key" />:</label>
									<input name="responseAttributeMapKey" type="text" id="responseAttributeMapKey">
								</div>
								<div class="amFormCell">
									<label for="responseAttributeMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="responseAttributeMapValue" type="text" id="responseAttributeMapValue">
								</div>
								<input type="submit" name="addResponseAttributeMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.response_attribute_map" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.session_attributes_processing" /></h3>
					<div>
						<label><spring:message code="label.session_attribute_fetch_mode" />:</label>
						<div class="amFormMultipleInputs">
							<input name="sessionAttributeFetchMode" value="HTTP_COOKIE" type="radio" id="sessionAttributeFetchModeHttpCookie" 
								<c:choose>
									<c:when test="${webAgent.sessionAttributeFetchMode eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeHttpCookie"><spring:message code="label.http_cookie" /></label>
							<br>
							<input name="sessionAttributeFetchMode" value="HTTP_HEADER" type="radio" id="sessionAttributeFetchModehttpHeader" 
								<c:choose>
									<c:when test="${webAgent.sessionAttributeFetchMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModehttpHeader"><spring:message code="label.http_header" /></label>
							<br>
							<input name="sessionAttributeFetchMode" value="NONE" type="radio" id="sessionAttributeFetchModeNone" 
								<c:choose>
									<c:when test="${webAgent.sessionAttributeFetchMode eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeNone"><spring:message code="label.none" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_fetch_mode" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.session_attribute_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="sessionAttributeMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="fvhm" items="${webAgent.sessionAttributeMap}">
									<input type="hidden" name="sessionAttributeMap" value="${fvhm}" />
								</c:forEach>	
								<select name="sessionAttributeMapDeleteValues" multiple="multiple" id="sessionAttributeMapDeleteValues">
									<c:forEach var="fvhmv" items="${webAgent.sessionAttributeMap}">
										<option value="${fvhmv}">${fvhmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteSessionAttributeMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="sessionAttributeMapKey"><spring:message code="label.map_key" />:</label>
									<input name="sessionAttributeMapKey" type="text" id="sessionAttributeMapKey">
								</div>
								<div class="amFormCell">
									<label for="sessionAttributeMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="sessionAttributeMapValue" type="text" id="sessionAttributeMapValue">
								</div>
								<input type="submit" name="addSessionAttributeMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.session_attribute_map" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.common_attributes_fetching_processing" /></h3>
					<div>
						<label for="attributeMultiValueSeparator"><spring:message code="label.attribute_multi_value_separator" /> :</label>
						<input name="attributeMultiValueSeparator" value="${webAgent.attributeMultiValueSeparator}" type="text" id="attributeMultiValueSeparator">
						<div class="amNote">
							<spring:message code="note.attribute_multi_value_separator" />
						</div>		
					</div>
				</form:form>
			</div>
		</div>	
	</div>

</div>









