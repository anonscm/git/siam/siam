<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<spring:message code="breadcrumb.agent" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="entity">
		<input type="hidden" name="action" value="doAddAgent" />
		
		<h3><spring:message code="title.new_agent" /></h3>
		<div class="amFormCell">
			<label for="name"><spring:message code="label.agent_name" />:</label>
			<input type="text" name="name" id="name">
		</div>
		<div class="amFormCell">
			<label for="type"><spring:message code="label.agent_type" />:</label>
			<select name="type" id="type">
				<c:forEach var="agentType" items="${agentTypeList}">
					<option value="${agentType}">${agentType}</option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" value="<spring:message code="action.create" />">
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>	
		<form:errors path="type" cssClass="portlet-msg-error"></form:errors>	
	</form:form>

	<h3><spring:message code="title.configured_agents" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_delete" method="post">
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllAgents eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllAgents" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllAgents" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th><spring:message code="label.name" /></th>
				<th><spring:message code="label.repository_location" /></th>
				<th>
					<div class="amDivButtons">
						<input type="submit" name="deleteAgent" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="agent" items="${agentList}">
				<tr>
					<td class="amInputCell">
						<input name="deleteAgentList" value="${agent.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllAgents eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td>${agent.type}</td>
					<td>
						<c:set var="isType" value="false"/>
						<c:forEach var="agentType" items="${agentTypeList}">
							<c:if test="${agent.type eq agentType}">
								<c:set var="isType" value="true"/>
							</c:if>
						</c:forEach>
						<c:choose>
							<c:when test="${isType eq true}">
								<c:choose>
									<c:when test="${'centralized' eq agent.configuration}">
										<portlet:renderURL var="editAgentURL">
											<portlet:param name="ctx" value="${agent.type}GlobalController"/>
											<portlet:param name="name" value="${agent.name}"/>
											<portlet:param name="type" value="${agent.type}"/>
										</portlet:renderURL>
										<a href="${editAgentURL}">${agent.name}</a>
									</c:when>
									<c:when test="${'local' eq agent.configuration}">
										<portlet:renderURL var="editLocalAgentURL">
											<portlet:param name="ctx" value="EditLocalController"/>
											<portlet:param name="name" value="${agent.name}"/>
										</portlet:renderURL>
										<a href="${editLocalAgentURL}">${agent.name}</a>
									</c:when>
									<c:otherwise>
										${agent.name}
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								${agent.name}
							</c:otherwise>
						</c:choose>
					</td>
					<td>${agent.configuration}</td>
					<td>
						<c:choose>
							<c:when test="${isType eq true and agent.export eq true }">
								<portlet:renderURL var="exportConfigurationURL">
									<portlet:param name="ctx" value="ExportAgentConfigurationController"/>
									<portlet:param name="name" value="${agent.name}"/>
									<portlet:param name="type" value="${agent.type}"/>
									<portlet:param name="config" value="${agent.configuration}"/>
								</portlet:renderURL>
								<a href="${exportConfigurationURL}"><spring:message code="label.export_configuration" /></a>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>	

</div>
