<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_sso" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><b><spring:message code="label.menu.sso" /></b></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentSSOController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.cookie_name" /></h3>
					<div>
						<label for="cookieName"><spring:message code="label.cookie_name" />:</label>
						<input name="cookieName" value="${j2eeAgent.cookieName}" type="text" id="cookieName">
						<div class="amNote">
							<spring:message code="note.cookie_name" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.caching" /></h3>
					<div>
						<label><spring:message code="label.sso_cache_enable" />:</label>
						<input name="ssoCacheEnable" value="true" type="checkbox" id="ssoCacheEnable" 
							<c:choose>
								<c:when test="${j2eeAgent.ssoCacheEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="ssoCacheEnable"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.sso_cache_enable" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.cross_domain_sso" /></h3>
					<div>
						<label><spring:message code="label.cross_domain_sso" />:</label>
						<input name="crossDomainSSO" value="true" type="checkbox" id="crossDomainSSO"
							<c:choose>
								<c:when test="${j2eeAgent.crossDomainSSO eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="crossDomainSSO"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cross_domain_sso" />
						</div>
					</div>
					<div>
						<label for="cdssoRedirectURI"><spring:message code="label.cdsso_redirect_uri" />:</label>
						<input name="cdssoRedirectURI" value="${j2eeAgent.cdssoRedirectURI}" type="text" id="cdssoRedirectURI">
						<div class="amNote">
							<spring:message code="note.label.cdsso_redirect_uri" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cdsso_servlet_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cdssoServletURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="csu" items="${j2eeAgent.cdssoServletURL}">
									<input type="hidden" name="cdssoServletURL" value="${csu}" />
								</c:forEach>	
								<select name="cdssoServletURLSelectValues" multiple="multiple" id="cdssoServletURLSelectValues">
									<c:forEach var="csuv" items="${j2eeAgent.cdssoServletURL}">
										<option value="${csuv}">${csuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpCdssoServletURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownCdssoServletURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopCdssoServletURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomCdssoServletURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deletedCdssoServletURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="cdssoServletURLAddValue"><spring:message code="label.new_value" /></label>
								<input name="cdssoServletURLAddValue" type="text" id="cdssoServletURLAddValue">
								<input type="submit" name="addCdssoServletURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cdsso_servlet_url" />
						</div>
					</div>
					<div>
						<label for="cdssoClockSkew"><spring:message code="label.cdsso_clock_skew" />:</label>
						<input name="cdssoClockSkew" value="${j2eeAgent.cdssoClockSkew}" type="text" id="cdssoClockSkew">
						<div class="amNote">
							<spring:message code="note.cdsso_clock_skew" />
						</div>
						<form:errors path="cdssoClockSkew" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cdsso_trusted_id_provider" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cdssoTrustedIDProviderSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ctip" items="${j2eeAgent.cdssoTrustedIDProvider}">
									<input type="hidden" name="cdssoTrustedIDProvider" value="${ctip}" />
								</c:forEach>	
								<select name="cdssoTrustedIDProviderSelectValues" multiple="multiple" id="cdssoTrustedIDProviderSelectValues">
									<c:forEach var="ctipv" items="${j2eeAgent.cdssoTrustedIDProvider}">
										<option value="${ctipv}">${ctipv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpCdssoTrustedIDProvider" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownCdssoTrustedIDProvider" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopCdssoTrustedIDProvider" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomCdssoTrustedIDProvider" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deleteCdssoTrustedIDProvider" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="cdssoTrustedIDProviderAddValue"><spring:message code="label.new_value" /></label>
								<input name="cdssoTrustedIDProviderAddValue" type="text" id="cdssoTrustedIDProviderAddValue">
								<input type="submit" name="addCdssoTrustedIDProvider" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cdsso_trusted_id_provider" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.cdsso_secure_enable" />:</label>
						<input name="cdssoSecureEnable" value="true" type="checkbox" id="cdssoSecureEnable" 
							<c:choose>
								<c:when test="${j2eeAgent.cdssoSecureEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="cdssoSecureEnable"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cdsso_secure_enable" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cdsso_domain_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cdssoDomainListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="cdl" items="${j2eeAgent.cdssoDomainList}">
									<input type="hidden" name="cdssoDomainList" value="${cdl}" />
								</c:forEach>	
								<select name="cdssoDomainListDeleteValues" multiple="multiple" id="cdssoDomainListDeleteValues">
									<c:forEach var="cdlv" items="${j2eeAgent.cdssoDomainList}">
										<option value="${cdlv}">${cdlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCdssoDomainList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="cdssoDomainListAddValue"><spring:message code="label.new_value" /></label>
								<input name="cdssoDomainListAddValue" type="text" id="cdssoDomainListAddValue">
								<input type="submit" name="addCdssoDomainList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cdsso_domain_list" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.cookie_reset" /></h3>
					<div>
						<label><spring:message code="label.cookie_reset" />:</label>
						<input name="cookieReset" value="true" type="checkbox" id="cookieReset"
							<c:choose>
								<c:when test="${j2eeAgent.cookieReset eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="cookieReset"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.cookie_reset" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cookies_reset_name_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cookiesResetNameListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="crnl" items="${j2eeAgent.cookiesResetNameList}">
									<input type="hidden" name="cookiesResetNameList" value="${crnl}" />
								</c:forEach>	
								<select name="cookiesResetNameListDeleteValues" multiple="multiple" id="cookiesResetNameListDeleteValues">
									<c:forEach var="crnlv" items="${j2eeAgent.cookiesResetNameList}">
										<option value="${crnlv}">${crnlv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCookiesResetNameList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="cookiesResetNameListAddValue"><spring:message code="label.new_value" /></label>
								<input name="cookiesResetNameListAddValue" type="text" id="cookiesResetNameListAddValue">
								<input type="submit" name="addCookiesResetNameList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cookies_reset_name_list_j2ee" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cookies_reset_domain_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cookiesResetDomainMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="afm" items="${j2eeAgent.cookiesResetDomainMap}">
									<input type="hidden" name="cookiesResetDomainMap" value="${afm}" />
								</c:forEach>	
								<select name="cookiesResetDomainMapDeleteValues" multiple="multiple" id="cookiesResetDomainMapDeleteValues">
									<c:forEach var="afmv" items="${j2eeAgent.cookiesResetDomainMap}">
										<option value="${afmv}">${afmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCookiesResetDomainMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="cookiesResetDomainMapKey"><spring:message code="label.map_key" />:</label>
									<input name="cookiesResetDomainMapKey" type="text" id="cookiesResetDomainMapKey">
								</div>
								<div class="amFormCell">
									<label for="cookiesResetDomainMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="cookiesResetDomainMapValue" type="text" id="cookiesResetDomainMapValue">
								</div>
								<input type="submit" name="addCookiesResetDomainMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.label.cookies_reset_domain_map" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.cookies_reset_path_map" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="cookiesResetPathMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="afm" items="${j2eeAgent.cookiesResetPathMap}">
									<input type="hidden" name="cookiesResetPathMap" value="${afm}" />
								</c:forEach>	
								<select name="cookiesResetPathMapDeleteValues" multiple="multiple" id="cookiesResetPathMapDeleteValues">
									<c:forEach var="afmv" items="${j2eeAgent.cookiesResetPathMap}">
										<option value="${afmv}">${afmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCookiesResetPathMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="cookiesResetPathMapKey"><spring:message code="label.map_key" />:</label>
									<input name="cookiesResetPathMapKey" type="text" id="cookiesResetPathMapKey">
								</div>
								<div class="amFormCell">
									<label for="cookiesResetPathMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="cookiesResetPathMapValue" type="text" id="cookiesResetPathMapValue">
								</div>
								<input type="submit" name="addCookiesResetPathMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.cookies_reset_path_map" />
						</div>
					</div>
	
				</form:form>	
			</div>
		</div>	
	</div>
</div>