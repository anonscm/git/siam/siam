<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_miscellaneous" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><b><spring:message code="label.menu.miscellaneous" /></b></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>	
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentMiscellaneousController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.locale" /></h3>
					<div>
						<label for="localeLanguage"><spring:message code="label.locale_language" />:</label>
						<input name="localeLanguage" value="${j2eeAgent.localeLanguage}" type="text" id="localeLanguage">
						<div class="amNote">
							<spring:message code="note.locale_language" />
						</div>
					</div>
					<div>
						<label for="localeCountry"><spring:message code="label.locale_country" />:</label>
						<input name="localeCountry" value="${j2eeAgent.localeCountry}" type="text" id="localeCountry">
						<div class="amNote">
							<spring:message code="note.locale_country" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.port_check_processing" /></h3>
					<div>
						<label><spring:message code="label.port_check_enable" />:</label>
						<input name="portCheckEnable" value="true" type="checkbox" id="portCheckEnable"
							<c:choose>
								<c:when test="${j2eeAgent.portCheckEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="portCheckEnable"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.port_check_enable" />
						</div>
					</div>
					<div>
						<label for="portCheckFile"><spring:message code="label.port_check_file" />:</label>
						<input name="portCheckFile" value="${j2eeAgent.portCheckFile}" type="text" id="portCheckFile">
						<div class="amNote">
							<spring:message code="note.port_check_file" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.port_check_setting" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="portCheckSettingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="pcs" items="${j2eeAgent.portCheckSetting}">
									<input type="hidden" name="portCheckSetting" value="${pcs}" />
								</c:forEach>	
								<select name="portCheckSettingDeleteValues" multiple="multiple" id="portCheckSettingDeleteValues">
									<c:forEach var="pcsv" items="${j2eeAgent.portCheckSetting}">
										<option value="${pcsv}">${pcsv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deletePortCheckSetting" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="portCheckSettingKey"><spring:message code="label.map_key" />:</label>
									<input name="portCheckSettingKey" type="text" id="portCheckSettingKey">
								</div>
								<div class="amFormCell">
									<label for="portCheckSettingValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="portCheckSettingValue" type="text" id="portCheckSettingValue">
								</div>
								<input type="submit" name="addPortCheckSetting" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.port_check_setting" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.bypass_principal_list" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.bypass_principal_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="bypassPrincipalListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="bpl" items="${j2eeAgent.bypassPrincipalList}">
									<input type="hidden" name="bypassPrincipalList" value="${bpl}" />
								</c:forEach>	
								<select name="bypassPrincipalListDeleteValues" multiple="multiple" id="bypassPrincipalListDeleteValues">
									<c:forEach var="bplv" items="${j2eeAgent.bypassPrincipalList}">
										<option value="${bplv}">${bplv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteBypassPrincipalList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="bypassPrincipalListAddValue"><spring:message code="label.new_value" /></label>
								<input name="bypassPrincipalListAddValue" type="text" id="bypassPrincipalListAddValue">
								<input type="submit" name="addBypassPrincipalList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.bypass_principal_list" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.agent_password_encryptor" /></h3>
					<div>
						<label for="encryptionProvider"><spring:message code="label.encryption_provider" />:</label>
						<input name="encryptionProvider" value="${j2eeAgent.encryptionProvider}" type="text" id="encryptionProvider">
						<div class="amNote">
							<spring:message code="note.encryption_provider" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.ignore_path_info" /></h3>
					<div>
						<label><spring:message code="label.ignore_path_info_in_request_url" />:</label>
						<input name="ignorePathInfoinRequestURL" value="true" type="checkbox" id="ignorePathInfoinRequestURL"
							<c:choose>
								<c:when test="${j2eeAgent.ignorePathInfoinRequestURL eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="ignorePathInfoinRequestURL"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.ignore_path_info_in_request_url" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.deprecated_agent_properties" /></h3>
					<div>
						<label for="gotoParameterName"><spring:message code="label.goto_parameter_name" />:</label>
						<input name="gotoParameterName" value="${j2eeAgent.gotoParameterName}" type="text" id="gotoParameterName">
						<div class="amNote">
							<spring:message code="note.goto_parameter_name" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.legacy_user_agent_support_enable" />:</label>
						<input name="legacyUserAgentSupportEnable" value="true" type="checkbox" id="legacyUserAgentSupportEnable"
							<c:choose>
								<c:when test="${j2eeAgent.legacyUserAgentSupportEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="legacyUserAgentSupportEnable"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.legacy_user_agent_support_enable" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.legacy_user_agent_list" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="legacyUserAgentListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="lual" items="${j2eeAgent.legacyUserAgentList}">
									<input type="hidden" name="legacyUserAgentList" value="${lual}" />
								</c:forEach>	
								<select name="legacyUserAgentListDeleteValues" multiple="multiple" id="legacyUserAgentListDeleteValues">
									<c:forEach var="lualv" items="${j2eeAgent.legacyUserAgentList}">
										<option value="${lualv}">${lualv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLegacyUserAgentList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="legacyUserAgentListAddValue"><spring:message code="label.new_value" /></label>
								<input name="legacyUserAgentListAddValue" type="text" id="legacyUserAgentListAddValue">
								<input type="submit" name="addLegacyUserAgentList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.legacy_user_agent_list" />
						</div>
					</div>
					<div>
						<label for="legacyUserAgentRedirectURI"><spring:message code="label.legacy_user_agent_redirect_uri" />:</label>
						<input name="legacyUserAgentRedirectURI" value="${j2eeAgent.legacyUserAgentRedirectURI}" type="text" id="legacyUserAgentRedirectURI">
						<div class="amNote">
							<spring:message code="note.legacy_user_agent_redirect_uri" />
						</div>
					</div>
					
				</form:form>	
			</div>
		</div>	
	</div>	
</div>
