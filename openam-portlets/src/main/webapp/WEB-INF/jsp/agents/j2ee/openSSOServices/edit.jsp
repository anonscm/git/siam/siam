<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_open_sso_services" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><b><spring:message code="label.menu.opensso_services" /></b></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentOpenSSOServicesController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.login_url" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.opensso_login_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="openSSOLoginURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ossolu" items="${j2eeAgent.openSSOLoginURL}">
									<input type="hidden" name="openSSOLoginURL" value="${ossolu}" />
								</c:forEach>	
								<select name="openSSOLoginURLSelectValues" multiple="multiple" id="openSSOLoginURLSelectValues">
									<c:forEach var="ossoluv" items="${j2eeAgent.openSSOLoginURL}">
										<option value="${ossoluv}">${ossoluv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpOpenSSOLoginURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownOpenSSOLoginURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopOpenSSOLoginURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomOpenSSOLoginURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deletedOpenSSOLoginURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="openSSOLoginURLddValue"><spring:message code="label.new_value" /></label>
								<input name="openSSOLoginURLddValue" type="text" id="openSSOLoginURLddValue">
								<input type="submit" name="addOpenSSOLoginURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.opensso_login_url" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.login_url_prioritized" />:</label>
						<input name="loginURLPrioritized" value="true" type="checkbox" id="loginURLPrioritized"
							<c:choose>
								<c:when test="${j2eeAgent.loginURLPrioritized eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="loginURLPrioritized"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.login_url_prioritized" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.login_url_probe" />:</label>
						<input name="loginURLProbe" value="true" type="checkbox" id="loginURLProbe" 
							<c:choose>
								<c:when test="${j2eeAgent.loginURLProbe eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="loginURLProbe"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.login_url_probe" />
						</div>
					</div>
					<div>
						<label for="loginURLProbeTimeout"><spring:message code="label.login_url_probe_timeout" />:</label>
						<input name="loginURLProbeTimeout" value="${j2eeAgent.loginURLProbeTimeout}" type="text" id="loginURLProbeTimeout">
						<div class="amNote">
							<spring:message code="note.login_url_probe_timeout" />
						</div>	
						<form:errors path="loginURLProbeTimeout" cssClass="portlet-msg-error" ></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.logout_url" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.opensso_logout_url" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="openSSOLogoutURLSelectValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="csu" items="${j2eeAgent.openSSOLogoutURL}">
									<input type="hidden" name="openSSOLogoutURL" value="${csu}" />
								</c:forEach>	
								<select name="openSSOLogoutURLSelectValues" multiple="multiple" id="openSSOLogoutURLSelectValues">
									<c:forEach var="csuv" items="${j2eeAgent.openSSOLogoutURL}">
										<option value="${csuv}">${csuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<div class="amFormMultipleInputs">
									<input type="submit" name="moveUpOpenSSOLogoutURL" value="<spring:message code="label.move_up" />">
									<br>
									<input type="submit" name="moveDownOpenSSOLogoutURL" value="<spring:message code="label.move_down" />">
									<br>
									<input type="submit" name="moveTopOpenSSOLogoutURL" value="<spring:message code="label.move_to_top" />">
									<br>
									<input type="submit" name="moveBottomOpenSSOLogoutURL" value="<spring:message code="label.move_to_bottom" />">
									<br>
									<input type="submit" name="deletedOpenSSOLogoutURL" value="<spring:message code="action.delete" />">
								</div>
							</div>
							<div>
								<label for="openSSOLogoutURLddValue"><spring:message code="label.new_value" /></label>
								<input name="openSSOLogoutURLddValue" type="text" id="openSSOLogoutURLddValue">
								<input type="submit" name="addOpenSSOLogoutURL" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.opensso_logout_url" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.logout_url_prioritized" />:</label>
						<input name="logoutURLPrioritized" value="true" type="checkbox" id="logoutURLPrioritized"
							<c:choose>
								<c:when test="${j2eeAgent.logoutURLPrioritized eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="logoutURLPrioritized"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.logout_url_prioritized" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.logout_url_probe" />:</label>
						<input name="logoutURLProbe" value="true" type="checkbox" id="logoutURLProbe"
							<c:choose>
								<c:when test="${j2eeAgent.logoutURLProbe eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="logoutURLProbe"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.logout_url_probe" />
						</div>
					</div>
					<div>
						<label for="logoutURLProbeTimeout"><spring:message code="label.logout_url_probe_timeout" />:</label>
						<input name="logoutURLProbeTimeout" value="${j2eeAgent.logoutURLProbeTimeout}" type="text" id="logoutURLProbeTimeout">
						<div class="amNote">
							<spring:message code="note.logout_url_probe_timeout" />
						</div>
						<form:errors path="logoutURLProbeTimeout" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.authentication_service" /></h3>
					<div>
						<label for="openSSOAuthenticationServiceProtocol"><spring:message code="label.opensso_authentication_service_protocol" />:</label>
						<input name="openSSOAuthenticationServiceProtocol" value="${j2eeAgent.openSSOAuthenticationServiceProtocol}" type="text" id="openSSOAuthenticationServiceProtocol">
						<div class="amNote">
							<spring:message code="note.opensso_authentication_service_protocol" />
						</div>
					</div>
					<div>
						<label for="openSSOAuthenticationServiceHostName"><spring:message code="label.opensso_authentication_service_host_name" />:</label>
						<input name="openSSOAuthenticationServiceHostName" value="${j2eeAgent.openSSOAuthenticationServiceHostName}" type="text" id="openSSOAuthenticationServiceHostName">
						<div class="amNote">
							<spring:message code="note.opensso_authentication_service_host_name" />
						</div>
					</div>
					<div>
						<label for="openSSOAuthenticationServicePort"><spring:message code="label.opensso_authentication_service_port" />:</label>
						<input name="openSSOAuthenticationServicePort" value="${j2eeAgent.openSSOAuthenticationServicePort}" type="text" id="openSSOAuthenticationServicePort">
						<div class="amNote">
							<spring:message code="note.opensso_authentication_service_port" />
						</div>
						<form:errors path="openSSOAuthenticationServicePort" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.policy_client_service" /></h3>
					<div>
						<label><spring:message code="label.enable_policy_notifications" />:</label>
						<input name="enablePolicyNotifications" value="true" type="checkbox" id="enablePolicyNotifications" 
							<c:choose>
								<c:when test="${j2eeAgent.enablePolicyNotifications eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="enablePolicyNotifications"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.enable_policy_notifications" />
						</div>
					</div>
					<div>
						<label for="policyClientPollingInterval"><spring:message code="label.policy_client_polling_interval" />:</label>
						<input name="policyClientPollingInterval" value="${j2eeAgent.policyClientPollingInterval}" type="text" id="policyClientPollingInterval">
						<div class="amNote">
							<spring:message code="note.policy_client_polling_interval" />
						</div>
						<form:errors path="policyClientPollingInterval" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.policy_client_cache_mode" />:</label>
						<div class="amFormMultipleInputs">
							<input name="policyClientCacheMode" value="self" type="radio" id="policyClientCacheModeSelf"
								<c:choose>
									<c:when test="${j2eeAgent.policyClientCacheMode eq 'self'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="policyClientCacheModeSelf"><spring:message code="label.policy_client_cache_mode.self" /></label>
							<br>
							<input name="policyClientCacheMode" value="subtree" type="radio" id="policyClientCacheModeSubtree" 
								<c:choose>
									<c:when test="${j2eeAgent.policyClientCacheMode eq 'subtree'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="policyClientCacheModeSubtree"><spring:message code="label.policy_client_cache_mode.subtree" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.policy_client_cache_mode" />
						</div>
					</div>
					<div>
						<label for="policyClientBooleanActionValues"><spring:message code="label.policy_client_boolean_action_values" />:</label>
						<input name="policyClientBooleanActionValues" value="${j2eeAgent.policyClientBooleanActionValues}" type="text" id="policyClientBooleanActionValues">
						<div class="amNote">
							<spring:message code="note.policy_client_boolean_action_values" />
						</div>
					</div>
					<div>
						<label for="policyClientResourceComparators"><spring:message code="label.policy_client_resource_comparators" />:</label>
						<input name="policyClientResourceComparators" value="${j2eeAgent.policyClientResourceComparators}" type="text" id="policyClientResourceComparators">
						<div class="amNote">
							<spring:message code="note.policy_client_resource_comparators" />
						</div>
					</div>
					<div>
						<label for="policyClientClockSkew"><spring:message code="label.policy_client_clock_skew" />:</label>
						<input name="policyClientClockSkew" value="${j2eeAgent.policyClientClockSkew}" type="text" id="policyClientClockSkew">
						<div class="amNote">
							<spring:message code="note.policy_client_clock_skew" />
						</div>
						<form:errors path="policyClientClockSkew" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.url_policy_env_get_parameters" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="urlPolicyEnvGETParametersDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="upegp" items="${j2eeAgent.urlPolicyEnvGETParameters}">
									<input type="hidden" name="urlPolicyEnvGETParameters" value="${upegp}" />
								</c:forEach>	
								<select name="urlPolicyEnvGETParametersDeleteValues" multiple="multiple" id="urlPolicyEnvGETParametersDeleteValues">
									<c:forEach var="upegpv" items="${j2eeAgent.urlPolicyEnvGETParameters}">
										<option value="${upegpv}">${upegpv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>	
								<input type="submit" name="deleteUrlPolicyEnvGETParameters" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="urlPolicyEnvGETParametersAddValue"><spring:message code="label.new_value" /></label>
								<input name="urlPolicyEnvGETParametersAddValue" type="text" id="urlPolicyEnvGETParametersAddValue">
								<input type="submit" name="addUrlPolicyEnvGETParameters" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.url_policy_env_get_parameters" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.url_policy_env_post_parameters" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="urlPolicyEnvPOSTParametersDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="upepp" items="${j2eeAgent.urlPolicyEnvPOSTParameters}">
									<input type="hidden" name="urlPolicyEnvPOSTParameters" value="${upepp}" />
								</c:forEach>	
								<select name="urlPolicyEnvPOSTParametersDeleteValues" multiple="multiple" id="urlPolicyEnvPOSTParametersDeleteValues">
									<c:forEach var="upeppv" items="${j2eeAgent.urlPolicyEnvPOSTParameters}">
										<option value="${upeppv}">${upeppv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteUrlPolicyEnvPOSTParameters" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="urlPolicyEnvPOSTParametersAddValue"><spring:message code="label.new_value" /></label>
								<input name="urlPolicyEnvPOSTParametersAddValue" type="text" id="urlPolicyEnvPOSTParametersAddValue">
								<input type="submit" name="addUrlPolicyEnvPOSTParameters" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.url_policy_env_post_parameters" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.url_policy_env_jsession_parameters" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="urlPolicyEnvJsessionParametersDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="upejp" items="${j2eeAgent.urlPolicyEnvJsessionParameters}">
									<input type="hidden" name="urlPolicyEnvJsessionParameters" value="${upejp}" />
								</c:forEach>	
								<select name="urlPolicyEnvJsessionParametersDeleteValues" multiple="multiple" id="urlPolicyEnvJsessionParametersDeleteValues">
									<c:forEach var="upejpv" items="${j2eeAgent.urlPolicyEnvJsessionParameters}">
										<option value="${upejpv}">${upejpv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteUrlPolicyEnvJsessionParameters" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="urlPolicyEnvJsessionParametersAddValue"><spring:message code="label.new_value" /></label>
								<input name="urlPolicyEnvJsessionParametersAddValue" type="text" id="urlPolicyEnvJsessionParametersAddValue">
								<input type="submit" name="addUrlPolicyEnvJsessionParameters" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.url_policy_env_jsession_parameters" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.user_data_cache_service" /></h3>
					<div>
						<label><spring:message code="label.enable_notification_of_user_data_caches" />:</label>
						<input name="enableNotificationofUserDataCaches" value="true" type="checkbox" id="enableNotificationofUserDataCaches" 
							<c:choose>
								<c:when test="${j2eeAgent.enableNotificationofUserDataCaches eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="enableNotificationofUserDataCaches"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.enable_notification_of_user_data_caches" />
						</div>
					</div>
					<div>
						<label for="userDataCachePollingTime"><spring:message code="label.user_data_cache_polling_time" />:</label>
						<input name="userDataCachePollingTime" value="${j2eeAgent.userDataCachePollingTime}" type="text" id="userDataCachePollingTime">
						<div class="amNote">
							<spring:message code="note.user_data_cache_polling_time" />
						</div>
						<form:errors path="userDataCachePollingTime" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.enable_notification_of_service_data_caches" />:</label>
						<input name="enableNotificationOfServiceDataCaches" value="true" type="checkbox" id="enableNotificationOfServiceDataCaches"
							<c:choose>
								<c:when test="${j2eeAgent.enableNotificationOfServiceDataCaches eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="enableNotificationOfServiceDataCaches"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.enable_notification_of_service_data_caches" />
						</div>
					</div>
					<div>
						<label for="serviceDataCacheTime"><spring:message code="label.service_data_cache_time" />:</label>
						<input name="serviceDataCacheTime" value="${j2eeAgent.serviceDataCacheTime}" type="text" id="serviceDataCacheTime">
						<div class="amNote">
							<spring:message code="note.service_data_cache_time" />
						</div>		
						<form:errors path="serviceDataCacheTime" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.session_client_service" /></h3>
					<div>
						<label><spring:message code="label.enable_client_polling" />:</label>
						<input name="enableClientPolling" value="true" type="checkbox" id="enableClientPolling" 
							<c:choose>
								<c:when test="${j2eeAgent.enableClientPolling eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="enableClientPolling"><spring:message code="label.enabled" /></label>
						<div class="amNote">
							<spring:message code="note.enable_client_polling" />
						</div>
					</div>
					<div>
						<label for="clientPollingPeriod"><spring:message code="label.client_polling_period" />:</label>
						<input name="clientPollingPeriod" value="${j2eeAgent.clientPollingPeriod}" type="text" id="clientPollingPeriod">
						<div class="amNote">
							<spring:message code="note.client_polling_period" />
						</div>		
						<form:errors path="clientPollingPeriod" cssClass="portlet-msg-error"></form:errors>
					</div>
					
				</form:form>	
			</div>
		</div>	
	</div>
</div>