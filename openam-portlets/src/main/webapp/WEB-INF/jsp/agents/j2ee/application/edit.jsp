<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_application" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><b><spring:message code="label.menu.application" /></b></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentApplicationController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>	
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.login_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.login_from_uri"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="loginFromUriDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="lfu" items="${j2eeAgent.loginFromUri}">
									<input type="hidden" name="loginFromUri" value="${lfu}" />
								</c:forEach>	
								<select name="loginFromUriDeleteValues" multiple="multiple" id="loginFromUriDeleteValues">
									<c:forEach var="lfuv" items="${j2eeAgent.loginFromUri}">
										<option value="${lfuv}">${lfuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLoginFromUri" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="loginFromUriAddValue"><spring:message code="label.new_value" /></label>
								<input name="loginFromUriAddValue" type="text" id="loginFromUriAddValue">
								<input type="submit" name="addLoginFromUri" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.login_from_uri"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.login_error_uri"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="loginErrorUriDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="arufc" items="${j2eeAgent.loginErrorUri}">
									<input type="hidden" name="loginErrorUri" value="${arufc}" />
								</c:forEach>	
								<select name="loginErrorUriDeleteValues" multiple="multiple" id="loginErrorUriDeleteValues">
									<c:forEach var="arufcv" items="${j2eeAgent.loginErrorUri}">
										<option value="${arufcv}">${arufcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLoginErrorUri" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="loginErrorUriAddValue"><spring:message code="label.new_value" /></label>
								<input name="loginErrorUriAddValue" type="text" id="loginErrorUriAddValue">
								<input type="submit" name="addLoginErrorUri" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.login_error_uri"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.use_internal_login"/>:</label>
						<input name="useInternalLogin" value="true" type="checkbox" id="useInternalLogin" 
							<c:choose>
								<c:when test="${j2eeAgent.useInternalLogin eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> > 
						<label class="amInputLabel" for="useInternalLogin"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.use_internal_login"/>
						</div>
					</div>
					<div>
						<label for="loginContentFileName"><spring:message code="label.login_content_file_name"/>:</label>
						<input name="loginContentFileName" value="${j2eeAgent.loginContentFileName}" type="text" id="loginContentFileName">
						<div class="amNote">
							<spring:message code="note.login_content_file_name"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.logout_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.application_logout_handler"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="applicationLogoutHandlerDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="alh" items="${j2eeAgent.applicationLogoutHandler}">
									<input type="hidden" name="applicationLogoutHandler" value="${alh}" />
								</c:forEach>	
								<select name="applicationLogoutHandlerDeleteValues" multiple="multiple" id="applicationLogoutHandlerDeleteValues">
									<c:forEach var="alhv" items="${j2eeAgent.applicationLogoutHandler}">
										<option value="${alhv}">${alhv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteApplicationLogoutHandler" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="applicationLogoutHandlerKey"><spring:message code="label.map_key" />:</label>
									<input name="applicationLogoutHandlerKey" type="text" id="applicationLogoutHandlerKey">
								</div>
								<div class="amFormCell">
									<label for="applicationLogoutHandlerValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="applicationLogoutHandlerValue" type="text" id="applicationLogoutHandlerValue">
								</div>
								<input type="submit" name="addApplicationLogoutHandler" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.application_logout_handler"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.application_logout_uri"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="applicationLogoutUriDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="alu" items="${j2eeAgent.applicationLogoutUri}">
									<input type="hidden" name="applicationLogoutUri" value="${alu}" />
								</c:forEach>	
								<select name="applicationLogoutUriDeleteValues" multiple="multiple" id="applicationLogoutUriDeleteValues">
									<c:forEach var="aluv" items="${j2eeAgent.applicationLogoutUri}">
										<option value="${aluv}">${aluv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteApplicationLogoutUri" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="applicationLogoutUriKey"><spring:message code="label.map_key" />:</label>
									<input name="applicationLogoutUriKey" type="text" id="applicationLogoutUriKey">
								</div>
								<div class="amFormCell">
									<label for="applicationLogoutUriValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="applicationLogoutUriValue" type="text" id="applicationLogoutUriValue">
								</div>
								<input type="submit" name="addApplicationLogoutUri" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.application_logout_uri"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.logout_reguest_parameter"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="logoutReguestParameterDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="lrp" items="${j2eeAgent.logoutReguestParameter}">
									<input type="hidden" name="logoutReguestParameter" value="${lrp}" />
								</c:forEach>	
								<select name="logoutReguestParameterDeleteValues" multiple="multiple" id="logoutReguestParameterDeleteValues">
									<c:forEach var="lrpv" items="${j2eeAgent.logoutReguestParameter}">
										<option value="${lrpv}">${lrpv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLogoutReguestParameter" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="logoutReguestParameterKey"><spring:message code="label.map_key" />:</label>
									<input name="logoutReguestParameterKey" type="text" id="logoutReguestParameterKey">
								</div>
								<div class="amFormCell">
									<label for="logoutReguestParameterValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="logoutReguestParameterValue" type="text" id="logoutReguestParameterValue">
								</div>
								<input type="submit" name="addLogoutReguestParameter" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.logout_reguest_parameter"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.logout_introspect_enabled"/>:</label>
						<input name="logoutIntrospectEnabled" value="true" type="checkbox" id="logoutIntrospectEnabled" 
							<c:choose>
								<c:when test="${j2eeAgent.logoutIntrospectEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="logoutIntrospectEnabled"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.logout_introspect_enabled"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.logout_entry_uri"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="logoutEntryUriDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="leu" items="${j2eeAgent.logoutEntryUri}">
									<input type="hidden" name="logoutEntryUri" value="${leu}" />
								</c:forEach>	
								<select name="logoutEntryUriDeleteValues" multiple="multiple" id="logoutEntryUriDeleteValues">
									<c:forEach var="leuv" items="${j2eeAgent.logoutEntryUri}">
										<option value="${leuv}">${leuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteLogoutEntryUri" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="logoutEntryUriKey"><spring:message code="label.map_key" />:</label>
									<input name="logoutEntryUriKey" type="text" id="logoutEntryUriKey" >
								</div>
								<div class="amFormCell">
									<label for="logoutEntryUriValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="logoutEntryUriValue" type="text" id="logoutEntryUriValue">
								</div>
								<input type="submit" name="addLogoutEntryUri" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.logout_entry_uri"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.access_denied_uri_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.response_access_denied_uri"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="responseAccessDeniedUriDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="radu" items="${j2eeAgent.responseAccessDeniedUri}">
									<input type="hidden" name="responseAccessDeniedUri" value="${radu}" />
								</c:forEach>	
								<select name="responseAccessDeniedUriDeleteValues" multiple="multiple" id="responseAccessDeniedUriDeleteValues">
									<c:forEach var="raduv" items="${j2eeAgent.responseAccessDeniedUri}">
										<option value="${raduv}">${raduv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteResponseAccessDeniedUri" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="responseAccessDeniedUriKey"><spring:message code="label.map_key" />:</label>
									<input name="responseAccessDeniedUriKey" type="text" id="responseAccessDeniedUriKey">
								</div>
								<div class="amFormCell">
									<label for="responseAccessDeniedUriValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="responseAccessDeniedUriValue" type="text" id="responseAccessDeniedUriValue">
								</div>
								<input type="submit" name="addResponseAccessDeniedUri" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.response_access_denied_uri"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.not_enforced_uri_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.not_enforced_uris"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="notEnforcedUrisDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="neu" items="${j2eeAgent.notEnforcedUris}">
									<input type="hidden" name="notEnforcedUris" value="${neu}" />
								</c:forEach>	
								<select name="notEnforcedUrisDeleteValues" multiple="multiple" id="notEnforcedUrisDeleteValues">
									<c:forEach var="neuv" items="${j2eeAgent.notEnforcedUris}">
										<option value="${neuv}">${neuv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNotEnforcedUris" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="notEnforcedUrisAddValue"><spring:message code="label.new_value" /></label>
								<input name="notEnforcedUrisAddValue" type="text" id="notEnforcedUrisAddValue">
								<input type="submit" name="addNotEnforcedUris" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.not_enforced_uris"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.invert_not_enforces_uris"/>:</label>
						<input name="invertNotEnforcesUris" value="true" type="checkbox" id="invertNotEnforcesUris" 
							<c:choose>
								<c:when test="${j2eeAgent.invertNotEnforcesUris eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="invertNotEnforcesUris"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.invert_not_enforces_uris"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.not_enforced_uris_cache_enabled"/>:</label>
						<input name="notEnforcedUrisCacheEnabled" value="true" type="checkbox" id="notEnforcedUrisCacheEnabled" 
							<c:choose>
								<c:when test="${j2eeAgent.notEnforcedUrisCacheEnabled eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="notEnforcedUrisCacheEnabled"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.not_enforced_uris_cache_enabled"/>
						</div>
					</div>
					<div>
						<label for="notEnforcedUrisCacheSize"><spring:message code="label.not_enforced_uris_cache_size"/>:</label>
						<input name="notEnforcedUrisCacheSize" value="${j2eeAgent.notEnforcedUrisCacheSize}" type="text" id="notEnforcedUrisCacheSize">
						<div class="amNote">
							<spring:message code="note.not_enforced_uris_cache_size"/>
						</div>
						<form:errors path="notEnforcedUrisCacheSize" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.refresh_session_idle_time"/>:</label>
						<input name="refreshSessionIdleTime" value="true" type="checkbox" id="refreshSessionIdleTime" 
							<c:choose>
								<c:when test="${j2eeAgent.refreshSessionIdleTime eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="refreshSessionIdleTime"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.refresh_session_idle_time"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.not_enforced_ip_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.not_enforced_client_ip_list"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="notEnforcedClientIpListDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="necil" items="${j2eeAgent.notEnforcedClientIpList}">
									<input type="hidden" name="notEnforcedClientIpList" value="${necil}" />
								</c:forEach>	
								<select name="notEnforcedClientIpListDeleteValues" multiple="multiple" id="notEnforcedClientIpListDeleteValues">
									<c:forEach var="necilv" items="${j2eeAgent.notEnforcedClientIpList}">
										<option value="${necilv}">${necilv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteNotEnforcedClientIpList" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="notEnforcedClientIpListAddValue"><spring:message code="label.new_value" /></label>
								<input name="notEnforcedClientIpListAddValue" type="text" id="notEnforcedClientIpListAddValue">
								<input type="submit" name="addNotEnforcedClientIpList" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.not_enforced_client_ip_list"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.not_enforced_ip_invert_list"/>:</label>
						<input name="notEnforcedIpInvertList" value="true" type="checkbox" id="notEnforcedIpInvertList" 
							<c:choose>
								<c:when test="${j2eeAgent.notEnforcedIpInvertList eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="notEnforcedIpInvertList"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.not_enforced_ip_invert_list"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.not_enforced_ip_cache_flag"/>:</label>
						<input name="notEnforcedIpCacheFlag" value="true" type="checkbox" id="notEnforcedIpCacheFlag" 
							<c:choose>
								<c:when test="${j2eeAgent.notEnforcedIpCacheFlag eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="notEnforcedIpCacheFlag"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.not_enforced_ip_cache_flag"/>
						</div>
					</div>
					<div>
						<label for="notEnforcedIpCacheSize"><spring:message code="label.not_enforced_ip_cache_size"/>:</label>
						<input name="notEnforcedIpCacheSize" value="${j2eeAgent.notEnforcedIpCacheSize}" type="text" id="notEnforcedIpCacheSize">
						<div class="amNote">
							<spring:message code="note.not_enforced_ip_cache_size"/>
						</div>		
						<form:errors path="notEnforcedIpCacheSize" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.profile_attributes_processing"/></h3>
					<div>
						<label><spring:message code="label.profile_attribute_fetch_mode"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="profileAttributeFetchMode" value="HTTP_COOKIE" type="radio" id="profileAttributeFetchModeHttpCoockie"
								<c:choose>
									<c:when test="${j2eeAgent.profileAttributeFetchMode eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeHttpCoockie"><spring:message code="label.http_cookie"/></label>
							<br>
							<input name="profileAttributeFetchMode" value="HTTP_HEADER" type="radio" id="profileAttributeFetchModeHttpHeader"
								<c:choose>
									<c:when test="${j2eeAgent.profileAttributeFetchMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeHttpHeader"><spring:message code="label.http_header"/></label>
							<br>
							<input name="profileAttributeFetchMode" value="NONE" type="radio" id="profileAttributeFetchModeNone"
								<c:choose>
									<c:when test="${j2eeAgent.profileAttributeFetchMode eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeNone"><spring:message code="label.none"/></label>
							<br>
							<input name="profileAttributeFetchMode" value="REQUEST_ATTRIBUTE" type="radio" id="profileAttributeFetchModeRequestAttribute"
								<c:choose>
									<c:when test="${j2eeAgent.profileAttributeFetchMode eq 'REQUEST_ATTRIBUTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="profileAttributeFetchModeRequestAttribute"><spring:message code="label.request_attribute"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_fetch_mode"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.profile_attribute_mapping"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="profileAttributeMappingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="pam" items="${j2eeAgent.profileAttributeMapping}">
									<input type="hidden" name="profileAttributeMapping" value="${pam}" />
								</c:forEach>	
								<select name="profileAttributeMappingDeleteValues" multiple="multiple" id="profileAttributeMappingDeleteValues">
									<c:forEach var="pamv" items="${j2eeAgent.profileAttributeMapping}">
										<option value="${pamv}">${pamv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteProfileAttributeMapping" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="profileAttributeMappingKey"><spring:message code="label.map_key" />:</label>
									<input name="profileAttributeMappingKey" type="text" id="profileAttributeMappingKey">
								</div>
								<div class="amFormCell">
									<label for="profileAttributeMappingValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="profileAttributeMappingValue" type="text" id="profileAttributeMappingValue">
								</div>
								<input type="submit" name="addProfileAttributeMapping" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.profile_attribute_mapping"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.respunse_attributes_processing"/></h3>
					<div>
						<label><spring:message code="label.response_attribute_fetch_mode"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="responseAttributeFetchModel" value="HTTP_COOKIE" type="radio" id="responseAttributeFetchModelHttpCookie"
								<c:choose>
									<c:when test="${j2eeAgent.responseAttributeFetchModel eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModelHttpCookie"><spring:message code="label.http_cookie"/></label>
							<br>
							<input name="responseAttributeFetchModel" value="HTTP_HEADER" type="radio" id="responseAttributeFetchModelHttpHeader"
								<c:choose>
									<c:when test="${j2eeAgent.responseAttributeFetchModel eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModelHttpHeader"><spring:message code="label.http_header"/></label>
							<br>
							<input name="responseAttributeFetchModel" value="NONE" type="radio" id="responseAttributeFetchModelNone" 
								<c:choose>
									<c:when test="${j2eeAgent.responseAttributeFetchModel eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModelNone"><spring:message code="label.none"/></label>
							<br>
							<input name="responseAttributeFetchModel" value="REQUEST_ATTRIBUTE" type="radio" id="responseAttributeFetchModelRequestAttribute"
								<c:choose>
									<c:when test="${j2eeAgent.responseAttributeFetchModel eq 'REQUEST_ATTRIBUTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="responseAttributeFetchModelRequestAttribute"><spring:message code="label.request_attribute"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.response_attribute_fetch_mode.j2ee"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.response_attribute_mapping"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="responseAttributeMappingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="ram" items="${j2eeAgent.responseAttributeMapping}">
									<input type="hidden" name="responseAttributeMapping" value="${ram}" />
								</c:forEach>	
								<select name="responseAttributeMappingDeleteValues" multiple="multiple" id="responseAttributeMappingDeleteValues">
									<c:forEach var="ramv" items="${j2eeAgent.responseAttributeMapping}">
										<option value="${ramv}">${ramv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteResponseAttributeMapping" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="responseAttributeMappingKey"><spring:message code="label.map_key" />:</label>
									<input name="responseAttributeMappingKey" type="text" id="responseAttributeMappingKey">
								</div>
								<div class="amFormCell">
									<label for="responseAttributeMappingValue"><spring:message code="label.corresponding_map_value" />:</label>
										<input name="responseAttributeMappingValue" type="text" id="responseAttributeMappingValue">
								</div>
								<input type="submit" name="addResponseAttributeMapping" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.response_attribute_mapping"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.common_attributes_fetching_processing"/></h3>
					<div>
						<label for="cookieSeparatorCharacter"><spring:message code="label.cookie_separator_character"/>:</label>
						<input name="cookieSeparatorCharacter" value="${j2eeAgent.cookieSeparatorCharacter}" type="text" id="cookieSeparatorCharacter">
						<div class="amNote">
							<spring:message code="note.cookie_separator_character"/>
						</div>
					</div>
					<div>
						<label for="fetchAttributeDateFormat"><spring:message code="label.fetch_attribute_date_format"/>:</label>
						<input name="fetchAttributeDateFormat" value="${j2eeAgent.fetchAttributeDateFormat}" type="text" id="fetchAttributeDateFormat">	
						<div class="amNote">
							<spring:message code="note.fetch_attribute_date_format"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.attribute_cookie_encode"/>:</label>
						<input name="attributeCookieEncode" value="true" type="checkbox" id="attributeCookieEncode" 
							<c:choose>
								<c:when test="${j2eeAgent.attributeCookieEncode eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="attributeCookieEncode"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.attribute_cookie_encode"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.session_attributes_processing"/></h3>
					<div>
						<label><spring:message code="label.session_attribute_fetch_mode"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="sessionAttributeFetchMode" value="HTTP_COOKIE" type="radio" id="sessionAttributeFetchModeHttpCoockie"
								<c:choose>
									<c:when test="${j2eeAgent.sessionAttributeFetchMode eq 'HTTP_COOKIE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeHttpCoockie"><spring:message code="label.http_cookie"/></label>
							<br>
							<input name="sessionAttributeFetchMode" value="HTTP_HEADER" type="radio" id="sessionAttributeFetchModeHttpHeader" 
								<c:choose>
									<c:when test="${j2eeAgent.sessionAttributeFetchMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeHttpHeader"><spring:message code="label.http_header"/></label>
							<br>
							<input name="sessionAttributeFetchMode" value="NONE" type="radio" id="sessionAttributeFetchModeNone"
								<c:choose>
									<c:when test="${j2eeAgent.sessionAttributeFetchMode eq 'NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeNone"><spring:message code="label.none"/></label>
							<br>
							<input name="sessionAttributeFetchMode" value="REQUEST_ATTRIBUTE" type="radio" id="sessionAttributeFetchModeRequestAttribute" 
								<c:choose>
									<c:when test="${j2eeAgent.sessionAttributeFetchMode eq 'REQUEST_ATTRIBUTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="sessionAttributeFetchModeRequestAttribute"><spring:message code="label.request_attribute"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.session_attribute_fetch_mode_j2ee"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.session_attribute_mapping"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="sessionAttributeMappingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="sam" items="${j2eeAgent.sessionAttributeMapping}">
									<input type="hidden" name="sessionAttributeMapping" value="${sam}" />
								</c:forEach>	
								<select name="sessionAttributeMappingDeleteValues" multiple="multiple" id="sessionAttributeMappingDeleteValues">
									<c:forEach var="samv" items="${j2eeAgent.sessionAttributeMapping}">
										<option value="${samv}">${samv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteSessionAttributeMapping" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="sessionAttributeMappingKey"><spring:message code="label.map_key" />:</label>
									<input name="sessionAttributeMappingKey" type="text" id="sessionAttributeMappingKey">
								</div>
								<div class="amFormCell">
									<label for="sessionAttributeMappingValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="sessionAttributeMappingValue" type="text" id="sessionAttributeMappingValue">
								</div>
								<input type="submit" name="addSessionAttributeMapping" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.session_attribute_mapping"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.privilege_attributes_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.default_privileged_attribute"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="defaultPrivilegedAttributeDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="dpa" items="${j2eeAgent.defaultPrivilegedAttribute}">
									<input type="hidden" name="defaultPrivilegedAttribute" value="${dpa}" />
								</c:forEach>	
								<select name="defaultPrivilegedAttributeDeleteValues" multiple="multiple" id="defaultPrivilegedAttributeDeleteValues">
									<c:forEach var="dpav" items="${j2eeAgent.defaultPrivilegedAttribute}">
										<option value="${dpav}">${dpav}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteDefaultPrivilegedAttribute" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="defaultPrivilegedAttributeAddValue"><spring:message code="label.new_value" /></label>
								<input name="defaultPrivilegedAttributeAddValue" type="text" id="defaultPrivilegedAttributeAddValue">
								<input type="submit" name="addDefaultPrivilegedAttribute" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.default_privileged_attribute"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.privileged_attribute_type"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="privilegedAttributeTypeDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="pat" items="${j2eeAgent.privilegedAttributeType}">
									<input type="hidden" name="privilegedAttributeType" value="${pat}" />
								</c:forEach>	
								<select name="privilegedAttributeTypeDeleteValues" multiple="multiple" id="privilegedAttributeTypeDeleteValues">
									<c:forEach var="patv" items="${j2eeAgent.privilegedAttributeType}">
										<option value="${patv}">${patv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deletePrivilegedAttributeType" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="privilegedAttributeTypeAddValue"><spring:message code="label.new_value" /></label>
								<input name="privilegedAttributeTypeAddValue" type="text" id="privilegedAttributeTypeAddValue">
								<input type="submit" name="addPrivilegedAttributeType" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.privileged_attribute_type"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.privileged_attributes_to_lower_case"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="privilegedAttributesToLowerCaseDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="patlc" items="${j2eeAgent.privilegedAttributesToLowerCase}">
									<input type="hidden" name="privilegedAttributesToLowerCase" value="${patlc}" />
								</c:forEach>	
								<select name="privilegedAttributesToLowerCaseDeleteValues" multiple="multiple" id="privilegedAttributesToLowerCaseDeleteValues">
									<c:forEach var="patlcv" items="${j2eeAgent.privilegedAttributesToLowerCase}">
										<option value="${patlcv}">${patlcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deletePrivilegedAttributesToLowerCase" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="privilegedAttributesToLowerCaseKey"><spring:message code="label.map_key" />:</label>
									<input name="privilegedAttributesToLowerCaseKey" type="text" id="privilegedAttributesToLowerCaseKey">
								</div>
								<div class="amFormCell">
									<label for="privilegedAttributesToLowerCaseValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="privilegedAttributesToLowerCaseValue" type="text" id="privilegedAttributesToLowerCaseValue">
								</div>
								<input type="submit" name="addPrivilegedAttributesToLowerCase" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.privileged_attributes_to_lower_case"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.privileged_session_attribute"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="privilegedSessionAttributeDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="arufc" items="${j2eeAgent.privilegedSessionAttribute}">
									<input type="hidden" name="privilegedSessionAttribute" value="${arufc}" />
								</c:forEach>	
								<select name="privilegedSessionAttributeDeleteValues" multiple="multiple" id="privilegedSessionAttributeDeleteValues">
									<c:forEach var="arufcv" items="${j2eeAgent.privilegedSessionAttribute}">
										<option value="${arufcv}">${arufcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deletePrivilegedSessionAttribute" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="privilegedSessionAttributeAddValue"><spring:message code="label.new_value" /></label>
								<input name="privilegedSessionAttributeAddValue" type="text" id="privilegedSessionAttributeAddValue">
								<input type="submit" name="addPrivilegedSessionAttribute" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.privileged_session_attribute"/> 
						</div>
					</div>
					<div>
						<label><spring:message code="label.enable_privileged_attribute_mapping"/>:</label>
						<input name="enablePrivilegedAttributeMapping" value="true" type="checkbox" id="enablePrivilegedAttributeMapping"
							<c:choose>
								<c:when test="${j2eeAgent.enablePrivilegedAttributeMapping eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="enablePrivilegedAttributeMapping"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.enable_privileged_attribute_mapping"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.privileged_attribute_mapping"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="privilegedAttributeMappingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="pam" items="${j2eeAgent.privilegedAttributeMapping}">
									<input type="hidden" name="privilegedAttributeMapping" value="${pam}" />
								</c:forEach>	
								<select name="privilegedAttributeMappingDeleteValues" multiple="multiple" id="privilegedAttributeMappingDeleteValues">
									<c:forEach var="pamv" items="${j2eeAgent.privilegedAttributeMapping}">
										<option value="${pamv}">${pamv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deletePrivilegedAttributeMapping" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="privilegedAttributeMappingKey"><spring:message code="label.map_key" />:</label>
									<input name="privilegedAttributeMappingKey" type="text" id="privilegedAttributeMappingKey">
								</div>
								<div class="amFormCell">
									<label for="privilegedAttributeMappingValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="privilegedAttributeMappingValue" type="text" id="privilegedAttributeMappingValue">
								</div>
								<input type="submit" name="addPrivilegedAttributeMapping" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.privileged_attribute_mapping"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.custom_authentication_processing"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_authentication_handler"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customAuthenticationProcessingDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="cap" items="${j2eeAgent.customAuthenticationProcessing}">
									<input type="hidden" name="customAuthenticationProcessing" value="${cap}" />
								</c:forEach>	
								<select name="customAuthenticationProcessingDeleteValues" multiple="multiple" id="customAuthenticationProcessingDeleteValues">
									<c:forEach var="capv" items="${j2eeAgent.customAuthenticationProcessing}">
										<option value="${capv}">${capv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomAuthenticationProcessing" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="customAuthenticationProcessingKey"><spring:message code="label.map_key" />:</label>
									<input name="customAuthenticationProcessingKey" type="text" id="customAuthenticationProcessingKey">
								</div>
								<div class="amFormCell">
									<label for="customAuthenticationProcessingValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="customAuthenticationProcessingValue" type="text" id="customAuthenticationProcessingValue">
								</div>
								<input type="submit" name="addCustomAuthenticationProcessing" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_authentication_handler"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_logout_handler"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customLogoutHandlerDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="clh" items="${j2eeAgent.customLogoutHandler}">
									<input type="hidden" name="customLogoutHandler" value="${clh}" />
								</c:forEach>	
								<select name="customLogoutHandlerDeleteValues" multiple="multiple" id="customLogoutHandlerDeleteValues">
									<c:forEach var="clhv" items="${j2eeAgent.customLogoutHandler}">
										<option value="${clhv}">${clhv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomLogoutHandler" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="customLogoutHandlerKey"><spring:message code="label.map_key" />:</label>
									<input name="customLogoutHandlerKey" type="text" id="customLogoutHandlerKey">
								</div>
								<div class="amFormCell">
									<label for="customLogoutHandlerValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="customLogoutHandlerValue" type="text">
								</div>
								<input type="submit" name="addCustomLogoutHandler" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_logout_handler"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_verification_handler"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customVerificationHandlerDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="afm" items="${j2eeAgent.customVerificationHandler}">
									<input type="hidden" name="customVerificationHandler" value="${afm}" />
								</c:forEach>	
								<select name="customVerificationHandlerDeleteValues" multiple="multiple" id="customVerificationHandlerDeleteValues">
									<c:forEach var="afmv" items="${j2eeAgent.customVerificationHandler}">
										<option value="${afmv}">${afmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomVerificationHandler" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="customVerificationHandlerKey"><spring:message code="label.map_key" />:</label>
									<input name="customVerificationHandlerKey" type="text" id="customVerificationHandlerKey">
								</div>
								<div class="amFormCell">
									<label for="customVerificationHandlerValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="customVerificationHandlerValue" type="text" id="customVerificationHandlerValue">
								</div>
								<input type="submit" name="addCustomVerificationHandler" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_verification_handler"/>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
