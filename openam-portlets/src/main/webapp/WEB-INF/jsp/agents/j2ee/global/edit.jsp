<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_global" />
	</div>
	
	<div class="amFormContent">
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><b><spring:message code="label.menu.global" /></b></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><spring:message code="label.menu.advanced" /></a></li>
				</ul>		
			</div>
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentGlobalController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:choose>
						<c:when test="${not empty message}">
							<span class="portlet-msg-success"><spring:message code="${message}"/></span>
						</c:when>
						<c:when test="${not empty error}">
							<span class="portlet-msg-error"><spring:message code="${error}"/></span>
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.profile" /></h3>
					<div>
						<label for="group"><spring:message code="label.group"/>:</label>
						<select name="group" id="group">
							<option value=""><spring:message code="action.none" /></option>
							<c:forEach var="item" items="${groupsList}">
								<option value="${item}"
									<c:choose>
										<c:when test="${item eq j2eeAgent.group}">selected="selected"</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								>${item}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label for="password">*<spring:message code="label.password"/>:</label>
						<input name="password" value="${j2eeAgent.password}" type="password" id="password"> 
						<form:errors path="password" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="passwordConfirm">*<spring:message code="label.password_confirm"/>:</label>
						<input name="passwordConfirm" value="${j2eeAgent.passwordConfirm}" type="password" id="passwordConfirm"> 
						<form:errors path="passwordConfirm" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label>*<spring:message code="label.status"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="status" value="Active" type="radio" id="statusActive"
								<c:choose>
									<c:when test="${j2eeAgent.status eq 'Active'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="statusActive"><spring:message code="label.active"/></label>
							<br>
							<input  name="status" value="Inactive" type="radio" id="statusInactive"
								<c:choose>
									<c:when test="${j2eeAgent.status eq 'Inactive'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="statusInactive"><spring:message code="label.inactive"/></label> 
						</div>
						<form:errors path="status" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label for="agentNotificationURL"><spring:message code="label.agent_notification_url" />:</label>
						<input name="agentNotificationURL" value="${j2eeAgent.agentNotificationURL}" type="text" id="agentNotificationURL">
						<div class="amNote">
							<spring:message code="note.agent_notification_url" />
						</div>
					</div>
					<div>
						<label><spring:message code="label.location_of_agent_configuration_repository"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="locationOfAgentConfigurationRepository" value="local" type="radio" id="locationOfAgentConfigurationRepositoryLocal"
								<c:choose>
									<c:when test="${j2eeAgent.locationOfAgentConfigurationRepository eq 'local'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="locationOfAgentConfigurationRepositoryLocal"><spring:message code="label.local"/></label>
							<br>
							<input name="locationOfAgentConfigurationRepository" value="centralized" type="radio" id="locationOfAgentConfigurationRepositoryCentralized"
								<c:choose>
									<c:when test="${j2eeAgent.locationOfAgentConfigurationRepository eq 'centralized'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="locationOfAgentConfigurationRepositoryCentralized"><spring:message code="label.centralized"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.location_of_agent_configuration_repository"/>
						</div>
					</div>
					<div>
						<label for="configurationReloadInterval"><spring:message code="label.configuration_reload_interval" />:</label>
						<input name="configurationReloadInterval" value="${j2eeAgent.configurationReloadInterval}" type="text" id="configurationReloadInterval">
						<div class="amNote">
							<spring:message code="note.configuration_reload_interval_j2ee" />		
						</div>
						<form:errors path="configurationReloadInterval" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.agent_configuration_change_notification"/>:</label>
						<input name="agentConfigurationChangeNotification" value="true" type="checkbox" id="agentConfigurationChangeNotification"
							<c:choose>
								<c:when test="${j2eeAgent.agentConfigurationChangeNotification eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="agentConfigurationChangeNotification"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.agent_configuration_change_notification_j2ee"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.agent_root_url_for_cdsso"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="agentRootUrlForCdssoDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="arufc" items="${j2eeAgent.agentRootUrlForCdsso}">
									<input type="hidden" name="agentRootUrlForCdsso" value="${arufc}" />
								</c:forEach>	
								<select name="agentRootUrlForCdssoDeleteValues" multiple="multiple" id="agentRootUrlForCdssoDeleteValues">
									<c:forEach var="arufcv" items="${j2eeAgent.agentRootUrlForCdsso}">
										<option value="${arufcv}">${arufcv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteAgentRootUrlForCdsso" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="agentRootUrlForCdssoAddValue"><spring:message code="label.new_value" /></label>
								<input name="agentRootUrlForCdssoAddValue" type="text" id="agentRootUrlForCdssoAddValue">
								<input type="submit" name="addAgentRootUrlForCdsso" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.agent_root_url_for_cdsso"/>
						</div>
						<form:errors path="agentRootUrlForCdsso" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.general" /></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.agent_filter_mode" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="agentFilterModeDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="afm" items="${j2eeAgent.agentFilterMode}">
									<input type="hidden" name="agentFilterMode" value="${afm}" />
								</c:forEach>	
								<select name="agentFilterModeDeleteValues" multiple="multiple" id="agentFilterModeDeleteValues">
									<c:forEach var="afmv" items="${j2eeAgent.agentFilterMode}">
										<option value="${afmv}">${afmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteAgentFilterMode" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="agentFilterModeKey"><spring:message code="label.map_key" />:</label>
									<input name="agentFilterModeKey" type="text" id="agentFilterModeKey">
								</div>
								<div class="amFormCell">
									<label for="agentFilterModeValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="agentFilterModeValue" type="text" id="agentFilterModeValue">
								</div>
								<input type="submit" name="addAgentFilterMode" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.agent_filter_mode" />
						</div>
						<form:errors path="agentFilterMode" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.http_session_binding" />:</label>
						<input name="httpSessionBinding" value="true" type="checkbox" id="httpSessionBinding"
							<c:choose>
								<c:when test="${j2eeAgent.httpSessionBinding eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> > 
						<label class="amInputLabel" for="httpSessionBinding"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.http_session_binding" />
						</div>
					</div>
					<div>
						<label for="loginAttemptLimit"><spring:message code="label.login_attempt_limit" />:</label>
						<input name="loginAttemptLimit" value="${j2eeAgent.loginAttemptLimit}" type="text" id="loginAttemptLimit">
						<div class="amNote">
							<spring:message code="note.login_attempt_limit" />
						</div>
						<form:errors path="loginAttemptLimit" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_response_header" /></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customResponseHeaderDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="crh" items="${j2eeAgent.customResponseHeader}">
									<input type="hidden" name="customResponseHeader" value="${crh}" />
								</c:forEach>	
								<select name="customResponseHeaderDeleteValues" multiple="multiple" id="customResponseHeaderDeleteValues">
									<c:forEach var="crhv" items="${j2eeAgent.customResponseHeader}">
										<option value="${crhv}">${crhv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomResponseHeader" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="customResponseHeaderKey"><spring:message code="label.map_key" />:</label>
									<input name="customResponseHeaderKey" type="text" id="customResponseHeaderKey">
								</div>
								<div class="amFormCell">
									<label for="customResponseHeaderValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="customResponseHeaderValue" type="text" id="customResponseHeaderValue">
								</div>
								<input type="submit" name="addCustomResponseHeader" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_response_header" />
						</div>
					</div>
					<div>
						<label for="redirectAttemptLimit"><spring:message code="label.redirect_attempt_limit" />:</label>
						<input name="redirectAttemptLimit" value="${j2eeAgent.redirectAttemptLimit}" type="text" id="redirectAttemptLimit">
						<div class="amNote">
							<spring:message code="note.redirect_attempt_limit" />
						</div>
						<form:errors path="redirectAttemptLimit" cssClass="portlet-msg-error"></form:errors>
					</div>
					<div>
						<label><spring:message code="label.agent_debug_level" />:</label>
						<div class="amFormMultipleInputs">
							<input name="agentDebugLevel" id="agentDebugLevel_error" value="error" type="radio"
								<c:choose>
									<c:when test="${j2eeAgent.agentDebugLevel eq 'error'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevel_error"><spring:message code="label.agent_debug_level.error" /></label>
							<br>
							<input name="agentDebugLevel" id="agentDebugLevel_message" value="message" type="radio"
								<c:choose>
									<c:when test="${j2eeAgent.agentDebugLevel eq 'message'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevel_message"><spring:message code="label.agent_debug_level.message" /></label>
							<br>
							<input name="agentDebugLevel" id="agentDebugLevel_off" value="off" type="radio"
								<c:choose>
									<c:when test="${j2eeAgent.agentDebugLevel eq 'off'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevel_off"><spring:message code="label.agent_debug_level.off" /></label>
							<br>
							<input name="agentDebugLevel" id="agentDebugLevel_warning" value="warning" type="radio"
								<c:choose>
									<c:when test="${j2eeAgent.agentDebugLevel eq 'warning'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="agentDebugLevel_warning"><spring:message code="label.agent_debug_level.warning" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.agent_debug_level" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="label.user_mapping" /></h3>
					<div>
						<label><spring:message code="label.user_mapping_mode" />:</label>
						<div class="amFormMultipleInputs">
							<input name="userMappingMode" value="HTTP_HEADER" type="radio" id="userMappingModeHttpHeader"
								<c:choose>
									<c:when test="${j2eeAgent.userMappingMode eq 'HTTP_HEADER'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="userMappingModeHttpHeader"><spring:message code="label.user_mapping_mode.http_header" /></label>
							<br>
							<input name="userMappingMode" value="PROFILE_ATTRIBUTE" type="radio" id="userMappingModeRequestAttribute"
								<c:choose>
									<c:when test="${j2eeAgent.userMappingMode eq 'PROFILE_ATTRIBUTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="userMappingModeRequestAttribute"><spring:message code="label.user_mapping_mode.profile_attribute" /></label>
							<br>
							<input name="userMappingMode" value="SESSION_PROPERTY" type="radio" id="userMappingModeSessionProperty"
								<c:choose>
									<c:when test="${j2eeAgent.userMappingMode eq 'SESSION_PROPERTY'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="userMappingModeSessionProperty"><spring:message code="label.user_mapping_mode.session_property" /></label>
							<br>
							<input name="userMappingMode" value="USER_ID" type="radio" id="userMappingModeUserId"
								<c:choose>
									<c:when test="${j2eeAgent.userMappingMode eq 'USER_ID'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="userMappingModeUserId"><spring:message code="label.user_mapping_mode.user_id" /></label>
						</div>
						<div class="amNote">
							<spring:message code="note.user_mapping_mode" />
						</div>
					</div>
					<div>
						<label for="userAttributeName"><spring:message code="label.user_attribute_name" />:</label>
						<input name="userAttributeName" value="${j2eeAgent.userAttributeName}" type="text" id="userAttributeName">
						<div class="amNote">
							<spring:message code="note.user_attribute_name" />
						</div>
					</div>	
					<div>
						<label><spring:message code="label.user_principal_flag" />:</label>
						<input name="userPrincipalFlag" value="true" type="checkbox" id="userPrincipalFlag" 
							<c:choose>
								<c:when test="${j2eeAgent.userPrincipalFlag eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> > 
						<label class="amInputLabel" for="userPrincipalFlag"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.user_principal_flag" />
						</div>
					</div>
					<div>
						<label for="userTokenName"><spring:message code="label.user_token_name" />:</label>
						<input name="userTokenName" value="${j2eeAgent.userTokenName}" type="text" id="userTokenName">
						<div class="amNote">
							<spring:message code="note.user_token_name" />
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.audit"/></h3>
					<div>
						<label><spring:message code="label.audit_access_types"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="auditAccessTypes" value="LOG_ALLOW" type="radio" id="auditAccessTypesLogAllow"
								<c:choose>
									<c:when test="${j2eeAgent.auditAccessTypes eq 'LOG_ALLOW'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditAccessTypesLogAllow"><spring:message code="label.log_allow"/></label>
							<br>
							<input name="auditAccessTypes" value="LOG_BOTH" type="radio" id="auditAccessTypesLogBoth" 
								<c:choose>
									<c:when test="${j2eeAgent.auditAccessTypes eq 'LOG_BOTH'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditAccessTypesLogBoth"><spring:message code="label.log_both"/></label>
							<br>
							<input name="auditAccessTypes" value="LOG_DENY" type="radio" id="auditAccessTypesLogDeny" 
								<c:choose>
									<c:when test="${j2eeAgent.auditAccessTypes eq 'LOG_DENY'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditAccessTypesLogDeny"><spring:message code="label.log_deny"/></label>
							<br>
							<input name="auditAccessTypes" value="LOG_NONE" type="radio" id="auditAccessTypesLogNone" 
								<c:choose>
									<c:when test="${j2eeAgent.auditAccessTypes eq 'LOG_NONE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditAccessTypesLogNone"><spring:message code="label.log_alone"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.audit_access_types"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.audit_log_location"/>:</label>
						<div class="amFormMultipleInputs">
							<input name="auditLogLocation" value="ALL" type="radio" id="auditLogLocationAll" 
								<c:choose>
									<c:when test="${j2eeAgent.auditLogLocation eq 'ALL'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditLogLocationAll"><spring:message code="label.audit_log_location.all"/></label>
							<br>
							<input name="auditLogLocation" value="LOCAL" type="radio" id="auditLogLocationLocal" 
								<c:choose>
									<c:when test="${j2eeAgent.auditLogLocation eq 'LOCAL'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditLogLocationLocal"><spring:message code="label.audit_log_location.local"/></label>
							<br>
							<input name="auditLogLocation" value="REMOTE" type="radio" id="auditLogLocationRemote" 
								<c:choose> 
									<c:when test="${j2eeAgent.auditLogLocation eq 'REMOTE'}">checked="checked"</c:when>
									<c:otherwise></c:otherwise>
								</c:choose> >
							<label class="amInputLabel" for="auditLogLocationRemote"><spring:message code="label.audit_log_location.remote"/></label>
						</div>
						<div class="amNote">
							<spring:message code="note.audit_log_location"/>
						</div>
					</div>
					<div>
						<label for="remoteLogFilename"><spring:message code="label.remote_log_filename"/>:</label>
						<input name="remoteLogFilename" value="${j2eeAgent.remoteLogFilename}" type="text" id="remoteLogFilename">
						<div class="amNote">
							<spring:message code="note.remote_log_filename"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.rotate_local_audit_log"/>:</label>
						<input name="rotateLocalAuditLog" value="true" type="checkbox" id="rotateLocalAuditLog"
							<c:choose>
								<c:when test="${j2eeAgent.rotateLocalAuditLog eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="rotateLocalAuditLog"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.rotate_local_audit_log"/>
						</div>
					</div>
					<div>
						<label for="localAuditLogRotationSize"><spring:message code="label.local_audit_log_rotation_size"/>:</label>
						<input name="localAuditLogRotationSize" value="${j2eeAgent.localAuditLogRotationSize}" type="text" id="localAuditLogRotationSize">
						<div class="amNote">
							<spring:message code="note.local_audit_log_rotation_size"/>
						</div>
						<form:errors path="localAuditLogRotationSize" cssClass="portlet-msg-error"></form:errors>
					</div>
					<hr>
					
					<h3><spring:message code="title.fully_qualified_domain_name_checking"/></h3>
					<div>
						<label><spring:message code="label.fqdn_check"/>:</label>
						<input name="fqdnCheck" value="true" type="checkbox" id="fqdnCheck" 
							<c:choose>
								<c:when test="${j2eeAgent.fqdnCheck eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="fqdnCheck"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.fqdn_check"/>
						</div>
					</div>
					<div>
						<label for="fqdnDefault"><spring:message code="label.fqdn_default"/>:</label>
						<input name="fqdnDefault" value="${j2eeAgent.fqdnDefault}" type="text" id="fqdnDefault">
						<div class="amNote">
							<spring:message code="note.fqdn_default"/>
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.fqdn_virtual_host_map"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="fqdnVirtualHostMapDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="fvhm" items="${j2eeAgent.fqdnVirtualHostMap}">
									<input type="hidden" name="fqdnVirtualHostMap" value="${fvhm}" />
								</c:forEach>	
								<select name="fqdnVirtualHostMapDeleteValues" multiple="multiple" id="fqdnVirtualHostMapDeleteValues">
									<c:forEach var="fvhmv" items="${j2eeAgent.fqdnVirtualHostMap}">
										<option value="${fvhmv}">${fvhmv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteFqdnVirtualHostMap" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label><spring:message code="action.new_value" /></label>
								<div class="amFormCell">
									<label for="fqdnVirtualHostMapKey"><spring:message code="label.map_key" />:</label>
									<input name="fqdnVirtualHostMapKey" type="text" id="fqdnVirtualHostMapKey">
								</div>
								<div class="amFormCell">
									<label for="fqdnVirtualHostMapValue"><spring:message code="label.corresponding_map_value" />:</label>
									<input name="fqdnVirtualHostMapValue" type="text" id="fqdnVirtualHostMapValue">
								</div>
								<input type="submit" name="addFqdnVirtualHostMap" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.fqdn_virtual_host_map"/>
						</div>
					</div>
				</form:form>	
			</div>
		</div>	
	</div>

</div>