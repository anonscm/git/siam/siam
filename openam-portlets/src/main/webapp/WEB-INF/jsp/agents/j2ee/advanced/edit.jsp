<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.configuration.service.AgentsConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="agentUrl"></portlet:renderURL>
		<a href="${agentUrl}"><spring:message code="breadcrumb.agent" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.j2ee_agent_advanced" />
	</div>
	
	<div class="amFormContent">
	
		<div class="amTitleContent tableRow">
			<div class="tableCell"></div>
			<div class="tableCell">
				<h2>
					<spring:message code="label.name" />: ${name} 
					&nbsp; 
					<spring:message code="label.type" />: <%= AgentsConstants.AGENT_TYPE_J2EE%>
				</h2>
			</div>	
		</div>
		<div class="tableRow">
			<div class="amLeftMenu tableCell">
				<portlet:renderURL var="editJ2EEAgentGlobalURL">
					<portlet:param name="ctx" value="J2EEAgentGlobalController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
				
				<portlet:renderURL var="editJ2EEAgentApplicationURL">
					<portlet:param name="ctx" value="J2EEAgentApplicationController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentSSOURL">
					<portlet:param name="ctx" value="J2EEAgentSSOController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>
					
				<portlet:renderURL var="editJ2EEAgentOpenSSOServicesURL">
					<portlet:param name="ctx" value="J2EEAgentOpenSSOServicesController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentAdvancedURL">
					<portlet:param name="ctx" value="J2EEAgentAdvancedController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>	
				
				<portlet:renderURL var="editJ2EEAgentMiscellaneousURL">
					<portlet:param name="ctx" value="J2EEAgentMiscellaneousController"/>
					<portlet:param name="name" value="${name}"/>
				</portlet:renderURL>		
				
				<ul class="lfr-menu-list">
					<li><a href="${editJ2EEAgentGlobalURL}"><spring:message code="label.menu.global" /></a></li>
					<li><a href="${editJ2EEAgentApplicationURL}"><spring:message code="label.menu.application" /></a></li>
					<li><a href="${editJ2EEAgentSSOURL}"><spring:message code="label.menu.sso" /></a></li>
					<li><a href="${editJ2EEAgentOpenSSOServicesURL}"><spring:message code="label.menu.opensso_services" /></a></li>
					<li><a href="${editJ2EEAgentMiscellaneousURL}"><spring:message code="label.menu.miscellaneous" /></a></li>
					<li><a href="${editJ2EEAgentAdvancedURL}"><b><spring:message code="label.menu.advanced" /></b></a></li>
				</ul>		
			</div>
			
			<div class="amContentRight tableCell">
				<portlet:actionURL var="formAction" windowState="normal"/>
				<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="j2eeAgent">
					<input type="hidden" name="ctx" value="J2EEAgentAdvancedController" />
					<input type="hidden" name="name" value="${name}">
					
					<c:if test="${not empty message}">
						<span class="portlet-msg-success"><spring:message code="${message}"/></span>
					</c:if>
					
					<div class="amDivButtons"> 
						<input type="submit" name="save" value="<spring:message code="action.save" />">
						<input type="submit" name="reset" value="<spring:message code="action.reset" />">
						<input type="submit" name="back" value="<spring:message code="action.back" />">
					</div>
					
					<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
					
					<h3><spring:message code="title.client_identification"/></h3>
					<div>
						<label for="clientIPAddressHeader"><spring:message code="label.client_iP_address_header"/>:</label>
						<input name="clientIPAddressHeader" value="${j2eeAgent.clientIPAddressHeader}" type="text" id="clientIPAddressHeader">
						<div class="amNote">
							<spring:message code="note.client_iP_address_header"/>
						</div>
					</div>
					<div>
						<label for="clientHostnameHeader"><spring:message code="label.client_hostname_header" />:</label>
						<input name="clientHostnameHeader" value="${j2eeAgent.clientHostnameHeader}" type="text" id="clientHostnameHeader">
						<div class="amNote">
							<spring:message code="note.client_hostname_header" />
						</div>		
					</div>
					<hr>
					
					<h3><spring:message code="title.web_service_processing" /></h3>
					<div>
						<label><spring:message code="label.web_service_enable" />:</label>
						<input name="webServiceEnable" value="true" type="checkbox" id="webServiceEnable"
							<c:choose>
								<c:when test="${j2eeAgent.webServiceEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="webServiceEnable">
							<spring:message code="label.enabled" />
						</label>
						<div class="amNote">
							<spring:message code="note.web_service_enable" />
						</div>
					</div>
					<div>
						<label class="amLabelTitle"><spring:message code="label.web_service_end_points"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="webServiceEndPointsDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="wsep" items="${j2eeAgent.webServiceEndPoints}">
									<input type="hidden" name="webServiceEndPoints" value="${wsep}" />
								</c:forEach>	
								<select name="webServiceEndPointsDeleteValues" multiple="multiple" id="webServiceEndPointsDeleteValues">
									<c:forEach var="wsepv" items="${j2eeAgent.webServiceEndPoints}">
										<option value="${wsepv}">${wsepv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteWebServiceEndPoints" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="webServiceEndPointsAddValue"><spring:message code="label.new_value" /></label>
								<input name="webServiceEndPointsAddValue" type="text" id="webServiceEndPointsAddValue">
								<input type="submit" name="addWebServiceEndPoints" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.web_service_end_points"/>
						</div>
					</div>
					<div>
						<label><spring:message code="label.web_service_process_get_enable"/>:</label>
						<input name="webServiceProcessGETEnable" value="true" type="checkbox" id="webServiceProcessGETEnable" 
							<c:choose>
								<c:when test="${j2eeAgent.webServiceProcessGETEnable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="webServiceProcessGETEnable"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.web_service_process_get_enable"/>
						</div>
					</div>
					<div>
						<label for="webServiceAuthenticator"><spring:message code="label.web_service_authenticator"/>:</label>
						<input name="webServiceAuthenticator" value="${j2eeAgent.webServiceAuthenticator}" type="text" id="webServiceAuthenticator">
						<div class="amNote">
							<spring:message code="note.web_service_authenticator"/>
						</div>			
					</div>
					<div>
						<label for="webServiceResponseProcessor"><spring:message code="label.web_service_response_processor"/>:</label>
						<input name="webServiceResponseProcessor" value="${j2eeAgent.webServiceResponseProcessor}" type="text" id="webServiceResponseProcessor">
						<div class="amNote">
							<spring:message code="note.web_service_response_processor"/>
						</div>			
					</div>
					<div>
						<label for="webServiceInternalErrorContentFile"><spring:message code="label.web_service_internal_error_content_file"/>:</label>
						<input name="webServiceInternalErrorContentFile" value="${j2eeAgent.webServiceInternalErrorContentFile}" type="text" id="webServiceInternalErrorContentFile">
						<div class="amNote">
							<spring:message code="note.web_service_internal_error_content_file"/>
						</div>
					</div>
					<div>
						<label for="webServiceAuthorizationErrorContentFile"><spring:message code="label.web_service_authorization_error_content_file"/>:</label>
						<input name="webServiceAuthorizationErrorContentFile" value="${j2eeAgent.webServiceAuthorizationErrorContentFile}" type="text" id="webServiceAuthorizationErrorContentFile">
						<div class="amNote">
							<spring:message code="note.web_service_authorization_error_content_file"/>
						</div>			
					</div>
					<hr>
					
					<h3><spring:message code="title.alternate_agent_url"/></h3>
					<div>
						<label for="alternativeAgentHostName"><spring:message code="label.alternative_agent_host_name"/>:</label>
						<input name="alternativeAgentHostName" value="${j2eeAgent.alternativeAgentHostName}" type="text" id="alternativeAgentHostName">
						<div class="amNote">
							<spring:message code="note.alternative_agent_host_name"/>
						</div>			
					</div>
					<div>
						<label for="alternativeAgentPortName"><spring:message code="label.alternative_agent_port_name"/>:</label>
						<input name="alternativeAgentPortName" value="${j2eeAgent.alternativeAgentPortName}" type="text" id="alternativeAgentPortName">
						<div class="amNote">
							<spring:message code="note.alternative_agent_port_name"/>
						</div>			
					</div>
					<div>
						<label for="alternativeAgentProtocol"><spring:message code="label.alternatice_agent_prtocol"/>:</label>
						<input name="alternativeAgentProtocol" value="${j2eeAgent.alternativeAgentProtocol}" type="text" id="alternativeAgentProtocol">
						<div class="amNote">
							<spring:message code="note.alternatice_agent_prtocol"/>
						</div>			
					</div>
					<hr>
					
					<h3><spring:message code="title.jboss_application_server"/> </h3>
					<div>
						<label><spring:message code="label.web_authentication_available"/>:</label>
						<input name="webAuthenticationAvailable" value="true" type="checkbox" id="webAuthenticationAvailable" 
							<c:choose>
								<c:when test="${j2eeAgent.webAuthenticationAvailable eq 'true'}">checked="checked"</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
						<label class="amInputLabel" for="webAuthenticationAvailable"><spring:message code="label.enabled"/></label>
						<div class="amNote">
							<spring:message code="note.web_authentication_available"/>
						</div>
					</div>
					<hr>
					
					<h3><spring:message code="title.custom_properties"/></h3>
					<div>
						<label class="amLabelTitle"><spring:message code="label.custom_properties"/></label>
						<div class="amFormMultipleValues">
							<div>
								<label for="customPropertiesDeleteValues"><spring:message code="lable.current_values" /></label>
								<c:forEach var="cp" items="${j2eeAgent.customProperties}">
									<input type="hidden" name="customProperties" value="${cp}" />
								</c:forEach>	
								<select name="customPropertiesDeleteValues" multiple="multiple" id="customPropertiesDeleteValues">
									<c:forEach var="cpv" items="${j2eeAgent.customProperties}">
										<option value="${cpv}">${cpv}</option>
									</c:forEach>
									<option>________________________</option>
								</select>
								<input type="submit" name="deleteCustomProperties" value="<spring:message code="action.delete" />">
							</div>
							<div>
								<label for="customPropertiesAddValue"><spring:message code="label.new_value" /></label>
								<input name="customPropertiesAddValue" type="text" id="customPropertiesAddValue">
								<input type="submit" name="addCustomProperties" value="<spring:message code="action.add" />">
							</div>
						</div>
						<div class="amNote">
							<spring:message code="note.custom_properties"/>
						</div>
					</div>
				</form:form>
			</div>
		</div>	
	</div>
</div>