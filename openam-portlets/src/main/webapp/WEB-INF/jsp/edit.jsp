<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<portlet:actionURL var="formAction" windowState="normal"/>
<form action="${formAction}" id="${ns}pageForm" method="post">
	<input type="hidden" name="cmd" value="doSaveRealm" />

	<h3>Set realm</h3>
	
	Realm Name:
	<select name="realmName">
		<option value="">Select a realm</option>
		<c:forEach var="realm" items="${realmsList}">
			<option value="${realm}" <c:if test="${realm eq currentRealm}">selected="selected"</c:if>>${realm}</option>
		</c:forEach>
	</select>
	
	<input type="submit" value="Save">
	
</form>	