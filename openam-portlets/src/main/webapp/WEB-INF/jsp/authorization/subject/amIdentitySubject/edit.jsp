<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>
<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.cdssso_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.policy_subject" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="subject">
		<input type="hidden" name="ctx" value="AMIdentitySubjectSubjectController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="subjectName" value="${subjectName}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3> <spring:message code="title.subject_openSSO_identity_subject" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${subject.name}" id="name"> 
			<input type="hidden" name="type" value="${subject.type}"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="exclusive"><spring:message code="label.exclusive" />:</label>
			<input type="checkbox" name="exclusive" value="exclusive" id="exclusive"
				<c:choose>
					<c:when test="${subject.exclusive eq 'exclusive'}">checked="checked"</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
		</div>
		<div>
			<select name="searchValuesType">
				<option value=""><spring:message code="label.select_identity_type" /></option>
				<c:forEach var="filterType" items="${filterTypeList}">
					<option value="${filterType}">${filterType}</option>
				</c:forEach>
			</select>
			<input type="text" name="searchValuesFiled" value="*">
			<input type="submit" name="searchValues" value="<spring:message code="action.search" />"/>
		</div>
		
		<div class="amFormCell">
			<label><spring:message code="label.available" /></label>
			<br>
			<c:forEach var="vl" items="${availabelValues}">
				<input type="hidden" name="allAvailabelValuea" value="${vl.universalId}" />
			</c:forEach>
			<select multiple="multiple" name="availableValues">
				<c:forEach var="vl" items="${availabelValues}">
					<option value="${vl.universalId}">${vl.name}</option>
				</c:forEach>
				<option value="ignoreMe">________________________</option>
			</select>
		</div>
		<div class="amFormCell">
			<br>
			<input type="submit" name="addValues" value="<spring:message code="action.add" />"/><br>
			<input type="submit" name="addAllValues" value="<spring:message code="action.add_all" />"/><br>
			<input type="submit" name="removeValues" value="<spring:message code="action.remove" />"/><br>
			<input type="submit" name="removeAllvalues" value="<spring:message code="action.remove_all" />"/>
		</div>
		<div class="amFormCell">
			<label><spring:message code="label.selected" /></label>
			<br>
			<select multiple="multiple" name="selectedValues">
				<c:forEach var="value" items="${selectedValuesList}">
					<option value="${value.universalId}">${value.name}</option>
				</c:forEach>
				<option value="ignoreMe">________________________</option>
			</select>
		</div>
		
		<form:errors path="values" cssClass="portlet-msg-error"></form:errors>
	</form:form>

</div>	