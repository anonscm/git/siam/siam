<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb" >
		<spring:message code="breadcrumb.authorization" />
	</div>

	<c:if test="${not empty error}">
		<div class="portlet-msg-error"><spring:message code="${error}" /></div>
	</c:if>

	<h3><spring:message code="title.new_policy" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_delete" method="post" commandName="entity">
		<input type="hidden" name="action" value="doSavePolicy" />

		<div class="amFormCell">
			<label for="name"><spring:message code="label.policy_name" />:</label>
			 <input type="text" name="name" id="name">
		</div>
		<div class="amFormCell">
			<label for="type"><spring:message code="label.policy_type" />:</label>
			<select name="type" id="type">
				<c:forEach var="typeP" items="${policyTypesList}">
					<option value="${typeP}">${typeP}</option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" value="<spring:message code="action.create" />">
		<form:errors path="name" cssClass="portlet-msg-error"></form:errors>	
		<form:errors path="type" cssClass="portlet-msg-error"></form:errors>	
	</form:form>
	
	<%-- 
	<h3><spring:message code="title.policy_evaluation_configuration" /></h3>
	<div>
		<label><spring:message code="label.policy_combining_algorithm" />:</label>
		<select>
			<option>Permit Overrides</option>
		</select>
	</div>
	--%>
	
	<h3><spring:message code="title.configured_policies" /></h3>
	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_delete" method="post">
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllPolicy eq checkAll}">
							<input type="submit" name="checkForDelete" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllPolicy" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDelete" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllPolicy" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th><spring:message code="label.name" /></th>
				<th>
					<spring:message code="label.repository_location" />
					<div class="amDivButtons">
						<input type="submit" name="deletePolicy" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="policy" items="${policiesList}">
				<tr>
					<td class="amInputCell">
						<input name="deletePolicyList" value="${policy.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllPolicy eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>>
					</td>
					<td>${policy.type}</td>
					<td>
						<portlet:renderURL var="editPolicyURL">
							<portlet:param name="ctx" value="${policy.type}PolicyController"/>
							<portlet:param name="policyName" value="${policy.name}"/>
							<portlet:param name="type" value="${policy.type}"/>
							<portlet:param name="action" value="<%=CommonConstants.ACTION_UPDATE %>"/>
						</portlet:renderURL>
						<a href="${editPolicyURL}">${policy.name}</a>
					</td>
					<td>
						<c:forEach var="resource" items="${policy.rules}">
							${resource.resourceName}<br>
						</c:forEach>
					</td>
				</tr>
			</c:forEach>
			<c:forEach var="policyXacml" items="${xacmlPoliciesList}">
				<tr>
					<td class="amInputCell">
						<input name="deleteXACMLPolicyList" value="${policyXacml.policySetId}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllPolicy eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>>
					</td>
					<td>XACML</td>
					<td>
						<portlet:renderURL var="editXACMLPolicyURL">
							<portlet:param name="ctx" value="XACMLPolicyController"/>
							<portlet:param name="policyName" value="${policyXacml.policySetId}"/>
							<portlet:param name="action" value="<%=CommonConstants.ACTION_UPDATE %>"/>
						</portlet:renderURL>
						<a href="${editXACMLPolicyURL}">${policyXacml.policySetId}</a>
					</td>
					<td></td>
				</tr>
			</c:forEach>
			
		</table>	
		<c:if test="${not empty xacmlPoliciesListError}">
			<span class="portlet-msg-error">
				<spring:message code="error.xacml_policies_list" />
				${xacmlPoliciesListError}
			</span>
		</c:if>
	</form:form>

</div>





