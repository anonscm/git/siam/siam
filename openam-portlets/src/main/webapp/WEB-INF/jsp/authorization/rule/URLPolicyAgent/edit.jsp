<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.am.service.AuthorizationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<c:set var="type"><%=AuthorizationConstants.POLICY_TYPE_CDSSO %></c:set>

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<c:choose>
			<c:when test="${policyType eq type}">
				<a href="${policyUrl}">
					<spring:message code="breadcrumb.cdssso_policy" />
				</a>
			</c:when>
			<c:otherwise>
				<a href="${policyUrl}">
					<spring:message code="breadcrumb.referral_policy" />
				</a>
			</c:otherwise>
		</c:choose>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.policy_rule" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="rule">
		<input type="hidden" name="ctx" value="RulesiPlanetAMWebAgentServiceController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="ruleName" value="${ruleName}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	
		<h3> <spring:message code="title.rule_url_policy_agent" /></h3>
		<input type="hidden" name="type" value="${rule.type}"/>
		<form:errors path="type" cssClass="portlet-msg-error"></form:errors> 
		
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${rule.name}" id="name"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors> 	
		</div>
		<div>
			<label for="resourceName"><spring:message code="label.resource_name" />:</label>
			<input type="text" name="resourceName" value="${rule.resourceName}" id="resourceName">
			<form:errors path="resourceName" cssClass="portlet-msg-error"></form:errors> 	
		</div>
	
		<c:if test="${policyType eq type}">
			<hr>
			<h3> <spring:message code="label.actions" /> </h3>
			<div class="amNote">
				<spring:message code="note.one_or_more_actions_required" />.
			</div>
			<form:errors path="get" cssClass="portlet-msg-error"></form:errors> 	
			<table >
				<tr class="odd">
					<th></th>
					<th><spring:message code="label.action" /></th>
					<th><spring:message code="label.value" /></th>
				</tr>
				
				<tr>
					<td class="amInputCell">
						<input type="checkbox" name="get" value="true"
							<c:choose>
								<c:when test="${rule.get eq 'true'}">
								   checked="checked"
								</c:when>
							</c:choose>/>
					</td>
					<td><spring:message code="label.get" /></td>
					<td>
						<input type="radio" name= "getValue"  value="allow" id="getValueAllow"
							<c:choose>
								<c:when test="${rule.getValue ne 'deny'}">
								   checked="checked"
								</c:when>
							</c:choose> >
						<label class="amInputLabel" for="getValueAllow"><spring:message code="label.allow" /></label>
						<br>
						<input type="radio" name= "getValue" value="deny" id="getValueDeny"
							<c:choose>
								<c:when test="${rule.getValue eq 'deny'}">
								   checked="checked"
								</c:when>
							</c:choose> >
						<label class="amInputLabel" for="getValueDeny"><spring:message code="label.deny" /></label>
					</td>
				</tr>
				<tr>
					<td class="amInputCell">
						<input type="checkbox" name="post" value="true" 
							<c:choose>
								<c:when test="${rule.post eq 'true'}">
								   checked="checked"
								</c:when>
							</c:choose> />
					</td>
					<td><spring:message code="label.post" /></td>
					<td>
						<input type="radio" name= "postValue" value="allow" id="postValueAllow"
							<c:choose>
								<c:when test="${rule.postValue ne 'deny'}">
								   checked="checked"
								</c:when>
							</c:choose> >
						<label class="amInputLabel" for="postValueAllow"><spring:message code="label.allow" /></label>
						<br>
						<input type="radio" name= "postValue" value="deny" id="postValueDeny"
							<c:choose>
								<c:when test="${rule.postValue eq 'deny'}">
								   checked="checked"
								</c:when>
							</c:choose> >
						<label class="amInputLabel" for="postValueDeny"><spring:message code="label.deny" /></label>
					</td>
				</tr>
			</table>
		</c:if>	
	</form:form>

</div>