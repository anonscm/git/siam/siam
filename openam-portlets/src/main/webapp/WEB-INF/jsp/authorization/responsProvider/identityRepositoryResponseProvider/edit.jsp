<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.cdssso_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.response_provider" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="responseProvider">
		<input type="hidden" name="ctx" value="IDRepoResponseProviderController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="responseProviderName" value="${responseProviderName}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.response_provider_identity_repository_response_provider" /> </h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${responseProvider.name}" id="name">
			<input type="hidden" name="type" value="${responseProvider.type}"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		<hr>
		
		<h3><spring:message code="label.values" /></h3>
		<div>
			<label class="amLabelTitle"><spring:message code="label.staticAttribute" />:</label>
			<div class="amFormMultipleValues">
				<div>
					<label for="staticAttributeListDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="ls" items="${responseProvider.staticAttributeList}">
						<input type="hidden" name="staticAttributeList" value="${ls}" />
					</c:forEach>	
					<select name="staticAttributeListDeleteValues" multiple="multiple" id="staticAttributeListDeleteValues">
						<c:forEach var="lsv" items="${responseProvider.staticAttributeList}">
							<option value="${lsv}">${lsv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteStaticAttributeList" value="<spring:message code="action.delete" />">
				</div>
				<div>
					<label for="staticAttributeListAddValue"><spring:message code="label.new_value" /></label>
					<input type="text" name="staticAttributeListAddValue" id="staticAttributeListAddValue">
					<input type="submit" name="addStaticAttributeList" value="<spring:message code="action.add" />">
				</div>
			</div>
			<form:errors path="staticAttributeList" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label class="amLabelTitle"><spring:message code="label.dynamicAttribute" />:</label>
			<div class="amFormMultipleValues">
				<div>
					<label for="dynamicAttributeListDeleteValues"><spring:message code="lable.current_values" /></label>
					<c:forEach var="ls" items="${responseProvider.dynamicAttributeList}">
						<input type="hidden" name="dynamicAttributeList" value="${ls}" />
					</c:forEach>	
					<select name="dynamicAttributeListDeleteValues" multiple="multiple" id="dynamicAttributeListDeleteValues" >
						<c:forEach var="lsv" items="${responseProvider.dynamicAttributeList}">
							<option value="${lsv}">${lsv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
				</div>
			</div>
		</div>
	</form:form>

</div>