<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.cdssso_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="conditionUrl">
			<portlet:param name="ctx" value="SessionPropertyConditionController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
			<portlet:param name="policyType" value="${policyType}"/>
			<portlet:param name="conditionName" value="${conditionName}"/>
		</portlet:renderURL>
		<a href="${conditionUrl}">
			<spring:message code="breadcrumb.policy_condition" />
		</a>	
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.policy_condition.session_property" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="sessionProperty">
		<input type="hidden" name="ctx" value="AddSessionPropertyConditionController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="conditionName" value="${conditionName}"/>
		<input type="hidden" name="sessionPropertyName" value="${sessionPropertyName}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.edit_property" /></h3>
		<div>
			<label for="name"><spring:message code="label.property_name" />:</label>
			<input type="text" name="name" value="${sessionProperty.name}" id="name"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		
		<div>
			<label class="amLabelTitle"><spring:message code="label.values" />:</label>
			<div class="amFormMultipleValues">
				<div>
					<label for="sessionPropertyDeleteValues"><spring:message code="lable.current_values" />:</label>
					<c:forEach var="ls" items="${sessionProperty.valueList}">
						<input type="hidden" name="valueList" value="${ls}" />
					</c:forEach>	
					<select name="sessionPropertyDeleteValues" multiple="multiple" id="sessionPropertyDeleteValues">
						<c:forEach var="lsv" items="${sessionProperty.valueList}">
							<option value="${lsv}">${lsv}</option>
						</c:forEach>
						<option>________________________</option>
					</select>
					<input type="submit" name="deleteValueList" value="<spring:message code="action.delete" />">
					<form:errors path="valueList" cssClass="portlet-msg-error"></form:errors>	
				</div>
				<div>
					<label for="sessionPropertyAddValue"><spring:message code="label.new_value" />:</label>
					<input type="text" name="sessionPropertyAddValue" id="sessionPropertyAddValue">
					<input type="submit" name="addValueList" value="<spring:message code="action.add" />">
				</div>
			</div>
		</div>
	</form:form>
</div>
