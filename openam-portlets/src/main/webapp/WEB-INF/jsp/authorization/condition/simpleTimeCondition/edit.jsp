<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.cdssso_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.policy_condition" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="condition">
		<input type="hidden" name="ctx" value="SimpleTimeConditionController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="conditionName" value="${conditionName}"/>
	
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.condition_time_day_date_time_and_timezone" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${condition.name}" id="name"> 
			<div class="amNote">
				<spring:message code="note.condition_time_day_date_time_and_timezone" />
			</div>
			<input type="hidden" name="type" value="${condition.type}"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>		
		<hr>
		
		<h3><spring:message code="labe.date" /></h3>
		<div>
			<label for="startDate"><spring:message code="label.start_date" />:</label>
			<input type="text" name="startDate" value="${condition.startDate}" id="startDate">
			<div class="amNote">
				<spring:message code="note.date" />
			</div>
			<form:errors path="startDate" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="endDate"><spring:message code="label.end_date" />:</label>
			<input type="text" name="endDate" value="${condition.endDate}" id="endDate">
			<div class="amNote">
				<spring:message code="note.date" />
			</div>
			<form:errors path="endDate" cssClass="portlet-msg-error"></form:errors>
		</div>
		<hr>
		
		<h3><spring:message code="title.time" /></h3>
		<div>
			<label for="startHour"><spring:message code="label.start_time" />:</label>
			<input type="text" name="startHour" value="${condition.startHour}" id="startHour">:
			<input type="text" name="startMinute" value="${condition.startMinute}">
			<select name="startAmPm">
				<option value="am"
					<c:choose>
						<c:when test="${condition.startAmPm eq 'am'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				><spring:message code="label.start_time.am" /></option>
				<option value="pm" 
					<c:choose>
						<c:when test="${condition.startAmPm eq 'pm'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				><spring:message code="label.start_time.pm" /></option>
			</select>
			<form:errors path="startHour" cssClass="portlet-msg-error"></form:errors>
			<form:errors path="startMinute" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="endHour"><spring:message code="label.end_time" />:</label>
			<input type="text" name="endHour" value="${condition.endHour}" id="endHour">:
			<input type="text" name="endMinute" value="${condition.endMinute}">
			<select name="endAmPm">
				<option value="am" 
					<c:choose>
						<c:when test="${condition.endAmPm eq 'am'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				><spring:message code="label.start_time.am" /></option>
				<option value="pm" 
					<c:choose>
						<c:when test="${condition.endAmPm eq 'pm'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				><spring:message code="label.start_time.pm" /></option>
			</select>
			<form:errors path="endHour" cssClass="portlet-msg-error"></form:errors>
			<form:errors path="endMinute" cssClass="portlet-msg-error"></form:errors>
		</div>
		<hr>
		
		<h3><spring:message code="label.day" /></h3>
		<div>
			<label for="startDay"><spring:message code="label.start_day" />:</label>
			<select name="startDay" id="startDay">
				<option value="">  </option>
				<option value="sun"
					<c:choose>
						<c:when test="${condition.startDay eq 'sun'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.sunday" />
				</option>
				<option value="mon"
					<c:choose>
						<c:when test="${condition.startDay eq 'mon'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.monday" />
				</option>
				<option value="tue"
					<c:choose>
						<c:when test="${condition.startDay eq 'tue'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.tuesday" />
				</option>
				<option value="wed"
					<c:choose>
						<c:when test="${condition.startDay eq 'wed'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.wednesday" />
				</option>
				<option value="thu"
					<c:choose>
						<c:when test="${condition.startDay eq 'thu'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.thursday" />
				</option>
				<option value="fri"
					<c:choose>
						<c:when test="${condition.startDay eq 'fri'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.friday" />
				</option>
				<option value="sat"
					<c:choose>
						<c:when test="${condition.startDay eq 'sat'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.saturday" />
				</option>
			</select>
		</div>
		<div>
			<label for="endDay"><spring:message code="label.end_day" />:</label>
			<select name="endDay" id="endDay">
				<option value="">  </option>
				<option value="sun"
					<c:choose>
						<c:when test="${condition.endDay eq 'sun'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.sunday" />
				</option>
				<option value="mon"
					<c:choose>
						<c:when test="${condition.endDay eq 'mon'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.monday" />
				</option>
				<option value="tue"
					<c:choose>
						<c:when test="${condition.endDay eq 'tue'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.tuesday" />
				</option>
				<option value="wed"
					<c:choose>
						<c:when test="${condition.endDay eq 'wed'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.wednesday" />
				</option>
				<option value="thu"
					<c:choose>
						<c:when test="${condition.endDay eq 'thu'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.thursday" />
				</option>
				<option value="fri"
					<c:choose>
						<c:when test="${condition.endDay eq 'fri'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.friday" />
				</option>
				<option value="sat"
					<c:choose>
						<c:when test="${condition.endDay eq 'sat'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="option.day.saturday" />
				</option>
			</select>
		</div>
		<hr>
		
		<h3><spring:message code="label.timezone" /></h3>
		<div>
			<input type="radio" name="timezoneType" value="standard" checked="checked" id="timezoneTypeStandard" >
			<label for="timezoneTypeStandard"><spring:message code="label.standard" /> </label>
			<select name="timezone">
				<c:forEach var="item" items="${timezoneAvailable}">
					<option value="${item}"
						<c:choose>
							<c:when test="${item eq condition.timezone}">selected="selected"</c:when>
							<c:otherwise></c:otherwise>
						</c:choose> 
					><spring:message code="label.timezone.${item}" /></option>
				</c:forEach>
			</select>
		</div>
		<div>
			<c:choose>
				<c:when test="${customTimezine eq true }">
					<input type="radio" name="timezoneType" value="custom" id="timezoneTypeCustom" checked="checked"> 
					<label for="timezoneTypeCustom"><spring:message code="label.custom" /></label>
					<input type="text" name="customTimezoneValue" value="${condition.timezone}">
				</c:when>
				<c:otherwise>
					<input type="radio" name="timezoneType" value="custom" id="timezoneTypeCustom"> 
					<label for="timezoneTypeCustom"><spring:message code="label.custom" /></label>
					<input type="text" name="customTimezoneValue" value="">
				</c:otherwise>
			</c:choose>
			
		</div>
		<form:errors path="timezone" cssClass="portlet-msg-error"></form:errors>
	</form:form>
</div>