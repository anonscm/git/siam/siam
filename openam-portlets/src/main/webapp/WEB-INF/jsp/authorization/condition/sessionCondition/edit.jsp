<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper"> 

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.cdssso_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.policy_condition" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="condition">
		<input type="hidden" name="ctx" value="SessionConditionController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="conditionName" value="${conditionName}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.condition_active_session_time" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${condition.name}" id="name"> 
			<input type="hidden" name="type" value="${condition.type}"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="maximumSessionTime"><spring:message code="label.maximum_session_time_minutes" />:</label>
			<input type="text" name="maximumSessionTime" value="${condition.maximumSessionTime}" id="maximumSessionTime"> 
			<form:errors path="maximumSessionTime" cssClass="portlet-msg-error"></form:errors>
		</div>
		<div>
			<label for="terminateSession"><spring:message code="label.terminate_session" />:</label>
			<select name="terminateSession" id="terminateSession">
				<option value="session_condition_false_value" 
					<c:choose>
						<c:when test="${condition.terminateSession eq 'session_condition_false_value'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="label.no" />
				</option>
				<option value="session_condition_true_value" 
					<c:choose>
						<c:when test="${condition.terminateSession eq 'session_condition_true_value'}">selected="selected"</c:when>
						<c:otherwise></c:otherwise>
					</c:choose> >
					<spring:message code="label.yes" />
				</option>
			</select>
		</div>
	</form:form>	
</div>