<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.am.service.AuthorizationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<portlet:renderURL var="policyUrl">
			<portlet:param name="ctx" value="${policyType}PolicyController"/>
			<portlet:param name="policyName" value="${policyName}"/>
			<portlet:param name="action" value="${actionValue}"/>
		</portlet:renderURL>
		<a href="${policyUrl}">
			<spring:message code="breadcrumb.referral_policy" />
		</a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.referral" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm" method="post" commandName="referral">
		<input type="hidden" name="ctx" value="ReferralController" />
		<input type="hidden" name="policyName" value="${policyName}" />
		<input type="hidden" name="policyType" value="${policyType}" />
		<input type="hidden" name="referralName" value="${referralName}" />
		<input type="hidden" name="referralType" value="${referralType}" />
		<input type="hidden" name="action" value="${actionValue}"/>
		
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<h3><spring:message code="title.edit_referral" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${referral.name}" id="name"> 
			<input type="hidden" name="type" value="${referral.type}"> 
			<form:errors path="name" cssClass="portlet-msg-error"></form:errors> 
		</div>
		<div>
			<label for="filter"><spring:message code="labe.filter" />:</label>
			<input type="text" name="filter" value="*" id="filter">
			<input type="submit" name="search" value="<spring:message code="action.search" />">
		</div>
		<div>
			<label for="value"><spring:message code="label.value" />:</label>
			<select name="value" id="value"> 
				<c:forEach var="realm" items="${realmList}">
					<option value="${realm.dn}"
						<c:choose>
							<c:when test="${referral.value eq realm.dn}">selected="selected"</c:when>
							<c:otherwise></c:otherwise>
						</c:choose> 
					>${realm.name}</option>
				</c:forEach>
			</select>
			<form:errors path="value" cssClass="portlet-msg-error"></form:errors> 
		</div>
	</form:form>	

</div>