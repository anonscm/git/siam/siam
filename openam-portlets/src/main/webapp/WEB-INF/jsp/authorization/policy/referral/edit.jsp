<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.am.service.AuthorizationConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.referral_policy" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_policy" method="post" commandName="policy">
		<input type="hidden" name="ctx" value="REFERRALPolicyController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="policyName" value="${policyName}"/>
		<input type="hidden" name="policyType" value="${policy.type}"/>
	
		<c:if test="${not empty message}">
			<span class="portlet-msg-success"><spring:message code="${message}"/></span>
		</c:if>
		<c:if test="${not empty error}">
			<span class="portlet-msg-error"><spring:message code="${error}"/></span>
		</c:if>

		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	
		<h3><spring:message code="title.general" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${policy.name}" id="name">
			<input type="hidden" name="type" value="${policy.type}">
		</div>
		<div>
			<label for="description"><spring:message code="label.description" />:</label>
			<textarea name="description" id="description">${policy.description}</textarea>
		</div>
		<div>
			<label><spring:message code="label.active" />:</label>
			<input name="active" value="true" type="checkbox" id="active"
				<c:choose>
					<c:when test="${policy.active eq 'true'}">
						checked="checked"
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="active"><spring:message code="label.yes" /></label>
		</div>
		<hr>
	
		<h3><spring:message code="title.new_rule" /></h3>
		<div class="amFormCell">
			<label for="ruleName"><spring:message code="label.rule_name" />:</label>
			<input type="text" name="ruleName" id="ruleName">
		</div>
		<div class="amFormCell">
			<label for="ruleType"><spring:message code="label.rule_type" />:</label>
			<select name="ruleType" id="ruleType">
				<c:forEach var="ruleType" items="${ruleTypeList}">
					<option value="${ruleType}"><spring:message code="label.${ruleType}" /></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" name="addRules" value="<spring:message code="action.create" />">
	
		<h3><spring:message code="title.configured_rules" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllRule eq checkAll}">
							<input type="submit" name="checkForDeleteRule" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllRule" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteRule" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllRule" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteRules" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="rule" items="${policy.rules}">
				<tr>
					<td class="amInputCell">
						<input name="deleteRuleList" value="${rule.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllRule eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td><spring:message code="label.${rule.type}" /></td>
					<td>
						<portlet:renderURL var="editRuleURL">
							<portlet:param name="ctx" value="Rules${rule.type}Controller"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="ruleName" value="${rule.name}"/>
						</portlet:renderURL>
						<a href="${editRuleURL}">${rule.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<hr>
	
		<h3><spring:message code="title.new_referral" /></h3>
		<div class="amFormCell">
			<label for="referralName"><spring:message code="label.referral_name" />:</label>
			<input type="text" name="referralName" id="referralName">
		</div>
		<div class="amFormCell">
			<c:set var="subOrg" ><%= AuthorizationConstants.REFERRAL_TYPE_SUB_ORG_REFERRAL %></c:set>
			<c:set var="peerOrg"><%= AuthorizationConstants.REFERRAL_TYPE_PEER_ORG_REFERRAL %></c:set>
			<c:choose>
				<c:when test="${hasSubRealm eq 'true' and hesPeerRealm eq 'true' }">
					<label for="referralType"><spring:message code="label.referral_type" /></label>
					<select name="referralType" id="referralType">
						<option value="${peerOrg}"><spring:message code="label.${peerOrg}" /></option>
						<option value="${subOrg}"><spring:message code="label.${subOrg}" /></option>
					</select>
					<input type="submit" name="addReferral" value="<spring:message code="action.create" />">
				</c:when>
				<c:when test="${hasSubRealm eq 'true' and hesPeerRealm ne 'true' }">
					<input type="hidden" name="referralType" value="${subOrg}" />
					<input type="submit" name="addReferral" value="<spring:message code="action.create" />">
				</c:when>
				<c:when test="${hasSubRealm ne 'true' and hesPeerRealm eq 'true' }">
					<input type="hidden" name="referralType" value="${peerOrg}" />
					<input type="submit" name="addReferral" value="<spring:message code="action.create" />">
				</c:when>
				<c:otherwise>
					<input type="submit" name="addReferral" value="<spring:message code="action.create" />" disabled="disabled">
				</c:otherwise>
			</c:choose>
		</div>

		<h3><spring:message code="title.configured_rules" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllReferral eq checkAll}">
							<input type="submit" name="checkForDeleteReferral" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllReferral" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteReferral" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllReferral" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteReferral" value="<spring:message code="action.delete_selected" />" >
					</div>
				</th>
			</tr>
			<c:forEach var="referral" items="${policy.referrals}">
				<tr>
					<td class="amInputCell">
						<input name="deleteReferralList" value="${referral.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllReferral eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td><spring:message code="label.${referral.type}"/></td>
					<td>
						<portlet:renderURL var="editReferralURL">
							<portlet:param name="ctx" value="ReferralController"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="referralName" value="${referral.name}"/>
							<portlet:param name="referralType" value="${referral.type}"/>
						</portlet:renderURL>
						<a href="${editReferralURL}">${referral.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>	
</div>
