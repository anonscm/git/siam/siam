<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@page import="org.osiam.frontend.common.service.CommonConstants"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">
	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.xacml_policy" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_policy" method="post" commandName="xacmlPolicy">
		<input type="hidden" name="ctx" value="XACMLPolicyController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="policyName" value="${xacmlPolicy.policyName}"/>
		
		<c:if test="${not empty error}">
			<span class="portlet-msg-error">
				<c:out value="${error}"/> 
			</span>
		</c:if>
	
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="validate" value="<spring:message code="action.validate" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
		
		<div>
			<label>XACML Policy:</label>
			${xacmlPolicy.policyName}
		</div>
		<div>
			<textarea name="policyXml" class="amLargeTextarea" >${xacmlPolicy.policyXml}</textarea>
		</div>
	</form:form>	
</div>

