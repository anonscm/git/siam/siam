<%--

    Copyright (C) 2011 tarent Solutions <info@tarent.de>

    The contents of this file are subject to the terms
    of the Common Development and Distribution License
    (the License). You may not use this file except in
    compliance with the License.

    You can obtain a copy of the License at
    http://www.sun.com/cddl/cddl.html
    See the License for the specific language governing
    permission and limitations under the License.

    When distributing Covered Code, include this CDDL
    Header Notice in each file and include the License file
    at src/main/recources/license/license.txt.
    If applicable, add the following below the CDDL Header,
    with the fields enclosed by brackets [] replaced by
    your own identifying information:
    "Portions Copyrighted [year] [name of copyright owner]"

--%>

<%@ include file="/WEB-INF/jsp/include.jsp"%>

<div class="amFrontendCSSWrapper">

	<div class="amBreadcrumb">
		<portlet:renderURL var="authorizationUrl"></portlet:renderURL>
		<a href="${authorizationUrl}"><spring:message code="breadcrumb.authorization" /> </a>
		&nbsp;&gt;&nbsp;
		<spring:message code="breadcrumb.cdssso_policy" />
	</div>

	<portlet:actionURL var="formAction" windowState="normal"/>
	<form:form action="${formAction}" id="${ns}pageForm_policy" method="post" commandName="policy">
		<input type="hidden" name="ctx" value="CDSSOPolicyController" />
		<input type="hidden" name="action" value="${actionValue}"/>
		<input type="hidden" name="policyName" value="${policyName}"/>
		<input type="hidden" name="policyType" value="${policy.type}"/>
	
		<c:if test="${not empty message}">
			<span class="portlet-msg-success"><spring:message code="${message}"/></span>
		</c:if>
	
		<c:if test="${not empty error}">
			<span class="portlet-msg-error"><spring:message code="${error}"/></span>
		</c:if>
	
		<div class="amDivButtons"> 
			<input type="submit" name="save" value="<spring:message code="action.save" />">
			<input type="submit" name="reset" value="<spring:message code="action.reset" />">
			<input type="submit" name="back" value="<spring:message code="action.back" />">
		</div>
	
		<h3><spring:message code="title.general" /></h3>
		<div>
			<label for="name"><spring:message code="label.name" />:</label>
			<input type="text" name="name" value="${policy.name}" id="name">
			<input type="hidden" name="type" value="${policy.type}">
		</div>
		<div>
			<label for="description"><spring:message code="label.description" />:</label>
			<textarea name="description" id="description">${policy.description}</textarea>
		</div>
		<div>
			<label><spring:message code="label.active" />:</label>
			<input name="active" value="true" type="checkbox" id="active" 
				<c:choose>
					<c:when test="${policy.active eq 'true'}">
						checked="checked"
					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose> >
			<label class="amInputLabel" for="active"><spring:message code="label.yes" /></label>
		</div>
		<hr>
	
		<h3><spring:message code="title.new_rule" /></h3>
		<div class="amFormCell">
			<label for="ruleName"><spring:message code="label.rule_name" />:</label> 
			<input type="text" name="ruleName" id="ruleName">
		</div>
		<div class="amFormCell">
			<label for="ruleType"><spring:message code="label.rule_type" />:</label>
			<select name="ruleType" id="ruleType">
				<c:forEach var="ruleType" items="${ruleTypeList}">
					<option value="${ruleType}"><spring:message code="label.${ruleType}" /></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" name="addRules" value="<spring:message code="action.create" />">
	
		<h3><spring:message code="title.configured_rules" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllRule eq checkAll}">
							<input type="submit" name="checkForDeleteRule" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllRule" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteRule" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllRule" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteRules" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="rule" items="${policy.rules}">
				<tr>
					<td class="amInputCell">
						<input name="deleteRuleList" value="${rule.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllRule eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>>
					</td>
					<td><spring:message code="label.${rule.type}" /></td>
					<td>
						<portlet:renderURL var="editRuleURL">
							<portlet:param name="ctx" value="Rules${rule.type}Controller"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="ruleName" value="${rule.name}"/>
						</portlet:renderURL>
						<a href="${editRuleURL}">${rule.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<hr>
	
		<h3><spring:message code="title.new_subject" /></h3>
		<div class="amFormCell">
			<label for="subjectName"><spring:message code="label.subject_name" />:</label>
			<input type="text" name="subjectName" id="subjectName">
		</div>
		<div class="amFormCell">
			<label for="subjectType"><spring:message code="label.subject_type" /> :</label>
			<select name="subjectType" id="subjectType">
				<c:forEach var="subjectType" items="${subjectTypeList}">
					<option value="${subjectType}"><spring:message code="label.${subjectType}" /></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" name="addSubject" value="<spring:message code="action.create" />">
	
		<h3><spring:message code="title.configured_subjects" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllSubject eq checkAll}">
							<input type="submit" name="checkForDeleteSubject" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllSubject" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteSubject" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllSubject" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteSubject" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="subject" items="${policy.subjects}">
				<tr>
					<td class="amInputCell">
						<input name="deleteSubjectList" value="${subject.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllSubject eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td><spring:message code="label.${subject.type}" /></td>
					<td>
						<portlet:renderURL var="editSubjectURL">
							<portlet:param name="ctx" value="${subject.type}SubjectController"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="subjectName" value="${subject.name}"/>
						</portlet:renderURL>
						<a href="${editSubjectURL}">${subject.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<hr>
	
		<h3><spring:message code="title.new_condition" /></h3>
		<div class="amFormCell">
			<label for="conditionName" ><spring:message code="label.condition_name" />:</label>
			<input type="text" name="conditionName" id="conditionName">
		</div>
		<div class="amFormCell">
			<label for="conditionType"><spring:message code="label.condition_type" />:</label>
			<select name="conditionType" id="conditionType">
				<c:forEach var="conditionType" items="${conditionTypeList}">
					<option value="${conditionType}"><spring:message code="label.${conditionType}" /></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" name="addCondition" value="<spring:message code="action.create" />">
	
		<h3><spring:message code="title.configured_conditions" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllCondition eq checkAll}">
							<input type="submit" name="checkForDeleteCondition" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllCondition" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteCondition" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllCondition" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteCondition" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="condition" items="${policy.conditions}">
				<tr>
					<td class="amInputCell">
						<input name="deleteConditionList" value="${condition.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllCondition eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>>
					</td>
					<td><spring:message code="label.${condition.type}" /></td>
					<td>
						<portlet:renderURL var="editConditionURL">
							<portlet:param name="ctx" value="${condition.type}Controller"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="conditionName" value="${condition.name}"/>
						</portlet:renderURL>
						<a href="${editConditionURL}">${condition.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<hr>
	
		<h3><spring:message code="title.new_response_provider" /></h3>
		<div class="amFormCell">
			<label for="responseProviderName"><spring:message code="label.response_provider_name" />: </label>
			<input type="text" name="responseProviderName" id="responseProviderName">
		</div>
		<div class="amFormCell">
			<label for="responseProviderType"><spring:message code="label.response_provider_type" /> :</label>
			<select name="responseProviderType" id="responseProviderType">
				<c:forEach var="responsProviderType" items="${responsProviderTypeList}">
					<option value="${responsProviderType}"><spring:message code="label.${responsProviderType}" /></option>
				</c:forEach>	
			</select>
		</div>
		<input type="submit" name="addResponseProvider" value="<spring:message code="action.create" />">
	
		<h3><spring:message code="title.configured_respose_providers" /></h3>
		<table>
			<tr class="odd">
				<th class="amInputCell">
					<c:choose>
						<c:when test="${checkAllResponseProvider eq checkAll}">
							<input type="submit" name="checkForDeleteResponseProvider" value="<spring:message code="action.none" />">
							<input type="hidden" name="checkAllResponseProvider" value="none" />
						</c:when>
						<c:otherwise>
							<input type="submit" name="checkForDeleteResponseProvider" value="<spring:message code="action.all" />">
							<input type="hidden" name="checkAllResponseProvider" value="all" />
						</c:otherwise>
					</c:choose>
				</th>
				<th><spring:message code="label.type" /></th>
				<th>
					<spring:message code="label.name" />
					<div class="amDivButtons">
						<input type="submit" name="deleteResponseProvider" value="<spring:message code="action.delete_selected" />">
					</div>
				</th>
			</tr>
			<c:forEach var="responseP" items="${policy.responseProviders}">
				<tr>
					<td class="amInputCell">
						<input name="deleteResponseProviderList" value="${responseP.name}" type="checkbox" 
							<c:choose>
								<c:when test="${checkAllResponseProvider eq checkAll}">
									checked="checked"
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose> >
					</td>
					<td><spring:message code="label.${responseP.type}" /></td>
					<td>
						<portlet:renderURL var="editResponseProviderURL">
							<portlet:param name="ctx" value="${responseP.type}Controller"/>
							<portlet:param name="policyName" value="${policyName}"/>
							<portlet:param name="policyType" value="${policy.type}"/>
							<portlet:param name="action" value="${actionValue}"/>
							<portlet:param name="responseProviderName" value="${responseP.name}"/>
						</portlet:renderURL>
						<a href="${editResponseProviderURL}">${responseP.name}</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</form:form>
</div>
