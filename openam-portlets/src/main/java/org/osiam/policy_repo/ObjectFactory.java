/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */


package org.osiam.policy_repo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.osiam.policy_repo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrievePolicySet_QNAME = new QName("http://policy_repo.osiam.org/", "retrievePolicySet");
    private final static QName _UpdatePolicySet_QNAME = new QName("http://policy_repo.osiam.org/", "updatePolicySet");
    private final static QName _AddPolicySetResponse_QNAME = new QName("http://policy_repo.osiam.org/", "addPolicySetResponse");
    private final static QName _RemovePolicySet_QNAME = new QName("http://policy_repo.osiam.org/", "removePolicySet");
    private final static QName _RetrievePolicySetListResponse_QNAME = new QName("http://policy_repo.osiam.org/", "retrievePolicySetListResponse");
    private final static QName _RetrievePolicySetResponse_QNAME = new QName("http://policy_repo.osiam.org/", "retrievePolicySetResponse");
    private final static QName _RemovePolicySetResponse_QNAME = new QName("http://policy_repo.osiam.org/", "removePolicySetResponse");
    private final static QName _AddPolicySet_QNAME = new QName("http://policy_repo.osiam.org/", "addPolicySet");
    private final static QName _UpdatePolicySetResponse_QNAME = new QName("http://policy_repo.osiam.org/", "updatePolicySetResponse");
    private final static QName _RetrievePolicySetList_QNAME = new QName("http://policy_repo.osiam.org/", "retrievePolicySetList");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.osiam.policy_repo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrievePolicySet }
     * 
     */
    public RetrievePolicySet createRetrievePolicySet() {
        return new RetrievePolicySet();
    }

    /**
     * Create an instance of {@link AddPolicySet }
     * 
     */
    public AddPolicySet createAddPolicySet() {
        return new AddPolicySet();
    }

    /**
     * Create an instance of {@link RemovePolicySetResponse }
     * 
     */
    public RemovePolicySetResponse createRemovePolicySetResponse() {
        return new RemovePolicySetResponse();
    }

    /**
     * Create an instance of {@link AddPolicySetResponse }
     * 
     */
    public AddPolicySetResponse createAddPolicySetResponse() {
        return new AddPolicySetResponse();
    }

    /**
     * Create an instance of {@link RemovePolicySet }
     * 
     */
    public RemovePolicySet createRemovePolicySet() {
        return new RemovePolicySet();
    }

    /**
     * Create an instance of {@link RetrievePolicySetListResponse }
     * 
     */
    public RetrievePolicySetListResponse createRetrievePolicySetListResponse() {
        return new RetrievePolicySetListResponse();
    }

    /**
     * Create an instance of {@link UpdatePolicySet }
     * 
     */
    public UpdatePolicySet createUpdatePolicySet() {
        return new UpdatePolicySet();
    }

    /**
     * Create an instance of {@link RetrievePolicySetList }
     * 
     */
    public RetrievePolicySetList createRetrievePolicySetList() {
        return new RetrievePolicySetList();
    }

    /**
     * Create an instance of {@link UpdatePolicySetResponse }
     * 
     */
    public UpdatePolicySetResponse createUpdatePolicySetResponse() {
        return new UpdatePolicySetResponse();
    }

    /**
     * Create an instance of {@link RetrievePolicySetResponse }
     * 
     */
    public RetrievePolicySetResponse createRetrievePolicySetResponse() {
        return new RetrievePolicySetResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicySet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "retrievePolicySet")
    public JAXBElement<RetrievePolicySet> createRetrievePolicySet(RetrievePolicySet value) {
        return new JAXBElement<RetrievePolicySet>(_RetrievePolicySet_QNAME, RetrievePolicySet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePolicySet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "updatePolicySet")
    public JAXBElement<UpdatePolicySet> createUpdatePolicySet(UpdatePolicySet value) {
        return new JAXBElement<UpdatePolicySet>(_UpdatePolicySet_QNAME, UpdatePolicySet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPolicySetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "addPolicySetResponse")
    public JAXBElement<AddPolicySetResponse> createAddPolicySetResponse(AddPolicySetResponse value) {
        return new JAXBElement<AddPolicySetResponse>(_AddPolicySetResponse_QNAME, AddPolicySetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePolicySet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "removePolicySet")
    public JAXBElement<RemovePolicySet> createRemovePolicySet(RemovePolicySet value) {
        return new JAXBElement<RemovePolicySet>(_RemovePolicySet_QNAME, RemovePolicySet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicySetListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "retrievePolicySetListResponse")
    public JAXBElement<RetrievePolicySetListResponse> createRetrievePolicySetListResponse(RetrievePolicySetListResponse value) {
        return new JAXBElement<RetrievePolicySetListResponse>(_RetrievePolicySetListResponse_QNAME, RetrievePolicySetListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicySetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "retrievePolicySetResponse")
    public JAXBElement<RetrievePolicySetResponse> createRetrievePolicySetResponse(RetrievePolicySetResponse value) {
        return new JAXBElement<RetrievePolicySetResponse>(_RetrievePolicySetResponse_QNAME, RetrievePolicySetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemovePolicySetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "removePolicySetResponse")
    public JAXBElement<RemovePolicySetResponse> createRemovePolicySetResponse(RemovePolicySetResponse value) {
        return new JAXBElement<RemovePolicySetResponse>(_RemovePolicySetResponse_QNAME, RemovePolicySetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPolicySet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "addPolicySet")
    public JAXBElement<AddPolicySet> createAddPolicySet(AddPolicySet value) {
        return new JAXBElement<AddPolicySet>(_AddPolicySet_QNAME, AddPolicySet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdatePolicySetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "updatePolicySetResponse")
    public JAXBElement<UpdatePolicySetResponse> createUpdatePolicySetResponse(UpdatePolicySetResponse value) {
        return new JAXBElement<UpdatePolicySetResponse>(_UpdatePolicySetResponse_QNAME, UpdatePolicySetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicySetList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://policy_repo.osiam.org/", name = "retrievePolicySetList")
    public JAXBElement<RetrievePolicySetList> createRetrievePolicySetList(RetrievePolicySetList value) {
        return new JAXBElement<RetrievePolicySetList>(_RetrievePolicySetList_QNAME, RetrievePolicySetList.class, null, value);
    }

}
