/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EEAdvancedBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentAdvancedController shows the edit "J2EE Agent" (Advanced attributes)
 * form and does request processing of the edit j2ee agent action.
 * 
 */
@Controller("J2EEAgentAdvancedController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentAdvancedController" })
public class J2EEAgentAdvancedController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentAdvancedController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "J2EE Agent" - advanced attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {

			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EEAdvancedBean agent = agentsService.getAttributesForJ2EEAdvancedBean(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}
		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/j2ee/advanced/edit";

	}

	/**
	 * Save new value in Web Service End Points.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEAdvancedBean}
	 * @param result
	 *            - BindingResult
	 * @param webServiceEndPointsAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addWebServiceEndPoints")
	public void doAddWebServiceEndPoints(@ModelAttribute("j2eeAgent") @Valid J2EEAdvancedBean j2eeAgent,
			BindingResult result, String webServiceEndPointsAddValue, ActionRequest request, ActionResponse response) {

		if (webServiceEndPointsAddValue != null && !webServiceEndPointsAddValue.equals("")) {
			j2eeAgent.getWebServiceEndPoints().add(webServiceEndPointsAddValue);
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Web Service End Points.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEAdvancedBean}
	 * @param result
	 *            - BindingResult
	 * @param webServiceEndPointsDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteWebServiceEndPoints")
	public void doDeleteWebServiceEndPoints(@ModelAttribute("j2eeAgent") @Valid J2EEAdvancedBean j2eeAgent,
			BindingResult result, String[] webServiceEndPointsDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (webServiceEndPointsDeleteValues != null) {
			for (int i = 0; i < webServiceEndPointsDeleteValues.length; i++) {
				j2eeAgent.getWebServiceEndPoints().remove(webServiceEndPointsDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Custom Properties.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEAdvancedBean}
	 * @param result
	 *            - BindingResult
	 * @param customPropertiesAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCustomProperties")
	public void doAddCustomProperties(@ModelAttribute("j2eeAgent") @Valid J2EEAdvancedBean j2eeAgent,
			BindingResult result, String customPropertiesAddValue, ActionRequest request, ActionResponse response) {

		if (customPropertiesAddValue != null && !customPropertiesAddValue.equals("")) {
			j2eeAgent.getCustomProperties().add(customPropertiesAddValue);
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Custom Properties.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEAdvancedBean}
	 * @param result
	 *            - BindingResult
	 * @param customPropertiesDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCustomProperties")
	public void doDeleteCustomProperties(@ModelAttribute("j2eeAgent") @Valid J2EEAdvancedBean j2eeAgent,
			BindingResult result, String[] customPropertiesDeleteValues, ActionRequest request, ActionResponse response) {

		if (customPropertiesDeleteValues != null) {
			for (int i = 0; i < customPropertiesDeleteValues.length; i++) {
				j2eeAgent.getCustomProperties().remove(customPropertiesDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EEAdvancedBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEAdvancedBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentAdvanced(@ModelAttribute("j2eeAgent") @Valid J2EEAdvancedBean j2eeAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveJ2EEAdvancedBean(token, realm, j2eeAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link J2EEAdvancedBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetJ2EEAgentAdvanced(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}
}
