/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * Encapsulates data for a SAMLV2 SP Assertion Content bean.
 */
public class SAMLV2SPAssertionContent {
	private static final Logger LOGGER = Logger.getLogger(SAMLV2IDPAssertionContent.class);

	private boolean signingandEncryptionAuthenticationRequestsSigned;
	private boolean signingandEncryptionAuthenticationAssertionsSigned;
	private boolean signingandEncryptionPostResponseSigned;
	private boolean signingandEncryptionArtifactResponseSigned;
	private boolean signingandEncryptionLogoutRequestSigned;
	private boolean signingandEncryptionLogoutResponseSigned;
	private boolean signingandEncryptionManageNameIDRequestSigned;
	private boolean signingandEncryptionManageNameIDResponseSigned;

	private boolean signingandEncryptionEncryptionAttribute;
	private boolean signingandEncryptionAssertionAttribute;
	private boolean signingandEncryptionNameIDAttribute;

	private String signingandEncryptionCertificateAliasesSigning;
	private String signingandEncryptionCertificateAliasesEncryption;
	private Integer signingandEncryptionCertificateAliasesKeySize;
	private String signingandEncryptionCertificateAliasesAlgorithm;

	private List<String> nameIDFormatListCurrentValues = new ArrayList<String>();

	private String authenticationContextMapper;
	@NotNull
	@Size(min = 1)
	private String authenticationContextDefaultAuthenticationContext;
	private List<SAMLv2AuthContext> samlv2AuthContextList = new ArrayList<SAMLv2AuthContext>();
	private String comparisonType;

	private String assertionTimeSkew;

	private boolean basicAuthenticationEnabled;
	private String basicAuthenticationUserName;
	private String basicAuthenticationPassword;

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2SPAssertionContent fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2SPAssertionContent bean = new SAMLV2SPAssertionContent();

		Document entityConfig = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNodeEntityConfig = entityConfig.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNodeEntityConfig, FederationConstants.SPSSO_CONFIG_NODE);
		Node configNode = configNodeSet.iterator().next();
		NodeList configChildNodeList = configNode.getChildNodes();

		for (int i = 0; i < configChildNodeList.getLength(); i++) {
			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}
			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}
			if (attributeName.equals(FederationConstants.NAMEID_VALUE_MAP_CURRENT_VALUES)) {
				bean.setNameIDFormatListCurrentValues(XmlUtil.getAttributeValue(node).getValueList());
			} else if (attributeName.equals(FederationConstants.AUTHENTICATION_REQUEST_SIGNED_SP)) {
				bean.setSigningandEncryptionAuthenticationRequestsSigned(XmlUtil
						.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ASSERTION_SIGNED)) {
				bean.setSigningandEncryptionAuthenticationAssertionsSigned(XmlUtil
						.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ARTIFACT_RESPONSE_SIGNED)) {
				bean.setSigningandEncryptionArtifactResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOGOUT_REQUEST_SIGNED)) {
				bean.setSigningandEncryptionLogoutRequestSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOGOUT_RESPONSE_SIGNED)) {
				bean.setSigningandEncryptionLogoutResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SIGNING_AND_ENCRYPTION_POST_RESPONSE_SIGNED)) {
				bean.setSigningandEncryptionPostResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.MANAGE_NAME_ID_REQUEST_SIGNED)) {
				bean.setSigningandEncryptionManageNameIDRequestSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.MANAGE_NAME_ID_RESPONSE_SIGNED)) {
				bean.setSigningandEncryptionManageNameIDResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.NAMEID_ENCRYPTION_SIGNED)) {
				bean.setSigningandEncryptionNameIDAttribute(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_SIGNING)) {
				bean.setSigningandEncryptionCertificateAliasesSigning(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION)) {
				bean.setSigningandEncryptionCertificateAliasesEncryption(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_KEY_SIZE)) {
				bean.setSigningandEncryptionCertificateAliasesKeySize(XmlUtil.getIntegerValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_ALGORITHM)) {
				bean.setSigningandEncryptionCertificateAliasesAlgorithm(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.NAMEID_FORMAT_NAMEID_FORMAT_LIST)) {
				List<String> valueList = XmlUtil.getAttributeValue(node).getValueList();
				bean.setNameIDFormatListCurrentValues(valueList);
			} else if (attributeName.equals(FederationConstants.AUTHENTICATION_CONTEXT_MAPPER_SP)) {
				bean.setAuthenticationContextMapper(XmlUtil.getValueFromAttributeValue(node));
				// } else if
				// (attributeName.equals(FederationConstants.NOT_BEFORE_TIME_SKEW))
				// {
				// bean.setAssertionTimeSkew(XmlUtil.getLongValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_PASSWORD)) {
				bean.setBasicAuthenticationPassword(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_USER_NAME)) {
				bean.setBasicAuthenticationUserName(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED)) {
				bean.setBasicAuthenticationEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SIGNING_AND_ENCRYPTION_ENCRYPTION_ATTRIBUTE)) {
				bean.setSigningandEncryptionEncryptionAttribute(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SIGNING_AND_ENCRYPTION_ASSERTION_ATTRIBUTE)) {
				bean.setSigningandEncryptionAssertionAttribute(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.COMPARISON_TYPE)) {
				bean.setComparisonType(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ASSERTION_TIME_SKEW)) {
				bean.setAssertionTimeSkew(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SP_AUTHCONTEXT_CLASSREF_MAPPING)) {
				List<String> valueList = XmlUtil.getAttributeValue(node).getValueList();
				List<SAMLv2AuthContext> contextList = new ArrayList<SAMLv2AuthContext>();
				for (String s : valueList) {
					contextList.add(SAMLv2AuthContext.fromString(s));
				}

				bean.setSamlv2AuthContextList(contextList);
			}

		}

		Document metadataDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY)));
		if (metadataDocument == null) {
			return bean;
		}
		Node rootNodeMetadata = metadataDocument.getFirstChild();
		Node descriptorNodeMetadata = XmlUtil.getChildNode(rootNodeMetadata, FederationConstants.SPSSO_DESCRIPTOR_NODE);
		Set<Node> singleSignOnServiceNodeSet = XmlUtil.getChildNodes(descriptorNodeMetadata,
				FederationConstants.NAME_ID_FORMAT_NODE);
		List<String> nameIdList = new ArrayList<String>();
		for (Node node : singleSignOnServiceNodeSet) {
			nameIdList.add(XmlUtil.getTextValueFromNode(node));
		}
		bean.setNameIDFormatListCurrentValues(nameIdList);

		boolean authSigned = XmlUtil.getNodeAttributeBooleanValue(descriptorNodeMetadata,
				FederationConstants.AUTHENTICATION_REQUEST_SIGNED_SP);
		boolean assertionSigned = XmlUtil.getNodeAttributeBooleanValue(descriptorNodeMetadata,
				FederationConstants.SIGNING_AND_ENCRYPTION_AUTHENTICATION_ASSERTIONS_SIGNED);

		bean.setSigningandEncryptionAuthenticationRequestsSigned(authSigned);
		bean.setSigningandEncryptionAuthenticationAssertionsSigned(assertionSigned);

		Set<Node> keyDescriptorNodeSet = XmlUtil.getChildNodes(descriptorNodeMetadata,
				FederationConstants.KEY_DESCRIPTOR_NODE);
		for (Node keyDescriptorNode : keyDescriptorNodeSet) {

			if (XmlUtil.getNodeAttributeValue(keyDescriptorNode, FederationConstants.USE_ATTRIBUTE).equals(
					FederationConstants.ENCRYPTION)) {

				Node encyptionMethodNode = XmlUtil.getChildNode(keyDescriptorNode,
						FederationConstants.ENCRYPTION_METHOD_NODE);

				bean.setSigningandEncryptionCertificateAliasesAlgorithm(XmlUtil.getNodeAttributeValue(
						encyptionMethodNode, FederationConstants.ALGORITHM_ATTRIBUTE));

				Node keySizeNode = XmlUtil.getChildNodeWithPrefix(encyptionMethodNode,
						FederationConstants.KEY_SIZE_NODE);

				bean.setSigningandEncryptionCertificateAliasesKeySize(new Integer(XmlUtil
						.getTextValueFromNode(keySizeNode)));

			}

		}

		return bean;
	}

	/**
	 * Add all data {@link Node} to given map.
	 * 
	 * @param map
	 *            - the map to add the nodes;
	 * @param samlv2spEntityProviderService
	 *            - the parent service.
	 * @throws CertificateEncodingException
	 */
	public void addAllDatatoMap(Map<String, Document> map, SAMLV2SPEntityProviderService samlv2spEntityProviderService)
			throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Node configNode = XmlUtil.getChildNode(configDocument.getFirstChild(), FederationConstants.SPSSO_CONFIG_NODE);

		XmlUtil.createAttributeValue(configNode, FederationConstants.AUTHENTICATION_REQUEST_SIGNED_SP,
				getSigningandEncryptionAuthenticationRequestsSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.ASSERTION_SIGNED,
				getSigningandEncryptionAuthenticationAssertionsSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.ARTIFACT_RESPONSE_SIGNED,
				getSigningandEncryptionArtifactResponseSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.LOGOUT_REQUEST_SIGNED,
				getSigningandEncryptionLogoutRequestSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.LOGOUT_RESPONSE_SIGNED,
				getSigningandEncryptionLogoutResponseSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.SIGNING_AND_ENCRYPTION_ENCRYPTION_ATTRIBUTE,
				getSigningandEncryptionEncryptionAttribute());

		XmlUtil.createAttributeValue(configNode, FederationConstants.SIGNING_AND_ENCRYPTION_ASSERTION_ATTRIBUTE,
				getSigningandEncryptionAssertionAttribute());

		XmlUtil.createAttributeValue(configNode, FederationConstants.SIGNING_AND_ENCRYPTION_NAMEID_ATTRIBUTE,
				getSigningandEncryptionNameIDAttribute());

		XmlUtil.createAttributeValue(configNode, FederationConstants.SIGNING_AND_ENCRYPTION_POST_RESPONSE_SIGNED,
				getSigningandEncryptionPostResponseSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.MANAGE_NAME_ID_REQUEST_SIGNED,
				getSigningandEncryptionManageNameIDRequestSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.MANAGE_NAME_ID_RESPONSE_SIGNED,
				getSigningandEncryptionManageNameIDResponseSigned());

		XmlUtil.createAttributeValue(configNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningandEncryptionCertificateAliasesSigning());

		XmlUtil.createAttributeValue(configNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getSigningandEncryptionCertificateAliasesEncryption());

		XmlUtil.createAttributeValue(configNode, FederationConstants.AUTHENTICATION_CONTEXT_MAPPER_SP,
				getAuthenticationContextMapper());

		XmlUtil.createAttributeValue(configNode, FederationConstants.NOT_BEFORE_TIME_SKEW, getAssertionTimeSkew());

		XmlUtil.createAttributeValue(configNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED,
				getBasicAuthenticationEnabled());

		XmlUtil.createAttributeValue(configNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_USER_NAME,
				getBasicAuthenticationUserName());

		XmlUtil.createAttributeValue(configNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_PASSWORD,
				getBasicAuthenticationPassword());

		XmlUtil.createAttributeValue(configNode, FederationConstants.COMPARISON_TYPE, getComparisonType());

		XmlUtil.createAttributeValue(configNode, FederationConstants.ASSERTION_TIME_SKEW, getAssertionTimeSkew());

		List<String> authList = new ArrayList<String>();
		for (SAMLv2AuthContext context : samlv2AuthContextList) {
			if (context.getSupported()) {
				authList.add(context.toString());
			}

		}

		XmlUtil.createAttributeValue(configNode, FederationConstants.SP_AUTHCONTEXT_CLASSREF_MAPPING, authList);

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);

		Node descriptorNode = XmlUtil.getChildNode(metadataDocument.getFirstChild(),
				FederationConstants.SPSSO_DESCRIPTOR_NODE);

		((Element) descriptorNode).setAttribute("AuthnRequestsSigned",
				Boolean.toString(getSigningandEncryptionAuthenticationRequestsSigned()));
		((Element) descriptorNode).setAttribute("WantAssertionsSigned",
				Boolean.toString(getSigningandEncryptionAuthenticationAssertionsSigned()));

		// Start Key Descriptor Signing

		if (getSigningandEncryptionCertificateAliasesSigning() != null
				&& !"".equals(getSigningandEncryptionCertificateAliasesSigning())) {

			Map<String, String> signingAttributes = new HashMap<String, String>();
			signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
			Node signingNode = XmlUtil.createElement(descriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE,
					signingAttributes);

			Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(
					getSigningandEncryptionCertificateAliasesSigning());

			String base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64SigningCertificate, null);
			x509CertificateNode.setPrefix("ds");

		}
		// Start Key Descriptor Encryption

		if (getSigningandEncryptionCertificateAliasesEncryption() != null
				&& !"".equals(getSigningandEncryptionCertificateAliasesEncryption())) {

			Map<String, String> encryptionAttributes = new HashMap<String, String>();
			encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
			Node encryptionNode = XmlUtil.createElement(descriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE,
					encryptionAttributes);

			Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(
					getSigningandEncryptionCertificateAliasesEncryption());

			String base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64EncryptionCertificate, null);
			x509CertificateNode.setPrefix("ds");

			String algorithm;

			if (getSigningandEncryptionCertificateAliasesAlgorithm() != null
					&& getSigningandEncryptionCertificateAliasesAlgorithm().length() != 0) {
				algorithm = getSigningandEncryptionCertificateAliasesAlgorithm();
			} else {
				algorithm = cert.getSigAlgName();
				// algorithm = XMLCipher.AES_128;
			}
			Integer keySize;
			if (getSigningandEncryptionCertificateAliasesKeySize() != null) {
				keySize = getSigningandEncryptionCertificateAliasesKeySize();
			} else {
				keySize = new Integer(128);
			}

			Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
			encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
			Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE,
					null, encryptionMethodAttributes);

			Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
					"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
			keySizeNode.setPrefix("xenc");

		}
	}

	public boolean getSigningandEncryptionAuthenticationRequestsSigned() {
		return signingandEncryptionAuthenticationRequestsSigned;
	}

	public void setSigningandEncryptionAuthenticationRequestsSigned(
			boolean signingandEncryptionAuthenticationRequestsSigned) {
		this.signingandEncryptionAuthenticationRequestsSigned = signingandEncryptionAuthenticationRequestsSigned;
	}

	public boolean getSigningandEncryptionAuthenticationAssertionsSigned() {
		return signingandEncryptionAuthenticationAssertionsSigned;
	}

	public void setSigningandEncryptionAuthenticationAssertionsSigned(
			boolean signingandEncryptionAuthenticationAssertionsSigned) {
		this.signingandEncryptionAuthenticationAssertionsSigned = signingandEncryptionAuthenticationAssertionsSigned;
	}

	public boolean getSigningandEncryptionPostResponseSigned() {
		return signingandEncryptionPostResponseSigned;
	}

	public void setSigningandEncryptionPostResponseSigned(boolean signingandEncryptionPostResponseSigned) {
		this.signingandEncryptionPostResponseSigned = signingandEncryptionPostResponseSigned;
	}

	public boolean getSigningandEncryptionArtifactResponseSigned() {
		return signingandEncryptionArtifactResponseSigned;
	}

	public void setSigningandEncryptionArtifactResponseSigned(boolean signingandEncryptionArtifactResponseSigned) {
		this.signingandEncryptionArtifactResponseSigned = signingandEncryptionArtifactResponseSigned;
	}

	public boolean getSigningandEncryptionLogoutRequestSigned() {
		return signingandEncryptionLogoutRequestSigned;
	}

	public void setSigningandEncryptionLogoutRequestSigned(boolean signingandEncryptionLogoutRequestSigned) {
		this.signingandEncryptionLogoutRequestSigned = signingandEncryptionLogoutRequestSigned;
	}

	public boolean getSigningandEncryptionLogoutResponseSigned() {
		return signingandEncryptionLogoutResponseSigned;
	}

	public void setSigningandEncryptionLogoutResponseSigned(boolean signingandEncryptionLogoutResponseSigned) {
		this.signingandEncryptionLogoutResponseSigned = signingandEncryptionLogoutResponseSigned;
	}

	public boolean getSigningandEncryptionManageNameIDRequestSigned() {
		return signingandEncryptionManageNameIDRequestSigned;
	}

	public void setSigningandEncryptionManageNameIDRequestSigned(boolean signingandEncryptionManageNameIDRequestSigned) {
		this.signingandEncryptionManageNameIDRequestSigned = signingandEncryptionManageNameIDRequestSigned;
	}

	public boolean getSigningandEncryptionManageNameIDResponseSigned() {
		return signingandEncryptionManageNameIDResponseSigned;
	}

	public void setSigningandEncryptionManageNameIDResponseSigned(boolean signingandEncryptionManageNameIDResponseSigned) {
		this.signingandEncryptionManageNameIDResponseSigned = signingandEncryptionManageNameIDResponseSigned;
	}

	public boolean getSigningandEncryptionEncryptionAttribute() {
		return signingandEncryptionEncryptionAttribute;
	}

	public void setSigningandEncryptionEncryptionAttribute(boolean signingandEncryptionEncryptionAttribute) {
		this.signingandEncryptionEncryptionAttribute = signingandEncryptionEncryptionAttribute;
	}

	public boolean getSigningandEncryptionAssertionAttribute() {
		return signingandEncryptionAssertionAttribute;
	}

	public void setSigningandEncryptionAssertionAttribute(boolean signingandEncryptionAssertionAttribute) {
		this.signingandEncryptionAssertionAttribute = signingandEncryptionAssertionAttribute;
	}

	public boolean getSigningandEncryptionNameIDAttribute() {
		return signingandEncryptionNameIDAttribute;
	}

	public void setSigningandEncryptionNameIDAttribute(boolean signingandEncryptionNameIDAttribute) {
		this.signingandEncryptionNameIDAttribute = signingandEncryptionNameIDAttribute;
	}

	public String getSigningandEncryptionCertificateAliasesSigning() {
		return signingandEncryptionCertificateAliasesSigning;
	}

	public void setSigningandEncryptionCertificateAliasesSigning(String signingandEncryptionCertificateAliasesSigning) {
		this.signingandEncryptionCertificateAliasesSigning = signingandEncryptionCertificateAliasesSigning;
	}

	public String getSigningandEncryptionCertificateAliasesEncryption() {
		return signingandEncryptionCertificateAliasesEncryption;
	}

	public void setSigningandEncryptionCertificateAliasesEncryption(
			String signingandEncryptionCertificateAliasesEncryption) {
		this.signingandEncryptionCertificateAliasesEncryption = signingandEncryptionCertificateAliasesEncryption;
	}

	public Integer getSigningandEncryptionCertificateAliasesKeySize() {
		return signingandEncryptionCertificateAliasesKeySize;
	}

	public void setSigningandEncryptionCertificateAliasesKeySize(Integer signingandEncryptionCertificateAliasesKeySize) {
		this.signingandEncryptionCertificateAliasesKeySize = signingandEncryptionCertificateAliasesKeySize;
	}

	public String getSigningandEncryptionCertificateAliasesAlgorithm() {
		return signingandEncryptionCertificateAliasesAlgorithm;
	}

	public void setSigningandEncryptionCertificateAliasesAlgorithm(
			String signingandEncryptionCertificateAliasesAlgorithm) {
		this.signingandEncryptionCertificateAliasesAlgorithm = signingandEncryptionCertificateAliasesAlgorithm;
	}

	public List<String> getNameIDFormatListCurrentValues() {
		return nameIDFormatListCurrentValues;
	}

	public void setNameIDFormatListCurrentValues(List<String> nameIDFormatListCurrentValues) {
		this.nameIDFormatListCurrentValues = nameIDFormatListCurrentValues;
	}

	public String getAuthenticationContextMapper() {
		return authenticationContextMapper;
	}

	public void setAuthenticationContextMapper(String authenticationContextMapper) {
		this.authenticationContextMapper = authenticationContextMapper;
	}

	public String getAuthenticationContextDefaultAuthenticationContext() {
		return authenticationContextDefaultAuthenticationContext;
	}

	public void setAuthenticationContextDefaultAuthenticationContext(
			String authenticationContextDefaultAuthenticationContext) {
		this.authenticationContextDefaultAuthenticationContext = authenticationContextDefaultAuthenticationContext;
	}

	public String getComparisonType() {
		return comparisonType;
	}

	public void setComparisonType(String comparisonType) {
		this.comparisonType = comparisonType;
	}

	public String getAssertionTimeSkew() {
		return assertionTimeSkew;
	}

	public void setAssertionTimeSkew(String assertionTimeSkew) {
		this.assertionTimeSkew = assertionTimeSkew;
	}

	public boolean getBasicAuthenticationEnabled() {
		return basicAuthenticationEnabled;
	}

	public void setBasicAuthenticationEnabled(boolean basicAuthenticationEnabled) {
		this.basicAuthenticationEnabled = basicAuthenticationEnabled;
	}

	public String getBasicAuthenticationUserName() {
		return basicAuthenticationUserName;
	}

	public void setBasicAuthenticationUserName(String basicAuthenticationUserName) {
		this.basicAuthenticationUserName = basicAuthenticationUserName;
	}

	public String getBasicAuthenticationPassword() {
		return basicAuthenticationPassword;
	}

	public void setBasicAuthenticationPassword(String basicAuthenticationPassword) {
		this.basicAuthenticationPassword = basicAuthenticationPassword;
	}

	public List<SAMLv2AuthContext> getSamlv2AuthContextList() {
		return samlv2AuthContextList;
	}

	public void setSamlv2AuthContextList(List<SAMLv2AuthContext> samlv2AuthContextList) {
		this.samlv2AuthContextList = samlv2AuthContextList;
	}

}
