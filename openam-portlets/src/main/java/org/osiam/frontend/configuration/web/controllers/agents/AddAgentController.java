/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.Agent;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * AddAgentController show add {@link Agent} form and and handles requests for
 * add new {@link Agent}.
 * 
 */
@Controller("AddAgentController")
@RequestMapping(value = "view", params = { "ctx=AddAgentController" })
public class AddAgentController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AddAgentController.class);

	@Autowired
	private AgentsService agentsService;

	@Autowired
	@Qualifier("agentValidator")
	private Validator agentValidator;

	/**
	 * Show add {@link Agent} form.
	 * 
	 * @param name
	 *            - {@link Agent} name
	 * @param type
	 *            - {@link Agent} type
	 * @param errorKey
	 *            - {@link String} key to display an error
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name.
	 */
	@RenderMapping
	public String addAgent(String name, String type, String errorKey, Model model, RenderRequest request) {

		model.addAttribute("name", name);
		model.addAttribute("type", type);
		model.addAttribute("errorKey", errorKey);

		return "agents/addAgent";

	}

	/**
	 * Save a new {@link Agent}.
	 * 
	 * @param agent
	 *            - {@link Agent} to insert
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 * 
	 */
	@ActionMapping(params = "addAgent")
	public void doSave(@ModelAttribute("agent") @Valid Agent agent, BindingResult result, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		response.setRenderParameter("name", agent.getName());
		response.setRenderParameter("type", agent.getType());

		agentValidator.validate(new ObjectForValidate(realm, token, agent), result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		try {
			agentsService.addAgent(token, realm, agent);
		} catch (IllegalArgumentException e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Form invalid, error key: " + e.getMessage());
			}
			response.setRenderParameter("errorKey", e.getMessage());
			return;
		}

		response.setRenderParameter("ctx", "AgentsController");

	}

	/**
	 * Cancel add {@link Agent} action.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "cancelAgent")
	public void doCancel(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
