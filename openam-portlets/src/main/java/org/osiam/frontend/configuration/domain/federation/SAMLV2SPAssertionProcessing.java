/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * Encapsulates data for SAMLV2 SP Assertion Procession.
 * 
 */
public class SAMLV2SPAssertionProcessing {
	private static final Logger LOGGER = Logger.getLogger(SAMLV2SPAssertionProcessing.class);
	private String attributeMapper;
	private List<String> attributeMapCurrentValues = new ArrayList<String>();

	private boolean autoFederationEnabled;
	private String autoFederationAttribute;

	private String accountMapper;
	private boolean useNameIDasUserIDEnabled;

	private String artifactMessageEncoding;

	private String transientUser;

	private String localAuthenticationUrl;
	private String intermediateUrl;
	private String externalApplicationLogoutURL;

	private String defaultRelayStateURL;

	private String adapter;
	private List<String> adapterEnvironmentCurrentValues = new ArrayList<String>();

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2SPAssertionProcessing fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2SPAssertionProcessing bean = new SAMLV2SPAssertionProcessing();

		Document entityConfigDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		NodeList childList = entityConfigDocument.getChildNodes();
		if (childList.getLength() != 1) {
			throw new IllegalArgumentException(String.format("The contains contains %d node . Expected 1",
					childList.getLength()));
		}
		Node rootNode = entityConfigDocument.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.SPSSO_CONFIG_NODE);
		if (configNodeSet.size() != 1) {
			throw new IllegalArgumentException(String.format("The root node contains %d node . Expected 1",
					configNodeSet.size()));
		}

		Node configNode = configNodeSet.iterator().next();
		NodeList configChildNodeList = configNode.getChildNodes();

		for (int i = 0; i < configChildNodeList.getLength(); i++) {
			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals(FederationConstants.ATTRIBUTE_MAPPER_SP)) {
				bean.setAttributeMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ATTRIBUTE_MAP_CURRENT_VALUES)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setAttributeMapCurrentValues((valuePair.getValueList()));
			} else if (attributeName.equals(FederationConstants.AUTO_FEDERATION_ENABLED_SP)) {
				bean.setAutoFederationEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.AUTO_FEDERATION_ATTRIBUTE)) {
				bean.setAutoFederationAttribute(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SP_ACCOUNT_MAPPER)) {
				bean.setAccountMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.USE_NAME_ID_AS_USER_ID_ENABLED)) {
				bean.setUseNameIDasUserIDEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ARTIFACT_MESSAGE_ENCODING)) {
				bean.setArtifactMessageEncoding(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.TRANSIENT_USER)) {
				bean.setTransientUser(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOCAL_AUTHENTICATION_URL)) {
				bean.setLocalAuthenticationUrl(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.INTERMEDIATE_URL)) {
				bean.setIntermediateUrl(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.EXTERNAL_APPLICATION_LOGOUT_URL)) {
				bean.setExternalApplicationLogoutURL(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.DEFAULT_RELAY_STATE_URL)) {
				bean.setDefaultRelayStateURL(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ADAPTER)) {
				bean.setAdapter(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ADAPTER_ENVIRONMENT_CURRENT_VALUES)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setAdapterEnvironmentCurrentValues(valuePair.getValueList());
			}
		}

		return bean;
	}

	/**
	 * Add all data {@link Node} to given map.
	 * 
	 * @param map
	 *            - the map to add the nodes;
	 * @param samlv2spEntityProviderService
	 *            - the parent service.
	 */
	public void addAllDatatoMap(Map<String, Document> map, SAMLV2SPEntityProviderService samlv2spEntityProviderService) {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Node idpConfigNode = XmlUtil
				.getChildNode(configDocument.getFirstChild(), FederationConstants.SPSSO_CONFIG_NODE);

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ATTRIBUTE_MAPPER_SP, getAttributeMapper());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ATTRIBUTE_MAP_CURRENT_VALUES,
				getAttributeMapCurrentValues());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.AUTO_FEDERATION_ENABLED_SP,
				getAutoFederationEnabled());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.AUTO_FEDERATION_ATTRIBUTE,
				getAutoFederationAttribute());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.SP_ACCOUNT_MAPPER, getAccountMapper());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.USE_NAME_ID_AS_USER_ID_ENABLED,
				getUseNameIDasUserIDEnabled());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ARTIFACT_MESSAGE_ENCODING,
				getArtifactMessageEncoding());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.TRANSIENT_USER, getTransientUser());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.LOCAL_AUTHENTICATION_URL,
				getLocalAuthenticationUrl());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.INTERMEDIATE_URL, getIntermediateUrl());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.EXTERNAL_APPLICATION_LOGOUT_URL,
				getExternalApplicationLogoutURL());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.DEFAULT_RELAY_STATE_URL,
				getDefaultRelayStateURL());
		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ADAPTER, getAdapter());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ADAPTER_ENVIRONMENT_CURRENT_VALUES,
				getAdapterEnvironmentCurrentValues());

	}

	public String getAttributeMapper() {
		return attributeMapper;
	}

	public void setAttributeMapper(String attributeMapper) {
		this.attributeMapper = attributeMapper;
	}

	public List<String> getAttributeMapCurrentValues() {
		return attributeMapCurrentValues;
	}

	public void setAttributeMapCurrentValues(List<String> attributeMapCurrentValues) {
		this.attributeMapCurrentValues = attributeMapCurrentValues;
	}

	public boolean getAutoFederationEnabled() {
		return autoFederationEnabled;
	}

	public void setAutoFederationEnabled(boolean autoFederationEnabled) {
		this.autoFederationEnabled = autoFederationEnabled;
	}

	public String getAutoFederationAttribute() {
		return autoFederationAttribute;
	}

	public void setAutoFederationAttribute(String autoFederationAttribute) {
		this.autoFederationAttribute = autoFederationAttribute;
	}

	public String getAccountMapper() {
		return accountMapper;
	}

	public void setAccountMapper(String accountMapper) {
		this.accountMapper = accountMapper;
	}

	public boolean getUseNameIDasUserIDEnabled() {
		return useNameIDasUserIDEnabled;
	}

	public void setUseNameIDasUserIDEnabled(boolean useNameIDasUserIDEnabled) {
		this.useNameIDasUserIDEnabled = useNameIDasUserIDEnabled;
	}

	public String getArtifactMessageEncoding() {
		return artifactMessageEncoding;
	}

	public void setArtifactMessageEncoding(String artifactMessageEncoding) {
		this.artifactMessageEncoding = artifactMessageEncoding;
	}

	public String getTransientUser() {
		return transientUser;
	}

	public void setTransientUser(String transientUser) {
		this.transientUser = transientUser;
	}

	public String getLocalAuthenticationUrl() {
		return localAuthenticationUrl;
	}

	public void setLocalAuthenticationUrl(String localAuthenticationUrl) {
		this.localAuthenticationUrl = localAuthenticationUrl;
	}

	public String getIntermediateUrl() {
		return intermediateUrl;
	}

	public void setIntermediateUrl(String intermediateUrl) {
		this.intermediateUrl = intermediateUrl;
	}

	public String getExternalApplicationLogoutURL() {
		return externalApplicationLogoutURL;
	}

	public void setExternalApplicationLogoutURL(String externalApplicationLogoutURL) {
		this.externalApplicationLogoutURL = externalApplicationLogoutURL;
	}

	public String getDefaultRelayStateURL() {
		return defaultRelayStateURL;
	}

	public void setDefaultRelayStateURL(String defaultRelayStateURL) {
		this.defaultRelayStateURL = defaultRelayStateURL;
	}

	public String getAdapter() {
		return adapter;
	}

	public void setAdapter(String adapter) {
		this.adapter = adapter;
	}

	public List<String> getAdapterEnvironmentCurrentValues() {
		return adapterEnvironmentCurrentValues;
	}

	public void setAdapterEnvironmentCurrentValues(List<String> adapterEnvironmentCurrentValues) {
		this.adapterEnvironmentCurrentValues = adapterEnvironmentCurrentValues;
	}

}
