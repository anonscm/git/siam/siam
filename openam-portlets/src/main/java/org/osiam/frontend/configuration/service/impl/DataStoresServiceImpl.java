/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.common.service.impl.BaseSSOService;
import org.osiam.frontend.configuration.domain.datastores.DataStore;
import org.osiam.frontend.configuration.domain.datastores.DatabaseBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForADBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForAMDSBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForOpenDSBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForTivoliBean;
import org.osiam.frontend.configuration.exceptions.DataStoresException;
import org.osiam.frontend.configuration.service.DataStoresConstants;
import org.osiam.frontend.configuration.service.DataStoresService;
import org.springframework.stereotype.Service;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.IdConstants;
import com.sun.identity.sm.AttributeSchema;
import com.sun.identity.sm.OrganizationConfigManager;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceConfig;
import com.sun.identity.sm.ServiceConfigManager;
import com.sun.identity.sm.ServiceSchema;
import com.sun.identity.sm.ServiceSchemaManager;

/**
 * Data stores service implementation. Defines methods for data stores
 * management.
 */
@Service("DataStoresService")
public class DataStoresServiceImpl extends BaseSSOService implements DataStoresService {

	private static final Logger LOGGER = Logger.getLogger(DataStoresServiceImpl.class);

	// private static final String NAMED_CONFIGURATION_ID =
	// "NamedConfiguration";

	private void validateServiceConfig(ServiceConfig serviceConfig, String realm) throws DataStoresException {
		if (serviceConfig == null) {
			LOGGER.error(String.format("Realm '%s' is not registered", realm));
			throw new DataStoresException(String.format("Realm '%s' is not registered", realm));
		}
	}

	@Override
	public List<DataStore> getDataStoresList(SSOToken token, String realm) throws DataStoresException {

		validateTokenRealmParams(token, realm);

		ServiceConfigManager serviceConfigManager;

		List<DataStore> dataStoreList;
		try {
			serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);

			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			@SuppressWarnings("unchecked")
			Set<String> names = organizationConfig.getSubConfigNames();
			dataStoreList = new ArrayList<DataStore>(names.size());

			for (Iterator<String> iter = names.iterator(); iter.hasNext();) {
				String name = iter.next();
				ServiceConfig organizationSubconfig = organizationConfig.getSubConfig(name);
				String type = organizationSubconfig.getSchemaID();

				DataStore dataStore = new DataStore();
				dataStore.setName(name);
				dataStore.setType(type);
				dataStoreList.add(dataStore);
			}

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}

		return dataStoreList;

	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<String> getDataStoreNameSet(SSOToken token, String realm) throws DataStoresException {

		validateTokenRealmParams(token, realm);

		ServiceConfigManager serviceConfigManager;
		Set<String> names;
		try {
			serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);

			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			names = organizationConfig.getSubConfigNames();

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}

		return names;
	}

	@Override
	public LDAPv3ForADBean getDefaultAttributeValuesForLDAPv3ForAD(SSOToken token) throws DataStoresException {
		return LDAPv3ForADBean.fromMap(getDefaultAttributeValues(token, DataStoresConstants.STORE_TYPE_LDAP_FOR_AD));
	}

	@Override
	public LDAPv3ForADBean getDefaultAttributeValuesForLDAPv3ForADAM(SSOToken token) throws DataStoresException {
		return LDAPv3ForADBean.fromMap(getDefaultAttributeValues(token, DataStoresConstants.STORE_TYPE_LDAP_FOR_ADAM));
	}

	@Override
	public DatabaseBean getDefaultAttributeValuesForDatabase(SSOToken token) throws DataStoresException {
		return DatabaseBean.fromMap(getDefaultAttributeValues(token, DataStoresConstants.STORE_TYPE_DATABASE));
	}

	@Override
	public LDAPv3ForAMDSBean getDefaultAttributeValuesForLDAPv3ForAMDS(SSOToken token) throws DataStoresException {
		return LDAPv3ForAMDSBean
				.fromMap(getDefaultAttributeValues(token, DataStoresConstants.STORE_TYPE_LDAP_FOR_AMDS));
	}

	@Override
	public LDAPv3ForOpenDSBean getDefaultAttributeValuesForLDAPv3ForOpenDS(SSOToken token) throws DataStoresException {
		return LDAPv3ForOpenDSBean.fromMap(getDefaultAttributeValues(token,
				DataStoresConstants.STORE_TYPE_LDAP_FOR_OPEN_DS));
	}

	@Override
	public LDAPv3ForTivoliBean getDefaultAttributeValuesForLDAPv3ForTivoli(SSOToken token) throws DataStoresException {
		return LDAPv3ForTivoliBean.fromMap(getDefaultAttributeValues(token,
				DataStoresConstants.STORE_TYPE_LDAP_FOR_TIVOLI));
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Set<String>> getDefaultAttributeValues(SSOToken token, String type) throws DataStoresException {
		if (token == null) {
			throw new IllegalArgumentException(
					"Param realm cannot be null. Obtain first the token object using an OpenSSOService implementation.");
		}
		if (type == null) {
			throw new IllegalArgumentException("Param type cannot be null.");
		}

		Map<String, Set<String>> values = null;
		try {
			ServiceSchemaManager serviceSchemaManager = new ServiceSchemaManager(IdConstants.REPO_SERVICE, token);
			ServiceSchema organizationSchema = serviceSchemaManager.getOrganizationSchema();

			ServiceSchema serviceSchema = organizationSchema.getSubSchema(type);
			Set<AttributeSchema> attributes = serviceSchema.getAttributeSchemas();
			values = new HashMap<String, Set<String>>(attributes.size() * 2);

			for (Iterator<AttributeSchema> iter = attributes.iterator(); iter.hasNext();) {
				AttributeSchema as = iter.next();
				String i18nKey = as.getI18NKey();
				if ((i18nKey != null) && (i18nKey.length() > 0)) {
					values.put(as.getName(), as.getDefaultValues());
				}
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}

		return (values != null) ? values : Collections.EMPTY_MAP;
	}

	@Override
	public void addLDAPv3ForADDataStore(SSOToken token, String realm, LDAPv3ForADBean dataStore)
			throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_LDAP_FOR_AD, dataStore.toMap());
	}

	@Override
	public void addLDAPv3ForADAMDataStore(SSOToken token, String realm, LDAPv3ForADBean dataStore)
			throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_LDAP_FOR_ADAM, dataStore.toMap());
	}

	@Override
	public void addDatabaseDataStore(SSOToken token, String realm, DatabaseBean dataStore) throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_DATABASE, dataStore.toMap());
	}

	@Override
	public void addLDAPv3ForAMDSDataStore(SSOToken token, String realm, LDAPv3ForAMDSBean dataStore)
			throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_LDAP_FOR_AMDS, dataStore.toMap());
	}

	@Override
	public void addLDAPv3ForOpenDSDataStore(SSOToken token, String realm, LDAPv3ForOpenDSBean dataStore)
			throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_LDAP_FOR_OPEN_DS,
				dataStore.toMap());
	}

	@Override
	public void addLDAPv3ForTivoliDataStore(SSOToken token, String realm, LDAPv3ForTivoliBean dataStore)
			throws DataStoresException {
		addDataStore(token, realm, dataStore.getName(), DataStoresConstants.STORE_TYPE_LDAP_FOR_TIVOLI,
				dataStore.toMap());
	}

	@Override
	public void addDataStore(SSOToken token, String realm, String dataStoreName, String dataStoreType,
			Map<String, Set<String>> attributes) throws DataStoresException {

		validateTokenRealmParams(token, realm);

		if (dataStoreName == null) {
			throw new IllegalArgumentException("Param dataStoreName cannot be null.");
		}
		if (dataStoreType == null) {
			throw new IllegalArgumentException(
					"Param dataStoreType cannot be null. Valid values are : LDAPv3ForAD, LDAPv3ForADAM, Database, LDAPv3ForOpenDS, LDAPv3ForAMDS, LDAPv3ForTivoli");
		}
		if (attributes == null) {
			throw new IllegalArgumentException(
					"Param attributes cannot be null. Please provide a Map with String keys and Set as records");
		}

		try {
			ServiceConfigManager serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);
			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);

			if (organizationConfig == null) {
				organizationConfig = createOrganizationConfig(token, realm);
			}

			organizationConfig.addSubConfig(dataStoreName, dataStoreType, 0, attributes);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("DataStore '%s' created.", dataStoreName));
			}

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}

	}

	@Override
	public void editDataStoreLDAPv3ForAD(SSOToken token, String realm, LDAPv3ForADBean dataStore)
			throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStoreLDAPv3ForADAM(SSOToken token, String realm, LDAPv3ForADBean dataStore)
			throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStoreDatabase(SSOToken token, String realm, DatabaseBean dataStore) throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStoreLDAPv3ForAMDS(SSOToken token, String realm, LDAPv3ForAMDSBean dataStore)
			throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStoreLDAPv3ForOpenDS(SSOToken token, String realm, LDAPv3ForOpenDSBean dataStore)
			throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStoreLDAPv3ForTivoli(SSOToken token, String realm, LDAPv3ForTivoliBean dataStore)
			throws DataStoresException {
		editDataStore(token, realm, dataStore.getName(), dataStore.toMap());
	}

	@Override
	public void editDataStore(SSOToken token, String realm, String dataStoreName, Map<String, Set<String>> attributes)
			throws DataStoresException {

		validateTokenRealmParams(token, realm);

		if (dataStoreName == null) {
			throw new IllegalArgumentException("Param dataStoreName cannot be null.");
		}

		if (attributes == null) {
			throw new IllegalArgumentException(
					"Param attributes cannot be null. Please provide a Map with String keys and Set as records");
		}

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);
			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			ServiceConfig subConfig = organizationConfig.getSubConfig(dataStoreName);
			subConfig.setAttributes(attributes);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("DataStore '%s' saved with the new attributes.", dataStoreName));
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}
	}

	@Override
	public void deleteDataStore(SSOToken token, String realm, Set<String> dataStoreNameSet) throws DataStoresException {

		validateTokenRealmParams(token, realm);

		if (dataStoreNameSet == null) {
			throw new IllegalArgumentException(
					"Param dataStoreNameSet cannot be null. Please provide a Set with String records with the names of the data stores to be deleted.");
		}

		try {
			ServiceConfigManager serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);
			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			for (Iterator<String> iter = dataStoreNameSet.iterator(); iter.hasNext();) {
				String name = iter.next();
				organizationConfig.removeSubConfig(name);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(String.format("DataStore '%s' removed.", name));
				}
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}
	}

	@Override
	public LDAPv3ForADBean getDataStoreAttributesValuesForLDAPv3ForAD(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException {
		return LDAPv3ForADBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public LDAPv3ForADBean getDataStoreAttributesValuesForLDAPv3ForADAM(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException {
		return LDAPv3ForADBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public DatabaseBean getDataStoreAttributesValuesForDatabase(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException {
		return DatabaseBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public LDAPv3ForAMDSBean getDataStoreAttributesValuesForLDAPv3ForAMDS(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException {
		return LDAPv3ForAMDSBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public LDAPv3ForOpenDSBean getDataStoreAttributesValuesForLDAPv3ForOpenDS(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException {
		return LDAPv3ForOpenDSBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public LDAPv3ForTivoliBean getDataStoreAttributesValuesForLDAPv3ForTivoli(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException {
		return LDAPv3ForTivoliBean.fromMap(getDataStoreAttributes(token, realm, dataStoreName));
	}

	@Override
	public Map<String, Set<String>> getDataStoreAttributes(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException {

		validateTokenRealmParams(token, realm);

		if (dataStoreName == null) {
			throw new IllegalArgumentException("Param dataStoreName cannot be null.");
		}

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(IdConstants.REPO_SERVICE, token);
			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			ServiceConfig ss = organizationConfig.getSubConfig(dataStoreName);
			@SuppressWarnings("unchecked")
			Map<String, Set<String>> attrValues = ss.getAttributes();
			return attrValues;

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new DataStoresException(e.getMessage(), e);
		}
	}

	private ServiceConfig createOrganizationConfig(SSOToken token, String realmName) throws DataStoresException {
		try {
			OrganizationConfigManager organizationConfigManager = new OrganizationConfigManager(token, realmName);
			Map<String, Set<String>> defaultAttributeValues = getDefaultAttributeValues(token);
			return organizationConfigManager.addServiceConfig(IdConstants.REPO_SERVICE, defaultAttributeValues);
		} catch (SMSException e) {
			throw new DataStoresException(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, Set<String>> getDefaultAttributeValues(SSOToken token) throws DataStoresException {
		Map<String, Set<String>> values = null;

		try {
			ServiceSchemaManager schemaManager = new ServiceSchemaManager(IdConstants.REPO_SERVICE, token);
			ServiceSchema orgSchema = schemaManager.getOrganizationSchema();
			Set<AttributeSchema> attributes = orgSchema.getAttributeSchemas();
			values = new HashMap<String, Set<String>>(attributes.size() * 2);

			for (Iterator<AttributeSchema> iter = attributes.iterator(); iter.hasNext();) {
				AttributeSchema as = iter.next();
				values.put(as.getName(), as.getDefaultValues());
			}
		} catch (SMSException e) {
			throw new DataStoresException(e.getMessage(), e);
		} catch (SSOException e) {
			throw new DataStoresException(e.getMessage(), e);
		}

		return (values != null) ? values : Collections.EMPTY_MAP;
	}

}
