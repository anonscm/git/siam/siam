/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2IDPAssertionContent;
import org.osiam.frontend.configuration.domain.federation.SAMLV2IDPEntityProviderService;
import org.osiam.frontend.configuration.domain.federation.SAMLv2IDPAuthContext;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * IDP Assertion Content
 * 
 * Samlv2IDPController shows the edit SAMLv2 IDP Assertion Content Attributes
 * form and does request processing of the edit SAMLv2 IDP Assertion Content
 * Attributes action.
 * 
 */
@Controller("Samlv2IDPController")
@RequestMapping(value = "view", params = { "ctx=samlv2identity_providerController" })
public class Samlv2IDPController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2IDPController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("samlv2IDPAssertionContentValidator")
	private Validator samlv2IDPAssertionContentValidator;

	/**
	 * Shows edit IDP Assertion Content Attributes for current
	 * {@link EntityProvider} form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2IDPEntityProviderService idpEntityProvider = (SAMLV2IDPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("idpAssertionContent")) {
			SAMLV2IDPAssertionContent idpAssertionContent = idpEntityProvider.getAssertionContent();
			idpAssertionContent.setSamlv2AuthContextList(federationService.getIDPAuthContext(idpAssertionContent
					.getSamlv2AuthContextList()));

			idpAssertionContent.setDefaultAuthenticationContext(getNameForDefaulAuthContext(idpAssertionContent
					.getSamlv2AuthContextList()));

			model.addAttribute("idpAssertionContent", idpAssertionContent);
		}

		model.addAttribute("metaAlias", idpEntityProvider.getMetaAlias());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		model.addAttribute("keyList", federationService.getKeyType());
		model.addAttribute("authenticationContextMap", federationService.getAuthenticationContextList());

		return "federation/entityProvider/samlv2/idp/assertionContent/edit";
	}

	/**
	 * Get AuthContext name that have that have isDefaul true.
	 * 
	 * @param list
	 *            - AuthContext list
	 * @return name of the AuthContext that have isDefaul true
	 */
	private String getNameForDefaulAuthContext(List<SAMLv2IDPAuthContext> list) {

		for (Iterator<SAMLv2IDPAuthContext> iterator = list.iterator(); iterator.hasNext();) {
			SAMLv2IDPAuthContext samLv2AuthContext = iterator.next();
			if (samLv2AuthContext.isDefault()) {
				return samLv2AuthContext.getName();
			}
		}
		return "";
	}

	/**
	 * Set isDefaul true for AuthContext with the given name.
	 * 
	 * @param list
	 *            - AuthContext list
	 * @param name
	 *            - AuthContext name
	 * @return - AuthContext list
	 */
	private List<SAMLv2IDPAuthContext> setDefaulAuthContext(List<SAMLv2IDPAuthContext> list, String name) {

		for (Iterator<SAMLv2IDPAuthContext> iterator = list.iterator(); iterator.hasNext();) {
			SAMLv2IDPAuthContext samLv2AuthContext = iterator.next();
			if (name.equals(samLv2AuthContext.getName())) {
				samLv2AuthContext.setDefault(true);
				if (!samLv2AuthContext.getSupported()) {
					samLv2AuthContext.setSupported(true);
				}
				break;
			}
		}
		return list;
	}

	/**
	 * Save values for Name ID Value Map Current Values.
	 * 
	 * @param idpAssertionContent
	 *            - {@link SAMLV2IDPAssertionContent}
	 * @param result
	 *            - BindingResult
	 * @param nameIDValueMapCurrentValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNameIDValueMapCurrentValues")
	public void doAddNameIDValueMapCurrentValues(
			@ModelAttribute("idpAssertionContent") @Valid SAMLV2IDPAssertionContent idpAssertionContent,
			BindingResult result, String nameIDValueMapCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDValueMapCurrentValuesAddValue != null && !nameIDValueMapCurrentValuesAddValue.equals("")) {
			idpAssertionContent.getNameIDValueMapCurrentValues().add(nameIDValueMapCurrentValuesAddValue);
		}

		idpAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(
				idpAssertionContent.getSamlv2AuthContextList(), idpAssertionContent.getDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value for Name ID Value Map Current Values.
	 * 
	 * @param idpAssertionContent
	 *            - {@link SAMLV2IDPAssertionContent}
	 * @param result
	 *            - BindingResult
	 * @param nameIDValueMapCurrentValuesDeleteValues
	 *            - value to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNameIDValueMapCurrentValues")
	public void doDeleteNameIDValueMapCurrentValues(
			@ModelAttribute("idpAssertionContent") @Valid SAMLV2IDPAssertionContent idpAssertionContent,
			BindingResult result, String[] nameIDValueMapCurrentValuesDeleteValues, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDValueMapCurrentValuesDeleteValues != null) {
			for (int i = 0; i < nameIDValueMapCurrentValuesDeleteValues.length; i++) {
				idpAssertionContent.getNameIDValueMapCurrentValues().remove(nameIDValueMapCurrentValuesDeleteValues[i]);
			}
		}

		idpAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(
				idpAssertionContent.getSamlv2AuthContextList(), idpAssertionContent.getDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value for Name I DValue Map Format Values.
	 * 
	 * @param idpAssertionContent
	 *            - {@link SAMLV2IDPAssertionContent}
	 * @param result
	 *            - BindingResult
	 * @param nameIDValueMapFormatValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNameIDValueMapFormatValues")
	public void doAddNameIDValueMapFormatValues(
			@ModelAttribute("idpAssertionContent") @Valid SAMLV2IDPAssertionContent idpAssertionContent,
			BindingResult result, String nameIDValueMapFormatValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDValueMapFormatValuesAddValue != null && !nameIDValueMapFormatValuesAddValue.equals("")) {
			idpAssertionContent.getNameIDValueMapFormatValues().add(nameIDValueMapFormatValuesAddValue);
		}

		idpAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(
				idpAssertionContent.getSamlv2AuthContextList(), idpAssertionContent.getDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete values for Name ID Value Map Format Values.
	 * 
	 * @param idpAssertionContent
	 *            - {@link SAMLV2IDPAssertionContent}
	 * @param result
	 *            - BindingResult
	 * @param nameIDValueMapFormatValuesDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNameIDValueMapFormatValues")
	public void doDeleteNameIDValueMapFormatValues(
			@ModelAttribute("idpAssertionContent") @Valid SAMLV2IDPAssertionContent idpAssertionContent,
			BindingResult result, String[] nameIDValueMapFormatValuesDeleteValues, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDValueMapFormatValuesDeleteValues != null) {
			for (int i = 0; i < nameIDValueMapFormatValuesDeleteValues.length; i++) {
				idpAssertionContent.getNameIDValueMapFormatValues().remove(nameIDValueMapFormatValuesDeleteValues[i]);
			}
		}

		idpAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(
				idpAssertionContent.getSamlv2AuthContextList(), idpAssertionContent.getDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save IDP Assertion Content Attributes for current {@link EntityProvider}.
	 * 
	 * @param idpAssertionContent
	 *            - {@link SAMLV2IDPAssertionContent}
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("idpAssertionContent") @Valid SAMLV2IDPAssertionContent idpAssertionContent,
			BindingResult result, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		samlv2IDPAssertionContentValidator.validate(new ObjectForValidate(realm, token, idpAssertionContent), result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		idpAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(
				idpAssertionContent.getSamlv2AuthContextList(), idpAssertionContent.getDefaultAuthenticationContext()));

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2IDPEntityProviderService idpEntityProvider = (SAMLV2IDPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		idpEntityProvider.setSigningCertificateAlias(idpAssertionContent.getCertificateAliasesSigning());
		idpEntityProvider.setEncryptionCertificateAlias(idpAssertionContent.getCertificateAliasesEncryption());
		
		idpEntityProvider.setAssertionContent(idpAssertionContent);

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, idpEntityProvider);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset IDP Assertion Content Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
