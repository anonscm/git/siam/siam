/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.datastores;

import org.osiam.frontend.configuration.domain.datastores.LDAPv3DataStore;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the DatabaseBean object.
 * <p>
 * Validate if the LDAP Bind Password entered do not match.<br>
 * </p>
 * 
 */
@Component("ldapv3DataStoreValidator")
public class LDAPv3DataStoreValidator implements Validator {

	@Override
	public boolean supports(Class<?> klass) {
		return LDAPv3DataStore.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LDAPv3DataStore object = (LDAPv3DataStore) target;

		if (object.getLdapBindPassword() != null
				&& !object.getLdapBindPassword().equals(object.getLdapBindPasswordConfirm())) {
			errors.rejectValue("ldapBindPasswordConfirm", "PassNotMath");
		}
	}
}
