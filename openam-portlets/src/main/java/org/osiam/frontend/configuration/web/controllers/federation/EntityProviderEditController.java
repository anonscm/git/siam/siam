/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * EntityProviderEditController shows the edit EntityProvider form and does
 * request processing of the edit EntityProvider action.
 * 
 */
@Controller("EntityProviderEditController")
@RequestMapping(value = "view", params = { "ctx=EntityProviderEditController" })
public class EntityProviderEditController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EntityProviderEditController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows list of {@link EntityProvider} services.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} name
	 * @param protocol
	 *            - {@link EntityProvider} protocol
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the views' name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String protocol, Model model, RenderRequest request) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("editEntityProvider, entityIdentifier: " + entityIdentifier);
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				protocol, entityIdentifier);

		model.addAttribute("entityProvider", entityProvider);

		return "federation/entityProvider/edit";
	}

	/**
	 * Back to list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "FederationController");
	}

}
