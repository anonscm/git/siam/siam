/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.WSFEDSPAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WsfedSPController shows the edit Wsfed SP Attributes form and does request
 * processing of the edit Wsfed SP Attributes action.
 * 
 */
@Controller("WsfedSPController")
@RequestMapping(value = "view", params = { "ctx=wsfedservice_providerController" })
public class WsfedSPController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WsfedSPController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows edit Wsfed SP Attributes for current EntityProvider form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "sp")) {

			EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
					FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, entityIdentifier);

			WSFEDSPAttributesBean sp = (WSFEDSPAttributesBean) entityProvider.getDescriptorMap().get(serviceType);

			model.addAttribute("sp", sp);
		}

		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/wsfed/sp/edit";
	}

	/**
	 * Add value for AttributeMapCurrentValues List.
	 * 
	 * @param sp
	 *            - {@link WSFEDSPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeMapCurrentValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAttributeMapCurrentValues")
	public void doAddAttributeMapCurrentValues(@ModelAttribute("sp") @Valid WSFEDSPAttributesBean sp,
			BindingResult result, String attributeMapCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesAddValue != null && !attributeMapCurrentValuesAddValue.equals("")) {
			sp.getAttributeMapCurrentValues().add(attributeMapCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete values form attributeMapCurrentValues list.
	 * 
	 * @param sp
	 *            - {@link WSFEDSPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeMapCurrentValuesDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAttributeMapCurrentValues")
	public void doDeleteAttributeMapCurrentValues(@ModelAttribute("sp") @Valid WSFEDSPAttributesBean sp,
			BindingResult result, String[] attributeMapCurrentValuesDeleteValues, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesDeleteValues != null) {
			for (int i = 0; i < attributeMapCurrentValuesDeleteValues.length; i++) {
				sp.getAttributeMapCurrentValues().remove(attributeMapCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save Wsfed SP Attributes for current EntityProvider.
	 * 
	 * @param sp
	 *            - {@link WSFEDSPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("sp") @Valid WSFEDSPAttributesBean sp, BindingResult result,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, entityIdentifier, sp);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");
	}

	/**
	 * Reset Wsfed SP Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link WSFEDSPAttributesBean}
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
