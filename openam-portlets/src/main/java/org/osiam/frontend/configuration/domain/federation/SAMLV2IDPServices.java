/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Encapsulates all data for the SAMLV2 IDP Services feature.
 * 
 */
public final class SAMLV2IDPServices {
	/**
	 * service attribute SOAP.
	 */
	public static final String SOAP = "SOAP";

	/**
	 * service attribute HTTP_POST.
	 */
	public static final String HTTP_POST = "HTTP-POST";

	/**
	 * service attribute HTTP_REDIRECT.
	 */
	public static final String HTTP_REDIRECT = "HTTP-Redirect";

	private boolean idpServiceAttributesArtifactResolutionServiceDefault;
	private String idpServiceAttributesArtifactResolutionServiceLocation;
	private Long idpServiceAttributesArtifactResolutionServiceIndex;

	private String idpServiceAttributesSingleLogoutServiceDefault;

	private String idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation;
	private String idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation;

	private String idpServiceAttributesSingleLogoutServicePOSTLocation;
	private String idpServiceAttributesSingleLogoutServicePOSTResponseLocation;

	private String idpServiceAttributesSingleLogoutServiceSOAPlocation;

	private String idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault;

	private String idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation;
	private String idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation;

	private String idpServiceAttributesManageNameIDServicePOSTLocation;
	private String idpServiceAttributesManageNameIDServicePOSTResponseLocation;

	private String idpServiceAttributesManageNameIDServiceSOAPLocation;

	private String idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation;
	private String idpServiceAttributesSingleSignOnServicePOSTLocation;
	private String idpServiceAttributesSingleSignOnServiceSOAPLocation;

	private String nameIDMapping;

	public String getNameIDMapping() {
		return nameIDMapping;
	}

	public void setNameIDMapping(String nameIDMapping) {
		this.nameIDMapping = nameIDMapping;
	}

	public boolean getIdpServiceAttributesArtifactResolutionServiceDefault() {
		return idpServiceAttributesArtifactResolutionServiceDefault;
	}

	public void setIdpServiceAttributesArtifactResolutionServiceDefault(
			boolean idpServiceAttributesArtifactResolutionServiceDefault) {
		this.idpServiceAttributesArtifactResolutionServiceDefault = idpServiceAttributesArtifactResolutionServiceDefault;
	}

	public String getIdpServiceAttributesArtifactResolutionServiceLocation() {
		return idpServiceAttributesArtifactResolutionServiceLocation;
	}

	public void setIdpServiceAttributesArtifactResolutionServiceLocation(
			String idpServiceAttributesArtifactResolutionServiceLocation) {
		this.idpServiceAttributesArtifactResolutionServiceLocation = idpServiceAttributesArtifactResolutionServiceLocation;
	}

	public Long getIdpServiceAttributesArtifactResolutionServiceIndex() {
		return idpServiceAttributesArtifactResolutionServiceIndex;
	}

	public void setIdpServiceAttributesArtifactResolutionServiceIndex(
			Long idpServiceAttributesArtifactResolutionServiceIndex) {
		this.idpServiceAttributesArtifactResolutionServiceIndex = idpServiceAttributesArtifactResolutionServiceIndex;
	}

	public String getIdpServiceAttributesSingleLogoutServiceDefault() {
		return idpServiceAttributesSingleLogoutServiceDefault;
	}

	public void setIdpServiceAttributesSingleLogoutServiceDefault(String idpServiceAttributesSingleLogoutServiceDefault) {
		this.idpServiceAttributesSingleLogoutServiceDefault = idpServiceAttributesSingleLogoutServiceDefault;
	}

	public String getIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation() {
		return idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation;
	}

	public void setIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation(
			String idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation) {
		this.idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation = idpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation;
	}

	public String getIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation() {
		return idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation;
	}

	public void setIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation(
			String idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation) {
		this.idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation = idpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation;
	}

	public String getIdpServiceAttributesSingleLogoutServicePOSTLocation() {
		return idpServiceAttributesSingleLogoutServicePOSTLocation;
	}

	public void setIdpServiceAttributesSingleLogoutServicePOSTLocation(
			String idpServiceAttributesSingleLogoutServicePOSTLocation) {
		this.idpServiceAttributesSingleLogoutServicePOSTLocation = idpServiceAttributesSingleLogoutServicePOSTLocation;
	}

	public String getIdpServiceAttributesSingleLogoutServicePOSTResponseLocation() {
		return idpServiceAttributesSingleLogoutServicePOSTResponseLocation;
	}

	public void setIdpServiceAttributesSingleLogoutServicePOSTResponseLocation(
			String idpServiceAttributesSingleLogoutServicePOSTResponseLocation) {
		this.idpServiceAttributesSingleLogoutServicePOSTResponseLocation = idpServiceAttributesSingleLogoutServicePOSTResponseLocation;
	}

	public String getIdpServiceAttributesSingleLogoutServiceSOAPlocation() {
		return idpServiceAttributesSingleLogoutServiceSOAPlocation;
	}

	public void setIdpServiceAttributesSingleLogoutServiceSOAPlocation(
			String idpServiceAttributesSingleLogoutServiceSOAPlocation) {
		this.idpServiceAttributesSingleLogoutServiceSOAPlocation = idpServiceAttributesSingleLogoutServiceSOAPlocation;
	}

	public String getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault() {
		return idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault;
	}

	public void setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault(
			String idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault) {
		this.idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault = idpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault;
	}

	public String getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation() {
		return idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation;
	}

	public void setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation(
			String idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation) {
		this.idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation = idpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation;
	}

	public String getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation() {
		return idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation;
	}

	public void setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation(
			String idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation) {
		this.idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation = idpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation;
	}

	public String getIdpServiceAttributesManageNameIDServicePOSTLocation() {
		return idpServiceAttributesManageNameIDServicePOSTLocation;
	}

	public void setIdpServiceAttributesManageNameIDServicePOSTLocation(
			String idpServiceAttributesManageNameIDServicePOSTLocation) {
		this.idpServiceAttributesManageNameIDServicePOSTLocation = idpServiceAttributesManageNameIDServicePOSTLocation;
	}

	public String getIdpServiceAttributesManageNameIDServicePOSTResponseLocation() {
		return idpServiceAttributesManageNameIDServicePOSTResponseLocation;
	}

	public void setIdpServiceAttributesManageNameIDServicePOSTResponseLocation(
			String idpServiceAttributesManageNameIDServicePOSTResponseLocation) {
		this.idpServiceAttributesManageNameIDServicePOSTResponseLocation = idpServiceAttributesManageNameIDServicePOSTResponseLocation;
	}

	public String getIdpServiceAttributesManageNameIDServiceSOAPLocation() {
		return idpServiceAttributesManageNameIDServiceSOAPLocation;
	}

	public void setIdpServiceAttributesManageNameIDServiceSOAPLocation(
			String idpServiceAttributesManageNameIDServiceSOAPLocation) {
		this.idpServiceAttributesManageNameIDServiceSOAPLocation = idpServiceAttributesManageNameIDServiceSOAPLocation;
	}

	public String getIdpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation() {
		return idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation;
	}

	public void setIdpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation(
			String idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation) {
		this.idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation = idpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation;
	}

	public String getIdpServiceAttributesSingleSignOnServicePOSTLocation() {
		return idpServiceAttributesSingleSignOnServicePOSTLocation;
	}

	public void setIdpServiceAttributesSingleSignOnServicePOSTLocation(
			String idpServiceAttributesSingleSignOnServicePOSTLocation) {
		this.idpServiceAttributesSingleSignOnServicePOSTLocation = idpServiceAttributesSingleSignOnServicePOSTLocation;
	}

	public String getIdpServiceAttributesSingleSignOnServiceSOAPLocation() {
		return idpServiceAttributesSingleSignOnServiceSOAPLocation;
	}

	public void setIdpServiceAttributesSingleSignOnServiceSOAPLocation(
			String idpServiceAttributesSingleSignOnServiceSOAPLocation) {
		this.idpServiceAttributesSingleSignOnServiceSOAPLocation = idpServiceAttributesSingleSignOnServiceSOAPLocation;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2IDPServices fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2IDPServices bean = new SAMLV2IDPServices();
		String xmlDocString = stringFromSet(map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));
		Document metadataDocument = XmlUtil.toDOMDocument(xmlDocString);
		if (metadataDocument == null) {
			return bean;
		}
		NodeList childList = metadataDocument.getChildNodes();
		if (childList.getLength() != 1) {
			throw new IllegalArgumentException(String.format("The contains contains %d node . Expected 1",
					childList.getLength()));
		}

		Node rootNode = metadataDocument.getFirstChild();
		Node descriptorNode = XmlUtil.getChildNode(rootNode, FederationConstants.IDPSSO_DESCRIPTOR_NODE);

		Set<Node> artifactNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.ARTIFACT_RESOLUTION_SERVICE_NODE);

		if (artifactNodeSet.size() != 0) {
			Node artifactNode = artifactNodeSet.iterator().next();
			bean.setIdpServiceAttributesArtifactResolutionServiceLocation(XmlUtil.getNodeAttributeValue(artifactNode,
					FederationConstants.LOCATION_ATTRIBUTE));
			bean.setIdpServiceAttributesArtifactResolutionServiceDefault(XmlUtil.getNodeAttributeBooleanValue(
					artifactNode, FederationConstants.IS_DEFAULT_ATTRIBUTE));
			bean.setIdpServiceAttributesArtifactResolutionServiceIndex(XmlUtil.getNodeAttributeLongValue(artifactNode,
					FederationConstants.INDEX_ATTRIBUTE));
		}
		
		int firstIndex = xmlDocString.indexOf("<IDPSSODescriptor");

		Set<Node> singleLogoutServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.SINGLE_LOGOUT_SERVICE_NODE);
		if (singleLogoutServiceNodeSet.size() != 0) {

			for (Node node : singleLogoutServiceNodeSet) {

				if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect")) {
					bean.setIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation(XmlUtil.getNodeAttributeValue(
							node, FederationConstants.LOCATION_ATTRIBUTE));
					bean.setIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation(XmlUtil
							.getNodeAttributeValue(node, FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
					bean.setIdpServiceAttributesSingleLogoutServicePOSTLocation(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.LOCATION_ATTRIBUTE));
					bean.setIdpServiceAttributesSingleLogoutServicePOSTResponseLocation(XmlUtil.getNodeAttributeValue(
							node, FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
					bean.setIdpServiceAttributesSingleLogoutServiceSOAPlocation(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.LOCATION_ATTRIBUTE));
				}
			}
			int httpRedirectIndex = xmlDocString
					.indexOf("<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect", firstIndex);
			int httpPostIndex = xmlDocString
					.indexOf("<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", firstIndex);
			int soapIndex = xmlDocString
					.indexOf("<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", firstIndex);
			if (httpRedirectIndex < httpPostIndex) {
				if (httpRedirectIndex < soapIndex) {
					bean.setIdpServiceAttributesSingleLogoutServiceDefault(HTTP_REDIRECT);
				} else {
					bean.setIdpServiceAttributesSingleLogoutServiceDefault(SOAP);
				}
			} else {
				if (httpPostIndex < soapIndex) {
					bean.setIdpServiceAttributesSingleLogoutServiceDefault(HTTP_POST);
				} else {
					bean.setIdpServiceAttributesSingleLogoutServiceDefault(SOAP);
				}
			}
		}

		Set<Node> manageNameIdServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.MANAGE_NAME_ID_SERVICE_NODE);

		for (Node node : manageNameIdServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect")) {
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation(XmlUtil
						.getNodeAttributeValue(node, FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
				bean.setIdpServiceAttributesManageNameIDServicePOSTLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setIdpServiceAttributesManageNameIDServicePOSTResponseLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setIdpServiceAttributesManageNameIDServiceSOAPLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
			}
		}

		int httpRedirectIndex = xmlDocString
				.indexOf("<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect", firstIndex);
		int httpPostIndex = xmlDocString
				.indexOf("<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", firstIndex);
		int soapIndex = xmlDocString
				.indexOf("<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", firstIndex);
		if (httpRedirectIndex < httpPostIndex) {
			if (httpRedirectIndex < soapIndex) {
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault(HTTP_REDIRECT);
			} else {
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault(SOAP);
			}
		} else {
			if (httpPostIndex < soapIndex) {
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault(HTTP_POST);
			} else {
				bean.setIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault(SOAP);
			}
		}

		Set<Node> singleSignOnServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.SINGLE_SIGN_ON_SERVICE_NODE);

		for (Node node : singleSignOnServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect")) {
				bean.setIdpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
				bean.setIdpServiceAttributesSingleSignOnServicePOSTLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setIdpServiceAttributesSingleSignOnServiceSOAPLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
			}
		}

		Set<Node> nameIDMappingNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.NAME_ID_MAPPING_SERVICE_NODE);
		for (Node node : nameIDMappingNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setNameIDMapping(XmlUtil.getNodeAttributeValue(node, FederationConstants.LOCATION_ATTRIBUTE));
			}
		}
		return bean;
	}




	/**
	 * 
	 * @return default instance, fields to default values.
	 */
	public static SAMLV2IDPServices createDefaultInstance() {
		SAMLV2IDPServices bean = new SAMLV2IDPServices();

		return bean;
	}

	/**
	 * SAMLV2IDPServices constructor.
	 */
	private SAMLV2IDPServices() {

	}

}
