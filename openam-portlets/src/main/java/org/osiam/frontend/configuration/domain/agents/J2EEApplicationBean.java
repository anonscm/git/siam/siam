/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.asSetWithSpecialEmptyList;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * 
 * This class encapsulates data for a j2ee application bean.
 * 
 */
public class J2EEApplicationBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private List<String> loginFromUri = new ArrayList<String>();
	private List<String> loginErrorUri = new ArrayList<String>();
	private boolean useInternalLogin;
	private String loginContentFileName;
	private List<String> applicationLogoutHandler = new ArrayList<String>();
	private List<String> applicationLogoutUri = new ArrayList<String>();
	private List<String> logoutReguestParameter = new ArrayList<String>();
	private boolean logoutIntrospectEnabled;
	private List<String> logoutEntryUri = new ArrayList<String>();
	private List<String> responseAccessDeniedUri = new ArrayList<String>();
	private List<String> notEnforcedUris = new ArrayList<String>();
	private boolean invertNotEnforcesUris;
	private boolean notEnforcedUrisCacheEnabled;
	@NotNull
	@Min(0)
	private Integer notEnforcedUrisCacheSize;
	private boolean refreshSessionIdleTime;
	private List<String> notEnforcedClientIpList = new ArrayList<String>();
	private boolean notEnforcedIpInvertList;
	private boolean notEnforcedIpCacheFlag;
	@NotNull
	@Min(0)
	private Integer notEnforcedIpCacheSize;
	private String profileAttributeFetchMode;
	private List<String> profileAttributeMapping = new ArrayList<String>();
	private String responseAttributeFetchModel;
	private List<String> responseAttributeMapping = new ArrayList<String>();
	private String cookieSeparatorCharacter;
	private String fetchAttributeDateFormat;
	private boolean attributeCookieEncode;
	private String sessionAttributeFetchMode;
	private List<String> sessionAttributeMapping = new ArrayList<String>();
	private List<String> defaultPrivilegedAttribute = new ArrayList<String>();
	private List<String> privilegedAttributeType = new ArrayList<String>();
	private List<String> privilegedAttributesToLowerCase = new ArrayList<String>();
	private List<String> privilegedSessionAttribute = new ArrayList<String>();
	private boolean enablePrivilegedAttributeMapping;
	private List<String> privilegedAttributeMapping = new ArrayList<String>();
	private List<String> customAuthenticationProcessing = new ArrayList<String>();
	private List<String> customLogoutHandler = new ArrayList<String>();
	private List<String> customVerificationHandler = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getLoginFromUri() {
		return loginFromUri;
	}

	public void setLoginFromUri(List<String> loginFromUri) {
		this.loginFromUri = loginFromUri;
	}

	public List<String> getLoginErrorUri() {
		return loginErrorUri;
	}

	public void setLoginErrorUri(List<String> loginErrorUri) {
		this.loginErrorUri = loginErrorUri;
	}

	public boolean getUseInternalLogin() {
		return useInternalLogin;
	}

	public void setUseInternalLogin(boolean useInternalLogin) {
		this.useInternalLogin = useInternalLogin;
	}

	public String getLoginContentFileName() {
		return loginContentFileName;
	}

	public void setLoginContentFileName(String loginContentFileName) {
		this.loginContentFileName = loginContentFileName;
	}

	public List<String> getApplicationLogoutHandler() {
		return applicationLogoutHandler;
	}

	public void setApplicationLogoutHandler(List<String> applicationLogoutHandler) {
		this.applicationLogoutHandler = applicationLogoutHandler;
	}

	public List<String> getApplicationLogoutUri() {
		return applicationLogoutUri;
	}

	public void setApplicationLogoutUri(List<String> applicationLogoutUri) {
		this.applicationLogoutUri = applicationLogoutUri;
	}

	public List<String> getLogoutReguestParameter() {
		return logoutReguestParameter;
	}

	public void setLogoutReguestParameter(List<String> logoutReguestParameter) {
		this.logoutReguestParameter = logoutReguestParameter;
	}

	public boolean getLogoutIntrospectEnabled() {
		return logoutIntrospectEnabled;
	}

	public void setLogoutIntrospectEnabled(boolean logoutIntrospectEnabled) {
		this.logoutIntrospectEnabled = logoutIntrospectEnabled;
	}

	public List<String> getLogoutEntryUri() {
		return logoutEntryUri;
	}

	public void setLogoutEntryUri(List<String> logoutEntryUri) {
		this.logoutEntryUri = logoutEntryUri;
	}

	public List<String> getResponseAccessDeniedUri() {
		return responseAccessDeniedUri;
	}

	public void setResponseAccessDeniedUri(List<String> responseAccessDeniedUri) {
		this.responseAccessDeniedUri = responseAccessDeniedUri;
	}

	public List<String> getNotEnforcedUris() {
		return notEnforcedUris;
	}

	public void setNotEnforcedUris(List<String> notEnforcedUris) {
		this.notEnforcedUris = notEnforcedUris;
	}

	public boolean getInvertNotEnforcesUris() {
		return invertNotEnforcesUris;
	}

	public void setInvertNotEnforcesUris(boolean invertNotEnforcesUris) {
		this.invertNotEnforcesUris = invertNotEnforcesUris;
	}

	public boolean getNotEnforcedUrisCacheEnabled() {
		return notEnforcedUrisCacheEnabled;
	}

	public void setNotEnforcedUrisCacheEnabled(boolean notEnforcedUrisCacheEnabled) {
		this.notEnforcedUrisCacheEnabled = notEnforcedUrisCacheEnabled;
	}

	public Integer getNotEnforcedUrisCacheSize() {
		return notEnforcedUrisCacheSize;
	}

	public void setNotEnforcedUrisCacheSize(Integer notEnforcedUrisCacheSize) {
		this.notEnforcedUrisCacheSize = notEnforcedUrisCacheSize;
	}

	public boolean getRefreshSessionIdleTime() {
		return refreshSessionIdleTime;
	}

	public void setRefreshSessionIdleTime(boolean refreshSessionIdleTime) {
		this.refreshSessionIdleTime = refreshSessionIdleTime;
	}

	public List<String> getNotEnforcedClientIpList() {
		return notEnforcedClientIpList;
	}

	public void setNotEnforcedClientIpList(List<String> notEnforcedClientIpList) {
		this.notEnforcedClientIpList = notEnforcedClientIpList;
	}

	public boolean getNotEnforcedIpInvertList() {
		return notEnforcedIpInvertList;
	}

	public void setNotEnforcedIpInvertList(boolean notEnforcedIpInvertList) {
		this.notEnforcedIpInvertList = notEnforcedIpInvertList;
	}

	public boolean getNotEnforcedIpCacheFlag() {
		return notEnforcedIpCacheFlag;
	}

	public void setNotEnforcedIpCacheFlag(boolean notEnforcedIpCacheFlag) {
		this.notEnforcedIpCacheFlag = notEnforcedIpCacheFlag;
	}

	public Integer getNotEnforcedIpCacheSize() {
		return notEnforcedIpCacheSize;
	}

	public void setNotEnforcedIpCacheSize(Integer notEnforcedIpCacheSize) {
		this.notEnforcedIpCacheSize = notEnforcedIpCacheSize;
	}

	public String getProfileAttributeFetchMode() {
		return profileAttributeFetchMode;
	}

	public void setProfileAttributeFetchMode(String profileAttributeFetchMode) {
		this.profileAttributeFetchMode = profileAttributeFetchMode;
	}

	public List<String> getProfileAttributeMapping() {
		return profileAttributeMapping;
	}

	public void setProfileAttributeMapping(List<String> profileAttributeMapping) {
		this.profileAttributeMapping = profileAttributeMapping;
	}

	public String getResponseAttributeFetchModel() {
		return responseAttributeFetchModel;
	}

	public void setResponseAttributeFetchModel(String responseAttributeFetchModel) {
		this.responseAttributeFetchModel = responseAttributeFetchModel;
	}

	public List<String> getResponseAttributeMapping() {
		return responseAttributeMapping;
	}

	public void setResponseAttributeMapping(List<String> responseAttributeMapping) {
		this.responseAttributeMapping = responseAttributeMapping;
	}

	public String getCookieSeparatorCharacter() {
		return cookieSeparatorCharacter;
	}

	public void setCookieSeparatorCharacter(String cookieSeparatorCharacter) {
		this.cookieSeparatorCharacter = cookieSeparatorCharacter;
	}

	public String getFetchAttributeDateFormat() {
		return fetchAttributeDateFormat;
	}

	public void setFetchAttributeDateFormat(String fetchAttributeDateFormat) {
		this.fetchAttributeDateFormat = fetchAttributeDateFormat;
	}

	public boolean getAttributeCookieEncode() {
		return attributeCookieEncode;
	}

	public void setAttributeCookieEncode(boolean attributeCookieEncode) {
		this.attributeCookieEncode = attributeCookieEncode;
	}

	public String getSessionAttributeFetchMode() {
		return sessionAttributeFetchMode;
	}

	public void setSessionAttributeFetchMode(String sessionAttributeFetchMode) {
		this.sessionAttributeFetchMode = sessionAttributeFetchMode;
	}

	public List<String> getSessionAttributeMapping() {
		return sessionAttributeMapping;
	}

	public void setSessionAttributeMapping(List<String> sessionAttributeMapping) {
		this.sessionAttributeMapping = sessionAttributeMapping;
	}

	public List<String> getDefaultPrivilegedAttribute() {
		return defaultPrivilegedAttribute;
	}

	public void setDefaultPrivilegedAttribute(List<String> defaultPrivilegedAttribute) {
		this.defaultPrivilegedAttribute = defaultPrivilegedAttribute;
	}

	public List<String> getPrivilegedAttributeType() {
		return privilegedAttributeType;
	}

	public void setPrivilegedAttributeType(List<String> privilegedAttributeType) {
		this.privilegedAttributeType = privilegedAttributeType;
	}

	public List<String> getPrivilegedAttributesToLowerCase() {
		return privilegedAttributesToLowerCase;
	}

	public void setPrivilegedAttributesToLowerCase(List<String> privilegedAttributesToLowerCase) {
		this.privilegedAttributesToLowerCase = privilegedAttributesToLowerCase;
	}

	public List<String> getPrivilegedSessionAttribute() {
		return privilegedSessionAttribute;
	}

	public void setPrivilegedSessionAttribute(List<String> privilegedSessionAttribute) {
		this.privilegedSessionAttribute = privilegedSessionAttribute;
	}

	public boolean getEnablePrivilegedAttributeMapping() {
		return enablePrivilegedAttributeMapping;
	}

	public void setEnablePrivilegedAttributeMapping(boolean enablePrivilegedAttributeMapping) {
		this.enablePrivilegedAttributeMapping = enablePrivilegedAttributeMapping;
	}

	public List<String> getPrivilegedAttributeMapping() {
		return privilegedAttributeMapping;
	}

	public void setPrivilegedAttributeMapping(List<String> privilegedAttributeMapping) {
		this.privilegedAttributeMapping = privilegedAttributeMapping;
	}

	public List<String> getCustomAuthenticationProcessing() {
		return customAuthenticationProcessing;
	}

	public void setCustomAuthenticationProcessing(List<String> customAuthenticationProcessing) {
		this.customAuthenticationProcessing = customAuthenticationProcessing;
	}

	public List<String> getCustomLogoutHandler() {
		return customLogoutHandler;
	}

	public void setCustomLogoutHandler(List<String> customLogoutHandler) {
		this.customLogoutHandler = customLogoutHandler;
	}

	public List<String> getCustomVerificationHandler() {
		return customVerificationHandler;
	}

	public void setCustomVerificationHandler(List<String> customVerificationHandler) {
		this.customVerificationHandler = customVerificationHandler;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static J2EEApplicationBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EEApplicationBean bean = new J2EEApplicationBean();
		bean.setName(name);
		bean.setLoginFromUri(removeIndexPrefix(listFromSet(attributes.get(AgentAttributesConstants.LOGIN_FORM_URI))));
		bean.setLoginErrorUri(removeIndexPrefix(listFromSet(attributes.get(AgentAttributesConstants.LOGIN_ERROR_URI))));
		bean.setUseInternalLogin(booleanFromSet(attributes.get(AgentAttributesConstants.USE_INTERNAL_LOGIN)));
		bean.setLoginContentFileName(stringFromSet(attributes.get(AgentAttributesConstants.LOGIN_CONTENT_FILE_NAME)));
		bean.setApplicationLogoutHandler(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.APPLICATION_LOGOUT_HANDLER)), ""));
		bean.setApplicationLogoutUri(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.APPLICATION_LOGOUT_URI)), ""));
		bean.setLogoutReguestParameter(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.LOGOUT_REQUEST_PARAMETER)), ""));
		bean.setLogoutIntrospectEnabled(booleanFromSet(attributes
				.get(AgentAttributesConstants.LOGOUT_INTROSPECT_ENABLED)));
		bean.setLogoutEntryUri(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.LOGOUT_ENTRY_URI_CURRENT_VALUES)), ""));
		bean.setResponseAccessDeniedUri(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.RESOURCE_ACCESS_DENIED_URI_CURRENT_VALUES)), ""));
		bean.setNotEnforcedUris(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_URIS_CURRENT_VALUES))));

		bean.setInvertNotEnforcesUris(booleanFromSet(attributes.get(AgentAttributesConstants.INVERT_NOT_ENFORCED_URIS)));
		bean.setNotEnforcedUrisCacheEnabled(booleanFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_URIS_CACHE_ENABLED)));
		bean.setNotEnforcedUrisCacheSize(integerFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_URIS_CACHE_SIZE)));
		bean.setRefreshSessionIdleTime(booleanFromSet(attributes
				.get(AgentAttributesConstants.REFRESH_SESSION_IDLE_TIME)));
		bean.setNotEnforcedClientIpList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_CLIENT_IP_LIST_CURRENT_VALUES))));
		bean.setNotEnforcedIpInvertList(booleanFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_IP_INVERT_LIST)));
		bean.setNotEnforcedIpCacheFlag(booleanFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_IP_CACHE_FLAG)));
		bean.setNotEnforcedIpCacheSize(integerFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_IP_CACHE_SIZE)));
		bean.setProfileAttributeFetchMode(stringFromSet(attributes
				.get(AgentAttributesConstants.PROFILE_ATTRIBUTE_FETCH_MODE)));
		bean.setProfileAttributeMapping(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.PROFILE_ATTRIBUTE_MAPPING_CURRENT_VALUES)), ""));
		bean.setResponseAttributeFetchModel(stringFromSet(attributes
				.get(AgentAttributesConstants.RESPONSE_ATTRIBUTE_FETCH_MODE)));
		bean.setResponseAttributeMapping(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.RESPONSE_ATTRIBUTE_MAPPING_CURRENT_VALUES)), ""));
		bean.setCookieSeparatorCharacter(stringFromSet(attributes
				.get(AgentAttributesConstants.COOKIE_SEPARATOR_CHARACTER)));
		bean.setFetchAttributeDateFormat(stringFromSet(attributes
				.get(AgentAttributesConstants.FETCH_ATTRIBUTE_DATE_FORMAT)));
		bean.setAttributeCookieEncode(booleanFromSet(attributes.get(AgentAttributesConstants.ATTRIBUTE_COOKIE_ENCODE)));
		bean.setSessionAttributeFetchMode(stringFromSet(attributes
				.get(AgentAttributesConstants.SESSION_ATTRIBUTE_FETCH_MODE)));
		bean.setSessionAttributeMapping(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.SESSION_ATTRIBUTE_MAPPING_CURRENT_VALUES)), ""));
		bean.setDefaultPrivilegedAttribute(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.DEFAULT_PRIVILEGED_ATTRIBUTE_CURRENT_VALUES))));
		bean.setPrivilegedAttributeType(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.PRIVILEGED_ATTRIBUTE_TYPE_CURRENT_VALUES))));
		bean.setPrivilegedAttributesToLowerCase(listFromSet(attributes
				.get(AgentAttributesConstants.PRIVILEGED_ATTRIBUTES_TO_LOWER_CASE_CURRENT_VALUES)));
		bean.setPrivilegedSessionAttribute(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.PRIVILEGED_SESSION_ATTRIBUTE_CURRENT_VALUES))));
		bean.setEnablePrivilegedAttributeMapping(booleanFromSet(attributes
				.get(AgentAttributesConstants.ENABLE_PRIVILEGED_ATTRIBUTE_MAPPING)));
		bean.setPrivilegedAttributeMapping(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_AUTHENTICATION_HANDLER_CURRENT_VALUES)), ""));
		bean.setCustomAuthenticationProcessing(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_LOGOUT_HANDLER_CURRENT_VALUES)), ""));
		bean.setCustomLogoutHandler(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_VERIFICATION_HANDLER_CURRENT_VALUES)), ""));

		return bean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.LOGIN_FORM_URI, asSet(addIndexPrefix(getLoginFromUri())));
		map.put(AgentAttributesConstants.LOGIN_ERROR_URI, asSet(addIndexPrefix(getLoginErrorUri())));

		map.put(AgentAttributesConstants.USE_INTERNAL_LOGIN, asSet(getUseInternalLogin()));
		map.put(AgentAttributesConstants.LOGIN_CONTENT_FILE_NAME, asSet(getLoginContentFileName()));

		map.put(AgentAttributesConstants.APPLICATION_LOGOUT_HANDLER,
				asSetWithSpecialEmptyList(getApplicationLogoutHandler()));

		map.put(AgentAttributesConstants.APPLICATION_LOGOUT_URI, asSetWithSpecialEmptyList(getApplicationLogoutUri()));
		map.put(AgentAttributesConstants.LOGOUT_REQUEST_PARAMETER,
				asSetWithSpecialEmptyList(getLogoutReguestParameter()));

		map.put(AgentAttributesConstants.LOGOUT_INTROSPECT_ENABLED, asSet(getLogoutIntrospectEnabled()));

		map.put(AgentAttributesConstants.LOGOUT_ENTRY_URI_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getLogoutEntryUri()));
		map.put(AgentAttributesConstants.RESOURCE_ACCESS_DENIED_URI_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getResponseAccessDeniedUri()));

		map.put(AgentAttributesConstants.NOT_ENFORCED_URIS_CURRENT_VALUES, asSet(addIndexPrefix(getNotEnforcedUris())));

		map.put(AgentAttributesConstants.INVERT_NOT_ENFORCED_URIS, asSet(getInvertNotEnforcesUris()));
		map.put(AgentAttributesConstants.NOT_ENFORCED_URIS_CACHE_ENABLED, asSet(getNotEnforcedUrisCacheEnabled()));
		map.put(AgentAttributesConstants.NOT_ENFORCED_URIS_CACHE_SIZE, asSet(getNotEnforcedUrisCacheSize()));
		map.put(AgentAttributesConstants.REFRESH_SESSION_IDLE_TIME, asSet(getRefreshSessionIdleTime()));
		map.put(AgentAttributesConstants.NOT_ENFORCED_CLIENT_IP_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getNotEnforcedClientIpList())));

		map.put(AgentAttributesConstants.NOT_ENFORCED_IP_INVERT_LIST, asSet(getNotEnforcedIpInvertList()));
		map.put(AgentAttributesConstants.NOT_ENFORCED_IP_CACHE_FLAG, asSet(getNotEnforcedIpCacheFlag()));
		map.put(AgentAttributesConstants.NOT_ENFORCED_IP_CACHE_SIZE, asSet(getNotEnforcedIpCacheSize()));
		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTE_FETCH_MODE, asSet(getProfileAttributeFetchMode()));
		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTE_MAPPING_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getProfileAttributeMapping()));
		map.put(AgentAttributesConstants.RESPONSE_ATTRIBUTE_FETCH_MODE, asSet(getResponseAttributeFetchModel()));
		map.put(AgentAttributesConstants.RESPONSE_ATTRIBUTE_MAPPING_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getResponseAttributeMapping()));

		map.put(AgentAttributesConstants.COOKIE_SEPARATOR_CHARACTER, asSet(getCookieSeparatorCharacter()));
		map.put(AgentAttributesConstants.FETCH_ATTRIBUTE_DATE_FORMAT, asSet(getFetchAttributeDateFormat()));
		map.put(AgentAttributesConstants.ATTRIBUTE_COOKIE_ENCODE, asSet(getAttributeCookieEncode()));
		map.put(AgentAttributesConstants.SESSION_ATTRIBUTE_FETCH_MODE, asSet(getSessionAttributeFetchMode()));
		map.put(AgentAttributesConstants.SESSION_ATTRIBUTE_MAPPING_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getSessionAttributeMapping()));
		map.put(AgentAttributesConstants.DEFAULT_PRIVILEGED_ATTRIBUTE_CURRENT_VALUES,
				asSet(addIndexPrefix(getDefaultPrivilegedAttribute())));
		map.put(AgentAttributesConstants.PRIVILEGED_ATTRIBUTE_TYPE_CURRENT_VALUES,
				asSet(addIndexPrefix(getPrivilegedAttributeType())));
		map.put(AgentAttributesConstants.PRIVILEGED_ATTRIBUTES_TO_LOWER_CASE_CURRENT_VALUES,
				asSet(getPrivilegedAttributesToLowerCase()));
		map.put(AgentAttributesConstants.PRIVILEGED_SESSION_ATTRIBUTE_CURRENT_VALUES,
				asSet(addIndexPrefix(getPrivilegedSessionAttribute())));
		map.put(AgentAttributesConstants.ENABLE_PRIVILEGED_ATTRIBUTE_MAPPING,
				asSet(getEnablePrivilegedAttributeMapping()));
		map.put(AgentAttributesConstants.CUSTOM_AUTHENTICATION_HANDLER_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getPrivilegedAttributeMapping()));
		map.put(AgentAttributesConstants.CUSTOM_LOGOUT_HANDLER_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getCustomAuthenticationProcessing()));
		map.put(AgentAttributesConstants.CUSTOM_VERIFICATION_HANDLER_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getCustomLogoutHandler()));

		return map;
	}

}
