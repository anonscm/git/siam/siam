/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

/**
 * Encapsulates data for a SAMLv2 IDP AuthContext.
 *
 */
public class SAMLv2IDPAuthContext extends SAMLv2AuthContext {

	private static final long serialVersionUID = 1L;

	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * SAMLv2IDPAuthContext constructor.
	 * 
	 * @param name
	 *            - name
	 */
	public SAMLv2IDPAuthContext(String name) {
		super(name);
	}

	/**
	 * SAMLv2IDPAuthContext constructor.
	 */
	public SAMLv2IDPAuthContext() {
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append("|");
		builder.append(getLevel());
		builder.append("|");
		if (key != null && !"none".equals(key)) {
			builder.append(key);
			builder.append("=");
			builder.append((value == null) ? "" : value);
		}
		builder.append("|default");

		return builder.toString();
	}

	/**
	 * 
	 * @param s
	 *            - String
	 * @return SAMLv2AuthContext
	 */
	public static SAMLv2IDPAuthContext fromString(String s) {

		String[] split = s.split("\\|");

		SAMLv2IDPAuthContext context = new SAMLv2IDPAuthContext();

		context.setName(split[0]);
		context.setLevel(split[1]);

		context.setSupported(true);

		if (split.length > 2 && split[2] != null && split[2].length() != 0) {
			if (split[2].contains("=")) {
				int index = split[2].indexOf("=");
				context.setKey(split[2].substring(0, index));
				context.setValue(split[2].substring(index + 1, split[2].length()));
			}
		}

		if (split.length > 3 && split[3] != null && "default".equals(split[3])) {
			context.setDefault(true);
		}

		return context;

	}

}
