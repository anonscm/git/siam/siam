/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPAdvanced;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPEntityProviderService;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * SP Advanced
 * 
 * Samlv2SPAdvancedController shows the edit SAMLv2 SP Advanced Attributes form
 * and does request processing of the edit SAMLv2 SP Advanced Attributes action.
 * 
 */
@Controller("Samlv2SPAdvancedController")
@RequestMapping(value = "view", params = { "ctx=Samlv2SPAdvancedController" })
public class Samlv2SPAdvancedController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2SPAdvancedController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows edit SP Advanced Attributes for current {@link EntityProvider}
	 * form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("spAdvanced")) {
			model.addAttribute("spAdvanced", spEntityProvider.getAdvanced());
		}

		model.addAttribute("metaAlias", spEntityProvider.getMetaAlias());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/sp/advanced/edit";
	}

	/**
	 * Add value to
	 * SaeConfigurationApplicationSecurityConfigurationCurrentValues list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSaeConfigurationApplicationSecurityConfigurationCurrentValues")
	public void doAddSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced, BindingResult result,
			String saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue != null
				&& !saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue.equals("")) {
			spAdvanced.getSaeConfigurationApplicationSecurityConfigurationCurrentValues().add(
					saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from
	 * SaeConfigurationApplicationSecurityConfigurationCurrentValues list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSaeConfigurationApplicationSecurityConfigurationCurrentValues")
	public void doDeleteSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced, BindingResult result,
			String[] saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		if (saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues != null) {
			for (int i = 0; i < saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues.length; i++) {
				spAdvanced.getSaeConfigurationApplicationSecurityConfigurationCurrentValues().remove(
						saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value to EcpConfigurationRequestIDPList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param ecpConfigurationRequestIDPListAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addEcpConfigurationRequestIDPList")
	public void doAddEcpConfigurationRequestIDPList(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced,
			BindingResult result, String ecpConfigurationRequestIDPListAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (ecpConfigurationRequestIDPListAddValue != null && !ecpConfigurationRequestIDPListAddValue.equals("")) {
			spAdvanced.getEcpConfigurationRequestIDPList().add(ecpConfigurationRequestIDPListAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from EcpConfigurationRequestIDPList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param ecpConfigurationRequestIDPListDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteEcpConfigurationRequestIDPList")
	public void doDeleteEcpConfigurationRequestIDPList(
			@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced, BindingResult result,
			String[] ecpConfigurationRequestIDPListDeleteValues, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (ecpConfigurationRequestIDPListDeleteValues != null) {
			for (int i = 0; i < ecpConfigurationRequestIDPListDeleteValues.length; i++) {
				spAdvanced.getEcpConfigurationRequestIDPList().remove(ecpConfigurationRequestIDPListDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value to RelayStateURLList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param relayStateURLListAddValue
	 *            - value to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addRelayStateURLList")
	public void doAddRelayStateURLList(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced,
			BindingResult result, String relayStateURLListAddValue, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (relayStateURLListAddValue != null && !relayStateURLListAddValue.equals("")) {
			spAdvanced.getRelayStateURLList().add(relayStateURLListAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from RelayStateURLList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param relayStateURLListDeleteValues
	 *            - value to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteRelayStateURLList")
	public void doDeleteRelayStateURLList(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced,
			String[] relayStateURLListDeleteValues, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		if (relayStateURLListDeleteValues != null) {
			for (int i = 0; i < relayStateURLListDeleteValues.length; i++) {
				spAdvanced.getRelayStateURLList().remove(relayStateURLListDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value to ProxyList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param proxyListAddValue
	 *            - value to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addProxyList")
	public void doAddProxyList(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced, BindingResult result,
			String proxyListAddValue, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		if (proxyListAddValue != null && !proxyListAddValue.equals("")) {
			spAdvanced.getProxyList().add(proxyListAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from ProxyList list.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced
	 * @param proxyListDeleteValues
	 *            - value to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteProxyList")
	public void doDeleteProxyList(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced,
			String[] proxyListDeleteValues, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		if (proxyListDeleteValues != null) {
			for (int i = 0; i < proxyListDeleteValues.length; i++) {
				spAdvanced.getProxyList().remove(proxyListDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save SAMLV2SPAdvanced.
	 * 
	 * @param spAdvanced
	 *            - SAMLV2SPAdvanced to be saved
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("spAdvanced") @Valid SAMLV2SPAdvanced spAdvanced, BindingResult result,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);
		spEntityProvider.setAdvanced(spAdvanced);

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, spEntityProvider);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}
}
