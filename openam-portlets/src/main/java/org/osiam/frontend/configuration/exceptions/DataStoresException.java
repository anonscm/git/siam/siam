/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.exceptions;

import org.osiam.frontend.common.exceptions.OSIAMException;

/**
 * Encapsulates any exception thrown from the DataStoresService.
 * 
 */
public class DataStoresException extends OSIAMException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor for DataStoresException.
	 */
	public DataStoresException() {
		super();
	}

	/**
	 * Constructor using message String.
	 * 
	 * @param message
	 *            - exception message
	 */
	public DataStoresException(String message) {
		super(message);
	}

	/**
	 * Constructor using message String and encapsulated Exception.
	 * 
	 * @param message
	 *            - exception message
	 * @param exception
	 *            - exception to be encapsulated
	 */
	public DataStoresException(String message, Exception exception) {
		super(message, exception);
	}

}
