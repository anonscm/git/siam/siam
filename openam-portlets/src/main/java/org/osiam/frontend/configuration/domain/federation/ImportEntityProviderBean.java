/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * Encapsulates data for an Import Entity Provider.
 * 
 */
public class ImportEntityProviderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String metadataFileLocationType;
	private MultipartFile metadataFileLocation;
	private String metadataFileURLLocation;

	private String extendedDataFileLocationType;
	private MultipartFile extendedDataFileLocation;
	private String extendedDataFileURLLocation;

	public String getMetadataFileLocationType() {
		return metadataFileLocationType;
	}

	public void setMetadataFileLocationType(String metadataFileLocationType) {
		this.metadataFileLocationType = metadataFileLocationType;
	}

	public MultipartFile getMetadataFileLocation() {
		return metadataFileLocation;
	}

	public void setMetadataFileLocation(MultipartFile metadataFileLocation) {
		this.metadataFileLocation = metadataFileLocation;
	}

	public String getExtendedDataFileLocationType() {
		return extendedDataFileLocationType;
	}

	public void setExtendedDataFileLocationType(String extendedDataFileLocationType) {
		this.extendedDataFileLocationType = extendedDataFileLocationType;
	}

	public MultipartFile getExtendedDataFileLocation() {
		return extendedDataFileLocation;
	}

	public void setExtendedDataFileLocation(MultipartFile extendedDataFileLocation) {
		this.extendedDataFileLocation = extendedDataFileLocation;
	}

	public String getMetadataFileURLLocation() {
		return metadataFileURLLocation;
	}

	public void setMetadataFileURLLocation(String metadataFileURLLocation) {
		this.metadataFileURLLocation = metadataFileURLLocation;
	}

	public String getExtendedDataFileURLLocation() {
		return extendedDataFileURLLocation;
	}

	public void setExtendedDataFileURLLocation(String extendedDataFileURLLocation) {
		this.extendedDataFileURLLocation = extendedDataFileURLLocation;
	}

}
