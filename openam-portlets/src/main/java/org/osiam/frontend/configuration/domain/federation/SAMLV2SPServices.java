/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Encapsulates data for a SAMLV2 SP Services bean.
 */
public class SAMLV2SPServices {

	private String singleLogoutServiceDefaultServices;

	private String singleLogoutServiceHTTPREDIRECTLocationServices;
	private String singleLogoutServiceHTTPREDIRECTResponseLocationServices;

	private String singleLogoutServicePOSTLocationServices;
	private String singleLogoutServicePOSTResponseLocationServices;

	private String singleLogoutServiceSOAPLocationServices;

	private String manageNameIDServiceDefaultServices;

	private String manageNameIDServiceHTTPREDIRECTLocationServices;
	private String manageNameIDServiceHTTPREDIRECTResponseLocationServices;

	private String manageNameIDServicePOSTLocationServices;
	private String manageNameIDServicePOSTResponseLocationServices;

	private String manageNameIDServiceSOAPLocationServices;
	private String manageNameIDServiceSOAPResponseLocationServices;

	private boolean assertionServiceHTTPARTIFACTDefault;
	private Long assertionServiceHTTPARTIFACTIndex;
	private String assertionServiceHTTPARTIFACTLocationServices;

	private String assertionServiceHTTPARTIFACTResponseLocationServices;

	private boolean assertionServicePOSTDefault;
	private Long assertionServicePOSTIndex;
	private String assertionServicePOSTLocationServices;

	private String assertionServicePOSTResponseLocationServices;

	private Long assertionServicePAOSIndex;
	private boolean assertionServicePAOSDefault;
	private String assertionServicePAOSLocationServices;

	public boolean getAssertionServiceHTTPARTIFACTDefault() {
		return assertionServiceHTTPARTIFACTDefault;
	}

	public void setAssertionServiceHTTPARTIFACTDefault(boolean assertionServiceHTTPARTIFACTDefault) {
		this.assertionServiceHTTPARTIFACTDefault = assertionServiceHTTPARTIFACTDefault;
	}

	public Long getAssertionServiceHTTPARTIFACTIndex() {
		return assertionServiceHTTPARTIFACTIndex;
	}

	public void setAssertionServiceHTTPARTIFACTIndex(Long assertionServiceHTTPARTIFACTIndex) {
		this.assertionServiceHTTPARTIFACTIndex = assertionServiceHTTPARTIFACTIndex;
	}

	public Long getAssertionServicePOSTIndex() {
		return assertionServicePOSTIndex;
	}

	public void setAssertionServicePOSTIndex(Long assertionServicePOSTIndex) {
		this.assertionServicePOSTIndex = assertionServicePOSTIndex;
	}

	public Long getAssertionServicePAOSIndex() {
		return assertionServicePAOSIndex;
	}

	public void setAssertionServicePAOSIndex(Long assertionServicePAOSIndex) {
		this.assertionServicePAOSIndex = assertionServicePAOSIndex;
	}

	public boolean getAssertionServicePOSTDefault() {
		return assertionServicePOSTDefault;
	}

	public void setAssertionServicePOSTDefault(boolean assertionServicePOSTDefault) {
		this.assertionServicePOSTDefault = assertionServicePOSTDefault;
	}

	public boolean getAssertionServicePAOSDefault() {
		return assertionServicePAOSDefault;
	}

	public void setAssertionServicePAOSDefault(boolean assertionServicePAOSDefault) {
		this.assertionServicePAOSDefault = assertionServicePAOSDefault;
	}

	public String getAssertionServiceHTTPARTIFACTResponseLocationServices() {
		return assertionServiceHTTPARTIFACTResponseLocationServices;
	}

	public void setAssertionServiceHTTPARTIFACTResponseLocationServices(
			String assertionServiceHTTPARTIFACTResponseLocationServices) {
		this.assertionServiceHTTPARTIFACTResponseLocationServices = assertionServiceHTTPARTIFACTResponseLocationServices;
	}

	public String getAssertionServicePOSTLocationServices() {
		return assertionServicePOSTLocationServices;
	}

	public void setAssertionServicePOSTLocationServices(String assertionServicePOSTLocationServices) {
		this.assertionServicePOSTLocationServices = assertionServicePOSTLocationServices;
	}

	public String getAssertionServicePOSTResponseLocationServices() {
		return assertionServicePOSTResponseLocationServices;
	}

	public void setAssertionServicePOSTResponseLocationServices(String assertionServicePOSTResponseLocationServices) {
		this.assertionServicePOSTResponseLocationServices = assertionServicePOSTResponseLocationServices;
	}

	public String getAssertionServicePAOSLocationServices() {
		return assertionServicePAOSLocationServices;
	}

	public void setAssertionServicePAOSLocationServices(String assertionServicePAOSLocationServices) {
		this.assertionServicePAOSLocationServices = assertionServicePAOSLocationServices;
	}

	public String getAssertionServiceHTTPARTIFACTLocationServices() {
		return assertionServiceHTTPARTIFACTLocationServices;
	}

	public void setAssertionServiceHTTPARTIFACTLocationServices(String assertionServiceHTTPARTIFACTLocationServices) {
		this.assertionServiceHTTPARTIFACTLocationServices = assertionServiceHTTPARTIFACTLocationServices;
	}

	/**
	 * @return {@link SAMLV2SPAdvanced} bean created based on the given map.
	 * @param map - map with input data values.
	 * @throws UnsupportedEncodingException - when input data is not in the
	 *         correct encoding.
	 */
	public static SAMLV2SPServices fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2SPServices bean = new SAMLV2SPServices();
		String xmlDocString = stringFromSet(map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));
		Document metadataDocument = XmlUtil.toDOMDocument(xmlDocString);
		if (metadataDocument == null) {
			return bean;
		}
		NodeList childList = metadataDocument.getChildNodes();
		if (childList.getLength() != 1) {
			throw new IllegalArgumentException(String.format("The contains contains %d node . Expected 1", childList
					.getLength()));
		}

		Node rootNode = metadataDocument.getFirstChild();
		Node descriptorNode = XmlUtil.getChildNode(rootNode, FederationConstants.SPSSO_DESCRIPTOR_NODE);

		Set<Node> singleLogoutServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.SINGLE_LOGOUT_SERVICE_NODE);
		int firstIndex = xmlDocString.indexOf("<SPSSODescriptor");

		if (singleLogoutServiceNodeSet.size() != 0) {
			for (Node node : singleLogoutServiceNodeSet) {
				if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect")) {
					bean.setSingleLogoutServiceHTTPREDIRECTLocationServices(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.LOCATION_ATTRIBUTE));
					bean.setSingleLogoutServiceHTTPREDIRECTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
					bean.setSingleLogoutServicePOSTLocationServices(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.LOCATION_ATTRIBUTE));
					bean.setSingleLogoutServicePOSTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
					bean.setSingleLogoutServiceSOAPLocationServices(XmlUtil.getNodeAttributeValue(node,
							FederationConstants.LOCATION_ATTRIBUTE));
				}
			}

			int httpRedirectIndex = xmlDocString.indexOf(
					"<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect", firstIndex);
			int httpPostIndex = xmlDocString.indexOf(
					"<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", firstIndex);
			int soapIndex = xmlDocString.indexOf(
					"<SingleLogoutService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", firstIndex);
			if (httpRedirectIndex < httpPostIndex) {
				if (httpRedirectIndex < soapIndex) {
					bean.setSingleLogoutServiceDefaultServices(FederationConstants.HTTP_REDIRECT);
				} else {
					bean.setSingleLogoutServiceDefaultServices(FederationConstants.SOAP);
				}
			} else {
				if (httpPostIndex < soapIndex) {
					bean.setSingleLogoutServiceDefaultServices(FederationConstants.HTTP_POST);
				} else {
					bean.setSingleLogoutServiceDefaultServices(FederationConstants.SOAP);
				}
			}
		}

		Set<Node> manageNameIdServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.MANAGE_NAME_ID_SERVICE_NODE);
		for (Node node : manageNameIdServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect")) {
				bean.setManageNameIDServiceHTTPREDIRECTLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setManageNameIDServiceHTTPREDIRECTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
				bean.setManageNameIDServicePOSTLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setManageNameIDServicePOSTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setManageNameIDServiceSOAPLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setManageNameIDServiceSOAPResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
			}
		}
		int httpRedirectIndex = xmlDocString.indexOf(
				"<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect", firstIndex);
		int httpPostIndex = xmlDocString.indexOf(
				"<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", firstIndex);
		int soapIndex = xmlDocString.indexOf(
				"<ManageNameIDService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", firstIndex);
		if (httpRedirectIndex < httpPostIndex) {
			if (httpRedirectIndex < soapIndex) {
				bean.setManageNameIDServiceDefaultServices(FederationConstants.HTTP_REDIRECT);
			} else {
				bean.setManageNameIDServiceDefaultServices(FederationConstants.SOAP);
			}
		} else {
			if (httpPostIndex < soapIndex) {
				bean.setManageNameIDServiceDefaultServices(FederationConstants.HTTP_POST);
			} else {
				bean.setManageNameIDServiceDefaultServices(FederationConstants.SOAP);
			}
		}

		Set<Node> artifactConsumerServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.ARTIFACT_CONSUMER_SERVICE_NODE);
		for (Node node : artifactConsumerServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact")) {
				bean.setAssertionServiceHTTPARTIFACTLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setAssertionServiceHTTPARTIFACTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				bean.setAssertionServiceHTTPARTIFACTIndex(XmlUtil.getNodeAttributeLongValue(node,
						FederationConstants.INDEX_ATTRIBUTE));
				bean.setAssertionServiceHTTPARTIFACTDefault(XmlUtil.getNodeAttributeBooleanValue(node,
						FederationConstants.IS_DEFAULT_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST")) {
				bean.setAssertionServicePOSTLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setAssertionServicePOSTResponseLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.RESPONSE_LOCATION_ATTRIBUTE));
				bean.setAssertionServicePOSTIndex(XmlUtil.getNodeAttributeLongValue(node,
						FederationConstants.INDEX_ATTRIBUTE));
				bean.setAssertionServicePOSTDefault(XmlUtil.getNodeAttributeBooleanValue(node,
						FederationConstants.IS_DEFAULT_ATTRIBUTE));
			} else if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:PAOS")) {
				bean.setAssertionServicePAOSLocationServices(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));
				bean.setAssertionServicePAOSIndex(XmlUtil.getNodeAttributeLongValue(node,
						FederationConstants.INDEX_ATTRIBUTE));
				bean.setAssertionServicePAOSDefault(XmlUtil.getNodeAttributeBooleanValue(node,
						FederationConstants.IS_DEFAULT_ATTRIBUTE));
			}
		}

		return bean;

	}

	private void addLogoutHttpRedirect(Node descriptorNode) {
		XmlUtil.createBindingLocationResponseElement(descriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
				getSingleLogoutServiceHTTPREDIRECTLocationServices(),
				getSingleLogoutServiceHTTPREDIRECTResponseLocationServices());
	}

	private void addLogoutHttpPost(Node descriptorNode) {
		XmlUtil.createBindingLocationResponseElement(descriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", getSingleLogoutServicePOSTLocationServices(),
				getSingleLogoutServicePOSTResponseLocationServices());
	}

	private void addLogoutSoap(Node descriptorNode) {
		XmlUtil.createBindingLocationElement(descriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", getSingleLogoutServiceSOAPLocationServices());
	}

	private void addManageHttpRedirect(Node descriptorNode) {
		XmlUtil.createBindingLocationResponseElement(descriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
				getManageNameIDServiceHTTPREDIRECTLocationServices(),
				getManageNameIDServiceHTTPREDIRECTResponseLocationServices());
	}

	private void addManageHttpPost(Node descriptorNode) {
		XmlUtil.createBindingLocationResponseElement(descriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", getManageNameIDServicePOSTLocationServices(),
				getManageNameIDServicePOSTResponseLocationServices());
	}

	private void addManageSoap(Node descriptorNode) {
		XmlUtil.createBindingLocationResponseElement(descriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", getManageNameIDServiceSOAPLocationServices(),
				getManageNameIDServiceSOAPResponseLocationServices());
	}

	/**
	 * Add all data {@link Node} to given map.
	 * @param map - the map to add the nodes;
	 * @param samlv2spEntityProviderService - the parent service.
	 */
	public void addAllDatatoMap(Map<String, Document> map, SAMLV2SPEntityProviderService samlv2spEntityProviderService) {

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);
	

		Node descriptorNode = XmlUtil.getChildNode(metadataDocument.getFirstChild(),
				FederationConstants.SPSSO_DESCRIPTOR_NODE);

		if (FederationConstants.HTTP_REDIRECT.equals(getSingleLogoutServiceDefaultServices())) {
			addLogoutHttpRedirect(descriptorNode);
			addLogoutHttpPost(descriptorNode);
			addLogoutSoap(descriptorNode);
		} else if (FederationConstants.HTTP_POST.equals(getSingleLogoutServiceDefaultServices())) {
			addLogoutHttpPost(descriptorNode);
			addLogoutHttpRedirect(descriptorNode);
			addLogoutSoap(descriptorNode);
		} else {
			addLogoutSoap(descriptorNode);
			addLogoutHttpRedirect(descriptorNode);
			addLogoutHttpPost(descriptorNode);
		}

		if (FederationConstants.HTTP_REDIRECT.equals(getManageNameIDServiceDefaultServices())) {
			addManageHttpRedirect(descriptorNode);
			addManageHttpPost(descriptorNode);
			addManageSoap(descriptorNode);
		} else if (FederationConstants.HTTP_POST.equals(getManageNameIDServiceDefaultServices())) {
			addManageHttpPost(descriptorNode);
			addManageHttpRedirect(descriptorNode);
			addManageSoap(descriptorNode);
		} else {
			addManageSoap(descriptorNode);
			addManageHttpRedirect(descriptorNode);
			addManageHttpPost(descriptorNode);
		}

		XmlUtil.createListElement(descriptorNode, "NameIDFormat", samlv2spEntityProviderService.getAssertionContent()
				.getNameIDFormatListCurrentValues());

		Map<String, String> attributes = new HashMap<String, String>();
		if (getAssertionServiceHTTPARTIFACTDefault()) {
			attributes.put("isDefault", "true");
		}
		attributes.put("index", getAssertionServiceHTTPARTIFACTIndex().toString());

		XmlUtil.createBindingLocationElement(descriptorNode, "AssertionConsumerService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact",
				getAssertionServiceHTTPARTIFACTLocationServices(), attributes);

		attributes = new HashMap<String, String>();
		if (getAssertionServicePOSTDefault()) {
			attributes.put("isDefault", "true");
		}
		attributes.put("index", getAssertionServicePOSTIndex().toString());

		XmlUtil
				.createBindingLocationElement(descriptorNode, "AssertionConsumerService",
						"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", getAssertionServicePOSTLocationServices(),
						attributes);

		attributes = new HashMap<String, String>();
		if (getAssertionServicePAOSDefault()) {
			attributes.put("isDefault", "true");
		}
		attributes.put("index", getAssertionServicePAOSIndex().toString());

		XmlUtil.createBindingLocationElement(descriptorNode, "AssertionConsumerService",
				"urn:oasis:names:tc:SAML:2.0:bindings:PAOS", getAssertionServicePAOSLocationServices(), attributes);

	}

	public String getSingleLogoutServiceDefaultServices() {
		return singleLogoutServiceDefaultServices;
	}

	public void setSingleLogoutServiceDefaultServices(String singleLogoutServiceDefaultServices) {
		this.singleLogoutServiceDefaultServices = singleLogoutServiceDefaultServices;
	}

	public String getSingleLogoutServiceHTTPREDIRECTLocationServices() {
		return singleLogoutServiceHTTPREDIRECTLocationServices;
	}

	public void setSingleLogoutServiceHTTPREDIRECTLocationServices(
			String singleLogoutServiceHTTPREDIRECTLocationServices) {
		this.singleLogoutServiceHTTPREDIRECTLocationServices = singleLogoutServiceHTTPREDIRECTLocationServices;
	}

	public String getSingleLogoutServiceHTTPREDIRECTResponseLocationServices() {
		return singleLogoutServiceHTTPREDIRECTResponseLocationServices;
	}

	public void setSingleLogoutServiceHTTPREDIRECTResponseLocationServices(
			String singleLogoutServiceHTTPREDIRECTResponseLocationServices) {
		this.singleLogoutServiceHTTPREDIRECTResponseLocationServices = singleLogoutServiceHTTPREDIRECTResponseLocationServices;
	}

	public String getSingleLogoutServicePOSTLocationServices() {
		return singleLogoutServicePOSTLocationServices;
	}

	public void setSingleLogoutServicePOSTLocationServices(String singleLogoutServicePOSTLocationServices) {
		this.singleLogoutServicePOSTLocationServices = singleLogoutServicePOSTLocationServices;
	}

	public String getSingleLogoutServicePOSTResponseLocationServices() {
		return singleLogoutServicePOSTResponseLocationServices;
	}

	public void setSingleLogoutServicePOSTResponseLocationServices(
			String singleLogoutServicePOSTResponseLocationServices) {
		this.singleLogoutServicePOSTResponseLocationServices = singleLogoutServicePOSTResponseLocationServices;
	}

	public String getSingleLogoutServiceSOAPLocationServices() {
		return singleLogoutServiceSOAPLocationServices;
	}

	public void setSingleLogoutServiceSOAPLocationServices(String singleLogoutServiceSOAPLocationServices) {
		this.singleLogoutServiceSOAPLocationServices = singleLogoutServiceSOAPLocationServices;
	}

	public String getManageNameIDServiceDefaultServices() {
		return manageNameIDServiceDefaultServices;
	}

	public void setManageNameIDServiceDefaultServices(String manageNameIDServiceDefaultServices) {
		this.manageNameIDServiceDefaultServices = manageNameIDServiceDefaultServices;
	}

	public String getManageNameIDServiceHTTPREDIRECTLocationServices() {
		return manageNameIDServiceHTTPREDIRECTLocationServices;
	}

	public void setManageNameIDServiceHTTPREDIRECTLocationServices(
			String manageNameIDServiceHTTPREDIRECTLocationServices) {
		this.manageNameIDServiceHTTPREDIRECTLocationServices = manageNameIDServiceHTTPREDIRECTLocationServices;
	}

	public String getManageNameIDServiceHTTPREDIRECTResponseLocationServices() {
		return manageNameIDServiceHTTPREDIRECTResponseLocationServices;
	}

	public void setManageNameIDServiceHTTPREDIRECTResponseLocationServices(
			String manageNameIDServiceHTTPREDIRECTResponseLocationServices) {
		this.manageNameIDServiceHTTPREDIRECTResponseLocationServices = manageNameIDServiceHTTPREDIRECTResponseLocationServices;
	}

	public String getManageNameIDServicePOSTLocationServices() {
		return manageNameIDServicePOSTLocationServices;
	}

	public void setManageNameIDServicePOSTLocationServices(String manageNameIDServicePOSTLocationServices) {
		this.manageNameIDServicePOSTLocationServices = manageNameIDServicePOSTLocationServices;
	}

	public String getManageNameIDServicePOSTResponseLocationServices() {
		return manageNameIDServicePOSTResponseLocationServices;
	}

	public void setManageNameIDServicePOSTResponseLocationServices(
			String manageNameIDServicePOSTResponseLocationServices) {
		this.manageNameIDServicePOSTResponseLocationServices = manageNameIDServicePOSTResponseLocationServices;
	}

	public String getManageNameIDServiceSOAPLocationServices() {
		return manageNameIDServiceSOAPLocationServices;
	}

	public void setManageNameIDServiceSOAPLocationServices(String manageNameIDServiceSOAPLocationServices) {
		this.manageNameIDServiceSOAPLocationServices = manageNameIDServiceSOAPLocationServices;
	}

	public String getManageNameIDServiceSOAPResponseLocationServices() {
		return manageNameIDServiceSOAPResponseLocationServices;
	}

	public void setManageNameIDServiceSOAPResponseLocationServices(
			String manageNameIDServiceSOAPResponseLocationServices) {
		this.manageNameIDServiceSOAPResponseLocationServices = manageNameIDServiceSOAPResponseLocationServices;
	}

}
