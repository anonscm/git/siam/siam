/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.datastores;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForADBean;
import org.osiam.frontend.configuration.service.DataStoresService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * LDAPv3ForADController shows the edit "Active Directory" DataStore form and
 * handles requests for add/delete certain parameter values and save DataStore.
 * 
 */
@Controller("LDAPv3ForADController")
@RequestMapping(value = "view", params = { "ctx=LDAPv3ForADController" })
public class LDAPv3ForADController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LDAPv3ForADController.class);

	@Autowired
	private DataStoresService dataStoresService;

	@Autowired
	@Qualifier("ldapv3DataStoreValidator")
	private Validator ldapv3DataStoreValidator;

	/**
	 * Show edit form for a "Active Directory" DataStore.
	 * 
	 * @param name
	 *            - DataStore name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name.
	 */
	@RenderMapping
	public String view(String name, String action, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "dataStoreAtt")) {
			LDAPv3ForADBean ds = new LDAPv3ForADBean();
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				ds = dataStoresService.getDataStoreAttributesValuesForLDAPv3ForAD(getSSOToken(realm, request), realm,
						name);
			} else {
				ds = dataStoresService.getDefaultAttributeValuesForLDAPv3ForAD(getSSOToken(realm, request));
			}

			ds.setName(name);

			model.addAttribute("dataStoreAtt", ds);
		}

		model.addAttribute("actionValue", action);

		return "datastores/activeDirectory/edit";
	}

	/**
	 * Save new value in LDAP Server list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapServerAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapServer")
	public void doAddLdapServer(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String ldapServerAddValue, ActionRequest request, ActionResponse response) {

		if (ldapServerAddValue != null && !ldapServerAddValue.equals("")) {
			dataStoreAtt.getLdapServer().add(ldapServerAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from LDAP Server list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapServerDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapServer")
	public void doDeleteLdapServer(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] ldapServerDeleteValues, ActionRequest request, ActionResponse response) {

		if (ldapServerDeleteValues != null) {
			for (int i = 0; i < ldapServerDeleteValues.length; i++) {
				dataStoreAtt.getLdapServer().remove(ldapServerDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Attribute Name Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeNameMappingAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAttributeNameMapping")
	public void doAddAttributeNameMapping(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String attributeNameMappingAddValue, ActionRequest request, ActionResponse response) {

		if (attributeNameMappingAddValue != null && !attributeNameMappingAddValue.equals("")) {
			dataStoreAtt.getAttributeNameMapping().add(attributeNameMappingAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Attribute Name Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeNameMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAttributeNameMapping")
	public void doDeleteAttributeNameMapping(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] attributeNameMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (attributeNameMappingDeleteValues != null) {
			for (int i = 0; i < attributeNameMappingDeleteValues.length; i++) {
				dataStoreAtt.getAttributeNameMapping().remove(attributeNameMappingDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldapv3 PlugIn Supported Types And Operations list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapv3PlugInSupportedTypesAndOperationsAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapPlugInSupportedTypesOperations")
	public void doAddLdapPlugInSupportedTypesOperations(
			@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt, BindingResult result,
			String ldapv3PlugInSupportedTypesAndOperationsAddValue, ActionRequest request, ActionResponse response) {

		if (ldapv3PlugInSupportedTypesAndOperationsAddValue != null
				&& !ldapv3PlugInSupportedTypesAndOperationsAddValue.equals("")) {
			dataStoreAtt.getLdapv3PlugInSupportedTypesAndOperations().add(
					ldapv3PlugInSupportedTypesAndOperationsAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldapv3 PlugIn Supported Types And Operations list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapv3PlugInSupportedTypesAndOperationsDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapPlugInSupportedTypesOperations")
	public void doDeleteLdapPlugInSupportedTypesOperations(
			@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt, BindingResult result,
			String[] ldapv3PlugInSupportedTypesAndOperationsDeleteValues, ActionRequest request, ActionResponse response) {

		if (ldapv3PlugInSupportedTypesAndOperationsDeleteValues != null) {
			for (int i = 0; i < ldapv3PlugInSupportedTypesAndOperationsDeleteValues.length; i++) {
				dataStoreAtt.getLdapv3PlugInSupportedTypesAndOperations().remove(
						ldapv3PlugInSupportedTypesAndOperationsDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldap User Object Class list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapUserObjectClassAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapUserObjectClass")
	public void doAddLdapUserObjectClass(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String ldapUserObjectClassAddValue, ActionRequest request, ActionResponse response) {

		if (ldapUserObjectClassAddValue != null && !ldapUserObjectClassAddValue.equals("")) {
			dataStoreAtt.getLdapUserObjectClass().add(ldapUserObjectClassAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldap User Object Class list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapUserObjectClassDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapUserObjectClass")
	public void doDeleteLdapUserObjectClass(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] ldapUserObjectClassDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (ldapUserObjectClassDeleteValues != null) {
			for (int i = 0; i < ldapUserObjectClassDeleteValues.length; i++) {
				dataStoreAtt.getLdapUserObjectClass().remove(ldapUserObjectClassDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldap User Attributes list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapUserAttributesAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapUserAttributes")
	public void doAddLdapUserAttributes(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String ldapUserAttributesAddValue, ActionRequest request, ActionResponse response) {

		if (ldapUserAttributesAddValue != null && !ldapUserAttributesAddValue.equals("")) {
			dataStoreAtt.getLdapUserAttributes().add(ldapUserAttributesAddValue);
		}
		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldap User Attributes list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapUserAttributesDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapUserAttributes")
	public void doDeleteLdapUserAttributes(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] ldapUserAttributesDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (ldapUserAttributesDeleteValues != null) {
			for (int i = 0; i < ldapUserAttributesDeleteValues.length; i++) {
				dataStoreAtt.getLdapUserAttributes().remove(ldapUserAttributesDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Create User Attribute Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param createUserAttributeMappingAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCreateUserAttributeMapping")
	public void doAddCreateUserAttributeMapping(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String createUserAttributeMappingAddValue, ActionRequest request,
			ActionResponse response) {

		if (createUserAttributeMappingAddValue != null && !createUserAttributeMappingAddValue.equals("")) {
			dataStoreAtt.getCreateUserAttributeMapping().add(createUserAttributeMappingAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Create User Attribute Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param createUserAttributeMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCreateUserAttributeMapping")
	public void doDeleteCreateUserAttributeMapping(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] createUserAttributeMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (createUserAttributeMappingDeleteValues != null) {
			for (int i = 0; i < createUserAttributeMappingDeleteValues.length; i++) {
				dataStoreAtt.getCreateUserAttributeMapping().remove(createUserAttributeMappingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldap Groups Object Class list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapGroupsObjectClassAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapGroupsObjectClass")
	public void doAddLdapGroupsObjectClass(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String ldapGroupsObjectClassAddValue, ActionRequest request, ActionResponse response) {

		if (ldapGroupsObjectClassAddValue != null && !ldapGroupsObjectClassAddValue.equals("")) {
			dataStoreAtt.getLdapGroupsObjectClass().add(ldapGroupsObjectClassAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldap Groups Object Class list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapGroupsObjectClassDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapGroupsObjectClass")
	public void doDeleteLdapGroupsObjectClass(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] ldapGroupsObjectClassDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (ldapGroupsObjectClassDeleteValues != null) {
			for (int i = 0; i < ldapGroupsObjectClassDeleteValues.length; i++) {
				dataStoreAtt.getLdapGroupsObjectClass().remove(ldapGroupsObjectClassDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldap Groups Attributes list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapGroupsAttributesAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapGroupsAttributes")
	public void doAddLdapGroupsAttributes(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String ldapGroupsAttributesAddValue, ActionRequest request, ActionResponse response) {

		if (ldapGroupsAttributesAddValue != null && !ldapGroupsAttributesAddValue.equals("")) {
			dataStoreAtt.getLdapGroupsAttributes().add(ldapGroupsAttributesAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldap Groups Attributes list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapGroupsAttributesDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapGroupsAttributes")
	public void doDeleteLdapGroupsAttributes(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt,
			BindingResult result, String[] ldapGroupsAttributesDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (ldapGroupsAttributesDeleteValues != null) {
			for (int i = 0; i < ldapGroupsAttributesDeleteValues.length; i++) {
				dataStoreAtt.getLdapGroupsAttributes().remove(ldapGroupsAttributesDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Ldap Exception Error Codes To Retry On list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapExceptionErrorCodesToRetryOnAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapExceptionErrorCodesToRetryOn")
	public void doAddLdapExceptionErrorCodesToRetryOn(
			@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt, BindingResult result,
			String ldapExceptionErrorCodesToRetryOnAddValue, ActionRequest request, ActionResponse response) {

		if (ldapExceptionErrorCodesToRetryOnAddValue != null && !ldapExceptionErrorCodesToRetryOnAddValue.equals("")) {
			dataStoreAtt.getLdapExceptionErrorCodesToRetryOn().add(ldapExceptionErrorCodesToRetryOnAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Ldap Exception Error Codes To Retry On list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param result
	 *            - BindingResult
	 * @param ldapExceptionErrorCodesToRetryOnDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapExceptionErrorCodesToRetryOn")
	public void doDeleteLdapExceptionErrorCodesToRetryOn(
			@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt, BindingResult result,
			String[] ldapExceptionErrorCodesToRetryOnDeleteValues, ActionRequest request, ActionResponse response) {

		if (ldapExceptionErrorCodesToRetryOnDeleteValues != null) {
			for (int i = 0; i < ldapExceptionErrorCodesToRetryOnDeleteValues.length; i++) {
				dataStoreAtt.getLdapExceptionErrorCodesToRetryOn().remove(
						ldapExceptionErrorCodesToRetryOnDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save {@link LDAPv3ForADBean}.
	 * 
	 * @param dataStoreAtt
	 *            - {@link LDAPv3ForADBean}
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("dataStoreAtt") @Valid LDAPv3ForADBean dataStoreAtt, BindingResult result,
			String action, ActionRequest request, ActionResponse response) {

		ldapv3DataStoreValidator.validate(dataStoreAtt, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		String realm = getCurrentRealm(request);

		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			dataStoresService.editDataStoreLDAPv3ForAD(getSSOToken(realm, request), realm, dataStoreAtt);
		} else {
			dataStoresService.addLDAPv3ForADDataStore(getSSOToken(realm, request), realm, dataStoreAtt);
		}
		response.setRenderParameter("ctx", "DataStoresController");
	}

	/**
	 * Reset {@link LDAPv3ForADBean} form.
	 * 
	 * @param name
	 *            - DataStore name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to data store list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "DataStoresController");
	}
}
