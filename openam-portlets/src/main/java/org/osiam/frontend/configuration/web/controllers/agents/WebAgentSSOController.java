/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.WebSSOBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WebAgentSSOController shows the edit "Web Agent" (SSO attributes) form and
 * does request processing of the edit web agent action.
 * 
 */
@Controller("WebAgentSSOController")
@RequestMapping(value = "view", params = { "ctx=WebAgentSSOController" })
public class WebAgentSSOController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAgentSSOController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "Web Agent" - SSO attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name.
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "webAgent")) {
			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			WebSSOBean agent = agentsService.getAttributesForWebSSO(token, realm, name);

			agent.setName(name);

			model.addAttribute("webAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/web/sso/edit";
	}

	/**
	 * Save new value in CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCdssoServletURL")
	public void doAddCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String cdssoServletURLAddValue, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLAddValue != null && !cdssoServletURLAddValue.equals("")) {
			webAgent.getCdssoServletURL().add(cdssoServletURLAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCdssoServletURL")
	public void doDeleteCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = 0; i < cdssoServletURLSelectValues.length; i++) {
				webAgent.getCdssoServletURL().remove(cdssoServletURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move up values from CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpCdssoServletURL")
	public void doMoveUpCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = 0; i < cdssoServletURLSelectValues.length; i++) {
				int poz = webAgent.getCdssoServletURL().indexOf(cdssoServletURLSelectValues[i]);
				if (poz > 0) {
					webAgent.setCdssoServletURL(moveUp(webAgent.getCdssoServletURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move down values from CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownCdssoServletURL")
	public void doMoveDownCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = cdssoServletURLSelectValues.length - 1; i >= 0; i--) {
				int poz = webAgent.getCdssoServletURL().indexOf(cdssoServletURLSelectValues[i]);
				if (poz < webAgent.getCdssoServletURL().size() - 1) {
					webAgent.setCdssoServletURL(moveDown(webAgent.getCdssoServletURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move top values from CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopCdssoServletURL")
	public void doMoveTopCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			webAgent.setCdssoServletURL(moveTop(webAgent.getCdssoServletURL(), cdssoServletURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move bottom values from CDSSO Servlet URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomCdssoServletURL")
	public void doMoveBottomCdssoServletURL(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			webAgent.setCdssoServletURL(moveBottom(webAgent.getCdssoServletURL(), cdssoServletURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Cookies Domain List.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesDomainListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCookiesDomainList")
	public void doAddCookiesDomainList(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String cookiesDomainListAddValue, ActionRequest request, ActionResponse response) {

		if (cookiesDomainListAddValue != null && !cookiesDomainListAddValue.equals("")) {
			webAgent.getCookiesDomainList().add(cookiesDomainListAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Cookies Domain List.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesDomainListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCookiesDomainList")
	public void doDeleteCookiesDomainList(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String[] cookiesDomainListDeleteValues, ActionRequest request, ActionResponse response) {

		if (cookiesDomainListDeleteValues != null) {
			for (int i = 0; i < cookiesDomainListDeleteValues.length; i++) {
				webAgent.getCookiesDomainList().remove(cookiesDomainListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Cookies Reset Name List.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetNameListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCookiesResetNameList")
	public void doAddCookiesResetNameList(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			String cookiesResetNameListAddValue, ActionRequest request, ActionResponse response) {

		if (cookiesResetNameListAddValue != null && !cookiesResetNameListAddValue.equals("")) {
			webAgent.getCookiesResetNameList().add(cookiesResetNameListAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from CookiesResetNameList.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetNameListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCookiesResetNameList")
	public void doDeleteCookiesResetNameList(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent,
			BindingResult result, String[] cookiesResetNameListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (cookiesResetNameListDeleteValues != null) {
			for (int i = 0; i < cookiesResetNameListDeleteValues.length; i++) {
				webAgent.getCookiesResetNameList().remove(cookiesResetNameListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save {@link WebSSOBean}.
	 * 
	 * @param webAgent
	 *            - {@link WebSSOBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveWebAgentSSO(@ModelAttribute("webAgent") @Valid WebSSOBean webAgent, BindingResult result,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", webAgent.getName());
		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveWebSSO(token, realm, webAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");

	}

	/**
	 * Reset {@link WebSSOBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentSSO(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
