/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

import java.util.List;

import org.osiam.frontend.configuration.domain.agents.Agent;
import org.osiam.frontend.configuration.domain.agents.J2EEAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.J2EEApplicationBean;
import org.osiam.frontend.configuration.domain.agents.J2EEGlobalBean;
import org.osiam.frontend.configuration.domain.agents.J2EEMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.J2EEOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.J2EESSOBean;
import org.osiam.frontend.configuration.domain.agents.LocalAgent;
import org.osiam.frontend.configuration.domain.agents.WebAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.WebApplicationBean;
import org.osiam.frontend.configuration.domain.agents.WebGlobalBean;
import org.osiam.frontend.configuration.domain.agents.WebMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.WebOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.WebSSOBean;
import org.osiam.frontend.configuration.exceptions.AgentsException;

import com.iplanet.sso.SSOToken;

/**
 * Agents service interface.
 * 
 * Defines methods for agents management.
 * 
 */
public interface AgentsService {

	/**
	 * Return list of agent types for a realm.
	 * 
	 * @return {@link List} of agent types
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	List<String> getAgentTypesList() throws AgentsException;

	/**
	 * Get the list of existing agents.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param maxResults
	 *            - the maximum number of results to return. Use
	 *            Integer.MAX_VALUE to return all values.
	 * @return {@link List} of agents from a realm
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	List<Agent> getAgentsList(SSOToken token, String realm, int maxResults) throws AgentsException;

	/**
	 * Save a new {@link Agent}.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agent
	 *            - {@link Agent} to insert.
	 * @throws AgentsException
	 *             - if something wrong happens.
	 */
	void addAgent(SSOToken token, String realm, Agent agent) throws AgentsException;

	/**
	 * Delete {@link Agent} for a given name.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param name
	 *            - {@link Agent} name to delete
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void deleteAgent(SSOToken token, String realm, String name) throws AgentsException;

	/**
	 * Get list of existing groups.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param maxResults
	 *            - the maximum number of results to return.
	 * @return {@link List} of group names
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	List<String> getGroupsList(SSOToken token, String realm, int maxResults) throws AgentsException;

	/**
	 * Get attributes for a "Local" agent.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link LocalAgent}
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	LocalAgent getAttributesForLocalAgent(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save attributes for a "Local" agent.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param localAgent
	 *            {@link LocalAgent} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveLocalAgent(SSOToken token, String realm, LocalAgent localAgent) throws AgentsException;

	/**
	 * Get "Global" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebGlobalBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebGlobalBean getAttributesForWebGlobal(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save "Global" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebGlobalBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebGlobal(SSOToken token, String realm, WebGlobalBean webAgent) throws AgentsException;

	/**
	 * Get "Application" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebApplicationBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebApplicationBean getAttributesForWebApplication(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "Application" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebApplicationBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebApplication(SSOToken token, String realm, WebApplicationBean webAgent) throws AgentsException;

	/**
	 * Get "SSO" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebSSOBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebSSOBean getAttributesForWebSSO(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save "SSO" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebSSOBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebSSO(SSOToken token, String realm, WebSSOBean webAgent) throws AgentsException;

	/**
	 * Get "OpenSSO Services" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebOpenSSOServicesBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebOpenSSOServicesBean getAttributesForWebOpenSSOServices(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "OpenSSO Service" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebOpenSSOServicesBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebOpenSSOServices(SSOToken token, String realm, WebOpenSSOServicesBean webAgent) throws AgentsException;

	/**
	 * Get "Miscellaneous" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebMiscellaneousBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebMiscellaneousBean getAttributesForWebMiscellaneous(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "Miscellaneous" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebMiscellaneousBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebMiscellaneous(SSOToken token, String realm, WebMiscellaneousBean webAgent) throws AgentsException;

	/**
	 * Get "Advanced" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link WebAdvancedBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	WebAdvancedBean getAttributesForWebAdvanced(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save "Advanced" attributes for "WebAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param webAgent
	 *            {@link WebAdvancedBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveWebAdvanced(SSOToken token, String realm, WebAdvancedBean webAgent) throws AgentsException;

	/**
	 * Get "Global" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEGlobalBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EEGlobalBean getAttributesForJ2EEGlobal(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save "Global" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            {@link J2EEGlobalBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EEGlobal(SSOToken token, String realm, J2EEGlobalBean j2eeAgent) throws AgentsException;

	/**
	 * Get "Application" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEApplicationBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EEApplicationBean getAttributesForJ2EEApplication(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "Application" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            {@link J2EEApplicationBean} - agent attribute to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EEApplication(SSOToken token, String realm, J2EEApplicationBean j2eeAgent) throws AgentsException;

	/**
	 * Get "SSO" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEApplicationBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EESSOBean getAttributesForJ2EESSO(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Save "SSO" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EESSO(SSOToken token, String realm, J2EESSOBean j2eeAgent) throws AgentsException;

	/**
	 * Get "OpenSSO Services" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEOpenSSOServicesBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EEOpenSSOServicesBean getAttributesForJ2EEOpenSSOServices(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "OpenSSO Service" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            {@link J2EEOpenSSOServicesBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EEOpenSSOServices(SSOToken token, String realm, J2EEOpenSSOServicesBean j2eeAgent)
			throws AgentsException;

	/**
	 * Get "Miscellaneous" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEMiscellaneousBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EEMiscellaneousBean getAttributesForJ2EEMiscellaneousBean(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "Miscellaneous" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            {@link J2EEMiscellaneousBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EEMiscellaneous(SSOToken token, String realm, J2EEMiscellaneousBean j2eeAgent) throws AgentsException;

	/**
	 * Get "Advanced" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param agentName
	 *            - {@link Agent} name
	 * @return {@link J2EEAdvancedBean} - agent attributes
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	J2EEAdvancedBean getAttributesForJ2EEAdvancedBean(SSOToken token, String realm, String agentName)
			throws AgentsException;

	/**
	 * Save "Advanced" attributes for "J2EEAgent".
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param j2eeAgent
	 *            {@link J2EEAdvancedBean} - agent attributes to save
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveJ2EEAdvancedBean(SSOToken token, String realm, J2EEAdvancedBean j2eeAgent) throws AgentsException;

	/**
	 * Get export configuration for given {@link Agent}.
	 * 
	 * @param agentName
	 *            - {@link Agent} name
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @return list of configurations
	 * @throws AgentsException
	 *             - when something wrong happens.
	 * 
	 */
	List<String> getExportConfiguration(SSOToken token, String realm, String agentName) throws AgentsException;

	/**
	 * Updates attributes for {@link LocalAgent}.
	 * 
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * @param agent
	 *            - the agent.
	 * @throws AgentsException
	 *             - when something wrong happens.
	 */
	void saveLocalAgentAttributes(SSOToken token, String realm, LocalAgent agent) throws AgentsException;

	/**
	 * Verify if agent name already exist.
	 * 
	 * @param token
	 *            - SSOToken
	 * @param realm
	 *            - realm
	 * @param agentName
	 *            - agent name
	 * @return true if agent name already exist
	 */
	boolean agentNameAlreadyExist(SSOToken token, String realm, String agentName);

}
