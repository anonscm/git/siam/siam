/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * This class encapsulates data for an {@link SAMLV2IDPEntityProviderService}
 * feature.
 */
public class SAMLV2IDPEntityProviderService extends EntityProviderDescriptor {

	private SAMLV2IDPAssertionContent assertionContent;

	private SAMLV2IDPAssertionProcessing assertionProcessing;

	private SAMLV2IDPServices services;

	private SAMLV2IDPAdvanced advanced;

	public SAMLV2IDPAssertionContent getAssertionContent() {
		return assertionContent;
	}

	public void setAssertionContent(SAMLV2IDPAssertionContent assertionContent) {
		this.assertionContent = assertionContent;
	}

	public SAMLV2IDPAssertionProcessing getAssertionProcessing() {
		return assertionProcessing;
	}

	public void setAssertionProcessing(SAMLV2IDPAssertionProcessing assertionProcessing) {
		this.assertionProcessing = assertionProcessing;
	}

	public SAMLV2IDPServices getServices() {
		return services;
	}

	public void setServices(SAMLV2IDPServices services) {
		this.services = services;
	}

	public SAMLV2IDPAdvanced getAdvanced() {
		return advanced;
	}

	public void setAdvanced(SAMLV2IDPAdvanced advanced) {
		this.advanced = advanced;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2IDPEntityProviderService fromMap(Map<String, Set<String>> map)
			throws UnsupportedEncodingException {

		SAMLV2IDPEntityProviderService service = new SAMLV2IDPEntityProviderService();

		String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));

		Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);
		Node idpssoConfigNode = XmlUtil.getChildNode(entityConfigDocument.getFirstChild(),
				FederationConstants.IDPSSO_CONFIG_NODE);
		service.setMetaAlias(XmlUtil.getNodeAttributeValue(idpssoConfigNode, FederationConstants.META_ALIAS_ATTRIBUTE));

		NodeList configChildNodeList = idpssoConfigNode.getChildNodes();

		for (int i = 0; i < configChildNodeList.getLength(); i++) {
			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}
			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_SIGNING)) {
				service.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION)) {
				service.setEncryptionCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			}

		}

		service.setAdvanced(SAMLV2IDPAdvanced.fromMap(map));
		service.setServices(SAMLV2IDPServices.fromMap(map));
		service.setAssertionProcessing(SAMLV2IDPAssertionProcessing.fromMap(map));
		service.setAssertionContent(SAMLV2IDPAssertionContent.fromMap(map));

		return service;

	}

	/**
	 * SAMLV2IDPEntityProviderService constructor.
	 */
	public SAMLV2IDPEntityProviderService() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put(FederationConstants.META_ALIAS_ATTRIBUTE, getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				FederationConstants.IDPSSO_CONFIG_NODE, queryAttributes);
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);

		Map<String, String> descriptorAttributes = new HashMap<String, String>();
		descriptorAttributes.put("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
		descriptorAttributes.put("WantAuthnRequestsSigned", "false");
		Node descriptorNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
				FederationConstants.IDPSSO_DESCRIPTOR_NODE, descriptorAttributes);

		Node idpConfigNode = XmlUtil.getChildNode(configDocument.getFirstChild(),
				FederationConstants.IDPSSO_CONFIG_NODE);

		XmlUtil.createAttributeValue(idpConfigNode, "description", "");

		XmlUtil.createAttributeValue(idpConfigNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED,
				assertionContent.getBasicauthenticationsettingforSoapbasedbindingEnabled());

		XmlUtil.createAttributeValue(idpConfigNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_USER_NAME,
				assertionContent.getBasicauthenticationsettingforSoapbasedbindingEnabledUserName());

		XmlUtil.createAttributeValue(idpConfigNode,
				FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_PASSWORD,
				assertionContent.getBasicauthenticationsettingforSoapbasedbindingEnabledPassword());

		XmlUtil.createAttributeValue(idpConfigNode, "autofedEnabled", "false");

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.AUTO_FED_ATTRIBUTE, "");

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.EFFECTIVE_TIME,
				assertionContent.getEffectiveTime());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.AUTHENTICATION_CONTEXT_MAPPER,
				assertionContent.getAuthenticationContextMapper());

		List<String> authList = new ArrayList<String>();
		for (SAMLv2AuthContext context : assertionContent.getSamlv2AuthContextList()) {
			if (context.getSupported()) {
				authList.add(context.toString());
			}
		}

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.IDP_AUTHCONTEXT_CLASSREF_MAPPING, authList);

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ACCOUNT_MAPPER,
				assertionProcessing.getAccountMapper());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ATTRIBUTE_MAPPER,
				assertionProcessing.getAttributeMapper());

		XmlUtil.createAttributeValue(idpConfigNode, "assertionIDRequestMapper",
				"com.sun.identity.saml2.plugins.DefaultAssertionIDRequestMapper");

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.NAMEID_VALUE_MAP_CURRENT_VALUES,
				assertionContent.getNameIDValueMapCurrentValues());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ECP_CONFIGURATION_IDP_SESSION_MAPPER,
				advanced.getEcpConfigurationIDPSessionMapper());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ATTRIBUTE_MAP_CURRENT_VALUES,
				assertionProcessing.getAttributeMapCurrentValues());

		XmlUtil.createAttributeValue(idpConfigNode, "wantNameIDEncrypted", assertionContent.getWantNameIDEncrypted());

		XmlUtil.createAttributeValue(idpConfigNode, "wantArtifactResolveSigned",
				assertionContent.getArtifactResolveSigned());

		XmlUtil.createAttributeValue(idpConfigNode, "wantLogoutRequestSigned",
				assertionContent.getLogoutRequestSigned());

		XmlUtil.createAttributeValue(idpConfigNode, "wantLogoutResponseSigned",
				assertionContent.getLogoutResponseSigned());

		XmlUtil.createAttributeValue(idpConfigNode, "wantMNIRequestSigned",
				assertionContent.getManageNameIDRequestSigned());

		XmlUtil.createAttributeValue(idpConfigNode, "wantMNIResponseSigned",
				assertionContent.getManageNameIDResponseSigned());

		XmlUtil.createAttributeValue(idpConfigNode, "cotlist", "");

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.DISCOVERY_BOOTSTRAPPING_ENABLED,
				assertionContent.getDiscoveryBootstrappingEnabled());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.ASSERTION_CACHE_ENABLED,
				assertionContent.getAssertionCacheEnabled());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.NOT_BEFORE_TIME_SKEW,
				assertionContent.getNotBeforeTimeSkew());

		XmlUtil.createAttributeValue(idpConfigNode,
				FederationConstants.SAE_CONFIGURATION_APPLICATION_SECURITY_CONFIGURATION_CURRENT_VALUES,
				advanced.getSaeConfigurationApplicationSecurityConfigurationCurrentValues());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.SAE_CONFIGURATION_IDP_URL,
				advanced.getSaeConfigurationIDPURL());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.LOCAL_CONFIGURATION_AUTH_URL,
				assertionProcessing.getLocalConfigurationAuthURL());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.EXTERNAL_APPLICATION_LOGOUT_URL,
				assertionProcessing.getLocalConfigurationExternalApplicationLogoutURL());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.SESSION_SYNCHRONIZATION_ENABLED, advanced
				.getSessionSynchronizationEnabled().toString());

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.RELAY_STATE_IDP_URL_LIST,
				advanced.getRelayStateURLList());

		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put("isDefault", Boolean.valueOf(services.getIdpServiceAttributesArtifactResolutionServiceDefault())
				.toString());
		attributes.put("index", services.getIdpServiceAttributesArtifactResolutionServiceIndex().toString());

		// Start Key Descriptor Signing

		if (getSigningCertificateAlias() != null && !"".equals(getSigningCertificateAlias())) {

			Map<String, String> signingAttributes = new HashMap<String, String>();
			signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
			Node signingNode = XmlUtil.createElement(descriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE,
					signingAttributes);

			Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getSigningCertificateAlias());

			String base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64SigningCertificate, null);
			x509CertificateNode.setPrefix("ds");

		}
		// Start Key Descriptor Encryption

		if (getEncryptionCertificateAlias() != null && !"".equals(getEncryptionCertificateAlias())) {

			Map<String, String> encryptionAttributes = new HashMap<String, String>();
			encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
			Node encryptionNode = XmlUtil.createElement(descriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE,
					encryptionAttributes);

			Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(
					getAssertionContent().getCertificateAliasesEncryption());

			String base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64EncryptionCertificate, null);
			x509CertificateNode.setPrefix("ds");

			String algorithm;

			if (getAssertionContent().getCertificateAliasesAlgorithm() != null
					&& getAssertionContent().getCertificateAliasesAlgorithm().length() != 0) {

				algorithm = getAssertionContent().getCertificateAliasesAlgorithm();

			} else {

				algorithm = cert.getSigAlgName();
				// algorithm=XMLCipher.AES_128;
			}

			Long keySize;

			if (getAssertionContent().getCertificateAliasesKeySize() != null) {

				keySize = getAssertionContent().getCertificateAliasesKeySize();
			} else {

				keySize = new Long(128);
			}

			Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
			encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
			Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE,
					null, encryptionMethodAttributes);

			Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
					"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
			keySizeNode.setPrefix("xenc");

		}

		XmlUtil.createBindingLocationElement(descriptorNode, FederationConstants.ARTIFACT_RESOLUTION_SERVICE_NODE,
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				services.getIdpServiceAttributesArtifactResolutionServiceLocation(), attributes);

		if (SAMLV2IDPServices.HTTP_REDIRECT.equals(services.getIdpServiceAttributesSingleLogoutServiceDefault())) {
			addLogoutHttpRedirect(descriptorNode);
			addLogoutHttpPost(descriptorNode);
			addLogoutSoap(descriptorNode);
		} else if (SAMLV2IDPServices.HTTP_POST.equals(services.getIdpServiceAttributesSingleLogoutServiceDefault())) {
			addLogoutHttpPost(descriptorNode);
			addLogoutHttpRedirect(descriptorNode);
			addLogoutSoap(descriptorNode);
		} else {
			addLogoutSoap(descriptorNode);
			addLogoutHttpPost(descriptorNode);
			addLogoutHttpRedirect(descriptorNode);
		}

		if (SAMLV2IDPServices.HTTP_REDIRECT.equals(services
				.getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault())) {
			addManageHttpRedirect(descriptorNode);
			addManageHttpPost(descriptorNode);
			addManageSoap(descriptorNode);
		} else if (SAMLV2IDPServices.HTTP_POST.equals(services
				.getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTDefault())) {
			addManageHttpPost(descriptorNode);
			addManageHttpRedirect(descriptorNode);
			addManageSoap(descriptorNode);
		} else {
			addManageSoap(descriptorNode);
			addManageHttpPost(descriptorNode);
			addManageHttpRedirect(descriptorNode);
		}

		XmlUtil.createListElement(descriptorNode, "NameIDFormat", getAssertionContent().getNameIDValueMapFormatValues());

		XmlUtil.createBindingLocationElement(descriptorNode, FederationConstants.SINGLE_SIGN_ON_SERVICE_NODE,
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
				services.getIdpServiceAttributesSingleSignOnServiceHTTPREDIRECTLocation());

		XmlUtil.createBindingLocationElement(descriptorNode, FederationConstants.SINGLE_SIGN_ON_SERVICE_NODE,
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
				services.getIdpServiceAttributesSingleSignOnServicePOSTLocation());

		XmlUtil.createBindingLocationElement(descriptorNode, FederationConstants.SINGLE_SIGN_ON_SERVICE_NODE,
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				services.getIdpServiceAttributesSingleSignOnServiceSOAPLocation());

		XmlUtil.createBindingLocationElement(descriptorNode, FederationConstants.NAME_ID_MAPPING_SERVICE_NODE,
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP", services.getNameIDMapping());

		metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);

		// Node desscriptorNode =
		// XmlUtil.getChildNode(metadataDocument.getFirstChild(),
		// FederationConstants.IDPSSO_DESCRIPTOR_NODE);

		((Element) descriptorNode).setAttribute("WantAuthnRequestsSigned",
				Boolean.toString(assertionContent.getAuthenticationRequestSigned()));

		XmlUtil.createBindingLocationElement(descriptorNode, "AssertionIDRequestService",
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				"http://opensso.example.ro:8080/opensso/AIDReqSoap/IDPRole/metaAlias/qweqew");

		XmlUtil.createBindingLocationElement(descriptorNode, "AssertionIDRequestService",
				"urn:oasis:names:tc:SAML:2.0:bindings:URI",
				"http://opensso.example.ro:8080/opensso/AIDReqUri/IDPRole/metaAlias/qweqew");

	}

	private void addLogoutHttpRedirect(Node desscriptorNode) {
		XmlUtil.createBindingLocationResponseElement(desscriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
				services.getIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTLocation(),
				services.getIdpServiceAttributesSingleLogoutServiceHTTPREDIRECTResponseLocation());

	}

	private void addLogoutHttpPost(Node desscriptorNode) {
		XmlUtil.createBindingLocationResponseElement(desscriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
				services.getIdpServiceAttributesSingleLogoutServicePOSTLocation(),
				services.getIdpServiceAttributesSingleLogoutServicePOSTResponseLocation());
	}

	private void addLogoutSoap(Node desscriptorNode) {
		XmlUtil.createBindingLocationElement(desscriptorNode, "SingleLogoutService",
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				services.getIdpServiceAttributesSingleLogoutServiceSOAPlocation());
	}

	private void addManageHttpRedirect(Node desscriptorNode) {
		XmlUtil.createBindingLocationResponseElement(desscriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
				services.getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTLocation(),
				services.getIdpServiceAttributesManageNameIDServiceHTTPREDIRECTResponseLocation());
	}

	private void addManageHttpPost(Node desscriptorNode) {
		XmlUtil.createBindingLocationResponseElement(desscriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
				services.getIdpServiceAttributesManageNameIDServicePOSTLocation(),
				services.getIdpServiceAttributesManageNameIDServicePOSTResponseLocation());
	}

	private void addManageSoap(Node desscriptorNode) {
		XmlUtil.createBindingLocationResponseElement(desscriptorNode, "ManageNameIDService",
				"urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				services.getIdpServiceAttributesManageNameIDServiceSOAPLocation(),
				services.getIdpServiceAttributesManageNameIDServiceSOAPLocation());

	}

}
