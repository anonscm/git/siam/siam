/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPAssertionContent;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPEntityProviderService;
import org.osiam.frontend.configuration.domain.federation.SAMLv2AuthContext;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * SP Assertion Content
 * 
 * Samlv2SPController shows the edit SAMLv2 SP Assertion Content Attributes form
 * and does request processing of the edit SAMLv2 SP Assertion Content Content
 * Attributes action.
 */
@Controller("Samlv2SPController")
@RequestMapping(value = "view", params = { "ctx=samlv2service_providerController" })
public class Samlv2SPController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2SPController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("samlv2SPAssertionContentValidator")
	private Validator samlv2SPAssertionContentValidator;

	/**
	 * Shows edit SP Assertion Content for current {@link EntityProvider} form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("spAssertionContent")) {
			SAMLV2SPAssertionContent spAssertionContent = spEntityProvider.getAssertionContent();
			spAssertionContent.setSamlv2AuthContextList(federationService.getAuthContext(spAssertionContent
					.getSamlv2AuthContextList()));

			spAssertionContent
					.setAuthenticationContextDefaultAuthenticationContext(getNameForDefaulAuthContext(spAssertionContent
							.getSamlv2AuthContextList()));

			model.addAttribute("spAssertionContent", spAssertionContent);
		}

		model.addAttribute("metaAlias", spEntityProvider.getMetaAlias());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		model.addAttribute("authenticationContextMap", federationService.getAuthenticationContextList());

		return "federation/entityProvider/samlv2/sp/assertionContent/edit";
	}

	/**
	 * Get AuthContext name that have that have isDefaul true.
	 * 
	 * @param list
	 *            - AuthContext list
	 * @return name of the AuthContext that have isDefaul true
	 */
	private String getNameForDefaulAuthContext(List<SAMLv2AuthContext> list) {

		for (Iterator<SAMLv2AuthContext> iterator = list.iterator(); iterator.hasNext();) {
			SAMLv2AuthContext samLv2AuthContext = iterator.next();
			if (samLv2AuthContext.isDefault()) {
				return samLv2AuthContext.getName();
			}
		}
		return "";
	}

	/**
	 * Set isDefaul true for AuthContext with the given name.
	 * 
	 * @param list
	 *            - AuthContext list
	 * @param name
	 *            - AuthContext name
	 * @return - AuthContext list
	 */
	private List<SAMLv2AuthContext> setDefaulAuthContext(List<SAMLv2AuthContext> list, String name) {

		for (Iterator<SAMLv2AuthContext> iterator = list.iterator(); iterator.hasNext();) {
			SAMLv2AuthContext samLv2AuthContext = iterator.next();
			if (name.equals(samLv2AuthContext.getName())) {
				samLv2AuthContext.setDefault(true);
				if (!samLv2AuthContext.getSupported()) {
					samLv2AuthContext.setSupported(true);
				}
				break;
			}
		}
		return list;
	}

	/**
	 * Add value to NameIDFormatListCurrentValues list.
	 * 
	 * @param spAssertionContent
	 *            - SAMLV2SPAssertionContent
	 * @param result
	 *            - BindingResult
	 * @param nameIDFormatListCurrentValuesAddValue
	 *            - value to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNameIDFormatListCurrentValues")
	public void doAddNameIDFormatListCurrentValues(
			@ModelAttribute("spAssertionContent") @Valid SAMLV2SPAssertionContent spAssertionContent,
			BindingResult result, String nameIDFormatListCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDFormatListCurrentValuesAddValue != null && !nameIDFormatListCurrentValuesAddValue.equals("")) {
			spAssertionContent.getNameIDFormatListCurrentValues().add(nameIDFormatListCurrentValuesAddValue);
		}

		spAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(spAssertionContent.getSamlv2AuthContextList(),
				spAssertionContent.getAuthenticationContextDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from NameIDFormatListCurrentValues list.
	 * 
	 * @param spAssertionContent
	 *            - SAMLV2SPAssertionContent
	 * @param nameIDFormatListCurrentValuesDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNameIDFormatListCurrentValues")
	public void doDeleteNameIDFormatListCurrentValues(
			@ModelAttribute("spAssertionContent") @Valid SAMLV2SPAssertionContent spAssertionContent,
			String[] nameIDFormatListCurrentValuesDeleteValues, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (nameIDFormatListCurrentValuesDeleteValues != null) {
			for (int i = 0; i < nameIDFormatListCurrentValuesDeleteValues.length; i++) {
				spAssertionContent.getNameIDFormatListCurrentValues().remove(
						nameIDFormatListCurrentValuesDeleteValues[i]);
			}
		}

		spAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(spAssertionContent.getSamlv2AuthContextList(),
				spAssertionContent.getAuthenticationContextDefaultAuthenticationContext()));

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save SAMLV2SPAssertionContent.
	 * 
	 * @param spAssertionContent
	 *            - SAMLV2SPAssertionContent to be saved
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("spAssertionContent") @Valid SAMLV2SPAssertionContent spAssertionContent,
			BindingResult result, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		samlv2SPAssertionContentValidator.validate(new ObjectForValidate(realm, token, spAssertionContent), result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		spAssertionContent.setSamlv2AuthContextList(setDefaulAuthContext(spAssertionContent.getSamlv2AuthContextList(),
				spAssertionContent.getAuthenticationContextDefaultAuthenticationContext()));

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);
		spEntityProvider.setAssertionContent(spAssertionContent);

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, spEntityProvider);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
