/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.iplanet.sso.SSOToken;

/**
 * FederationController portlet controller.
 */
@Controller("FederationController")
@RequestMapping(value = "view", params = "ctx=FederationController")
public class FederationController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FederationController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("circleOfTrustValidator")
	private Validator circleOfTrustValidator;

	@Autowired
	@Qualifier("entityProviderValidation")
	private Validator entityProviderValidation;

	/**
	 * Show EntityProvider list and CircleOfTrust list.
	 * 
	 * @param checkAllEntityProviders
	 *            - use to decide if all the EntityProvider are checked for
	 *            deletion
	 * @param checkAllCircleOfTrust
	 *            - use to decide if all the CircleOfTrust are checked for
	 *            deletion
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 * 
	 */
	@RenderMapping
	public String view(String checkAllEntityProviders, String checkAllCircleOfTrust, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		model.addAttribute("entityProviderProtocolList", federationService.getEntityProviderProtocolList());

		model.addAttribute("entityProvidersList", federationService.getEntityProviderList(token, realm));
		model.addAttribute("checkAllEntityProviders", checkAllEntityProviders);

		model.addAttribute("circleOfTrustList", federationService.getCircleOfTrustList(token, realm));
		model.addAttribute("checkAllCircleOfTrust", checkAllCircleOfTrust);

		model.addAttribute("entityProviderProtocolList",
				new String[] { FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL,
						FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL });

		return "federation/view";
	}

	/**
	 * Redirect to add EntityProvider form.
	 * 
	 * @param entityP
	 *            - {@link BaseEntity}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "action=doAddEntityProvider")
	public void doAddEntityProvider(@ModelAttribute("entityP") @Valid BaseEntity entityP, BindingResult result,
			ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		entityProviderValidation.validate(new ObjectForValidate(realm, token, entityP), result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		response.setRenderParameter("entityIdentifier", entityP.getName());
		response.setRenderParameter("protocol", entityP.getType());
		response.setRenderParameter("ctx", "EntityProviderAddController");
	}

	/**
	 * Delete EntityProvider.
	 * 
	 * @param deleteEntityProviderList
	 *            - list of EntityProvider names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteEntityProvider")
	public void doDeleteEntityProvider(String[] deleteEntityProviderList, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (deleteEntityProviderList != null) {
			for (int i = 0; i < deleteEntityProviderList.length; i++) {
				federationService.deleteEntityProvider(token, realm, deleteEntityProviderList[i]);
			}
		}
	}

	/**
	 * Set value for "checkAllEntityProviders".
	 * 
	 * @param checkAllEntityProviders
	 *            - use to decide if all the EntityProvider are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteEntityProviders")
	public void doCheckForDeleteEntityProviders(String checkAllEntityProviders, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("checkAllEntityProviders", checkAllEntityProviders);
	}

	/**
	 * Redirect to import entity provider form.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "importEntityProvider")
	public void doImportEntityProvider(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "ImportEntityProviderController");

	}

	/**
	 * Redirect to add CircleOfTrust form.
	 * 
	 * @param entity
	 *            - {@link BaseEntity}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "action=doAddCircleOfTrust")
	public void doAddCircleOfTrust(@ModelAttribute("entity") @Valid BaseEntity entity, BindingResult result,
			ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		circleOfTrustValidator.validate(new ObjectForValidate(realm, token, entity), result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		response.setRenderParameter("circleOfTrustName", entity.getName());
		response.setRenderParameter("action", CommonConstants.ACTION_INSERT);
		response.setRenderParameter("ctx", "CircleOfTrustController");

	}

	/**
	 * Delete CircleOfTrust.
	 * 
	 * @param deleteCircleOfTrustList
	 *            - list of CircleOfTrust names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCircleOfTrust")
	public void doDeleteCircleOfTrust(String[] deleteCircleOfTrustList, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (deleteCircleOfTrustList != null) {
			for (int i = 0; i < deleteCircleOfTrustList.length; i++) {
				federationService.deleteCircleOfTrust(token, realm, deleteCircleOfTrustList[i]);
			}
		}
	}

	/**
	 * Set value for "checkAllCircleOfTrust".
	 * 
	 * @param checkAllCircleOfTrust
	 *            - use to decide if all the CircleOfTrust are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteCircleOfTrust")
	public void doCheckForDeleteCircleOfTrust(String checkAllCircleOfTrust, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("checkAllCircleOfTrust", checkAllCircleOfTrust);
	}

	/**
	 * Export metadata selected entity provider.
	 * 
	 * @param entityName
	 *            - selected entity provider name
	 * @param response
	 *            - ResourceResponse
	 * @throws Exception
	 */
	@ResourceMapping("exportEntityProviderMetadata")
	public void doExportEntityProviderMetadata(String entityName, String entityType, ResourceResponse response,
			ResourceRequest request) throws Exception {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		String name = entityName.replace(' ', '_') + "-metadata.xml";
		String content = federationService.getEntityProvidersForExportMetadata(token, realm, entityType, entityName);

		exportEntityProveder(name, content, response);
	}

	/**
	 * Export extendedData selected entity provider.
	 * 
	 * @param entityName
	 *            - selected entity provider name
	 * @param response
	 *            - ResourceResponse
	 * @throws Exception
	 */
	@ResourceMapping("exportEntityProviderExtendedData")
	public void doExportEntityProviderExtendedData(String entityName, String entityType, ResourceResponse response,
			ResourceRequest request) throws Exception {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		String name = entityName.replace(' ', '_') + "-extended-data.xml";
		String content = federationService
				.getEntityProvidersForExportExtendedData(token, realm, entityType, entityName);

		exportEntityProveder(name, content, response);
	}

	/**
	 * Tell browser program going to return an application file instead of html
	 * page.
	 * 
	 * @param name
	 *            - file name
	 * @param content
	 *            - file content
	 * @param response
	 *            - ResourceResponse
	 * @throws IOException
	 */
	private void exportEntityProveder(String name, String content, ResourceResponse response) throws IOException {

		response.setContentType("text/xml");
		response.setProperty("Content-Disposition", "attachment;filename=" + name);

		PrintWriter out = response.getWriter();

		StringBuffer sb = new StringBuffer();
		sb.append(content);
		out.write(sb.toString());
		out.flush();

	}

}
