/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * 
 * This class encapsulates data for an {@link EntityProvider} feature (e.g. one
 * tab in the user interface).
 */
public class EntityProviderDescriptor {

	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_AUTHENTICATION_AUTHORITY = "authentication_authority";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_ATTRIBUTE_QUERY_PROVIDER = "attribute_query_provider";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_ATTRIBUTE_AUTHORITY = "attribute_authority";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_XACML_POLICY_ENFORCEMENT_POINT = "xacml_policy_enforcement_point";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_XACML_POLICY_DECISION_POINT = "xacml_policy_decision_point";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_SERVICE_PROVIDER = "service_provider";
	/**
	 * SAMLV2 service type.
	 */
	public static final String SAMLV2_IDENTITY_PROVIDER = "identity_provider";

	/**
	 * WSFED service type.
	 */
	public static final String WSFED_SERVICE_PROVIDER = "service_provider";
	/**
	 * WSFED service type.
	 */
	public static final String WSFED_IDENTITY_PROVIDER = "identity_provider";
	/**
	 * WSFED service type.
	 */
	public static final String WSFED_GENERAL = "general";

	/**
	 * ENTITY_PROVIDER.
	 */
	public static final String ENTITY_PROVIDER = "ENTITY_PROVIDER";

	private String entityTypeKey;

	private String type;
	@NotNull
	@Size(min = 1)
	private String metaAlias;

	private String signingCertificateAlias;
	private String encryptionCertificateAlias;

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	/**
	 * Return the metaalias value, always prefixed with slash.
	 * 
	 * @return - the metaalias value, always prefixed with slash.
	 */
	public String getMetaAliasWithSlash() {
		if (metaAlias.startsWith("/")) {
			return metaAlias;
		} else {
			return "/" + metaAlias;
		}
	}

	public String getMetaAlias() {
		return metaAlias;
	}

	public void setMetaAlias(String metaAlias) {
		this.metaAlias = metaAlias;
	}

	public String getSigningCertificateAlias() {
		return signingCertificateAlias;
	}

	public void setSigningCertificateAlias(String signingCertificateAlias) {
		this.signingCertificateAlias = signingCertificateAlias;
	}

	public String getEncryptionCertificateAlias() {
		return encryptionCertificateAlias;
	}

	public void setEncryptionCertificateAlias(String encryptionCertificateAlias) {
		this.encryptionCertificateAlias = encryptionCertificateAlias;
	}

	/**
	 * Creates empty {@link EntityProviderDescriptor}.
	 */
	public EntityProviderDescriptor() {
		setEntityTypeKey(ENTITY_PROVIDER);

	}

	/**
	 * Creates {@link EntityProviderDescriptor} with given type, metaalias,
	 * signingCertificateAlias, encryptionCertificateAlias.
	 * 
	 * @param type
	 *            - the type.
	 * @param metaAlias
	 *            - the metaalias.
	 * @param signingCertificateAlias
	 *            - the signing certificate alias.
	 * @param encryptionCertificateAlias
	 *            - the encryption certificate alias.
	 */
	public EntityProviderDescriptor(String type, String metaAlias, String signingCertificateAlias,
			String encryptionCertificateAlias) {
		super();
		this.type = type;
		this.metaAlias = metaAlias;
		this.signingCertificateAlias = signingCertificateAlias;
		this.encryptionCertificateAlias = encryptionCertificateAlias;
	}

	/**
	 * Converts from map of XML data, as read from the persistence layer, to map
	 * of {@link EntityProviderDescriptor}.
	 * 
	 * @param map
	 *            - map with XML attributes, as read from the persistence layer.
	 * @return map of {@link EntityProviderDescriptor}.
	 * @throws UnsupportedEncodingException
	 *             - when given XML cannot be parsed due to wrong encoding.
	 */
	public static Map<String, EntityProviderDescriptor> fromAttributeMaptoMap(Map<String, Set<String>> map)
			throws UnsupportedEncodingException {
		Map<String, EntityProviderDescriptor> ret = new HashMap<String, EntityProviderDescriptor>();

		// saml
		if (map.containsKey(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {

			String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));

			Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);

			Node root = entityConfigDocument.getFirstChild();

			if (XmlUtil.getChildNodes(root, FederationConstants.IDPSSO_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_IDENTITY_PROVIDER, SAMLV2IDPEntityProviderService.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.SPSSO_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_SERVICE_PROVIDER, SAMLV2SPEntityProviderService.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.XACML_PDP_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_XACML_POLICY_DECISION_POINT, SAMLV2XACMLPDPAttributesBean.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.XACML_PEP_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_XACML_POLICY_ENFORCEMENT_POINT, SAMLV2XACMLPEPAttributesBean.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.ATTRIBUTE_AUTHORITY_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_ATTRIBUTE_AUTHORITY, SAMLV2AttributeAuthorityBean.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.ATTRIBUTE_QUERY_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_ATTRIBUTE_QUERY_PROVIDER, SAMLV2AttributeQueryAttributesBean.fromMap(map));
			}

			if (XmlUtil.getChildNodes(root, FederationConstants.AUTHN_AUTORITY_CONFIG_NODE).size() != 0) {
				ret.put(SAMLV2_AUTHENTICATION_AUTHORITY, SAMLV2AuthnAuthorityAttributesBean.fromMap(map));
			}

		}

		// WS-FED
		if (map.containsKey(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {

			String entityConfig = stringFromSet(map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY));
			if (entityConfig != null) {
				if (entityConfig.length() > 0) {
					Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);

					Node root = entityConfigDocument.getFirstChild();

					if (XmlUtil.getChildNodes(root, FederationConstants.IDPSSO_CONFIG_NODE).size() != 0) {
						ret.put(WSFED_IDENTITY_PROVIDER, WSFEDIDPAttributesBean.fromMap(map));
					}

					if (XmlUtil.getChildNodes(root, FederationConstants.SPSSO_CONFIG_NODE).size() != 0) {
						ret.put(WSFED_SERVICE_PROVIDER, WSFEDSPAttributesBean.fromMap(map));
					}
				}
			}

			ret.put(WSFED_GENERAL, WSFEDGeneralAttributesBean.fromMap(map));

		}

		return ret;
	}

	/**
	 * Add all data {@link Node} to given map.
	 * 
	 * @param map
	 *            - the map to add the nodes;
	 * @throws CertificateEncodingException 
	 */
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

	}

	@Override
	public String toString() {
		return "" + type + ";" + metaAlias + ";" + signingCertificateAlias + ";" + encryptionCertificateAlias;
	}

	/**
	 * Can be one of the following values :
	 * EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY,
	 * EntityProviderDescriptor.SAMLV2_ATTRIBUTE_QUERY_PROVIDER,
	 * EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY,
	 * EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT,
	 * EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT,
	 * EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER,
	 * EntityProviderDescriptor.SAMLV2_IDENTITY_PROVIDER,
	 * EntityProviderDescriptor.WSFED_SERVICE_PROVIDER,
	 * EntityProviderDescriptor.WSFED_IDENTITY_PROVIDER,
	 * EntityProviderDescriptor.WSFED_GENERAL.
	 * 
	 * @return the key
	 */
	public String getEntityTypeKey() {
		return entityTypeKey;
	}

	public void setEntityTypeKey(String entityTypeKey) {
		this.entityTypeKey = entityTypeKey;
	}
}
