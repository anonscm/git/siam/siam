/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * Encapsulates data for a WSFED IDP Attributes Bean.
 * 
 */

public class WSFEDIDPAttributesBean extends EntityProviderDescriptor {
	private static final Logger LOGGER = Logger.getLogger(WSFEDGeneralAttributesBean.class);

	private String claimTypeDisplayName;
	private String claimTypeUri;

	private String nameFormatID;
	private String nameIDAttribute;
	private boolean nameIncludesDomain;
	private String domainAttribute;
	private String upnDomain;

	private String accountMapper;

	private String attributeMapper;
	private List<String> attributeMapCurrentValues = new ArrayList<String>();

	private String assertionEffectiveTime;

	private String assertionNotBeforeTimeSkew;
	private String authnContextMapper;

	public String getAuthnContextMapper() {
		return authnContextMapper;
	}

	public void setAuthnContextMapper(String authnContextMapper) {
		this.authnContextMapper = authnContextMapper;
	}

	public String getAssertionNotBeforeTimeSkew() {
		return assertionNotBeforeTimeSkew;
	}

	public void setAssertionNotBeforeTimeSkew(String assertionNotBeforeTimeSkew) {
		this.assertionNotBeforeTimeSkew = assertionNotBeforeTimeSkew;
	}

	public String getClaimTypeDisplayName() {
		return claimTypeDisplayName;
	}

	public void setClaimTypeDisplayName(String claimTypes) {
		this.claimTypeDisplayName = claimTypes;
	}

	public String getNameFormatID() {
		return nameFormatID;
	}

	public void setNameFormatID(String nameFormatID) {
		this.nameFormatID = nameFormatID;
	}

	public String getNameIDAttribute() {
		return nameIDAttribute;
	}

	public void setNameIDAttribute(String nameIDAttribute) {
		this.nameIDAttribute = nameIDAttribute;
	}

	public boolean getNameIncludesDomain() {
		return nameIncludesDomain;
	}

	public void setNameIncludesDomain(boolean nameIncludesDomain) {
		this.nameIncludesDomain = nameIncludesDomain;
	}

	public String getDomainAttribute() {
		return domainAttribute;
	}

	public void setDomainAttribute(String domainAttribute) {
		this.domainAttribute = domainAttribute;
	}

	public String getUpnDomain() {
		return upnDomain;
	}

	public void setUpnDomain(String upnDomain) {
		this.upnDomain = upnDomain;
	}

	public String getAccountMapper() {
		return accountMapper;
	}

	public void setAccountMapper(String accountMapper) {
		this.accountMapper = accountMapper;
	}

	public String getAttributeMapper() {
		return attributeMapper;
	}

	public void setAttributeMapper(String attributeMapper) {
		this.attributeMapper = attributeMapper;
	}

	public List<String> getAttributeMapCurrentValues() {
		return attributeMapCurrentValues;
	}

	public void setAttributeMapCurrentValues(List<String> attributeMapCurrentValues) {
		this.attributeMapCurrentValues = attributeMapCurrentValues;
	}

	public String getAssertionEffectiveTime() {
		return assertionEffectiveTime;
	}

	public String getClaimTypeUri() {
		return claimTypeUri;
	}

	public void setClaimTypeUri(String claimTypeUri) {
		this.claimTypeUri = claimTypeUri;
	}

	public void setAssertionEffectiveTime(String assertionEffectiveTime) {
		this.assertionEffectiveTime = assertionEffectiveTime;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static WSFEDIDPAttributesBean fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		WSFEDIDPAttributesBean bean = new WSFEDIDPAttributesBean();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.IDPSSO_CONFIG_NODE);

		Node configNode = configNodeSet.iterator().next();

		bean.setMetaAlias(XmlUtil.getNodeAttributeValue(configNode, "metaAlias"));

		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals("signingCertAlias")) {
				bean.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("claimTypeOffered")) {
				bean.setClaimTypeDisplayName(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("nameIdFormat")) {
				bean.setNameFormatID(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("nameIdAttribute")) {
				bean.setNameIDAttribute(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("nameIncludesDomain")) {
				bean.setNameIncludesDomain(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals("domainAttribute")) {
				bean.setDomainAttribute(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("upnDomain")) {
				bean.setUpnDomain(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("idpAccountMapper")) {
				bean.setAccountMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("idpAttributeMapper")) {
				bean.setAttributeMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("attributeMap")) {
				bean.setAttributeMapCurrentValues(XmlUtil.getAttributeValue(node).getValueList());
			} else if (attributeName.equals("assertionEffectiveTime")) {
				String time = XmlUtil.getValueFromAttributeValue(node);
				if (time != null) {
					bean.setAssertionEffectiveTime(time);
				}
			} else if (attributeName.equals("assertionNotBeforeTimeSkew")) {
				bean.setAssertionNotBeforeTimeSkew(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("idpAuthncontextMapper")) {
				bean.setAuthnContextMapper(XmlUtil.getValueFromAttributeValue(node));
			}

		}

		if (bean.getNameIDAttribute() == null || "".equals(bean.getNameIDAttribute())) {
			if (FederationConstants.NAME_ID_FORMAT_EMAIL.equals(bean.getNameFormatID())) {
				bean.setNameIDAttribute("mail");
			} else if (FederationConstants.NAME_ID_FORMAT_COMMON_NAME.equals(bean.getNameFormatID())) {
				bean.setNameIDAttribute("cn");
			} else {
				bean.setNameIDAttribute("uid");
			}
		}

		Document metatadaDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY)));

		if (metatadaDocument == null) {
			return bean;
		}
		Node metadataRootNode = metatadaDocument.getFirstChild();

		if (metadataRootNode != null) {

			Node claimNode = XmlUtil.getChildNode(metadataRootNode, "UriNamedClaimTypesOffered");
			Node claimNodeClaimType = XmlUtil.getChildNode(claimNode, "ClaimType");
			Node claimNodeDisplayName = XmlUtil.getChildNode(claimNodeClaimType, "DisplayName");

			bean.setClaimTypeDisplayName(XmlUtil.getTextValueFromNode(claimNodeDisplayName));
			bean.setClaimTypeUri(XmlUtil.getNodeAttributeValue(claimNodeClaimType, "Uri"));

		}

		return bean;
	}

	/**
	 * WSFEDIDPAttributesBean constructor.
	 */
	public WSFEDIDPAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.WSFED_IDENTITY_PROVIDER);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) {
		Document metadataDocument = map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY);
		Document configDocument = map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Node claimNode = XmlUtil.createElement(metadataDocument.getFirstChild(), "UriNamedClaimTypesOffered");
		Map<String, String> nodeAttributes = new HashMap<String, String>();
		nodeAttributes.put("Uri", claimTypeUri);
		Node claimTypeNode = XmlUtil.createElement(claimNode, "ClaimType", nodeAttributes);
		XmlUtil.createTextElement(claimTypeNode, "DisplayName", claimTypeDisplayName);

		nodeAttributes = new HashMap<String, String>();
		nodeAttributes.put("metaAlias", getMetaAliasWithSlash());
		Node idpssoConfigNode = XmlUtil.createElement(configDocument.getFirstChild(), "IDPSSOConfig", nodeAttributes);

		XmlUtil.createAttributeValue(idpssoConfigNode, "nameIdFormat", nameFormatID);

		if (FederationConstants.NAME_ID_FORMAT_UPN.equals(nameFormatID)) {
			nameIDAttribute = "uid";
			if (nameIncludesDomain) {
				this.domainAttribute = "";
				this.upnDomain = "";
			}
		} else {
			this.nameIncludesDomain = false;
			this.domainAttribute = "";
			this.upnDomain = "";
		}

		if (FederationConstants.NAME_ID_FORMAT_EMAIL.equals(nameFormatID)) {
			nameIDAttribute = "mail";
		}
		if (FederationConstants.NAME_ID_FORMAT_COMMON_NAME.equals(nameFormatID)) {
			nameIDAttribute = "cn";
		}

		XmlUtil.createAttributeValue(idpssoConfigNode, "nameIdAttribute", nameIDAttribute);
		XmlUtil.createAttributeValue(idpssoConfigNode, "nameIncludesDomain", nameIncludesDomain);
		XmlUtil.createAttributeValue(idpssoConfigNode, "domainAttribute", domainAttribute);
		XmlUtil.createAttributeValue(idpssoConfigNode, "upnDomain", upnDomain);
		XmlUtil.createAttributeValue(idpssoConfigNode, "signingCertAlias", getSigningCertificateAlias());
		XmlUtil.createAttributeValue(idpssoConfigNode, "assertionNotBeforeTimeSkew", assertionNotBeforeTimeSkew);
		XmlUtil.createAttributeValue(idpssoConfigNode, "assertionEffectiveTime", assertionEffectiveTime);
		XmlUtil.createAttributeValue(idpssoConfigNode, "idpAuthncontextMapper", authnContextMapper);
		XmlUtil.createAttributeValue(idpssoConfigNode, "idpAccountMapper", accountMapper);
		XmlUtil.createAttributeValue(idpssoConfigNode, "idpAttributeMapper", attributeMapper);
		XmlUtil.createAttributeValue(idpssoConfigNode, "attributeMap", attributeMapCurrentValues);

	}
}
