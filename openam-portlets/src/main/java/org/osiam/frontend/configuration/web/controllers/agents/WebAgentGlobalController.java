/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.WebGlobalBean;
import org.osiam.frontend.configuration.service.AgentsConstants;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WebAgentGlobalController shows the edit "Web Agent" (Global attributes) form
 * and does request processing of the edit web agent action.
 * 
 */
@Controller("WebAgentGlobalController")
@RequestMapping(value = "view", params = { "ctx=WebAgentGlobalController" })
public class WebAgentGlobalController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAgentGlobalController.class);

	@Autowired
	private AgentsService agentsService;

	@Autowired
	@Qualifier("webGlobalBeanValidator")
	private Validator webGlobalBeanValidator;

	/**
	 * Show edit form for a "Web Agent" - global attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "webAgent")) {
			String realm = getCurrentRealm(request);
			WebGlobalBean agent = agentsService.getAttributesForWebGlobal(getSSOToken(realm, request), realm, name);
			agent.setName(name);

			model.addAttribute("webAgent", agent);
		}

		model.addAttribute("name", name);
		String realm = getCurrentRealm(request);
		model.addAttribute("groupsList",
				agentsService.getGroupsList(getSSOToken(realm, request), realm, Integer.MAX_VALUE));

		model.addAttribute("message", message);
		return "agents/web/global/edit";

	}

	/**
	 * Save new value in Agent Root URL for CDSSO list.
	 * 
	 * @param webAgent
	 *            - {@link WebGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentRootURLforCDSSOAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAgentRootURLforCDSSO")
	public void doAddAgentRootURLforCDSSO(@ModelAttribute("webAgent") @Valid WebGlobalBean webAgent,
			BindingResult result, String agentRootURLforCDSSOAddValue, ActionRequest request, ActionResponse response) {

		if (agentRootURLforCDSSOAddValue != null && !agentRootURLforCDSSOAddValue.equals("")) {
			webAgent.getAgentRootURLforCDSSO().add(agentRootURLforCDSSOAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Agent Root URL for CDSSO list.
	 * 
	 * @param webAgent
	 *            - {@link WebGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentRootURLforCDSSODeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAgentRootURLforCDSSO")
	public void doDeleteAgentRootURLforCDSSO(@ModelAttribute("webAgent") @Valid WebGlobalBean webAgent,
			BindingResult result, String[] agentRootURLforCDSSODeleteValues, ActionRequest request,
			ActionResponse response) {

		if (agentRootURLforCDSSODeleteValues != null) {
			for (int i = 0; i < agentRootURLforCDSSODeleteValues.length; i++) {
				webAgent.getAgentRootURLforCDSSO().remove(agentRootURLforCDSSODeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in FQDN Virtual Host Map.
	 * 
	 * @param webAgent
	 *            - {@link WebGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param fqdnVirtualHostMapKey
	 *            - {@link String} key to be insert
	 * @param fqdnVirtualHostMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addFqdnVirtualHostMap")
	public void doAddFqdnVirtualHostMap(@ModelAttribute("webAgent") @Valid WebGlobalBean webAgent,
			BindingResult result, String fqdnVirtualHostMapKey, String fqdnVirtualHostMapValue, ActionRequest request,
			ActionResponse response) {

		if (fqdnVirtualHostMapKey != null && !fqdnVirtualHostMapKey.equals("") && fqdnVirtualHostMapValue != null
				&& !fqdnVirtualHostMapValue.equals("")) {
			webAgent.getFqdnVirtualHostMap().add("[" + fqdnVirtualHostMapKey + "]=" + fqdnVirtualHostMapValue);
		}

		response.setRenderParameter("name", webAgent.getName());
		response.setRenderParameter("ctx", "WebAgentGlobalController");
	}

	/**
	 * Delete values from FQDN Virtual Host Map.
	 * 
	 * @param webAgent
	 *            - {@link WebGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param fqdnVirtualHostMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteFqdnVirtualHostMap")
	public void doDeleteFqdnVirtualHostMap(@ModelAttribute("webAgent") @Valid WebGlobalBean webAgent,
			BindingResult result, String[] fqdnVirtualHostMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (fqdnVirtualHostMapDeleteValues != null) {
			for (int i = 0; i < fqdnVirtualHostMapDeleteValues.length; i++) {
				webAgent.getFqdnVirtualHostMap().remove(fqdnVirtualHostMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
		response.setRenderParameter("ctx", "WebAgentGlobalController");
	}

	/**
	 * Save {@link WebGlobalBean}.
	 * 
	 * @param webAgent
	 *            - {@link WebGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveWebAgentGlobal(@ModelAttribute("webAgent") @Valid WebGlobalBean webAgent, BindingResult result,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", webAgent.getName());

		webGlobalBeanValidator.validate(webAgent, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveWebGlobal(token, realm, webAgent);

		if (AgentsConstants.AGENT_CONFIGURATION_LOCAL.equals(webAgent.getLocationOfAgentConfigurationRepository())) {
			response.setRenderParameter("ctx", "EditLocalController");
		} else {
			response.setRenderParameter("message", "message.prifile_was_updated");
		}

	}

	/**
	 * Reset {@link WebGlobalBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentGlobal(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
