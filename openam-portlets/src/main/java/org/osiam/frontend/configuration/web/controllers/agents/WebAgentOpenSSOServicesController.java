/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.WebOpenSSOServicesBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WebAgentOpenSSOServicesController shows the edit "Web Agent" (OpenSSO
 * Services attributes) form and does request processing of the edit web agent
 * action.
 * 
 */
@Controller("WebAgentOpenSSOServicesController")
@RequestMapping(value = "view", params = { "ctx=WebAgentOpenSSOServicesController" })
public class WebAgentOpenSSOServicesController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAgentOpenSSOServicesController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "Web Agent" - OpenSSO Services attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "webAgent")) {

			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			WebOpenSSOServicesBean agent = agentsService.getAttributesForWebOpenSSOServices(token, realm, name);

			agent.setName(name);
			model.addAttribute("webAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/web/openSSOServices/edit";
	}

	/**
	 * Save new value in OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addOpenSSOLoginURL")
	public void doAddOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String openSSOLoginURLAddValue, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLAddValue != null && !openSSOLoginURLAddValue.equals("")) {
			webAgent.getOpenSSOLoginURL().add(openSSOLoginURLAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteOpenSSOLoginURL")
	public void doDeleteOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = 0; i < openSSOLoginURLSelectValues.length; i++) {
				webAgent.getOpenSSOLoginURL().remove(openSSOLoginURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move up values from OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpOpenSSOLoginURL")
	public void doMoveUpOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = 0; i < openSSOLoginURLSelectValues.length; i++) {
				int poz = webAgent.getOpenSSOLoginURL().indexOf(openSSOLoginURLSelectValues[i]);
				if (poz > 0) {
					webAgent.setOpenSSOLoginURL(moveUp(webAgent.getOpenSSOLoginURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move down values from OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownOpenSSOLoginURL")
	public void doMoveDownOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = openSSOLoginURLSelectValues.length - 1; i >= 0; i--) {
				int poz = webAgent.getOpenSSOLoginURL().indexOf(openSSOLoginURLSelectValues[i]);
				if (poz < webAgent.getOpenSSOLoginURL().size() - 1) {
					webAgent.setOpenSSOLoginURL(moveDown(webAgent.getOpenSSOLoginURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move top values from OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopOpenSSOLoginURL")
	public void doMoveTopOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			webAgent.setOpenSSOLoginURL(moveTop(webAgent.getOpenSSOLoginURL(), openSSOLoginURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move bottom values from OpenSSO Login URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomOpenSSOLoginURL")
	public void doMoveBottomOpenSSOLoginURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			webAgent.setOpenSSOLoginURL(moveBottom(webAgent.getOpenSSOLoginURL(), openSSOLoginURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in OpenSSO Logout URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addOpenSSOLogoutURL")
	public void doAddOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String openSSOLogoutURLAddValue, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLAddValue != null && !openSSOLogoutURLAddValue.equals("")) {
			webAgent.getOpenSSOLogoutURL().add(openSSOLogoutURLAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from OpenSSO Logout URLlist.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteOpenSSOLogoutURL")
	public void doDeleteOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = 0; i < openSSOLogoutURLSelectValues.length; i++) {
				webAgent.getOpenSSOLogoutURL().remove(openSSOLogoutURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move up values from OpenSSO Logout URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpOpenSSOLogoutURL")
	public void doMoveUpOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = 0; i < openSSOLogoutURLSelectValues.length; i++) {
				int poz = webAgent.getOpenSSOLogoutURL().indexOf(openSSOLogoutURLSelectValues[i]);
				if (poz > 0) {
					webAgent.setOpenSSOLogoutURL(moveUp(webAgent.getOpenSSOLogoutURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move down values from OpenSSO Logout URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownOpenSSOLogoutURL")
	public void doMoveDownOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = openSSOLogoutURLSelectValues.length - 1; i >= 0; i--) {
				int poz = webAgent.getOpenSSOLogoutURL().indexOf(openSSOLogoutURLSelectValues[i]);
				if (poz < webAgent.getOpenSSOLogoutURL().size() - 1) {
					webAgent.setOpenSSOLogoutURL(moveDown(webAgent.getOpenSSOLogoutURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move top values from OpenSSO Logout URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopOpenSSOLogoutURL")
	public void doMoveTopOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			webAgent.setOpenSSOLogoutURL(moveTop(webAgent.getOpenSSOLogoutURL(), openSSOLogoutURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move bottom values from OpenSSO Logout URL list.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomOpenSSOLogoutURL")
	public void doMoveBottomOpenSSOLogoutURL(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			webAgent.setOpenSSOLogoutURL(moveBottom(webAgent.getOpenSSOLogoutURL(), openSSOLogoutURLSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLogoutURLList")
	public void doAddLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String logoutURLListAddValue, ActionRequest request, ActionResponse response) {

		if (logoutURLListAddValue != null && !logoutURLListAddValue.equals("")) {
			webAgent.getLogoutURLList().add(logoutURLListAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLogoutURLList")
	public void doDeleteLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutURLListSelectValues, ActionRequest request, ActionResponse response) {

		if (logoutURLListSelectValues != null) {
			for (int i = 0; i < logoutURLListSelectValues.length; i++) {
				webAgent.getLogoutURLList().remove(logoutURLListSelectValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move up values from Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpLogoutURLList")
	public void doMoveUpLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutURLListSelectValues, ActionRequest request, ActionResponse response) {

		if (logoutURLListSelectValues != null) {
			for (int i = 0; i < logoutURLListSelectValues.length; i++) {
				int poz = webAgent.getLogoutURLList().indexOf(logoutURLListSelectValues[i]);
				if (poz > 0) {
					webAgent.setLogoutURLList(moveUp(webAgent.getLogoutURLList(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move down values from Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownLogoutURLList")
	public void doMoveDownLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutURLListSelectValues, ActionRequest request, ActionResponse response) {

		if (logoutURLListSelectValues != null) {
			for (int i = logoutURLListSelectValues.length - 1; i >= 0; i--) {
				int poz = webAgent.getLogoutURLList().indexOf(logoutURLListSelectValues[i]);
				if (poz < webAgent.getLogoutURLList().size() - 1) {
					webAgent.setLogoutURLList(moveDown(webAgent.getLogoutURLList(), poz));
				}
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move top values from Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopLogoutURLList")
	public void doMoveTopLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutURLListSelectValues, ActionRequest request, ActionResponse response) {

		if (logoutURLListSelectValues != null) {
			webAgent.setLogoutURLList(moveTop(webAgent.getLogoutURLList(), logoutURLListSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Move bottom values from Logout URL List.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutURLListSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomLogoutURLList")
	public void doMoveBottomLogoutURLList(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutURLListSelectValues, ActionRequest request, ActionResponse response) {

		if (logoutURLListSelectValues != null) {
			webAgent.setLogoutURLList(moveBottom(webAgent.getLogoutURLList(), logoutURLListSelectValues));
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Logout Cookies List For Reset.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutCookiesListForResetAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLogoutCookiesListForReset")
	public void doAddLogoutCookiesListForReset(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String logoutCookiesListForResetAddValue, ActionRequest request,
			ActionResponse response) {

		if (logoutCookiesListForResetAddValue != null && !logoutCookiesListForResetAddValue.equals("")) {
			webAgent.getLogoutCookiesListForReset().add(logoutCookiesListForResetAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Logout Cookies List For Reset.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutCookiesListForResetDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLogoutCookiesListForReset")
	public void doDeleteLogoutCookiesListForReset(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, String[] logoutCookiesListForResetDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (logoutCookiesListForResetDeleteValues != null) {
			for (int i = 0; i < logoutCookiesListForResetDeleteValues.length; i++) {
				webAgent.getLogoutCookiesListForReset().remove(logoutCookiesListForResetDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save {@link WebOpenSSOServicesBean}.
	 * 
	 * @param webAgent
	 *            - {@link WebOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveWebAgentOpenSSOServices(@ModelAttribute("webAgent") @Valid WebOpenSSOServicesBean webAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", webAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveWebOpenSSOServices(token, realm, webAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");

	}

	/**
	 * Reset {@link WebOpenSSOServicesBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentOpenSSOServices(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
