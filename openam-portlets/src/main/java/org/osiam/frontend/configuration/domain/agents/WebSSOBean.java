/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a web SSO bean.
 * 
 */
public class WebSSOBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String cookieName;
	private boolean cookieSecurity;
	private boolean crossDomainSSO;
	private List<String> cdssoServletURL = new ArrayList<String>();
	private List<String> cookiesDomainList = new ArrayList<String>();
	private boolean cookieReset;
	private List<String> cookiesResetNameList = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCookieName() {
		return cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public boolean getCookieSecurity() {
		return cookieSecurity;
	}

	public void setCookieSecurity(boolean cookieSecurity) {
		this.cookieSecurity = cookieSecurity;
	}

	public boolean getCrossDomainSSO() {
		return crossDomainSSO;
	}

	public void setCrossDomainSSO(boolean crossDomainSSO) {
		this.crossDomainSSO = crossDomainSSO;
	}

	public List<String> getCdssoServletURL() {
		return cdssoServletURL;
	}

	public void setCdssoServletURL(List<String> cdssoServletURL) {
		this.cdssoServletURL = cdssoServletURL;
	}

	public List<String> getCookiesDomainList() {
		return cookiesDomainList;
	}

	public void setCookiesDomainList(List<String> cookiesDomainList) {
		this.cookiesDomainList = cookiesDomainList;
	}

	public boolean getCookieReset() {
		return cookieReset;
	}

	public void setCookieReset(boolean cookieReset) {
		this.cookieReset = cookieReset;
	}

	public List<String> getCookiesResetNameList() {
		return cookiesResetNameList;
	}

	public void setCookiesResetNameList(List<String> cookiesResetNameList) {
		this.cookiesResetNameList = cookiesResetNameList;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebSSOBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebSSOBean bean = new WebSSOBean();
		bean.setName(name);
		bean.setCookieName(stringFromSet(attributes.get(AgentAttributesConstants.COOKIE_NAME)));
		bean.setCookieSecurity(booleanFromSet(attributes.get(AgentAttributesConstants.COOKIE_SECURITY)));
		bean.setCrossDomainSSO(booleanFromSet(attributes.get(AgentAttributesConstants.CROSS_DOMAIN_SSO)));
		bean.setCdssoServletURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.CDSSO_SERVLET_URL_CURRENT_VALUES))));
		bean.setCookiesDomainList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.COOKIES_DOMAIN_LIST_CURRENT_VALUES))));
		bean.setCookieReset(booleanFromSet(attributes.get(AgentAttributesConstants.COOKIE_RESET)));
		bean.setCookiesResetNameList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.COOKIES_RESET_NAME_LIST_CURRENT_VALUES))));

		return bean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		map.put(AgentAttributesConstants.COOKIE_NAME, asSet(getCookieName()));
		map.put(AgentAttributesConstants.COOKIE_SECURITY, asSet(getCookieSecurity()));
		map.put(AgentAttributesConstants.CROSS_DOMAIN_SSO, asSet(getCrossDomainSSO()));
		map.put(AgentAttributesConstants.CDSSO_SERVLET_URL_CURRENT_VALUES, asSet(addIndexPrefix(getCdssoServletURL())));
		map.put(AgentAttributesConstants.COOKIES_DOMAIN_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getCookiesDomainList())));
		map.put(AgentAttributesConstants.COOKIE_RESET, asSet(getCookieReset()));
		map.put(AgentAttributesConstants.COOKIES_RESET_NAME_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getCookiesResetNameList())));

		return map;
	}

}
