/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import java.util.Iterator;
import java.util.Map;

import com.sun.identity.saml2.meta.SAML2MetaException;
import com.sun.identity.shared.Constants;
import com.sun.identity.shared.configuration.SystemPropertiesManager;

/**
 * Creates default XML data for WSFED entity providers.
 */
public class WSFedDefaultDataCreator {

	private WSFedDefaultDataCreator() {

	}

	private static void buildWSFEDIdentityProvider(StringBuffer buff, String idpAlias, String url,
			EntityProviderDescriptor entityProviderDescriptor) {

		String idpSCertAlias = entityProviderDescriptor.getSigningCertificateAlias();
		String idpECertAlias = entityProviderDescriptor.getEncryptionCertificateAlias();

		if (idpSCertAlias == null) {
			idpSCertAlias = "";
		}
		if (idpECertAlias == null) {
			idpECertAlias = "";
		}

		buff.append("<IDPSSOConfig metaAlias=\"" + idpAlias + "\">\n" + "        " + "<Attribute name=\"displayName"
				+ "\">\n" + "<Value>" + idpAlias + "</Value>\n" + "</Attribute><Attribute name=\"nameIdFormat"
				+ "\">\n" + "<Value>" + "</Value>\n" + "</Attribute><Attribute name=\"nameIdAttribute" + "\">\n"
				+ "<Value>" + "</Value>\n" + "</Attribute><Attribute name=\"nameIncludesDomain" + "\">\n" + "<Value>"
				+ "</Value>\n" + "</Attribute><Attribute name=\"domainAttribute" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"upnDomain" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"signingCertAlias" + "\">\n" + "<Value>" +idpSCertAlias+ "</Value>\n"
				+ "</Attribute><Attribute name=\"assertionNotBeforeTimeSkew" + "\">\n" + "<Value>" + "600</Value>\n"
				+ "</Attribute><Attribute name=\"assertionEffectiveTime" + "\">\n" + "<Value>" + "600</Value>\n"
				+ "</Attribute><Attribute name=\"idpAuthncontextMapper" + "\">\n" + "<Value>"
				+ "com.sun.identity.wsfederation.plugins.DefaultIDPAuthenticationMethodMapper</Value>\n"
				+ "</Attribute><Attribute name=\"idpAccountMapper" + "\">\n" + "<Value>"
				+ "com.sun.identity.wsfederation.plugins.DefaultIDPAccountMapper</Value>\n"
				+ "</Attribute><Attribute name=\"idpAttributeMapper" + "\">\n" + "<Value>"
				+ "com.sun.identity.wsfederation.plugins.DefaultIDPAttributeMapper</Value>\n"
				+ "</Attribute><Attribute name=\"attributeMap" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"cotlist" + "\">\n" + "<Value>"
				+ "</Value></Attribute>\n</IDPSSOConfig>");

		System.out.println("------------------------");
		System.out.println(buff.toString());
		System.out.println("------------------------");

	}

	private static void buildWSFEDServiceProvider(StringBuffer buff, String spAlias, String url,
			EntityProviderDescriptor entityProviderDescriptor) {

		String spSCertAlias = entityProviderDescriptor.getSigningCertificateAlias();
		String spECertAlias = entityProviderDescriptor.getEncryptionCertificateAlias();

		if (spSCertAlias == null) {
			spSCertAlias = "";
		}
		if (spECertAlias == null) {
			spECertAlias = "";
		}

		buff.append("<SPSSOConfig metaAlias=\"" + spAlias + "\">\n" + "        " + "<Attribute name=\"displayName"
				+ "\">\n" + "<Value>" + spAlias + "</Value>\n" + "</Attribute><Attribute name=\"AccountRealmSelection"
				+ "\">\n" + "<Value>" + "cookie</Value>\n" + "</Attribute><Attribute name=\"HomeRealmDiscoveryService"
				+ "\">\n" + "<Value>" + url + "/RealmSelection/metaAlias" + spAlias + "</Value>\n"
				+ "</Attribute><Attribute name=\"signingCertAlias" + "\">\n" + "<Value>" +spSCertAlias+ "</Value>\n"
				+ "</Attribute><Attribute name=\"assertionEffectiveTime" + "\">\n" + "<Value>" + "600</Value>\n"
				+ "</Attribute><Attribute name=\"spAttributeMapper" + "\">\n"
				+ "<Value>com.sun.identity.wsfederation.plugins.DefaultSPAttributeMapper</Value>"
				+ "</Attribute><Attribute name=\"spAuthncontextMapper" + "\">\n"
				+ "<Value>com.sun.identity.saml2.plugins.DefaultSPAuthnContextMapper</Value>"
				+ "</Attribute><Attribute name=\"spAuthncontextClassrefMapping" + "\">\n" + "<Value>"
				+ "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport|0|default</Value>\n"
				+ "</Attribute><Attribute name=\"spAuthncontextComparisonType" + "\">\n" + "<Value>"
				+ "exact</Value>\n" + "</Attribute><Attribute name=\"attributeMap" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"saml2AuthModuleName" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"defaultRelayState" + "\">\n" + "<Value>" + "</Value>\n"
				+ "</Attribute><Attribute name=\"assertionTimeSkew" + "\">\n" + "<Value>" + "300</Value>\n"
				+ "</Attribute><Attribute name=\"assertionCacheEnabled" + "\">\n" + "<Value>" + "true</Value>\n"
				+ "</Attribute><Attribute name=\"wantAssertionSigned" + "\">\n" + "<Value>" + "true</Value>\n"
				+ "</Attribute><Attribute name=\"cotlist" + "\">\n" + "<Value>" + "</Value></Attribute>\n"
				+ "\n</SPSSOConfig>");
	}

	private static EntityProviderDescriptor getEntityProviderDescriptorByType(String type,
			Map<String, EntityProviderDescriptor> descriptorMap, boolean forImport) {
		Iterator<EntityProviderDescriptor> iterator = descriptorMap.values().iterator();
		while (iterator.hasNext()) {
			EntityProviderDescriptor entityProviderDescriptor = iterator.next();
			String typeDescriptor;
			if (forImport) {
				typeDescriptor = entityProviderDescriptor.getEntityTypeKey();
			} else {
				typeDescriptor = entityProviderDescriptor.getType();
			}
			if (type.equals(typeDescriptor)) {
				return entityProviderDescriptor;
			}
		}
		return null;
	}

	private static String getHostURL() {
		String protocol = SystemPropertiesManager.get(Constants.AM_SERVER_PROTOCOL);
		String host = SystemPropertiesManager.get(Constants.AM_SERVER_HOST);
		String port = SystemPropertiesManager.get(Constants.AM_SERVER_PORT);
		String deploymentURI = SystemPropertiesManager.get(Constants.AM_SERVICES_DEPLOYMENT_DESCRIPTOR);
		return protocol + "://" + host + ":" + port + deploymentURI;
	}

	/**
	 * Created default XML data.
	 * 
	 * @param entityID
	 *            - the entity ID.
	 * @param realm
	 *            - the realm.
	 * @param url
	 *            - the host URL.
	 * @param descriptorMap
	 *            - the description map values.
	 * @return - the default XML data.
	 * @throws SAML2MetaException
	 *             - when something wrong happens.
	 */
	public static String createDefaultMetadataTemplate(String entityID, String realm, String url,
			Map<String, EntityProviderDescriptor> descriptorMap) throws SAML2MetaException {
		StringBuffer buff = new StringBuffer();
		if (url == null) {
			url = getHostURL();
		}
		buff.append("<Federation\n" + "    xmlns=\"http://schemas.xmlsoap.org/ws/2006/12/federation\"\n"
				+ "    FederationID=\"" + entityID + "\">\n");
		buff.append("<TokenIssuerName>" + entityID + "</TokenIssuerName>");
		buff.append("<TokenIssuerEndpoint>" + "<ns1:Address xmlns:ns1=\"http://www.w3.org/2005/08/addressing\">" + url
				+ "/WSFederationServlet/metaAlias" + entityID + "</ns1:Address>" + "</TokenIssuerEndpoint>");
		buff.append("<TokenTypesOffered><TokenType Uri=\"urn:oasis:names:tc:SAML:1.1\" /></TokenTypesOffered>");
		buff.append("<UriNamedClaimTypesOffered><ClaimType Uri=\"http://schemas.xmlsoap.org/claims/UPN\"><DisplayName>UPN</DisplayName></ClaimType></UriNamedClaimTypesOffered>");
		buff.append("<TokenIssuerName>" + entityID + "</TokenIssuerName>");
		buff.append("<TokenIssuerEndpoint><ns2:Address xmlns:ns2=\"http://www.w3.org/2005/08/addressing\">" + url
				+ "/WSFederationServlet/metaAlias/qwqw</ns2:Address></TokenIssuerEndpoint>");
		buff.append("<SingleSignOutNotificationEndpoint><ns3:Address xmlns:ns3=\"http://www.w3.org/2005/08/addressing\">"
				+ url + "/WSFederationServlet/metaAlias/qwqw</ns3:Address></SingleSignOutNotificationEndpoint>");

		buff.append("</Federation>\n");
		return buff.toString();
	}

	/**
	 * Creates default XML data.
	 * 
	 * @param entityID
	 *            - the entity ID.
	 * @param realm
	 *            - the realm.
	 * @param url
	 *            - the host URL.
	 * @param hosted
	 *            - whether its hosted or not.
	 * @param descriptorMap
	 *            - the descriptor map values.
	 * @param forImport
	 *            - true if create imported entity provider
	 * @return - the default XML data.
	 */
	public static String createDefaultConfigDataTemplate(String entityID, String realm, String url, boolean hosted,
			Map<String, EntityProviderDescriptor> descriptorMap, boolean forImport) {
		if (url == null) {
			url = getHostURL();
		}
		StringBuffer buff = new StringBuffer();
		String strHosted = (hosted) ? "true" : "false";
		buff.append("<FederationConfig xmlns=\"urn:sun:fm:wsfederation:1.0:federationconfig\"\n" + "     hosted=\""
				+ strHosted + "\"\n" + "    FederationID=\"" + entityID + "\">\n\n");

		EntityProviderDescriptor entityProviderDescriptor = getEntityProviderDescriptorByType(
				EntityProviderDescriptor.WSFED_IDENTITY_PROVIDER, descriptorMap, forImport);
		if (entityProviderDescriptor != null) {
			buildWSFEDIdentityProvider(buff, entityProviderDescriptor.getMetaAliasWithSlash(), url,
					entityProviderDescriptor);
		}

		entityProviderDescriptor = getEntityProviderDescriptorByType(EntityProviderDescriptor.WSFED_SERVICE_PROVIDER,
				descriptorMap, forImport);
		if (entityProviderDescriptor != null) {
			buildWSFEDServiceProvider(buff, entityProviderDescriptor.getMetaAliasWithSlash(), url,
					entityProviderDescriptor);
		}

		buff.append("</FederationConfig>\n");
		return buff.toString();
	}
}
