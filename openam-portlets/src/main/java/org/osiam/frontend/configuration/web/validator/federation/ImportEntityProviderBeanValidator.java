/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.federation;

import org.osiam.frontend.configuration.domain.federation.ImportEntityProviderBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the ImportEntityProviderBean object.
 * <p>
 * MetadataFile can not be null.<br>
 * </p>
 * 
 */
@Component("importEntityProviderBeanValidator")
public class ImportEntityProviderBeanValidator implements Validator {

	@Override
	public boolean supports(Class<?> klass) {
		return ImportEntityProviderBean.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ImportEntityProviderBean object = (ImportEntityProviderBean) target;

		if ((object.getMetadataFileLocation() == null || object.getMetadataFileLocation().getSize() <= Long.valueOf(0))
				&& (object.getMetadataFileURLLocation() == null || "".equals(object.getMetadataFileURLLocation()))) {
			errors.rejectValue("metadataFileLocationType", "NotNull");
		} else {
			if (FederationConstants.LOCATION_TYPE_FILE.equals(object.getMetadataFileLocationType())
					&& (object.getMetadataFileLocation() == null || object.getMetadataFileLocation().getSize() <= Long
							.valueOf(0))) {
				errors.rejectValue("metadataFileLocationType", "NotNull");
			}

			if (FederationConstants.LOCATION_TYPE_URL.equals(object.getMetadataFileLocationType())
					&& (object.getMetadataFileURLLocation() == null || "".equals(object.getMetadataFileURLLocation()))) {
				errors.rejectValue("metadataFileLocationType", "NotNull");
			}
		}
	}
}
