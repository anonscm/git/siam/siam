/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.CollectionUtils;

/**
 * 
 * Encapsulates data for an entity provider.
 * 
 */
public class CircleOfTrust implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1)
	private String name;
	private String idffWriterServiceURL;
	private String idffReaderServiceURL;
	private String saml2WriterServiceURL;
	private String saml2ReaderServiceURL;
	private String description;
	private List<String> trustedProviders = new ArrayList<String>();
	private String realm;
	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdffWriterServiceURL() {
		return idffWriterServiceURL;
	}

	public void setIdffWriterServiceURL(String idffWriterServiceURL) {
		this.idffWriterServiceURL = idffWriterServiceURL;
	}

	public String getIdffReaderServiceURL() {
		return idffReaderServiceURL;
	}

	public void setIdffReaderServiceURL(String idffReaderServiceURL) {
		this.idffReaderServiceURL = idffReaderServiceURL;
	}

	public String getSaml2WriterServiceURL() {
		return saml2WriterServiceURL;
	}

	public void setSaml2WriterServiceURL(String saml2WriterServiceURL) {
		this.saml2WriterServiceURL = saml2WriterServiceURL;
	}

	public String getSaml2ReaderServiceURL() {
		return saml2ReaderServiceURL;
	}

	public void setSaml2ReaderServiceURL(String saml2ReaderServiceURL) {
		this.saml2ReaderServiceURL = saml2ReaderServiceURL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getTrustedProviders() {
		return trustedProviders;
	}

	public void setTrustedProviders(List<String> trustedProviders) {
		this.trustedProviders = trustedProviders;
	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static CircleOfTrust fromMap(String name, Map<String, Set<String>> attributes) {
		CircleOfTrust bean = new CircleOfTrust();
		bean.setName(name);
		bean.setStatus(CollectionUtils.stringFromSet(attributes.get(FederationConstants.STATUS_KEY)));
		bean.setIdffReaderServiceURL(CollectionUtils.stringFromSet(attributes
				.get(FederationConstants.IDFF_READER_SERVICE_URL_KEY)));
		bean.setIdffWriterServiceURL(CollectionUtils.stringFromSet(attributes
				.get(FederationConstants.IDFF_WRITER_SERVICE_URL_KEY)));
		bean.setSaml2ReaderServiceURL(CollectionUtils.stringFromSet(attributes
				.get(FederationConstants.SAML2_READER_SERVICE_URL_KEY)));
		bean.setSaml2WriterServiceURL(CollectionUtils.stringFromSet(attributes
				.get(FederationConstants.SAML2_WRITER_SERVICE_URL_KEY)));
		bean.setDescription(CollectionUtils.stringFromSet(attributes.get(FederationConstants.DESCRIPTION_KEY)));
		bean.setTrustedProviders(CollectionUtils.listFromSet(attributes.get(FederationConstants.TRUSTED_PROVIDERS_KEY)));

		return bean;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(FederationConstants.STATUS_KEY, CollectionUtils.asSet(getStatus()));
		map.put(FederationConstants.IDFF_READER_SERVICE_URL_KEY, CollectionUtils.asSet(getIdffReaderServiceURL()));
		map.put(FederationConstants.IDFF_WRITER_SERVICE_URL_KEY, CollectionUtils.asSet(getIdffWriterServiceURL()));
		map.put(FederationConstants.SAML2_READER_SERVICE_URL_KEY, CollectionUtils.asSet(getSaml2ReaderServiceURL()));
		map.put(FederationConstants.SAML2_WRITER_SERVICE_URL_KEY, CollectionUtils.asSet(getSaml2WriterServiceURL()));
		map.put(FederationConstants.DESCRIPTION_KEY, CollectionUtils.asSet(getDescription()));
		map.put(FederationConstants.TRUSTED_PROVIDERS_KEY, CollectionUtils.asSet(getTrustedProviders()));

		return map;
	}

}
