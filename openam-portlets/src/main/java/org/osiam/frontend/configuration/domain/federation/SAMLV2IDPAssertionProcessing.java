/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Encapsulates all data for the SAMLV2 IDP Assertion Processing feature.
 * 
 */
public class SAMLV2IDPAssertionProcessing {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2IDPAssertionProcessing.class);

	private String attributeMapper;
	private List<String> attributeMapCurrentValues = new ArrayList<String>();

	private String accountMapper;

	private String localConfigurationAuthURL;
	private String localConfigurationExternalApplicationLogoutURL;

	public String getAttributeMapper() {
		return attributeMapper;
	}

	public void setAttributeMapper(String attributeMapper) {
		this.attributeMapper = attributeMapper;
	}

	public List<String> getAttributeMapCurrentValues() {
		return attributeMapCurrentValues;
	}

	public void setAttributeMapCurrentValues(List<String> attributeMapCurrentValues) {
		this.attributeMapCurrentValues = attributeMapCurrentValues;
	}

	public String getAccountMapper() {
		return accountMapper;
	}

	public void setAccountMapper(String accountMapper) {
		this.accountMapper = accountMapper;
	}

	public String getLocalConfigurationAuthURL() {
		return localConfigurationAuthURL;
	}

	public void setLocalConfigurationAuthURL(String localConfigurationAuthURL) {
		this.localConfigurationAuthURL = localConfigurationAuthURL;
	}

	public String getLocalConfigurationExternalApplicationLogoutURL() {
		return localConfigurationExternalApplicationLogoutURL;
	}

	public void setLocalConfigurationExternalApplicationLogoutURL(String localConfigurationExternalApplicationLogoutURL) {
		this.localConfigurationExternalApplicationLogoutURL = localConfigurationExternalApplicationLogoutURL;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2IDPAssertionProcessing fromMap(Map<String, Set<String>> map)
			throws UnsupportedEncodingException {

		SAMLV2IDPAssertionProcessing bean = new SAMLV2IDPAssertionProcessing();

		Document entityConfigDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		NodeList childList = entityConfigDocument.getChildNodes();
		if (childList.getLength() != 1) {
			throw new IllegalArgumentException(String.format("The contains contains %d node . Expected 1",
					childList.getLength()));
		}
		Node rootNode = entityConfigDocument.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.IDPSSO_CONFIG_NODE);
		if (configNodeSet.size() != 1) {
			throw new IllegalArgumentException(String.format("The root node contains %d node . Expected 1",
					configNodeSet.size()));
		}

		Node configNode = configNodeSet.iterator().next();
		NodeList configChildNodeList = configNode.getChildNodes();

		for (int i = 0; i < configChildNodeList.getLength(); i++) {
			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals(FederationConstants.ATTRIBUTE_MAPPER)) {
				bean.setAttributeMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ATTRIBUTE_MAP_CURRENT_VALUES)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setAttributeMapCurrentValues((valuePair.getValueList()));
			} else if (attributeName.equals(FederationConstants.ACCOUNT_MAPPER)) {
				bean.setAccountMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOCAL_CONFIGURATION_AUTH_URL)) {
				bean.setLocalConfigurationAuthURL(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOCAL_CONFIGURATION_EXTERNAL_APPLICATION_LOGOUT_URL)) {
				bean.setLocalConfigurationExternalApplicationLogoutURL(XmlUtil.getValueFromAttributeValue(node));
			}
		}

		return bean;
	}

	

}
