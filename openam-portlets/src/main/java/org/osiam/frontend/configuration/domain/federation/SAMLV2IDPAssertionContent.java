/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Encapsulates data for a SAMLV2 IDP Assertion Content bean.
 */
public class SAMLV2IDPAssertionContent {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2IDPAssertionContent.class);

	private boolean authenticationRequestSigned;
	private boolean artifactResolveSigned;
	private boolean logoutRequestSigned;
	private boolean logoutResponseSigned;
	private boolean manageNameIDRequestSigned;
	private boolean manageNameIDResponseSigned;
	private boolean nameIDEncryptionSigned;

	private String certificateAliasesSigning;
	private String certificateAliasesEncryption;
	private Long certificateAliasesKeySize;
	private String certificateAliasesAlgorithm;

	private String nameIDFormatNameIDFormatList;

	private String wantNameIDEncrypted;

	private List<String> nameIDValueMapCurrentValues = new ArrayList<String>();
	private List<String> nameIDValueMapFormatValues = new ArrayList<String>();

	private String authenticationContextMapper;
	private String defaultAuthenticationContext;
	private List<SAMLv2IDPAuthContext> samlv2AuthContextList = new ArrayList<SAMLv2IDPAuthContext>();

	private Long notBeforeTimeSkew;
	private Long effectiveTime;

	private boolean basicauthenticationsettingforSoapbasedbindingEnabled;
	private String basicauthenticationsettingforSoapbasedbindingEnabledUserName;
	private String basicauthenticationsettingforSoapbasedbindingEnabledPassword;

	private boolean assertionCacheEnabled;
	private boolean discoveryBootstrappingEnabled;

	private List<String> relayStateUrlListCurrentValues;

	public boolean getAuthenticationRequestSigned() {
		return authenticationRequestSigned;
	}

	public void setAuthenticationRequestSigned(boolean authenticationRequestSigned) {
		this.authenticationRequestSigned = authenticationRequestSigned;
	}

	public boolean getArtifactResolveSigned() {
		return artifactResolveSigned;
	}

	public void setArtifactResolveSigned(boolean artifactResolveSigned) {
		this.artifactResolveSigned = artifactResolveSigned;
	}

	public boolean getLogoutRequestSigned() {
		return logoutRequestSigned;
	}

	public void setLogoutRequestSigned(boolean logoutRequestSigned) {
		this.logoutRequestSigned = logoutRequestSigned;
	}

	public boolean getLogoutResponseSigned() {
		return logoutResponseSigned;
	}

	public void setLogoutResponseSigned(boolean logoutResponseSigned) {
		this.logoutResponseSigned = logoutResponseSigned;
	}

	public boolean getManageNameIDRequestSigned() {
		return manageNameIDRequestSigned;
	}

	public String getWantNameIDEncrypted() {
		return wantNameIDEncrypted;
	}

	public void setWantNameIDEncrypted(String wantNameIDEncrypted) {
		this.wantNameIDEncrypted = wantNameIDEncrypted;
	}

	public void setManageNameIDRequestSigned(boolean manageNameIDRequestSigned) {
		this.manageNameIDRequestSigned = manageNameIDRequestSigned;
	}

	public boolean getManageNameIDResponseSigned() {
		return manageNameIDResponseSigned;
	}

	public void setManageNameIDResponseSigned(boolean manageNameIDResponseSigned) {
		this.manageNameIDResponseSigned = manageNameIDResponseSigned;
	}

	public boolean getNameIDEncryptionSigned() {
		return nameIDEncryptionSigned;
	}

	public void setNameIDEncryptionSigned(boolean nameIDEncryptionSigned) {
		this.nameIDEncryptionSigned = nameIDEncryptionSigned;
	}

	public String getCertificateAliasesSigning() {
		return certificateAliasesSigning;
	}

	public void setCertificateAliasesSigning(String certificateAliasesSigning) {
		this.certificateAliasesSigning = certificateAliasesSigning;
	}

	public String getCertificateAliasesEncryption() {
		return certificateAliasesEncryption;
	}

	public void setCertificateAliasesEncryption(String certificateAliasesEncryption) {
		this.certificateAliasesEncryption = certificateAliasesEncryption;
	}

	public Long getCertificateAliasesKeySize() {
		return certificateAliasesKeySize;
	}

	public void setCertificateAliasesKeySize(Long certificateAliasesKeySize) {
		this.certificateAliasesKeySize = certificateAliasesKeySize;
	}

	public String getCertificateAliasesAlgorithm() {
		return certificateAliasesAlgorithm;
	}

	public void setCertificateAliasesAlgorithm(String certificateAliasesAlgorithm) {
		this.certificateAliasesAlgorithm = certificateAliasesAlgorithm;
	}

	public String getNameIDFormatNameIDFormatList() {
		return nameIDFormatNameIDFormatList;
	}

	public void setNameIDFormatNameIDFormatList(String nameIDFormatNameIDFormatList) {
		this.nameIDFormatNameIDFormatList = nameIDFormatNameIDFormatList;
	}

	public List<String> getNameIDValueMapCurrentValues() {
		return nameIDValueMapCurrentValues;
	}

	public void setNameIDValueMapCurrentValues(List<String> nameIDValueMapCurrentValues) {
		this.nameIDValueMapCurrentValues = nameIDValueMapCurrentValues;
	}

	public List<String> getNameIDValueMapFormatValues() {
		return nameIDValueMapFormatValues;
	}

	public void setNameIDValueMapFormatValues(List<String> nameIDValueMapFormatValues) {
		this.nameIDValueMapFormatValues = nameIDValueMapFormatValues;
	}

	public String getAuthenticationContextMapper() {
		return authenticationContextMapper;
	}

	public void setAuthenticationContextMapper(String authenticationContextMapper) {
		this.authenticationContextMapper = authenticationContextMapper;
	}

	public String getDefaultAuthenticationContext() {
		return defaultAuthenticationContext;
	}

	public void setDefaultAuthenticationContext(String defaultAuthenticationContext) {
		this.defaultAuthenticationContext = defaultAuthenticationContext;
	}

	public Long getNotBeforeTimeSkew() {
		return notBeforeTimeSkew;
	}

	public void setNotBeforeTimeSkew(Long notBeforeTimeSkew) {
		this.notBeforeTimeSkew = notBeforeTimeSkew;
	}

	public Long getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(Long effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public boolean getBasicauthenticationsettingforSoapbasedbindingEnabled() {
		return basicauthenticationsettingforSoapbasedbindingEnabled;
	}

	public void setBasicauthenticationsettingforSoapbasedbindingEnabled(
			boolean basicauthenticationsettingforSoapbasedbindingEnabled) {
		this.basicauthenticationsettingforSoapbasedbindingEnabled = basicauthenticationsettingforSoapbasedbindingEnabled;
	}

	public String getBasicauthenticationsettingforSoapbasedbindingEnabledUserName() {
		return basicauthenticationsettingforSoapbasedbindingEnabledUserName;
	}

	public void setBasicauthenticationsettingforSoapbasedbindingEnabledUserName(
			String basicauthenticationsettingforSoapbasedbindingEnabledUserName) {
		this.basicauthenticationsettingforSoapbasedbindingEnabledUserName = basicauthenticationsettingforSoapbasedbindingEnabledUserName;
	}

	public String getBasicauthenticationsettingforSoapbasedbindingEnabledPassword() {
		return basicauthenticationsettingforSoapbasedbindingEnabledPassword;
	}

	public void setBasicauthenticationsettingforSoapbasedbindingEnabledPassword(
			String basicauthenticationsettingforSoapbasedbindingEnabledPassword) {
		this.basicauthenticationsettingforSoapbasedbindingEnabledPassword = basicauthenticationsettingforSoapbasedbindingEnabledPassword;
	}

	public boolean getAssertionCacheEnabled() {
		return assertionCacheEnabled;
	}

	public void setAssertionCacheEnabled(boolean assertionCacheEnabled) {
		this.assertionCacheEnabled = assertionCacheEnabled;
	}

	public boolean getDiscoveryBootstrappingEnabled() {
		return discoveryBootstrappingEnabled;
	}

	public void setDiscoveryBootstrappingEnabled(boolean discoveryBootstrappingEnabled) {
		this.discoveryBootstrappingEnabled = discoveryBootstrappingEnabled;
	}

	public List<String> getRelayStateUrlListCurrentValues() {
		return relayStateUrlListCurrentValues;
	}

	public void setRelayStateUrlListCurrentValues(List<String> relayStateUrlListCurrentValues) {
		this.relayStateUrlListCurrentValues = relayStateUrlListCurrentValues;
	}

	public List<SAMLv2IDPAuthContext> getSamlv2AuthContextList() {
		return samlv2AuthContextList;
	}

	public void setSamlv2AuthContextList(List<SAMLv2IDPAuthContext> samlv2AuthContextList) {
		this.samlv2AuthContextList = samlv2AuthContextList;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2IDPAssertionContent fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2IDPAssertionContent bean = new SAMLV2IDPAssertionContent();

		Document entityConfig = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNodeEntityConfig = entityConfig.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNodeEntityConfig, FederationConstants.IDPSSO_CONFIG_NODE);
		Node configNode = configNodeSet.iterator().next();

		NodeList configChildNodeList = configNode.getChildNodes();

		for (int i = 0; i < configChildNodeList.getLength(); i++) {
			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}
			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}
			if (attributeName.equals(FederationConstants.NAMEID_VALUE_MAP_CURRENT_VALUES)) {
				bean.setNameIDValueMapCurrentValues(XmlUtil.getAttributeValue(node).getValueList());
			} else if (attributeName.equals(FederationConstants.AUTHENTICATION_REQUEST_SIGNED)) {
				bean.setAuthenticationRequestSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ARTIFACT_RESOLVE_SIGNED)) {
				bean.setArtifactResolveSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOGOUT_REQUEST_SIGNED)) {
				bean.setLogoutRequestSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.LOGOUT_RESPONSE_SIGNED)) {
				bean.setLogoutResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.MANAGE_NAME_ID_REQUEST_SIGNED)) {
				bean.setManageNameIDRequestSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.MANAGE_NAME_ID_RESPONSE_SIGNED)) {
				bean.setManageNameIDResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.NAMEID_ENCRYPTION_SIGNED)) {
				bean.setWantNameIDEncrypted(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_SIGNING)) {
				bean.setCertificateAliasesSigning(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION)) {
				bean.setCertificateAliasesEncryption(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.NAMEID_FORMAT_NAMEID_FORMAT_LIST)) {
				bean.setNameIDFormatNameIDFormatList(XmlUtil.getValueFromAttributeValue(node));

			} else if (attributeName.equals(FederationConstants.AUTHENTICATION_CONTEXT_MAPPER)) {
				bean.setAuthenticationContextMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.NOT_BEFORE_TIME_SKEW)) {
				bean.setNotBeforeTimeSkew(XmlUtil.getLongValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.EFFECTIVE_TIME)) {
				bean.setEffectiveTime(XmlUtil.getLongValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED)) {
				bean.setBasicauthenticationsettingforSoapbasedbindingEnabled(XmlUtil
						.getBooleanValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_USER_NAME)) {
				bean.setBasicauthenticationsettingforSoapbasedbindingEnabledUserName(XmlUtil
						.getValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_PASSWORD)) {
				bean.setBasicauthenticationsettingforSoapbasedbindingEnabledPassword(XmlUtil
						.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ASSERTION_CACHE_ENABLED)) {
				bean.setAssertionCacheEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.DISCOVERY_BOOTSTRAPPING_ENABLED)) {
				bean.setDiscoveryBootstrappingEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.IDP_AUTHCONTEXT_CLASSREF_MAPPING)) {
				List<String> valueList = XmlUtil.getAttributeValue(node).getValueList();
				List<SAMLv2IDPAuthContext> contextList = new ArrayList<SAMLv2IDPAuthContext>();
				for (String s : valueList) {
					contextList.add(SAMLv2IDPAuthContext.fromString(s));
				}

				bean.setSamlv2AuthContextList(contextList);
			}

		}

		Document metadataDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY)));
		if (metadataDocument == null) {
			return bean;
		}
		Node rootNodeMetadata = metadataDocument.getFirstChild();
		Node descriptorNode = XmlUtil.getChildNode(rootNodeMetadata, FederationConstants.IDPSSO_DESCRIPTOR_NODE);
		String authRequestSigned = XmlUtil.getNodeAttributeValue(descriptorNode, "WantAuthnRequestsSigned");
		bean.setAuthenticationRequestSigned(Boolean.parseBoolean(authRequestSigned));
		Set<Node> singleSignOnServiceNodeSet = XmlUtil.getChildNodes(descriptorNode,
				FederationConstants.NAME_ID_FORMAT_NODE);
		List<String> nameIdList = new ArrayList<String>();
		for (Node node : singleSignOnServiceNodeSet) {
			nameIdList.add(XmlUtil.getTextValueFromNode(node));
		}
		bean.setNameIDValueMapFormatValues(nameIdList);

		Set<Node> keyDescriptorNodeSet = XmlUtil.getChildNodes(descriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE);
		for (Node keyDescriptorNode : keyDescriptorNodeSet) {

			if (XmlUtil.getNodeAttributeValue(keyDescriptorNode, FederationConstants.USE_ATTRIBUTE).equals(
					FederationConstants.ENCRYPTION)) {

				Node encyptionMethodNode = XmlUtil.getChildNode(keyDescriptorNode,
						FederationConstants.ENCRYPTION_METHOD_NODE);

				bean.setCertificateAliasesAlgorithm(XmlUtil.getNodeAttributeValue(encyptionMethodNode,
						FederationConstants.ALGORITHM_ATTRIBUTE));

				Node keySizeNode = XmlUtil.getChildNodeWithPrefix(encyptionMethodNode,
						FederationConstants.KEY_SIZE_NODE);

				bean.setCertificateAliasesKeySize(new Long(XmlUtil.getTextValueFromNode(keySizeNode)));
			}

		}

		return bean;
	}

}
