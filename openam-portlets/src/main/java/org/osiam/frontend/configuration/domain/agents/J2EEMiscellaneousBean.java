/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a j2ee miscellaneous bean.
 * 
 */
public class J2EEMiscellaneousBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String localeLanguage;
	private String localeCountry;
	private boolean portCheckEnable;
	private String portCheckFile;
	private List<String> portCheckSetting = new ArrayList<String>();
	private List<String> bypassPrincipalList = new ArrayList<String>();
	private String encryptionProvider;
	private boolean ignorePathInfoinRequestURL;
	private String gotoParameterName;
	private boolean legacyUserAgentSupportEnable;
	private List<String> legacyUserAgentList = new ArrayList<String>();
	private String legacyUserAgentRedirectURI;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocaleLanguage() {
		return localeLanguage;
	}

	public void setLocaleLanguage(String localeLanguage) {
		this.localeLanguage = localeLanguage;
	}

	public String getLocaleCountry() {
		return localeCountry;
	}

	public void setLocaleCountry(String localeCountry) {
		this.localeCountry = localeCountry;
	}

	public boolean getPortCheckEnable() {
		return portCheckEnable;
	}

	public void setPortCheckEnable(boolean portCheckEnable) {
		this.portCheckEnable = portCheckEnable;
	}

	public String getPortCheckFile() {
		return portCheckFile;
	}

	public void setPortCheckFile(String portCheckFile) {
		this.portCheckFile = portCheckFile;
	}

	public List<String> getPortCheckSetting() {
		return portCheckSetting;
	}

	public void setPortCheckSetting(List<String> portCheckSetting) {
		this.portCheckSetting = portCheckSetting;
	}

	public List<String> getBypassPrincipalList() {
		return bypassPrincipalList;
	}

	public void setBypassPrincipalList(List<String> bypassPrincipalList) {
		this.bypassPrincipalList = bypassPrincipalList;
	}

	public String getEncryptionProvider() {
		return encryptionProvider;
	}

	public void setEncryptionProvider(String encryptionProvider) {
		this.encryptionProvider = encryptionProvider;
	}

	public boolean getIgnorePathInfoinRequestURL() {
		return ignorePathInfoinRequestURL;
	}

	public void setIgnorePathInfoinRequestURL(boolean ignorePathInfoinRequestURL) {
		this.ignorePathInfoinRequestURL = ignorePathInfoinRequestURL;
	}

	public String getGotoParameterName() {
		return gotoParameterName;
	}

	public void setGotoParameterName(String gotoParameterName) {
		this.gotoParameterName = gotoParameterName;
	}

	public boolean getLegacyUserAgentSupportEnable() {
		return legacyUserAgentSupportEnable;
	}

	public void setLegacyUserAgentSupportEnable(boolean legacyUserAgentSupportEnable) {
		this.legacyUserAgentSupportEnable = legacyUserAgentSupportEnable;
	}

	public List<String> getLegacyUserAgentList() {
		return legacyUserAgentList;
	}

	public void setLegacyUserAgentList(List<String> legacyUserAgentList) {
		this.legacyUserAgentList = legacyUserAgentList;
	}

	public String getLegacyUserAgentRedirectURI() {
		return legacyUserAgentRedirectURI;
	}

	public void setLegacyUserAgentRedirectURI(String legacyUserAgentRedirectURI) {
		this.legacyUserAgentRedirectURI = legacyUserAgentRedirectURI;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static J2EEMiscellaneousBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EEMiscellaneousBean bean = new J2EEMiscellaneousBean();
		bean.setName(name);
		bean.setLocaleLanguage(stringFromSet(attributes.get(AgentAttributesConstants.LOCALE_LANGUAGE)));
		bean.setLocaleCountry(stringFromSet(attributes.get(AgentAttributesConstants.LOCALE_COUNTRY)));
		bean.setPortCheckEnable(booleanFromSet(attributes.get(AgentAttributesConstants.PORT_CHECK_ENABLE)));
		bean.setPortCheckFile(stringFromSet(attributes.get(AgentAttributesConstants.PORT_CHECK_FILE)));
		bean.setPortCheckSetting(listFromSet(attributes.get(AgentAttributesConstants.PORT_CHECK_SETTING_CURRENT_VALUES)));
		bean.setBypassPrincipalList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.BYPASS_PRINCIPAL_LIST_CURRENT_VALUES))));
		bean.setEncryptionProvider(stringFromSet(attributes.get(AgentAttributesConstants.ENCRYPTION_PROVIDER)));
		bean.setIgnorePathInfoinRequestURL(booleanFromSet(attributes
				.get(AgentAttributesConstants.IGNORE_PATH_INFO_IN_REQUEST_URL)));
		bean.setGotoParameterName(stringFromSet(attributes.get(AgentAttributesConstants.GOTO_PARAMETER_NAME)));
		bean.setLegacyUserAgentSupportEnable(booleanFromSet(attributes
				.get(AgentAttributesConstants.LEGACY_USER_AGENT_SUPPORT_ENABLE)));
		bean.setLegacyUserAgentList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.LEGACY_USER_AGENT_LIST_CURRENT_VALUES))));
		bean.setLegacyUserAgentRedirectURI(stringFromSet(attributes
				.get(AgentAttributesConstants.LEGACY_USER_AGENT_REDIRECT_URI)));

		return bean;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.LOCALE_LANGUAGE, asSet(getLocaleLanguage()));
		map.put(AgentAttributesConstants.LOCALE_COUNTRY, asSet(getLocaleCountry()));
		map.put(AgentAttributesConstants.PORT_CHECK_ENABLE, asSet(getPortCheckEnable()));
		map.put(AgentAttributesConstants.PORT_CHECK_FILE, asSet(getPortCheckFile()));

		map.put(AgentAttributesConstants.PORT_CHECK_SETTING_CURRENT_VALUES, asSet(getPortCheckSetting()));

		map.put(AgentAttributesConstants.BYPASS_PRINCIPAL_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getBypassPrincipalList())));
		map.put(AgentAttributesConstants.ENCRYPTION_PROVIDER, asSet(getEncryptionProvider()));
		map.put(AgentAttributesConstants.IGNORE_PATH_INFO_IN_REQUEST_URL, asSet(getIgnorePathInfoinRequestURL()));
		map.put(AgentAttributesConstants.GOTO_PARAMETER_NAME, asSet(getGotoParameterName()));
		map.put(AgentAttributesConstants.LEGACY_USER_AGENT_SUPPORT_ENABLE, asSet(getLegacyUserAgentSupportEnable()));
		map.put(AgentAttributesConstants.LEGACY_USER_AGENT_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getLegacyUserAgentList())));
		map.put(AgentAttributesConstants.LEGACY_USER_AGENT_REDIRECT_URI, asSet(getLegacyUserAgentRedirectURI()));

		return map;
	}

}
