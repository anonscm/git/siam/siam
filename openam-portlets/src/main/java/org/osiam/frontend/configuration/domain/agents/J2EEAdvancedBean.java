/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * 
 * Encapsulates data for a j2ee advanced bean.
 * 
 */
public class J2EEAdvancedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String clientIPAddressHeader;
	private String clientHostnameHeader;
	private boolean webServiceEnable;
	private List<String> webServiceEndPoints = new ArrayList<String>();
	private boolean webServiceProcessGETEnable;
	private String webServiceAuthenticator;
	private String webServiceResponseProcessor;
	private String webServiceInternalErrorContentFile;
	private String webServiceAuthorizationErrorContentFile;
	private String alternativeAgentHostName;
	private String alternativeAgentPortName;
	private String alternativeAgentProtocol;
	private boolean webAuthenticationAvailable;
	private List<String> customProperties = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientIPAddressHeader() {
		return clientIPAddressHeader;
	}

	public void setClientIPAddressHeader(String clientIPAddressHeader) {
		this.clientIPAddressHeader = clientIPAddressHeader;
	}

	public String getClientHostnameHeader() {
		return clientHostnameHeader;
	}

	public void setClientHostnameHeader(String clientHostnameHeader) {
		this.clientHostnameHeader = clientHostnameHeader;
	}

	public boolean getWebServiceEnable() {
		return webServiceEnable;
	}

	public void setWebServiceEnable(boolean webServiceEnable) {
		this.webServiceEnable = webServiceEnable;
	}

	public List<String> getWebServiceEndPoints() {
		return webServiceEndPoints;
	}

	public void setWebServiceEndPoints(List<String> webServiceEndPoints) {
		this.webServiceEndPoints = webServiceEndPoints;
	}

	public boolean getWebServiceProcessGETEnable() {
		return webServiceProcessGETEnable;
	}

	public void setWebServiceProcessGETEnable(boolean webServiceProcessGETEnable) {
		this.webServiceProcessGETEnable = webServiceProcessGETEnable;
	}

	public String getWebServiceAuthenticator() {
		return webServiceAuthenticator;
	}

	public void setWebServiceAuthenticator(String webServiceAuthenticator) {
		this.webServiceAuthenticator = webServiceAuthenticator;
	}

	public String getWebServiceResponseProcessor() {
		return webServiceResponseProcessor;
	}

	public void setWebServiceResponseProcessor(String webServiceResponseProcessor) {
		this.webServiceResponseProcessor = webServiceResponseProcessor;
	}

	public String getWebServiceInternalErrorContentFile() {
		return webServiceInternalErrorContentFile;
	}

	public void setWebServiceInternalErrorContentFile(String webServiceInternalErrorContentFile) {
		this.webServiceInternalErrorContentFile = webServiceInternalErrorContentFile;
	}

	public String getWebServiceAuthorizationErrorContentFile() {
		return webServiceAuthorizationErrorContentFile;
	}

	public void setWebServiceAuthorizationErrorContentFile(String webServiceAuthorizationErrorContentFile) {
		this.webServiceAuthorizationErrorContentFile = webServiceAuthorizationErrorContentFile;
	}

	public String getAlternativeAgentHostName() {
		return alternativeAgentHostName;
	}

	public void setAlternativeAgentHostName(String alternativeAgentHostName) {
		this.alternativeAgentHostName = alternativeAgentHostName;
	}

	public String getAlternativeAgentPortName() {
		return alternativeAgentPortName;
	}

	public void setAlternativeAgentPortName(String alternativeAgentPortName) {
		this.alternativeAgentPortName = alternativeAgentPortName;
	}

	public String getAlternativeAgentProtocol() {
		return alternativeAgentProtocol;
	}

	public void setAlternativeAgentProtocol(String alternativeAgentProtocol) {
		this.alternativeAgentProtocol = alternativeAgentProtocol;
	}

	public boolean getWebAuthenticationAvailable() {
		return webAuthenticationAvailable;
	}

	public void setWebAuthenticationAvailable(boolean webAuthenticationAvailable) {
		this.webAuthenticationAvailable = webAuthenticationAvailable;
	}

	public List<String> getCustomProperties() {
		return customProperties;
	}

	public void setCustomProperties(List<String> customProperties) {
		this.customProperties = customProperties;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static J2EEAdvancedBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EEAdvancedBean bean = new J2EEAdvancedBean();
		bean.setName(name);
		bean.setClientIPAddressHeader(stringFromSet(attributes.get(AgentAttributesConstants.CLIENT_IP_ADDRESS_HEADER)));
		bean.setClientHostnameHeader(stringFromSet(attributes.get(AgentAttributesConstants.CLIENT_HOSTNAME_HEADER)));
		bean.setWebServiceEnable(booleanFromSet(attributes.get(AgentAttributesConstants.WEB_SERVICE_ENABLE)));
		bean.setWebServiceEndPoints(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_END_POINTS_CURRENT_VALUES))));
		bean.setWebServiceProcessGETEnable(booleanFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_PROCESS_GET_ENABLE)));
		bean.setWebServiceAuthenticator(stringFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_AUTHENTICATOR)));
		bean.setWebServiceResponseProcessor(stringFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_RESPONSE_PROCESSOR)));
		bean.setWebServiceInternalErrorContentFile(stringFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_INTERNAL_ERROR_CONTENT_FILE)));
		bean.setWebServiceAuthorizationErrorContentFile(stringFromSet(attributes
				.get(AgentAttributesConstants.WEB_SERVICE_AUTHORIZATION_ERROR_CONTENT_FILE)));
		bean.setAlternativeAgentHostName(stringFromSet(attributes
				.get(AgentAttributesConstants.ALTERNATIVE_AGENT_HOST_NAME)));
		bean.setAlternativeAgentPortName(stringFromSet(attributes
				.get(AgentAttributesConstants.ALTERNATIVE_AGENT_PORT_NAME)));
		bean.setAlternativeAgentProtocol(stringFromSet(attributes
				.get(AgentAttributesConstants.ALTERNATIVE_AGENT_PROTOCOL)));
		bean.setWebAuthenticationAvailable(booleanFromSet(attributes
				.get(AgentAttributesConstants.WEBAUTHENTICATION_AVAILABLE)));
		bean.setCustomProperties(listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_PROPERTIES_CURRENT_VALUES)));

		return bean;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.CLIENT_IP_ADDRESS_HEADER, asSet(getClientIPAddressHeader()));
		map.put(AgentAttributesConstants.CLIENT_HOSTNAME_HEADER, asSet(getClientHostnameHeader()));

		map.put(AgentAttributesConstants.WEB_SERVICE_ENABLE, asSet(getWebServiceEnable()));

		map.put(AgentAttributesConstants.WEB_SERVICE_END_POINTS_CURRENT_VALUES,
				asSet(addIndexPrefix(getWebServiceEndPoints())));

		map.put(AgentAttributesConstants.WEB_SERVICE_PROCESS_GET_ENABLE, asSet(getWebServiceProcessGETEnable()));

		map.put(AgentAttributesConstants.WEB_SERVICE_AUTHENTICATOR, asSet(getWebServiceAuthenticator()));

		map.put(AgentAttributesConstants.WEB_SERVICE_RESPONSE_PROCESSOR, asSet(getWebServiceResponseProcessor()));

		map.put(AgentAttributesConstants.WEB_SERVICE_INTERNAL_ERROR_CONTENT_FILE,
				asSet(getWebServiceInternalErrorContentFile()));

		map.put(AgentAttributesConstants.WEB_SERVICE_AUTHORIZATION_ERROR_CONTENT_FILE,
				asSet(getWebServiceAuthorizationErrorContentFile()));

		map.put(AgentAttributesConstants.ALTERNATIVE_AGENT_HOST_NAME, asSet(getAlternativeAgentHostName()));

		map.put(AgentAttributesConstants.ALTERNATIVE_AGENT_PORT_NAME, asSet(getAlternativeAgentPortName()));

		map.put(AgentAttributesConstants.ALTERNATIVE_AGENT_PROTOCOL, asSet(getAlternativeAgentProtocol()));

		if (getWebAuthenticationAvailable()) {
			map.put(AgentAttributesConstants.WEBAUTHENTICATION_AVAILABLE, asSet(getWebAuthenticationAvailable()));
		}

		map.put(AgentAttributesConstants.CUSTOM_PROPERTIES_CURRENT_VALUES, asSet(getCustomProperties()));

		return map;
	}

}
