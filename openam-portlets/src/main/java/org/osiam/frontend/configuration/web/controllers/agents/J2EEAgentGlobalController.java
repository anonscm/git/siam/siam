/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EEGlobalBean;
import org.osiam.frontend.configuration.service.AgentsConstants;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentGlobalController shows the edit "J2EE Agent" (Global attributes)
 * form and does request processing of the edit j2ee agent action.
 * 
 */
@Controller("J2EEAgentGlobalController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentGlobalController" })
public class J2EEAgentGlobalController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentGlobalController.class);

	@Autowired
	private AgentsService agentsService;

	@Autowired
	@Qualifier("j2EEGlobalBeanValidator")
	private Validator j2EEGlobalBeanValidator;

	/**
	 * Show edit form for a "J2EE Agent" - global attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param error
	 *            - error on save
	 * @param request
	 *            -RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, String error, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {
			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EEGlobalBean agent = agentsService.getAttributesForJ2EEGlobal(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}

		model.addAttribute("name", name);
		String realm = getCurrentRealm(request);
		model.addAttribute("groupsList",
				agentsService.getGroupsList(getSSOToken(realm, request), realm, Integer.MAX_VALUE));
		model.addAttribute("message", message);
		model.addAttribute("error", error);
		return "agents/j2ee/global/edit";

	}

	/**
	 * Save new value in Agent Root URL For CDSSO list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentRootUrlForCdssoAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAgentRootUrlForCdsso")
	public void doAddAgentRootUrlForCdsso(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String agentRootUrlForCdssoAddValue, ActionRequest request, ActionResponse response) {

		if (agentRootUrlForCdssoAddValue != null && !agentRootUrlForCdssoAddValue.equals("")) {
			j2eeAgent.getAgentRootUrlForCdsso().add(agentRootUrlForCdssoAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Agent Root URL For CDSSO list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentRootUrlForCdssoDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAgentRootUrlForCdsso")
	public void doDeleteAgentRootUrlForCdsso(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String[] agentRootUrlForCdssoDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (agentRootUrlForCdssoDeleteValues != null) {
			for (int i = 0; i < agentRootUrlForCdssoDeleteValues.length; i++) {
				j2eeAgent.getAgentRootUrlForCdsso().remove(agentRootUrlForCdssoDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Agent Filter Mode.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentFilterModeKey
	 *            - {@link String} key to be insert
	 * @param agentFilterModeValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAgentFilterMode")
	public void doAddAgentFilterMode(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String agentFilterModeKey, String agentFilterModeValue, ActionRequest request,
			ActionResponse response) {

		if (agentFilterModeValue != null && !agentFilterModeValue.equals("")) {
			if (agentFilterModeKey != null && !agentFilterModeKey.equals("")) {
				j2eeAgent.getAgentFilterMode().add("[" + agentFilterModeKey + "]=" + agentFilterModeValue);
			} else {
				j2eeAgent.getAgentFilterMode().add(agentFilterModeValue);
			}
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Agent Filter Mode.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param agentFilterModeDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAgentFilterMode")
	public void doDeleteAgentFilterMode(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String[] agentFilterModeDeleteValues, ActionRequest request, ActionResponse response) {

		if (agentFilterModeDeleteValues != null) {
			for (int i = 0; i < agentFilterModeDeleteValues.length; i++) {
				j2eeAgent.getAgentFilterMode().remove(agentFilterModeDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Custom Response Header.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param customResponseHeaderKey
	 *            - {@link String} key to be insert
	 * @param customResponseHeaderValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCustomResponseHeader")
	public void doAddCustomResponseHeader(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String customResponseHeaderKey, String customResponseHeaderValue,
			ActionRequest request, ActionResponse response) {

		if (customResponseHeaderKey != null && !customResponseHeaderKey.equals("") && customResponseHeaderValue != null
				&& !customResponseHeaderValue.equals("")) {
			j2eeAgent.getCustomResponseHeader().add("[" + customResponseHeaderKey + "]=" + customResponseHeaderValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Agent Filter Mode.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param customResponseHeaderDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCustomResponseHeader")
	public void doDeleteCustomResponseHeader(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String[] customResponseHeaderDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (customResponseHeaderDeleteValues != null) {
			for (int i = 0; i < customResponseHeaderDeleteValues.length; i++) {
				j2eeAgent.getCustomResponseHeader().remove(customResponseHeaderDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in FQDN Virtual Host Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param fqdnVirtualHostMapKey
	 *            - {@link String} key to be insert
	 * @param fqdnVirtualHostMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addFqdnVirtualHostMap")
	public void doAddFqdnVirtualHostMap(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String fqdnVirtualHostMapKey, String fqdnVirtualHostMapValue, ActionRequest request,
			ActionResponse response) {

		if (fqdnVirtualHostMapKey != null && !fqdnVirtualHostMapKey.equals("") && fqdnVirtualHostMapValue != null
				&& !fqdnVirtualHostMapValue.equals("")) {
			j2eeAgent.getFqdnVirtualHostMap().add("[" + fqdnVirtualHostMapKey + "]=" + fqdnVirtualHostMapValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from FQDN Virtual Host Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param fqdnVirtualHostMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteFqdnVirtualHostMap")
	public void doDeleteFqdnVirtualHostMap(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, String[] fqdnVirtualHostMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (fqdnVirtualHostMapDeleteValues != null) {
			for (int i = 0; i < fqdnVirtualHostMapDeleteValues.length; i++) {
				j2eeAgent.getFqdnVirtualHostMap().remove(fqdnVirtualHostMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EEGlobalBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEGlobalBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentGlobal(@ModelAttribute("j2eeAgent") @Valid J2EEGlobalBean j2eeAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		j2EEGlobalBeanValidator.validate(j2eeAgent, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		try {
			agentsService.saveJ2EEGlobal(token, realm, j2eeAgent);
		} catch (IllegalArgumentException e) {
			response.setRenderParameter("error", e.getMessage());
			return;
		}

		if (AgentsConstants.AGENT_CONFIGURATION_LOCAL.equals(j2eeAgent.getLocationOfAgentConfigurationRepository())) {
			response.setRenderParameter("ctx", "EditLocalController");
		} else {
			response.setRenderParameter("message", "message.prifile_was_updated");
		}
	}

	/**
	 * Reset {@link J2EEGlobalBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetJ2EEAgentGlobal(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}
}
