/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.domain.datastores.DataStore;
import org.osiam.frontend.configuration.domain.datastores.DatabaseBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForADBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForAMDSBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForOpenDSBean;
import org.osiam.frontend.configuration.domain.datastores.LDAPv3ForTivoliBean;
import org.osiam.frontend.configuration.exceptions.DataStoresException;

import com.iplanet.sso.SSOToken;

/**
 * DataStores service interface. Defines methods for data stores management.
 */
public interface DataStoresService {

	/**
	 * @return list of data stores for a realm.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */

	List<DataStore> getDataStoresList(SSOToken token, String realm) throws DataStoresException;

	/**
	 * @return list of data store names for a realm.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	Set<String> getDataStoreNameSet(SSOToken token, String realm) throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addLDAPv3ForADDataStore(SSOToken token, String realm, LDAPv3ForADBean dataStore) throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addLDAPv3ForADAMDataStore(SSOToken token, String realm, LDAPv3ForADBean dataStore) throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addDatabaseDataStore(SSOToken token, String realm, DatabaseBean dataStore) throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addLDAPv3ForAMDSDataStore(SSOToken token, String realm, LDAPv3ForAMDSBean dataStore)
			throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addLDAPv3ForOpenDSDataStore(SSOToken token, String realm, LDAPv3ForOpenDSBean dataStore)
			throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @param realm
	 *            - the realm
	 * 
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addLDAPv3ForTivoliDataStore(SSOToken token, String realm, LDAPv3ForTivoliBean dataStore)
			throws DataStoresException;

	/**
	 * Add a new data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the data store name
	 * @param dataStoreType
	 *            - the data store type
	 * @param attributes
	 *            - map containing the data store attributes
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void addDataStore(SSOToken token, String realm, String dataStoreName, String dataStoreType,
			Map<String, Set<String>> attributes) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForADBean getDefaultAttributeValuesForLDAPv3ForAD(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForADBean getDefaultAttributeValuesForLDAPv3ForADAM(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	DatabaseBean getDefaultAttributeValuesForDatabase(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForAMDSBean getDefaultAttributeValuesForLDAPv3ForAMDS(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForOpenDSBean getDefaultAttributeValuesForLDAPv3ForOpenDS(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForTivoliBean getDefaultAttributeValuesForLDAPv3ForTivoli(SSOToken token) throws DataStoresException;

	/**
	 * @return default attributes for a data store type.
	 * 
	 * @param token
	 *            - the token
	 * @param type
	 *            - the data store type
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	Map<String, Set<String>> getDefaultAttributeValues(SSOToken token, String type) throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreLDAPv3ForAD(SSOToken token, String realm, LDAPv3ForADBean dataStore) throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreLDAPv3ForADAM(SSOToken token, String realm, LDAPv3ForADBean dataStore) throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreDatabase(SSOToken token, String realm, DatabaseBean dataStore) throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreLDAPv3ForAMDS(SSOToken token, String realm, LDAPv3ForAMDSBean dataStore)
			throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreLDAPv3ForOpenDS(SSOToken token, String realm, LDAPv3ForOpenDSBean dataStore)
			throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStore
	 *            - the data store
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStoreLDAPv3ForTivoli(SSOToken token, String realm, LDAPv3ForTivoliBean dataStore)
			throws DataStoresException;

	/**
	 * Edit an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the data store name
	 * @param attributes
	 *            - map containing the data store attributes
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void editDataStore(SSOToken token, String realm, String dataStoreName, Map<String, Set<String>> attributes)
			throws DataStoresException;

	/**
	 * Delete an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreNameSet
	 *            - set containing the names of the data stores to be deleted
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	void deleteDataStore(SSOToken token, String realm, Set<String> dataStoreNameSet) throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForADBean getDataStoreAttributesValuesForLDAPv3ForAD(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForADBean getDataStoreAttributesValuesForLDAPv3ForADAM(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	DatabaseBean getDataStoreAttributesValuesForDatabase(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForAMDSBean getDataStoreAttributesValuesForLDAPv3ForAMDS(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForOpenDSBean getDataStoreAttributesValuesForLDAPv3ForOpenDS(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException;

	/**
	 * @return attributes for a given data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - the name of the data store to return the attributes for
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	LDAPv3ForTivoliBean getDataStoreAttributesValuesForLDAPv3ForTivoli(SSOToken token, String realm,
			String dataStoreName) throws DataStoresException;

	/**
	 * @return attributes for an existing data store.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param dataStoreName
	 *            - name of the data set
	 * 
	 * @throws DataStoresException
	 *             - when something wrong happens.
	 */
	Map<String, Set<String>> getDataStoreAttributes(SSOToken token, String realm, String dataStoreName)
			throws DataStoresException;

}
