/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;

import static org.osiam.frontend.util.AttributeUtils.addPrefixIfEmpty;

import static org.osiam.frontend.util.AttributeUtils.removePrefix;

import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * 
 * Encapsulates data for a web application bean.
 * 
 */
public class WebApplicationBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private List<String> notEnforcedURLs = new ArrayList<String>();
	private boolean invertNotEnforcedURLs;
	private boolean fetchAttributesForNotEnforcedURLs;
	private List<String> notEnforcedClientIPList = new ArrayList<String>();
	private boolean clientIPValidation;
	private String profileAttributeFetchMode;
	private List<String> profileAttributeMap = new ArrayList<String>();
	private String responseAttributeFetchMode;
	private List<String> responseAttributeMap = new ArrayList<String>();
	private String sessionAttributeFetchMode;
	private List<String> sessionAttributeMap = new ArrayList<String>();
	private String attributeMultiValueSeparator;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getNotEnforcedURLs() {
		return notEnforcedURLs;
	}

	public void setNotEnforcedURLs(List<String> notEnforcedURLs) {
		this.notEnforcedURLs = notEnforcedURLs;
	}

	public boolean getInvertNotEnforcedURLs() {
		return invertNotEnforcedURLs;
	}

	public void setInvertNotEnforcedURLs(boolean invertNotEnforcedURLs) {
		this.invertNotEnforcedURLs = invertNotEnforcedURLs;
	}

	public boolean getFetchAttributesForNotEnforcedURLs() {
		return fetchAttributesForNotEnforcedURLs;
	}

	public void setFetchAttributesForNotEnforcedURLs(boolean fetchAttributesForNotEnforcedURLs) {
		this.fetchAttributesForNotEnforcedURLs = fetchAttributesForNotEnforcedURLs;
	}

	public List<String> getNotEnforcedClientIPList() {
		return notEnforcedClientIPList;
	}

	public void setNotEnforcedClientIPList(List<String> notEnforcedClientIPList) {
		this.notEnforcedClientIPList = notEnforcedClientIPList;
	}

	public boolean getClientIPValidation() {
		return clientIPValidation;
	}

	public void setClientIPValidation(boolean clientIPValidation) {
		this.clientIPValidation = clientIPValidation;
	}

	public String getProfileAttributeFetchMode() {
		return profileAttributeFetchMode;
	}

	public void setProfileAttributeFetchMode(String profileAttributeFetchMode) {
		this.profileAttributeFetchMode = profileAttributeFetchMode;
	}

	public List<String> getProfileAttributeMap() {
		return profileAttributeMap;
	}

	public void setProfileAttributeMap(List<String> profileAttributeMap) {
		this.profileAttributeMap = profileAttributeMap;
	}

	public String getResponseAttributeFetchMode() {
		return responseAttributeFetchMode;
	}

	public void setResponseAttributeFetchMode(String responseAttributeFetchMode) {
		this.responseAttributeFetchMode = responseAttributeFetchMode;
	}

	public List<String> getResponseAttributeMap() {
		return responseAttributeMap;
	}

	public void setResponseAttributeMap(List<String> responseAttributeMap) {
		this.responseAttributeMap = responseAttributeMap;
	}

	public String getSessionAttributeFetchMode() {
		return sessionAttributeFetchMode;
	}

	public void setSessionAttributeFetchMode(String sessionAttributeFetchMode) {
		this.sessionAttributeFetchMode = sessionAttributeFetchMode;
	}

	public List<String> getSessionAttributeMap() {
		return sessionAttributeMap;
	}

	public void setSessionAttributeMap(List<String> sessionAttributeMap) {
		this.sessionAttributeMap = sessionAttributeMap;
	}

	public String getAttributeMultiValueSeparator() {
		return attributeMultiValueSeparator;
	}

	public void setAttributeMultiValueSeparator(String attributeMultiValueSeparator) {
		this.attributeMultiValueSeparator = attributeMultiValueSeparator;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebApplicationBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebApplicationBean webApplicationBean = new WebApplicationBean();
		webApplicationBean.setName(name);
		webApplicationBean.setNotEnforcedURLs(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_URLS_CURRENT_VALUES))));
		webApplicationBean.setInvertNotEnforcedURLs(booleanFromSet(attributes
				.get(AgentAttributesConstants.INVERT_NOT_ENFORCED_URLS)));
		webApplicationBean.setFetchAttributesForNotEnforcedURLs(booleanFromSet(attributes
				.get(AgentAttributesConstants.FETCH_ATTRIBUTES_FOR_NOT_ENFORCED_URLS)));
		webApplicationBean.setNotEnforcedClientIPList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.NOT_ENFORCED_CLIENT_IP_LIST_CURRENT_VALUES))));
		webApplicationBean.setClientIPValidation(booleanFromSet(attributes
				.get(AgentAttributesConstants.CLIENT_IP_VALIDATION)));
		webApplicationBean.setProfileAttributeFetchMode(stringFromSet(attributes
				.get(AgentAttributesConstants.PROFILE_ATTRIBUTE_FETCH_MODE)));
		webApplicationBean.setProfileAttributeMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.PROFILE_ATTRIBUTE_MAP_CURRENT_VALUES)), ""));
		webApplicationBean.setResponseAttributeFetchMode(stringFromSet(attributes
				.get(AgentAttributesConstants.RESPONSE_ATTRIBUTE_FETCH_MODE)));
		webApplicationBean.setResponseAttributeMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.RESPONSE_ATTRIBUTE_MAP_CURRENT_VALUES)), ""));
		webApplicationBean.setSessionAttributeFetchMode(stringFromSet(attributes
				.get(AgentAttributesConstants.SESSION_ATTRIBUTE_FETCH_MODE)));
		webApplicationBean.setSessionAttributeMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.SESSION_ATTRIBUTE_MAP_CURRENT_VALUES)), ""));
		webApplicationBean.setAttributeMultiValueSeparator(stringFromSet(attributes
				.get(AgentAttributesConstants.ATTRIBUTE_MULTI_VALUE_SEPARATOR)));

		return webApplicationBean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.NOT_ENFORCED_URLS_CURRENT_VALUES, asSet(addIndexPrefix(getNotEnforcedURLs())));
		map.put(AgentAttributesConstants.INVERT_NOT_ENFORCED_URLS, asSet(getInvertNotEnforcedURLs()));

		map.put(AgentAttributesConstants.FETCH_ATTRIBUTES_FOR_NOT_ENFORCED_URLS,
				asSet(getFetchAttributesForNotEnforcedURLs()));

		map.put(AgentAttributesConstants.CLIENT_IP_VALIDATION, asSet(getClientIPValidation()));

		map.put(AgentAttributesConstants.NOT_ENFORCED_CLIENT_IP_LIST_CURRENT_VALUES,
				asSet(addIndexPrefix(getNotEnforcedClientIPList())));

		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTE_FETCH_MODE, asSet(getProfileAttributeFetchMode()));
		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTE_MAP_CURRENT_VALUES,
				asSet(addPrefixIfEmpty(getProfileAttributeMap(), "[]=")));
		map.put(AgentAttributesConstants.RESPONSE_ATTRIBUTE_FETCH_MODE, asSet(getResponseAttributeFetchMode()));
		map.put(AgentAttributesConstants.RESPONSE_ATTRIBUTE_MAP_CURRENT_VALUES,
				asSet(addPrefixIfEmpty(getResponseAttributeMap(), "[]=")));
		map.put(AgentAttributesConstants.SESSION_ATTRIBUTE_FETCH_MODE, asSet(getSessionAttributeFetchMode()));
		map.put(AgentAttributesConstants.SESSION_ATTRIBUTE_MAP_CURRENT_VALUES,
				asSet(addPrefixIfEmpty(getSessionAttributeMap(), "[]=")));
		map.put(AgentAttributesConstants.ATTRIBUTE_MULTI_VALUE_SEPARATOR, asSet(getAttributeMultiValueSeparator()));

		return map;
	}
}
