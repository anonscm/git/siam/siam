/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.WebApplicationBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WebAgentApplicationController shows the edit "Web Agent" (Application
 * attributes) form and does request processing of the edit web agent action.
 * 
 */
@Controller("WebAgentApplicationController")
@RequestMapping(value = "view", params = { "ctx=WebAgentApplicationController" })
public class WebAgentApplicationController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAgentApplicationController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "Web Agent" - application attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editWebAgentApplication(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "webAgent")) {

			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			WebApplicationBean agent = agentsService.getAttributesForWebApplication(token, realm, name);

			agent.setName(name);

			model.addAttribute("webAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);

		return "agents/web/application/edit";
	}

	/**
	 * Save new value in Not Enforced URLs list.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedURLsAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNotEnforcedURLs")
	public void doAddNotEnforcedURLs(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String notEnforcedURLsAddValue, ActionRequest request, ActionResponse response) {

		if (notEnforcedURLsAddValue != null && !notEnforcedURLsAddValue.equals("")) {
			webAgent.getNotEnforcedURLs().add(notEnforcedURLsAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Not Enforced URLs list.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedURLsDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNotEnforcedURLs")
	public void doDeleteNotEnforcedURLs(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String[] notEnforcedURLsDeleteValues, ActionRequest request, ActionResponse response) {

		if (notEnforcedURLsDeleteValues != null) {
			for (int i = 0; i < notEnforcedURLsDeleteValues.length; i++) {
				webAgent.getNotEnforcedURLs().remove(notEnforcedURLsDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Not Enforced Client IP List.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedClientIPListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNotEnforcedClientIPList")
	public void doAddNotEnforcedClientIPList(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String notEnforcedClientIPListAddValue, ActionRequest request, ActionResponse response) {

		if (notEnforcedClientIPListAddValue != null && !notEnforcedClientIPListAddValue.equals("")) {
			webAgent.getNotEnforcedClientIPList().add(notEnforcedClientIPListAddValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Not Enforced Client IP List.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedClientIPListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNotEnforcedClientIPList")
	public void doDeleteNotEnforcedClientIPList(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String[] notEnforcedClientIPListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (notEnforcedClientIPListDeleteValues != null) {
			for (int i = 0; i < notEnforcedClientIPListDeleteValues.length; i++) {
				webAgent.getNotEnforcedClientIPList().remove(notEnforcedClientIPListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Profile Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param profileAttributeMapKey
	 *            - {@link String} key to be insert
	 * @param profileAttributeMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addProfileAttributeMap")
	public void doAddProfileAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String profileAttributeMapKey, String profileAttributeMapValue,
			ActionRequest request, ActionResponse response) {

		if (profileAttributeMapKey != null && !profileAttributeMapKey.equals("") && profileAttributeMapValue != null
				&& !profileAttributeMapValue.equals("")) {
			webAgent.getProfileAttributeMap().add("[" + profileAttributeMapKey + "]=" + profileAttributeMapValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Profile Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param profileAttributeMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteProfileAttributeMap")
	public void doDeleteProfileAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String[] profileAttributeMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (profileAttributeMapDeleteValues != null) {
			for (int i = 0; i < profileAttributeMapDeleteValues.length; i++) {
				webAgent.getProfileAttributeMap().remove(profileAttributeMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Response Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAttributeMapKey
	 *            - {@link String} key to be insert
	 * @param responseAttributeMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addResponseAttributeMap")
	public void doAddResponseAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String responseAttributeMapKey, String responseAttributeMapValue,
			ActionRequest request, ActionResponse response) {

		if (responseAttributeMapKey != null && !responseAttributeMapKey.equals("") && responseAttributeMapValue != null
				&& !responseAttributeMapValue.equals("")) {
			webAgent.getResponseAttributeMap().add("[" + responseAttributeMapKey + "]=" + responseAttributeMapValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Response Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAttributeMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteResponseAttributeMap")
	public void doDeleteResponseAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String[] responseAttributeMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (responseAttributeMapDeleteValues != null) {
			for (int i = 0; i < responseAttributeMapDeleteValues.length; i++) {
				webAgent.getResponseAttributeMap().remove(responseAttributeMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save new value in Session Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param sessionAttributeMapKey
	 *            - {@link String} key to be insert
	 * @param sessionAttributeMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSessionAttributeMap")
	public void doAddSessionAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String sessionAttributeMapKey, String sessionAttributeMapValue,
			ActionRequest request, ActionResponse response) {

		if (sessionAttributeMapKey != null && !sessionAttributeMapKey.equals("") && sessionAttributeMapValue != null
				&& !sessionAttributeMapValue.equals("")) {
			webAgent.getSessionAttributeMap().add("[" + sessionAttributeMapKey + "]=" + sessionAttributeMapValue);
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Delete values from Session Attribute Map.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param sessionAttributeMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSessionAttributeMap")
	public void doDeleteSessionAttributeMap(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, String[] sessionAttributeMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (sessionAttributeMapDeleteValues != null) {
			for (int i = 0; i < sessionAttributeMapDeleteValues.length; i++) {
				webAgent.getSessionAttributeMap().remove(sessionAttributeMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", webAgent.getName());
	}

	/**
	 * Save {@link WebApplicationBean}.
	 * 
	 * @param webAgent
	 *            - {@link WebApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveWebAgentApplication(@ModelAttribute("webAgent") @Valid WebApplicationBean webAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", webAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveWebApplication(token, realm, webAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link WebApplicationBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentApplication(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}
}
