/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * Encapsulates all data for the Attribute Authority feature.
 */
public final class SAMLV2AttributeAuthorityBean extends EntityProviderDescriptor {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2AttributeAuthorityBean.class);

	// TODO: Key size Algorithm

	private String attributeServiceBasicLocation;
	private String attributeServiceBasicDefaultMapper;

	private boolean attributeServiceX509SupportsService;
	private String attributeServiceX509Location;
	private String attributeServiceX509X509Mapper;

	private String assertionIDRequestSoapLocation;
	private String assertionIDRequestURILocation;
	private String assertionIDRequestMapper;

	private String attributeProfile;
	private String subjectDataStore;

	public String getAttributeServiceBasicLocation() {
		return attributeServiceBasicLocation;
	}

	public void setAttributeServiceBasicLocation(String attributeServiceBasicLocation) {
		this.attributeServiceBasicLocation = attributeServiceBasicLocation;
	}

	public String getAttributeServiceBasicDefaultMapper() {
		return attributeServiceBasicDefaultMapper;
	}

	public void setAttributeServiceBasicDefaultMapper(String attributeServiceBasicDefaultMapper) {
		this.attributeServiceBasicDefaultMapper = attributeServiceBasicDefaultMapper;
	}

	public boolean getAttributeServiceX509SupportsService() {
		return attributeServiceX509SupportsService;
	}

	public void setAttributeServiceX509SupportsService(boolean attributeServiceX509SupportsService) {
		this.attributeServiceX509SupportsService = attributeServiceX509SupportsService;
	}

	public String getAttributeServiceX509Location() {
		return attributeServiceX509Location;
	}

	public void setAttributeServiceX509Location(String attributeServiceX509Location) {
		this.attributeServiceX509Location = attributeServiceX509Location;
	}

	public String getAttributeServiceX509X509Mapper() {
		return attributeServiceX509X509Mapper;
	}

	public void setAttributeServiceX509X509Mapper(String attributeServiceX509X509Mapper) {
		this.attributeServiceX509X509Mapper = attributeServiceX509X509Mapper;
	}

	public String getAssertionIDRequestSoapLocation() {
		return assertionIDRequestSoapLocation;
	}

	public void setAssertionIDRequestSoapLocation(String assertionIDRequestSoapLocation) {
		this.assertionIDRequestSoapLocation = assertionIDRequestSoapLocation;
	}

	public String getAssertionIDRequestURILocation() {
		return assertionIDRequestURILocation;
	}

	public void setAssertionIDRequestURILocation(String assertionIDRequestURILocation) {
		this.assertionIDRequestURILocation = assertionIDRequestURILocation;
	}

	public String getAssertionIDRequestMapper() {
		return assertionIDRequestMapper;
	}

	public void setAssertionIDRequestMapper(String assertionIDRequestMapper) {
		this.assertionIDRequestMapper = assertionIDRequestMapper;
	}

	public String getAttributeProfile() {
		return attributeProfile;
	}

	public void setAttributeProfile(String attributeProfile) {
		this.attributeProfile = attributeProfile;
	}

	public String getSubjectDataStore() {
		return subjectDataStore;
	}

	public void setSubjectDataStore(String subjectDataStore) {
		this.subjectDataStore = subjectDataStore;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static EntityProviderDescriptor fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {
		SAMLV2AttributeAuthorityBean ret = new SAMLV2AttributeAuthorityBean();
		String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));
		Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);
		Node configRoot = entityConfigDocument.getFirstChild();
		Node configNode = XmlUtil.getChildNode(configRoot, FederationConstants.ATTRIBUTE_AUTHORITY_CONFIG_NODE);

		String entityDescriptor = stringFromSet(map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));
		Document entityDescriptorDocument = XmlUtil.toDOMDocument(entityDescriptor);
		if (entityDescriptorDocument == null) {
			return ret;
		}
		Node descriptorRoot = entityDescriptorDocument.getFirstChild();
		Node descriptorNode = XmlUtil.getChildNode(descriptorRoot,
				FederationConstants.ATTRIBUTE_AUTHORITY_DESCRIPTOR_NODE);

		ret.setType("attribute_authority");
		ret.setMetaAlias(XmlUtil.getNodeAttributeValue(configNode, "metaAlias"));

		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals("signingCertAlias")) {
				ret.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("encryptionCertAlias")) {
				ret.setEncryptionCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("default_attributeAuthorityMapper")) {
				ret.setAttributeServiceBasicDefaultMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("x509Subject_attributeAuthorityMapper")) {
				ret.setAttributeServiceX509X509Mapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("assertionIDRequestMapper")) {
				ret.setAssertionIDRequestMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("x509SubjectDataStoreAttrName")) {
				ret.setSubjectDataStore(XmlUtil.getValueFromAttributeValue(node));
			}
		}

		NodeList descriptorChildNodeList = descriptorNode.getChildNodes();
		for (int i = 0; i < descriptorChildNodeList.getLength(); i++) {

			Node node = descriptorChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (node.getNodeName().equals("AttributeProfile")) {
				ret.setAttributeProfile(XmlUtil.getTextValueFromNode(node));
			} else if (node.getNodeName().equals("AttributeService")) {
				if (XmlUtil.getNodeAttributeValue(node, "xmlns:ns1") != null) {
					ret.setAttributeServiceX509SupportsService(XmlUtil.getNodeAttributeBooleanValue(node,
							"ns1:supportsX509Query"));
					ret.setAttributeServiceX509Location(XmlUtil.getNodeAttributeValue(node, "Location"));
				} else {
					ret.setAttributeServiceBasicLocation(XmlUtil.getNodeAttributeValue(node, "Location"));
				}
			} else if (node.getNodeName().equals("AssertionIDRequestService")) {
				if (XmlUtil.getNodeAttributeValue(node, "Binding").equals("urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
					ret.setAssertionIDRequestSoapLocation(XmlUtil.getNodeAttributeValue(node, "Location"));
				} else if (XmlUtil.getNodeAttributeValue(node, "Binding").equals(
						"urn:oasis:names:tc:SAML:2.0:bindings:URI")) {
					ret.setAssertionIDRequestURILocation(XmlUtil.getNodeAttributeValue(node, "Location"));
				}
			}
		}

		return ret;
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {
		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);
		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put("metaAlias", getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				"AttributeAuthorityConfig", queryAttributes);

		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());

		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());

		XmlUtil.createAttributeValue(attributeQueryConfigNode, "default_attributeAuthorityMapper",
				getAttributeServiceBasicDefaultMapper());

		XmlUtil.createAttributeValue(attributeQueryConfigNode, "x509Subject_attributeAuthorityMapper",
				getAttributeServiceX509X509Mapper());

		XmlUtil.createAttributeValue(attributeQueryConfigNode, "assertionIDRequestMapper",
				getAssertionIDRequestMapper());

		XmlUtil.createAttributeValue(attributeQueryConfigNode, "x509SubjectDataStoreAttrName", getSubjectDataStore());

		Map<String, String> descriptorAttributes = new HashMap<String, String>();
		descriptorAttributes.put("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
		Node attributeDescriptorNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
				"AttributeAuthorityDescriptor", descriptorAttributes);

		// Start Key Descriptor Signing

		if (getSigningCertificateAlias() != null && !"".equals(getSigningCertificateAlias())) {

			Map<String, String> signingAttributes = new HashMap<String, String>();
			signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
			Node signingNode = XmlUtil.createElement(attributeDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, signingAttributes);

			Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getSigningCertificateAlias());
			String base64SigningCertificate = "";
			if (cert != null) {
				base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);
			}

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64SigningCertificate, null);
			x509CertificateNode.setPrefix("ds");

		}
		// Start Key Descriptor Encryption

		if (getEncryptionCertificateAlias() != null && !"".equals(getEncryptionCertificateAlias())) {

			Map<String, String> encryptionAttributes = new HashMap<String, String>();
			encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
			Node encryptionNode = XmlUtil.createElement(attributeDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, encryptionAttributes);

			Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getEncryptionCertificateAlias());
			String algorithm = "";
			String base64EncryptionCertificate = "";
			if (cert != null) {
				base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);
				algorithm = cert.getSigAlgName();
			}
			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64EncryptionCertificate, null);
			x509CertificateNode.setPrefix("ds");
			// algorithm=XMLCipher.AES_128;
			Long keySize = new Long(128);

			Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
			encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
			Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE,
					null, encryptionMethodAttributes);

			Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
					"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
			keySizeNode.setPrefix("xenc");

		}
		
		
		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put("Binding", "urn:oasis:names:tc:SAML:2.0:bindings:SOAP");
		attributes.put("Location", getAttributeServiceBasicLocation());
		XmlUtil.createElement(attributeDescriptorNode, "AttributeService", attributes);

		attributes = new HashMap<String, String>();
		attributes.put("ns1:supportsX509Query", Boolean.toString(getAttributeServiceX509SupportsService()));
		attributes.put("Binding", "urn:oasis:names:tc:SAML:2.0:bindings:SOAP");
		attributes.put("Location", getAttributeServiceX509Location());
		attributes.put("xmlns:ns1", "urn:oasis:names:tc:SAML:metadata:X509:query");
		XmlUtil.createElement(attributeDescriptorNode, "AttributeService", attributes);

		attributes = new HashMap<String, String>();
		attributes.put("supportsX509Query", Boolean.toString(getAttributeServiceX509SupportsService()));
		attributes.put("Binding", "urn:oasis:names:tc:SAML:2.0:bindings:SOAP");
		attributes.put("Location", getAssertionIDRequestSoapLocation());
		XmlUtil.createElement(attributeDescriptorNode, "AssertionIDRequestService", attributes);

		attributes = new HashMap<String, String>();
		attributes.put("supportsX509Query", Boolean.toString(getAttributeServiceX509SupportsService()));
		attributes.put("Binding", "urn:oasis:names:tc:SAML:2.0:bindings:URI");
		attributes.put("Location", getAssertionIDRequestURILocation());
		XmlUtil.createElement(attributeDescriptorNode, "AssertionIDRequestService", attributes);

		XmlUtil.createTextElement(attributeDescriptorNode, "AttributeProfile", getAttributeProfile());

	}

	/**
	 * 
	 */
	public SAMLV2AttributeAuthorityBean() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_ATTRIBUTE_AUTHORITY);
	}
}
