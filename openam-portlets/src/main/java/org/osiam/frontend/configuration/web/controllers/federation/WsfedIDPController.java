/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.util.Arrays;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.WSFEDIDPAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * WsfedIDPController shows the edit Wsfed IDP Attributes form and does request
 * processing of the edit Wsfed IDP Attributes action.
 * 
 */
@Controller("WsfedIDPController")
@RequestMapping(value = "view", params = { "ctx=wsfedidentity_providerController" })
public class WsfedIDPController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WsfedSPController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("wsfedIDPAttributesBeanValidator")
	private Validator wsfedIDPAttributesBeanValidator;

	/**
	 * Shows edit Wsfed IDP Attributes for current EntityProvider form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "idp")) {
			EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
					FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, entityIdentifier);

			WSFEDIDPAttributesBean idp = (WSFEDIDPAttributesBean) entityProvider.getDescriptorMap().get(serviceType);
			model.addAttribute("idp", idp);
		}

		model.addAttribute("typeList", Arrays.asList(FederationConstants.NAME_ID_FORMAT_UPN,
				FederationConstants.NAME_ID_FORMAT_EMAIL_ADDRESS, FederationConstants.NAME_ID_FORMAT_COMMON_NAME));

		model.addAttribute("nameFormatList", Arrays.asList(FederationConstants.NAME_ID_FORMAT_UPN,
				FederationConstants.NAME_ID_FORMAT_EMAIL, FederationConstants.NAME_ID_FORMAT_COMMON_NAME));

		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/wsfed/idp/edit";
	}

	/**
	 * Add value for AttributeMapCurrentValues List.
	 * 
	 * @param idp
	 *            - {@link WSFEDIDPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeMapCurrentValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAttributeMapCurrentValues")
	public void doAddAttributeMapCurrentValues(@ModelAttribute("idp") @Valid WSFEDIDPAttributesBean idp,
			BindingResult result, String attributeMapCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesAddValue != null && !attributeMapCurrentValuesAddValue.equals("")) {
			idp.getAttributeMapCurrentValues().add(attributeMapCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete values form attributeMapCurrentValues list.
	 * 
	 * @param idp
	 *            - {@link WSFEDIDPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeMapCurrentValuesDeleteValues
	 *            - values to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAttributeMapCurrentValues")
	public void doDeleteAttributeMapCurrentValues(@ModelAttribute("idp") @Valid WSFEDIDPAttributesBean idp,
			BindingResult result, String[] attributeMapCurrentValuesDeleteValues, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesDeleteValues != null) {
			for (int i = 0; i < attributeMapCurrentValuesDeleteValues.length; i++) {
				idp.getAttributeMapCurrentValues().remove(attributeMapCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save Wsfed IDP Attributes for current EntityProvider.
	 * 
	 * @param idp
	 *            - {@link WSFEDIDPAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("idp") @Valid WSFEDIDPAttributesBean idp, BindingResult result,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		
		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		wsfedIDPAttributesBeanValidator.validate(new ObjectForValidate(realm, token, idp), result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL, entityIdentifier, idp);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");
	}

	/**
	 * Reset Attribute Query Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link WSFEDIDPAttributesBean}
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
