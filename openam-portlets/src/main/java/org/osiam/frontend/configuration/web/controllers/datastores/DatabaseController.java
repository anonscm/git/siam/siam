/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.datastores;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.datastores.DatabaseBean;
import org.osiam.frontend.configuration.service.DataStoresService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * DatabaseController shows the edit "Database Repository" DataStore form and
 * handles requests for add/delete certain parameter values and save DataStore.
 * 
 */
@Controller("DatabaseController")
@RequestMapping(value = "view", params = { "ctx=DatabaseController" })
public class DatabaseController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseController.class);

	@Autowired
	private DataStoresService dataStoresService;
	
	@Autowired
	@Qualifier("databaseBeanValidator")
	private Validator databaseBeanValidator;

	/**
	 * Show edit form for a "Database Repository" DataStore.
	 * 
	 * @param name
	 *            - DataStore name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String view(String name, String action, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "dataStoreAtt")) {
			DatabaseBean ds = new DatabaseBean();
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				ds = dataStoresService
						.getDataStoreAttributesValuesForDatabase(getSSOToken(realm, request), realm, name);
			} else {
				ds = dataStoresService.getDefaultAttributeValuesForDatabase(getSSOToken(realm, request));
			}
			ds.setName(name);

			model.addAttribute("dataStoreAtt", ds);
		}

		model.addAttribute("actionValue", action);

		return "datastores/databaseRepository/edit";
	}

	/**
	 * Save new value in Attribute Name Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeNameMappingAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAttributeNameMapping")
	public void doAddAttributeNameMapping(@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt,
			BindingResult result, String attributeNameMappingAddValue, ActionRequest request, ActionResponse response) {

		if (attributeNameMappingAddValue != null && !attributeNameMappingAddValue.equals("")) {
			dataStoreAtt.getAttributeNameMapping().add(attributeNameMappingAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Attribute Name Mapping list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param attributeNameMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAttributeNameMapping")
	public void doDeleteAttributeNameMapping(@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt,
			BindingResult result, String[] attributeNameMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (attributeNameMappingDeleteValues != null) {
			for (int i = 0; i < attributeNameMappingDeleteValues.length; i++) {
				dataStoreAtt.getAttributeNameMapping().remove(attributeNameMappingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in Database PlugIn Supported Types And Operations list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param databasePlugInSupportedTypesAndOperationsAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLdapPlugInSupportedTypesOperations")
	public void doAddLdapPlugInSupportedTypesOperations(
			@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt, BindingResult result,
			String databasePlugInSupportedTypesAndOperationsAddValue, ActionRequest request, ActionResponse response) {

		if (databasePlugInSupportedTypesAndOperationsAddValue != null
				&& !databasePlugInSupportedTypesAndOperationsAddValue.equals("")) {
			dataStoreAtt.getDatabasePlugInSupportedTypesAndOperations().add(
					databasePlugInSupportedTypesAndOperationsAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from Database PlugIn Supported Types And Operations list.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param databasePlugInSupportedTypesAndOperationsDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLdapPlugInSupportedTypesOperations")
	public void doDeleteLdapPlugInSupportedTypesOperations(
			@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt, BindingResult result,
			String[] databasePlugInSupportedTypesAndOperationsDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (databasePlugInSupportedTypesAndOperationsDeleteValues != null) {
			for (int i = 0; i < databasePlugInSupportedTypesAndOperationsDeleteValues.length; i++) {
				dataStoreAtt.getDatabasePlugInSupportedTypesAndOperations().remove(
						databasePlugInSupportedTypesAndOperationsDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save new value in List Of User Attributes Names In Database.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param listOfUserAttributesNamesInDatabaseAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addListOfUserAttributesNamesInDatabase")
	public void doAddListOfUserAttributesNamesInDatabase(
			@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt, BindingResult result,
			String listOfUserAttributesNamesInDatabaseAddValue, ActionRequest request, ActionResponse response) {

		if (listOfUserAttributesNamesInDatabaseAddValue != null
				&& !listOfUserAttributesNamesInDatabaseAddValue.equals("")) {
			dataStoreAtt.getListOfUserAttributesNamesInDatabase().add(listOfUserAttributesNamesInDatabaseAddValue);
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Delete values from List Of User Attributes Names In Database.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param listOfUserAttributesNamesInDatabaseDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteListOfUserAttributesNamesInDatabase")
	public void doDeleteListOfUserAttributesNamesInDatabase(
			@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt, BindingResult result,
			String[] listOfUserAttributesNamesInDatabaseDeleteValues, ActionRequest request, ActionResponse response) {

		if (listOfUserAttributesNamesInDatabaseDeleteValues != null) {
			for (int i = 0; i < listOfUserAttributesNamesInDatabaseDeleteValues.length; i++) {
				dataStoreAtt.getListOfUserAttributesNamesInDatabase().remove(
						listOfUserAttributesNamesInDatabaseDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", dataStoreAtt.getName());
	}

	/**
	 * Save {@link DatabaseBean}.
	 * 
	 * @param dataStoreAtt
	 *            - {@link DatabaseBean}
	 * @param result
	 *            - BindingResult
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("dataStoreAtt") @Valid DatabaseBean dataStoreAtt, BindingResult result,
			String action, ActionRequest request, ActionResponse response) {

		databaseBeanValidator.validate(dataStoreAtt, result);
		
		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);

		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			dataStoresService.editDataStoreDatabase(getSSOToken(realm, request), realm, dataStoreAtt);
		} else {
			dataStoresService.addDatabaseDataStore(getSSOToken(realm, request), realm, dataStoreAtt);
		}
		response.setRenderParameter("ctx", "DataStoresController");
	}

	/**
	 * Reset {@link DatabaseBean} form.
	 * 
	 * @param name
	 *            - DataStore name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to data store list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "DataStoresController");
	}

}
