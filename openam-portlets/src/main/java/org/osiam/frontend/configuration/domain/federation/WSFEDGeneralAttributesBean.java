/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * Encapsulates data for a WSFED General Attributes Bean.
 * 
 */
public class WSFEDGeneralAttributesBean extends EntityProviderDescriptor {
	private static final Logger LOGGER = Logger.getLogger(WSFEDGeneralAttributesBean.class);

	private String spDisplayName;
	private String idpDisplayName;

	private String tokenIssuerNameList;
	private String tokenIssuerEndpointList;

	private List<String> tokenTypesOffered;
	private String singleSignOutNotificationEndpoint;
	private String uriNamedClaimTypesOfferedDisplay;
	private String uriNamedClaimTypesOfferedURL;

	public String getUriNamedClaimTypesOfferedDisplay() {
		return uriNamedClaimTypesOfferedDisplay;
	}

	public void setUriNamedClaimTypesOfferedDisplay(String uriNamedClaimTypesOfferedDisplay) {
		this.uriNamedClaimTypesOfferedDisplay = uriNamedClaimTypesOfferedDisplay;
	}

	public String getUriNamedClaimTypesOfferedURL() {
		return uriNamedClaimTypesOfferedURL;
	}

	public void setUriNamedClaimTypesOfferedURL(String uriNamedClaimTypesOfferedURL) {
		this.uriNamedClaimTypesOfferedURL = uriNamedClaimTypesOfferedURL;
	}

	public String getSingleSignOutNotificationEndpoint() {
		return singleSignOutNotificationEndpoint;
	}

	public void setSingleSignOutNotificationEndpoint(String singleSignOutNotificationEndpoint) {
		this.singleSignOutNotificationEndpoint = singleSignOutNotificationEndpoint;
	}

	public List<String> getTokenTypesOffered() {
		return tokenTypesOffered;
	}

	public void setTokenTypesOffered(List<String> tokenTypesOffered) {
		this.tokenTypesOffered = tokenTypesOffered;
	}

	public String getSpDisplayName() {
		return spDisplayName;
	}

	public void setSpDisplayName(String sPDisplayName) {
		this.spDisplayName = sPDisplayName;
	}

	public String getIdpDisplayName() {
		return idpDisplayName;
	}

	public void setIdpDisplayName(String iDPDisplayName) {
		this.idpDisplayName = iDPDisplayName;
	}

	public String getTokenIssuerNameList() {
		return tokenIssuerNameList;
	}

	public void setTokenIssuerNameList(String tokenIssuerNameList) {
		this.tokenIssuerNameList = tokenIssuerNameList;
	}

	public String getTokenIssuerEndpointList() {
		return tokenIssuerEndpointList;
	}

	public void setTokenIssuerEndpointList(String tokenIssuerEndpointList) {
		this.tokenIssuerEndpointList = tokenIssuerEndpointList;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static EntityProviderDescriptor fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		WSFEDGeneralAttributesBean bean = new WSFEDGeneralAttributesBean();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();
		Set<Node> idpConfigNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.IDPSSO_CONFIG_NODE);

		if (idpConfigNodeSet != null) {
			Iterator<Node> iterator = idpConfigNodeSet.iterator();
			if (iterator.hasNext()) {
				Node idpConfigNode = iterator.next();
				bean.setMetaAlias(XmlUtil.getNodeAttributeValue(idpConfigNode, "metaAlias"));

				NodeList idpConfigChildNodeList = idpConfigNode.getChildNodes();

				for (int i = 0; i < idpConfigChildNodeList.getLength(); i++) {

					Node node = idpConfigChildNodeList.item(i);
					if (node.getNodeType() == Node.TEXT_NODE) {
						continue;
					}
					if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
						continue;
					}

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
					}

					String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(String.format("Attribute name: %s", attributeName));
					}

					if (attributeName.equals(FederationConstants.DISPLAY_NAME)) {
						AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
						bean.setIdpDisplayName(valuePair.getValueList().get(0));
					}
				}
			}
		} else {
			bean.setIdpDisplayName(null);
		}
		Set<Node> spConfigNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.SPSSO_CONFIG_NODE);
		if (spConfigNodeSet != null) {
			Iterator<Node> iterator = spConfigNodeSet.iterator();
			if (iterator.hasNext()) {
				Node spConfigNode = iterator.next();
				NodeList spConfigChildNodeList = spConfigNode.getChildNodes();

				for (int i = 0; i < spConfigChildNodeList.getLength(); i++) {

					Node node = spConfigChildNodeList.item(i);
					if (node.getNodeType() == Node.TEXT_NODE) {
						continue;
					}
					if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
						continue;
					}

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
					}

					String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(String.format("Attribute name: %s", attributeName));
					}

					if (attributeName.equals(FederationConstants.DISPLAY_NAME)) {
						AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
						bean.setSpDisplayName(valuePair.getValueList().get(0));
					}
				}
			}
		} else {
			bean.setSpDisplayName(null);
		}

		Document metatadaDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY)));

		Node metadataRootNode = metatadaDocument.getFirstChild();

		if (metadataRootNode != null) {

			List<String> tokenIssuerNameList = new ArrayList<String>();
			Set<Node> tokenIssuerNameNodeSet = XmlUtil.getChildNodes(metadataRootNode, "TokenIssuerName");
			Iterator<Node> tokenIssuerNameNodeSetIterator = tokenIssuerNameNodeSet.iterator();
			while (tokenIssuerNameNodeSetIterator.hasNext()) {
				Node tokenIssuerNameNode = tokenIssuerNameNodeSetIterator.next();
				tokenIssuerNameList.add(tokenIssuerNameNode.getTextContent());
			}

			Node tokenIssuerEndpointNode = XmlUtil.getChildNode(metadataRootNode, "TokenIssuerEndpoint");
			NodeList children = tokenIssuerEndpointNode.getChildNodes();

			for (int i = 0; i < children.getLength(); i++) {
				String value = children.item(i).getTextContent().trim();
				if (value.length() > 2) {
					bean.setTokenIssuerEndpointList(value);
				}
			}

			List<String> tokenTypesOfferedList = new ArrayList<String>();

			if (XmlUtil.childNodeExists(metadataRootNode, "TokenTypesOffered")) {
				Node tokenTypesOfferedNode = XmlUtil.getChildNode(metadataRootNode, "TokenTypesOffered");
				Set<Node> tokenTypesOfferedChildren = XmlUtil.getChildNodes(tokenTypesOfferedNode, "TokenType");
				Iterator<Node> tokenTypesOfferedChildrenSetIterator = tokenTypesOfferedChildren.iterator();
				while (tokenTypesOfferedChildrenSetIterator.hasNext()) {
					Node node = tokenTypesOfferedChildrenSetIterator.next();
					tokenTypesOfferedList.add(XmlUtil.getNodeAttributeValue(node, "Uri"));

				}
				bean.setTokenTypesOffered(tokenTypesOfferedList);
			}

			Node signOutNode = null;
			if (XmlUtil.getChildNodes(metadataRootNode, "SingleSignOutNotificationEndpoint").size() != 0) {
				signOutNode = XmlUtil.getChildNode(metadataRootNode, "SingleSignOutNotificationEndpoint");
				bean.setSingleSignOutNotificationEndpoint(signOutNode.getTextContent());
			}

			if (XmlUtil.childNodeExists(metadataRootNode, "UriNamedClaimTypesOffered")) {
				Node typesNode = XmlUtil.getChildNode(metadataRootNode, "UriNamedClaimTypesOffered");
				Node claimChildren = XmlUtil.getChildNode(typesNode, "ClaimType");
				bean.setUriNamedClaimTypesOfferedURL(XmlUtil.getNodeAttributeValue(claimChildren, "Uri"));
				Node nameChildren = XmlUtil.getChildNode(claimChildren, "DisplayName");
				bean.setUriNamedClaimTypesOfferedDisplay(nameChildren.getTextContent());
			}

			bean.setTokenIssuerNameList(tokenIssuerNameList.get(0));

		}

		return bean;
	}

	/**
	 * WSFEDGeneralAttributesBean constructor.
	 */
	public WSFEDGeneralAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.WSFED_GENERAL);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) {
		Document metadataDocument = map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY);
		Document configDocument = map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		XmlUtil.createTextElement(metadataDocument.getFirstChild(), "TokenIssuerName", tokenIssuerNameList);

		Node tokenNode = XmlUtil.createElement(metadataDocument.getFirstChild(), "TokenIssuerEndpoint");
		Map<String, String> addressAttributes = new HashMap<String, String>();
		addressAttributes.put("xmlns:ns1", "http://www.w3.org/2005/08/addressing");
		XmlUtil.createTextElement(tokenNode, "ns1" + ":Address",null, tokenIssuerEndpointList, addressAttributes);

		if (getTokenTypesOffered() != null) {
			Node tokenTypesOfferedNode = XmlUtil.createElement(metadataDocument.getFirstChild(), "TokenTypesOffered");
			for (int i = 0; i < tokenTypesOffered.size(); i++) {
				Map<String, String> nodeAttributes = new HashMap<String, String>();
				nodeAttributes.put("Uri", tokenTypesOffered.get(i));
				XmlUtil.createTextElement(tokenTypesOfferedNode, "TokenType",null, "", nodeAttributes);
			}
		}

		if (getUriNamedClaimTypesOfferedDisplay() != null) {
			Node tokenTypesOfferedNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
					"UriNamedClaimTypesOffered");
			Map<String, String> nodeAttributes = new HashMap<String, String>();
			nodeAttributes.put("Uri", getUriNamedClaimTypesOfferedURL());
			Node node = XmlUtil.createTextElement(tokenTypesOfferedNode, "ClaimType",null, "", nodeAttributes);
			XmlUtil.createTextElement(node, "DisplayName",null, getUriNamedClaimTypesOfferedDisplay(), null);

		}

		if (singleSignOutNotificationEndpoint != null) {
			Node signOutNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
					"SingleSignOutNotificationEndpoint");
			Map<String, String> nodeAttributes = new HashMap<String, String>();
			nodeAttributes.put("xmlns:ns2", "http://www.w3.org/2005/08/addressing");
			XmlUtil.createTextElement(signOutNode, "ns2" + ":Address",null, singleSignOutNotificationEndpoint,
					nodeAttributes);
		}

		if (getIdpDisplayName() != null) {
			Node idpssoConfigNode = XmlUtil.getChildNode(configDocument.getFirstChild(), "IDPSSOConfig");
			XmlUtil.createAttributeValue(idpssoConfigNode, "displayName", getIdpDisplayName());
		}

		if (getSpDisplayName() != null) {
			Node spConfigNode = XmlUtil.getChildNode(configDocument.getFirstChild(), "SPSSOConfig");
			XmlUtil.createAttributeValue(spConfigNode, "displayName", getSpDisplayName());
		}

	}
}
