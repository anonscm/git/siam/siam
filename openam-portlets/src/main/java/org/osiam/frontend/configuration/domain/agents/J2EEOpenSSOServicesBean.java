/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a j2ee open sso services bean.
 * 
 */
public class J2EEOpenSSOServicesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private List<String> openSSOLoginURL = new ArrayList<String>();
	private boolean loginURLPrioritized;
	private boolean loginURLProbe;
	@NotNull
	@Min(0)
	private Integer loginURLProbeTimeout;
	private List<String> openSSOLogoutURL = new ArrayList<String>();
	private boolean logoutURLPrioritized;
	private boolean logoutURLProbe;
	@NotNull
	@Min(0)
	private Integer logoutURLProbeTimeout;
	private String openSSOAuthenticationServiceProtocol;
	private String openSSOAuthenticationServiceHostName;
	@NotNull
	@Min(0)
	private Integer openSSOAuthenticationServicePort;
	private boolean enablePolicyNotifications;
	@NotNull
	@Min(0)
	private Integer policyClientPollingInterval;
	private String policyClientCacheMode;
	private String policyClientBooleanActionValues;
	private String policyClientResourceComparators;
	@NotNull
	@Min(0)
	private Integer policyClientClockSkew;
	private List<String> urlPolicyEnvGETParameters = new ArrayList<String>();
	private List<String> urlPolicyEnvPOSTParameters = new ArrayList<String>();
	private List<String> urlPolicyEnvJsessionParameters = new ArrayList<String>();
	private boolean enableNotificationofUserDataCaches;
	@NotNull
	@Min(0)
	private Integer userDataCachePollingTime;
	private boolean enableNotificationOfServiceDataCaches;
	@NotNull
	@Min(0)
	private Integer serviceDataCacheTime;
	private boolean enableClientPolling;
	@NotNull
	@Min(0)
	private Integer clientPollingPeriod;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getOpenSSOLoginURL() {
		return openSSOLoginURL;
	}

	public void setOpenSSOLoginURL(List<String> openSSOLoginURL) {
		this.openSSOLoginURL = openSSOLoginURL;
	}

	public boolean getLoginURLPrioritized() {
		return loginURLPrioritized;
	}

	public void setLoginURLPrioritized(boolean loginURLPrioritized) {
		this.loginURLPrioritized = loginURLPrioritized;
	}

	public boolean getLoginURLProbe() {
		return loginURLProbe;
	}

	public void setLoginURLProbe(boolean loginURLProbe) {
		this.loginURLProbe = loginURLProbe;
	}

	public Integer getLoginURLProbeTimeout() {
		return loginURLProbeTimeout;
	}

	public void setLoginURLProbeTimeout(Integer loginURLProbeTimeout) {
		this.loginURLProbeTimeout = loginURLProbeTimeout;
	}

	public List<String> getOpenSSOLogoutURL() {
		return openSSOLogoutURL;
	}

	public void setOpenSSOLogoutURL(List<String> openSSOLogoutURL) {
		this.openSSOLogoutURL = openSSOLogoutURL;
	}

	public boolean getLogoutURLPrioritized() {
		return logoutURLPrioritized;
	}

	public void setLogoutURLPrioritized(boolean logoutURLPrioritized) {
		this.logoutURLPrioritized = logoutURLPrioritized;
	}

	public boolean getLogoutURLProbe() {
		return logoutURLProbe;
	}

	public void setLogoutURLProbe(boolean logoutURLProbe) {
		this.logoutURLProbe = logoutURLProbe;
	}

	public Integer getLogoutURLProbeTimeout() {
		return logoutURLProbeTimeout;
	}

	public void setLogoutURLProbeTimeout(Integer logoutURLProbeTimeout) {
		this.logoutURLProbeTimeout = logoutURLProbeTimeout;
	}

	public String getOpenSSOAuthenticationServiceProtocol() {
		return openSSOAuthenticationServiceProtocol;
	}

	public void setOpenSSOAuthenticationServiceProtocol(String openSSOAuthenticationServiceProtocol) {
		this.openSSOAuthenticationServiceProtocol = openSSOAuthenticationServiceProtocol;
	}

	public String getOpenSSOAuthenticationServiceHostName() {
		return openSSOAuthenticationServiceHostName;
	}

	public void setOpenSSOAuthenticationServiceHostName(String openSSOAuthenticationServiceHostName) {
		this.openSSOAuthenticationServiceHostName = openSSOAuthenticationServiceHostName;
	}

	public Integer getOpenSSOAuthenticationServicePort() {
		return openSSOAuthenticationServicePort;
	}

	public void setOpenSSOAuthenticationServicePort(Integer openSSOAuthenticationServicePort) {
		this.openSSOAuthenticationServicePort = openSSOAuthenticationServicePort;
	}

	public boolean getEnablePolicyNotifications() {
		return enablePolicyNotifications;
	}

	public void setEnablePolicyNotifications(boolean enablePolicyNotifications) {
		this.enablePolicyNotifications = enablePolicyNotifications;
	}

	public Integer getPolicyClientPollingInterval() {
		return policyClientPollingInterval;
	}

	public void setPolicyClientPollingInterval(Integer policyClientPollingInterval) {
		this.policyClientPollingInterval = policyClientPollingInterval;
	}

	public String getPolicyClientCacheMode() {
		return policyClientCacheMode;
	}

	public void setPolicyClientCacheMode(String policyClientCacheMode) {
		this.policyClientCacheMode = policyClientCacheMode;
	}

	public String getPolicyClientBooleanActionValues() {
		return policyClientBooleanActionValues;
	}

	public void setPolicyClientBooleanActionValues(String policyClientBooleanActionValues) {
		this.policyClientBooleanActionValues = policyClientBooleanActionValues;
	}

	public String getPolicyClientResourceComparators() {
		return policyClientResourceComparators;
	}

	public void setPolicyClientResourceComparators(String policyClientResourceComparators) {
		this.policyClientResourceComparators = policyClientResourceComparators;
	}

	public Integer getPolicyClientClockSkew() {
		return policyClientClockSkew;
	}

	public void setPolicyClientClockSkew(Integer policyClientClockSkew) {
		this.policyClientClockSkew = policyClientClockSkew;
	}

	public List<String> getUrlPolicyEnvGETParameters() {
		return urlPolicyEnvGETParameters;
	}

	public void setUrlPolicyEnvGETParameters(List<String> urlPolicyEnvGETParameters) {
		this.urlPolicyEnvGETParameters = urlPolicyEnvGETParameters;
	}

	public List<String> getUrlPolicyEnvPOSTParameters() {
		return urlPolicyEnvPOSTParameters;
	}

	public void setUrlPolicyEnvPOSTParameters(List<String> urlPolicyEnvPOSTParameters) {
		this.urlPolicyEnvPOSTParameters = urlPolicyEnvPOSTParameters;
	}

	public List<String> getUrlPolicyEnvJsessionParameters() {
		return urlPolicyEnvJsessionParameters;
	}

	public void setUrlPolicyEnvJsessionParameters(List<String> urlPolicyEnvJsessionParameters) {
		this.urlPolicyEnvJsessionParameters = urlPolicyEnvJsessionParameters;
	}

	public boolean getEnableNotificationofUserDataCaches() {
		return enableNotificationofUserDataCaches;
	}

	public void setEnableNotificationofUserDataCaches(boolean enableNotificationofUserDataCaches) {
		this.enableNotificationofUserDataCaches = enableNotificationofUserDataCaches;
	}

	public Integer getUserDataCachePollingTime() {
		return userDataCachePollingTime;
	}

	public void setUserDataCachePollingTime(Integer userDataCachePollingTime) {
		this.userDataCachePollingTime = userDataCachePollingTime;
	}

	public boolean getEnableNotificationOfServiceDataCaches() {
		return enableNotificationOfServiceDataCaches;
	}

	public void setEnableNotificationOfServiceDataCaches(boolean enableNotificationOfServiceDataCaches) {
		this.enableNotificationOfServiceDataCaches = enableNotificationOfServiceDataCaches;
	}

	public Integer getServiceDataCacheTime() {
		return serviceDataCacheTime;
	}

	public void setServiceDataCacheTime(Integer serviceDataCacheTime) {
		this.serviceDataCacheTime = serviceDataCacheTime;
	}

	public boolean getEnableClientPolling() {
		return enableClientPolling;
	}

	public void setEnableClientPolling(boolean enableClientPolling) {
		this.enableClientPolling = enableClientPolling;
	}

	public Integer getClientPollingPeriod() {
		return clientPollingPeriod;
	}

	public void setClientPollingPeriod(Integer clientPollingPeriod) {
		this.clientPollingPeriod = clientPollingPeriod;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static J2EEOpenSSOServicesBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EEOpenSSOServicesBean bean = new J2EEOpenSSOServicesBean();
		bean.setName(name);
		bean.setOpenSSOLoginURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.OPENSSO_LOGIN_URL))));
		bean.setLoginURLPrioritized(booleanFromSet(attributes.get(AgentAttributesConstants.LOGIN_URL_PRIORITIZED)));
		bean.setLoginURLProbe(booleanFromSet(attributes.get(AgentAttributesConstants.LOGIN_URL_PROBE)));
		bean.setLoginURLProbeTimeout(integerFromSet(attributes.get(AgentAttributesConstants.LOGIN_URL_PROBE_TIMEOUT)));
		bean.setOpenSSOLogoutURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.OPENAM_LOGOUT_URL))));
		bean.setLogoutURLPrioritized(booleanFromSet(attributes.get(AgentAttributesConstants.LOGOUT_URL_PRIORITIZED)));
		bean.setLogoutURLProbe(booleanFromSet(attributes.get(AgentAttributesConstants.LOGOUT_URL_PROBE)));
		bean.setLogoutURLProbeTimeout(integerFromSet(attributes.get(AgentAttributesConstants.LOGOUT_URL_PROBE_TIMEOUT)));
		bean.setOpenSSOAuthenticationServiceProtocol(stringFromSet(attributes
				.get(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_PROTOCOL)));
		bean.setOpenSSOAuthenticationServiceHostName(stringFromSet(attributes
				.get(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_HOST_NAME)));
		bean.setOpenSSOAuthenticationServicePort(integerFromSet(attributes
				.get(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_PORT)));
		bean.setEnablePolicyNotifications(booleanFromSet(attributes
				.get(AgentAttributesConstants.ENABLE_POLICY_NOTIFICATIONS)));
		bean.setPolicyClientPollingInterval(integerFromSet(attributes
				.get(AgentAttributesConstants.POLICY_CLIENT_POLLING_INTERVAL)));
		bean.setPolicyClientCacheMode(stringFromSet(attributes.get(AgentAttributesConstants.POLICY_CLIENT_CACHE_MODE)));
		bean.setPolicyClientBooleanActionValues(stringFromSet(attributes
				.get(AgentAttributesConstants.POLICY_CLIENT_BOOLEAN_ACTION_VALUES)));
		bean.setPolicyClientResourceComparators(stringFromSet(attributes
				.get(AgentAttributesConstants.POLICY_CLIENT_RESOURCE_COMPARATORS)));
		bean.setPolicyClientClockSkew(integerFromSet(attributes.get(AgentAttributesConstants.POLICY_CLIENT_CLOCK_SKEW)));
		bean.setUrlPolicyEnvGETParameters(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.URL_POLICY_ENV_GET_PARAMETERS))));
		bean.setUrlPolicyEnvPOSTParameters(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.URL_POLICY_ENV_POST_PARAMETERS))));
		bean.setUrlPolicyEnvJsessionParameters(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.URL_POLICY_ENV_JSESSION_PARAMETERS))));
		bean.setEnableNotificationofUserDataCaches(booleanFromSet(attributes
				.get(AgentAttributesConstants.ENABLE_NOTIFICATION_OF_USER_DATA_CACHES)));
		bean.setUserDataCachePollingTime(integerFromSet(attributes
				.get(AgentAttributesConstants.USER_DATA_CACHE_POLLING_TIME)));
		bean.setEnableNotificationOfServiceDataCaches(booleanFromSet(attributes
				.get(AgentAttributesConstants.ENABLE_NOTIFICATION_OF_SERVICE_DATA_CACHES)));
		bean.setServiceDataCacheTime(integerFromSet(attributes.get(AgentAttributesConstants.SERVICE_DATA_CACHE_TIME)));
		bean.setEnableClientPolling(booleanFromSet(attributes.get(AgentAttributesConstants.ENABLE_CLIENT_POLLING)));
		bean.setClientPollingPeriod(integerFromSet(attributes.get(AgentAttributesConstants.CLIENT_POLLING_PERIOD)));

		return bean;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.OPENSSO_LOGIN_URL, asSet(addIndexPrefix(getOpenSSOLoginURL())));

		map.put(AgentAttributesConstants.LOGIN_URL_PRIORITIZED, asSet(getLoginURLPrioritized()));
		map.put(AgentAttributesConstants.LOGIN_URL_PROBE, asSet(getLoginURLProbe()));
		map.put(AgentAttributesConstants.LOGIN_URL_PROBE_TIMEOUT, asSet(getLoginURLProbeTimeout()));

		map.put(AgentAttributesConstants.OPENAM_LOGOUT_URL, asSet(addIndexPrefix(getOpenSSOLogoutURL())));
		map.put(AgentAttributesConstants.LOGOUT_URL_PRIORITIZED, asSet(getLogoutURLPrioritized()));
		map.put(AgentAttributesConstants.LOGOUT_URL_PROBE, asSet(getLogoutURLProbe()));
		map.put(AgentAttributesConstants.LOGOUT_URL_PROBE_TIMEOUT, asSet(getLogoutURLProbeTimeout()));
		map.put(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_PROTOCOL,
				asSet(getOpenSSOAuthenticationServiceProtocol()));

		map.put(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_HOST_NAME,
				asSet(getOpenSSOAuthenticationServiceHostName()));
		map.put(AgentAttributesConstants.OPENAM_AUTHENTICATION_SERVICE_PORT,
				asSet(getOpenSSOAuthenticationServicePort()));
		map.put(AgentAttributesConstants.ENABLE_POLICY_NOTIFICATIONS, asSet(getEnablePolicyNotifications()));
		map.put(AgentAttributesConstants.POLICY_CLIENT_POLLING_INTERVAL, asSet(getPolicyClientPollingInterval()));
		map.put(AgentAttributesConstants.POLICY_CLIENT_CACHE_MODE, asSet(getPolicyClientCacheMode()));
		map.put(AgentAttributesConstants.POLICY_CLIENT_BOOLEAN_ACTION_VALUES,
				asSet(getPolicyClientBooleanActionValues()));
		map.put(AgentAttributesConstants.POLICY_CLIENT_RESOURCE_COMPARATORS,
				asSet(getPolicyClientResourceComparators()));
		map.put(AgentAttributesConstants.POLICY_CLIENT_CLOCK_SKEW, asSet(getPolicyClientClockSkew()));
		map.put(AgentAttributesConstants.URL_POLICY_ENV_GET_PARAMETERS,
				asSet(addIndexPrefix(getUrlPolicyEnvGETParameters())));
		map.put(AgentAttributesConstants.URL_POLICY_ENV_POST_PARAMETERS,
				asSet(addIndexPrefix(getUrlPolicyEnvPOSTParameters())));
		map.put(AgentAttributesConstants.URL_POLICY_ENV_JSESSION_PARAMETERS,
				asSet(addIndexPrefix(getUrlPolicyEnvJsessionParameters())));
		map.put(AgentAttributesConstants.ENABLE_NOTIFICATION_OF_USER_DATA_CACHES,
				asSet(getEnableNotificationofUserDataCaches()));
		map.put(AgentAttributesConstants.USER_DATA_CACHE_POLLING_TIME, asSet(getUserDataCachePollingTime()));
		map.put(AgentAttributesConstants.ENABLE_NOTIFICATION_OF_SERVICE_DATA_CACHES,
				asSet(getEnableNotificationOfServiceDataCaches()));
		map.put(AgentAttributesConstants.SERVICE_DATA_CACHE_TIME, asSet(getServiceDataCacheTime()));
		map.put(AgentAttributesConstants.ENABLE_CLIENT_POLLING, asSet(getEnableClientPolling()));

		map.put(AgentAttributesConstants.CLIENT_POLLING_PERIOD, asSet(getClientPollingPeriod()));

		return map;
	}

}
