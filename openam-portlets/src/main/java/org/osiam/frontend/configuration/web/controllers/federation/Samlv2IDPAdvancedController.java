/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2IDPAdvanced;
import org.osiam.frontend.configuration.domain.federation.SAMLV2IDPEntityProviderService;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * IDP Advanced
 * 
 * Samlv2IDPAdvancedController shows the edit SAMLv2 IDP Advanced Attributes
 * form and does request processing of the edit SAMLv2 IDP Advanced Attributes
 * action.
 * 
 */
@Controller("Samlv2IDPAdvancedController")
@RequestMapping(value = "view", params = { "ctx=Samlv2IDPAdvancedController" })
public class Samlv2IDPAdvancedController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2IDPAdvancedController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows edit IDP Advanced Attributes for current {@link EntityProvider}
	 * form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2IDPEntityProviderService idpEntityProvider = (SAMLV2IDPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("idpAdvanced")) {
			model.addAttribute("idpAdvanced", idpEntityProvider.getAdvanced());
		}

		model.addAttribute("metaAlias", idpEntityProvider.getMetaAlias());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/idp/advanced/edit";
	}

	/**
	 * Save values for Sae Configuration Application Security Configuration
	 * Current Values.
	 * 
	 * @param idpAdvanced
	 *            - {@link SAMLV2IDPAdvanced}
	 * @param result
	 *            - BindingResult
	 * @param saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue
	 *            - list with values to be added
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSaeConfigurationApplicationSecurityConfigurationCurrentValues")
	public void doAddSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			@ModelAttribute("idpAdvanced") @Valid SAMLV2IDPAdvanced idpAdvanced, BindingResult result,
			String saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue != null
				&& !saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue.equals("")) {
			idpAdvanced.getSaeConfigurationApplicationSecurityConfigurationCurrentValues().add(
					saeConfigurationApplicationSecurityConfigurationCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value for Sae Configuration Application Security Configuration
	 * Current Values.
	 * 
	 * @param idpAdvanced
	 *            - {@link SAMLV2IDPAdvanced}
	 * @param result
	 *            - BindingResult
	 * @param saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues
	 *            - list with values to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSaeConfigurationApplicationSecurityConfigurationCurrentValues")
	public void doDeleteSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			@ModelAttribute("idpAdvanced") @Valid SAMLV2IDPAdvanced idpAdvanced, BindingResult result,
			String[] saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		if (saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues != null) {
			for (int i = 0; i < saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues.length; i++) {
				idpAdvanced.getSaeConfigurationApplicationSecurityConfigurationCurrentValues().remove(
						saeConfigurationApplicationSecurityConfigurationCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value for Relay State URL List.
	 * 
	 * @param idpAdvanced
	 *            - SAMLV2IDPAdvanced
	 * @param result
	 *            - BindingResult
	 * @param relayStateURLListAddValue
	 *            - list with values to be added
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addRelayStateURLList")
	public void doAddRelayStateURLList(@ModelAttribute("idpAdvanced") @Valid SAMLV2IDPAdvanced idpAdvanced,
			BindingResult result, String relayStateURLListAddValue, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (relayStateURLListAddValue != null && !relayStateURLListAddValue.equals("")) {
			idpAdvanced.getRelayStateURLList().add(relayStateURLListAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value for Relay State URL List.
	 * 
	 * @param idpAdvanced
	 *            - {@link SAMLV2IDPAdvanced}
	 * @param result
	 *            - BindingResult
	 * @param relayStateURLListDeleteValues
	 *            - list with value to be deletetd
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteRelayStateURLList")
	public void doDeleteRelayStateURLList(@ModelAttribute("idpAdvanced") @Valid SAMLV2IDPAdvanced idpAdvanced,
			BindingResult result, String[] relayStateURLListDeleteValues, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (relayStateURLListDeleteValues != null) {
			for (int i = 0; i < relayStateURLListDeleteValues.length; i++) {
				idpAdvanced.getRelayStateURLList().remove(relayStateURLListDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save IDP Advanced Attributes .
	 * 
	 * @param idpAdvanced
	 *            - {@link SAMLV2IDPAdvanced} to be save
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("idpAdvanced") @Valid SAMLV2IDPAdvanced idpAdvanced, BindingResult result,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2IDPEntityProviderService idpEntityProvider = (SAMLV2IDPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		idpEntityProvider.setAdvanced(idpAdvanced);

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, idpEntityProvider);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset edit IDP Advanced Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
