/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Encapsulates data for a SP Entity Provider Service.
 * 
 */
public class SAMLV2SPEntityProviderService extends EntityProviderDescriptor {

	private SAMLV2SPAssertionContent assertionContent;

	private SAMLV2SPAssertionProcessing assertionProcessing;

	private SAMLV2SPServices services;

	private SAMLV2SPAdvanced advanced;

	public SAMLV2SPAssertionContent getAssertionContent() {
		return assertionContent;
	}

	public void setAssertionContent(SAMLV2SPAssertionContent assertionContent) {
		this.assertionContent = assertionContent;
	}

	public SAMLV2SPAssertionProcessing getAssertionProcessing() {
		return assertionProcessing;
	}

	public void setAssertionProcessing(SAMLV2SPAssertionProcessing assertionProcessing) {
		this.assertionProcessing = assertionProcessing;
	}

	public SAMLV2SPServices getServices() {
		return services;
	}

	public void setServices(SAMLV2SPServices services) {
		this.services = services;
	}

	public SAMLV2SPAdvanced getAdvanced() {
		return advanced;
	}

	public void setAdvanced(SAMLV2SPAdvanced advanced) {
		this.advanced = advanced;
	}

	/**
	 * @return {@link SAMLV2SPEntityProviderService} bean created based on the
	 *         given map.
	 * 
	 * @param map
	 *            - map with input data values.
	 * 
	 * @throws UnsupportedEncodingException
	 *             - when input data is not in the correct encoding.
	 */
	public static SAMLV2SPEntityProviderService fromMap(Map<String, Set<String>> map)
			throws UnsupportedEncodingException {

		SAMLV2SPEntityProviderService service = new SAMLV2SPEntityProviderService();

		String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));

		Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);
		Node idpssoConfigNode = XmlUtil.getChildNode(entityConfigDocument.getFirstChild(),
				FederationConstants.SPSSO_CONFIG_NODE);
		service.setMetaAlias(XmlUtil.getNodeAttributeValue(idpssoConfigNode, FederationConstants.META_ALIAS_ATTRIBUTE));

		service.setAdvanced(SAMLV2SPAdvanced.fromMap(map));
		service.setServices(SAMLV2SPServices.fromMap(map));
		service.setAssertionProcessing(SAMLV2SPAssertionProcessing.fromMap(map));
		service.setAssertionContent(SAMLV2SPAssertionContent.fromMap(map));

		return service;

	}

	/**
	 * SAMLV2SPEntityProviderService constructor.
	 */
	public SAMLV2SPEntityProviderService() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_SERVICE_PROVIDER);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put(FederationConstants.META_ALIAS_ATTRIBUTE, getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				FederationConstants.SPSSO_CONFIG_NODE, queryAttributes);
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);

		Map<String, String> descriptorAttributes = new HashMap<String, String>();
		descriptorAttributes.put("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
		String requestSigned = "false";
		String assertionSigned = "false";
		if ("true".equals(assertionContent.getSigningandEncryptionAuthenticationRequestsSigned())) {
			requestSigned = "true";

		}
		if ("true".equals(assertionContent.getSigningandEncryptionAuthenticationAssertionsSigned())) {
			assertionSigned = "true";
		}
		descriptorAttributes.put("AuthnRequestsSigned", requestSigned);
		descriptorAttributes.put("WantAssertionsSigned", assertionSigned);
		XmlUtil.createElement(metadataDocument.getFirstChild(), FederationConstants.SPSSO_DESCRIPTOR_NODE,
				descriptorAttributes);

		assertionContent.addAllDatatoMap(map, this);
		advanced.addAllDatatoMap(map, this);
		services.addAllDatatoMap(map, this);
		assertionProcessing.addAllDatatoMap(map, this);
		

	}

}
