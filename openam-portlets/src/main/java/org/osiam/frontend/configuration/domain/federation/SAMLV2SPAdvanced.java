/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * Encapsulates data for a SP Advanced Bean.
 * 
 */
public class SAMLV2SPAdvanced {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2SPAdvanced.class);

	private String saeConfigurationSPURL;
	private String saeConfigurationSPLogoutURL;
	private List<String> saeConfigurationApplicationSecurityConfigurationCurrentValues = new ArrayList<String>();

	private String ecpConfigurationRequestFinderImplementation;
	private String ecpConfigurationRequestGetComplete;
	private List<String> ecpConfigurationRequestIDPList = new ArrayList<String>();

	private boolean idpProxyEnabled;
	private boolean introductionProxy;
	private String proxyCount;
	private List<String> proxyList = new ArrayList<String>();

	private String sessionSynchronizationEnabled;

	private List<String> relayStateURLList = new ArrayList<String>();

	public String getSaeConfigurationSPURL() {
		return saeConfigurationSPURL;
	}

	public void setSaeConfigurationSPURL(String saeConfigurationSPURL) {
		this.saeConfigurationSPURL = saeConfigurationSPURL;
	}

	public String getSaeConfigurationSPLogoutURL() {
		return saeConfigurationSPLogoutURL;
	}

	public void setSaeConfigurationSPLogoutURL(String saeConfigurationSPLogoutURL) {
		this.saeConfigurationSPLogoutURL = saeConfigurationSPLogoutURL;
	}

	public String getEcpConfigurationRequestFinderImplementation() {
		return ecpConfigurationRequestFinderImplementation;
	}

	public void setEcpConfigurationRequestFinderImplementation(String ecpConfigurationRequestFinderImplementation) {
		this.ecpConfigurationRequestFinderImplementation = ecpConfigurationRequestFinderImplementation;
	}

	public String getEcpConfigurationRequestGetComplete() {
		return ecpConfigurationRequestGetComplete;
	}

	public void setEcpConfigurationRequestGetComplete(String ecpConfigurationRequestGetComplete) {
		this.ecpConfigurationRequestGetComplete = ecpConfigurationRequestGetComplete;
	}

	public List<String> getEcpConfigurationRequestIDPList() {
		return ecpConfigurationRequestIDPList;
	}

	public void setEcpConfigurationRequestIDPList(List<String> ecpConfigurationRequestIDPList) {
		this.ecpConfigurationRequestIDPList = ecpConfigurationRequestIDPList;
	}

	public List<String> getRelayStateURLList() {
		return relayStateURLList;
	}

	public void setRelayStateURLList(List<String> relayStateURLList) {
		this.relayStateURLList = relayStateURLList;
	}

	public List<String> getSaeConfigurationApplicationSecurityConfigurationCurrentValues() {
		return saeConfigurationApplicationSecurityConfigurationCurrentValues;
	}

	public void setSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			List<String> saeConfigurationApplicationSecurityConfigurationCurrentValues) {
		this.saeConfigurationApplicationSecurityConfigurationCurrentValues = saeConfigurationApplicationSecurityConfigurationCurrentValues;
	}

	public String getSessionSynchronizationEnabled() {
		return sessionSynchronizationEnabled;
	}

	public void setSessionSynchronizationEnabled(String sessionSynchronizationEnabled) {
		this.sessionSynchronizationEnabled = sessionSynchronizationEnabled;
	}

	public boolean getIdpProxyEnabled() {
		return idpProxyEnabled;
	}

	public void setIdpProxyEnabled(boolean idpProxy) {
		this.idpProxyEnabled = idpProxy;
	}

	public boolean getIntroductionProxy() {
		return introductionProxy;
	}

	public void setIntroductionProxy(boolean introductionProxy) {
		this.introductionProxy = introductionProxy;
	}

	public String getProxyCount() {
		return proxyCount;
	}

	public void setProxyCount(String proxyCount) {
		this.proxyCount = proxyCount;
	}

	public List<String> getProxyList() {
		return proxyList;
	}

	public void setProxyList(List<String> proxyList) {
		this.proxyList = proxyList;
	}

	/**
	 * @return {@link SAMLV2SPAdvanced} bean created based on the given map.
	 * 
	 * @param map
	 *            - map with input data values.
	 * 
	 * @throws UnsupportedEncodingException
	 *             - when input data is not in the correct encoding.
	 */
	public static SAMLV2SPAdvanced fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2SPAdvanced bean = new SAMLV2SPAdvanced();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.SPSSO_CONFIG_NODE);

		Node configNode = configNodeSet.iterator().next();
		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals("saeSPUrl")) {
				bean.setSaeConfigurationSPURL(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.SAE_CONFIGURATION_APPLICATION_SECURITY_CONFIGURATION_CURRENT_VALUES)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setSaeConfigurationApplicationSecurityConfigurationCurrentValues((valuePair.getValueList()));
			} else if (attributeName.equals("spSessionSyncEnabled")) {
				bean.setSessionSynchronizationEnabled(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.IDP_PROXY_ENABLED)) {
				bean.setIdpProxyEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.IDP_PROXY_COUNT)) {
				bean.setProxyCount(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.IDP_PROXY_LIST)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setProxyList(valuePair.getValueList());
			} else if (attributeName.equals("ECPRequestIDPListFinderImpl")) {
				bean.setEcpConfigurationRequestFinderImplementation(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("ECPRequestIDPListGetComplete")) {
				bean.setEcpConfigurationRequestGetComplete(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("ECPRequestIDPList")) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setEcpConfigurationRequestIDPList(valuePair.getValueList());
			} else if (attributeName.equals("useIntroductionForIDPProxy")) {
				bean.setIntroductionProxy(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals("relayStateUrlList")) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setRelayStateURLList(valuePair.getValueList());
			} else if (attributeName.equals("saeSPLogoutUrl")) {
				bean.setSaeConfigurationSPLogoutURL(XmlUtil.getValueFromAttributeValue(node));
			}
		}
		return bean;
	}

	/**
	 * Add all data {@link Node} to given map.
	 * 
	 * @param map
	 *            - the map to add the nodes;
	 * @param samlv2spEntityProviderService
	 *            - the parent service.
	 */
	public void addAllDatatoMap(Map<String, Document> map, SAMLV2SPEntityProviderService samlv2spEntityProviderService) {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Node idpConfigNode = XmlUtil
				.getChildNode(configDocument.getFirstChild(), FederationConstants.SPSSO_CONFIG_NODE);

		XmlUtil.createAttributeValue(idpConfigNode, FederationConstants.SAE_CONFIGURATION_SP_URL,
				getSaeConfigurationSPURL());
		XmlUtil.createAttributeValue(idpConfigNode, "saeSPLogoutUrl", getSaeConfigurationSPLogoutURL());
		XmlUtil.createAttributeValue(idpConfigNode, "saeAppSecretList",
				getSaeConfigurationApplicationSecurityConfigurationCurrentValues());
		XmlUtil.createAttributeValue(idpConfigNode, "ECPRequestIDPListFinderImpl",
				getEcpConfigurationRequestFinderImplementation());
		XmlUtil.createAttributeValue(idpConfigNode, "ECPRequestIDPListGetComplete",
				getEcpConfigurationRequestGetComplete());
		XmlUtil.createAttributeValue(idpConfigNode, "ECPRequestIDPList", getEcpConfigurationRequestIDPList());
		XmlUtil.createAttributeValue(idpConfigNode, "enableIDPProxy", getIdpProxyEnabled());
		XmlUtil.createAttributeValue(idpConfigNode, "useIntroductionForIDPProxy", getIntroductionProxy());
		XmlUtil.createAttributeValue(idpConfigNode, "idpProxyCount", getProxyCount());
		XmlUtil.createAttributeValue(idpConfigNode, "idpProxyList", getProxyList());
		XmlUtil.createAttributeValue(idpConfigNode, "spSessionSyncEnabled", getSessionSynchronizationEnabled());
		XmlUtil.createAttributeValue(idpConfigNode, "relayStateUrlList", getRelayStateURLList());

	}
}
