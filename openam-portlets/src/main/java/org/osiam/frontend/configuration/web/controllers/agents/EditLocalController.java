/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.LocalAgent;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * EditLocalController shows the edit "Local Agent" (Local Agent attributes)
 * form and does request processing of the edit local agent action.
 * 
 */
@Controller("EditLocalController")
@RequestMapping(value = "view", params = { "ctx=EditLocalController" })
public class EditLocalController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EditLocalController.class);

	@Autowired
	private AgentsService agentsService;

	@Autowired
	@Qualifier("localAgentValidator")
	private Validator localAgentValidator;

	/**
	 * Show edit form for a "Local Agent".
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "localAgent")) {
			String realm = getCurrentRealm(request);
			LocalAgent agent = agentsService.getAttributesForLocalAgent(getSSOToken(realm, request), realm, name);
			agent.setName(name);

			model.addAttribute("localAgent", agent);
		}
		model.addAttribute("name", name);

		return "agents/editAgent";
	}

	/**
	 * Save new value in Agent Root URL for CDSSO list.
	 * 
	 * @param localAgent
	 *            - {@link LocalAgent}
	 * @param result
	 *            - BindingResult
	 * @param agentRootURLforCDSSOAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAgentRootURLforCDSSO")
	public void doAddAgentRootURLforCDSSO(@ModelAttribute("localAgent") @Valid LocalAgent localAgent,
			BindingResult result, String agentRootURLforCDSSOAddValue, ActionRequest request, ActionResponse response) {

		if (agentRootURLforCDSSOAddValue != null && !agentRootURLforCDSSOAddValue.equals("")) {
			localAgent.getAgentRootURLforCDSSO().add(agentRootURLforCDSSOAddValue);
		}

		response.setRenderParameter("name", localAgent.getName());
	}

	/**
	 * Delete values from Agent Root URL for CDSSO list.
	 * 
	 * @param localAgent
	 *            - {@link LocalAgent}
	 * @param result
	 *            - BindingResult
	 * @param agentRootURLforCDSSODeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAgentRootURLforCDSSO")
	public void doDeleteAgentRootURLforCDSSO(@ModelAttribute("localAgent") @Valid LocalAgent localAgent,
			BindingResult result, String[] agentRootURLforCDSSODeleteValues, ActionRequest request,
			ActionResponse response) {

		if (agentRootURLforCDSSODeleteValues != null) {
			for (int i = 0; i < agentRootURLforCDSSODeleteValues.length; i++) {
				localAgent.getAgentRootURLforCDSSO().remove(agentRootURLforCDSSODeleteValues[i]);
			}
		}

		response.setRenderParameter("name", localAgent.getName());
	}

	/**
	 * Save {@link LocalAgent}.
	 * 
	 * @param localAgent
	 *            - {@link LocalAgent}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveLocalAgent(@ModelAttribute("localAgent") @Valid LocalAgent localAgent, BindingResult result,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", localAgent.getName());

		localAgentValidator.validate(localAgent, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveLocalAgentAttributes(token, realm, localAgent);

		response.setRenderParameter("ctx", "AgentsController");
	}

	/**
	 * Reset {@link LocalAgent} form.
	 * 
	 * @param name
	 *            - Agent name.
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetLocalAgent(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
