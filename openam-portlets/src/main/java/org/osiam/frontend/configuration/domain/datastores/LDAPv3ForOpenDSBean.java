/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * LDAPv3ForOpenDSBean object which defines attributes for a "OpenDS"
 * {@link DataStore}.
 * 
 */
public class LDAPv3ForOpenDSBean extends LDAPv3DataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();
		return map;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param map
	 *            - map of attributes.
	 * 
	 */
	public static LDAPv3ForOpenDSBean fromMap(Map<String, Set<String>> map) {
		LDAPv3ForOpenDSBean bean = new LDAPv3ForOpenDSBean();
		LDAPv3ForOpenDSBean.setBaseFields(map, bean);

		return bean;
	}
}
