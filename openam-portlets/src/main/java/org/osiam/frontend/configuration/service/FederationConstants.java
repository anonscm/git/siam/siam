/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

/**
 * Encapsulates federation constants.
 */
public final class FederationConstants {

	private FederationConstants() {

	}

	/**
	 * Circle of trust active status value.
	 */
	public static final String ACTIVE = "active";

	/**
	 * Circle of trust active status key.
	 */
	public static final String STATUS_KEY = "sun-fm-cot-status";

	/**
	 * Circle of trust IDFF writer service URL key.
	 */
	public static final String IDFF_WRITER_SERVICE_URL_KEY = "sun-fm-idff-writerservice-url";

	/**
	 * Circle of trust IDFF reader service URL key.
	 */
	public static final String IDFF_READER_SERVICE_URL_KEY = "sun-fm-idff-readerservice-url";

	/**
	 * Circle of trust saml2 writer service URL key.
	 */
	public static final String SAML2_WRITER_SERVICE_URL_KEY = "sun-fm-saml2-writerservice-url";

	/**
	 * Circle of trust saml2 reader service URL key.
	 */
	public static final String SAML2_READER_SERVICE_URL_KEY = "sun-fm-saml2-readerservice-url";

	/**
	 * Circle of trust description key.
	 */
	public static final String DESCRIPTION_KEY = "sun-fm-cot-description";

	/**
	 * Circle of trust trusted providers key.
	 */
	public static final String TRUSTED_PROVIDERS_KEY = "sun-fm-trusted-providers";

	/**
	 * Realm.
	 */

	public static final String REALM = "singleChoiceRealm";
	/**
	 * Entity Identifier.
	 */

	public static final String ENTITY_IDENTIFIER = "tfEntityId";

	/**
	 * Entity Identifier attribute.
	 */

	public static final String ENTITY_IDENTIFIER_ATTRIBUTE = "entityID";

	/**
	 * Hosted Identifier attribute.
	 */

	public static final String HOSTED_ATTRIBUTE = "hosted";

	/**
	 * Hosted Identifier Value.
	 */

	public static final String HOSTED_VALUE = "Hosted";

	/**
	 * Remote Identifier Value.
	 */

	public static final String REMOTE_VALUE = "Remote";

	/**
	 * Remote Identifier Value.
	 */

	public static final String XMLNS_ATTRIBUTE = "xmlns";

	/**
	 * Meta Alias.
	 */
	public static final String META_ALIAS_ATTRIBUTE = "metaAlias";

	/**
	 * Identity Provider Meta Alias.
	 */

	public static final String IDENTITY_PROVIDER_META_ALIAS = "tfidentityprovider";
	/**
	 * Identity Provider Signing certificate alias.
	 */

	public static final String IDENTITY_PROVIDER_SIGNING_CERTIFICATE_ALIAS = "tfidpscertalias";
	/**
	 * Identity Provider Encryption certificate alias.
	 */

	public static final String IDENTITY_PROVIDER_ENCRYPTION_CERTIFICATE_ALIAS = "tfidpecertalias";
	/**
	 * Service Provider Meta Alias.
	 */

	public static final String SERVICE_PROVIDER_META_ALIAS = "tfserviceprovider";
	/**
	 * Service Provider Signing certificate alias.
	 */

	public static final String SERVICE_PROVIDER_SIGNING_CERTIFICATE_ALIAS = "tfspscertalias";
	/**
	 * Service Provider Encryption certificate alias.
	 */

	public static final String SERVICE_PROVIDER_ENCRYPTION_CERTIFICATE_ALIAS = "tfspecertalias";
	/**
	 * Attribute Query Provider Meta Alias.
	 */

	public static final String ATTRIBUTE_QUERY_PROVIDER_META_ALIAS = "tfattrqueryprovider";
	/**
	 * Attribute Query Provider Signing certificate alias.
	 */

	public static final String ATTRIBUTE_QUERY_PROVIDER_SIGNING_CERTIFICATE_ALIAS = "tfattrqscertalias";
	/**
	 * Attribute Query Provider Encryption certificate alias.
	 */

	public static final String ATTRIBUTE_QUERY_PROVIDER_ENCRYPTION_CERTIFICATE_ALIAS = "tfattrqecertalias";
	/**
	 * Attribute Authority Meta Alias.
	 */

	public static final String ATTRIBUTE_AUTHORITY_META_ALIAS = "tfattrauthority";
	/**
	 * Attribute Authority Signing certificate alias.
	 */

	public static final String ATTRIBUTE_AUTHORITY_SIGNING_CERTIFICATE_ALIAS = "tfattrascertalias";
	/**
	 * Attribute Authority Encryption certificate alias.
	 */

	public static final String ATTRIBUTE_AUTHORITY_ENCRYPTION_CERTIFICATE_ALIAS = "tfattraecertalias";
	/**
	 * Authentication Authority Meta Alias.
	 */

	public static final String AUTHENTICATION_AUTHORITY_META_ALIAS = "tfauthnauthority";
	/**
	 * Authentication Authority Signing certificate alias.
	 */

	public static final String AUTHENTICATION_AUTHORITY_SIGNING_CERTIFICATE_ALIAS = "tfauthnascertalias";
	/**
	 * Authentication Authority Encryption certificate alias.
	 */

	public static final String AUTHENTICATION_AUTHORITY_ENCRYPTION_CERTIFICATE_ALIAS = "tfauthnaecertalias";
	/**
	 * XACML Policy Enforcement Point Meta Alias.
	 */

	public static final String XACML_POLICY_ENFORCEMENT_POINT_META_ALIAS = "tfxacmlpep";
	/**
	 * XACML Policy Enforcement Point Signing certificate alias.
	 */

	public static final String XACML_POLICY_ENFORCEMENT_POINT_SIGNING_CERTIFICATE_ALIAS = "tfxacmlpepscertalias";
	/**
	 * XACML Policy Enforcement Point Encryption certificate alias.
	 */

	public static final String XACML_POLICY_ENFORCEMENT_POINT_ENCRYPTION_CERTIFICATE_ALIAS = "tfxacmlpepecertalias";
	/**
	 * XACML Policy Decision Point Meta Alias.
	 */

	public static final String XACML_POLICY_DECISION_POINT_META_ALIAS = "tfxacmlpdp";
	/**
	 * XACML Policy Decision Point Signing certificate alias.
	 */

	public static final String XACML_POLICY_DECISION_POINT_SIGNING_CERTIFICATE_ALIAS = "tfxacmlpdpscertalias";

	/**
	 * XACML Policy Decision Point Encryption certificate alias.
	 */
	public static final String XACML_POLICY_DECISION_POINT_ENCRYPTION_CERTIFICATE_ALIAS = "tfxacmlpdpecertalias";

	/**
	 * Authentication Request Signed.
	 */

	public static final String AUTHENTICATION_REQUEST_SIGNED = "WantAuthnRequestsSigned";

	/**
	 * Authentication Request Signed.
	 */

	public static final String AUTHENTICATION_REQUEST_SIGNED_SP = "AuthnRequestsSigned";

	/**
	 * Artifact Resolve Signed.
	 */

	public static final String ARTIFACT_RESOLVE_SIGNED = "wantArtifactResolveSigned";

	/**
	 * Artifact Response Signed.
	 */

	public static final String ARTIFACT_RESPONSE_SIGNED = "wantArtifactResponseSigned";

	/**
	 * Logout Request Signed.
	 */

	public static final String LOGOUT_REQUEST_SIGNED = "wantLogoutRequestSigned";
	/**
	 * Logout Response Signed.
	 */

	public static final String LOGOUT_RESPONSE_SIGNED = "wantLogoutResponseSigned";
	/**
	 * Manage Name ID Request Signed.
	 */

	public static final String MANAGE_NAME_ID_REQUEST_SIGNED = "wantMNIRequestSigned";
	/**
	 * Manage Name ID Response Signed.
	 */

	public static final String MANAGE_NAME_ID_RESPONSE_SIGNED = "wantMNIResponseSigned";
	/**
	 * NameID Encryption Signed.
	 */

	public static final String NAMEID_ENCRYPTION_SIGNED = "wantNameIDEncrypted";
	/**
	 * Certificate Aliases Signing.
	 */

	public static final String CERTIFICATE_ALIASES_SIGNING = "signingCertAlias";
	/**
	 * Certificate Aliases Encryption.
	 */

	public static final String CERTIFICATE_ALIASES_ENCRYPTION = "encryptionCertAlias";

	/**
	 * attributeAuthorityMapper.
	 */

	public static final String DEFAULT_ATTRIBUTE_AUTHORITY_MAPPER = "default_attributeAuthorityMapper";

	/**
	 * Certificate Aliases Key Size.
	 */

	public static final String CERTIFICATE_ALIASES_KEY_SIZE = "keySize";
	/**
	 * Certificate Aliases Algorithm.
	 */

	public static final String CERTIFICATE_ALIASES_ALGORITHM = "Algorithm";
	/**
	 * NameID Format NameID Format List.
	 */

	public static final String NAMEID_FORMAT_NAMEID_FORMAT_LIST = "nameidlist";
	/**
	 * NameID Value Map Current Values.
	 */

	public static final String NAMEID_VALUE_MAP_CURRENT_VALUES = "nameIDFormatMap";
	/**
	 * Authentication Context Mapper.
	 */

	public static final String AUTHENTICATION_CONTEXT_MAPPER = "idpAuthncontextMapper";
	/**
	 * Default Authentication Context.
	 */

	public static final String DEFAULT_AUTHENTICATION_CONTEXT = "idpDefaultAuthnContext";
	/**
	 * Not Before Time Skew.
	 */

	public static final String NOT_BEFORE_TIME_SKEW = "assertionNotBeforeTimeSkew";
	/**
	 * Effective Time.
	 */

	public static final String EFFECTIVE_TIME = "assertionEffectiveTime";
	/**
	 * Basic authentication setting for Soap based binding Enabled.
	 */

	public static final String BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED = "basicAuthOn";
	/**
	 * Basic authentication setting for Soap based binding Enabled User Name.
	 */

	public static final String BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_USER_NAME = "basicAuthUser";
	/**
	 * Basic authentication setting for Soap based binding Enabled Password.
	 */

	public static final String BASIC_AUTHENTICATION_SETTING_FOR_SOAP_BASED_BINDING_ENABLED_PASSWORD = "basicAuthPassword";
	/**
	 * Assertion Cache Enabled.
	 */

	public static final String ASSERTION_CACHE_ENABLED = "assertionCacheEnabled";
	/**
	 * Discovery Bootstrapping Enabled.
	 */

	public static final String DISCOVERY_BOOTSTRAPPING_ENABLED = "discoveryBootstrappingEnabled";

	/**
	 * idpAuthncontextClassrefMapping.
	 */
	public static final String IDP_AUTHCONTEXT_CLASSREF_MAPPING = "idpAuthncontextClassrefMapping";

	/**
	 * spAuthncontextClassrefMapping.
	 */
	public static final String SP_AUTHCONTEXT_CLASSREF_MAPPING = "spAuthncontextClassrefMapping";

	/**
	 * Attribute Mapper.
	 */

	public static final String ATTRIBUTE_MAPPER = "idpAttributeMapper";
	/**
	 * Attribute Map Current Values.
	 */

	public static final String ATTRIBUTE_MAP_CURRENT_VALUES = "attributeMap";
	/**
	 * Account Mapper.
	 */

	public static final String ACCOUNT_MAPPER = "idpAccountMapper";
	/**
	 * Local Configuration Auth URL.
	 */

	public static final String LOCAL_CONFIGURATION_AUTH_URL = "AuthUrl";
	/**
	 * Local Configuration External Application Logout URL.
	 */

	public static final String LOCAL_CONFIGURATION_EXTERNAL_APPLICATION_LOGOUT_URL = "appLogoutUrl";
	/**
	 * IDP Service Attributes Artifact Resolution Service Default.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_ARTIFACT_RESOLUTION_SERVICE_DEFAULT = "isDefault";
	/**
	 * IDP Service Attributes Artifact Resolution Service Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_ARTIFACT_RESOLUTION_SERVICE_LOCATION = "artLocation";
	/**
	 * IDP Service Attributes Artifact Resolution Service Index.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_ARTIFACT_RESOLUTION_SERVICE_INDEX = "index";
	/**
	 * IDP Service Attributes Single Logout Service Default.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_DEFAULT = "singleChoiceSingleLogOutProfile";
	/**
	 * IDP Service Attributes Single Logout Service HTTP REDIRECT Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_HTTP_REDIRECT_LOCATION = "slohttpLocation";
	/**
	 * IDP Service Attributes Single Logout Service HTTP REDIRECT Response.
	 * Location
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_HTTP_REDIRECT_RESPONSE_LOCATION = "slohttpResponseLocation";
	/**
	 * IDP Service Attributes Single Logout Service POST Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_POST_LOCATION = "slopostLocation";
	/**
	 * IDP Service Attributes Single Logout Service POST Response Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_POST_RESPONSE_LOCATION = "slopostResponseLocation";
	/**
	 * IDP Service Attributes Single Logout Service SOAP location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_LOGOUT_SERVICE_SOAP_LOCATION = "slosoapLocation";
	/**
	 * IDP Service Attributes Manage NameID Service HTTP REDIRECT Default.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_HTTP_REDIRECT_DEFAULT = "singleChoiceMangeNameIDProfile";
	/**
	 * IDP Service Attributes Manage NameID Service HTTP REDIRECT Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_HTTP_REDIRECT_LOCATION = "mnihttpLocation";
	/**
	 * IDP Service Attributes Manage NameID Service HTTP REDIRECT Response.
	 * Location
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_HTTP_REDIRECT_RESPONSE_LOCATION = "mnihttpResponseLocation";
	/**
	 * IDP Service Attributes Manage NameID Service POST Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_POST_LOCATION = "mnipostLocation";
	/**
	 * IDP Service Attributes Manage NameID Service POST Response Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_POST_RESPONSE_LOCATION = "mnipostResponseLocation";
	/**
	 * IDP Service Attributes Manage NameID Service SOAP Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_MANAGE_NAMEID_SERVICE_SOAP_LOCATION = "mnisoapLocation";
	/**
	 * IDP Service Attributes Single SignOn Service HTTP REDIRECT Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_SIGNON_SERVICE_HTTP_REDIRECT_LOCATION = "ssohttpLocation";
	/**
	 * IDP Service Attributes Single SignOn Service POST Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_SIGNON_SERVICE_POST_LOCATION = "ssosoapLocation";
	/**
	 * IDP Service Attributes Single SignOn Service SOAP Location.
	 */

	public static final String IDP_SERVICE_ATTRIBUTES_SINGLE_SIGNON_SERVICE_SOAP_LOCATION = "ssosoapsLocation";

	/**
	 * IDP Proxy.
	 */
	public static final String IDP_PROXY_ENABLED = "enableIDPProxy";

	/**
	 * IDP Proxy Introduction.
	 */
	public static final String IDP_PROXY_INTRODUCTION = "useIntroductionForIDPProxy";

	/**
	 * IDP Proxy Count.
	 */
	public static final String IDP_PROXY_COUNT = "idpProxyCount";

	/**
	 * IDP Proxy List.
	 */
	public static final String IDP_PROXY_LIST = "idpProxyList";

	/**
	 * sae Configuration IDP URL.
	 */
	public static final String SAE_CONFIGURATION_IDP_URL = "saeIDPUrl";

	/**
	 * sae Configuration SP URL.
	 */
	public static final String SAE_CONFIGURATION_SP_URL = "saeSPUrl";

	/**
	 * SAMLv2 IDP advanced relay state url list.
	 */
	public static final String RELAY_STATE_IDP_URL_LIST = "relayStateUrlList";

	/**
	 * sae Configuration Application Security Configuration Current Values.
	 */
	public static final String SAE_CONFIGURATION_APPLICATION_SECURITY_CONFIGURATION_CURRENT_VALUES = "saeAppSecretList";

	/**
	 * ECP Configuration IDP Session Mapper.
	 */
	public static final String ECP_CONFIGURATION_IDP_SESSION_MAPPER = "idpECPSessionMapper";

	/**
	 * Session Synchronization Enabled.
	 */

	public static final String SESSION_SYNCHRONIZATION_ENABLED = "idpSessionSyncEnabled";
	/**
	 * IDP Finder Implementation IDP Finder implementation class.
	 */

	public static final String IDP_FINDER_IMPLEMENTATION_IDP_FINDER_IMPLEMENTATION_CLASS = "proxyIDPFinderClass";
	/**
	 * IDP Finder Implementation IdP Finder JSP.
	 */

	public static final String IDP_FINDER_IMPLEMENTATION_IDP_FINDER_JSP = "proxyIDPFinderJSP";
	/**
	 * IDP Finder Implementation Enable Proxy IDP Finder for all SPs Enabled.
	 */

	public static final String IDP_FINDER_IMPLEMENTATION_ENABLE_PROXY_IDP_FINDER_FOR_ALL_SPS_ENABLED = "enableProxyIDPFinderForAllSPs";
	/**
	 * Relay State URL List Current Values.
	 */

	public static final String RELAY_STATE_URL_LIST_CURRENT_VALUES = "relayStateUrlList";
	/**
	 * Provider Type.
	 */

	public static final String PROVIDER_TYPE = "tfProviderType";
	/**
	 * Description.
	 */

	public static final String DESCRIPTION = "providerDescription";
	/**
	 * Protocol Support Enumeration Current Values.
	 */

	public static final String PROTOCOL_SUPPORT_ENUMERATION_CURRENT_VALUES = "txtProtocolSupportEnum";
	/**
	 * Name Identifier Encryption Enabled.
	 */

	public static final String NAME_IDENTIFIER_ENCRYPTION_ENABLED = "enableNameIDEncryption";
	/**
	 * Certificate Aliases Sign Certificate Alias.
	 */

	public static final String CERTIFICATE_ALIASES_SIGN_CERTIFICATE_ALIAS = "signingCertAlias";
	/**
	 * Certificate Aliases Encryption Cert Alias.
	 */

	public static final String CERTIFICATE_ALIASES_ENCRYPTION_CERT_ALIAS = "encryptionCertAlias";
	/**
	 * Certificate Aliases Key Size.
	 */

	public static final String ENCRYPTION_CERTIFICATE_ALIASES_KEY_SIZE = "encryptionKeySize";
	/**
	 * Certificate Aliases Algorithm.
	 */

	public static final String ENCRYPTION_CERTIFICATE_ALIASES_ALGORITHM = "encryptionAlgorithm";
	/**
	 * Communication URLs SOAP Endpoint.
	 */

	public static final String COMMUNICATION_URLS_SOAP_ENDPOINT = "tfSOAPEndpointURL";
	/**
	 * Communication URLs Single Sign On Service URL.
	 */

	public static final String COMMUNICATION_URLS_SINGLE_SIGN_ON_SERVICE_URL = "tfSingleSignOnServiceURL";
	/**
	 * Communication URLs Single Logout Service.
	 */

	public static final String COMMUNICATION_URLS_SINGLE_LOGOUT_SERVICE = "tfSingleLogoutServiceURL";
	/**
	 * Communication URLs Single Logout Return.
	 */

	public static final String COMMUNICATION_URLS_SINGLE_LOGOUT_RETURN = "tfSingleLogoutReturnURL";
	/**
	 * Communication URLs Federation Termination Service.
	 */

	public static final String COMMUNICATION_URLS_FEDERATION_TERMINATION_SERVICE = "tfFederationTerminationServiceURL";
	/**
	 * Communication URLs Federation Termination Return.
	 */

	public static final String COMMUNICATION_URLS_FEDERATION_TERMINATION_RETURN = "tfFederationTerminationReturnURL";
	/**
	 * Communication URLs Name Registration Service.
	 */

	public static final String COMMUNICATION_URLS_NAME_REGISTRATION_SERVICE = "tfNameRegistrationServiceURL";
	/**
	 * Communication URLs Name Registration Return.
	 */

	public static final String COMMUNICATION_URLS_NAME_REGISTRATION_RETURN = "tfNameRegistrationReturnURL";
	/**
	 * Communication Profiles Federation Termination.
	 */

	public static final String COMMUNICATION_PROFILES_FEDERATION_TERMINATION = "singleChoiceFederationTerminationProfile";
	/**
	 * Communication Profiles Single Logout.
	 */

	public static final String COMMUNICATION_PROFILES_SINGLE_LOGOUT = "singleChoiceSingleLogoutProfile";
	/**
	 * Communication Profiles Name Registration.
	 */

	public static final String COMMUNICATION_PROFILES_NAME_REGISTRATION = "singleChoiceNameRegistrationProfile";
	/**
	 * Communication Profiles Single Signon/Federation.
	 */

	public static final String COMMUNICATION_PROFILES_SINGLE_SIGNON_FEDERATION = "singleChoiceFederationProfile";
	/**
	 * Identity Provider Configuration Provider Alias.
	 */

	public static final String IDENTITY_PROVIDER_CONFIGURATION_PROVIDER_ALIAS = "tfAlias";
	/**
	 * Identity Provider Configuration Authentication Type.
	 */

	public static final String IDENTITY_PROVIDER_CONFIGURATION_AUTHENTICATION_TYPE = "authType";
	/**
	 * Identity Provider Configuration Assertion Issuer.
	 */

	public static final String IDENTITY_PROVIDER_CONFIGURATION_ASSERTION_ISSUER = "assertionIssuer";
	/**
	 * Identity Provider Configuration Responds With.
	 */

	public static final String IDENTITY_PROVIDER_CONFIGURATION_RESPONDS_WITH = "responsdWith";
	/**
	 * Identity Provider Configuration Provider Status.
	 */

	public static final String IDENTITY_PROVIDER_CONFIGURATION_PROVIDER_STATUS = "providerStatus";
	/**
	 * Service URL Home Page URL.
	 */

	public static final String SERVICE_URL_HOME_PAGE_URL = "providerHomePageURL";
	/**
	 * Service URL Single Sign On Failure Redirect URL.
	 */

	public static final String SERVICE_URL_SINGLE_SIGN_ON_FAILURE_REDIRECT_URL = "ssoFailureRedirectURL";
	/**
	 * Service URL Federate Page URL.
	 */

	public static final String SERVICE_URL_FEDERATE_PAGE_URL = "doFederatePageURL";
	/**
	 * Service URL Registration Done URL.
	 */

	public static final String SERVICE_URL_REGISTRATION_DONE_URL = "registrationDoneURL";
	/**
	 * Service URL List of COTs Page URL.
	 */

	public static final String SERVICE_URL_LIST_OF_COTS_PAGE_URL = "listOfCOTsPageURL";
	/**
	 * Service URL Federation Done URL.
	 */

	public static final String SERVICE_URL_FEDERATION_DONE_URL = "federationDoneURL";
	/**
	 * Service URL Termination Done URL.
	 */

	public static final String SERVICE_URL_TERMINATION_DONE_URL = "terminationDoneURL";
	/**
	 * Service URL Error Page URL.
	 */

	public static final String SERVICE_URL_ERROR_PAGE_URL = "errorPageURL";
	/**
	 * Service URL Logout Done URL.
	 */

	public static final String SERVICE_URL_LOGOUT_DONE_URL = "logoutDoneURL";
	/**
	 * Plugins Name Identifier Implementation.
	 */

	public static final String PLUGINS_NAME_IDENTIFIER_IMPLEMENTATION = "nameIDImplementationClass";
	/**
	 * Plugins Attribute Statement Plug in.
	 */

	public static final String PLUGINS_ATTRIBUTE_STATEMENT_PLUG_IN = "attributePlugin";
	/**
	 * Plugins User Provider Class.
	 */

	public static final String PLUGINS_USER_PROVIDER_CLASS = "userProviderClass";
	/**
	 * Identity Provider Attribute MapperAttribute Mapper Class.
	 */

	public static final String IDENTITY_PROVIDER_ATTRIBUTE_MAPPERATTRIBUTE_MAPPER_CLASS = "attributeMapperClass";
	/**
	 * Identity Provider Attribute Mapper Identity Provider Attribute Mapping.
	 * Current Values
	 */

	public static final String IDENTITY_PROVIDER_ATTRIBUTE_MAPPER_IDENTITY_PROVIDER_ATTRIBUTE_MAPPING_CURRENT_VALUES = "idpAttributeMap";
	/**
	 * Bootstrapping Generate Discovery Bootstrapping Resource Offering.
	 */

	public static final String BOOTSTRAPPING_GENERATE_DISCOVERY_BOOTSTRAPPING_RESOURCE_OFFERING = "generateBootstrapping";
	/**
	 * Auto Federation Enabled.
	 */

	public static final String AUTO_FEDERATION_ENABLED = "enableAutoFederation";
	/**
	 * Auto Federation Common Attribute Name.
	 */

	public static final String AUTO_FEDERATION_COMMON_ATTRIBUTE_NAME = "autoFederationAttribute";
	/**
	 * Authentication Context Default Authentication Context.
	 */

	public static final String AUTHENTICATION_CONTEXT_DEFAULT_AUTHENTICATION_CONTEXT = "defaultAuthnContext";
	/**
	 * saml Attributes Assertion Interval.
	 */

	public static final String SAML_ATTRIBUTES_ASSERTION_INTERVAL = "assertionInterval";
	/**
	 * saml Attributes Cleanup Interval.
	 */

	public static final String SAML_ATTRIBUTES_CLEANUP_INTERVAL = "cleanupInterval";
	/**
	 * saml Attributes Artifact Timeout.
	 */

	public static final String SAML_ATTRIBUTES_ARTIFACT_TIMEOUT = "artifactTimeout";
	/**
	 * saml Attributes Assertion Limit.
	 */

	public static final String SAML_ATTRIBUTES_ASSERTION_LIMIT = "assertionLimit";
	/**
	 * Display Name.
	 */

	public static final String DISPLAY_NAME = "displayName";
	/**
	 * Token Issuer Name.
	 */

	public static final String TOKEN_ISSUER_NAME = "TokenIssuerName";
	/**
	 * Token Issuer Endpoint.
	 */

	public static final String TOKEN_ISSUER_ENDPOINT = "TokenIssuerEndpoint";
	/**
	 * Signing Cert Alias.
	 */

	public static final String SIGNING_CERT_ALIAS = "signingCertAlias";
	/**
	 * Claim Types.
	 */

	public static final String CLAIM_TYPES = "claimTypeOffered";
	/**
	 * Name Identifier and Mappings Name ID Format.
	 */

	public static final String NAME_IDENTIFIER_AND_MAPPINGS_NAME_ID_FORMAT = "nameIdFormat";
	/**
	 * Name Identifier and Mappings Name ID Attribute.
	 */

	public static final String NAME_IDENTIFIER_AND_MAPPINGS_NAME_ID_ATTRIBUTE = "nameIdAttribute";
	/**
	 * Name Identifier and Mappings Name Includes Domain.
	 */

	public static final String NAME_IDENTIFIER_AND_MAPPINGS_NAME_INCLUDES_DOMAIN = "nameIncludesDomain";
	/**
	 * Name Identifier and Mappings Domain Attribute.
	 */

	public static final String NAME_IDENTIFIER_AND_MAPPINGS_DOMAIN_ATTRIBUTE = "domainAttribute";
	/**
	 * Name Identifier and Mappings UPN Domain.
	 */

	public static final String NAME_IDENTIFIER_AND_MAPPINGS_UPN_DOMAIN = "upnDomain";

	/**
	 * Assertion Effective Time.
	 */

	public static final String ASSERTION_EFFECTIVE_TIME = "assertionEffectiveTime";

	/**
	 * Binding Attribute Key.
	 */
	public static final String BINDING_ATTRIBUTE = "Binding";

	/**
	 * HTTP Redirect Binding Attribute Key.
	 */
	public static final String HTTP_REDIRECT_BINDING_ATTRIBUTE = "urnoasisnamestcSAML2.0bindingsHTTP-Redirect";

	/**
	 * HTTP Post Binding Attribute Key.
	 */
	public static final String HTTP_POST_BINDING_ATTRIBUTE = "urnoasisnamestcSAML2.0bindingsHTTP-POST";

	/**
	 * SOAP Binding Attribute Key.
	 */
	public static final String SOAP_BINDING_ATTRIBUTE = "urnoasisnamestcSAML2.0bindingsSOAP";

	/**
	 * SAML2 Metadata Attribute Key.
	 */
	public static final String SAML2_METADATA_ATTRIBUTE_KEY = "sun-fm-saml2-metadata";

	/**
	 * IDFF Metadata Attribute Key.
	 */
	public static final String IDFF_METADATA_ATTRIBUTE_KEY = "sun-fm-idff-metadata";

	/**
	 * IDPSSO Config Node.
	 */
	public static final String IDPSSO_CONFIG_NODE = "IDPSSOConfig";

	/**
	 * SP SSO Config Node.
	 */
	public static final String SPSSO_CONFIG_NODE = "SPSSOConfig";

	/**
	 * XACML PEP Config Node.
	 */
	public static final String XACML_PEP_CONFIG_NODE = "XACMLAuthzDecisionQueryConfig";

	/**
	 * XACML PEP Descriptor Node.
	 */
	public static final String XACML_PEP_DESCRIPTOR_NODE = "XACMLAuthzDecisionQueryDescriptor";

	/**
	 * XACML PDP Config Node.
	 */
	public static final String XACML_PDP_CONFIG_NODE = "XACMLPDPConfig";

	/**
	 * Attribute Authority Node.
	 */
	public static final String ATTRIBUTE_AUTHORITY_DESCRIPTOR_NODE = "AttributeAuthorityDescriptor";

	/**
	 * Attribute Authority Node.
	 */
	public static final String ATTRIBUTE_AUTHORITY_CONFIG_NODE = "AttributeAuthorityConfig";

	/**
	 * Attribute Query Node.
	 */
	public static final String ATTRIBUTE_QUERY_CONFIG_NODE = "AttributeQueryConfig";

	/**
	 * AuthnAuthorityConfig Node.
	 */
	public static final String AUTHN_AUTORITY_CONFIG_NODE = "AuthnAuthorityConfig";

	/**
	 * Attribute Authority Node.
	 */
	public static final String AUTHN_AUTORITY_DESCRIPTOR_NODE = "AuthnAuthorityDescriptor";

	/**
	 * XACML PDP Descriptor Node.
	 */
	public static final String XACML_PDP_DESCRIPTOR_NODE = "XACMLPDPDescriptor";

	/**
	 * IDPSSO Descriptor Node.
	 */
	public static final String IDPSSO_DESCRIPTOR_NODE = "IDPSSODescriptor";

	/**
	 * SPSSO Descriptor Node.
	 */
	public static final String SPSSO_DESCRIPTOR_NODE = "SPSSODescriptor";

	/**
	 * Artifact Consumer Service Node.
	 */
	public static final String ARTIFACT_CONSUMER_SERVICE_NODE = "AssertionConsumerService";

	/**
	 * Artifact Resolution Service Node.
	 */
	public static final String ARTIFACT_RESOLUTION_SERVICE_NODE = "ArtifactResolutionService";

	/**
	 * Location Attribute Node.
	 */
	public static final String LOCATION_ATTRIBUTE = "Location";

	/**
	 * Response Location Attribute Node.
	 */
	public static final String RESPONSE_LOCATION_ATTRIBUTE = "ResponseLocation";

	/**
	 * Is Default Attribute.
	 */
	public static final String IS_DEFAULT_ATTRIBUTE = "isDefault";

	/**
	 * Index Attribute.
	 */
	public static final String INDEX_ATTRIBUTE = "index";

	/**
	 * Single Logout Service Node.
	 */
	public static final String SINGLE_LOGOUT_SERVICE_NODE = "SingleLogoutService";

	/**
	 * Single Sign On Service Node.
	 */
	public static final String SINGLE_SIGN_ON_SERVICE_NODE = "SingleSignOnService";

	/**
	 * NameIDMapping Service Node.
	 */
	public static final String NAME_ID_MAPPING_SERVICE_NODE = "NameIDMappingService";

	/**
	 * Name ID Format Node.
	 */
	public static final String NAME_ID_FORMAT_NODE = "NameIDFormat";

	/**
	 * Manage ID Service Node.
	 */
	public static final String MANAGE_NAME_ID_SERVICE_NODE = "ManageNameIDService";

	/**
	 * Manage ID Service Node.
	 */
	public static final String AUTO_FED_ATTRIBUTE = "autofedAttribute";

	/**
	 * Authentication Context - Authentication Context List.
	 */
	public static final String IDP_AUTHN_CONTEXT_CLASS_REF_MAPPING = "idpAuthncontextClassrefMapping";

	/**
	 * SAML2 Entity Config Attributes XML Key.
	 */
	public static final String SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY = "sun-fm-saml2-entityconfig";

	/**
	 * SAML2 Entity Config Attributes XML Key.
	 */
	public static final String WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY = "sun-fm-wsfederation-entityconfig";

	/**
	 * WSFED Entity Config Attributes XML Key.
	 */
	public static final String WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY = "sun-fm-wsfederation-metadata";

	/**
	 * IDFF Entity Config Attributes XML Key.
	 */
	public static final String IDFF_ENTITY_PROVIDER_ATTRIBUTE_KEY = "sun-fm-idff-entityconfig";

	/**
	 * Entity Provider Type Value.
	 */
	public static final String ENTITY_PROVIDER_WSFED_PROTOCOL = "wsfed";

	/**
	 * Entity Provider Type Value.
	 */
	public static final String ENTITY_PROVIDER_SAMLV2_PROTOCOL = "samlv2";

	/**
	 * Entity Provider Type Value (use for circle of trust).
	 */
	public static final String ENTITY_PROVIDER_SAML2_PROTOCOL = "saml2";

	/**
	 * Signing Key Alias.
	 */

	public static final String SIGNING_KEY_ALIAS = "signingCertAlias";
	/**
	 * Encryption Key Alias.
	 */

	public static final String ENCRYPTION_KEY_ALIAS = "encryptionCertAlias";
	/**
	 * Basic Authorization Enabled.
	 */

	public static final String BASIC_AUTHORIZATION_ENABLED = "basicAuthOn";
	/**
	 * Basic Authorization User.
	 */

	public static final String BASIC_AUTHORIZATION_USER = "basicAuthUser";
	/**
	 * Basic Authorization Password.
	 */

	public static final String BASIC_AUTHORIZATION_PASSWORD = "basicAuthPassword";
	/**
	 * Authorization Decision Query Signed.
	 */

	public static final String AUTHORIZATION_DECISION_QUERY_SIGNED = "wantXACMLAuthzDecisionQuerySigned";
	/**
	 * Authorization Service Location.
	 */

	public static final String AUTHORIZATION_SERVICE_LOCATION = "XACMLAuthzServiceLocation";

	/**
	 * Authorization Decision Response Signed.
	 */

	public static final String AUTHORIZATION_DECISION_RESPONSE_SIGNED = "wantXACMLAuthzDecisionResponseSigned";

	/**
	 * Assertion Encrypted.
	 */

	public static final String ASSERTION_ENCRYPTED = "wantAssertionEncrypted";

	/**
	 * Attribute Service Basic Location.
	 */

	public static final String ATTRIBUTE_SERVICE_BASIC_LOCATION = "attrSerdefaultLocation";
	/**
	 * Attribute Service Basic Default Mapper.
	 */

	public static final String ATTRIBUTE_SERVICE_BASIC_DEFAULT_MAPPER = "default_attributeAuthorityMapper";
	/**
	 * Attribute Service X509 Supports Service.
	 */

	public static final String ATTRIBUTE_SERVICE_X509_SUPPORTS_SERVICE = "supportsx";
	/**
	 * Attribute Service X509 Location.
	 */

	public static final String ATTRIBUTE_SERVICE_X509_LOCATION = "attrSerLocation";
	/**
	 * Attribute Service X509 X.509 Mapper.
	 */

	public static final String ATTRIBUTE_SERVICE_X509_X509_MAPPER = "x509Subject_attributeAuthorityMapper";
	/**
	 * AssertionID Request Soap Location.
	 */

	public static final String ASSERTIONID_REQUEST_SOAP_LOCATION = "soapAssertionidrequest";
	/**
	 * AssertionID Request URI Location.
	 */

	public static final String ASSERTIONID_REQUEST_URI_LOCATION = "uriAssertionIDRequest";
	/**
	 * AssertionID Request Mapper.
	 */

	public static final String ASSERTIONID_REQUEST_MAPPER = "assertionIDRequestMapper";
	/**
	 * Attribute Profile.
	 */

	public static final String ATTRIBUTE_PROFILE = "AttributeProfile";
	/**
	 * Cert Aliases, Signing and Encrytpion Signing.
	 */

	public static final String CERT_ALIASES_SIGNING_AND_ENCRYTPION_SIGNING = "signingCertAlias";
	/**
	 * Cert Aliases, Signing and Encrytpion Encryption.
	 */

	public static final String CERT_ALIASES_SIGNING_AND_ENCRYTPION_ENCRYPTION = "encryptionCertAlias";
	/**
	 * Subject Data Store.
	 */

	public static final String SUBJECT_DATA_STORE = "x509SubjectDataStoreAttrName";

	/**
	 * NameID Format - Current Values.
	 */

	public static final String NAMEID_FORMAT_CURRENT_VALUES = "attrnameidlist";

	/**
	 * Authn Query Service URL.
	 */

	public static final String AUTHN_QUERY_SERVICE_URL = "authnQueryServLocation";

	/**
	 * SP Display Name.
	 */

	public static final String SP_DISPLAY_NAME = "displayName";
	/**
	 * IDP Display Name.
	 */

	public static final String IDP_DISPLAY_NAME = "idpdisplayName";

	/**
	 * Signing Cert Alias Alias.
	 */

	public static final String SIGNING_CERT_ALIAS_ALIAS = "WSFedIDP.signingCertAlias";

	/**
	 * Assertion Signed.
	 */

	public static final String ASSERTION_SIGNED = "wantAssertionSigned";
	/**
	 * Account Mapper.
	 */

	public static final String SP_ACCOUNT_MAPPER = "spAccountMapper";
	/**
	 * Attribute Mapper.
	 */

	public static final String SP_ATTRIBUTE_MAPPER = "spAttributeMapper";

	/**
	 * Assertion Time Skew.
	 */

	public static final String ASSERTION_TIME_SKEW = "assertionTimeSkew";
	/**
	 * Default Relay State.
	 */

	public static final String DEFAULT_RELAY_STATE = "defaultRelayState";
	/**
	 * Home Realm Discovery Service.
	 */

	public static final String HOME_REALM_DISCOVERY_SERVICE = "HomeRealmDiscoveryService";
	/**
	 * Account Realm Selection Radio Cookie.
	 */

	public static final String ACCOUNT_REALM_SELECTION_RADIO_COOKIE = "AccountRealmSelection";
	/**
	 * Account Realm Selection Radio UserAgentKey.
	 */

	public static final String ACCOUNT_REALM_SELECTION_RADIO_USERAGENTKEY = "AccountRealmSelection";
	/**
	 * Account Realm Selection Cookie Name.
	 */

	public static final String ACCOUNT_REALM_SELECTION_COOKIE_NAME = "AccountRealmCookieName";
	/**
	 * Account Realm Selection UserAgentKey.
	 */

	public static final String ACCOUNT_REALM_SELECTION_USERAGENTKEY = "UserAgentKey";

	/**
	 * Signing and Encryption Authentication Requests Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_AUTHENTICATION_REQUESTS_SIGNED = "AuthnRequestsSigned";
	/**
	 * Signing and Encryption Authentication Assertions Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_AUTHENTICATION_ASSERTIONS_SIGNED = "WantAssertionsSigned";
	/**
	 * Signing and Encryption Post Response Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_POST_RESPONSE_SIGNED = "wantPOSTResponseSigned";
	/**
	 * Signing and Encryption Artifact Response Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_ARTIFACT_RESPONSE_SIGNED = "wantArtifactResponseSigned";
	/**
	 * Signing and Encryption Logout Request Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_LOGOUT_REQUEST_SIGNED = "wantLogoutRequestSigned";
	/**
	 * Signing and Encryption Logout Response Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_LOGOUT_RESPONSE_SIGNED = "wantLogoutResponseSigned";
	/**
	 * Signing and Encryption Manage Name ID Request Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_MANAGE_NAME_ID_REQUEST_SIGNED = "wantMNIRequestSigned";
	/**
	 * Signing and Encryption Manage Name ID Response Signed.
	 */

	public static final String SIGNING_AND_ENCRYPTION_MANAGE_NAME_ID_RESPONSE_SIGNED = "wantMNIResponseSigned";
	/**
	 * Signing and Encryption Encryption Attribute.
	 */

	public static final String SIGNING_AND_ENCRYPTION_ENCRYPTION_ATTRIBUTE = "wantAttributeEncrypted";
	/**
	 * Signing and Encryption Assertion Attribute.
	 */

	public static final String SIGNING_AND_ENCRYPTION_ASSERTION_ATTRIBUTE = "wantAssertionEncrypted";
	/**
	 * Signing and Encryption NameID Attribute.
	 */

	public static final String SIGNING_AND_ENCRYPTION_NAMEID_ATTRIBUTE = "wantNameIDEncrypted";
	/**
	 * Signing and Encryption Certificate Aliases Signing.
	 */

	public static final String SIGNING_AND_ENCRYPTION_CERTIFICATE_ALIASES_SIGNING = "signingCertAlias";
	/**
	 * Signing and Encryption Certificate Aliases Encryption.
	 */

	public static final String SIGNING_AND_ENCRYPTION_CERTIFICATE_ALIASES_ENCRYPTION = "encryptionCertAlias";
	/**
	 * Signing and Encryption Certificate Aliases Key Size.
	 */

	public static final String SIGNING_AND_ENCRYPTION_CERTIFICATE_ALIASES_KEY_SIZE = "keySize";
	/**
	 * Signing and Encryption Certificate Aliases Algorithm.
	 */

	public static final String SIGNING_AND_ENCRYPTION_CERTIFICATE_ALIASES_ALGORITHM = "Algorithm";
	/**
	 * NameID Format List Current Values.
	 */

	public static final String NAMEID_FORMAT_LIST_CURRENT_VALUES = "SAMLv2SPAssertionContent.nameidlist";
	/**
	 * Authentication Context Mapper.
	 */

	public static final String AUTHENTICATION_CONTEXT_MAPPER_SP = "spAuthncontextMapper";
	/**
	 * Authentication Context Default Authentication Context.
	 */

	public static final String AUTHENTICATION_CONTEXT_DEFAULT_AUTHENTICATION_CONTEXT_SP = "spDefaultAuthnContext";
	/**
	 * Comparison Type.
	 */

	public static final String COMPARISON_TYPE = "spAuthncontextComparisonType";

	/**
	 * Basic Authentication Enabled.
	 */

	public static final String BASIC_AUTHENTICATION_ENABLED = "basicAuthOn";
	/**
	 * Basic Authentication User Name.
	 */

	public static final String BASIC_AUTHENTICATION_USER_NAME = "basicAuthUser";
	/**
	 * Basic Authentication Password.
	 */

	public static final String BASIC_AUTHENTICATION_PASSWORD = "basicAuthPassword";

	/**
	 * Attribute Mapper.
	 */

	public static final String ATTRIBUTE_MAPPER_SP = "spAttributeMapper";
	/**
	 * Attribute Map Current Values.
	 */

	public static final String ATTRIBUTE_MAP_CURRENT_VALUES_SP = "attributeMap";
	/**
	 * Auto Federation Enabled.
	 */

	public static final String AUTO_FEDERATION_ENABLED_SP = "autofedEnabled";
	/**
	 * Auto Federation Attribute.
	 */

	public static final String AUTO_FEDERATION_ATTRIBUTE = "autofedAttribute";
	/**
	 * Account Mapper.
	 */

	public static final String ACCOUNT_MAPPER_SP = "spAccountMapper";
	/**
	 * Use Name ID as User ID Enabled.
	 */

	public static final String USE_NAME_ID_AS_USER_ID_ENABLED = "useNameIDAsSPUserID";
	/**
	 * Artifact Message Encoding.
	 */

	public static final String ARTIFACT_MESSAGE_ENCODING = "responseArtifactMessageEncoding";
	/**
	 * Transient User.
	 */

	public static final String TRANSIENT_USER = "transientUser";
	/**
	 * Local Authentication Url.
	 */

	public static final String LOCAL_AUTHENTICATION_URL = "localAuthURL";
	/**
	 * Intermediate Url.
	 */

	public static final String INTERMEDIATE_URL = "intermediateUrl";
	/**
	 * External Application Logout URL.
	 */

	public static final String EXTERNAL_APPLICATION_LOGOUT_URL = "appLogoutUrl";
	/**
	 * Default Relay State URL.
	 */

	public static final String DEFAULT_RELAY_STATE_URL = "defaultRelayState";
	/**
	 * Adapter.
	 */

	public static final String ADAPTER = "spAdapter";
	/**
	 * Adapter Environment Current Values.
	 */

	public static final String ADAPTER_ENVIRONMENT_CURRENT_VALUES = "spAdapterEnv";

	/**
	 * Single Logout Service Default Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_DEFAULT_SERVICES = "isDefaultSLO";
	/**
	 * Single Logout Service HTTP-REDIRECT Location Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_HTTPREDIRECT_LOCATION_SERVICES = "slohttpLocation";
	/**
	 * Single Logout Service HTTP-REDIRECT Response Location Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_HTTPREDIRECT_RESPONSE_LOCATION_SERVICES = "slohttpResponseLocation";
	/**
	 * Single Logout Service POST Location Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_POST_LOCATION_SERVICES = "slopostLocation";
	/**
	 * Single Logout Service POST Response Location Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_POST_RESPONSE_LOCATION_SERVICES = "slopostResponseLocation";
	/**
	 * Single Logout Service SOAP Location Services.
	 */

	public static final String SINGLE_LOGOUT_SERVICE_SOAP_LOCATION_SERVICES = "slosoapLocation";
	/**
	 * Manage NameID Service Default Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_DEFAULT_SERVICES = "isDefaultMNI";
	/**
	 * Manage NameID Service HTTP-REDIRECT Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_HTTPREDIRECT_LOCATION_SERVICES = "mnihttpLocation";
	/**
	 * Manage NameID Service HTTP-REDIRECT Response Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_HTTPREDIRECT_RESPONSE_LOCATION_SERVICES = "mnihttpResponseLocation";
	/**
	 * Manage NameID Service POST Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_POST_LOCATION_SERVICES = "mnipostLocation";
	/**
	 * Manage NameID Service POST Response Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_POST_RESPONSE_LOCATION_SERVICES = "mnipostResponseLocation";
	/**
	 * Manage NameID Service SOAP Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_SOAP_LOCATION_SERVICES = "mnisoapLocation";
	/**
	 * Manage NameID Service SOAP Response Location Services.
	 */

	public static final String MANAGE_NAMEID_SERVICE_SOAP_RESPONSE_LOCATION_SERVICES = "mnisoapResponseLocation";

	/**
	 * Entity Config Tag.
	 */

	public static final String ENTITY_CONFIG_TAG = "EntityConfig";

	/**
	 * Authn Query Service Node Tag.
	 */
	public static final String AUTHN_QUERY_SERVICE_NODE = "AuthnQueryService";

	/**
	 * Assertion ID Request Service Node Tag.
	 */
	public static final String ASSERTION_ID_REQUEST_SERVICE_NODE = "AssertionIDRequestService";

	/**
	 * Entity Descriptor Node Tag.
	 */
	public static final String ENTITY_DESCRIPTOR_TAG = "EntityDescriptor";

	/**
	 * Name id format.
	 */
	public static final String NAME_ID_FORMAT_UPN = "UPN";

	/**
	 * Name id format.
	 */
	public static final String NAME_ID_FORMAT_EMAIL_ADDRESS = "Email Address";

	/**
	 * Name id format.
	 */
	public static final String NAME_ID_FORMAT_EMAIL = "Email";

	/**
	 * Name id format.
	 */
	public static final String NAME_ID_FORMAT_COMMON_NAME = "Common Name";

	/**
	 * SOAP.
	 */
	public static final String SOAP = "SOAP";

	/**
	 * HTTP_POST.
	 */
	public static final String HTTP_POST = "HTTP-POST";

	/**
	 * HTTP-Redirect.
	 */
	public static final String HTTP_REDIRECT = "HTTP-Redirect";

	/**
	 * Location type file.
	 */
	public static final String LOCATION_TYPE_FILE = "File";

	/**
	 * Location type URL.
	 */
	public static final String LOCATION_TYPE_URL = "URL";
	
	/**
	 * KeyDescriptor node.
	 */
	public static final String KEY_DESCRIPTOR_NODE = "KeyDescriptor";
	
	
	/**
	 * EncyptionMethod node.
	 */
	public static final String ENCRYPTION_METHOD_NODE = "EncryptionMethod";
	
	/**
	 * KeySize node.
	 */
	public static final String KEY_SIZE_NODE = "KeySize";
	
	
	/**
	 * KeyInfo node.
	 */
	public static final String KEY_INFO_NODE = "KeyInfo";
	
	
	/**
	 * X509Data node.
	 */
	public static final String X509DATA_NODE = "X509Data";
	
	
	/**
	 * X509Certificate node.
	 */
	public static final String X509CERTIFICATE_NODE = "X509Certificate";
	
     
	
	
	/**
	 * use attribute.
	 */
	public static final String USE_ATTRIBUTE = "use";
	
	/**
	 * alghorithm attribute.
	 */
	public static final String ALGORITHM_ATTRIBUTE = "Algorithm";
	
	
	/**
	 * use encryption.
	 */
	public static final String ENCRYPTION = "encryption";
	

	/**
	 * use signing.
	 */
	public static final String SIGNINNG = "signing";


	
	
}
