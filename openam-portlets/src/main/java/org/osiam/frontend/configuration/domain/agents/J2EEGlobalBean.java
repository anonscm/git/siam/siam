/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addPrefix;
import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.asSetWithSpecialEmptyList;
import static org.osiam.frontend.util.CollectionUtils.asSetWithValidation;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a j2ee global bean.
 */
public class J2EEGlobalBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String group;
	@NotNull
	@Size(min = 1)
	private String password;
	@NotNull
	@Size(min = 1)
	private String passwordConfirm;
	@NotNull
	@Size(min = 1)
	private String status;
	private String agentNotificationURL;;
	private String locationOfAgentConfigurationRepository;
	@NotNull
	@Min(0)
	private Integer configurationReloadInterval;
	private boolean agentConfigurationChangeNotification;
	private List<String> agentRootUrlForCdsso = new ArrayList<String>();
	private List<String> agentFilterMode = new ArrayList<String>();
	private boolean httpSessionBinding;
	@NotNull
	@Min(0)
	private Integer loginAttemptLimit;
	private List<String> customResponseHeader = new ArrayList<String>();
	@NotNull
	@Min(0)
	private Integer redirectAttemptLimit;
	private String agentDebugLevel;
	private String userMappingMode;
	private String userAttributeName;
	private boolean userPrincipalFlag;
	private String userTokenName;
	private String auditAccessTypes;
	private String auditLogLocation;
	private String remoteLogFilename;
	private boolean rotateLocalAuditLog;
	@NotNull
	@Min(0)
	private Integer localAuditLogRotationSize;
	private boolean fqdnCheck;
	private String fqdnDefault;
	private List<String> fqdnVirtualHostMap = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAgentNotificationURL() {
		return agentNotificationURL;
	}

	public void setAgentNotificationURL(String agentNotificationURL) {
		this.agentNotificationURL = agentNotificationURL;
	}

	public String getLocationOfAgentConfigurationRepository() {
		return locationOfAgentConfigurationRepository;
	}

	public void setLocationOfAgentConfigurationRepository(String locationOfAgentConfigurationRepository) {
		this.locationOfAgentConfigurationRepository = locationOfAgentConfigurationRepository;
	}

	public Integer getConfigurationReloadInterval() {
		return configurationReloadInterval;
	}

	public void setConfigurationReloadInterval(Integer configurationReloadInterval) {
		this.configurationReloadInterval = configurationReloadInterval;
	}

	public boolean getAgentConfigurationChangeNotification() {
		return agentConfigurationChangeNotification;
	}

	public void setAgentConfigurationChangeNotification(boolean agentConfigurationChangeNotification) {
		this.agentConfigurationChangeNotification = agentConfigurationChangeNotification;
	}

	public List<String> getAgentRootUrlForCdsso() {
		return agentRootUrlForCdsso;
	}

	public void setAgentRootUrlForCdsso(List<String> agentRootUrlForCdsso) {
		this.agentRootUrlForCdsso = agentRootUrlForCdsso;
	}

	public List<String> getAgentFilterMode() {
		return agentFilterMode;
	}

	public void setAgentFilterMode(List<String> agentFilterMode) {
		this.agentFilterMode = agentFilterMode;
	}

	public boolean getHttpSessionBinding() {
		return httpSessionBinding;
	}

	public void setHttpSessionBinding(boolean httpSessionBinding) {
		this.httpSessionBinding = httpSessionBinding;
	}

	public Integer getLoginAttemptLimit() {
		return loginAttemptLimit;
	}

	public void setLoginAttemptLimit(Integer loginAttemptLimit) {
		this.loginAttemptLimit = loginAttemptLimit;
	}

	public List<String> getCustomResponseHeader() {
		return customResponseHeader;
	}

	public void setCustomResponseHeader(List<String> customResponseHeader) {
		this.customResponseHeader = customResponseHeader;
	}

	public Integer getRedirectAttemptLimit() {
		return redirectAttemptLimit;
	}

	public void setRedirectAttemptLimit(Integer redirectAttemptLimit) {
		this.redirectAttemptLimit = redirectAttemptLimit;
	}

	public String getAgentDebugLevel() {
		return agentDebugLevel;
	}

	public void setAgentDebugLevel(String agentDebugLevel) {
		this.agentDebugLevel = agentDebugLevel;
	}

	public String getUserMappingMode() {
		return userMappingMode;
	}

	public void setUserMappingMode(String userMappingMode) {
		this.userMappingMode = userMappingMode;
	}

	public String getUserAttributeName() {
		return userAttributeName;
	}

	public void setUserAttributeName(String userAttributeName) {
		this.userAttributeName = userAttributeName;
	}

	public boolean getUserPrincipalFlag() {
		return userPrincipalFlag;
	}

	public void setUserPrincipalFlag(boolean userPrincipalFlag) {
		this.userPrincipalFlag = userPrincipalFlag;
	}

	public String getUserTokenName() {
		return userTokenName;
	}

	public void setUserTokenName(String userTokenName) {
		this.userTokenName = userTokenName;
	}

	public String getAuditAccessTypes() {
		return auditAccessTypes;
	}

	public void setAuditAccessTypes(String auditAccessTypes) {
		this.auditAccessTypes = auditAccessTypes;
	}

	public String getAuditLogLocation() {
		return auditLogLocation;
	}

	public void setAuditLogLocation(String auditLogLocation) {
		this.auditLogLocation = auditLogLocation;
	}

	public String getRemoteLogFilename() {
		return remoteLogFilename;
	}

	public void setRemoteLogFilename(String remoteLogFilename) {
		this.remoteLogFilename = remoteLogFilename;
	}

	public boolean getRotateLocalAuditLog() {
		return rotateLocalAuditLog;
	}

	public void setRotateLocalAuditLog(boolean rotateLocalAuditLog) {
		this.rotateLocalAuditLog = rotateLocalAuditLog;
	}

	public Integer getLocalAuditLogRotationSize() {
		return localAuditLogRotationSize;
	}

	public void setLocalAuditLogRotationSize(Integer localAuditLogRotationSize) {
		this.localAuditLogRotationSize = localAuditLogRotationSize;
	}

	public boolean getFqdnCheck() {
		return fqdnCheck;
	}

	public void setFqdnCheck(boolean fqdnCheck) {
		this.fqdnCheck = fqdnCheck;
	}

	public String getFqdnDefault() {
		return fqdnDefault;
	}

	public void setFqdnDefault(String fqdnDefault) {
		this.fqdnDefault = fqdnDefault;
	}

	public List<String> getFqdnVirtualHostMap() {
		return fqdnVirtualHostMap;
	}

	public void setFqdnVirtualHostMap(List<String> fqdnVirtualHostMap) {
		this.fqdnVirtualHostMap = fqdnVirtualHostMap;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 */
	public static J2EEGlobalBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EEGlobalBean bean = new J2EEGlobalBean();
		bean.setName(name);
		bean.setGroup(stringFromSet(attributes.get(AgentAttributesConstants.GROUP)));
		bean.setPassword(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setPasswordConfirm(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setStatus(stringFromSet(attributes.get(AgentAttributesConstants.STATUS)));
		bean.setAgentNotificationURL(stringFromSet(attributes.get(AgentAttributesConstants.AGENT_NOTIFICATION_URL)));
		bean.setLocationOfAgentConfigurationRepository(stringFromSet(attributes
				.get(AgentAttributesConstants.LOCATION_OF_AGENT_CONFIGURATION_REPOSITORY)));
		bean.setConfigurationReloadInterval(integerFromSet(attributes
				.get(AgentAttributesConstants.CONFIGURATION_RELOAD_INTERVAL_J2EE)));
		bean.setAgentConfigurationChangeNotification(booleanFromSet(attributes
				.get(AgentAttributesConstants.AGENT_CONFIGURATION_CHANGE_NOTIFICATION)));
		bean.setAgentRootUrlForCdsso(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES)),
				"agentRootURL="));
		bean.setAgentFilterMode(listFromSet(attributes.get(AgentAttributesConstants.AGENT_FILTER_MODE_CURRENT_VALUES)));
		bean.setHttpSessionBinding(booleanFromSet(attributes.get(AgentAttributesConstants.HTTP_SESSION_BINDING)));
		bean.setLoginAttemptLimit(integerFromSet(attributes.get(AgentAttributesConstants.LOGIN_ATTEMPT_LIMIT)));
		bean.setCustomResponseHeader(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_RESPONSE_HEADER_CURRENT_VALUES)), ""));
		bean.setRedirectAttemptLimit(integerFromSet(attributes.get(AgentAttributesConstants.REDIRECT_ATTEMPT_LIMIT)));
		bean.setAgentDebugLevel(stringFromSet(attributes.get(AgentAttributesConstants.AGENT_DEBUG_LEVEL_J2EE)));
		bean.setUserMappingMode(stringFromSet(attributes.get(AgentAttributesConstants.USER_MAPPING_MODE)));
		bean.setUserAttributeName(stringFromSet(attributes.get(AgentAttributesConstants.USER_ATTRIBUTE_NAME)));
		bean.setUserPrincipalFlag(booleanFromSet(attributes.get(AgentAttributesConstants.USER_PRINCIPAL_FLAG)));
		bean.setUserTokenName(stringFromSet(attributes.get(AgentAttributesConstants.USER_TOKEN_NAME)));
		bean.setAuditAccessTypes(stringFromSet(attributes.get(AgentAttributesConstants.AUDIT_ACCESS_TYPES)));
		bean.setAuditLogLocation(stringFromSet(attributes.get(AgentAttributesConstants.AUDIT_LOG_LOCATION)));
		bean.setRemoteLogFilename(stringFromSet(attributes.get(AgentAttributesConstants.REMOTE_LOG_FILE_NAME)));
		bean.setLocalAuditLogRotationSize(integerFromSet(attributes
				.get(AgentAttributesConstants.LOCAL_AUDIT_LOG_ROTATION_SIZE)));
		bean.setRotateLocalAuditLog(booleanFromSet(attributes.get(AgentAttributesConstants.ROTATE_LOCAL_AUDIT_LOG)));
		bean.setFqdnDefault(stringFromSet(attributes.get(AgentAttributesConstants.FQDN_DEFAULT)));
		bean.setFqdnCheck(booleanFromSet(attributes.get(AgentAttributesConstants.FQDN_CHECK)));
		bean.setFqdnVirtualHostMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.FQDN_VIRTUAL_HOST_MAP_CURRENT_VALUES)), ""));

		return bean;

	}

	/**
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		if ((group != null) && (!"".equals(group))) {
			map.put(AgentAttributesConstants.GROUP, asSet(getGroup()));
		}

		map.put(AgentAttributesConstants.PASSWORD, asSet(getPassword()));
		map.put(AgentAttributesConstants.STATUS, asSet(getStatus()));
		map.put(AgentAttributesConstants.AGENT_NOTIFICATION_URL, asSet(getAgentNotificationURL()));
		map.put(AgentAttributesConstants.LOCATION_OF_AGENT_CONFIGURATION_REPOSITORY,
				asSet(getLocationOfAgentConfigurationRepository()));

		map.put(AgentAttributesConstants.CONFIGURATION_RELOAD_INTERVAL_J2EE, asSet(getConfigurationReloadInterval()));

		map.put(AgentAttributesConstants.AGENT_CONFIGURATION_CHANGE_NOTIFICATION,
				asSet(getAgentConfigurationChangeNotification()));
		map.put(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES,
				asSet(addPrefix(getAgentRootUrlForCdsso(), "agentRootURL=")));

		Set<String> agentFilterModeSet = asSet(getAgentFilterMode());
		if (agentFilterModeSet != null) {
			if (agentFilterModeSet.size() > 0) {
				map.put(AgentAttributesConstants.AGENT_FILTER_MODE_CURRENT_VALUES, agentFilterModeSet);
			}
		}

		map.put(AgentAttributesConstants.HTTP_SESSION_BINDING, asSet(getHttpSessionBinding()));
		map.put(AgentAttributesConstants.LOGIN_ATTEMPT_LIMIT, asSet(getLoginAttemptLimit()));
		map.put(AgentAttributesConstants.REDIRECT_ATTEMPT_LIMIT, asSet(getRedirectAttemptLimit()));
		map.put(AgentAttributesConstants.CUSTOM_RESPONSE_HEADER_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getCustomResponseHeader()));

		Set<String> agentDebugLevelSet = asSetWithValidation(getAgentDebugLevel(),
				Arrays.asList("error", "message", "off", "warning"), "error.illegal_value.agent_debug_level_set");
		if (agentDebugLevelSet != null) {
			map.put(AgentAttributesConstants.AGENT_DEBUG_LEVEL_J2EE, agentDebugLevelSet);
		}

		map.put(AgentAttributesConstants.USER_MAPPING_MODE, asSet(getUserMappingMode()));
		map.put(AgentAttributesConstants.USER_ATTRIBUTE_NAME, asSet(getUserAttributeName()));
		map.put(AgentAttributesConstants.USER_PRINCIPAL_FLAG, asSet(getUserPrincipalFlag()));

		map.put(AgentAttributesConstants.USER_TOKEN_NAME, asSet(getUserTokenName()));

		map.put(AgentAttributesConstants.AUDIT_ACCESS_TYPES, asSet(getAuditAccessTypes()));
		map.put(AgentAttributesConstants.AUDIT_LOG_LOCATION, asSet(getAuditLogLocation()));

		map.put(AgentAttributesConstants.REMOTE_LOG_FILE_NAME, asSet(getRemoteLogFilename()));
		map.put(AgentAttributesConstants.ROTATE_LOCAL_AUDIT_LOG, asSet(getRotateLocalAuditLog()));
		map.put(AgentAttributesConstants.LOCAL_AUDIT_LOG_ROTATION_SIZE, asSet(getLocalAuditLogRotationSize()));
		map.put(AgentAttributesConstants.FQDN_CHECK, asSet(getFqdnCheck()));
		map.put(AgentAttributesConstants.FQDN_DEFAULT, asSet(getFqdnDefault()));

		map.put(AgentAttributesConstants.FQDN_VIRTUAL_HOST_MAP_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getFqdnVirtualHostMap()));

		return map;
	}
}
