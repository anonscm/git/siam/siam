/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

/**
 * 
 * Encapsulates constants for DataStore fields management.
 * 
 */
public final class DataStoresConstants {

	/**
	 * Default password for when user is not changing it.
	 */
	public static final String DEFAULT_PASSWORD = "KmhUnWR1MYWDYW4xuqdF5nbm+CXIyOVt";

	/**
	 * Constant string for LDAP for AD data store type.
	 */
	public static final String STORE_TYPE_LDAP_FOR_AD = "LDAPv3ForAD";
	/**
	 * Constant string for LDAP for ADAM data store type.
	 */
	public static final String STORE_TYPE_LDAP_FOR_ADAM = "LDAPv3ForADAM";
	/**
	 * Constant string for database data store type.
	 */
	public static final String STORE_TYPE_DATABASE = "Database";
	/**
	 * Constant string for LDAP for OPENDS data store type.
	 */
	public static final String STORE_TYPE_LDAP_FOR_OPEN_DS = "LDAPv3ForOpenDS";
	/**
	 * Constant string for LDAP for AMDS data store type.
	 */
	public static final String STORE_TYPE_LDAP_FOR_AMDS = "LDAPv3ForAMDS";
	/**
	 * Constant string for LDAP for TIVOLI data store type.
	 */
	public static final String STORE_TYPE_LDAP_FOR_TIVOLI = "LDAPv3ForTivoli";

	private DataStoresConstants() {

	}

	// Active Directory

	/**
	 * The delay time between retries.
	 */
	public static final String DELAY_BETWEEN_RETRIES = "com.iplanet.am.ldap.connection.delay.between.retries";

	/**
	 * LDAP Bind DN.
	 */
	public static final String LDAP_BIND_DN = "sun-idrepo-ldapv3-config-authid";

	/**
	 * LDAP Organization DN.
	 */
	public static final String LDAP_ORGANIZATION_DN = "sun-idrepo-ldapv3-config-organization_name";

	/**
	 * Persistent Search Base DN.
	 */
	public static final String PERSISTENT_SEARCH_BASE_DN = "sun-idrepo-ldapv3-config-psearchbase";

	/**
	 * Persistent Search Filter.
	 */
	public static final String PERSISTENT_SEARCH_FILTER = "sun-idrepo-ldapv3-config-psearch-filter";

	/**
	 * Persistent Search Scope.
	 */
	public static final String PERSISTENT_SEARCH_SCOPE = "sun-idrepo-ldapv3-config-psearch-scope";

	/**
	 * Attribute Name of User Status.
	 */
	public static final String ATTRIBUTE_NAME_OF_USER_STATUS = "sun-idrepo-ldapv3-config-isactive";

	/**
	 * Database Attribute Name of User Status.
	 */
	public static final String DATABASE_ATTRIBUTE_NAME_OF_USER_STATUS = "sun-opensso-database-UserStatusAttr";

	/**
	 * LDAP Connection Pool Maximum Size.
	 */
	public static final String LDAP_CONNECTION_POOL_MAXIMUM_SIZE = "sun-idrepo-ldapv3-config-connection_pool_max_size";

	/**
	 * LDAP Connection Pool Minimum Size.
	 */
	public static final String LDAP_CONNECTION_POOL_MINIMUM_SIZE = "sun-idrepo-ldapv3-config-connection_pool_min_size";

	/**
	 * Maximum Size of the Cache.
	 */
	public static final String MAXIMUM_SIZE_OF_THE_CACHE = "sun-idrepo-ldapv3-config-cache-size";

	/**
	 * Maximum Age of Cached Items.
	 */
	public static final String MAXIMUM_AGE_OF_CACHED_ITEMS = "sun-idrepo-ldapv3-config-cache-ttl";

	/**
	 * Maximum Number of Retries After Error Codes.
	 */
	public static final String MAXIMUM_NUMBER_OF_RETRIES_AFTER_ERROR_CODES = "sun-idrepo-ldapv3-config-numretires";

	/**
	 * Persistent Search Maximum Idle Time Before Restart.
	 */
	public static final String PERSISTENT_SEARCH_MAXIMUM_IDLE_TIME_BEFORE_RESTART = "sun-idrepo-ldapv3-config-idletimeout";

	/**
	 * Authentication Naming Attribute.
	 */
	public static final String AUTHENTICATION_NAMING_ATTRIBUTE = "sun-idrepo-ldapv3-config-auth-naming-attr";

	/**
	 * LDAP People Container Value.
	 */
	public static final String LDAP_PEOPLE_CONTAINER_VALUE = "sun-idrepo-ldapv3-config-people-container-value";

	/**
	 * LDAP People Container Naming Attribute.
	 */
	public static final String LDAP_PEOPLE_CONTAINER_NAMING_ATTRIBUTE = "sun-idrepo-ldapv3-config-people-container-name";

	/**
	 * Attribute Name of UNIQUE Member.
	 */
	public static final String ATTRIBUTE_NAME_OF_UNIQUE_MEMBER = "sun-idrepo-ldapv3-config-uniquemember";

	/**
	 * Attribute Name of Group Member URL.
	 */
	public static final String ATTRIBUTE_NAME_OF_GROUP_MEMBER_URL = "sun-idrepo-ldapv3-config-memberurl";

	/**
	 * Ldap Roles Search Attribute.
	 */
	public static final String LDAP_ROLES_SEARCH_ATTRIBUTE = "sun-idrepo-ldapv3-config-roles-search-attribute";

	/**
	 * Ldap Filter Roles Search Attribute.
	 */
	public static final String LDAP_FILTER_ROLES_SEARCH_ATTRIBUTE = "sun-idrepo-ldapv3-config-filterroles-search-attribute";

	/**
	 * Ldap Filter Roles Search Filter.
	 */
	public static final String LDAP_FILTER_ROLES_SEARCH_FILTER = "sun-idrepo-ldapv3-config-filterroles-search-filter";

	/**
	 * Ldap Roles Search Filter.
	 */
	public static final String LDAP_ROLES_SEARCH_FILTER = "sun-idrepo-ldapv3-config-roles-search-filter";

	/**
	 * Ldap Filter Roles Object Class.
	 */
	public static final String LDAP_FILTER_ROLES_OBJECT_CLASS_CURRENT_VALUES = "sun-idrepo-ldapv3-config-filterrole-objectclass";

	/**
	 * Ldap Filter Roles Attributes.
	 */
	public static final String LDAP_FILTER_ROLES_ATTRIBUTES_CURRENT_VALUES = "sun-idrepo-ldapv3-config-filterrole-attributes";

	/**
	 * Attribute Name for Filtered Role Membership.
	 */
	public static final String ATTRIBUTE_NAME_FOR_FILTERED_ROLE_MEMBERSHIP = "sun-idrepo-ldapv3-config-nsrole";

	/**
	 * Attribute Name of Role Membership.
	 */
	public static final String ATTRIBUTE_NAME_OF_ROLE_MEMBERSHIP = "sun-idrepo-ldapv3-config-nsroledn";

	/**
	 * Attribute Name of Filtered Role Filter.
	 */
	public static final String ATTRIBUTE_NAME_OF_FILTERED_ROLE_FILTER = "sun-idrepo-ldapv3-config-nsrolefilter";

	/**
	 * Ldap Roles Object Class.
	 */
	public static final String LDAP_ROLES_OBJECT_CLASS_CURRENT_VALUES = "sun-idrepo-ldapv3-config-role-objectclass";

	/**
	 * Ldap Roles Attributes.
	 */
	public static final String LDAP_ROLES_ATTRIBUTES_CURRENT_VALUES = "sun-idrepo-ldapv3-config-role-attributes";

	/**
	 * Attribute Name for Group Membership.
	 */
	public static final String ATTRIBUTE_NAME_FOR_GROUP_MEMBERSHIP = "sun-idrepo-ldapv3-config-memberof";

	/**
	 * LDAP Groups Container Value.
	 */
	public static final String LDAP_GROUPS_CONTAINER_VALUE = "sun-idrepo-ldapv3-config-group-container-value";

	/**
	 * LDAP Groups container Naming Attribute.
	 */
	public static final String LDAP_GROUPS_CONTAINER_NAMING_ATTRIBUTE = "sun-idrepo-ldapv3-config-group-container-name";

	/**
	 * LDAP Groups Search Filter.
	 */
	public static final String LDAP_GROUPS_SEARCH_FILTER = "sun-idrepo-ldapv3-config-groups-search-filter";

	/**
	 * LDAP Groups Search Attribute.
	 */
	public static final String LDAP_GROUPS_SEARCH_ATTRIBUTE = "sun-idrepo-ldapv3-config-groups-search-attribute";

	/**
	 * User Status Inactive Value.
	 */
	public static final String USER_STATUS_INACTIVE_VALUE = "sun-idrepo-ldapv3-config-inactive";

	/**
	 * User Status active Value.
	 */
	public static final String USER_STATUS_ACTIVE_VALUE = "sun-idrepo-ldapv3-config-active";

	/**
	 * Database User Status active Value.
	 */
	public static final String DATABASE_USER_STATUS_ACTIVE_VALUE = "sun-opensso-database-activeValue";

	/**
	 * Database User Status in active Value.
	 */
	public static final String DATABASE_USER_STATUS_INACTIVE_VALUE = "sun-opensso-database-inactiveValue";

	/**
	 * LDAPv3 Plug-in Search Scope.
	 * 
	 * Possible values for this constant are :
	 * 
	 * "SCOPE_BASE", "SCOPE_ONE", "SCOPE_SUB"
	 */
	public static final String LDAPV3_PLUGIN_SEARCH_SCOPE = "sun-idrepo-ldapv3-config-search-scope";

	/**
	 * LDAPv3 Repository Plug-in Class Name.
	 */
	public static final String LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME = "sunIdRepoClass";

	/**
	 * Search Timeout.
	 */
	public static final String SEARCH_TIMEOUT = "sun-idrepo-ldapv3-config-time-limit";

	/**
	 * Maximum Results Returned from Search.
	 */
	public static final String MAXIMUM_RESULTS_RETURNED_FROM_SEARCH = "sun-idrepo-ldapv3-config-max-result";

	/**
	 * Database Maximum Results Returned from Search.
	 */
	public static final String DATABASE_MAXIMUM_RESULTS_RETURNED_FROM_SEARCH = "sun-opensso-database-config-max-result";

	/**
	 * LDAP Bind Password.
	 */
	public static final String LDAP_BIND_PASSWORD = "sun-idrepo-ldapv3-config-authpw";

	/**
	 * LDAP Users Search Attribute.
	 */
	public static final String LDAP_USERS_SEARCH_ATTRIBUTE = "sun-idrepo-ldapv3-config-users-search-attribute";

	/**
	 * LDAP Users Search Filter.
	 */
	public static final String LDAP_USERS_SEARCH_FILTER = "sun-idrepo-ldapv3-config-users-search-filter";

	/**
	 * LDAP SSL/TLS.
	 * 
	 * List of possible values is : "true", "false"
	 */
	public static final String LDAP_SSL_TLS = "sun-idrepo-ldapv3-config-ssl-enabled";

	/**
	 * LDAP Follows Referral.
	 * 
	 * List of possible values is : "true", "false"
	 */
	public static final String LDAP_FOLLOWS_REFERRAL = "sun-idrepo-ldapv3-config-referrals";

	/**
	 * Identity Types That Can Be Authenticated.
	 * 
	 * List of possible values is : "User", (don't know other values yet)
	 */
	public static final String IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED = "sun-idrepo-ldapv3-config-authenticatable-type";

	/**
	 * Default Group Members Users DN.
	 * 
	 * 
	 */
	public static final String DEFAULT_GROUP_MEMBERS_USER_DN = "sun-idrepo-ldapv3-config-dftgroupmember";

	/**
	 * Database Repository Plugin Class Name.
	 * 
	 * 
	 */
	public static final String DATABASE_REPOSITORY_PLUGIN_CLASS_NAME = "sunIdRepoClass";

	/**
	 * Database Data Access Object Plugin Class Name.
	 * 
	 * 
	 */
	public static final String DATABASE_DATA_ACCESS_OBJECT_PLUGIN_CLASS_NAME = "sun-opensso-database-dao-class-name";

	/**
	 * Database Plugin Supported Types and Operations Current Values.
	 * 
	 * 
	 */
	public static final String DATABASE_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES = "sun-opensso-database-sunIdRepoSupportedOperations";

	/**
	 * Caching.
	 */
	public static final String CACHING = "sun-idrepo-ldapv3-config-cache-enabled";

	/**
	 * LDAP Server Current Values.
	 * 
	 * Accepts multiple String values
	 */
	public static final String LDAP_SERVER_CURRENT_VALUES = "sun-idrepo-ldapv3-config-ldap-server";

	/**
	 * Attribute Name Mapping - Current Values.
	 * 
	 * 
	 * Accepts multiple String values
	 */
	public static final String ATTRIBUTE_NAME_MAPPING = "sunIdRepoAttributeMapping";

	/**
	 * LDAPv3 Plug-in Supported Types and Operations - Current Values.
	 * 
	 * Accepts multiple String values
	 */
	public static final String LDAPV3_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES = "sunIdRepoSupportedOperations";

	/**
	 * LDAP User Object Class - Current Values.
	 */

	public static final String LDAP_USER_OBJECT_CLASS_CURRENT_VALUES = "sun-idrepo-ldapv3-config-user-objectclass";

	/**
	 * LDAP User Attributes - Current Values.
	 */

	public static final String LDAP_USER_ATTRIBUTES_CURRENT_VALUES = "sun-idrepo-ldapv3-config-user-attributes";

	/**
	 * Create User Attribute Mapping - Current Values.
	 */

	public static final String CREATE_USER_ATTRIBUTE_MAPPING_CURRENT_VALUES = "sun-idrepo-ldapv3-config-createuser-attr-mapping";

	/**
	 * LDAP Groups Object Class - Current Values.
	 */

	public static final String LDAP_GROUPS_OBJECT_CLASS_CURRENT_VALUES = "sun-idrepo-ldapv3-config-group-objectclass";

	/**
	 * LDAP Groups Attributes - Current Values.
	 */

	public static final String LDAP_GROUPS_ATTRIBUTES_CURRENT_VALUES = "sun-idrepo-ldapv3-config-group-attributes";

	/**
	 * LDAPException Error Codes to Retry On - Current Values.
	 */

	public static final String LDAP_EXCEPTION_ERROR_CODES_TO_RETRY_ON_CURRENT_VALUES = "sun-idrepo-ldapv3-config-errorcodes";

	/**
	 * Database Plug-in Supported Types and Operations.
	 */

	public static final String DATABASE_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS = "sun-opensso-database-sunIdRepoSupportedOperations";

	/**
	 * Required Field Database Data Access Object Plugin Class Name.
	 */

	public static final String REQUIRED_FIELD_DATABASE_DATA_ACCESS_OBJECT_PLUGIN_CLASS_NAME = "sun-opensso-database-dao-class-name";

	/**
	 * Connection Type.
	 * 
	 * - possible values : JDBC, JNDI
	 */

	public static final String CONNECTION_TYPE = "sun-opensso-database-dao-JDBCConnectionType";

	/**
	 * Database DataSource Name.
	 */

	public static final String DATABASE_DATASOURCE_NAME = "sun-opensso-database-DataSourceJndiName";

	/**
	 * JDBC Driver Class Name.
	 */

	public static final String JDBC_DRIVER_CLASS_NAME = "sun-opensso-database-JDBCDriver";

	/**
	 * Password for Connecting to Database.
	 */

	public static final String PASSWORD_FOR_CONNECTING_TO_DATABASE = "sun-opensso-database-JDBCDbpassword";

	/**
	 * JDBC Driver URL.
	 */

	public static final String JDBC_DRIVER_URL = "sun-opensso-database-JDBCUrl";

	/**
	 * Connect This User to Database.
	 */

	public static final String CONNECT_THIS_USER_TO_DATABASE = "sun-opensso-database-JDBCDbuser";

	/**
	 * Required Field Database User Table Name.
	 */

	public static final String DATABASE_USER_TABLE_NAME = "sun-opensso-database-UserTableName";

	/**
	 * List of User Attributes Names in Database - Current Values.
	 */

	public static final String LIST_OF_USER_ATTRIBUTES_NAMES_IN_DATABASE_CURRENT_VALUES = "sun-opensso-database-UserAttrs";

	/**
	 * Required Field User Password Attribute Name.
	 */

	public static final String USER_PASSWORD_ATTRIBUTE_NAME = "sun-opensso-database-UserPasswordAttr";

	/**
	 * Required Field User ID Attribute Name.
	 */

	public static final String USER_ID_ATTRIBUTE_NAME = "sun-opensso-database-UserIDAttr";

	/**
	 * Users Search Attribute in Database.
	 */

	public static final String USERS_SEARCH_ATTRIBUTE_IN_DATABASE = "sun-opensso-database-config-users-search-attribute";

	/**
	 * Database Membership Table Name.
	 */

	public static final String DATABASE_MEMBERSHIP_TABLE_NAME = "sun-opensso-database-MembershipTableName";

	/**
	 * Membership ID Attribute Name.
	 */

	public static final String MEMBERSHIP_ID_ATTRIBUTE_NAME = "sun-opensso-database-MembershipIDAttr";

	/**
	 * Membership Search Attribute In Database.
	 */

	public static final String MEMBERSHIP_SEARCH_ATTRIBUTE_IN_DATABASE = "sun-opensso-database-membership-search-attribute";

}
