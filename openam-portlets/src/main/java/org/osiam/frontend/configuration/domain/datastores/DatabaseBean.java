/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.DataStoresConstants;

/**
 * LDAPv3ForADBean object which defines attributes for a "Database Repository"
 * {@link DataStore}.
 * 
 */
public class DatabaseBean extends BaseDataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	private String loadSchemaWhenFinished;
	private List<String> attributeNameMapping = new ArrayList<String>();
	@NotNull
	@Size(min = 1)
	private String databaseRepositoryPluginClassName;
	private List<String> databasePlugInSupportedTypesAndOperations = new ArrayList<String>();
	@NotNull
	@Size(min = 1)
	private String databaseDataAccessObjectPluginClassName;
	private String connectionType;
	@NotNull
	@Size(min = 1)
	private String databaseDatasourceName;
	private String jdbcDriverClassName;
	private String passwordForConnectingToDatabase;
	private String passwordForConnectingToDatabaseConfirm;
	private String jdbcDriverUrl;
	private String connectThisUserToDatabase;
	@NotNull
	@Size(min = 1)
	private String databaseUserTableName;
	@Size(min = 1)
	private List<String> listOfUserAttributesNamesInDatabase = new ArrayList<String>();
	@NotNull
	@Size(min = 1)
	private String userPasswordAttributeName;
	@NotNull
	@Size(min = 1)
	private String userIdAttributeName;
	@NotNull
	@Size(min = 1)
	private String attributeNameOfUserStatus;
	@NotNull
	@Size(min = 1)
	private String userStatusActiveValue;
	@NotNull
	@Size(min = 1)
	private String userStatusInActiveValue;
	@NotNull
	private Integer maximumResultsReturnedFromSearch;
	private String usersSearchAttributeInDatabase;
	private String databaseMembershipTableName;
	@NotNull
	@Size(min = 1)
	private String membershipIdAttributeName;
	private String membershipSearchAttributeInDatabase;

	public String getLoadSchemaWhenFinished() {
		return loadSchemaWhenFinished;
	}

	public void setLoadSchemaWhenFinished(String loadSchemaWhenFinished) {
		this.loadSchemaWhenFinished = loadSchemaWhenFinished;
	}

	public List<String> getAttributeNameMapping() {
		return attributeNameMapping;
	}

	public void setAttributeNameMapping(List<String> attributeNameMapping) {
		this.attributeNameMapping = attributeNameMapping;
	}

	public String getDatabaseRepositoryPluginClassName() {
		return databaseRepositoryPluginClassName;
	}

	public void setDatabaseRepositoryPluginClassName(String databaseRepositoryPluginClassName) {
		this.databaseRepositoryPluginClassName = databaseRepositoryPluginClassName;
	}

	public List<String> getDatabasePlugInSupportedTypesAndOperations() {
		return databasePlugInSupportedTypesAndOperations;
	}

	public void setDatabasePlugInSupportedTypesAndOperations(List<String> databasePlugInSupportedTypesAndOperations) {
		this.databasePlugInSupportedTypesAndOperations = databasePlugInSupportedTypesAndOperations;
	}

	public String getDatabaseDataAccessObjectPluginClassName() {
		return databaseDataAccessObjectPluginClassName;
	}

	public void setDatabaseDataAccessObjectPluginClassName(String databaseDataAccessObjectPluginClassName) {
		this.databaseDataAccessObjectPluginClassName = databaseDataAccessObjectPluginClassName;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getDatabaseDatasourceName() {
		return databaseDatasourceName;
	}

	public void setDatabaseDatasourceName(String databaseDatasourceName) {
		this.databaseDatasourceName = databaseDatasourceName;
	}

	public String getJdbcDriverClassName() {
		return jdbcDriverClassName;
	}

	public void setJdbcDriverClassName(String jdbcDriverClassName) {
		this.jdbcDriverClassName = jdbcDriverClassName;
	}

	public String getPasswordForConnectingToDatabase() {
		return passwordForConnectingToDatabase;
	}

	public void setPasswordForConnectingToDatabase(String passwordForConnectingToDatabase) {
		this.passwordForConnectingToDatabase = passwordForConnectingToDatabase;
	}

	public String getPasswordForConnectingToDatabaseConfirm() {
		return passwordForConnectingToDatabaseConfirm;
	}

	public void setPasswordForConnectingToDatabaseConfirm(String passwordForConnectingToDatabaseConfirm) {
		this.passwordForConnectingToDatabaseConfirm = passwordForConnectingToDatabaseConfirm;
	}

	public String getJdbcDriverUrl() {
		return jdbcDriverUrl;
	}

	public void setJdbcDriverUrl(String jdbcDriverUrl) {
		this.jdbcDriverUrl = jdbcDriverUrl;
	}

	public String getConnectThisUserToDatabase() {
		return connectThisUserToDatabase;
	}

	public void setConnectThisUserToDatabase(String connectThisUserToDatabase) {
		this.connectThisUserToDatabase = connectThisUserToDatabase;
	}

	public String getDatabaseUserTableName() {
		return databaseUserTableName;
	}

	public void setDatabaseUserTableName(String databaseUserTableName) {
		this.databaseUserTableName = databaseUserTableName;
	}

	public List<String> getListOfUserAttributesNamesInDatabase() {
		return listOfUserAttributesNamesInDatabase;
	}

	public void setListOfUserAttributesNamesInDatabase(List<String> listOfUserAttributesNamesInDatabase) {
		this.listOfUserAttributesNamesInDatabase = listOfUserAttributesNamesInDatabase;
	}

	public String getUserPasswordAttributeName() {
		return userPasswordAttributeName;
	}

	public void setUserPasswordAttributeName(String userPasswordAttributeName) {
		this.userPasswordAttributeName = userPasswordAttributeName;
	}

	public String getUserIdAttributeName() {
		return userIdAttributeName;
	}

	public void setUserIdAttributeName(String userIdAttributeName) {
		this.userIdAttributeName = userIdAttributeName;
	}

	public String getAttributeNameOfUserStatus() {
		return attributeNameOfUserStatus;
	}

	public void setAttributeNameOfUserStatus(String attributeNameOfUserStatus) {
		this.attributeNameOfUserStatus = attributeNameOfUserStatus;
	}

	public String getUserStatusActiveValue() {
		return userStatusActiveValue;
	}

	public void setUserStatusActiveValue(String userStatusActiveValue) {
		this.userStatusActiveValue = userStatusActiveValue;
	}

	public String getUserStatusInActiveValue() {
		return userStatusInActiveValue;
	}

	public void setUserStatusInActiveValue(String userStatusInActiveValue) {
		this.userStatusInActiveValue = userStatusInActiveValue;
	}

	public Integer getMaximumResultsReturnedFromSearch() {
		return maximumResultsReturnedFromSearch;
	}

	public void setMaximumResultsReturnedFromSearch(Integer maximumResultsReturnedFromSearch) {
		this.maximumResultsReturnedFromSearch = maximumResultsReturnedFromSearch;
	}

	public String getUsersSearchAttributeInDatabase() {
		return usersSearchAttributeInDatabase;
	}

	public void setUsersSearchAttributeInDatabase(String usersSearchAttributeInDatabase) {
		this.usersSearchAttributeInDatabase = usersSearchAttributeInDatabase;
	}

	public String getDatabaseMembershipTableName() {
		return databaseMembershipTableName;
	}

	public void setDatabaseMembershipTableName(String databaseMembershipTableName) {
		this.databaseMembershipTableName = databaseMembershipTableName;
	}

	public String getMembershipIdAttributeName() {
		return membershipIdAttributeName;
	}

	public void setMembershipIdAttributeName(String membershipIdAttributeName) {
		this.membershipIdAttributeName = membershipIdAttributeName;
	}

	public String getMembershipSearchAttributeInDatabase() {
		return membershipSearchAttributeInDatabase;
	}

	public void setMembershipSearchAttributeInDatabase(String membershipSearchAttributeInDatabase) {
		this.membershipSearchAttributeInDatabase = membershipSearchAttributeInDatabase;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();

		map.put(DataStoresConstants.ATTRIBUTE_NAME_MAPPING, asSet(getAttributeNameMapping()));
		map.put(DataStoresConstants.DATABASE_REPOSITORY_PLUGIN_CLASS_NAME,
				asSet(getDatabaseRepositoryPluginClassName()));
		map.put(DataStoresConstants.DATABASE_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES,
				asSet(getDatabasePlugInSupportedTypesAndOperations()));
		map.put(DataStoresConstants.DATABASE_DATA_ACCESS_OBJECT_PLUGIN_CLASS_NAME,
				asSet(getDatabaseDataAccessObjectPluginClassName()));
		map.put(DataStoresConstants.CONNECTION_TYPE, asSet(getConnectionType()));
		map.put(DataStoresConstants.DATABASE_DATASOURCE_NAME, asSet(getDatabaseDatasourceName()));
		map.put(DataStoresConstants.JDBC_DRIVER_CLASS_NAME, asSet(getJdbcDriverClassName()));
		if (!DataStoresConstants.DEFAULT_PASSWORD.equals(getPasswordForConnectingToDatabase())) {
			map.put(DataStoresConstants.PASSWORD_FOR_CONNECTING_TO_DATABASE,
					asSet(getPasswordForConnectingToDatabase()));
		}

		map.put(DataStoresConstants.JDBC_DRIVER_URL, asSet(getJdbcDriverUrl()));
		map.put(DataStoresConstants.CONNECT_THIS_USER_TO_DATABASE, asSet(getConnectThisUserToDatabase()));
		map.put(DataStoresConstants.DATABASE_USER_TABLE_NAME, asSet(getDatabaseUserTableName()));
		map.put(DataStoresConstants.LIST_OF_USER_ATTRIBUTES_NAMES_IN_DATABASE_CURRENT_VALUES,
				asSet(getListOfUserAttributesNamesInDatabase()));
		map.put(DataStoresConstants.USER_PASSWORD_ATTRIBUTE_NAME, asSet(getUserPasswordAttributeName()));
		map.put(DataStoresConstants.USER_ID_ATTRIBUTE_NAME, asSet(getUserIdAttributeName()));
		map.put(DataStoresConstants.DATABASE_ATTRIBUTE_NAME_OF_USER_STATUS, asSet(getAttributeNameOfUserStatus()));
		map.put(DataStoresConstants.DATABASE_USER_STATUS_ACTIVE_VALUE, asSet(getUserStatusActiveValue()));
		map.put(DataStoresConstants.DATABASE_USER_STATUS_INACTIVE_VALUE, asSet(getUserStatusInActiveValue()));
		map.put(DataStoresConstants.DATABASE_MAXIMUM_RESULTS_RETURNED_FROM_SEARCH,
				asSet(getMaximumResultsReturnedFromSearch()));
		map.put(DataStoresConstants.USERS_SEARCH_ATTRIBUTE_IN_DATABASE, asSet(getUsersSearchAttributeInDatabase()));
		map.put(DataStoresConstants.DATABASE_MEMBERSHIP_TABLE_NAME, asSet(getDatabaseMembershipTableName()));
		map.put(DataStoresConstants.MEMBERSHIP_ID_ATTRIBUTE_NAME, asSet(getMembershipIdAttributeName()));
		map.put(DataStoresConstants.MEMBERSHIP_SEARCH_ATTRIBUTE_IN_DATABASE,
				asSet(getMembershipSearchAttributeInDatabase()));
		return map;

	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param map
	 *            - map of attributes.
	 * 
	 */
	public static DatabaseBean fromMap(Map<String, Set<String>> map) {
		DatabaseBean bean = new DatabaseBean();
		BaseDataStore.setBaseFields(map, bean);

		bean.setAttributeNameMapping(listFromSet(map.get(DataStoresConstants.ATTRIBUTE_NAME_MAPPING)));
		bean.setDatabaseRepositoryPluginClassName(stringFromSet(map
				.get(DataStoresConstants.DATABASE_REPOSITORY_PLUGIN_CLASS_NAME)));
		bean.setDatabasePlugInSupportedTypesAndOperations(listFromSet(map
				.get(DataStoresConstants.DATABASE_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES)));
		bean.setDatabaseDataAccessObjectPluginClassName(stringFromSet(map
				.get(DataStoresConstants.DATABASE_DATA_ACCESS_OBJECT_PLUGIN_CLASS_NAME)));
		bean.setConnectionType(stringFromSet(map.get(DataStoresConstants.CONNECTION_TYPE)));
		bean.setDatabaseDatasourceName(stringFromSet(map.get(DataStoresConstants.DATABASE_DATASOURCE_NAME)));
		bean.setJdbcDriverClassName(stringFromSet(map.get(DataStoresConstants.JDBC_DRIVER_CLASS_NAME)));

		bean.setJdbcDriverUrl(stringFromSet(map.get(DataStoresConstants.JDBC_DRIVER_URL)));
		bean.setConnectThisUserToDatabase(stringFromSet(map.get(DataStoresConstants.CONNECT_THIS_USER_TO_DATABASE)));
		bean.setDatabaseUserTableName(stringFromSet(map.get(DataStoresConstants.DATABASE_USER_TABLE_NAME)));
		bean.setListOfUserAttributesNamesInDatabase(listFromSet(map
				.get(DataStoresConstants.LIST_OF_USER_ATTRIBUTES_NAMES_IN_DATABASE_CURRENT_VALUES)));
		bean.setUserPasswordAttributeName(stringFromSet(map.get(DataStoresConstants.USER_PASSWORD_ATTRIBUTE_NAME)));
		bean.setUserIdAttributeName(stringFromSet(map.get(DataStoresConstants.USER_ID_ATTRIBUTE_NAME)));
		bean.setAttributeNameOfUserStatus(stringFromSet(map
				.get(DataStoresConstants.DATABASE_ATTRIBUTE_NAME_OF_USER_STATUS)));
		bean.setUserStatusActiveValue(stringFromSet(map.get(DataStoresConstants.DATABASE_USER_STATUS_ACTIVE_VALUE)));
		bean.setUserStatusInActiveValue(stringFromSet(map.get(DataStoresConstants.DATABASE_USER_STATUS_INACTIVE_VALUE)));
		bean.setMaximumResultsReturnedFromSearch(integerFromSet(map
				.get(DataStoresConstants.DATABASE_MAXIMUM_RESULTS_RETURNED_FROM_SEARCH)));
		bean.setUsersSearchAttributeInDatabase(stringFromSet(map
				.get(DataStoresConstants.USERS_SEARCH_ATTRIBUTE_IN_DATABASE)));
		bean.setDatabaseMembershipTableName(stringFromSet(map.get(DataStoresConstants.DATABASE_MEMBERSHIP_TABLE_NAME)));
		bean.setMembershipIdAttributeName(stringFromSet(map.get(DataStoresConstants.MEMBERSHIP_ID_ATTRIBUTE_NAME)));
		bean.setMembershipSearchAttributeInDatabase(stringFromSet(map
				.get(DataStoresConstants.MEMBERSHIP_SEARCH_ATTRIBUTE_IN_DATABASE)));

		bean.setPasswordForConnectingToDatabase("".equals(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_PASSWORD))) ? ""
				: DataStoresConstants.DEFAULT_PASSWORD);
		bean.setPasswordForConnectingToDatabaseConfirm("".equals(stringFromSet(map
				.get(DataStoresConstants.LDAP_BIND_PASSWORD))) ? "" : DataStoresConstants.DEFAULT_PASSWORD);

		return bean;
	}

}
