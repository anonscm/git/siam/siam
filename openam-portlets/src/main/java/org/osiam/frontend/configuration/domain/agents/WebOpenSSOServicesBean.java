/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a web open SSO services bean.
 * 
 */
public class WebOpenSSOServicesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private List<String> openSSOLoginURL = new ArrayList<String>();
	@NotNull
	@Min(0)
	private Integer agentConnectionTimeout;
	@NotNull
	@Min(0)
	private Integer pollingPeriodForPrimaryServer;
	private List<String> openSSOLogoutURL = new ArrayList<String>();
	private List<String> logoutURLList = new ArrayList<String>();
	private List<String> logoutCookiesListForReset = new ArrayList<String>();
	private String logoutRedirectURL;
	@NotNull
	@Min(0)
	private Integer policyCachePollingPeriod;
	@NotNull
	@Min(0)
	private Integer ssoCachePollingPeriod;
	private String userIDParameter;
	private String userIDParameterType;
	private boolean fetchPoliciesFromRootResource;
	private boolean retrieveClientHostname;
	@NotNull
	@Min(0)
	private Integer policyClockSkew;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getOpenSSOLoginURL() {
		return openSSOLoginURL;
	}

	public void setOpenSSOLoginURL(List<String> openSSOLoginURL) {
		this.openSSOLoginURL = openSSOLoginURL;
	}

	public Integer getAgentConnectionTimeout() {
		return agentConnectionTimeout;
	}

	public void setAgentConnectionTimeout(Integer agentConnectionTimeout) {
		this.agentConnectionTimeout = agentConnectionTimeout;
	}

	public Integer getPollingPeriodForPrimaryServer() {
		return pollingPeriodForPrimaryServer;
	}

	public void setPollingPeriodForPrimaryServer(Integer pollingPeriodForPrimaryServer) {
		this.pollingPeriodForPrimaryServer = pollingPeriodForPrimaryServer;
	}

	public List<String> getOpenSSOLogoutURL() {
		return openSSOLogoutURL;
	}

	public void setOpenSSOLogoutURL(List<String> openSSOLogoutURL) {
		this.openSSOLogoutURL = openSSOLogoutURL;
	}

	public List<String> getLogoutURLList() {
		return logoutURLList;
	}

	public void setLogoutURLList(List<String> logoutURLList) {
		this.logoutURLList = logoutURLList;
	}

	public List<String> getLogoutCookiesListForReset() {
		return logoutCookiesListForReset;
	}

	public void setLogoutCookiesListForReset(List<String> logoutCookiesListForReset) {
		this.logoutCookiesListForReset = logoutCookiesListForReset;
	}

	public String getLogoutRedirectURL() {
		return logoutRedirectURL;
	}

	public void setLogoutRedirectURL(String logoutRedirectURL) {
		this.logoutRedirectURL = logoutRedirectURL;
	}

	public Integer getPolicyCachePollingPeriod() {
		return policyCachePollingPeriod;
	}

	public void setPolicyCachePollingPeriod(Integer policyCachePollingPeriod) {
		this.policyCachePollingPeriod = policyCachePollingPeriod;
	}

	public Integer getSsoCachePollingPeriod() {
		return ssoCachePollingPeriod;
	}

	public void setSsoCachePollingPeriod(Integer ssoCachePollingPeriod) {
		this.ssoCachePollingPeriod = ssoCachePollingPeriod;
	}

	public String getUserIDParameter() {
		return userIDParameter;
	}

	public void setUserIDParameter(String userIDParameter) {
		this.userIDParameter = userIDParameter;
	}

	public String getUserIDParameterType() {
		return userIDParameterType;
	}

	public void setUserIDParameterType(String userIDParameterType) {
		this.userIDParameterType = userIDParameterType;
	}

	public boolean getFetchPoliciesFromRootResource() {
		return fetchPoliciesFromRootResource;
	}

	public void setFetchPoliciesFromRootResource(boolean fetchPoliciesFromRootResource) {
		this.fetchPoliciesFromRootResource = fetchPoliciesFromRootResource;
	}

	public boolean getRetrieveClientHostname() {
		return retrieveClientHostname;
	}

	public void setRetrieveClientHostname(boolean retrieveClientHostname) {
		this.retrieveClientHostname = retrieveClientHostname;
	}

	public Integer getPolicyClockSkew() {
		return policyClockSkew;
	}

	public void setPolicyClockSkew(Integer policyClockSkew) {
		this.policyClockSkew = policyClockSkew;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebOpenSSOServicesBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebOpenSSOServicesBean bean = new WebOpenSSOServicesBean();
		bean.setName(name);
		bean.setOpenSSOLoginURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.OPENSSO_LOGIN_URL_CURRENT_VALUES))));
		bean.setAgentConnectionTimeout(integerFromSet(attributes.get(AgentAttributesConstants.AGENT_CONNECTION_TIMEOUT)));
		bean.setPollingPeriodForPrimaryServer(integerFromSet(attributes
				.get(AgentAttributesConstants.POLLING_PERIOD_FOR_PRIMARY_SERVER)));
		bean.setOpenSSOLogoutURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.OPENSSO_LOGOUT_URL_CURRENT_VALUES))));
		bean.setLogoutURLList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.LOGOUT_URL_LIST_CURRENT_VALUES))));
		bean.setLogoutCookiesListForReset(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.LOGOUT_COOKIES_LIST_FOR_RESET_CURRENT_VALUES))));
		bean.setLogoutRedirectURL(stringFromSet(attributes.get(AgentAttributesConstants.LOGOUT_REDIRECT_URL)));
		bean.setPolicyCachePollingPeriod(integerFromSet(attributes
				.get(AgentAttributesConstants.POLICY_CACHE_POLLING_PERIOD)));
		bean.setSsoCachePollingPeriod(integerFromSet(attributes.get(AgentAttributesConstants.SSO_CACHE_POLLING_PERIOD)));
		bean.setUserIDParameter(stringFromSet(attributes.get(AgentAttributesConstants.USER_ID_PARAMETER)));
		bean.setUserIDParameterType(stringFromSet(attributes.get(AgentAttributesConstants.USER_ID_PARAMETER_TYPE)));
		bean.setFetchPoliciesFromRootResource(booleanFromSet(attributes
				.get(AgentAttributesConstants.FETCH_POLICIES_FROM_ROOT_RESOURCE)));
		bean.setRetrieveClientHostname(booleanFromSet(attributes.get(AgentAttributesConstants.RETRIEVE_CLIENT_HOSTNAME)));
		bean.setPolicyClockSkew(integerFromSet(attributes.get(AgentAttributesConstants.POLICY_CLOCK_SKEW)));

		return bean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		map.put(AgentAttributesConstants.OPENSSO_LOGIN_URL_CURRENT_VALUES, asSet(addIndexPrefix(getOpenSSOLoginURL())));
		map.put(AgentAttributesConstants.AGENT_CONNECTION_TIMEOUT, asSet(getAgentConnectionTimeout()));
		map.put(AgentAttributesConstants.POLLING_PERIOD_FOR_PRIMARY_SERVER, asSet(getPollingPeriodForPrimaryServer()));
		map.put(AgentAttributesConstants.OPENSSO_LOGOUT_URL_CURRENT_VALUES,
				asSet(addIndexPrefix(getOpenSSOLogoutURL())));
		map.put(AgentAttributesConstants.LOGOUT_URL_LIST_CURRENT_VALUES, asSet(addIndexPrefix(getLogoutURLList())));
		map.put(AgentAttributesConstants.LOGOUT_COOKIES_LIST_FOR_RESET_CURRENT_VALUES,
				asSet(addIndexPrefix(getLogoutCookiesListForReset())));
		map.put(AgentAttributesConstants.LOGOUT_REDIRECT_URL, asSet(getLogoutRedirectURL()));
		map.put(AgentAttributesConstants.POLICY_CACHE_POLLING_PERIOD, asSet(getPolicyCachePollingPeriod()));
		map.put(AgentAttributesConstants.SSO_CACHE_POLLING_PERIOD, asSet(getSsoCachePollingPeriod()));
		map.put(AgentAttributesConstants.USER_ID_PARAMETER, asSet(getUserIDParameter()));
		map.put(AgentAttributesConstants.USER_ID_PARAMETER_TYPE, asSet(getUserIDParameterType()));
		map.put(AgentAttributesConstants.FETCH_POLICIES_FROM_ROOT_RESOURCE, asSet(getFetchPoliciesFromRootResource()));
		map.put(AgentAttributesConstants.RETRIEVE_CLIENT_HOSTNAME, asSet(getRetrieveClientHostname()));
		map.put(AgentAttributesConstants.POLICY_CLOCK_SKEW, asSet(getPolicyClockSkew()));

		return map;
	}

}
