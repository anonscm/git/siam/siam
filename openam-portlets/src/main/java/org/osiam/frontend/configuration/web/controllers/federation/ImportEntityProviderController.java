/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.ImportEntityProviderBean;
import org.osiam.frontend.configuration.exceptions.FederationException;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * ImportEntityProviderController shows the import entity provider form and does
 * request processing of the import entity provider action.
 * 
 */
@Controller("ImportEntityProviderController")
@RequestMapping(value = "view", params = "ctx=ImportEntityProviderController")
public class ImportEntityProviderController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImportEntityProviderController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("importEntityProviderBeanValidator")
	private Validator importEntityProviderBeanValidator;

	private String fileTemp;

	public void setFileTemp(String fileTemp) {
		this.fileTemp = fileTemp;
	}

	/**
	 * Shows the import entity provider form.
	 * 
	 * @param error
	 *            - error message
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return the view's name
	 */
	@RenderMapping
	public String view(String error, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		model.addAttribute("realm", realm);
		model.addAttribute("error", error);

		return "federation/entityProvider/import";
	}

	/**
	 * Import entity provider.
	 * 
	 * @param importEntityProvider
	 *            - ImportEntityProviderBean
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("importEntityProvider") @Valid ImportEntityProviderBean importEntityProvider,
			BindingResult result, ActionRequest request, ActionResponse response) {

		importEntityProviderBeanValidator.validate(importEntityProvider, result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		String metadataFile = "";
		String metadataFileContent = null;

		if (FederationConstants.LOCATION_TYPE_URL.equals(importEntityProvider.getMetadataFileLocationType())) {
			metadataFile = getFileURLLocationFileName(importEntityProvider.getMetadataFileURLLocation());
		} else {
			metadataFile = getFileLocationFileName(importEntityProvider.getMetadataFileLocation());
		}

		if (metadataFile != null && !"".equals(metadataFile)) {
			metadataFileContent = getFileContent(metadataFile);
		}

		String extendedDataFile = "";
		String extendedDataFileContent = null;

		if (FederationConstants.LOCATION_TYPE_URL.equals(importEntityProvider.getExtendedDataFileLocationType())) {
			extendedDataFile = getFileURLLocationFileName(importEntityProvider.getExtendedDataFileURLLocation());
		} else {
			extendedDataFile = getFileLocationFileName(importEntityProvider.getExtendedDataFileLocation());
		}

		if (extendedDataFile != null && !"".equals(extendedDataFile)) {
			extendedDataFileContent = getFileContent(extendedDataFile);
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		
		String importMessage= federationService.importEntityProvider(token, realm, metadataFileContent, extendedDataFileContent);
		if (importMessage != null) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid xml");
			}
			response.setRenderParameter("error", importMessage);
			return;
		}

		response.setRenderParameter("ctx", "FederationController");

	}

	/**
	 * Cancel import form.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "cancel")
	public void doCancel(ActionRequest request, ActionResponse response) {

	}
	
	/**
	 * Back to list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "FederationController");
	}

	/**
	 * Upload file.
	 * 
	 * @param file
	 *            - file to upload
	 * @return name of the uploaded file
	 */
	private String getFileLocationFileName(MultipartFile file) {

		try {
			String fileName = file.getName();
			if (!fileName.equals("")) {
				File fileToCreate = new File(fileTemp, fileName);
				file.transferTo(fileToCreate);

				return fileTemp + fileName;
			}
		} catch (IOException e) {
			LOGGER.error("IOException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Download file and save on disc.
	 * 
	 * @param file
	 *            - file url to download
	 * @return - name of the download file
	 */
	private String getFileURLLocationFileName(String file) {
		InputStream is = null;
		OutputStream outStream = null;

		int slashIndex = file.lastIndexOf('/');
		String fileName = file.substring(slashIndex + 1);

		if (file != null && !"".equals(file) && !fileName.equals("")) {
			try {
				int ByteRead, ByteWritten = 0;
				URL url = new URL(file);
				outStream = new BufferedOutputStream(new FileOutputStream(fileTemp + fileName));
				URLConnection uCon = url.openConnection();
				is = uCon.getInputStream();
				byte[] buf = new byte[1024];
				while ((ByteRead = is.read(buf)) != -1) {
					outStream.write(buf, 0, ByteRead);
					ByteWritten += ByteRead;
				}
				return fileTemp + fileName;

			} catch (IOException e) {
				LOGGER.error("IOException occured", e);
				throw new FederationException(e.getMessage(), e);
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					LOGGER.error("IOException occured", e);
					throw new FederationException(e.getMessage(), e);
				}
				try {
					outStream.close();
				} catch (IOException e) {
					LOGGER.error("IOException occured", e);
					throw new FederationException(e.getMessage(), e);
				}
			}
		}

		return null;
	}

	/**
	 * Get file content.
	 * 
	 * @param fileName
	 *            - file name
	 * @return content file
	 */
	private String getFileContent(String fileName) {
		StringBuffer result = new StringBuffer();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String str;
			while ((str = in.readLine()) != null) {
				result.append(str);
			}

		} catch (IOException e) {
			LOGGER.error("IOException occured", e);
			throw new FederationException(e.getMessage(), e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				LOGGER.error("IOException occured", e);
				throw new FederationException(e.getMessage(), e);
			}
		}

		deleteFile(fileName);

		return result.toString();
	}

	/**
	 * Delete file.
	 * 
	 * @param fileName
	 *            - file name
	 */
	private void deleteFile(String fileName) {

		File file = new File(fileName);
		file.delete();
	}

}
