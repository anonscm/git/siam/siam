/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * CircleOfTrustController shows the add/edit {@link CircleOfTrust} form and
 * does request processing of the edit/add {@link CircleOfTrust} action.
 * 
 */
@Controller("CircleOfTrustController")
@RequestMapping(value = "view", params = "ctx=CircleOfTrustController")
public class CircleOfTrustController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CircleOfTrustController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("circleOfTrustUrlValidator")
	private Validator circleOfTrustUrlValidator;

	/**
	 * Shows add/edit {@link CircleOfTrust} form.
	 * 
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param selectedValue
	 *            - TrustedProviders values
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return {@link String} the view's name
	 * 
	 */
	@RenderMapping
	public String view(String circleOfTrustName, String action, String[] selectedValue, Model model,
			RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "circleOfTrust")) {
			CircleOfTrust circleOfTrust = new CircleOfTrust();
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				circleOfTrust = federationService.getCircleOfTrust(token, realm, circleOfTrustName);
			} else {
				circleOfTrust = new CircleOfTrust();
				circleOfTrust.setName(circleOfTrustName);
				circleOfTrust.setStatus("active");
				circleOfTrust.setRealm(realm);

			}
			selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
			circleOfTrust.getTrustedProviders().toArray(selectedValue);
			model.addAttribute("circleOfTrust", circleOfTrust);
		}

		model.addAttribute("circleOfTrustName", circleOfTrustName);
		model.addAttribute("actionValue", action);
		model.addAttribute("availabelEntities",
				getAvailablelEntities(federationService.getAvailablelEntities(token, realm), selectedValue));

		return "federation/circleOfTrust/edit";
	}

	/**
	 * Get available entities.
	 * 
	 * @param list
	 *            - all entities
	 * @param selectedValue
	 *            - existing entities
	 * @return - all the entities that are not selected
	 */
	private List<String> getAvailablelEntities(List<String> list, String[] selectedValue) {

		if (selectedValue != null) {
			for (int i = 0; i < selectedValue.length; i++) {
				((ArrayList<String>) list).remove(selectedValue[i]);
			}
		}
		return list;

	}

	/**
	 * Add values for TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param availableEntitiesSelected
	 *            - {@link List} of selected values
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addEntities")
	public void doAddEntities(CircleOfTrust circleOfTrust, String[] availableEntitiesSelected,
			String circleOfTrustName, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		circleOfTrust.setTrustedProviders(addValue(availableEntitiesSelected, circleOfTrust.getTrustedProviders()));

		String[] selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
		circleOfTrust.getTrustedProviders().toArray(selectedValue);
		response.setRenderParameter("selectedValue", selectedValue);

	}

	/**
	 * Add values in given list.
	 * 
	 * @param availableEntitiesSelected
	 *            - values to be added
	 * @param list
	 *            - given list
	 * @return - list that contains all the values
	 */
	private List<String> addValue(String[] availableEntitiesSelected, List<String> list) {
		if (availableEntitiesSelected != null) {
			for (int i = 0; i < availableEntitiesSelected.length; i++) {
				list.add(availableEntitiesSelected[i]);
			}
		}
		return list;
	}

	/**
	 * Add all the available values TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param allAvailabelEntities
	 *            - all available values
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAllEntities")
	public void doAddAllEntities(CircleOfTrust circleOfTrust, String[] allAvailabelEntities, String circleOfTrustName,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		circleOfTrust.setTrustedProviders(addValue(allAvailabelEntities, circleOfTrust.getTrustedProviders()));

		String[] selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
		circleOfTrust.getTrustedProviders().toArray(selectedValue);
		response.setRenderParameter("selectedValue", selectedValue);

	}

	/**
	 * Remove value from TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param selectedEntities
	 *            - values to be deleted
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "removeEntities")
	public void doRemoveEntities(CircleOfTrust circleOfTrust, String[] selectedEntities, String circleOfTrustName,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		circleOfTrust.setTrustedProviders(deleteValue(selectedEntities, circleOfTrust.getTrustedProviders()));

		String[] selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
		circleOfTrust.getTrustedProviders().toArray(selectedValue);
		response.setRenderParameter("selectedValue", selectedValue);

	}

	/**
	 * Remove values form given list.
	 * 
	 * @param selectedEntities
	 *            - values to be deleted
	 * @param list
	 *            - given list
	 * @return - list that contains all the values without selected value
	 */
	private List<String> deleteValue(String[] selectedEntities, List<String> list) {
		if (selectedEntities != null) {
			for (int i = 0; i < selectedEntities.length; i++) {
				list.remove(selectedEntities[i]);
			}
		}
		return list;
	}

	/**
	 * Remove all the values from TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "removeAllEntities")
	public void doRemoveAllEntities(CircleOfTrust circleOfTrust, String circleOfTrustName, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		circleOfTrust.setTrustedProviders(null);

	}

	/**
	 * Move up values from TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param selectedEntities
	 *            - values to be moved
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpEntities")
	public void doMoveUp(CircleOfTrust circleOfTrust, String[] selectedEntities, String circleOfTrustName,
			ActionRequest request, ActionResponse response) {

		if (selectedEntities != null) {
			for (int i = 0; i < selectedEntities.length; i++) {
				int poz = circleOfTrust.getTrustedProviders().indexOf(selectedEntities[i]);
				if (poz > 0) {
					circleOfTrust.setTrustedProviders(moveUp(circleOfTrust.getTrustedProviders(), poz));
				}
			}
		}
		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		String[] selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
		circleOfTrust.getTrustedProviders().toArray(selectedValue);
		response.setRenderParameter("selectedValue", selectedValue);

	}

	/**
	 * Move down values from TrustedProviders list.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust}
	 * @param selectedEntities
	 *            - values to be moved
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownEntities")
	public void doMoveDown(CircleOfTrust circleOfTrust, String[] selectedEntities, String circleOfTrustName,
			ActionRequest request, ActionResponse response) {

		if (selectedEntities != null) {
			for (int i = selectedEntities.length - 1; i >= 0; i--) {
				int poz = circleOfTrust.getTrustedProviders().indexOf(selectedEntities[i]);
				if (poz < circleOfTrust.getTrustedProviders().size() - 1) {
					circleOfTrust.setTrustedProviders(moveDown(circleOfTrust.getTrustedProviders(), poz));
				}
			}
		}
		response.setRenderParameter("circleOfTrustName", circleOfTrustName);

		String[] selectedValue = new String[circleOfTrust.getTrustedProviders().size()];
		circleOfTrust.getTrustedProviders().toArray(selectedValue);
		response.setRenderParameter("selectedValue", selectedValue);
	}

	/**
	 * Save {@link CircleOfTrust}.
	 * 
	 * @param circleOfTrust
	 *            - {@link CircleOfTrust} to be added
	 * @param result
	 *            - BindingResult
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("circleOfTrust") @Valid CircleOfTrust circleOfTrust, BindingResult result,
			String circleOfTrustName, String action, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		circleOfTrustUrlValidator.validate(circleOfTrust, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			response.setRenderParameter("circleOfTrustName", circleOfTrust.getName());
			return;
		}

		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			federationService.updateCircleOfTrust(token, realm, circleOfTrust);
		} else {
			federationService.addCircleOfTrust(token, realm, circleOfTrust);
		}

		response.setRenderParameter("ctx", "FederationController");

	}

	/**
	 * Reset add/edit {@link CircleOfTrust} form.
	 * 
	 * @param circleOfTrustName
	 *            - {@link CircleOfTrust} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String circleOfTrustName, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("circleOfTrustName", circleOfTrustName);
	}

	/**
	 * Back to list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "FederationController");
	}

}
