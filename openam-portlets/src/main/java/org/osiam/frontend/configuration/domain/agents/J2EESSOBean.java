/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removeIndexPrefix;
import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.asSetWithSpecialEmptyList;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a j2ee SSO bean.
 */
public class J2EESSOBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String cookieName;
	private boolean ssoCacheEnable;
	private boolean crossDomainSSO;
	private String cdssoRedirectURI;
	private List<String> cdssoServletURL = new ArrayList<String>();
	@NotNull
	@Min(0)
	private Integer cdssoClockSkew;
	private List<String> cdssoTrustedIDProvider = new ArrayList<String>();
	private boolean cdssoSecureEnable;
	private List<String> cdssoDomainList = new ArrayList<String>();
	private boolean cookieReset;
	private List<String> cookiesResetNameList = new ArrayList<String>();
	private List<String> cookiesResetDomainMap = new ArrayList<String>();
	private List<String> cookiesResetPathMap = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCookieName() {
		return cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public boolean getSsoCacheEnable() {
		return ssoCacheEnable;
	}

	public void setSsoCacheEnable(boolean ssoCacheEnable) {
		this.ssoCacheEnable = ssoCacheEnable;
	}

	public boolean getCrossDomainSSO() {
		return crossDomainSSO;
	}

	public void setCrossDomainSSO(boolean crossDomainSSO) {
		this.crossDomainSSO = crossDomainSSO;
	}

	public String getCdssoRedirectURI() {
		return cdssoRedirectURI;
	}

	public void setCdssoRedirectURI(String cdssoRedirectURI) {
		this.cdssoRedirectURI = cdssoRedirectURI;
	}

	public List<String> getCdssoServletURL() {
		return cdssoServletURL;
	}

	public void setCdssoServletURL(List<String> cdssoServletURL) {
		this.cdssoServletURL = cdssoServletURL;
	}

	public Integer getCdssoClockSkew() {
		return cdssoClockSkew;
	}

	public void setCdssoClockSkew(Integer cdssoClockSkew) {
		this.cdssoClockSkew = cdssoClockSkew;
	}

	public List<String> getCdssoTrustedIDProvider() {
		return cdssoTrustedIDProvider;
	}

	public void setCdssoTrustedIDProvider(List<String> cdssoTrustedIDProvider) {
		this.cdssoTrustedIDProvider = cdssoTrustedIDProvider;
	}

	public boolean getCdssoSecureEnable() {
		return cdssoSecureEnable;
	}

	public void setCdssoSecureEnable(boolean cdssoSecureEnable) {
		this.cdssoSecureEnable = cdssoSecureEnable;
	}

	public List<String> getCdssoDomainList() {
		return cdssoDomainList;
	}

	public void setCdssoDomainList(List<String> cdssoDomainList) {
		this.cdssoDomainList = cdssoDomainList;
	}

	public boolean getCookieReset() {
		return cookieReset;
	}

	public void setCookieReset(boolean cookieReset) {
		this.cookieReset = cookieReset;
	}

	public List<String> getCookiesResetNameList() {
		return cookiesResetNameList;
	}

	public void setCookiesResetNameList(List<String> cookiesResetNameList) {
		this.cookiesResetNameList = cookiesResetNameList;
	}

	public List<String> getCookiesResetDomainMap() {
		return cookiesResetDomainMap;
	}

	public void setCookiesResetDomainMap(List<String> cookiesResetDomainMap) {
		this.cookiesResetDomainMap = cookiesResetDomainMap;
	}

	public List<String> getCookiesResetPathMap() {
		return cookiesResetPathMap;
	}

	public void setCookiesResetPathMap(List<String> cookiesResetPathMap) {
		this.cookiesResetPathMap = cookiesResetPathMap;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 */
	public static J2EESSOBean fromMap(String name, Map<String, Set<String>> attributes) {
		J2EESSOBean bean = new J2EESSOBean();
		bean.setName(name);
		bean.setCookieName(stringFromSet(attributes.get(AgentAttributesConstants.J2EE_SSO_COOKIE_NAME)));
		bean.setSsoCacheEnable(booleanFromSet(attributes.get(AgentAttributesConstants.SSO_CACHE_ENABLE)));
		bean.setCrossDomainSSO(booleanFromSet(attributes.get(AgentAttributesConstants.CROSS_DOMAIN_SSO)));
		bean.setCdssoRedirectURI(stringFromSet(attributes.get(AgentAttributesConstants.CDSSO_REDIRECT_URI)));
		bean.setCdssoServletURL(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.CDSSO_SERVLET_URL))));
		bean.setCdssoClockSkew(integerFromSet(attributes.get(AgentAttributesConstants.CDSSO_CLOCK_SKEW)));
		bean.setCdssoTrustedIDProvider(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.CDSSO_TRUSTED_ID_PROVIDER))));
		bean.setCdssoSecureEnable(booleanFromSet(attributes.get(AgentAttributesConstants.CDSSO_SECURE_ENABLE)));
		bean.setCdssoDomainList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.CDSSO_DOMAIN_LIST_CURRENT_VALUES))));
		bean.setCookieReset(booleanFromSet(attributes.get(AgentAttributesConstants.COOKIE_RESET)));
		bean.setCookiesResetNameList(removeIndexPrefix(listFromSet(attributes
				.get(AgentAttributesConstants.COOKIES_RESET_NAME_LIST))));
		bean.setCookiesResetDomainMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.COOKIES_RESET_DOMAIN_MAP_CURRENT_VALUES)), ""));
		bean.setCookiesResetPathMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.COOKIES_RESET_PATH_MAP_CURRENT_VALUES)), ""));

		return bean;
	}

	/**
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		if (getCookieName() != null) {
			if (!"".equals(getCookieName())) {
				map.put(AgentAttributesConstants.J2EE_SSO_COOKIE_NAME, asSet(getCookieName()));
			}
		}
		map.put(AgentAttributesConstants.SSO_CACHE_ENABLE, asSet(getSsoCacheEnable()));
		map.put(AgentAttributesConstants.CROSS_DOMAIN_SSO, asSet(getCrossDomainSSO()));

		map.put(AgentAttributesConstants.CDSSO_REDIRECT_URI, asSet(getCdssoRedirectURI()));
		map.put(AgentAttributesConstants.CDSSO_SERVLET_URL, asSet(addIndexPrefix(getCdssoServletURL())));
		map.put(AgentAttributesConstants.CDSSO_CLOCK_SKEW, asSet(getCdssoClockSkew()));
		map.put(AgentAttributesConstants.CDSSO_TRUSTED_ID_PROVIDER, asSet(addIndexPrefix(getCdssoTrustedIDProvider())));

		map.put(AgentAttributesConstants.CDSSO_SECURE_ENABLE, asSet(getCdssoSecureEnable()));
		map.put(AgentAttributesConstants.CDSSO_DOMAIN_LIST_CURRENT_VALUES, asSet(addIndexPrefix(getCdssoDomainList())));
		map.put(AgentAttributesConstants.COOKIE_RESET, asSet(getCookieReset()));
		map.put(AgentAttributesConstants.COOKIES_RESET_NAME_LIST, asSet(addIndexPrefix(getCookiesResetNameList())));
		map.put(AgentAttributesConstants.COOKIES_RESET_DOMAIN_MAP_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getCookiesResetDomainMap()));
		map.put(AgentAttributesConstants.COOKIES_RESET_PATH_MAP_CURRENT_VALUES,
				asSetWithSpecialEmptyList(getCookiesResetPathMap()));

		return map;
	}

}
