/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * 
 * Encapsulates data for a SAMLV2 XACML PDP Attributes Bean.
 * 
 */

public class SAMLV2XACMLPDPAttributesBean extends EntityProviderDescriptor {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2XACMLPDPAttributesBean.class);

	private String protocolSupportEnumeration;

	private boolean basicAuthorizationEnabled;
	private String basicAuthorizationUser;
	private String basicAuthorizationPassword;

	private boolean authorizationDecisionQuerySigned;

	private String authorizationServiceBinding;
	private String authorizationServiceLocation;

	public boolean getBasicAuthorizationEnabled() {
		return basicAuthorizationEnabled;
	}

	public void setBasicAuthorizationEnabled(boolean basicAuthorizationEnabled) {
		this.basicAuthorizationEnabled = basicAuthorizationEnabled;
	}

	public String getBasicAuthorizationUser() {
		return basicAuthorizationUser;
	}

	public void setBasicAuthorizationUser(String basicAuthorizationUser) {
		this.basicAuthorizationUser = basicAuthorizationUser;
	}

	public String getBasicAuthorizationPassword() {
		return basicAuthorizationPassword;
	}

	public void setBasicAuthorizationPassword(String basicAuthorizationPassword) {
		this.basicAuthorizationPassword = basicAuthorizationPassword;
	}

	public boolean getAuthorizationDecisionQuerySigned() {
		return authorizationDecisionQuerySigned;
	}

	public void setAuthorizationDecisionQuerySigned(boolean authorizationDecisionQuerySigned) {
		this.authorizationDecisionQuerySigned = authorizationDecisionQuerySigned;
	}

	public String getAuthorizationServiceLocation() {
		return authorizationServiceLocation;
	}

	public void setAuthorizationServiceLocation(String authorizationServiceLocation) {
		this.authorizationServiceLocation = authorizationServiceLocation;
	}

	public String getAuthorizationServiceBinding() {
		return authorizationServiceBinding;
	}

	public void setAuthorizationServiceBinding(String authorizationServiceBinding) {
		this.authorizationServiceBinding = authorizationServiceBinding;
	}

	public String getProtocolSupportEnumeration() {
		return protocolSupportEnumeration;
	}

	public void setProtocolSupportEnumeration(String protocolSupportEnumeration) {
		this.protocolSupportEnumeration = protocolSupportEnumeration;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static EntityProviderDescriptor fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));
		Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);
		Node configRoot = entityConfigDocument.getFirstChild();
		Node configNode = XmlUtil.getChildNode(configRoot, FederationConstants.XACML_PDP_CONFIG_NODE);

		String entityDescriptor = stringFromSet(map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));
		Document entityDescriptorDocument = XmlUtil.toDOMDocument(entityDescriptor);
		SAMLV2XACMLPDPAttributesBean ret = new SAMLV2XACMLPDPAttributesBean();
		if (entityDescriptorDocument == null) {
			return ret;
		}
		Node descriptorRoot = entityDescriptorDocument.getFirstChild();
		Node descriptorNode = XmlUtil.getChildNode(descriptorRoot, FederationConstants.XACML_PDP_DESCRIPTOR_NODE);

		ret.setType("xacml_policy_decision_point");
		ret.setMetaAlias(XmlUtil.getNodeAttributeValue(configNode, "metaAlias"));

		ret.setProtocolSupportEnumeration(XmlUtil.getNodeAttributeValue(descriptorNode, "protocolSupportEnumeration"));

		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals("signingCertAlias")) {
				ret.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("encryptionCertAlias")) {
				ret.setEncryptionCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthUser")) {
				ret.setBasicAuthorizationUser(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthPassword")) {
				ret.setBasicAuthorizationPassword(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthOn")) {
				ret.setBasicAuthorizationEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals("wantXACMLAuthzDecisionQuerySigned")) {
				ret.setAuthorizationDecisionQuerySigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			}
		}

		if (XmlUtil.getChildNodes(descriptorNode, "XACMLAuthzService").size() > 0) {
			Node locationNode = XmlUtil.getChildNode(descriptorNode, "XACMLAuthzService");
			ret.setAuthorizationServiceBinding(XmlUtil.getNodeAttributeValue(locationNode, "Binding"));
			ret.setAuthorizationServiceLocation(XmlUtil.getNodeAttributeValue(locationNode, "Location"));
		}

		return ret;
	}

	/**
	 * SAMLV2XACMLPDPAttributesBean constructor.
	 */
	public SAMLV2XACMLPDPAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_XACML_POLICY_DECISION_POINT);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put(FederationConstants.META_ALIAS_ATTRIBUTE, getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				FederationConstants.XACML_PDP_CONFIG_NODE, queryAttributes);
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthUser", getBasicAuthorizationUser());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthPassword", getBasicAuthorizationPassword());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthOn",
				String.valueOf(getBasicAuthorizationEnabled()));
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "wantXACMLAuthzDecisionQuerySigned",
				String.valueOf(getAuthorizationDecisionQuerySigned()));

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);
		Map<String, String> descriptorAttributes = new HashMap<String, String>();
		descriptorAttributes.put("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
		Node attributeQueryDescriptorNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
				"XACMLPDPDescriptor", descriptorAttributes);

		// Start Key Descriptor Signing

		if (getSigningCertificateAlias() != null && !"".equals(getSigningCertificateAlias())) {

			Map<String, String> signingAttributes = new HashMap<String, String>();
			signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
			Node signingNode = XmlUtil.createElement(attributeQueryDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, signingAttributes);

			Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getSigningCertificateAlias());
			String base64SigningCertificate = "";
			if (cert != null) {
				base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);
			}
			
			Node x509CertificateNode = XmlUtil.createTextElement(x509Node,
					FederationConstants.X509CERTIFICATE_NODE, "http://www.w3.org/2000/09/xmldsig#",
					base64SigningCertificate, null);
			x509CertificateNode.setPrefix("ds");

		}
		// Start Key Descriptor Encryption

		if (getEncryptionCertificateAlias() != null && !"".equals(getEncryptionCertificateAlias())) {

			Map<String, String> encryptionAttributes = new HashMap<String, String>();
			encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
			Node encryptionNode = XmlUtil.createElement(attributeQueryDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, encryptionAttributes);

			Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getEncryptionCertificateAlias());
			String algorithm = "";
			String base64EncryptionCertificate="";
			if (cert != null) {
				base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);
				algorithm = cert.getSigAlgName();
			}
			Node x509CertificateNode = XmlUtil.createTextElement(x509Node,
					FederationConstants.X509CERTIFICATE_NODE, "http://www.w3.org/2000/09/xmldsig#",
					base64EncryptionCertificate, null);
			x509CertificateNode.setPrefix("ds");
			// algorithm=XMLCipher.AES_128;
			Long keySize = new Long(128);

			Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
			encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
			Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE,
					null, encryptionMethodAttributes);

			Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
					"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
			keySizeNode.setPrefix("xenc");

		}

		XmlUtil.createBindingLocationElement(attributeQueryDescriptorNode, "XACMLAuthzService",
				getAuthorizationServiceBinding(), getAuthorizationServiceLocation());

	}

}
