/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.DataStoresConstants;

/**
 * LDAPv3ForAMDSBean object which defines attributes for a
 * "Sun DS With OpenSSO Schema" {@link DataStore}.
 * 
 */
public class LDAPv3ForAMDSBean extends LDAPv3DataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	private String attributeNameOfGroupMemberUrl;
	private String ldapRolesSearchAttribute;
	private String ldapRolesSearchFilter;
	private List<String> ldapRolesObjectClass = new ArrayList<String>();
	private List<String> ldapRolesAttributes = new ArrayList<String>();
	private String ldapFilterRolesSearchAttribute;
	private String ldapFilterRolesSearchFilter;
	private List<String> ldapFilterRolesObjectClass = new ArrayList<String>();
	private List<String> ldapFilterRolesAttributes = new ArrayList<String>();
	private String attributeNameForFilteredRoleMembership;
	private String attributeNameOfRoleMembership;
	private String attributeNameOfFilteredRoleFilter;

	public String getAttributeNameOfGroupMemberUrl() {
		return attributeNameOfGroupMemberUrl;
	}

	public void setAttributeNameOfGroupMemberUrl(String attributeNameOfGroupMemberUrl) {
		this.attributeNameOfGroupMemberUrl = attributeNameOfGroupMemberUrl;
	}

	public String getLdapRolesSearchAttribute() {
		return ldapRolesSearchAttribute;
	}

	public void setLdapRolesSearchAttribute(String ldapRolesSearchAttribute) {
		this.ldapRolesSearchAttribute = ldapRolesSearchAttribute;
	}

	public String getLdapRolesSearchFilter() {
		return ldapRolesSearchFilter;
	}

	public void setLdapRolesSearchFilter(String ldapRolesSearchFilter) {
		this.ldapRolesSearchFilter = ldapRolesSearchFilter;
	}

	public List<String> getLdapRolesObjectClass() {
		return ldapRolesObjectClass;
	}

	public void setLdapRolesObjectClass(List<String> ldapRolesObjectClass) {
		this.ldapRolesObjectClass = ldapRolesObjectClass;
	}

	public List<String> getLdapRolesAttributes() {
		return ldapRolesAttributes;
	}

	public void setLdapRolesAttributes(List<String> ldapRolesAttributes) {
		this.ldapRolesAttributes = ldapRolesAttributes;
	}

	public String getLdapFilterRolesSearchAttribute() {
		return ldapFilterRolesSearchAttribute;
	}

	public void setLdapFilterRolesSearchAttribute(String ldapFilterRolesSearchAttribute) {
		this.ldapFilterRolesSearchAttribute = ldapFilterRolesSearchAttribute;
	}

	public String getLdapFilterRolesSearchFilter() {
		return ldapFilterRolesSearchFilter;
	}

	public void setLdapFilterRolesSearchFilter(String ldapFilterRolesSearchFilter) {
		this.ldapFilterRolesSearchFilter = ldapFilterRolesSearchFilter;
	}

	public List<String> getLdapFilterRolesObjectClass() {
		return ldapFilterRolesObjectClass;
	}

	public void setLdapFilterRolesObjectClass(List<String> ldapFilterRolesObjectClass) {
		this.ldapFilterRolesObjectClass = ldapFilterRolesObjectClass;
	}

	public List<String> getLdapFilterRolesAttributes() {
		return ldapFilterRolesAttributes;
	}

	public void setLdapFilterRolesAttributes(List<String> ldapFilterRolesAttributes) {
		this.ldapFilterRolesAttributes = ldapFilterRolesAttributes;
	}

	public String getAttributeNameForFilteredRoleMembership() {
		return attributeNameForFilteredRoleMembership;
	}

	public void setAttributeNameForFilteredRoleMembership(String attributeNameForFilteredRoleMembership) {
		this.attributeNameForFilteredRoleMembership = attributeNameForFilteredRoleMembership;
	}

	public String getAttributeNameOfRoleMembership() {
		return attributeNameOfRoleMembership;
	}

	public void setAttributeNameOfRoleMembership(String attributeNameOfRoleMembership) {
		this.attributeNameOfRoleMembership = attributeNameOfRoleMembership;
	}

	public String getAttributeNameOfFilteredRoleFilter() {
		return attributeNameOfFilteredRoleFilter;
	}

	public void setAttributeNameOfFilteredRoleFilter(String attributeNameOfFilteredRoleFilter) {
		this.attributeNameOfFilteredRoleFilter = attributeNameOfFilteredRoleFilter;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();

		map.put(DataStoresConstants.ATTRIBUTE_NAME_OF_GROUP_MEMBER_URL, asSet(getAttributeNameOfGroupMemberUrl()));
		map.put(DataStoresConstants.LDAP_ROLES_SEARCH_ATTRIBUTE, asSet(getLdapRolesSearchAttribute()));
		map.put(DataStoresConstants.LDAP_ROLES_SEARCH_FILTER, asSet(getLdapRolesSearchFilter()));
		map.put(DataStoresConstants.LDAP_ROLES_OBJECT_CLASS_CURRENT_VALUES, asSet(getLdapRolesObjectClass()));
		map.put(DataStoresConstants.LDAP_ROLES_ATTRIBUTES_CURRENT_VALUES, asSet(getLdapRolesAttributes()));
		map.put(DataStoresConstants.LDAP_FILTER_ROLES_SEARCH_ATTRIBUTE, asSet(getLdapFilterRolesSearchAttribute()));
		map.put(DataStoresConstants.LDAP_FILTER_ROLES_SEARCH_FILTER, asSet(getLdapFilterRolesSearchFilter()));
		map.put(DataStoresConstants.LDAP_FILTER_ROLES_OBJECT_CLASS_CURRENT_VALUES,
				asSet(getLdapFilterRolesObjectClass()));
		map.put(DataStoresConstants.LDAP_FILTER_ROLES_ATTRIBUTES_CURRENT_VALUES, asSet(getLdapFilterRolesAttributes()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_FOR_FILTERED_ROLE_MEMBERSHIP,
				asSet(getAttributeNameForFilteredRoleMembership()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_OF_ROLE_MEMBERSHIP, asSet(getAttributeNameOfRoleMembership()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_OF_FILTERED_ROLE_FILTER,
				asSet(getAttributeNameOfFilteredRoleFilter()));

		return map;

	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param map
	 *            - map of attributes.
	 * 
	 */
	public static LDAPv3ForAMDSBean fromMap(Map<String, Set<String>> map) {
		LDAPv3ForAMDSBean bean = new LDAPv3ForAMDSBean();
		LDAPv3DataStore.setBaseFields(map, bean);

		bean.setAttributeNameOfGroupMemberUrl(stringFromSet(map
				.get(DataStoresConstants.ATTRIBUTE_NAME_OF_GROUP_MEMBER_URL)));
		bean.setLdapRolesSearchAttribute(stringFromSet(map.get(DataStoresConstants.LDAP_ROLES_SEARCH_ATTRIBUTE)));
		bean.setLdapRolesSearchFilter(stringFromSet(map.get(DataStoresConstants.LDAP_ROLES_SEARCH_FILTER)));
		bean.setLdapRolesObjectClass(listFromSet(map.get(DataStoresConstants.LDAP_ROLES_OBJECT_CLASS_CURRENT_VALUES)));
		bean.setLdapRolesAttributes(listFromSet(map.get(DataStoresConstants.LDAP_ROLES_ATTRIBUTES_CURRENT_VALUES)));
		bean.setLdapFilterRolesSearchAttribute(stringFromSet(map
				.get(DataStoresConstants.LDAP_FILTER_ROLES_SEARCH_ATTRIBUTE)));
		bean.setLdapFilterRolesSearchFilter(stringFromSet(map.get(DataStoresConstants.LDAP_FILTER_ROLES_SEARCH_FILTER)));
		bean.setLdapFilterRolesObjectClass(listFromSet(map
				.get(DataStoresConstants.LDAP_FILTER_ROLES_OBJECT_CLASS_CURRENT_VALUES)));
		bean.setLdapFilterRolesAttributes(listFromSet(map
				.get(DataStoresConstants.LDAP_FILTER_ROLES_ATTRIBUTES_CURRENT_VALUES)));
		bean.setAttributeNameForFilteredRoleMembership(stringFromSet(map
				.get(DataStoresConstants.ATTRIBUTE_NAME_FOR_FILTERED_ROLE_MEMBERSHIP)));
		bean.setAttributeNameOfRoleMembership(stringFromSet(map
				.get(DataStoresConstants.ATTRIBUTE_NAME_OF_ROLE_MEMBERSHIP)));
		bean.setAttributeNameOfFilteredRoleFilter(stringFromSet(map
				.get(DataStoresConstants.ATTRIBUTE_NAME_OF_FILTERED_ROLE_FILTER)));

		return bean;
	}

}
