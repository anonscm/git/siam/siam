/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EEMiscellaneousBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentMiscellaneousController shows the edit "J2EE Agent" (Miscellaneous
 * attributes) form and does request processing of the edit j2ee agent action.
 * 
 */
@Controller("J2EEAgentMiscellaneousController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentMiscellaneousController" })
public class J2EEAgentMiscellaneousController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentMiscellaneousController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "J2EE Agent" - miscellaneous attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return {@link String} the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {
			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EEMiscellaneousBean agent = agentsService.getAttributesForJ2EEMiscellaneousBean(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/j2ee/miscellaneous/edit";

	}

	/**
	 * Save new value in Port Check Setting.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param portCheckSettingKey
	 *            - {@link String} key to be insert
	 * @param portCheckSettingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addPortCheckSetting")
	public void doAddPortCheckSetting(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String portCheckSettingKey, String portCheckSettingValue, ActionRequest request,
			ActionResponse response) {

		if (portCheckSettingKey != null && !portCheckSettingKey.equals("") && portCheckSettingValue != null
				&& !portCheckSettingValue.equals("")) {
			j2eeAgent.getPortCheckSetting().add("[" + portCheckSettingKey + "]=" + portCheckSettingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Port Check Setting.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param portCheckSettingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePortCheckSetting")
	public void doDeletePortCheckSetting(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String[] portCheckSettingDeleteValues, ActionRequest request, ActionResponse response) {

		if (portCheckSettingDeleteValues != null) {
			for (int i = 0; i < portCheckSettingDeleteValues.length; i++) {
				j2eeAgent.getPortCheckSetting().remove(portCheckSettingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Bypass Principal List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param bypassPrincipalListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addBypassPrincipalList")
	public void doAddBypassPrincipalList(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String bypassPrincipalListAddValue, ActionRequest request, ActionResponse response) {

		if (bypassPrincipalListAddValue != null && !bypassPrincipalListAddValue.equals("")) {
			j2eeAgent.getBypassPrincipalList().add(bypassPrincipalListAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Bypass Principal List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param bypassPrincipalListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteBypassPrincipalList")
	public void doDeleteBypassPrincipalList(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String[] bypassPrincipalListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (bypassPrincipalListDeleteValues != null) {
			for (int i = 0; i < bypassPrincipalListDeleteValues.length; i++) {
				j2eeAgent.getBypassPrincipalList().remove(bypassPrincipalListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Legacy User Agent List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param legacyUserAgentListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLegacyUserAgentList")
	public void doAddLegacyUserAgentList(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String legacyUserAgentListAddValue, ActionRequest request, ActionResponse response) {

		if (legacyUserAgentListAddValue != null && !legacyUserAgentListAddValue.equals("")) {
			j2eeAgent.getLegacyUserAgentList().add(legacyUserAgentListAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Legacy User Agent List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param legacyUserAgentListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLegacyUserAgentList")
	public void doDeleteLegacyUserAgentList(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, String[] legacyUserAgentListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (legacyUserAgentListDeleteValues != null) {
			for (int i = 0; i < legacyUserAgentListDeleteValues.length; i++) {
				j2eeAgent.getLegacyUserAgentList().remove(legacyUserAgentListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EEMiscellaneousBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEMiscellaneousBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentGlobal(@ModelAttribute("j2eeAgent") @Valid J2EEMiscellaneousBean j2eeAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveJ2EEMiscellaneous(token, realm, j2eeAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link J2EEMiscellaneousBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentGlobal(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}
}
