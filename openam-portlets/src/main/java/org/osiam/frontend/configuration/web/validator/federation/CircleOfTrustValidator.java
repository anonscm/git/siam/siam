/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.federation;

import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.configuration.domain.datastores.DatabaseBean;
import org.osiam.frontend.configuration.service.FederationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the CircleOfTrust object.
 * <p>
 * Validate if the circle of trust name already exist<br>
 * </p>
 * 
 */
@Component("circleOfTrustValidator")
public class CircleOfTrustValidator implements Validator {

	@Autowired
	private FederationService federationService;

	@Override
	public boolean supports(Class<?> klass) {
		return DatabaseBean.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ObjectForValidate ofv = (ObjectForValidate) target;
		BaseEntity object = (BaseEntity) ofv.getObject();

		if (object.getName() != null && !"".equals(object.getName())
				&& federationService.circleOfTrustNameAlreadyExist(ofv.getToken(), ofv.getRealm(), object.getName())) {
			errors.rejectValue("name", "NameExist");
		}

	}
}
