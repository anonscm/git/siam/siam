/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

import java.util.List;
import java.util.Map;

import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.EntityProviderDescriptor;
import org.osiam.frontend.configuration.domain.federation.SAMLv2AuthContext;
import org.osiam.frontend.configuration.domain.federation.SAMLv2IDPAuthContext;
import org.osiam.frontend.configuration.exceptions.FederationException;

import com.iplanet.sso.SSOToken;

/**
 * Federation service interface. Defines methods for entity providers
 * management.
 */
public interface FederationService {

	/**
	 * @return list of entity providers for given realm
	 * @param realm
	 *            - the realm
	 * @param token
	 *            - the SSO token.
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	List<EntityProvider> getEntityProviderList(SSOToken token, String realm) throws FederationException;

	/**
	 * @return {@link EntityProvider} instance matching given name and type.
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * @param type
	 *            - the {@link EntityProvider} type.
	 * @param name
	 *            - the {@link EntityProvider} name.
	 * 
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	EntityProvider getEntityProvidersForGivenTypeAndGivenName(SSOToken token, String realm, String type, String name)
			throws FederationException;

	/**
	 * @return list of circle of trust for given realm.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm name
	 * @throws FederationException
	 *             - when something wrong happens
	 */
	List<CircleOfTrust> getCircleOfTrustList(SSOToken token, String realm) throws FederationException;

	/**
	 * Adds given {@link CircleOfTrust}.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm name
	 * @param circleOfTrust
	 *            - the given {@link CircleOfTrust} to be added
	 * @throws FederationException
	 *             - when something wrong happens
	 */
	void addCircleOfTrust(SSOToken token, String realm, CircleOfTrust circleOfTrust) throws FederationException;

	/**
	 * Updates given {@link CircleOfTrust}.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm name
	 * @param newCircleOfTrust
	 *            - the new given {@link CircleOfTrust} with the new data to be
	 *            updated.
	 * @throws FederationException
	 *             - when something wrong happens
	 */
	void updateCircleOfTrust(SSOToken token, String realm, CircleOfTrust newCircleOfTrust) throws FederationException;

	/**
	 * Adds new {@link EntityProvider}.
	 * 
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * @param entityProvider
	 *            - the {@link EntityProvider} to be added.
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	void addEntityProvider(SSOToken token, String realm, EntityProvider entityProvider) throws FederationException;

	/**
	 * @return {@link CircleOfTrust} matching given name.
	 * 
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * @param circleOfTrustName
	 *            - the circle of trust name.
	 * 
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	CircleOfTrust getCircleOfTrust(SSOToken token, String realm, String circleOfTrustName) throws FederationException;

	/**
	 * 
	 * @return list of entity provider protocols.
	 */
	List<String> getEntityProviderProtocolList();

	/**
	 * Removes given {@link EntityProvider}.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm name
	 * @param entityProviderName
	 *            - the given {@link EntityProvider} name to be removed
	 * @throws FederationException
	 *             - when something wrong happens
	 */
	void deleteEntityProvider(SSOToken token, String realm, String entityProviderName) throws FederationException;

	/**
	 * Removes given {@link CircleOfTrust}.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm name
	 * @param circleOfTrustName
	 *            - the given {@link CircleOfTrust} name to be removed
	 * @throws FederationException
	 *             - when something wrong happens
	 */
	void deleteCircleOfTrust(SSOToken token, String realm, String circleOfTrustName) throws FederationException;

	/**
	 * @return list of available entities.
	 * 
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * 
	 * @throws FederationException
	 *             -when something wrong happens.
	 */
	List<String> getAvailablelEntities(SSOToken token, String realm) throws FederationException;

	/**
	 * 
	 * @return list of authentication contexts.
	 */
	Map<String, String> getAuthenticationContextList();

	/**
	 * 
	 * @return list of key types.
	 */
	List<String> getKeyType();

	/**
	 * 
	 * @param supportedAuthContext
	 *            - the supported auth context.
	 * @return list of {@link SAMLv2AuthContext}.
	 */
	List<SAMLv2AuthContext> getAuthContext(List<SAMLv2AuthContext> supportedAuthContext);

	/**
	 * @param supportedAuthContext
	 *            - the supported auth context.
	 * @return list of {@link SAMLv2IDPAuthContext}.
	 */
	List<SAMLv2IDPAuthContext> getIDPAuthContext(List<SAMLv2IDPAuthContext> supportedAuthContext);

	/**
	 * 
	 * @return single logout service data.
	 */
	Map<String, String> getSingleLogoutService();

	/**
	 * Saves attributes for given {@link EntityProvider}.
	 * 
	 * @param token
	 *            - the SSO token.
	 * @param realm
	 *            - the realm.
	 * @param bean
	 *            - the {@link EntityProvider} bean.
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	void saveAttributesEntityProvider(SSOToken token, String realm, EntityProvider bean) throws FederationException;

	/**
	 * @param token
	 *            - the token.
	 * @param realm
	 *            - the realm.
	 * @return entity provider list names.
	 * @throws FederationException
	 *             - when something wrong happens.
	 */
	List<String> getEntityProviderNamesList(SSOToken token, String realm) throws FederationException;

	/**
	 * @param token
	 *            - the token.
	 * @param realm
	 *            - the realm.
	 * @param metadataFile
	 *            - metadata file content.
	 * @param extendedDataFile
	 *            - extended metadata file content.
	 * @throws FederationException
	 *             - when something wrong happens.
	 * 
	 * @return true if import has succeded and false otherwise.
	 */
	String importEntityProvider(SSOToken token, String realm, String metadataFile, String extendedDataFile)
			throws FederationException;

	/**
	 * Update EntityProviderDescriptor.
	 * 
	 * @param token
	 *            - SSOToken
	 * @param realm
	 *            - realm
	 * @param type
	 *            - type
	 * @param name
	 *            - name
	 * @param newEntityProviderDescriptor
	 *            - EntityProviderDescriptor
	 */
	void updateEntityProviderDescriptor(SSOToken token, String realm, String type, String name,
			EntityProviderDescriptor newEntityProviderDescriptor);

	/**
	 * Verify if circle of trust name already exist.
	 * 
	 * @param token
	 *            - SSOToken
	 * @param realm
	 *            - realm
	 * @param name
	 *            - circle of trust name
	 * @return true if circle of trust name already exist
	 */
	boolean circleOfTrustNameAlreadyExist(SSOToken token, String realm, String name);

	/**
	 * Verify if entity provider name already exist.
	 * 
	 * @param token
	 *            - SSOToken
	 * @param realm
	 *            - realm
	 * @param name
	 *            - entity provider name
	 * @param protocol
	 *            - protocol
	 * @return true if entity provider name already exist
	 */
	boolean entityProviderNameAlreadyExist(SSOToken token, String realm, String name, String protocol);

	/**
	 * Validate certification.
	 * 
	 * @param token
	 *            - SSOToken
	 * @param realm
	 *            - realm
	 * @param certification
	 *            - certification
	 * @return true if certification is valid
	 */
	boolean validateCertification(SSOToken token, String realm, String certification);

	/**
	 * @return SPServiceAttributes
	 */
	List<String> getSPServiceAttributes();

	/**
	* Get metadata for given entity provider.
	 * 
	 * @param token - SSOToken
	 * @param realm - realm
	 * @param type - entity provider type
	 * @param name - entity provider name
	 * @return metadata
	 * @throws FederationException
	 */
	public String getEntityProvidersForExportMetadata(SSOToken token, String realm, String type, String name)
			throws FederationException;

	/**
	 * Get extended data for given entity provider.
	 * 
	 * @param token - SSOToken
	 * @param realm - realm
	 * @param type - entity provider type
	 * @param name - entity provider name
	 * @return extended data
	 * @throws FederationException
	 */
	public String getEntityProvidersForExportExtendedData(SSOToken token, String realm, String type, String name)
			throws FederationException;

}
