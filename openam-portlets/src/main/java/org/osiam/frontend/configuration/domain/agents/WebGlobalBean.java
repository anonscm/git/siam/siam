/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addPrefix;
import static org.osiam.frontend.util.AttributeUtils.addPrefixIfEmpty;
import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a web global bean.
 * 
 */
public class WebGlobalBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1)
	private String name;

	private String group;
	@NotNull
	@Size(min = 1)
	private String password;
	@NotNull
	@Size(min = 1)
	private String passwordConfirm;
	@NotNull
	@Size(min = 1)
	private String status;
	private String locationOfAgentConfigurationRepository;
	private boolean agentConfigurationChangeNotification;
	private boolean enableNotifications;
	private String agentNotificationURL;
	private String agentDeploymentURIPrefix;
	@NotNull
	@Min(0)
	private Integer configurationReloadInterval;
	@NotNull
	@Min(0)
	private Integer configurationCleanupInterval;
	private List<String> agentRootURLforCDSSO = new ArrayList<String>();

	private boolean ssoOnlyMode;
	private String resourcesAccessDeniedURL;
	private String agentDebugLevel;
	private boolean agentDebugFileRotation;
	@NotNull
	@Min(0)
	private Integer agentDebugFileSize;

	private String auditAccessTypes;
	private String auditLogLocation;
	private String remoteLogFilename;
	@NotNull
	@Min(0)
	private Integer remoteAuditLogInterval;
	private boolean rotateLocalAuditLog;
	@NotNull
	@Min(0)
	private Integer localAuditLogRotationSize;

	private boolean fqdnCheck;
	private String fqdnDefault;
	private List<String> fqdnVirtualHostMap = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocationOfAgentConfigurationRepository() {
		return locationOfAgentConfigurationRepository;
	}

	public void setLocationOfAgentConfigurationRepository(String locationOfAgentConfigurationRepository) {
		this.locationOfAgentConfigurationRepository = locationOfAgentConfigurationRepository;
	}

	public boolean getAgentConfigurationChangeNotification() {
		return agentConfigurationChangeNotification;
	}

	public void setAgentConfigurationChangeNotification(boolean agentConfigurationChangeNotification) {
		this.agentConfigurationChangeNotification = agentConfigurationChangeNotification;
	}

	public boolean getEnableNotifications() {
		return enableNotifications;
	}

	public void setEnableNotifications(boolean enableNotifications) {
		this.enableNotifications = enableNotifications;
	}

	public String getAgentNotificationURL() {
		return agentNotificationURL;
	}

	public void setAgentNotificationURL(String agentNotificationURL) {
		this.agentNotificationURL = agentNotificationURL;
	}

	public String getAgentDeploymentURIPrefix() {
		return agentDeploymentURIPrefix;
	}

	public void setAgentDeploymentURIPrefix(String agentDeploymentURIPrefix) {
		this.agentDeploymentURIPrefix = agentDeploymentURIPrefix;
	}

	public Integer getConfigurationReloadInterval() {
		return configurationReloadInterval;
	}

	public void setConfigurationReloadInterval(Integer configurationReloadInterval) {
		this.configurationReloadInterval = configurationReloadInterval;
	}

	public Integer getConfigurationCleanupInterval() {
		return configurationCleanupInterval;
	}

	public void setConfigurationCleanupInterval(Integer configurationCleanupInterval) {
		this.configurationCleanupInterval = configurationCleanupInterval;
	}

	public List<String> getAgentRootURLforCDSSO() {
		return agentRootURLforCDSSO;
	}

	public void setAgentRootURLforCDSSO(List<String> agentRootURLforCDSSO) {
		this.agentRootURLforCDSSO = agentRootURLforCDSSO;
	}

	public boolean getSsoOnlyMode() {
		return ssoOnlyMode;
	}

	public void setSsoOnlyMode(boolean ssoOnlyMode) {
		this.ssoOnlyMode = ssoOnlyMode;
	}

	public String getResourcesAccessDeniedURL() {
		return resourcesAccessDeniedURL;
	}

	public void setResourcesAccessDeniedURL(String resourcesAccessDeniedURL) {
		this.resourcesAccessDeniedURL = resourcesAccessDeniedURL;
	}

	public String getAgentDebugLevel() {
		return agentDebugLevel;
	}

	public void setAgentDebugLevel(String agentDebugLevel) {
		this.agentDebugLevel = agentDebugLevel;
	}

	public boolean getAgentDebugFileRotation() {
		return agentDebugFileRotation;
	}

	public void setAgentDebugFileRotation(boolean agentDebugFileRotation) {
		this.agentDebugFileRotation = agentDebugFileRotation;
	}

	public Integer getAgentDebugFileSize() {
		return agentDebugFileSize;
	}

	public void setAgentDebugFileSize(Integer agentDebugFileSize) {
		this.agentDebugFileSize = agentDebugFileSize;
	}

	public String getAuditAccessTypes() {
		return auditAccessTypes;
	}

	public void setAuditAccessTypes(String auditAccessTypes) {
		this.auditAccessTypes = auditAccessTypes;
	}

	public String getAuditLogLocation() {
		return auditLogLocation;
	}

	public void setAuditLogLocation(String auditLogLocation) {
		this.auditLogLocation = auditLogLocation;
	}

	public String getRemoteLogFilename() {
		return remoteLogFilename;
	}

	public void setRemoteLogFilename(String remoteLogFilename) {
		this.remoteLogFilename = remoteLogFilename;
	}

	public Integer getRemoteAuditLogInterval() {
		return remoteAuditLogInterval;
	}

	public void setRemoteAuditLogInterval(Integer remoteAuditLogInterval) {
		this.remoteAuditLogInterval = remoteAuditLogInterval;
	}

	public boolean getRotateLocalAuditLog() {
		return rotateLocalAuditLog;
	}

	public void setRotateLocalAuditLog(boolean rotateLocalAuditLog) {
		this.rotateLocalAuditLog = rotateLocalAuditLog;
	}

	public Integer getLocalAuditLogRotationSize() {
		return localAuditLogRotationSize;
	}

	public void setLocalAuditLogRotationSize(Integer localAuditLogRotationSize) {
		this.localAuditLogRotationSize = localAuditLogRotationSize;
	}

	public boolean getFqdnCheck() {
		return fqdnCheck;
	}

	public void setFqdnCheck(boolean fqdnCheck) {
		this.fqdnCheck = fqdnCheck;
	}

	public String getFqdnDefault() {
		return fqdnDefault;
	}

	public void setFqdnDefault(String fqdnDefault) {
		this.fqdnDefault = fqdnDefault;
	}

	public List<String> getFqdnVirtualHostMap() {
		return fqdnVirtualHostMap;
	}

	public void setFqdnVirtualHostMap(List<String> fqdnVirtualHostMap) {
		this.fqdnVirtualHostMap = fqdnVirtualHostMap;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebGlobalBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebGlobalBean bean = new WebGlobalBean();
		bean.setName(name);
		bean.setGroup(stringFromSet(attributes.get(AgentAttributesConstants.GROUP)));
		bean.setPassword(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setPasswordConfirm(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setStatus(stringFromSet(attributes.get(AgentAttributesConstants.STATUS)));
		bean.setLocationOfAgentConfigurationRepository(stringFromSet(attributes
				.get(AgentAttributesConstants.LOCATION_OF_AGENT_CONFIGURATION_REPOSITORY)));
		bean.setAgentConfigurationChangeNotification(booleanFromSet(attributes
				.get(AgentAttributesConstants.AGENT_CONFIGURATION_CHANGE_NOTIFICATION)));
		bean.setEnableNotifications(booleanFromSet(attributes.get(AgentAttributesConstants.ENABLE_NOTIFICATIONS)));
		bean.setAgentNotificationURL(stringFromSet(attributes.get(AgentAttributesConstants.AGENT_NOTIFICATION_URL)));
		bean.setAgentDeploymentURIPrefix(stringFromSet(attributes
				.get(AgentAttributesConstants.AGENT_DEPLOYMENT_URI_PREFIX)));
		bean.setConfigurationReloadInterval(integerFromSet(attributes
				.get(AgentAttributesConstants.CONFIGURATION_RELOAD_INTERVAL_WEB)));
		bean.setConfigurationCleanupInterval(integerFromSet(attributes
				.get(AgentAttributesConstants.CONFIGURATION_CLEANUP_INTERVAL)));
		bean.setAgentRootURLforCDSSO(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES)),
				"agentRootURL="));
		bean.setSsoOnlyMode(booleanFromSet(attributes.get(AgentAttributesConstants.SSO_ONLY_MODE)));
		bean.setResourcesAccessDeniedURL(stringFromSet(attributes
				.get(AgentAttributesConstants.RESOURCES_ACCESS_DENIED_URL)));
		bean.setAgentDebugLevel(stringFromSet(attributes.get(AgentAttributesConstants.AGENT_DEBUG_LEVEL)));
		bean.setAgentDebugFileRotation(booleanFromSet(attributes
				.get(AgentAttributesConstants.AGENT_DEBUG_FILE_ROTATION)));
		bean.setAgentDebugFileSize(integerFromSet(attributes.get(AgentAttributesConstants.AGENT_DEBUG_FILE_SIZE)));
		bean.setAuditAccessTypes(stringFromSet(attributes.get(AgentAttributesConstants.AUDIT_ACCESS_TYPES)));
		bean.setAuditLogLocation(stringFromSet(attributes.get(AgentAttributesConstants.AUDIT_LOG_LOCATION)));
		bean.setRemoteLogFilename(stringFromSet(attributes.get(AgentAttributesConstants.REMOTE_LOG_FILENAME)));
		bean.setRemoteAuditLogInterval(integerFromSet(attributes
				.get(AgentAttributesConstants.REMOTE_AUDIT_LOG_INTERVAL)));
		bean.setRotateLocalAuditLog(booleanFromSet(attributes.get(AgentAttributesConstants.ROTATE_LOCAL_AUDIT_LOG)));
		bean.setLocalAuditLogRotationSize(integerFromSet(attributes
				.get(AgentAttributesConstants.LOCAL_AUDIT_LOG_ROTATION_SIZE)));
		bean.setFqdnCheck(booleanFromSet(attributes.get(AgentAttributesConstants.FQDN_CHECK)));
		bean.setFqdnDefault(stringFromSet(attributes.get(AgentAttributesConstants.FQDN_DEFAULT)));
		bean.setFqdnVirtualHostMap(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.FQDN_VIRTUAL_HOST_MAP_CURRENT_VALUES)), ""));
		return bean;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 * 
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		if ((group != null) && (!"".equals(group))) {
			map.put(AgentAttributesConstants.GROUP, asSet(getGroup()));
		}

		map.put(AgentAttributesConstants.PASSWORD, asSet(getPassword()));
		map.put(AgentAttributesConstants.STATUS, asSet(getStatus()));
		map.put(AgentAttributesConstants.LOCATION_OF_AGENT_CONFIGURATION_REPOSITORY,
				asSet(getLocationOfAgentConfigurationRepository()));
		map.put(AgentAttributesConstants.AGENT_CONFIGURATION_CHANGE_NOTIFICATION,
				asSet(getAgentConfigurationChangeNotification()));
		map.put(AgentAttributesConstants.ENABLE_NOTIFICATIONS, asSet(getEnableNotifications()));
		map.put(AgentAttributesConstants.AGENT_NOTIFICATION_URL, asSet(getAgentNotificationURL()));
		map.put(AgentAttributesConstants.AGENT_DEPLOYMENT_URI_PREFIX, asSet(getAgentDeploymentURIPrefix()));
		map.put(AgentAttributesConstants.CONFIGURATION_RELOAD_INTERVAL_WEB, asSet(getConfigurationReloadInterval()));
		map.put(AgentAttributesConstants.CONFIGURATION_CLEANUP_INTERVAL, asSet(getConfigurationCleanupInterval()));
		map.put(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES,
				asSet(addPrefix(getAgentRootURLforCDSSO(), "agentRootURL=")));
		map.put(AgentAttributesConstants.SSO_ONLY_MODE, asSet(getSsoOnlyMode()));
		map.put(AgentAttributesConstants.RESOURCES_ACCESS_DENIED_URL, asSet(getResourcesAccessDeniedURL()));
		map.put(AgentAttributesConstants.AGENT_DEBUG_LEVEL, asSet(getAgentDebugLevel()));
		map.put(AgentAttributesConstants.AGENT_DEBUG_FILE_ROTATION, asSet(getAgentDebugFileRotation()));
		map.put(AgentAttributesConstants.AGENT_DEBUG_FILE_SIZE, asSet(getAgentDebugFileSize()));
		map.put(AgentAttributesConstants.AUDIT_ACCESS_TYPES, asSet(getAuditAccessTypes()));
		map.put(AgentAttributesConstants.AUDIT_LOG_LOCATION, asSet(getAuditLogLocation()));
		map.put(AgentAttributesConstants.REMOTE_LOG_FILENAME, asSet(getRemoteLogFilename()));
		map.put(AgentAttributesConstants.REMOTE_AUDIT_LOG_INTERVAL, asSet(getRemoteAuditLogInterval()));
		map.put(AgentAttributesConstants.FQDN_CHECK, asSet(getFqdnCheck()));
		map.put(AgentAttributesConstants.FQDN_DEFAULT, asSet(getFqdnDefault()));
		map.put(AgentAttributesConstants.FQDN_VIRTUAL_HOST_MAP_CURRENT_VALUES,
				asSet(addPrefixIfEmpty(getFqdnVirtualHostMap(), "[]=")));
		map.put(AgentAttributesConstants.ROTATE_LOCAL_AUDIT_LOG, asSet(getRotateLocalAuditLog()));
		map.put(AgentAttributesConstants.LOCAL_AUDIT_LOG_ROTATION_SIZE, asSet((getLocalAuditLogRotationSize())));

		return map;
	}

}
