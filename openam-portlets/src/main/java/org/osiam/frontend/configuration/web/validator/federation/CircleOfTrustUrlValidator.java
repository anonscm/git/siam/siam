/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.federation;

import java.util.regex.Pattern;

import org.osiam.frontend.configuration.domain.datastores.DatabaseBean;
import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the CircleOfTrust object.
 * <p>
 * Validate if URL format
 * </p>
 * 
 */
@Component("circleOfTrustUrlValidator")
public class CircleOfTrustUrlValidator implements Validator {

	private static final Pattern PATTERN_URL = Pattern.compile("^http://.*");

	@Override
	public boolean supports(Class<?> klass) {
		return DatabaseBean.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CircleOfTrust object = (CircleOfTrust) target;

		if (object.getIdffReaderServiceURL() != null && !"".equals(object.getIdffReaderServiceURL())) {
			if (!PATTERN_URL.matcher(object.getIdffReaderServiceURL()).matches()) {
				errors.rejectValue("idffReaderServiceURL", "InvalidUrl");
			}
		}

		if (object.getIdffWriterServiceURL() != null && !"".equals(object.getIdffWriterServiceURL())) {
			if (!PATTERN_URL.matcher(object.getIdffWriterServiceURL()).matches()) {
				errors.rejectValue("idffWriterServiceURL", "InvalidUrl");
			}
		}

		if (object.getSaml2ReaderServiceURL() != null && !"".equals(object.getSaml2ReaderServiceURL())) {
			if (!PATTERN_URL.matcher(object.getSaml2ReaderServiceURL()).matches()) {
				errors.rejectValue("saml2ReaderServiceURL", "InvalidUrl");
			}
		}

		if (object.getSaml2WriterServiceURL() != null && !"".equals(object.getSaml2WriterServiceURL())) {
			if (!PATTERN_URL.matcher(object.getSaml2WriterServiceURL()).matches()) {
				errors.rejectValue("saml2WriterServiceURL", "InvalidUrl");
			}
		}
	}
}
