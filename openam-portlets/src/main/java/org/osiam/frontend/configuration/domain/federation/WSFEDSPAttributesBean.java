/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * Encapsulates data for a WSFED SP Attributes Bean.
 * 
 */

public class WSFEDSPAttributesBean extends EntityProviderDescriptor {
	private static final Logger LOGGER = Logger.getLogger(WSFEDSPAttributesBean.class);
	private boolean assertionSigned;

	private String accountMapper;

	private String attributeMapper;
	private List<String> attributeMapCurrentValues = new ArrayList<String>();

	private String assertionEffectiveTime;

	private String assertionTimeSkew;

	private String saml2AuthModuleName;

	private String defaultRelayState;

	private String homeRealmDiscoveryService;

	private String accountRealmSelection;
	private String cookieName;
	private String userAgentKey;

	private String authncontextMapper;
	private String authncontextClassRefMapping;
	private String authncontextComparisonType;
	private String assertionCacheEnabled;

	public String getAssertionCacheEnabled() {
		return assertionCacheEnabled;
	}

	public void setAssertionCacheEnabled(String assertionCacheEnabled) {
		this.assertionCacheEnabled = assertionCacheEnabled;
	}

	public String getAuthncontextComparisonType() {
		return authncontextComparisonType;
	}

	public void setAuthncontextComparisonType(String authncontextComparisonType) {
		this.authncontextComparisonType = authncontextComparisonType;
	}

	public String getAuthncontextClassRefMapping() {
		return authncontextClassRefMapping;
	}

	public void setAuthncontextClassRefMapping(String authncontextClassRefMapping) {
		this.authncontextClassRefMapping = authncontextClassRefMapping;
	}

	public String getAuthncontextMapper() {
		return authncontextMapper;
	}

	public void setAuthncontextMapper(String authncontextMapper) {
		this.authncontextMapper = authncontextMapper;
	}

	public String getSaml2AuthModuleName() {
		return saml2AuthModuleName;
	}

	public void setSaml2AuthModuleName(String saml2AuthModuleName) {
		this.saml2AuthModuleName = saml2AuthModuleName;
	}

	public boolean getAssertionSigned() {
		return assertionSigned;
	}

	public void setAssertionSigned(boolean assertionSigned) {
		this.assertionSigned = assertionSigned;
	}

	public String getAccountMapper() {
		return accountMapper;
	}

	public void setAccountMapper(String accountMapper) {
		this.accountMapper = accountMapper;
	}

	public String getAttributeMapper() {
		return attributeMapper;
	}

	public void setAttributeMapper(String attributeMapper) {
		this.attributeMapper = attributeMapper;
	}

	public List<String> getAttributeMapCurrentValues() {
		return attributeMapCurrentValues;
	}

	public void setAttributeMapCurrentValues(List<String> attributeMapCurrentValues) {
		this.attributeMapCurrentValues = attributeMapCurrentValues;
	}

	public String getAssertionEffectiveTime() {
		return assertionEffectiveTime;
	}

	public void setAssertionEffectiveTime(String assertionEffectiveTime) {
		this.assertionEffectiveTime = assertionEffectiveTime;
	}

	public String getAssertionTimeSkew() {
		return assertionTimeSkew;
	}

	public void setAssertionTimeSkew(String assertionTimeSkew) {
		this.assertionTimeSkew = assertionTimeSkew;
	}

	public String getDefaultRelayState() {
		return defaultRelayState;
	}

	public void setDefaultRelayState(String defaultRelayState) {
		this.defaultRelayState = defaultRelayState;
	}

	public String getHomeRealmDiscoveryService() {
		return homeRealmDiscoveryService;
	}

	public void setHomeRealmDiscoveryService(String homeRealmDiscoveryService) {
		this.homeRealmDiscoveryService = homeRealmDiscoveryService;
	}

	public String getAccountRealmSelection() {
		return accountRealmSelection;
	}

	public void setAccountRealmSelection(String accountRealmSelection) {
		this.accountRealmSelection = accountRealmSelection;
	}

	public String getCookieName() {
		return cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public String getUserAgentKey() {
		return userAgentKey;
	}

	public void setUserAgentKey(String userAgentKey) {
		this.userAgentKey = userAgentKey;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static WSFEDSPAttributesBean fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		WSFEDSPAttributesBean bean = new WSFEDSPAttributesBean();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();

		Set<Node> spConfigNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.SPSSO_CONFIG_NODE);
		if (spConfigNodeSet != null) {
			Node spConfigNode = spConfigNodeSet.iterator().next();

			bean.setMetaAlias(XmlUtil.getNodeAttributeValue(spConfigNode, "metaAlias"));

			NodeList spConfigChildNodeList = spConfigNode.getChildNodes();

			for (int i = 0; i < spConfigChildNodeList.getLength(); i++) {

				Node node = spConfigChildNodeList.item(i);

				if (node.getNodeType() == Node.TEXT_NODE) {
					continue;
				}
				if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
					continue;
				}

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
				}

				String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(String.format("Attribute name: %s", attributeName));
				}

				if (attributeName.equals("wantAssertionSigned")) {
					bean.setAssertionSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
				} else if (attributeName.equals("spAccountMapper")) {
					bean.setAccountMapper(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("spAttributeMapper")) {
					bean.setAttributeMapper(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("attributeMap")) {
					bean.setAttributeMapCurrentValues(XmlUtil.getAttributeValue(node).getValueList());
				} else if (attributeName.equals("assertionEffectiveTime")) {
					bean.setAssertionEffectiveTime(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("defaultRelayState")) {
					bean.setDefaultRelayState(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("HomeRealmDiscoveryService")) {
					bean.setHomeRealmDiscoveryService(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("AccountRealmSelection")) {
					bean.setAccountRealmSelection(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("AccountRealmCookieName")) {
					bean.setCookieName(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("assertionTimeSkew")) {
					bean.setAssertionTimeSkew(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("saml2AuthModuleName")) {
					bean.setSaml2AuthModuleName(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("spAuthncontextMapper")) {
					bean.setAuthncontextMapper(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("spAuthncontextClassrefMapping")) {
					bean.setAuthncontextClassRefMapping(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("spAuthncontextComparisonType")) {
					bean.setAuthncontextComparisonType(XmlUtil.getValueFromAttributeValue(node));
				} else if (attributeName.equals("assertionCacheEnabled")) {
					bean.setAssertionCacheEnabled(XmlUtil.getValueFromAttributeValue(node));
				}

			}

		}

		return bean;
	}

	/**
	 * WSFEDSPAttributesBean constructor.
	 */
	public WSFEDSPAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.WSFED_SERVICE_PROVIDER);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) {
	
		Document configDocument = map.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> nodeAttributes = new HashMap<String, String>();
		nodeAttributes.put("metaAlias", getMetaAliasWithSlash());
		Node spssoConfigNode = XmlUtil.createElement(configDocument.getFirstChild(), "SPSSOConfig", nodeAttributes);

		XmlUtil.createAttributeValue(spssoConfigNode, "AccountRealmSelection", accountRealmSelection);
		XmlUtil.createAttributeValue(spssoConfigNode, "AccountRealmCookieName", cookieName);
		XmlUtil.createAttributeValue(spssoConfigNode, "HomeRealmDiscoveryService", homeRealmDiscoveryService);
		XmlUtil.createAttributeValue(spssoConfigNode, "signingCertAlias", getSigningCertificateAlias());
		XmlUtil.createAttributeValue(spssoConfigNode, "assertionEffectiveTime", assertionEffectiveTime);
		XmlUtil.createAttributeValue(spssoConfigNode, "spAccountMapper", accountMapper);
		XmlUtil.createAttributeValue(spssoConfigNode, "spAttributeMapper", attributeMapper);
		XmlUtil.createAttributeValue(spssoConfigNode, "spAuthncontextMapper", authncontextMapper);
		XmlUtil.createAttributeValue(spssoConfigNode, "spAuthncontextClassrefMapping", authncontextClassRefMapping);
		XmlUtil.createAttributeValue(spssoConfigNode, "spAuthncontextComparisonType", authncontextComparisonType);
		XmlUtil.createAttributeValue(spssoConfigNode, "attributeMap", attributeMapCurrentValues);
		XmlUtil.createAttributeValue(spssoConfigNode, "saml2AuthModuleName", saml2AuthModuleName);
		XmlUtil.createAttributeValue(spssoConfigNode, "defaultRelayState", defaultRelayState);
		XmlUtil.createAttributeValue(spssoConfigNode, "assertionTimeSkew", assertionTimeSkew);
		XmlUtil.createAttributeValue(spssoConfigNode, "assertionCacheEnabled", assertionCacheEnabled);
		XmlUtil.createAttributeValue(spssoConfigNode, "wantAssertionSigned", assertionSigned);

	}
}
