/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.agents;

import java.util.Iterator;
import java.util.regex.Pattern;

import org.osiam.frontend.configuration.domain.agents.J2EEGlobalBean;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the WebGlobalBean object.
 * <p>
 * Validate if the passwords entered do not match.<br>
 * Validate if the values form AgentRootURLforCDSSO list are valid.
 * </p>
 * 
 */
@Component("j2EEGlobalBeanValidator")
public class J2EEGlobalBeanValidator implements Validator {

	private static final Pattern PATTERN_URL = Pattern
			.compile("^http://[A-Za-z0-9.]+[- /.][A-Za-z]+[- /:][0-9][0-9][0-9][0-9]/");

	private static final Pattern PATTERN_FILTER_MODE = Pattern
			.compile("^\\[.+\\]\\=(ALL|NONE|J2EE_POLICY|URL_POLICY|SSO_ONLY)");

	private static final Pattern PATTERN_FILTER_MODE_ALL = Pattern.compile("(ALL)");

	@Override
	public boolean supports(Class<?> klass) {
		return J2EEGlobalBean.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		J2EEGlobalBean object = (J2EEGlobalBean) target;

		if (object.getPassword() != null && !object.getPassword().equals(object.getPasswordConfirm())) {
			errors.rejectValue("passwordConfirm", "PassNotMath");
		}

		for (Iterator<String> iterator = object.getAgentRootUrlForCdsso().iterator(); iterator.hasNext();) {
			String value = iterator.next();
			if (!PATTERN_URL.matcher(value).matches()) {
				errors.rejectValue("agentRootUrlForCdsso", "InvalidUrl");
			}
		}

		for (Iterator<String> iterator = object.getAgentFilterMode().iterator(); iterator.hasNext();) {
			String value = iterator.next();
			if (!PATTERN_FILTER_MODE.matcher(value).matches() && !PATTERN_FILTER_MODE_ALL.matcher(value).matches()) {
				errors.rejectValue("agentFilterMode", "InvalidValue");
			}
		}

	}
}
