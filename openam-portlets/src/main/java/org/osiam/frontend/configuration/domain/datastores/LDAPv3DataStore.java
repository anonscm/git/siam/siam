/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.DataStoresConstants;

/**
 * Encapsulates data for a LDAP V3 data store.
 * 
 */
public class LDAPv3DataStore extends BaseDataStore {

	private boolean loadSchemaWhenFinished;
	@Size(min = 1)
	private List<String> ldapServer = new ArrayList<String>();
	private String ldapBindDn;
	private String ldapBindPassword;
	private String ldapBindPasswordConfirm;
	@NotNull
	@Size(min = 1)
	private String ldapOrganizationDn;
	private boolean ldapSslTls;
	@NotNull
	private Integer ldapConnectionPoolMinimumSize;
	@NotNull
	private Integer ldapConnectionPoolMaximumSize;
	@NotNull
	private Integer maximumResultsReturnedFromSearch;
	@NotNull
	private Integer searchTimeout;
	private boolean ldapFollowsReferral;
	@NotNull
	@Size(min = 1)
	private String ldapv3RepositoryPlugInClassName;
	private List<String> attributeNameMapping = new ArrayList<String>();
	private List<String> ldapv3PlugInSupportedTypesAndOperations = new ArrayList<String>();
	private String ldapv3PlugInSearchScope;
	private String ldapUsersSearchAttribute;
	private String ldapUsersSearchFilter;
	private List<String> ldapUserObjectClass = new ArrayList<String>();
	private List<String> ldapUserAttributes = new ArrayList<String>();
	private List<String> createUserAttributeMapping = new ArrayList<String>();
	private String attributeNameOfUserStatus;

	private String ldapGroupsSearchAttribute;
	private String ldapGroupsSearchFilter;
	private String ldapGroupsContainerNamingAttribute;
	private String ldapGroupsContainerValue;
	private List<String> ldapGroupsObjectClass = new ArrayList<String>();
	private List<String> ldapGroupsAttributes = new ArrayList<String>();
	private String attributeNameForGroupMembership;
	private String attributeNameOfUnqiueMember;

	private String ldapPeopleContainerNamingAttribute;
	private String ldapPeopleContainerValue;
	// TODO: need value of identityTypesThatCanBeAuthenticated (other than
	// "User")
	private String identityTypesThatCanBeAuthenticated;
	private String authenticationNamingAttribute;
	private String persistentSearchBaseDn;
	private String persistentSearchFilter;
	private String persistentSearchScope;
	@NotNull
	private Integer persistentSearchMaximumIdleTimeBeforeRestart;
	@NotNull
	private Integer maximumNumberOfRetriesAfterErrorCodes;
	@NotNull
	private Integer theDelayTimeBetweenRetries;

	private List<String> ldapExceptionErrorCodesToRetryOn = new ArrayList<String>();
	private boolean caching;
	@NotNull
	private Integer maximumAgeOfCachedItems;
	@NotNull
	private Integer maximumSizeOfTheCache;

	public boolean getLoadSchemaWhenFinished() {
		return loadSchemaWhenFinished;
	}

	public void setLoadSchemaWhenFinished(boolean loadSchemaWhenFinished) {
		this.loadSchemaWhenFinished = loadSchemaWhenFinished;
	}

	public List<String> getLdapServer() {
		return ldapServer;
	}

	public void setLdapServer(List<String> ldapServer) {
		this.ldapServer = ldapServer;
	}

	public String getLdapBindDn() {
		return ldapBindDn;
	}

	public void setLdapBindDn(String ldapBindDn) {
		this.ldapBindDn = ldapBindDn;
	}

	public String getLdapBindPassword() {
		return ldapBindPassword;
	}

	public void setLdapBindPassword(String ldapBindPassword) {
		this.ldapBindPassword = ldapBindPassword;
	}

	public String getLdapBindPasswordConfirm() {
		return ldapBindPasswordConfirm;
	}

	public void setLdapBindPasswordConfirm(String ldapBindPasswordConfirm) {
		this.ldapBindPasswordConfirm = ldapBindPasswordConfirm;
	}

	public String getLdapOrganizationDn() {
		return ldapOrganizationDn;
	}

	public void setLdapOrganizationDn(String ldapOrganizationDn) {
		this.ldapOrganizationDn = ldapOrganizationDn;
	}

	public boolean getLdapSslTls() {
		return ldapSslTls;
	}

	public void setLdapSslTls(boolean ldapSslTls) {
		this.ldapSslTls = ldapSslTls;
	}

	public Integer getLdapConnectionPoolMinimumSize() {
		return ldapConnectionPoolMinimumSize;
	}

	public void setLdapConnectionPoolMinimumSize(Integer ldapConnectionPoolMinimumSize) {
		this.ldapConnectionPoolMinimumSize = ldapConnectionPoolMinimumSize;
	}

	public Integer getLdapConnectionPoolMaximumSize() {
		return ldapConnectionPoolMaximumSize;
	}

	public void setLdapConnectionPoolMaximumSize(Integer ldapConnectionPoolMaximumSize) {
		this.ldapConnectionPoolMaximumSize = ldapConnectionPoolMaximumSize;
	}

	public Integer getMaximumResultsReturnedFromSearch() {
		return maximumResultsReturnedFromSearch;
	}

	public void setMaximumResultsReturnedFromSearch(Integer maximumResultsReturnedFromSearch) {
		this.maximumResultsReturnedFromSearch = maximumResultsReturnedFromSearch;
	}

	public Integer getSearchTimeout() {
		return searchTimeout;
	}

	public void setSearchTimeout(Integer searchTimeout) {
		this.searchTimeout = searchTimeout;
	}

	public boolean getLdapFollowsReferral() {
		return ldapFollowsReferral;
	}

	public void setLdapFollowsReferral(boolean ldapFollowsReferral) {
		this.ldapFollowsReferral = ldapFollowsReferral;
	}

	public String getLdapv3RepositoryPlugInClassName() {
		return ldapv3RepositoryPlugInClassName;
	}

	public void setLdapv3RepositoryPlugInClassName(String ldapv3RepositoryPlugInClassName) {
		this.ldapv3RepositoryPlugInClassName = ldapv3RepositoryPlugInClassName;
	}

	public List<String> getAttributeNameMapping() {
		return attributeNameMapping;
	}

	public void setAttributeNameMapping(List<String> attributeNameMapping) {
		this.attributeNameMapping = attributeNameMapping;
	}

	public List<String> getLdapv3PlugInSupportedTypesAndOperations() {
		return ldapv3PlugInSupportedTypesAndOperations;
	}

	public void setLdapv3PlugInSupportedTypesAndOperations(List<String> ldapv3PlugInSupportedTypesAndOperations) {
		this.ldapv3PlugInSupportedTypesAndOperations = ldapv3PlugInSupportedTypesAndOperations;
	}

	public String getLdapv3PlugInSearchScope() {
		return ldapv3PlugInSearchScope;
	}

	public void setLdapv3PlugInSearchScope(String ldapv3PlugInSearchScope) {
		this.ldapv3PlugInSearchScope = ldapv3PlugInSearchScope;
	}

	public String getLdapUsersSearchAttribute() {
		return ldapUsersSearchAttribute;
	}

	public void setLdapUsersSearchAttribute(String ldapUsersSearchAttribute) {
		this.ldapUsersSearchAttribute = ldapUsersSearchAttribute;
	}

	public String getLdapUsersSearchFilter() {
		return ldapUsersSearchFilter;
	}

	public void setLdapUsersSearchFilter(String ldapUsersSearchFilter) {
		this.ldapUsersSearchFilter = ldapUsersSearchFilter;
	}

	public List<String> getLdapUserObjectClass() {
		return ldapUserObjectClass;
	}

	public void setLdapUserObjectClass(List<String> ldapUserObjectClass) {
		this.ldapUserObjectClass = ldapUserObjectClass;
	}

	public List<String> getLdapUserAttributes() {
		return ldapUserAttributes;
	}

	public void setLdapUserAttributes(List<String> ldapUserAttributes) {
		this.ldapUserAttributes = ldapUserAttributes;
	}

	public List<String> getCreateUserAttributeMapping() {
		return createUserAttributeMapping;
	}

	public void setCreateUserAttributeMapping(List<String> createUserAttributeMapping) {
		this.createUserAttributeMapping = createUserAttributeMapping;
	}

	public String getAttributeNameOfUserStatus() {
		return attributeNameOfUserStatus;
	}

	public void setAttributeNameOfUserStatus(String attributeNameOfUserStatus) {
		this.attributeNameOfUserStatus = attributeNameOfUserStatus;
	}

	public String getLdapGroupsSearchAttribute() {
		return ldapGroupsSearchAttribute;
	}

	public void setLdapGroupsSearchAttribute(String ldapGroupsSearchAttribute) {
		this.ldapGroupsSearchAttribute = ldapGroupsSearchAttribute;
	}

	public String getLdapGroupsSearchFilter() {
		return ldapGroupsSearchFilter;
	}

	public void setLdapGroupsSearchFilter(String ldapGroupsSearchFilter) {
		this.ldapGroupsSearchFilter = ldapGroupsSearchFilter;
	}

	public String getLdapGroupsContainerNamingAttribute() {
		return ldapGroupsContainerNamingAttribute;
	}

	public void setLdapGroupsContainerNamingAttribute(String ldapGroupsContainerNamingAttribute) {
		this.ldapGroupsContainerNamingAttribute = ldapGroupsContainerNamingAttribute;
	}

	public String getLdapGroupsContainerValue() {
		return ldapGroupsContainerValue;
	}

	public void setLdapGroupsContainerValue(String ldapGroupsContainerValue) {
		this.ldapGroupsContainerValue = ldapGroupsContainerValue;
	}

	public List<String> getLdapGroupsObjectClass() {
		return ldapGroupsObjectClass;
	}

	public void setLdapGroupsObjectClass(List<String> ldapGroupsObjectClass) {
		this.ldapGroupsObjectClass = ldapGroupsObjectClass;
	}

	public List<String> getLdapGroupsAttributes() {
		return ldapGroupsAttributes;
	}

	public void setLdapGroupsAttributes(List<String> ldapGroupsAttributes) {
		this.ldapGroupsAttributes = ldapGroupsAttributes;
	}

	public String getAttributeNameForGroupMembership() {
		return attributeNameForGroupMembership;
	}

	public void setAttributeNameForGroupMembership(String attributeNameForGroupMembership) {
		this.attributeNameForGroupMembership = attributeNameForGroupMembership;
	}

	public String getAttributeNameOfUnqiueMember() {
		return attributeNameOfUnqiueMember;
	}

	public void setAttributeNameOfUnqiueMember(String attributeNameOfUnqiueMember) {
		this.attributeNameOfUnqiueMember = attributeNameOfUnqiueMember;
	}

	public String getLdapPeopleContainerNamingAttribute() {
		return ldapPeopleContainerNamingAttribute;
	}

	public void setLdapPeopleContainerNamingAttribute(String ldapPeopleContainerNamingAttribute) {
		this.ldapPeopleContainerNamingAttribute = ldapPeopleContainerNamingAttribute;
	}

	public String getLdapPeopleContainerValue() {
		return ldapPeopleContainerValue;
	}

	public void setLdapPeopleContainerValue(String ldapPeopleContainerValue) {
		this.ldapPeopleContainerValue = ldapPeopleContainerValue;
	}

	public String getIdentityTypesThatCanBeAuthenticated() {
		return identityTypesThatCanBeAuthenticated;
	}

	public void setIdentityTypesThatCanBeAuthenticated(String identityTypesThatCanBeAuthenticated) {
		this.identityTypesThatCanBeAuthenticated = identityTypesThatCanBeAuthenticated;
	}

	public String getAuthenticationNamingAttribute() {
		return authenticationNamingAttribute;
	}

	public void setAuthenticationNamingAttribute(String authenticationNamingAttribute) {
		this.authenticationNamingAttribute = authenticationNamingAttribute;
	}

	public String getPersistentSearchBaseDn() {
		return persistentSearchBaseDn;
	}

	public void setPersistentSearchBaseDn(String persistentSearchBaseDn) {
		this.persistentSearchBaseDn = persistentSearchBaseDn;
	}

	public String getPersistentSearchFilter() {
		return persistentSearchFilter;
	}

	public void setPersistentSearchFilter(String persistentSearchFilter) {
		this.persistentSearchFilter = persistentSearchFilter;
	}

	public String getPersistentSearchScope() {
		return persistentSearchScope;
	}

	public void setPersistentSearchScope(String persistentSearchScope) {
		this.persistentSearchScope = persistentSearchScope;
	}

	public Integer getPersistentSearchMaximumIdleTimeBeforeRestart() {
		return persistentSearchMaximumIdleTimeBeforeRestart;
	}

	public void setPersistentSearchMaximumIdleTimeBeforeRestart(Integer persistentSearchMaximumIdleTimeBeforeRestart) {
		this.persistentSearchMaximumIdleTimeBeforeRestart = persistentSearchMaximumIdleTimeBeforeRestart;
	}

	public Integer getMaximumNumberOfRetriesAfterErrorCodes() {
		return maximumNumberOfRetriesAfterErrorCodes;
	}

	public void setMaximumNumberOfRetriesAfterErrorCodes(Integer maximumNumberOfRetriesAfterErrorCodes) {
		this.maximumNumberOfRetriesAfterErrorCodes = maximumNumberOfRetriesAfterErrorCodes;
	}

	public Integer getTheDelayTimeBetweenRetries() {
		return theDelayTimeBetweenRetries;
	}

	public void setTheDelayTimeBetweenRetries(Integer theDelayTimeBetweenRetries) {
		this.theDelayTimeBetweenRetries = theDelayTimeBetweenRetries;
	}

	public List<String> getLdapExceptionErrorCodesToRetryOn() {
		return ldapExceptionErrorCodesToRetryOn;
	}

	public void setLdapExceptionErrorCodesToRetryOn(List<String> ldapExceptionErrorCodesToRetryOn) {
		this.ldapExceptionErrorCodesToRetryOn = ldapExceptionErrorCodesToRetryOn;
	}

	public boolean getCaching() {
		return caching;
	}

	public void setCaching(boolean caching) {
		this.caching = caching;
	}

	public Integer getMaximumAgeOfCachedItems() {
		return maximumAgeOfCachedItems;
	}

	public void setMaximumAgeOfCachedItems(Integer maximumAgeOfCachedItems) {
		this.maximumAgeOfCachedItems = maximumAgeOfCachedItems;
	}

	public Integer getMaximumSizeOfTheCache() {
		return maximumSizeOfTheCache;
	}

	public void setMaximumSizeOfTheCache(Integer maximumSizeOfTheCache) {
		this.maximumSizeOfTheCache = maximumSizeOfTheCache;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	protected Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();

		map.put(DataStoresConstants.LDAP_SERVER_CURRENT_VALUES, asSet(getLdapServer()));
		map.put(DataStoresConstants.LDAP_BIND_DN, asSet(getLdapBindDn()));
		if (!DataStoresConstants.DEFAULT_PASSWORD.equals(getLdapBindPassword())) {
			map.put(DataStoresConstants.LDAP_BIND_PASSWORD, asSet(getLdapBindPassword()));
		}
		map.put(DataStoresConstants.LDAP_ORGANIZATION_DN, asSet(getLdapOrganizationDn()));
		map.put(DataStoresConstants.LDAP_SSL_TLS, asSet(getLdapSslTls()));
		map.put(DataStoresConstants.LDAP_CONNECTION_POOL_MINIMUM_SIZE, asSet(getLdapConnectionPoolMinimumSize()));
		map.put(DataStoresConstants.LDAP_CONNECTION_POOL_MAXIMUM_SIZE, asSet(getLdapConnectionPoolMaximumSize()));
		map.put(DataStoresConstants.MAXIMUM_RESULTS_RETURNED_FROM_SEARCH, asSet(getMaximumResultsReturnedFromSearch()));
		map.put(DataStoresConstants.SEARCH_TIMEOUT, asSet(getSearchTimeout()));
		map.put(DataStoresConstants.LDAP_FOLLOWS_REFERRAL, asSet(getLdapFollowsReferral()));
		map.put(DataStoresConstants.LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME, asSet(getLdapv3RepositoryPlugInClassName()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_MAPPING, asSet(getAttributeNameMapping()));
		map.put(DataStoresConstants.LDAPV3_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES,
				asSet(getLdapv3PlugInSupportedTypesAndOperations()));
		map.put(DataStoresConstants.LDAPV3_PLUGIN_SEARCH_SCOPE, asSet(getLdapv3PlugInSearchScope()));
		map.put(DataStoresConstants.LDAP_USERS_SEARCH_ATTRIBUTE, asSet(getLdapUsersSearchAttribute()));
		map.put(DataStoresConstants.LDAP_USERS_SEARCH_FILTER, asSet(getLdapUsersSearchFilter()));
		map.put(DataStoresConstants.LDAP_USER_OBJECT_CLASS_CURRENT_VALUES, asSet(getLdapUserObjectClass()));
		map.put(DataStoresConstants.LDAP_USER_ATTRIBUTES_CURRENT_VALUES, asSet(getLdapUserAttributes()));
		map.put(DataStoresConstants.CREATE_USER_ATTRIBUTE_MAPPING_CURRENT_VALUES,
				asSet(getCreateUserAttributeMapping()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_OF_USER_STATUS, asSet(getAttributeNameOfUserStatus()));
		map.put(DataStoresConstants.LDAP_GROUPS_SEARCH_ATTRIBUTE, asSet(getLdapGroupsSearchAttribute()));
		map.put(DataStoresConstants.LDAP_GROUPS_SEARCH_FILTER, asSet(getLdapGroupsSearchFilter()));
		map.put(DataStoresConstants.LDAP_GROUPS_CONTAINER_NAMING_ATTRIBUTE,
				asSet(getLdapGroupsContainerNamingAttribute()));
		map.put(DataStoresConstants.LDAP_GROUPS_CONTAINER_VALUE, asSet(getLdapGroupsContainerValue()));
		map.put(DataStoresConstants.LDAP_GROUPS_OBJECT_CLASS_CURRENT_VALUES, asSet(getLdapGroupsObjectClass()));
		map.put(DataStoresConstants.LDAP_GROUPS_ATTRIBUTES_CURRENT_VALUES, asSet(getLdapGroupsAttributes()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_FOR_GROUP_MEMBERSHIP, asSet(getAttributeNameForGroupMembership()));
		map.put(DataStoresConstants.ATTRIBUTE_NAME_OF_UNIQUE_MEMBER, asSet(getAttributeNameOfUnqiueMember()));
		map.put(DataStoresConstants.LDAP_PEOPLE_CONTAINER_NAMING_ATTRIBUTE,
				asSet(getLdapPeopleContainerNamingAttribute()));
		map.put(DataStoresConstants.LDAP_PEOPLE_CONTAINER_VALUE, asSet(getLdapPeopleContainerValue()));

		if ("User".equals(getIdentityTypesThatCanBeAuthenticated())) {
			map.put(DataStoresConstants.IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED,
					asSet(getIdentityTypesThatCanBeAuthenticated()));
		}
		map.put(DataStoresConstants.AUTHENTICATION_NAMING_ATTRIBUTE, asSet(getAuthenticationNamingAttribute()));
		map.put(DataStoresConstants.PERSISTENT_SEARCH_BASE_DN, asSet(getPersistentSearchBaseDn()));
		map.put(DataStoresConstants.PERSISTENT_SEARCH_FILTER, asSet(getPersistentSearchFilter()));
		map.put(DataStoresConstants.PERSISTENT_SEARCH_SCOPE, asSet(getPersistentSearchScope()));
		map.put(DataStoresConstants.PERSISTENT_SEARCH_MAXIMUM_IDLE_TIME_BEFORE_RESTART,
				asSet(getPersistentSearchMaximumIdleTimeBeforeRestart()));
		map.put(DataStoresConstants.MAXIMUM_NUMBER_OF_RETRIES_AFTER_ERROR_CODES,
				asSet(getMaximumNumberOfRetriesAfterErrorCodes()));
		map.put(DataStoresConstants.DELAY_BETWEEN_RETRIES, asSet(getTheDelayTimeBetweenRetries()));
		map.put(DataStoresConstants.CACHING, asSet(getCaching()));
		map.put(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS, asSet(getMaximumAgeOfCachedItems()));
		map.put(DataStoresConstants.PERSISTENT_SEARCH_SCOPE, asSet(getPersistentSearchScope()));
		map.put(DataStoresConstants.LDAP_EXCEPTION_ERROR_CODES_TO_RETRY_ON_CURRENT_VALUES,
				asSet(getLdapExceptionErrorCodesToRetryOn()));
		map.put(DataStoresConstants.MAXIMUM_SIZE_OF_THE_CACHE, asSet(getMaximumSizeOfTheCache()));

		return map;
	}

	/**
	 * Parse the given map and sets the fields on the given bean accordingly.
	 * 
	 * @param map
	 *            - the attributes map
	 * @param bean
	 *            - the bean to set the fields on
	 */
	protected static void setBaseFields(Map<String, Set<String>> map, LDAPv3DataStore bean) {
		BaseDataStore.setBaseFields(map, bean);

		bean.setLdapServer(listFromSet(map.get(DataStoresConstants.LDAP_SERVER_CURRENT_VALUES)));
		bean.setLdapBindDn(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_DN)));
		// bean.setLdapBindPassword(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_PASSWORD)));
		// bean.setLdapBindPasswordConfirm(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_PASSWORD)));
		bean.setLdapOrganizationDn(stringFromSet(map.get(DataStoresConstants.LDAP_ORGANIZATION_DN)));
		bean.setLdapSslTls(booleanFromSet(map.get(DataStoresConstants.LDAP_SSL_TLS)));
		bean.setLdapConnectionPoolMinimumSize(integerFromSet(map
				.get(DataStoresConstants.LDAP_CONNECTION_POOL_MINIMUM_SIZE)));
		bean.setLdapConnectionPoolMaximumSize(integerFromSet(map
				.get(DataStoresConstants.LDAP_CONNECTION_POOL_MAXIMUM_SIZE)));
		bean.setMaximumResultsReturnedFromSearch(integerFromSet(map
				.get(DataStoresConstants.MAXIMUM_RESULTS_RETURNED_FROM_SEARCH)));
		bean.setSearchTimeout(integerFromSet(map.get(DataStoresConstants.SEARCH_TIMEOUT)));
		bean.setLdapFollowsReferral(booleanFromSet(map.get(DataStoresConstants.LDAP_FOLLOWS_REFERRAL)));
		bean.setLdapv3RepositoryPlugInClassName(stringFromSet(map
				.get(DataStoresConstants.LDAPV3_REPOSITORY_PLUGIN_CLASS_NAME)));
		bean.setAttributeNameMapping(listFromSet(map.get(DataStoresConstants.ATTRIBUTE_NAME_MAPPING)));
		bean.setLdapv3PlugInSupportedTypesAndOperations(listFromSet(map
				.get(DataStoresConstants.LDAPV3_PLUGIN_SUPPORTED_TYPES_AND_OPERATIONS_CURRENT_VALUES)));
		bean.setLdapv3PlugInSearchScope(stringFromSet(map.get(DataStoresConstants.LDAPV3_PLUGIN_SEARCH_SCOPE)));
		bean.setLdapUsersSearchAttribute(stringFromSet(map.get(DataStoresConstants.LDAP_USERS_SEARCH_ATTRIBUTE)));
		bean.setLdapUsersSearchFilter(stringFromSet(map.get(DataStoresConstants.LDAP_USERS_SEARCH_FILTER)));
		bean.setLdapUserObjectClass(listFromSet(map.get(DataStoresConstants.LDAP_USER_OBJECT_CLASS_CURRENT_VALUES)));
		bean.setLdapUserAttributes(listFromSet(map.get(DataStoresConstants.LDAP_USER_ATTRIBUTES_CURRENT_VALUES)));
		bean.setCreateUserAttributeMapping(listFromSet(map
				.get(DataStoresConstants.CREATE_USER_ATTRIBUTE_MAPPING_CURRENT_VALUES)));
		bean.setAttributeNameOfUserStatus(stringFromSet(map.get(DataStoresConstants.ATTRIBUTE_NAME_OF_USER_STATUS)));
		bean.setLdapGroupsSearchAttribute(stringFromSet(map.get(DataStoresConstants.LDAP_GROUPS_SEARCH_ATTRIBUTE)));
		bean.setLdapGroupsSearchFilter(stringFromSet(map.get(DataStoresConstants.LDAP_GROUPS_SEARCH_FILTER)));
		bean.setLdapGroupsContainerNamingAttribute(stringFromSet(map
				.get(DataStoresConstants.LDAP_GROUPS_CONTAINER_NAMING_ATTRIBUTE)));
		bean.setLdapGroupsContainerValue(stringFromSet(map.get(DataStoresConstants.LDAP_GROUPS_CONTAINER_VALUE)));
		bean.setLdapGroupsObjectClass(listFromSet(map.get(DataStoresConstants.LDAP_GROUPS_OBJECT_CLASS_CURRENT_VALUES)));
		bean.setLdapGroupsAttributes(listFromSet(map.get(DataStoresConstants.LDAP_GROUPS_ATTRIBUTES_CURRENT_VALUES)));
		bean.setAttributeNameForGroupMembership(stringFromSet(map
				.get(DataStoresConstants.ATTRIBUTE_NAME_FOR_GROUP_MEMBERSHIP)));
		bean.setAttributeNameOfUnqiueMember(stringFromSet(map.get(DataStoresConstants.ATTRIBUTE_NAME_OF_UNIQUE_MEMBER)));
		bean.setLdapPeopleContainerNamingAttribute(stringFromSet(map
				.get(DataStoresConstants.LDAP_PEOPLE_CONTAINER_NAMING_ATTRIBUTE)));
		bean.setLdapPeopleContainerValue(stringFromSet(map.get(DataStoresConstants.LDAP_PEOPLE_CONTAINER_VALUE)));
		bean.setIdentityTypesThatCanBeAuthenticated(stringFromSet(map
				.get(DataStoresConstants.IDENTITY_TYPES_THAT_CAN_BE_AUTHENTICATED)));
		bean.setAuthenticationNamingAttribute(stringFromSet(map
				.get(DataStoresConstants.AUTHENTICATION_NAMING_ATTRIBUTE)));
		bean.setPersistentSearchBaseDn(stringFromSet(map.get(DataStoresConstants.PERSISTENT_SEARCH_BASE_DN)));
		bean.setPersistentSearchFilter(stringFromSet(map.get(DataStoresConstants.PERSISTENT_SEARCH_FILTER)));
		bean.setPersistentSearchScope(stringFromSet(map.get(DataStoresConstants.PERSISTENT_SEARCH_SCOPE)));
		bean.setPersistentSearchMaximumIdleTimeBeforeRestart(integerFromSet(map
				.get(DataStoresConstants.PERSISTENT_SEARCH_MAXIMUM_IDLE_TIME_BEFORE_RESTART)));
		bean.setMaximumNumberOfRetriesAfterErrorCodes(integerFromSet(map
				.get(DataStoresConstants.MAXIMUM_NUMBER_OF_RETRIES_AFTER_ERROR_CODES)));
		bean.setTheDelayTimeBetweenRetries(integerFromSet(map.get(DataStoresConstants.DELAY_BETWEEN_RETRIES)));
		bean.setCaching(booleanFromSet(map.get(DataStoresConstants.CACHING)));
		bean.setMaximumAgeOfCachedItems(integerFromSet(map.get(DataStoresConstants.MAXIMUM_AGE_OF_CACHED_ITEMS)));
		bean.setPersistentSearchScope(stringFromSet(map.get(DataStoresConstants.PERSISTENT_SEARCH_SCOPE)));
		bean.setLdapExceptionErrorCodesToRetryOn(listFromSet(map
				.get(DataStoresConstants.LDAP_EXCEPTION_ERROR_CODES_TO_RETRY_ON_CURRENT_VALUES)));
		bean.setMaximumSizeOfTheCache(integerFromSet(map.get(DataStoresConstants.MAXIMUM_SIZE_OF_THE_CACHE)));
		bean.setLdapBindPassword("".equals(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_PASSWORD))) ? ""
				: DataStoresConstants.DEFAULT_PASSWORD);
		bean.setLdapBindPasswordConfirm("".equals(stringFromSet(map.get(DataStoresConstants.LDAP_BIND_PASSWORD))) ? ""
				: DataStoresConstants.DEFAULT_PASSWORD);
	}

}
