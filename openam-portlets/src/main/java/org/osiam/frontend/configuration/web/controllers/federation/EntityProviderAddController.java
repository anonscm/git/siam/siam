/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.EntityProviderDescriptor;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * EntityProviderAddController shows the add EntityProvider form and does
 * request processing of the add EntityProvider action.
 * 
 */
@Controller("EntityProviderAddController")
@RequestMapping(value = "view", params = { "ctx=EntityProviderAddController" })
public class EntityProviderAddController extends BaseController {

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("entityProviderDescriptorValidator")
	private Validator entityProviderDescriptorValidator;

	/**
	 * Show add {@link EntityProvider} form.
	 * 
	 * @param checkAllServices
	 *            - use to decide if all the services are checked for deletion
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param protocol
	 *            - {@link EntityProvider} protocol
	 * @param added
	 *            - if a service was added this parameter has the value "true"
	 * @param error
	 *            - error message
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */

	@RenderMapping
	public String add(String checkAllServices, String entityIdentifier, String protocol, String added, String error,
			Model model, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "entityProvider")) {
			EntityProvider entityProvider = new EntityProvider();
			entityProvider.setEntityIdentifier(entityIdentifier);
			entityProvider.setProtocol(protocol);
			model.addAttribute("entityProvider", entityProvider);
		}

		if ("true".equals(added) || !model.containsAttribute("eentityProviderService")) {
			model.addAttribute("eentityProviderService", new EntityProviderDescriptor());
		}

		if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(protocol)) {
			model.addAttribute("serviceTypes", new String[] { "identity_provider", "service_provider",
					"attribute_query_provider", "attribute_authority", "authentication_authority",
					"xacml_policy_enforcement_point", "xacml_policy_decision_point" });
		} else {
			model.addAttribute("serviceTypes", new String[] { "identity_provider", "service_provider" });
		}

		model.addAttribute("checkAllServices", checkAllServices);
		model.addAttribute("error", error);

		return "federation/entityProvider/add";
	}

	/**
	 * Save an {@link EntityProvider}.
	 * 
	 * @param entityProvider
	 *            - {@link EntityProvider} to save
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(EntityProvider entityProvider, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		entityProvider.setRealm(realm);
		if (entityProvider.getDescriptorMap() != null && entityProvider.getDescriptorMap().size() > 0) {
			federationService.addEntityProvider(token, realm, entityProvider);
		} else {
			response.setRenderParameter("error", "error.descriptor_map_empty");
			return;
		}

		response.setRenderParameter("ctx", "FederationController");
	}

	/**
	 * Cancel {@link EntityProvider} form.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "cancel")
	public void doCancel(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "FederationController");
	}

	/**
	 * Add service for current {@link EntityProvider}.
	 * 
	 * @param entityProvider
	 *            - {@link EntityProvider}
	 * @param eentityProviderDescriptor
	 *            - {@link EntityProviderDescriptor}
	 * @param result
	 *            - BindingResult
	 * @param model
	 *            - Model
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addService")
	public void doAddService(EntityProvider entityProvider,
			@ModelAttribute("eentityProviderService") @Valid EntityProviderDescriptor eentityProviderDescriptor,
			BindingResult result, Model model, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		entityProviderDescriptorValidator.validate(new ObjectForValidate(realm, token, eentityProviderDescriptor),
				result);

		if (!result.hasErrors()) {
			entityProvider.getDescriptorMap().put(eentityProviderDescriptor.getType(), eentityProviderDescriptor);
			response.setRenderParameter("added", "true");
			model.addAttribute("eentityProviderService", null);
		}
		response.setRenderParameter("entityIdentifier", entityProvider.getEntityIdentifier());
		response.setRenderParameter("protocol", entityProvider.getProtocol());
	}

	/**
	 * Delete service for current {@link EntityProvider}.
	 * 
	 * @param entityProvider
	 *            - {@link EntityProvider}
	 * @param deleteServiceList
	 *            - list that contains service names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteServices")
	public void doDeleteServices(EntityProvider entityProvider, String[] deleteServiceList, ActionRequest request,
			ActionResponse response) {

		if (deleteServiceList != null) {
			for (String service : deleteServiceList) {
				entityProvider.getDescriptorMap().remove(service);
			}
		}
		response.setRenderParameter("entityIdentifier", entityProvider.getEntityIdentifier());
		response.setRenderParameter("protocol", entityProvider.getProtocol());
	}

	/**
	 * Set value for "checkAllServices".
	 * 
	 * @param entityProvider
	 *            - {@link EntityProvider}
	 * @param checkAllServices
	 *            - use to decide if all the services are checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(EntityProvider entityProvider, String checkAllServices, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityProvider.getEntityIdentifier());
		response.setRenderParameter("protocol", entityProvider.getProtocol());

		response.setRenderParameter("checkAllServices", checkAllServices);
	}
}
