/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a web miscellaneous bean.
 * 
 */
public class WebMiscellaneousBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String agentLocale;
	private boolean anonymousUser;
	// private String encodeSpecialCharsInCookies;
	private String profileAttributesCookiePrefix;
	@NotNull
	@Min(0)
	private Integer profileAttributesCookieMaxage;
	private boolean urlComparisonCaseSensitivityCheck;
	private boolean encodeURLSpecialCharacters;
	private boolean ignorePreferredNamingURLInNamingRequest;
	private boolean ignoreServerCheck;
	private boolean ignorePathInfoInRequestURL;
	private boolean nativeEncodingOfProfileAttributes;
	private String gotoParameterName;
	private String anonymousUserDefaultValue;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAgentLocale() {
		return agentLocale;
	}

	public void setAgentLocale(String agentLocale) {
		this.agentLocale = agentLocale;
	}

	public boolean getAnonymousUser() {
		return anonymousUser;
	}

	public void setAnonymousUser(boolean anonymousUser) {
		this.anonymousUser = anonymousUser;
	}

	public String getProfileAttributesCookiePrefix() {
		return profileAttributesCookiePrefix;
	}

	public void setProfileAttributesCookiePrefix(String profileAttributesCookiePrefix) {
		this.profileAttributesCookiePrefix = profileAttributesCookiePrefix;
	}

	public Integer getProfileAttributesCookieMaxage() {
		return profileAttributesCookieMaxage;
	}

	public void setProfileAttributesCookieMaxage(Integer profileAttributesCookieMaxage) {
		this.profileAttributesCookieMaxage = profileAttributesCookieMaxage;
	}

	public boolean getUrlComparisonCaseSensitivityCheck() {
		return urlComparisonCaseSensitivityCheck;
	}

	public void setUrlComparisonCaseSensitivityCheck(boolean urlComparisonCaseSensitivityCheck) {
		this.urlComparisonCaseSensitivityCheck = urlComparisonCaseSensitivityCheck;
	}

	public boolean getEncodeURLSpecialCharacters() {
		return encodeURLSpecialCharacters;
	}

	public void setEncodeURLSpecialCharacters(boolean encodeURLSpecialCharacters) {
		this.encodeURLSpecialCharacters = encodeURLSpecialCharacters;
	}

	public boolean getIgnorePreferredNamingURLInNamingRequest() {
		return ignorePreferredNamingURLInNamingRequest;
	}

	public void setIgnorePreferredNamingURLInNamingRequest(boolean ignorePreferredNamingURLInNamingRequest) {
		this.ignorePreferredNamingURLInNamingRequest = ignorePreferredNamingURLInNamingRequest;
	}

	public boolean getIgnorePathInfoInRequestURL() {
		return ignorePathInfoInRequestURL;
	}

	public void setIgnorePathInfoInRequestURL(boolean ignorePathInfoInRequestURL) {
		this.ignorePathInfoInRequestURL = ignorePathInfoInRequestURL;
	}

	public boolean getNativeEncodingOfProfileAttributes() {
		return nativeEncodingOfProfileAttributes;
	}

	public void setNativeEncodingOfProfileAttributes(boolean nativeEncodingOfProfileAttributes) {
		this.nativeEncodingOfProfileAttributes = nativeEncodingOfProfileAttributes;
	}

	public String getGotoParameterName() {
		return gotoParameterName;
	}

	public void setGotoParameterName(String gotoParameterName) {
		this.gotoParameterName = gotoParameterName;
	}

	public String getAnonymousUserDefaultValue() {
		return anonymousUserDefaultValue;
	}

	public void setAnonymousUserDefaultValue(String anonymousUserDefaultValue) {
		this.anonymousUserDefaultValue = anonymousUserDefaultValue;
	}

	public boolean getIgnoreServerCheck() {
		return ignoreServerCheck;
	}

	public void setIgnoreServerCheck(boolean ignoreServerCheck) {
		this.ignoreServerCheck = ignoreServerCheck;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebMiscellaneousBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebMiscellaneousBean bean = new WebMiscellaneousBean();
		bean.setName(name);

		bean.setAgentLocale(stringFromSet(attributes.get(AgentAttributesConstants.AGENT_LOCALE)));
		bean.setAnonymousUser(booleanFromSet(attributes.get(AgentAttributesConstants.ANONYMOUS_USER)));
		// bean.setEncodeSpecialCharsInCookies(stringFromSet(attributes
		// .get(AgentAttributesConstants.ENCODE_SPECIAL_CHARS_IN_COOKIES)));
		bean.setProfileAttributesCookiePrefix(stringFromSet(attributes
				.get(AgentAttributesConstants.PROFILE_ATTRIBUTES_COOKIE_PREFIX)));
		bean.setProfileAttributesCookieMaxage(integerFromSet(attributes
				.get(AgentAttributesConstants.PROFILE_ATTRIBUTES_COOKIE_MAXAGE)));
		bean.setUrlComparisonCaseSensitivityCheck(booleanFromSet(attributes
				.get(AgentAttributesConstants.URL_COMPARISON_CASE_SENSITIVITY_CHECK)));
		bean.setEncodeURLSpecialCharacters(booleanFromSet(attributes
				.get(AgentAttributesConstants.ENCODE_URLS_SPECIAL_CHARACTERS)));
		bean.setIgnorePreferredNamingURLInNamingRequest(booleanFromSet(attributes
				.get(AgentAttributesConstants.IGNORE_PREFERRED_NAMING_URL_IN_NAMING_REQUEST)));
		bean.setIgnoreServerCheck(booleanFromSet(attributes.get(AgentAttributesConstants.IGNORE_SERVER_CHECK)));
		bean.setIgnorePathInfoInRequestURL(booleanFromSet(attributes
				.get(AgentAttributesConstants.IGNORE_PATH_INFO_IN_REQUEST_URL)));
		bean.setNativeEncodingOfProfileAttributes(booleanFromSet(attributes
				.get(AgentAttributesConstants.NATIVE_ENCODING_OF_PROFILE_ATTRIBUTES)));
		bean.setGotoParameterName(stringFromSet(attributes.get(AgentAttributesConstants.GOTO_PARAMETER_NAME)));
		bean.setAnonymousUserDefaultValue(stringFromSet(attributes
				.get(AgentAttributesConstants.ANONYMOUS_USER_DEFAULT_VALUE)));

		return bean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		map.put(AgentAttributesConstants.AGENT_LOCALE, asSet(getAgentLocale()));
		map.put(AgentAttributesConstants.ANONYMOUS_USER, asSet(getAnonymousUser()));
		// map.put(AgentAttributesConstants.ENCODE_SPECIAL_CHARS_IN_COOKIES,
		// asSet(getEncodeSpecialCharsInCookies()));
		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTES_COOKIE_PREFIX, asSet(getProfileAttributesCookiePrefix()));
		map.put(AgentAttributesConstants.PROFILE_ATTRIBUTES_COOKIE_MAXAGE, asSet(getProfileAttributesCookieMaxage()));
		map.put(AgentAttributesConstants.URL_COMPARISON_CASE_SENSITIVITY_CHECK,
				asSet(getUrlComparisonCaseSensitivityCheck()));
		map.put(AgentAttributesConstants.ENCODE_URLS_SPECIAL_CHARACTERS, asSet(getEncodeURLSpecialCharacters()));
		map.put(AgentAttributesConstants.IGNORE_PREFERRED_NAMING_URL_IN_NAMING_REQUEST,
				asSet(getIgnorePreferredNamingURLInNamingRequest()));
		map.put(AgentAttributesConstants.IGNORE_SERVER_CHECK, asSet(getIgnoreServerCheck()));
		map.put(AgentAttributesConstants.IGNORE_PATH_INFO_IN_REQUEST_URL, asSet(getIgnorePathInfoInRequestURL()));
		map.put(AgentAttributesConstants.NATIVE_ENCODING_OF_PROFILE_ATTRIBUTES,
				asSet(getNativeEncodingOfProfileAttributes()));
		map.put(AgentAttributesConstants.GOTO_PARAMETER_NAME, asSet(getGotoParameterName()));
		map.put(AgentAttributesConstants.ANONYMOUS_USER_DEFAULT_VALUE, asSet(getAnonymousUserDefaultValue()));

		return map;
	}

}
