/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

/**
 * Encapsulates agents constants.
 * 
 */
public final class AgentsConstants {

	private AgentsConstants() {

	}

	/**
	 * Constant string for j2ee agent type.
	 */
	public static final String AGENT_TYPE_J2EE = "J2EEAgent";
	/**
	 * Constant string for web agent type.
	 */
	public static final String AGENT_TYPE_WEB = "WebAgent";
	/**
	 * Constant string for 2.2 agent type.
	 */
	public static final String AGENT_TYPE_2_DOT_2_AGENT = "2.2_Agent";
	/**
	 * Constant string for shared agent type.
	 */
	public static final String AGENT_TYPE_AGENT_AUTHENTICATOR = "SharedAgent";

	/**
	 * Constant string for local configuration storage.
	 */
	public static final String AGENT_CONFIGURATION_LOCAL = "local";
	/**
	 * Constant string for centralized configuration storage.
	 */
	public static final String AGENT_CONFIGURATION_CENTRALIZED = "centralized";

	/**
	 * Constant string used as key when persisting to XML.
	 */
	public static final String ATTR_NAME_AGENT_TYPE = "AgentType";
	/**
	 * Constant string used as key when persisting to XML.
	 */
	public static final String ATTR_CONFIG_REPO = "com.sun.identity.agents.config.repository.location";

}
