/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.validator.agents;

import java.util.regex.Pattern;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.configuration.domain.agents.Agent;
import org.osiam.frontend.configuration.service.AgentsConstants;
import org.osiam.frontend.configuration.service.AgentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the Agent object.
 * <p>
 * Validate if agent name already exist.<br>
 * Validate if the passwords entered do not match.<br>
 * Validate if Server URL is null when Configuration has the value
 * AgentsConstants.AGENT_CONFIGURATION_CENTRALIZED.<br>
 * Validate if the Server URL is valid.<br>
 * Validate if the Agent URL is valid.
 * </p>
 * 
 */
@Component("agentValidator")
public class AgentValidator implements Validator {

	@Autowired
	private AgentsService agentsService;

	private static final  Pattern PATTERN_URL = Pattern
			.compile("^http://[A-Za-z0-9.]+[- /.][A-Za-z]+[- /:][0-9]{1,9}[- //].+");

	private static final Pattern PATTERN_AGENT_URL = Pattern
			.compile("^http://[A-Za-z0-9.]+[- /.][A-Za-z]+[- /:][0-9][0-9][0-9][0-9]");

	@Override
	public boolean supports(Class<?> klass) {
		return Agent.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ObjectForValidate ofv = (ObjectForValidate) target;
		Agent object = (Agent) ofv.getObject();

		if (agentsService.agentNameAlreadyExist(ofv.getToken(), ofv.getRealm(), object.getName())) {
			errors.rejectValue("name", "NameAlreadyExist");
		}

		if (object.getPassword() != null && !object.getPassword().equals(object.getReEnterPassword())) {
			errors.rejectValue("reEnterPassword", "PassNotMath");
		}

		if (AgentsConstants.AGENT_CONFIGURATION_CENTRALIZED.equals(object.getConfiguration())) {
			if (object.getServerURL() == null || "".equals(object.getServerURL())) {
				errors.rejectValue("serverURL", "Mandatory");
			}
			if (!PATTERN_URL.matcher(object.getServerURL()).matches()) {
				errors.rejectValue("serverURL", "InvalidUrl");
			}
		}

		if (AgentsConstants.AGENT_TYPE_WEB.equals(object.getType())) {
			if (!PATTERN_AGENT_URL.matcher(object.getAgentURL()).matches()) {
				errors.rejectValue("agentURL", "InvalidUrl");
			}
		} else if (AgentsConstants.AGENT_TYPE_J2EE.equals(object.getType())) {
			if (!PATTERN_URL.matcher(object.getAgentURL()).matches()) {
				errors.rejectValue("agentURL", "InvalidUrl");
			}
		}

	}
}
