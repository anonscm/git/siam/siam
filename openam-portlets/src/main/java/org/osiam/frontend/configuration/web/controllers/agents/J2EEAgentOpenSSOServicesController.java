/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EEOpenSSOServicesBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentOpenSSOServicesController shows the edit "J2EE Agent" (OpenSSO
 * Services attributes) form and does request processing of the edit j2ee agent
 * action.
 * 
 */
@Controller("J2EEAgentOpenSSOServicesController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentOpenSSOServicesController" })
public class J2EEAgentOpenSSOServicesController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentOpenSSOServicesController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "J2EE Agent" - openSSO services attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {
			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EEOpenSSOServicesBean agent = agentsService.getAttributesForJ2EEOpenSSOServices(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/j2ee/openSSOServices/edit";

	}

	/**
	 * Save new value in OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addOpenSSOLoginURL")
	public void doAddOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String openSSOLoginURLddValue, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLddValue != null && !openSSOLoginURLddValue.equals("")) {
			j2eeAgent.getOpenSSOLoginURL().add(openSSOLoginURLddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletedOpenSSOLoginURL")
	public void doDeleteOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = 0; i < openSSOLoginURLSelectValues.length; i++) {
				j2eeAgent.getOpenSSOLoginURL().remove(openSSOLoginURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move up values from OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpOpenSSOLoginURL")
	public void doMoveUpOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = 0; i < openSSOLoginURLSelectValues.length; i++) {
				int poz = j2eeAgent.getOpenSSOLoginURL().indexOf(openSSOLoginURLSelectValues[i]);
				if (poz > 0) {
					j2eeAgent.setOpenSSOLoginURL(moveUp(j2eeAgent.getOpenSSOLoginURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move down values from OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownOpenSSOLoginURL")
	public void doMoveDownOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			for (int i = openSSOLoginURLSelectValues.length - 1; i >= 0; i--) {
				int poz = j2eeAgent.getOpenSSOLoginURL().indexOf(openSSOLoginURLSelectValues[i]);
				if (poz < j2eeAgent.getOpenSSOLoginURL().size() - 1) {
					j2eeAgent.setOpenSSOLoginURL(moveDown(j2eeAgent.getOpenSSOLoginURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move top values from OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopOpenSSOLoginURL")
	public void doMoveTopOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			j2eeAgent.setOpenSSOLoginURL(moveTop(j2eeAgent.getOpenSSOLoginURL(), openSSOLoginURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move bottom values from OpenSSO Login URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLoginURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomOpenSSOLoginURL")
	public void doMoveBottomOpenSSOLoginURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLoginURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLoginURLSelectValues != null) {
			j2eeAgent.setOpenSSOLoginURL(moveBottom(j2eeAgent.getOpenSSOLoginURL(), openSSOLoginURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addOpenSSOLogoutURL")
	public void doAddOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String openSSOLogoutURLddValue, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLddValue != null && !openSSOLogoutURLddValue.equals("")) {
			j2eeAgent.getOpenSSOLogoutURL().add(openSSOLogoutURLddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletedOpenSSOLogoutURL")
	public void doDeleteOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = 0; i < openSSOLogoutURLSelectValues.length; i++) {
				j2eeAgent.getOpenSSOLogoutURL().remove(openSSOLogoutURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move up values from OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpOpenSSOLogoutURL")
	public void doMoveUpOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = 0; i < openSSOLogoutURLSelectValues.length; i++) {
				int poz = j2eeAgent.getOpenSSOLogoutURL().indexOf(openSSOLogoutURLSelectValues[i]);
				if (poz > 0) {
					j2eeAgent.setOpenSSOLogoutURL(moveUp(j2eeAgent.getOpenSSOLogoutURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move down values from OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownOpenSSOLogoutURL")
	public void doMoveDownOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			for (int i = openSSOLogoutURLSelectValues.length - 1; i >= 0; i--) {
				int poz = j2eeAgent.getOpenSSOLogoutURL().indexOf(openSSOLogoutURLSelectValues[i]);
				if (poz < j2eeAgent.getOpenSSOLogoutURL().size() - 1) {
					j2eeAgent.setOpenSSOLogoutURL(moveDown(j2eeAgent.getOpenSSOLogoutURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move top values from OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopOpenSSOLogoutURL")
	public void doMoveTopOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			j2eeAgent.setOpenSSOLogoutURL(moveTop(j2eeAgent.getOpenSSOLogoutURL(), openSSOLogoutURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move bottom values from OpenSSO Logout URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param openSSOLogoutURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomOpenSSOLogoutURL")
	public void doMoveBottomOpenSSOLogoutURL(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String[] openSSOLogoutURLSelectValues, ActionRequest request, ActionResponse response) {

		if (openSSOLogoutURLSelectValues != null) {
			j2eeAgent.setOpenSSOLogoutURL(moveBottom(j2eeAgent.getOpenSSOLogoutURL(), openSSOLogoutURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in URL Policy Env GET Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvGETParametersAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addUrlPolicyEnvGETParameters")
	public void doAddUrlPolicyEnvGETParameters(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String urlPolicyEnvGETParametersAddValue, ActionRequest request,
			ActionResponse response) {

		if (urlPolicyEnvGETParametersAddValue != null && !urlPolicyEnvGETParametersAddValue.equals("")) {
			j2eeAgent.getUrlPolicyEnvGETParameters().add(urlPolicyEnvGETParametersAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from URL Policy Env GET Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvGETParametersDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteUrlPolicyEnvGETParameters")
	public void doDeleteUrlPolicyEnvGETParameters(
			@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent, BindingResult result,
			String[] urlPolicyEnvGETParametersDeleteValues, ActionRequest request, ActionResponse response) {

		if (urlPolicyEnvGETParametersDeleteValues != null) {
			for (int i = 0; i < urlPolicyEnvGETParametersDeleteValues.length; i++) {
				j2eeAgent.getUrlPolicyEnvGETParameters().remove(urlPolicyEnvGETParametersDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in URL Policy Env POST Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvPOSTParametersAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addUrlPolicyEnvPOSTParameters")
	public void doAddUrlPolicyEnvPOSTParameters(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, String urlPolicyEnvPOSTParametersAddValue, ActionRequest request,
			ActionResponse response) {

		if (urlPolicyEnvPOSTParametersAddValue != null && !urlPolicyEnvPOSTParametersAddValue.equals("")) {
			j2eeAgent.getUrlPolicyEnvPOSTParameters().add(urlPolicyEnvPOSTParametersAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from URL Policy Env POST Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvPOSTParametersDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteUrlPolicyEnvPOSTParameters")
	public void doDeleteUrlPolicyEnvPOSTParameters(
			@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent, BindingResult result,
			String[] urlPolicyEnvPOSTParametersDeleteValues, ActionRequest request, ActionResponse response) {

		if (urlPolicyEnvPOSTParametersDeleteValues != null) {
			for (int i = 0; i < urlPolicyEnvPOSTParametersDeleteValues.length; i++) {
				j2eeAgent.getUrlPolicyEnvPOSTParameters().remove(urlPolicyEnvPOSTParametersDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in URL Policy Env Jsession Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvJsessionParametersAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addUrlPolicyEnvJsessionParameters")
	public void doAddUrlPolicyEnvJsessionParameters(
			@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent, BindingResult result,
			String urlPolicyEnvJsessionParametersAddValue, ActionRequest request, ActionResponse response) {

		if (urlPolicyEnvJsessionParametersAddValue != null && !urlPolicyEnvJsessionParametersAddValue.equals("")) {
			j2eeAgent.getUrlPolicyEnvJsessionParameters().add(urlPolicyEnvJsessionParametersAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from URL Policy Env Jsession Parameters.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param urlPolicyEnvJsessionParametersDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteUrlPolicyEnvJsessionParameters")
	public void doDeleteUrlPolicyEnvJsessionParameters(
			@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent, BindingResult result,
			String[] urlPolicyEnvJsessionParametersDeleteValues, ActionRequest request, ActionResponse response) {

		if (urlPolicyEnvJsessionParametersDeleteValues != null) {
			for (int i = 0; i < urlPolicyEnvJsessionParametersDeleteValues.length; i++) {
				j2eeAgent.getUrlPolicyEnvJsessionParameters().remove(urlPolicyEnvJsessionParametersDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EEOpenSSOServicesBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEOpenSSOServicesBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentOpenSSOServices(@ModelAttribute("j2eeAgent") @Valid J2EEOpenSSOServicesBean j2eeAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveJ2EEOpenSSOServices(token, realm, j2eeAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link J2EEOpenSSOServicesBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentOpenSSOServices(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
