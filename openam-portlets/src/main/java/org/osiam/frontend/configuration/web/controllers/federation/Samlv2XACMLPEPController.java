/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2XACMLPEPAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * XACML PEP
 * 
 * Samlv2XACMLPEPController shows the edit SAMLv2 XACML PEP Attributes form and
 * does request processing of the edit SAMLv2 XACML PEP Attributes action.
 * 
 */
@Controller("Samlv2XACMLPEPController")
@RequestMapping(value = "view", params = { "ctx=samlv2xacml_policy_enforcement_pointController" })
public class Samlv2XACMLPEPController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2XACMLPEPController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows edit XACML PEP for current {@link EntityProvider} form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2XACMLPEPAttributesBean xacmlPep = (SAMLV2XACMLPEPAttributesBean) entityProvider.getDescriptorMap().get(
				serviceType);

		if (!model.containsAttribute("xacmlPep")) {
			model.addAttribute("xacmlPep", xacmlPep);
		}

		
		model.addAttribute("entityLocation", entityProvider.getLocation());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/xacmlpep/edit";
	}

	/**
	 * Save SAMLV2XACMLPEPAttributesBean.
	 * 
	 * @param xacmlPep
	 *            - SAMLV2XACMLPEPAttributesBean to save
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("xacmlPep") @Valid SAMLV2XACMLPEPAttributesBean xacmlPep, BindingResult result,
			String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
		
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		
		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2XACMLPEPAttributesBean oldXacmlPep = (SAMLV2XACMLPEPAttributesBean) entityProvider.getDescriptorMap().get(
				serviceType);
		
		xacmlPep.setLocation(entityProvider.getLocation());
		
		if(FederationConstants.REMOTE_VALUE.equals(entityProvider.getLocation())){
			xacmlPep.setBase64SigningCertificate(oldXacmlPep.getBase64SigningCertificate());
			xacmlPep.setBase64EncryptionCertificate(oldXacmlPep.getBase64EncryptionCertificate());
			xacmlPep.setAlgorithm(oldXacmlPep.getAlgorithm());
			
			xacmlPep.setEncryptionCertificateAlias(oldXacmlPep.getEncryptionCertificateAlias());
			xacmlPep.setSigningCertificateAlias(oldXacmlPep.getSigningCertificateAlias());
		}
		
		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, xacmlPep);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");
	}

	/**
	 * Reset form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
