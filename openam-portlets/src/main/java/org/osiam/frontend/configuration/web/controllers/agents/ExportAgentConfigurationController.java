/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.service.AgentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * Show export page for selected agent and does request processing of the export
 * agent action.
 */
@Controller("ExportAgentConfigurationController")
@RequestMapping(value = "view", params = { "ctx=ExportAgentConfigurationController" })
public class ExportAgentConfigurationController extends BaseController {

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show export page for selected agent.
	 * 
	 * @param name
	 *            - Agent name
	 * @param type
	 *            - Agent type
	 * @param config
	 *            - Agent configuration
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String view(String name, String type, String config, Model model, RenderRequest request) {

		model.addAttribute("name", name);
		model.addAttribute("type", type);

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		model.addAttribute("configList", agentsService.getExportConfiguration(token, realm, name));

		return "agents/export";

	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
