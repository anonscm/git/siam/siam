/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.DataStoresConstants;

/**
 * LDAPv3ForTivoliBean object which defines attributes for a
 * "Tivoli Directory Server" {@link DataStore}.
 * 
 */
public class LDAPv3ForTivoliBean extends LDAPv3DataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	private String defaultGroupMembersUserDn;

	public String getDefaultGroupMembersUserDn() {
		return defaultGroupMembersUserDn;
	}

	public void setDefaultGroupMembersUserDn(String defaultGroupMembersUserDn) {
		this.defaultGroupMembersUserDn = defaultGroupMembersUserDn;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();
		map.put(DataStoresConstants.DEFAULT_GROUP_MEMBERS_USER_DN, asSet(getDefaultGroupMembersUserDn()));

		return map;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param map
	 *            - map of attributes.
	 * 
	 */
	public static LDAPv3ForTivoliBean fromMap(Map<String, Set<String>> map) {
		LDAPv3ForTivoliBean bean = new LDAPv3ForTivoliBean();
		LDAPv3ForOpenDSBean.setBaseFields(map, bean);
		bean.setDefaultGroupMembersUserDn(stringFromSet(map.get(DataStoresConstants.DEFAULT_GROUP_MEMBERS_USER_DN)));

		return bean;
	}

}
