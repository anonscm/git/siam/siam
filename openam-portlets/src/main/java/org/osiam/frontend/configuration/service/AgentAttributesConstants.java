/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service;

/**
 * Encapsulates attributes constants for agents.
 * 
 */
public final class AgentAttributesConstants {

	private AgentAttributesConstants() {

	}

	/**
	 * Group.
	 */
	public static final String GROUP = "agentgroup";

	/**
	 * Password.
	 */

	public static final String PASSWORD = "userpassword";
	/**
	 * Status.
	 */

	public static final String STATUS = "sunIdentityServerDeviceStatus";
	/**
	 * Location of Agent Configuration Repository.
	 */

	public static final String LOCATION_OF_AGENT_CONFIGURATION_REPOSITORY = "com.sun.identity.agents.config.repository.location";
	/**
	 * Agent Configuration Change Notification.
	 */

	public static final String AGENT_CONFIGURATION_CHANGE_NOTIFICATION = "com.sun.identity.agents.config.change.notification.enable";
	/**
	 * Enable Notifications.
	 */

	public static final String ENABLE_NOTIFICATIONS = "com.sun.identity.agents.config.notification.enable";
	/**
	 * Agent Notification URL.
	 */

	public static final String AGENT_NOTIFICATION_URL = "com.sun.identity.client.notification.url";
	/**
	 * Agent Deployment URI Prefix.
	 */

	public static final String AGENT_DEPLOYMENT_URI_PREFIX = "com.sun.identity.agents.config.agenturi.prefix";
	/**
	 * Configuration Reload Interval Web.
	 */

	public static final String CONFIGURATION_RELOAD_INTERVAL_WEB = "com.sun.identity.agents.config.polling.interval";

	/**
	 * Configuration Reload Interval.
	 */

	public static final String CONFIGURATION_RELOAD_INTERVAL_J2EE = "com.sun.identity.agents.config.load.interval";

	/**
	 * Configuration Cleanup Interval.
	 */

	public static final String CONFIGURATION_CLEANUP_INTERVAL = "com.sun.identity.agents.config.cleanup.interval";
	/**
	 * Agent Root URL for CDSSO - Current Values.
	 */

	public static final String AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES = "sunIdentityServerDeviceKeyValue";
	/**
	 * SSO Only Mode.
	 */

	public static final String SSO_ONLY_MODE = "com.sun.identity.agents.config.sso.only";
	/**
	 * Resources Access Denied URL.
	 */

	public static final String RESOURCES_ACCESS_DENIED_URL = "com.sun.identity.agents.config.access.denied.url";
	/**
	 * Agent Debug Level.
	 */

	public static final String AGENT_DEBUG_LEVEL = "com.sun.identity.agents.config.debug.level";

	/**
	 * Agent Debug Level J2EE.
	 */

	public static final String AGENT_DEBUG_LEVEL_J2EE = "com.iplanet.services.debug.level";

	/**
	 * Agent Debug File Rotation.
	 */

	public static final String AGENT_DEBUG_FILE_ROTATION = "com.sun.identity.agents.config.debug.file.rotate";
	/**
	 * Agent Debug File Size.
	 */

	public static final String AGENT_DEBUG_FILE_SIZE = "com.sun.identity.agents.config.debug.file.size";
	/**
	 * Audit Access Types.
	 */

	public static final String AUDIT_ACCESS_TYPES = "com.sun.identity.agents.config.audit.accesstype";
	/**
	 * Audit Log Location.
	 */

	public static final String AUDIT_LOG_LOCATION = "com.sun.identity.agents.config.log.disposition";
	/**
	 * Remote Log Filename.
	 */

	public static final String REMOTE_LOG_FILENAME = "com.sun.identity.agents.config.remote.logfile";
	/**
	 * Remote Audit Log Interval.
	 */

	public static final String REMOTE_AUDIT_LOG_INTERVAL = "com.sun.identity.agents.config.remote.log.interval";
	/**
	 * FQDN Check.
	 */

	public static final String FQDN_CHECK = "com.sun.identity.agents.config.fqdn.check.enable";
	/**
	 * FQDN Default.
	 */

	public static final String FQDN_DEFAULT = "com.sun.identity.agents.config.fqdn.default";
	/**
	 * FQDN Virtual Host Map - Current Values.
	 */

	public static final String FQDN_VIRTUAL_HOST_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.fqdn.mapping";
	/**
	 * Rotate Local Audit Log.
	 */

	public static final String ROTATE_LOCAL_AUDIT_LOG = "com.sun.identity.agents.config.local.log.rotate";
	/**
	 * Local Audit Log Rotation Size.
	 */

	public static final String LOCAL_AUDIT_LOG_ROTATION_SIZE = "com.sun.identity.agents.config.local.log.size";
	/**
	 * Ignore Path Info for Not Enforced URLs.
	 */

	public static final String IGNORE_PATH_INFO_FOR_NOT_ENFORCED_URLS = "com.sun.identity.agents.config.ignore.path.info.for.not.enforced.list";
	/**
	 * Not Enforced URLs - Current Values.
	 */

	public static final String NOT_ENFORCED_URLS_CURRENT_VALUES = "com.sun.identity.agents.config.notenforced.url";
	/**
	 * Invert Not Enforced URLs.
	 */

	public static final String INVERT_NOT_ENFORCED_URLS = "com.sun.identity.agents.config.notenforced.url.invert";
	/**
	 * Fetch Attributes for Not Enforced URLs.
	 */

	public static final String FETCH_ATTRIBUTES_FOR_NOT_ENFORCED_URLS = "com.sun.identity.agents.config.notenforced.url.attributes.enable";
	/**
	 * Not Enforced Client IP List - Current Values.
	 */

	public static final String NOT_ENFORCED_CLIENT_IP_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.notenforced.ip";
	/**
	 * Client IP Validation.
	 */

	public static final String CLIENT_IP_VALIDATION = "com.sun.identity.agents.config.client.ip.validation.enable";
	/**
	 * Profile Attribute Fetch Mode.
	 */

	public static final String PROFILE_ATTRIBUTE_FETCH_MODE = "com.sun.identity.agents.config.profile.attribute.fetch.mode";
	/**
	 * Profile Attribute Map - Current Values.
	 */

	public static final String PROFILE_ATTRIBUTE_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.profile.attribute.mapping";
	/**
	 * Response Attribute Fetch Mode.
	 */

	public static final String RESPONSE_ATTRIBUTE_FETCH_MODE = "com.sun.identity.agents.config.response.attribute.fetch.mode";
	/**
	 * Response Attribute Map - Current Values.
	 */

	public static final String RESPONSE_ATTRIBUTE_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.response.attribute.mapping";
	/**
	 * Session Attribute Fetch Mode.
	 */

	public static final String SESSION_ATTRIBUTE_FETCH_MODE = "com.sun.identity.agents.config.session.attribute.fetch.mode";
	/**
	 * Session Attribute Map - Current Values.
	 */

	public static final String SESSION_ATTRIBUTE_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.session.attribute.mapping";

	/**
	 * Attribute Multi Value Separator.
	 */
	public static final String ATTRIBUTE_MULTI_VALUE_SEPARATOR = "com.sun.identity.agents.config.attribute.multi.value.separator";
	/**
	 * Cookie Name.
	 */
	public static final String COOKIE_NAME = "com.sun.identity.agents.config.cookie.name";

	/**
	 * J2EE SSO Cookie Name.
	 */
	public static final String J2EE_SSO_COOKIE_NAME = "com.iplanet.am.cookie.name";

	/**
	 * Cookie Security.
	 */

	public static final String COOKIE_SECURITY = "com.sun.identity.agents.config.cookie.secure";
	/**
	 * Cross Domain SSO.
	 */

	public static final String CROSS_DOMAIN_SSO = "com.sun.identity.agents.config.cdsso.enable";
	/**
	 * CDSSO Servlet URL Current Values.
	 */

	public static final String CDSSO_SERVLET_URL_CURRENT_VALUES = "com.sun.identity.agents.config.cdsso.cdcservlet.url";
	/**
	 * Cookies Domain List Current Values.
	 */

	public static final String COOKIES_DOMAIN_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.cdsso.cookie.domain";
	/**
	 * Cookie Reset.
	 */

	public static final String COOKIE_RESET = "com.sun.identity.agents.config.cookie.reset.enable";
	/**
	 * Cookies Reset Name List Current Values.
	 */

	public static final String COOKIES_RESET_NAME_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.cookie.reset";
	/**
	 * OpenSSO Login URL Current Values.
	 */

	public static final String OPENSSO_LOGIN_URL_CURRENT_VALUES = "com.sun.identity.agents.config.login.url";
	/**
	 * Agent Connection Timeout.
	 */

	public static final String AGENT_CONNECTION_TIMEOUT = "com.sun.identity.agents.config.auth.connection.timeout";
	/**
	 * Polling Period for Primary Server.
	 */

	public static final String POLLING_PERIOD_FOR_PRIMARY_SERVER = "com.sun.identity.agents.config.poll.primary.server";
	/**
	 * OpenSSO Logout URL Current Values.
	 */

	public static final String OPENSSO_LOGOUT_URL_CURRENT_VALUES = "com.sun.identity.agents.config.logout.url";
	/**
	 * Logout URL List Current Values.
	 */

	public static final String LOGOUT_URL_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.agent.logout.url";
	/**
	 * Logout Cookies List for Reset Current Values.
	 */

	public static final String LOGOUT_COOKIES_LIST_FOR_RESET_CURRENT_VALUES = "com.sun.identity.agents.config.logout.cookie.reset";
	/**
	 * Logout Redirect URL.
	 */

	public static final String LOGOUT_REDIRECT_URL = "com.sun.identity.agents.config.logout.redirect.url";
	/**
	 * Policy Cache Polling Period.
	 */

	public static final String POLICY_CACHE_POLLING_PERIOD = "com.sun.identity.agents.config.policy.cache.polling.interval";
	/**
	 * SSO Cache Polling Period.
	 */

	public static final String SSO_CACHE_POLLING_PERIOD = "com.sun.identity.agents.config.sso.cache.polling.interval";
	/**
	 * User ID Parameter.
	 */

	public static final String USER_ID_PARAMETER = "com.sun.identity.agents.config.userid.param";
	/**
	 * User ID Parameter Type.
	 */

	public static final String USER_ID_PARAMETER_TYPE = "com.sun.identity.agents.config.userid.param.type";
	/**
	 * Fetch Policies from Root Resource.
	 */

	public static final String FETCH_POLICIES_FROM_ROOT_RESOURCE = "com.sun.identity.agents.config.fetch.from.root.resource";
	/**
	 * Retrieve Client Hostname.
	 */

	public static final String RETRIEVE_CLIENT_HOSTNAME = "com.sun.identity.agents.config.get.client.host.name";
	/**
	 * Policy Clock Skew.
	 */

	public static final String POLICY_CLOCK_SKEW = "com.sun.identity.agents.config.policy.clock.skew";

	/**
	 * Agent Locale.
	 */

	public static final String AGENT_LOCALE = "com.sun.identity.agents.config.locale";
	/**
	 * Anonymous User.
	 */

	public static final String ANONYMOUS_USER = "com.sun.identity.agents.config.anonymous.user.enable";
	/**
	 * Encode special chars in Cookies.
	 */

	public static final String ENCODE_SPECIAL_CHARS_IN_COOKIES = "com.sun.identity.agents.config.encode.cookie.special.chars.enable";
	/**
	 * Profile Attributes Cookie Prefix.
	 */

	public static final String PROFILE_ATTRIBUTES_COOKIE_PREFIX = "com.sun.identity.agents.config.profile.attribute.cookie.prefix";
	/**
	 * Profile Attributes Cookie Maxage.
	 */

	public static final String PROFILE_ATTRIBUTES_COOKIE_MAXAGE = "com.sun.identity.agents.config.profile.attribute.cookie.maxage";
	/**
	 * URL Comparison Case Sensitivity Check.
	 */

	public static final String URL_COMPARISON_CASE_SENSITIVITY_CHECK = "com.sun.identity.agents.config.url.comparison.case.ignore";
	/**
	 * Encode URL's Special Characters.
	 */

	public static final String ENCODE_URLS_SPECIAL_CHARACTERS = "com.sun.identity.agents.config.encode.url.special.chars.enable";
	/**
	 * Ignore Preferred Naming URL in Naming Request.
	 */

	public static final String IGNORE_PREFERRED_NAMING_URL_IN_NAMING_REQUEST = "com.sun.identity.agents.config.ignore.preferred.naming.url";
	/**
	 * Ignore Server Check.
	 */

	public static final String IGNORE_SERVER_CHECK = "com.sun.identity.agents.config.ignore.server.check";
	/**
	 * Ignore Path Info in Request URL.
	 */

	public static final String IGNORE_PATH_INFO_IN_REQUEST_URL = "com.sun.identity.agents.config.ignore.path.info";
	/**
	 * Native Encoding of Profile Attributes.
	 */

	public static final String NATIVE_ENCODING_OF_PROFILE_ATTRIBUTES = "com.sun.identity.agents.config.convert.mbyte.enable";
	/**
	 * Goto Parameter Name.
	 */

	public static final String GOTO_PARAMETER_NAME = "com.sun.identity.agents.config.redirect.param";
	/**
	 * Anonymous User Default Value.
	 */

	public static final String ANONYMOUS_USER_DEFAULT_VALUE = "com.sun.identity.agents.config.anonymous.user.id";
	/**
	 * Client IP Address Header.
	 */

	public static final String CLIENT_IP_ADDRESS_HEADER = "com.sun.identity.agents.config.client.ip.header";
	/**
	 * Client Hostname Header.
	 */

	public static final String CLIENT_HOSTNAME_HEADER = "com.sun.identity.agents.config.client.hostname.header";
	/**
	 * Load Balancer Setup.
	 */

	public static final String LOAD_BALANCER_SETUP = "com.sun.identity.agents.config.load.balancer.enable";
	/**
	 * Override Request URL Protocol.
	 */

	public static final String OVERRIDE_REQUEST_URL_PROTOCOL = "com.sun.identity.agents.config.override.protocol";
	/**
	 * Override Request URL Host.
	 */

	public static final String OVERRIDE_REQUEST_URL_HOST = "com.sun.identity.agents.config.override.host";
	/**
	 * Override Request URL Port.
	 */

	public static final String OVERRIDE_REQUEST_URL_PORT = "com.sun.identity.agents.config.override.port";
	/**
	 * Override Notification URL.
	 */

	public static final String OVERRIDE_NOTIFICATION_URL = "com.sun.identity.agents.config.override.notification.url";
	/**
	 * POST Data Preservation.
	 */

	public static final String POST_DATA_PRESERVATION = "com.sun.identity.agents.config.postdata.preserve.enable";
	/**
	 * POST Data Entries Cache Period.
	 */

	public static final String POST_DATA_ENTRIES_CACHE_PERIOD = "com.sun.identity.agents.config.postcache.entry.lifetime";
	/**
	 * Override Proxy Server's Host and Port.
	 */

	public static final String OVERRIDE_PROXY_SERVERS_HOST_AND_PORT = "com.sun.identity.agents.config.proxy.override.host.port";

	/**
	 * Authentication Type.
	 */
	public static final String AUTHENTICATION_TYPE = "com.sun.identity.agents.config.iis.auth.type";
	/**
	 * Replay Password Key.
	 */
	public static final String REPLAY_PASSWORD_KEY = "com.sun.identity.agents.config.replaypasswd.key";
	/**
	 * Filter Priority.
	 */

	public static final String FILTER_PRIORITY = "com.sun.identity.agents.config.iis.filter.priority";
	/**
	 * Filter configured with OWA.
	 */

	public static final String FILTER_CONFIGURED_WITH_OWA = "com.sun.identity.agents.config.iis.owa.enable";
	/**
	 * Change URL Protocol to https.
	 */

	public static final String CHANGE_URL_PROTOCOL_TO_HTTPS = "com.sun.identity.agents.config.iis.owa.enable.change.protocol";
	/**
	 * Idle Session Timeout Page URL.
	 */

	public static final String IDLE_SESSION_TIMEOUT_PAGE_URL = "com.sun.identity.agents.config.iis.owa.enable.session.timeout.url";

	/**
	 * Check User in Domino Database.
	 */
	public static final String CHECK_USER_IN_DOMINO_DATABASE = "com.sun.identity.agents.config.domino.check.name.database";

	/**
	 * Use LTPA token.
	 */
	public static final String USE_LTPA_TOKEN = "com.sun.identity.agents.config.domino.ltpa.enable";
	/**
	 * LTPA Token Cookie Name.
	 */

	public static final String LTPA_TOKEN_COOKIE_NAME = "com.sun.identity.agents.config.domino.ltpa.cookie.name";
	/**
	 * LTPA Token Configuration Name.
	 */

	public static final String LTPA_TOKEN_CONFIGURATION_NAME = "com.sun.identity.agents.config.domino.ltpa.config.name";
	/**
	 * LTPA Token Organization Name.
	 */

	public static final String LTPA_TOKEN_ORGANIZATION_NAME = "com.sun.identity.agents.config.domino.ltpa.org.name";
	/**
	 * Custom Properties Current Values.
	 */

	public static final String CUSTOM_PROPERTIES_CURRENT_VALUES = "com.sun.identity.agents.config.freeformproperties";
	/**
	 * Group.
	 */

	public static final String AGENT_FILTER_MODE_CURRENT_VALUES = "com.sun.identity.agents.config.filter.mode";
	/**
	 * HTTP Session Binding.
	 */

	public static final String HTTP_SESSION_BINDING = "com.sun.identity.agents.config.httpsession.binding";
	/**
	 * Login Attempt Limit.
	 */

	public static final String LOGIN_ATTEMPT_LIMIT = "com.sun.identity.agents.config.login.attempt.limit";
	/**
	 * Custom Response Header Current Values.
	 */

	public static final String CUSTOM_RESPONSE_HEADER_CURRENT_VALUES = "com.sun.identity.agents.config.response.header";
	/**
	 * Redirect Attempt Limit.
	 */

	public static final String REDIRECT_ATTEMPT_LIMIT = "com.sun.identity.agents.config.redirect.attempt.limit";
	/**
	 * Agent Debug Level.
	 */

	public static final String USER_MAPPING_MODE = "com.sun.identity.agents.config.user.mapping.mode";
	/**
	 * User Attribute Name.
	 */

	public static final String USER_ATTRIBUTE_NAME = "com.sun.identity.agents.config.user.attribute.name";
	/**
	 * User Principal Flag.
	 */

	public static final String USER_PRINCIPAL_FLAG = "com.sun.identity.agents.config.user.principal";
	/**
	 * User Token Name.
	 */

	public static final String USER_TOKEN_NAME = "com.sun.identity.agents.config.user.token";
	/**
	 * Audit Access Types.
	 */

	public static final String REMOTE_LOG_FILE_NAME = "com.sun.identity.agents.config.remote.logfile";

	/**
	 * Login Form URI.
	 */
	public static final String LOGIN_FORM_URI = "com.sun.identity.agents.config.login.form";

	/**
	 * Login Error URI.
	 */
	public static final String LOGIN_ERROR_URI = "com.sun.identity.agents.config.login.error.uri";

	/**
	 * Use Internal Login.
	 */
	public static final String USE_INTERNAL_LOGIN = "com.sun.identity.agents.config.login.use.internal";

	/**
	 * Login Content File Name.
	 */
	public static final String LOGIN_CONTENT_FILE_NAME = "com.sun.identity.agents.config.login.content.file";

	/**
	 * Application Logout Handler.
	 */
	public static final String APPLICATION_LOGOUT_HANDLER = "com.sun.identity.agents.config.logout.application.handler";

	/**
	 * Application Logout URI.
	 */
	public static final String APPLICATION_LOGOUT_URI = "com.sun.identity.agents.config.logout.uri";
	/**
	 * Logout Request Parameter.
	 */

	public static final String LOGOUT_REQUEST_PARAMETER = "com.sun.identity.agents.config.logout.request.param";
	/**
	 * Logout Introspect Enabled.
	 */

	public static final String LOGOUT_INTROSPECT_ENABLED = "com.sun.identity.agents.config.logout.introspect.enabled";
	/**
	 * Logout Entry URI Current Values.
	 */

	public static final String LOGOUT_ENTRY_URI_CURRENT_VALUES = "com.sun.identity.agents.config.logout.entry.uri";
	/**
	 * Resource Access Denied URI Current Values.
	 */

	public static final String RESOURCE_ACCESS_DENIED_URI_CURRENT_VALUES = "com.sun.identity.agents.config.access.denied.uri";
	/**
	 * Not Enforced URIs Current Values.
	 */

	public static final String NOT_ENFORCED_URIS_CURRENT_VALUES = "com.sun.identity.agents.config.notenforced.uri";
	/**
	 * Invert Not Enforced URIs.
	 */

	public static final String INVERT_NOT_ENFORCED_URIS = "com.sun.identity.agents.config.notenforced.uri.invert";
	/**
	 * Not Enforced URIs Cache Enabled.
	 */

	public static final String NOT_ENFORCED_URIS_CACHE_ENABLED = "com.sun.identity.agents.config.notenforced.uri.cache.enable";
	/**
	 * Not Enforced URIs Cache Size.
	 */

	public static final String NOT_ENFORCED_URIS_CACHE_SIZE = "com.sun.identity.agents.config.notenforced.uri.cache.size";
	/**
	 * Refresh Session Idle Time.
	 */

	public static final String REFRESH_SESSION_IDLE_TIME = "com.sun.identity.agents.config.notenforced.refresh.session.idletime";

	/**
	 * Not Enforced IP Invert List.
	 */

	public static final String NOT_ENFORCED_IP_INVERT_LIST = "com.sun.identity.agents.config.notenforced.ip.invert";

	/**
	 * Not Enforced IP Cache Flag.
	 */

	public static final String NOT_ENFORCED_IP_CACHE_FLAG = "com.sun.identity.agents.config.notenforced.ip.cache.enable";

	/**
	 * Not Enforced IP Cache Size.
	 */

	public static final String NOT_ENFORCED_IP_CACHE_SIZE = "com.sun.identity.agents.config.notenforced.ip.cache.size";

	/**
	 * Profile Attribute Mapping Current Values.
	 */

	public static final String PROFILE_ATTRIBUTE_MAPPING_CURRENT_VALUES = "com.sun.identity.agents.config.profile.attribute.mapping";

	/**
	 * Response Attribute Mapping Current Values.
	 */

	public static final String RESPONSE_ATTRIBUTE_MAPPING_CURRENT_VALUES = "com.sun.identity.agents.config.response.attribute.mapping";

	/**
	 * Cookie Separator Character.
	 */
	public static final String COOKIE_SEPARATOR_CHARACTER = "com.sun.identity.agents.config.attribute.cookie.separator";

	/**
	 * Fetch Attribute Date Format.
	 */

	public static final String FETCH_ATTRIBUTE_DATE_FORMAT = "com.sun.identity.agents.config.attribute.date.format";

	/**
	 * Attribute Cookie Encode.
	 */

	public static final String ATTRIBUTE_COOKIE_ENCODE = "com.sun.identity.agents.config.attribute.cookie.encode";

	/**
	 * Session Attribute Mapping Current Values.
	 */

	public static final String SESSION_ATTRIBUTE_MAPPING_CURRENT_VALUES = "com.sun.identity.agents.config.session.attribute.mapping";

	/**
	 * Default Privileged Attribute Current Values.
	 */

	public static final String DEFAULT_PRIVILEGED_ATTRIBUTE_CURRENT_VALUES = "com.sun.identity.agents.config.default.privileged.attribute";

	/**
	 * Privileged Attribute Type Current Values.
	 */

	public static final String PRIVILEGED_ATTRIBUTE_TYPE_CURRENT_VALUES = "com.sun.identity.agents.config.privileged.attribute.type";

	/**
	 * Privileged Attributes To Lower Case Current Values.
	 */

	public static final String PRIVILEGED_ATTRIBUTES_TO_LOWER_CASE_CURRENT_VALUES = "com.sun.identity.agents.config.privileged.attribute.tolowercase";

	/**
	 * Privileged Session Attribute Current Values.
	 */

	public static final String PRIVILEGED_SESSION_ATTRIBUTE_CURRENT_VALUES = "com.sun.identity.agents.config.privileged.session.attribute";

	/**
	 * Enable Privileged Attribute Mapping.
	 */

	public static final String ENABLE_PRIVILEGED_ATTRIBUTE_MAPPING = "com.sun.identity.agents.config.privileged.attribute.mapping.enable";

	/**
	 * Custom Authentication Handler Current Values.
	 */

	public static final String CUSTOM_AUTHENTICATION_HANDLER_CURRENT_VALUES = "com.sun.identity.agents.config.auth.handler";

	/**
	 * Custom Logout Handler Current Values.
	 */

	public static final String CUSTOM_LOGOUT_HANDLER_CURRENT_VALUES = "com.sun.identity.agents.config.logout.handler";

	/**
	 * Custom Verification Handler Current Values.
	 */

	public static final String CUSTOM_VERIFICATION_HANDLER_CURRENT_VALUES = "com.sun.identity.agents.config.verification.handler";

	/**
	 * SSO Cache Enable.
	 */
	public static final String SSO_CACHE_ENABLE = "com.sun.identity.agents.config.amsso.cache.enable";

	/**
	 * CDSSO Redirect URI.
	 */

	public static final String CDSSO_REDIRECT_URI = "com.sun.identity.agents.config.cdsso.redirect.uri";
	/**
	 * CDSSO Servlet URL.
	 */

	public static final String CDSSO_SERVLET_URL = "com.sun.identity.agents.config.cdsso.cdcservlet.url";
	/**
	 * CDSSO Clock Skew.
	 */

	public static final String CDSSO_CLOCK_SKEW = "com.sun.identity.agents.config.cdsso.clock.skew";
	/**
	 * CDSSO Trusted ID Provider.
	 */

	public static final String CDSSO_TRUSTED_ID_PROVIDER = "com.sun.identity.agents.config.cdsso.trusted.id.provider";
	/**
	 * CDSSO Secure Enable.
	 */

	public static final String CDSSO_SECURE_ENABLE = "com.sun.identity.agents.config.cdsso.secure.enable";
	/**
	 * CDSSO Domain List Current Values.
	 */

	public static final String CDSSO_DOMAIN_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.cdsso.domain";
	/**
	 * Cookies Reset Name List.
	 */

	public static final String COOKIES_RESET_NAME_LIST = "com.sun.identity.agents.config.cookie.reset.name";
	/**
	 * Cookies Reset Domain Map Current Values.
	 */

	public static final String COOKIES_RESET_DOMAIN_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.cookie.reset.domain";
	/**
	 * Cookies Reset Path Map Current Values.
	 */

	public static final String COOKIES_RESET_PATH_MAP_CURRENT_VALUES = "com.sun.identity.agents.config.cookie.reset.path";
	/**
	 * OpenSSO Login URL.
	 */

	public static final String OPENSSO_LOGIN_URL = "com.sun.identity.agents.config.login.url";
	/**
	 * Login URL Prioritized.
	 */

	public static final String LOGIN_URL_PRIORITIZED = "com.sun.identity.agents.config.login.url.prioritized";
	/**
	 * Login URL Probe.
	 */

	public static final String LOGIN_URL_PROBE = "com.sun.identity.agents.config.login.url.probe.enabled";

	/**
	 * Login URL Probe Timeout.
	 */
	public static final String LOGIN_URL_PROBE_TIMEOUT = "com.sun.identity.agents.config.login.url.probe.timeout";
	/**
	 * OpenAM Logout URL.
	 */

	public static final String OPENAM_LOGOUT_URL = "com.sun.identity.agents.config.logout.url";
	/**
	 * Logout URL Prioritized.
	 */

	public static final String LOGOUT_URL_PRIORITIZED = "com.sun.identity.agents.config.logout.url.prioritized";
	/**
	 * Logout URL Probe.
	 */

	public static final String LOGOUT_URL_PROBE = "com.sun.identity.agents.config.logout.url.probe.enabled";
	/**
	 * Logout URL Probe Timeout.
	 */

	public static final String LOGOUT_URL_PROBE_TIMEOUT = "com.sun.identity.agents.config.logout.url.probe.timeout";
	/**
	 * OpenAM Authentication Service Protocol.
	 */

	public static final String OPENAM_AUTHENTICATION_SERVICE_PROTOCOL = "com.iplanet.am.server.protocol";
	/**
	 * OpenAM Authentication Service Host Name.
	 */

	public static final String OPENAM_AUTHENTICATION_SERVICE_HOST_NAME = "com.iplanet.am.server.host";
	/**
	 * OpenAM Authentication Service Port.
	 */

	public static final String OPENAM_AUTHENTICATION_SERVICE_PORT = "com.iplanet.am.server.port";
	/**
	 * Enable Policy Notifications.
	 */

	public static final String ENABLE_POLICY_NOTIFICATIONS = "com.sun.identity.agents.notification.enabled";
	/**
	 * Policy Client Polling Interval.
	 */

	public static final String POLICY_CLIENT_POLLING_INTERVAL = "com.sun.identity.agents.polling.interval";
	/**
	 * Policy Client Cache Mode.
	 */

	public static final String POLICY_CLIENT_CACHE_MODE = "com.sun.identity.policy.client.cacheMode";
	/**
	 * Policy Client Boolean Action Values.
	 */

	public static final String POLICY_CLIENT_BOOLEAN_ACTION_VALUES = "com.sun.identity.policy.client.booleanActionValues";
	/**
	 * Policy Client Resource Comparators.
	 */

	public static final String POLICY_CLIENT_RESOURCE_COMPARATORS = "com.sun.identity.policy.client.resourceComparators";
	/**
	 * Policy Client Clock Skew.
	 */

	public static final String POLICY_CLIENT_CLOCK_SKEW = "com.sun.identity.policy.client.clockSkew";
	/**
	 * URL Policy Env GET Parameters.
	 */

	public static final String URL_POLICY_ENV_GET_PARAMETERS = "com.sun.identity.agents.config.policy.env.get.param";
	/**
	 * URL Policy Env POST Parameters.
	 */

	public static final String URL_POLICY_ENV_POST_PARAMETERS = "com.sun.identity.agents.config.policy.env.post.param";
	/**
	 * URL Policy Env jsession Parameters.
	 */

	public static final String URL_POLICY_ENV_JSESSION_PARAMETERS = "com.sun.identity.agents.config.policy.env.jsession.param";
	/**
	 * Enable Notification of User Data Caches.
	 */

	public static final String ENABLE_NOTIFICATION_OF_USER_DATA_CACHES = "com.sun.identity.idm.remote.notification.enabled";
	/**
	 * User Data Cache Polling Time.
	 */

	public static final String USER_DATA_CACHE_POLLING_TIME = "com.iplanet.am.sdk.remote.pollingTime";
	/**
	 * Enable Notification of Service Data Caches.
	 */

	public static final String ENABLE_NOTIFICATION_OF_SERVICE_DATA_CACHES = "com.sun.identity.sm.notification.enabled";
	/**
	 * Service Data Cache Time.
	 */

	public static final String SERVICE_DATA_CACHE_TIME = "com.sun.identity.sm.cacheTime";
	/**
	 * Enable Client Polling.
	 */

	public static final String ENABLE_CLIENT_POLLING = "com.iplanet.am.session.client.polling.enable";
	/**
	 * Client Polling Period.
	 */

	public static final String CLIENT_POLLING_PERIOD = "com.iplanet.am.session.client.polling.period";
	/**
	 * Locale Language.
	 */

	public static final String LOCALE_LANGUAGE = "com.sun.identity.agents.config.locale.language";
	/**
	 * Locale Country.
	 */

	public static final String LOCALE_COUNTRY = "com.sun.identity.agents.config.locale.country";
	/**
	 * Port Check Enable.
	 */

	public static final String PORT_CHECK_ENABLE = "com.sun.identity.agents.config.port.check.enable";
	/**
	 * Port Check File.
	 */

	public static final String PORT_CHECK_FILE = "com.sun.identity.agents.config.port.check.file";
	/**
	 * Port Check Setting Current Values.
	 */

	public static final String PORT_CHECK_SETTING_CURRENT_VALUES = "com.sun.identity.agents.config.port.check.setting";
	/**
	 * Bypass Principal List Current Values.
	 */

	public static final String BYPASS_PRINCIPAL_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.bypass.principal";
	/**
	 * Encryption Provider.
	 */

	public static final String ENCRYPTION_PROVIDER = "com.iplanet.security.encryptor";
	/**
	 * Legacy User Agent Support Enable.
	 */

	public static final String LEGACY_USER_AGENT_SUPPORT_ENABLE = "com.sun.identity.agents.config.legacy.support.enable";
	/**
	 * Legacy User Agent List Current Values.
	 */

	public static final String LEGACY_USER_AGENT_LIST_CURRENT_VALUES = "com.sun.identity.agents.config.legacy.user.agent";
	/**
	 * Legacy User Agent Redirect URI.
	 */

	public static final String LEGACY_USER_AGENT_REDIRECT_URI = "com.sun.identity.agents.config.legacy.redirect.uri";

	/**
	 * Web Service Enable.
	 */

	public static final String WEB_SERVICE_ENABLE = "com.sun.identity.agents.config.webservice.enable";
	/**
	 * Web Service End Points Current Values.
	 */

	public static final String WEB_SERVICE_END_POINTS_CURRENT_VALUES = "com.sun.identity.agents.config.webservice.endpoint";
	/**
	 * Web Service Process GET Enable.
	 */

	public static final String WEB_SERVICE_PROCESS_GET_ENABLE = "com.sun.identity.agents.config.webservice.process.get.enable";
	/**
	 * Web Service Authenticator.
	 */

	public static final String WEB_SERVICE_AUTHENTICATOR = "com.sun.identity.agents.config.webservice.authenticator";
	/**
	 * Web Service Response Processor.
	 */

	public static final String WEB_SERVICE_RESPONSE_PROCESSOR = "com.sun.identity.agents.config.webservice.responseprocessor";
	/**
	 * Web Service Internal Error Content File.
	 */

	public static final String WEB_SERVICE_INTERNAL_ERROR_CONTENT_FILE = "com.sun.identity.agents.config.webservice.internalerror.content";
	/**
	 * Web Service Authorization Error Content File.
	 */

	public static final String WEB_SERVICE_AUTHORIZATION_ERROR_CONTENT_FILE = "com.sun.identity.agents.config.webservice.autherror.content";
	/**
	 * Alternative Agent Host Name.
	 */

	public static final String ALTERNATIVE_AGENT_HOST_NAME = "com.sun.identity.agents.config.agent.host";
	/**
	 * Alternative Agent Port Name.
	 */

	public static final String ALTERNATIVE_AGENT_PORT_NAME = "com.sun.identity.agents.config.agent.port";
	/**
	 * Alternative Agent Protocol.
	 */

	public static final String ALTERNATIVE_AGENT_PROTOCOL = "com.sun.identity.agents.config.agent.protocol";
	/**
	 * WebAuthentication Available.
	 */

	public static final String WEBAUTHENTICATION_AVAILABLE = "com.sun.identity.agents.config.jboss.webauth.available";

	/**
	 * Authentication Chain.
	 */

	public static final String AUTHENTICATION_CHAIN = "authenticationchain";
	/**
	 * Token Conversion Type.
	 */

	public static final String TOKEN_CONVERSION_TYPE = "tokenconversiontype";

	/**
	 * Detect Message Replay.
	 */

	public static final String DETECT_MESSAGE_REPLAY = "detectmessagereplay";
	/**
	 * Detect User Token Replay.
	 */

	public static final String DETECT_USER_TOKEN_REPLAY = "detectusertokenreplay";
	/**
	 * Private Key Type.
	 */

	public static final String PRIVATE_KEY_TYPE = "keyType";
	/**
	 * Liberty Service Type URN.
	 */

	public static final String LIBERTY_SERVICE_TYPE_URN = "libertyservicetype";

	/**
	 * Is Request Signature Verified.
	 */

	public static final String IS_REQUEST_SIGNATURE_VERIFIED = "isrequestsigned";
	/**
	 * Is Response Signed Enabled.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED = "isResponseSign";
	/**
	 * Is Response Signed Enabled For Body.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY = "Body";
	/**
	 * Is Response Signed Enabled For Body SecurityToken.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_SECURITYTOKEN = "SecurityToken";
	/**
	 * Is Response Signed Enabled For Body Timestamp.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_TIMESTAMP = "Timestamp";
	/**
	 * Is Response Signed Enabled For Body To.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_TO = "To";
	/**
	 * Is Response Signed Enabled For Body From.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_FROM = "From";
	/**
	 * Is Response Signed Enabled For Body ReplyTo.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_REPLYTO = "ReplyTo";
	/**
	 * Is Response Signed Enabled For Body Action.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_ACTION = "Action";
	/**
	 * Is Response Signed Enabled For Body MessageID.
	 */

	public static final String IS_RESPONSE_SIGNED_ENABLED_FOR_BODY_MESSAGEID = "MessageID";

	/**
	 * Is Request Decrypted.
	 */

	public static final String IS_REQUEST_DECRYPTED = "isrequestencrypt";

	/**
	 * Is Request Decrypted.
	 */

	public static final String IS_REQUEST_HEADER_DECRYPTED = "isrequestheaderencrypt";

	/**
	 * Is Request Decrypted For Body.
	 */

	public static final String IS_REQUEST_DECRYPTED_FOR_BODY = "isrequestencrypted";
	/**
	 * Is Request Decrypted For Header.
	 */

	public static final String IS_REQUEST_DECRYPTED_FOR_HEADER = "isRequestHeaderEncrypt";
	/**
	 * Is Response Encrypted.
	 */

	public static final String IS_RESPONSE_ENCRYPTED = "isresponsedecrypted";
	/**
	 * Encryption Algorithm.
	 */

	public static final String ENCRYPTION_ALGORITHM = "encryptionalgorithm";
	/**
	 * Encryption Strength.
	 */

	public static final String ENCRYPTION_STRENGTH = "encryptionstrength";
	/**
	 * Public Key Alias of Web Service Client.
	 */

	public static final String PUBLIC_KEY_ALIAS_OF_WEB_SERVICE_CLIENT = "publicKeyAlias";

	/**
	 * Web Service Security Proxy End Point.
	 */

	public static final String WEB_SERVICE_SECURITY_PROXY_END_POINT = "wspproxyendpoint";
	/**
	 * Web Service End Point.
	 */

	public static final String WEB_SERVICE_END_POINT = "wspendpoint";

	/**
	 * Kerberos Key Tab File.
	 */

	public static final String KERBEROS_KEY_TAB_FILE = "kerberoskeytabfile";
	/**
	 * Verify Kerberos Signature.
	 */

	public static final String VERIFY_KERBEROS_SIGNATURE = "isverifykrbsignature";

	/**
	 * Lists the elements selected from the checkbox group that is under the
	 * "Is Response Signed Enabled".
	 */

	public static final String SIGNED_ELEMENTS = "signedelements";

	/**
	 * Discovery Configuration.
	 */

	public static final String DISCOVERY_CONFIGURATION = "discovery";
	/**
	 * User Authentication Required.
	 */

	public static final String USER_AUTHENTICATION_REQUIRED = "foruserauthn";
	/**
	 * Use Pass Through Security Token.
	 */

	public static final String USE_PASS_THROUGH_SECURITY_TOKEN = "ispassthroughsecuritytoken";

	/**
	 * WS Trust Version.
	 */

	public static final String WS_TRUST_VERSION = "wstrustversion";
	/**
	 * Security Mechanism.
	 */

	public static final String SECURITY_MECHANISM = "SecurityMech";
	/**
	 * STS Configuration.
	 */

	public static final String STS_CONFIGURATION = "sts";
	/**
	 * Preserve Security Headers in Message.
	 */

	public static final String PRESERVE_SECURITY_HEADERS_IN_MESSAGE = "preservesecurityheader";
	/**
	 * Credential for User Token Name.
	 */

	public static final String CREDENTIAL_FOR_USER_TOKEN_NAME = "usernametokenname";
	/**
	 * Credential for User Token Password.
	 */

	public static final String CREDENTIAL_FOR_USER_TOKEN_PASSWORD = "usernametokenpassword";
	/**
	 * Requested Key Type.
	 */

	public static final String REQUESTED_KEY_TYPE = "KeyType";
	/**
	 * Requested Claims Current Values.
	 */

	public static final String REQUESTED_CLAIMS_CURRENT_VALUES = "RequestedClaims";
	/**
	 * DNS Claim.
	 */

	public static final String DNS_CLAIM = "DnsClaim";
	/**
	 * SAML Attribute Mapping Current Values.
	 */

	public static final String SAML_ATTRIBUTE_MAPPING_CURRENT_VALUES = "SAMLAttributeMapping";
	/**
	 * SAML NameID Mapper Plugin.
	 */

	public static final String SAML_NAMEID_MAPPER_PLUGIN = "NameIDMapper";
	/**
	 * SAML Attributes Namespace.
	 */

	public static final String SAML_ATTRIBUTES_NAMESPACE = "AttributeNamespace";
	/**
	 * Include Memberships.
	 */

	public static final String INCLUDE_MEMBERSHIPS = "includeMemberships";
	/**
	 * Is Response Signature Verified.
	 */

	public static final String IS_RESPONSE_SIGNATURE_VERIFIED = "isresponsesigned";
	/**
	 * Is Request Signed Enabled.
	 */

	public static final String IS_REQUEST_SIGNED_ENABLED = "isrequestsigned";
	/**
	 * Signing Reference Type.
	 */

	public static final String SIGNING_REFERENCE_TYPE = "signingreftype";
	/**
	 * Is Request Encryption Enabled.
	 */

	public static final String IS_REQUEST_ENCRYPTION_ENABLED = "isRequestEncryptedEnabled";
	/**
	 * Is Response Decrypted.
	 */

	public static final String IS_RESPONSE_DECRYPTED = "isresponsedecrypted";
	/**
	 * Encryption Algorithm Type.
	 */

	public static final String ENCRYPTION_ALGORITHM_TYPE = "encryptionalgorithm";
	/**
	 * Encryption Algorithm Strength.
	 */

	public static final String ENCRYPTION_ALGORITHM_STRENGTH = "encryptionstrength";
	/**
	 * Public Key Alias of Web Service Provider.
	 */

	public static final String PUBLIC_KEY_ALIAS_OF_WEB_SERVICE_PROVIDER = "publicKeyAlias";
	/**
	 * Private Key Alias.
	 */

	public static final String PRIVATE_KEY_ALIAS = "certalias";
	/**
	 * Key Store Usage.
	 */

	public static final String KEY_STORE_USAGE = "keystoreusage";
	/**
	 * Location of Key Store.
	 */

	public static final String LOCATION_OF_KEY_STORE = "keystorelocation";
	/**
	 * Password of Key Store.
	 */

	public static final String PASSWORD_OF_KEY_STORE = "keystorepassword";
	/**
	 * Password of Key.
	 */

	public static final String PASSWORD_OF_KEY = "keypassword";
	/**
	 * Security Token Service End Point.
	 */

	public static final String SECURITY_TOKEN_SERVICE_END_POINT = "securitytokenendpoint";
	/**
	 * Security Token Service MEX End Point.
	 */

	public static final String SECURITY_TOKEN_SERVICE_MEX_END_POINT = "securitytokenmetadataendpoint";
	/**
	 * Kerberos Domain Server.
	 */

	public static final String KERBEROS_DOMAIN_SERVER = "kerberosdomainserver";
	/**
	 * Kerberos Domain.
	 */

	public static final String KERBEROS_DOMAIN = "kerberosdomain";
	/**
	 * Kerberos Service Principal.
	 */

	public static final String KERBEROS_SERVICE_PRINCIPAL = "kerberosserviceprincipal";
	/**
	 * Kerberos Ticket Cache Directory.
	 */

	public static final String KERBEROS_TICKET_CACHE_DIRECTORY = "kerberosticketcachedir";
	/**
	 * Active.
	 */

	public static final String ACTIVE = "sunIdentityServerDeviceStatus";
	/**
	 * Discovery Service End Point.
	 */

	public static final String DISCOVERY_SERVICE_END_POINT = "DiscoveryEndpoint";
	/**
	 * Authentication Web Service End Point.
	 */

	public static final String AUTHENTICATION_WEB_SERVICE_END_POINT = "AuthNServiceEndpoint";
	/**
	 * Description.
	 */

	public static final String DESCRIPTION = "description";
	/**
	 * Agent Key Values Current Values.
	 */

	public static final String AGENT_KEY_VALUES_CURRENT_VALUES = "sunIdentityServerDeviceKeyValue";
	/**
	 * Agent Profiles allowed to Read Current Values.
	 */

	public static final String AGENT_PROFILES_ALLOWED_TO_READ_CURRENT_VALUES = "AgentsAllowedToRead.AvailableListBox";

	/**
	 * Agent Type.
	 */
	public static final String AGENT_TYPE = "AgentType";
}
