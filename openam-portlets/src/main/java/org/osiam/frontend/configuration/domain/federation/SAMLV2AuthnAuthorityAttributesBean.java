/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * Encapsulates data for a SAMLV2 Authn Authoritity bean.
 */
public class SAMLV2AuthnAuthorityAttributesBean extends EntityProviderDescriptor {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2AuthnAuthorityAttributesBean.class);

	private String authnQueryServiceURL;
	private String assertionIDRequestSoapLocation;
	private String assertionIDRequestURILocation;
	private String assertionIDRequestURIMapper;

	public String getAuthnQueryServiceURL() {
		return authnQueryServiceURL;
	}

	public void setAuthnQueryServiceURL(String authnQueryServiceURL) {
		this.authnQueryServiceURL = authnQueryServiceURL;
	}

	public String getAssertionIDRequestSoapLocation() {
		return assertionIDRequestSoapLocation;
	}

	public void setAssertionIDRequestSoapLocation(String assertionIDRequestSoapLocation) {
		this.assertionIDRequestSoapLocation = assertionIDRequestSoapLocation;
	}

	public String getAssertionIDRequestURILocation() {
		return assertionIDRequestURILocation;
	}

	public void setAssertionIDRequestURILocation(String assertionIDRequestURILocation) {
		this.assertionIDRequestURILocation = assertionIDRequestURILocation;
	}

	public String getAssertionIDRequestURIMapper() {
		return assertionIDRequestURIMapper;
	}

	public void setAssertionIDRequestURIMapper(String assertionIDRequestURIMapper) {
		this.assertionIDRequestURIMapper = assertionIDRequestURIMapper;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2AuthnAuthorityAttributesBean fromMap(Map<String, Set<String>> map)
			throws UnsupportedEncodingException {

		SAMLV2AuthnAuthorityAttributesBean bean = new SAMLV2AuthnAuthorityAttributesBean();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.AUTHN_AUTORITY_CONFIG_NODE);

		Node configNode = configNodeSet.iterator().next();

		bean.setMetaAlias(XmlUtil.getNodeAttributeValue(configNode, FederationConstants.META_ALIAS_ATTRIBUTE));

		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_SIGNING)) {
				bean.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION)) {
				bean.setEncryptionCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.ASSERTIONID_REQUEST_MAPPER)) {
				bean.setAssertionIDRequestURIMapper(XmlUtil.getValueFromAttributeValue(node));
			}

		}

		Document metadataDocument = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY)));

		if (metadataDocument == null) {
			return bean;
		}
		Node rootNodeMetadata = metadataDocument.getFirstChild();

		Set<Node> descriptorNodes = XmlUtil.getChildNodes(rootNodeMetadata,
				FederationConstants.AUTHN_AUTORITY_DESCRIPTOR_NODE);
		if (descriptorNodes.size() == 0) {
			return bean;
		}

		Set<Node> manageNameIdServiceNodeSet = XmlUtil.getChildNodes(descriptorNodes.iterator().next(),
				FederationConstants.AUTHN_QUERY_SERVICE_NODE);

		for (Node node : manageNameIdServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setAuthnQueryServiceURL(XmlUtil
						.getNodeAttributeValue(node, FederationConstants.LOCATION_ATTRIBUTE));

			}
		}

		Set<Node> assertionIDRequestServiceNodeSet = XmlUtil.getChildNodes(descriptorNodes.iterator().next(),
				FederationConstants.ASSERTION_ID_REQUEST_SERVICE_NODE);

		for (Node node : assertionIDRequestServiceNodeSet) {
			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:SOAP")) {
				bean.setAssertionIDRequestSoapLocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));

			}

			if (XmlUtil.getNodeAttributeValue(node, FederationConstants.BINDING_ATTRIBUTE).equals(
					"urn:oasis:names:tc:SAML:2.0:bindings:URI")) {
				bean.setAssertionIDRequestURILocation(XmlUtil.getNodeAttributeValue(node,
						FederationConstants.LOCATION_ATTRIBUTE));

			}

		}

		// bean.setNameIDFormatCurrentValues(nameIdList);

		return bean;

	}

	/**
	 * SAMLV2AuthnAuthorityAttributesBean constructor.
	 */
	public SAMLV2AuthnAuthorityAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_AUTHENTICATION_AUTHORITY);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put(FederationConstants.META_ALIAS_ATTRIBUTE, getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				FederationConstants.AUTHN_AUTORITY_CONFIG_NODE, queryAttributes);
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.ASSERTIONID_REQUEST_MAPPER,
				getAssertionIDRequestURIMapper());

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);
		Map<String, String> descriptorAttributes = new HashMap<String, String>();
		descriptorAttributes.put("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol");
		Node attributeQueryDescriptorNode = XmlUtil.createElement(metadataDocument.getFirstChild(),
				FederationConstants.AUTHN_AUTORITY_DESCRIPTOR_NODE, descriptorAttributes);
		
		// Start Key Descriptor Signing

		if (getSigningCertificateAlias() != null && !"".equals(getSigningCertificateAlias())) {

			Map<String, String> signingAttributes = new HashMap<String, String>();
			signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
			Node signingNode = XmlUtil.createElement(attributeQueryDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, signingAttributes);

			Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getSigningCertificateAlias());
			String base64SigningCertificate = "";
			if (cert != null) {
				base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);
			}

			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64SigningCertificate, null);
			x509CertificateNode.setPrefix("ds");

		}
		// Start Key Descriptor Encryption

		if (getEncryptionCertificateAlias() != null && !"".equals(getEncryptionCertificateAlias())) {

			Map<String, String> encryptionAttributes = new HashMap<String, String>();
			encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
			Node encryptionNode = XmlUtil.createElement(attributeQueryDescriptorNode,
					FederationConstants.KEY_DESCRIPTOR_NODE, encryptionAttributes);

			Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			keyInfo.setPrefix("ds");

			Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
					"http://www.w3.org/2000/09/xmldsig#", null);
			x509Node.setPrefix("ds");

			X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(getEncryptionCertificateAlias());
			String algorithm = "";
			String base64EncryptionCertificate = "";
			if (cert != null) {
				base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);
				algorithm = cert.getSigAlgName();
			}
			Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
					"http://www.w3.org/2000/09/xmldsig#", base64EncryptionCertificate, null);
			x509CertificateNode.setPrefix("ds");
			// algorithm=XMLCipher.AES_128;
			Long keySize = new Long(128);

			Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
			encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
			Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE,
					null, encryptionMethodAttributes);

			Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
					"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
			keySizeNode.setPrefix("xenc");

		}
		

		XmlUtil.createBindingLocationElement(attributeQueryDescriptorNode,
				FederationConstants.AUTHN_QUERY_SERVICE_NODE, "urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				getAuthnQueryServiceURL());

		XmlUtil.createBindingLocationElement(attributeQueryDescriptorNode,
				FederationConstants.ASSERTION_ID_REQUEST_SERVICE_NODE, "urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
				getAssertionIDRequestSoapLocation());

		XmlUtil.createBindingLocationElement(attributeQueryDescriptorNode,
				FederationConstants.ASSERTION_ID_REQUEST_SERVICE_NODE, "urn:oasis:names:tc:SAML:2.0:bindings:URI",
				getAssertionIDRequestURILocation());

	}

}
