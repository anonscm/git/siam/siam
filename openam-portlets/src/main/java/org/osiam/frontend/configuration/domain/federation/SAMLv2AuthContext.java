/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import java.io.Serializable;

/**
 * 
 * 
 * Encapsulates data for a SAMLV2 Auth Contenxt Bean.
 * 
 */
public class SAMLv2AuthContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private boolean supported;
	private String level;
	private boolean isDefault;

	/**
	 * Creates new {@link SAMLv2AuthContext} instance.
	 * 
	 * @param name
	 *            - the name.
	 */
	public SAMLv2AuthContext(String name) {
		this.name = name;
	}

	/**
	 * Creates new empty {@link SAMLv2AuthContext} instance.
	 */
	public SAMLv2AuthContext() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getSupported() {
		return supported;
	}

	public void setSupported(boolean supported) {
		this.supported = supported;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault1) {
		this.isDefault = isDefault1;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append(name);
		builder.append("|");
		builder.append((level == null || "".equals(level)) ? 0 : level);
		builder.append("|");
		if (isDefault) {
			builder.append("default");
		}

		return builder.toString();
	}

	/**
	 * 
	 * @param s
	 *            - String
	 * @return SAMLv2AuthContext
	 */
	public static SAMLv2AuthContext fromString(String s) {

		String[] split = s.split("\\|");

		SAMLv2AuthContext context = new SAMLv2AuthContext();

		context.setName(split[0]);
		if (split.length > 1 && split[1] != null && split[1].length() != 0) {
			context.setLevel(split[1]);
		}
		context.setSupported(true);

		if (split.length > 2 && split[2] != null && split[2].length() != 0) {

			context.setDefault(true);
		}
		return context;

	}

}
