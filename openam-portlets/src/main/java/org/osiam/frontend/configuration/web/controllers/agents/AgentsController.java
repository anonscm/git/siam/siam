/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Agents portlet controller.
 * 
 */
@Controller("AgentsController")
@RequestMapping(value = "view", params = "ctx=AgentsController")
public class AgentsController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgentsController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show existing Agent list and the insert new Agent form. For each item of
	 * the list are available a deleting action and a link to edit Agent.
	 * 
	 * @param checkAllAgents
	 *            - use to decide if all the Agent are checked for deletion
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String view(String checkAllAgents, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		model.addAttribute("agentList",
				agentsService.getAgentsList(getSSOToken(realm, request), realm, Integer.MAX_VALUE));

		model.addAttribute("agentTypeList", agentsService.getAgentTypesList());
		model.addAttribute("checkAllAgents", checkAllAgents);

		return "agents/view";
	}

	/**
	 * Delete Agent.
	 * 
	 * @param deleteAgentList
	 *            - Agent list to delete
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAgent")
	public void doDeleteAgent(String[] deleteAgentList, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);

		if (deleteAgentList != null) {
			for (int i = 0; i < deleteAgentList.length; i++) {
				agentsService.deleteAgent(getSSOToken(realm, request), realm, deleteAgentList[i]);
			}
		}
	}

	/**
	 * Redirect to add new Agent form.
	 * 
	 * @param entity
	 *            - entity to insert
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "action=doAddAgent")
	public void doAddAgent(@ModelAttribute("entity") @Valid BaseEntity entity, BindingResult result,
			ActionRequest request, ActionResponse response) {

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		response.setRenderParameter("name", entity.getName());
		response.setRenderParameter("type", entity.getType());
		response.setRenderParameter("ctx", "AddAgentController");
	}

	/**
	 * Set value for "checkAllAgents".
	 * 
	 * @param checkAllAgents
	 *            - use to decide if all the Agent are checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(String checkAllAgents, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("checkAllAgents", checkAllAgents);
	}

}
