/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2AuthnAuthorityAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * Authentication Authority
 * 
 * Samlv2AuthenticationAuthorityController shows the edit SAMLv2 Authentication
 * Authority Attributes form and does request processing of the edit SAMLv2
 * Authentication Authority Attributes action.
 * 
 */
@Controller("Samlv2AuthenticationAuthorityController")
@RequestMapping(value = "view", params = { "ctx=samlv2authentication_authorityController" })
public class Samlv2AuthenticationAuthorityController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2AuthenticationAuthorityController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("entityProviderDescriptorValidator")
	private Validator entityProviderDescriptorValidator;

	/**
	 * Shows edit Authentication Authority Attributes for current
	 * {@link EntityProvider} form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute("authnAuthority")) {

			EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

			SAMLV2AuthnAuthorityAttributesBean authnAuthority = (SAMLV2AuthnAuthorityAttributesBean) entityProvider
					.getDescriptorMap().get(serviceType);

			model.addAttribute("authnAuthority", authnAuthority);
		}

		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/authnAuth/edit";
	}

	/**
	 * Save Authentication Authority Attributes for current
	 * {@link EntityProvider}.
	 * 
	 * @param authnAuthority
	 *            - {@link SAMLV2AuthnAuthorityAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("authnAuthority") @Valid SAMLV2AuthnAuthorityAttributesBean authnAuthority,
			BindingResult result, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		entityProviderDescriptorValidator.validate(new ObjectForValidate(realm, token, authnAuthority), result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, authnAuthority);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset Authentication Authority Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
