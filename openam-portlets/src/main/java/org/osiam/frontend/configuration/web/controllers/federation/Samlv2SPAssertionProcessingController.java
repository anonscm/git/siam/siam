/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPAssertionProcessing;
import org.osiam.frontend.configuration.domain.federation.SAMLV2SPEntityProviderService;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * SP Assertion Processing
 * 
 * Samlv2SPAssertionProcessingController shows the edit SAMLv2 SP Assertion
 * Processing Attributes form and does request processing of the edit SAMLv2 SP
 * Assertion Processing Content Attributes action.
 */
@Controller("Samlv2SPAssertionProcessingController")
@RequestMapping(value = "view", params = { "ctx=Samlv2SPAssertionProcessingController" })
public class Samlv2SPAssertionProcessingController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2SPAssertionProcessingController.class);

	@Autowired
	private FederationService federationService;

	/**
	 * Shows edit SP Assertion Processing for current {@link EntityProvider}
	 * form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("spAssertionProcessing")) {
			model.addAttribute("spAssertionProcessing", spEntityProvider.getAssertionProcessing());
		}

		model.addAttribute("metaAlias", spEntityProvider.getMetaAlias());
		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/sp/assertionProcessing/edit";
	}

	/**
	 * Add value to AttributeMapCurrentValues list.
	 * 
	 * @param spAssertionProcessing
	 *            - SAMLV2SPAssertionProcessing
	 * @param result
	 *            - BindingResult
	 * @param attributeMapCurrentValuesAddValue
	 *            - value to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAttributeMapCurrentValues")
	public void doAddAttributeMapCurrentValues(
			@ModelAttribute("spAssertionProcessing") @Valid SAMLV2SPAssertionProcessing spAssertionProcessing,
			BindingResult result, String attributeMapCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesAddValue != null && !attributeMapCurrentValuesAddValue.equals("")) {
			spAssertionProcessing.getAttributeMapCurrentValues().add(attributeMapCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from AttributeMapCurrentValues list.
	 * 
	 * @param spAssertionProcessing
	 *            - SAMLV2SPAssertionProcessing
	 * @param attributeMapCurrentValuesDeleteValues
	 *            - values to de deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAttributeMapCurrentValues")
	public void doDeleteAttributeMapCurrentValues(
			@ModelAttribute("spAssertionProcessing") @Valid SAMLV2SPAssertionProcessing spAssertionProcessing,
			String[] attributeMapCurrentValuesDeleteValues, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (attributeMapCurrentValuesDeleteValues != null) {
			for (int i = 0; i < attributeMapCurrentValuesDeleteValues.length; i++) {
				spAssertionProcessing.getAttributeMapCurrentValues().remove(attributeMapCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Add value to AdapterEnvironmentCurrentValues list.
	 * 
	 * @param spAssertionProcessing
	 *            - SAMLV2SPAssertionProcessing
	 * @param result
	 *            - BindingResult
	 * @param adapterEnvironmentCurrentValuesAddValue
	 *            - values to be added
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAdapterEnvironmentCurrentValues")
	public void doAddAdapterEnvironmentCurrentValues(
			@ModelAttribute("spAssertionProcessing") @Valid SAMLV2SPAssertionProcessing spAssertionProcessing,
			BindingResult result, String adapterEnvironmentCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (adapterEnvironmentCurrentValuesAddValue != null && !adapterEnvironmentCurrentValuesAddValue.equals("")) {
			spAssertionProcessing.getAdapterEnvironmentCurrentValues().add(adapterEnvironmentCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value from AdapterEnvironmentCurrentValues list.
	 * 
	 * @param spAssertionProcessing
	 *            - SAMLV2SPAssertionProcessing
	 * @param adapterEnvironmentCurrentValuesDeleteValues
	 *            - value to be deleted
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteAdapterEnvironmentCurrentValues")
	public void doDeleteAdapterEnvironmentCurrentValues(
			@ModelAttribute("spAssertionProcessing") @Valid SAMLV2SPAssertionProcessing spAssertionProcessing,
			String[] adapterEnvironmentCurrentValuesDeleteValues, String entityIdentifier, String serviceType,
			ActionRequest request, ActionResponse response) {

		if (adapterEnvironmentCurrentValuesDeleteValues != null) {
			for (int i = 0; i < adapterEnvironmentCurrentValuesDeleteValues.length; i++) {
				spAssertionProcessing.getAdapterEnvironmentCurrentValues().remove(
						adapterEnvironmentCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save SAMLV2SPEntityProviderService.
	 * 
	 * @param spAssertionProcessing
	 *            - SAMLV2SPEntityProviderService to be save
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(
			@ModelAttribute("spAssertionProcessing") @Valid SAMLV2SPAssertionProcessing spAssertionProcessing,
			BindingResult result, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2SPEntityProviderService spEntityProvider = (SAMLV2SPEntityProviderService) entityProvider
				.getDescriptorMap().get(serviceType);

		spEntityProvider.setAssertionProcessing(spAssertionProcessing);
		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, spEntityProvider);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset form.
	 * 
	 * @param entityIdentifier
	 *            - EntityProvider entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}
}
