/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * This class encapsulates common data for all data store types.
 * 
 */
public class BaseDataStore {

	@NotNull
	@Size(min = 1)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	protected Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		return map;
	}

	/**
	 * sets the name field in the given bean, based on the given map.
	 * 
	 * @param map
	 *            - the attributes map
	 * @param baseDataStore
	 *            - the {@link DataStore} on which the name is set.
	 */
	protected static void setBaseFields(Map<String, Set<String>> map, BaseDataStore baseDataStore) {

	}
}
