/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EEApplicationBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentApplicationController shows the edit "J2EE Agent" (Application
 * attributes) form and does request processing of the edit j2ee agent action.
 * 
 */
@Controller("J2EEAgentApplicationController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentApplicationController" })
public class J2EEAgentApplicationController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentApplicationController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "J2EE Agent" - application attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {
			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EEApplicationBean agent = agentsService.getAttributesForJ2EEApplication(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}
		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/j2ee/application/edit";

	}

	/**
	 * Save new value in Login From URI list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param loginFromUriAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLoginFromUri")
	public void doAddLoginFromUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String loginFromUriAddValue, ActionRequest request, ActionResponse response) {

		if (loginFromUriAddValue != null && !loginFromUriAddValue.equals("")) {
			j2eeAgent.getLoginFromUri().add(loginFromUriAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Login From URI list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param loginFromUriDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLoginFromUri")
	public void doDeleteLoginFromUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] loginFromUriDeleteValues, ActionRequest request, ActionResponse response) {

		if (loginFromUriDeleteValues != null) {
			for (int i = 0; i < loginFromUriDeleteValues.length; i++) {
				j2eeAgent.getLoginFromUri().remove(loginFromUriDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Login Error URI list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param loginErrorUriAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLoginErrorUri")
	public void doAddLoginErrorUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String loginErrorUriAddValue, ActionRequest request, ActionResponse response) {

		if (loginErrorUriAddValue != null && !loginErrorUriAddValue.equals("")) {
			j2eeAgent.getLoginErrorUri().add(loginErrorUriAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Login Error URI list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param loginErrorUriDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLoginErrorUri")
	public void doDeleteLoginErrorUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] loginErrorUriDeleteValues, ActionRequest request, ActionResponse response) {

		if (loginErrorUriDeleteValues != null) {
			for (int i = 0; i < loginErrorUriDeleteValues.length; i++) {
				j2eeAgent.getLoginErrorUri().remove(loginErrorUriDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Application Logout Handler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param applicationLogoutHandlerKey
	 *            - {@link String} key to be insert
	 * @param applicationLogoutHandlerValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addApplicationLogoutHandler")
	public void doAddApplicationLogoutHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String applicationLogoutHandlerKey, String applicationLogoutHandlerValue,
			ActionRequest request, ActionResponse response) {

		if (applicationLogoutHandlerKey != null && !applicationLogoutHandlerKey.equals("")
				&& applicationLogoutHandlerValue != null && !applicationLogoutHandlerValue.equals("")) {
			j2eeAgent.getApplicationLogoutHandler().add(
					"[" + applicationLogoutHandlerKey + "]=" + applicationLogoutHandlerValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Application Logout Handler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param applicationLogoutHandlerDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteApplicationLogoutHandler")
	public void doDeleteApplicationLogoutHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] applicationLogoutHandlerDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (applicationLogoutHandlerDeleteValues != null) {
			for (int i = 0; i < applicationLogoutHandlerDeleteValues.length; i++) {
				j2eeAgent.getApplicationLogoutHandler().remove(applicationLogoutHandlerDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Application Logout URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param applicationLogoutUriKey
	 *            - {@link String} key to be insert
	 * @param applicationLogoutUriValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addApplicationLogoutUri")
	public void doAddApplicationLogoutUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String applicationLogoutUriKey, String applicationLogoutUriValue,
			ActionRequest request, ActionResponse response) {

		if (applicationLogoutUriKey != null && !applicationLogoutUriKey.equals("") && applicationLogoutUriValue != null
				&& !applicationLogoutUriValue.equals("")) {
			j2eeAgent.getApplicationLogoutUri().add("[" + applicationLogoutUriKey + "]=" + applicationLogoutUriValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Application Logout URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param applicationLogoutUriDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteApplicationLogoutUri")
	public void doDeleteApplicationLogoutUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] applicationLogoutUriDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (applicationLogoutUriDeleteValues != null) {
			for (int i = 0; i < applicationLogoutUriDeleteValues.length; i++) {
				j2eeAgent.getApplicationLogoutUri().remove(applicationLogoutUriDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Logout Reguest Parameter.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutReguestParameterKey
	 *            - {@link String} key to be insert
	 * @param logoutReguestParameterValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLogoutReguestParameter")
	public void doAddLogoutReguestParameter(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String logoutReguestParameterKey, String logoutReguestParameterValue,
			ActionRequest request, ActionResponse response) {

		if (logoutReguestParameterKey != null && !logoutReguestParameterKey.equals("")
				&& logoutReguestParameterValue != null && !logoutReguestParameterValue.equals("")) {
			j2eeAgent.getLogoutReguestParameter().add(
					"[" + logoutReguestParameterKey + "]=" + logoutReguestParameterValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Logout Reguest Parameter.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutReguestParameterDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLogoutReguestParameter")
	public void doDeleteLogoutReguestParameter(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] logoutReguestParameterDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (logoutReguestParameterDeleteValues != null) {
			for (int i = 0; i < logoutReguestParameterDeleteValues.length; i++) {
				j2eeAgent.getLogoutReguestParameter().remove(logoutReguestParameterDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Logout Entry URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutEntryUriKey
	 *            - {@link String} key to be insert
	 * @param logoutEntryUriValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addLogoutEntryUri")
	public void doAddLogoutEntryUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String logoutEntryUriKey, String logoutEntryUriValue, ActionRequest request,
			ActionResponse response) {

		if (logoutEntryUriKey != null && !logoutEntryUriKey.equals("") && logoutEntryUriValue != null
				&& !logoutEntryUriValue.equals("")) {
			j2eeAgent.getLogoutEntryUri().add("[" + logoutEntryUriKey + "]=" + logoutEntryUriValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Logout Entry URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param logoutEntryUriDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteLogoutEntryUri")
	public void doDeleteLogoutEntryUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] logoutEntryUriDeleteValues, ActionRequest request, ActionResponse response) {

		if (logoutEntryUriDeleteValues != null) {
			for (int i = 0; i < logoutEntryUriDeleteValues.length; i++) {
				j2eeAgent.getLogoutEntryUri().remove(logoutEntryUriDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Response Access Denied URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAccessDeniedUriKey
	 *            - {@link String} key to be insert
	 * @param responseAccessDeniedUriValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addResponseAccessDeniedUri")
	public void doAddResponseAccessDeniedUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String responseAccessDeniedUriKey, String responseAccessDeniedUriValue,
			ActionRequest request, ActionResponse response) {

		if (responseAccessDeniedUriKey != null && !responseAccessDeniedUriKey.equals("")
				&& responseAccessDeniedUriValue != null && !responseAccessDeniedUriValue.equals("")) {
			j2eeAgent.getResponseAccessDeniedUri().add(
					"[" + responseAccessDeniedUriKey + "]=" + responseAccessDeniedUriValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Response Access Denied URI.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAccessDeniedUriDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteResponseAccessDeniedUri")
	public void doDeleteResponseAccessDeniedUri(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] responseAccessDeniedUriDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (responseAccessDeniedUriDeleteValues != null) {
			for (int i = 0; i < responseAccessDeniedUriDeleteValues.length; i++) {
				j2eeAgent.getResponseAccessDeniedUri().remove(responseAccessDeniedUriDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Not Enforced URIs list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedUrisAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNotEnforcedUris")
	public void doAddNotEnforcedUris(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String notEnforcedUrisAddValue, ActionRequest request, ActionResponse response) {

		if (notEnforcedUrisAddValue != null && !notEnforcedUrisAddValue.equals("")) {
			j2eeAgent.getNotEnforcedUris().add(notEnforcedUrisAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Not Enforced URIs list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedUrisDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNotEnforcedUris")
	public void doDeleteNotEnforcedUris(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] notEnforcedUrisDeleteValues, ActionRequest request, ActionResponse response) {

		if (notEnforcedUrisDeleteValues != null) {
			for (int i = 0; i < notEnforcedUrisDeleteValues.length; i++) {
				j2eeAgent.getNotEnforcedUris().remove(notEnforcedUrisDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Not Enforced Client IP List list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedClientIpListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNotEnforcedClientIpList")
	public void doAddNotEnforcedClientIpList(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String notEnforcedClientIpListAddValue, ActionRequest request, ActionResponse response) {

		if (notEnforcedClientIpListAddValue != null && !notEnforcedClientIpListAddValue.equals("")) {
			j2eeAgent.getNotEnforcedClientIpList().add(notEnforcedClientIpListAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Not Enforced Client IP List list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param notEnforcedClientIpListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNotEnforcedClientIpList")
	public void doDeleteNotEnforcedClientIpList(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] notEnforcedClientIpListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (notEnforcedClientIpListDeleteValues != null) {
			for (int i = 0; i < notEnforcedClientIpListDeleteValues.length; i++) {
				j2eeAgent.getNotEnforcedClientIpList().remove(notEnforcedClientIpListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Profile Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param profileAttributeMappingKey
	 *            - {@link String} key to be insert
	 * @param profileAttributeMappingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addProfileAttributeMapping")
	public void doAddProfileAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String profileAttributeMappingKey, String profileAttributeMappingValue,
			ActionRequest request, ActionResponse response) {

		if (profileAttributeMappingKey != null && !profileAttributeMappingKey.equals("")
				&& profileAttributeMappingValue != null && !profileAttributeMappingValue.equals("")) {
			j2eeAgent.getProfileAttributeMapping().add(
					"[" + profileAttributeMappingKey + "]=" + profileAttributeMappingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Profile Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param profileAttributeMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteProfileAttributeMapping")
	public void doDeleteProfileAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] profileAttributeMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (profileAttributeMappingDeleteValues != null) {
			for (int i = 0; i < profileAttributeMappingDeleteValues.length; i++) {
				j2eeAgent.getProfileAttributeMapping().remove(profileAttributeMappingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Response Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAttributeMappingKey
	 *            - {@link String} key to be insert
	 * @param responseAttributeMappingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addResponseAttributeMapping")
	public void doAddResponseAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String responseAttributeMappingKey, String responseAttributeMappingValue,
			ActionRequest request, ActionResponse response) {

		if (responseAttributeMappingKey != null && !responseAttributeMappingKey.equals("")
				&& responseAttributeMappingValue != null && !responseAttributeMappingValue.equals("")) {
			j2eeAgent.getResponseAttributeMapping().add(
					"[" + responseAttributeMappingKey + "]=" + responseAttributeMappingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Response Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param responseAttributeMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteResponseAttributeMapping")
	public void doDeleteResponseAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] responseAttributeMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (responseAttributeMappingDeleteValues != null) {
			for (int i = 0; i < responseAttributeMappingDeleteValues.length; i++) {
				j2eeAgent.getResponseAttributeMapping().remove(responseAttributeMappingDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Session Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param sessionAttributeMappingKey
	 *            - {@link String} key to be insert
	 * @param sessionAttributeMappingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSessionAttributeMapping")
	public void doAddSessionAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String sessionAttributeMappingKey, String sessionAttributeMappingValue,
			ActionRequest request, ActionResponse response) {

		if (sessionAttributeMappingKey != null && !sessionAttributeMappingKey.equals("")
				&& sessionAttributeMappingValue != null && !sessionAttributeMappingValue.equals("")) {
			j2eeAgent.getSessionAttributeMapping().add(
					"[" + sessionAttributeMappingKey + "]=" + sessionAttributeMappingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Session Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param sessionAttributeMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSessionAttributeMapping")
	public void doDeleteSessionAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] sessionAttributeMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (sessionAttributeMappingDeleteValues != null) {
			for (int i = 0; i < sessionAttributeMappingDeleteValues.length; i++) {
				j2eeAgent.getSessionAttributeMapping().remove(sessionAttributeMappingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Default Privileged Attribute list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param defaultPrivilegedAttributeAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addDefaultPrivilegedAttribute")
	public void doAddDefaultPrivilegedAttribute(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String defaultPrivilegedAttributeAddValue, ActionRequest request,
			ActionResponse response) {

		if (defaultPrivilegedAttributeAddValue != null && !defaultPrivilegedAttributeAddValue.equals("")) {
			j2eeAgent.getDefaultPrivilegedAttribute().add(defaultPrivilegedAttributeAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Default Privileged Attribute list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param defaultPrivilegedAttributeDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteDefaultPrivilegedAttribute")
	public void doDeleteDefaultPrivilegedAttribute(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] defaultPrivilegedAttributeDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (defaultPrivilegedAttributeDeleteValues != null) {
			for (int i = 0; i < defaultPrivilegedAttributeDeleteValues.length; i++) {
				j2eeAgent.getDefaultPrivilegedAttribute().remove(defaultPrivilegedAttributeDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Privileged Attribute Type list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributeTypeAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addPrivilegedAttributeType")
	public void doAddPrivilegedAttributeType(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String privilegedAttributeTypeAddValue, ActionRequest request, ActionResponse response) {

		if (privilegedAttributeTypeAddValue != null && !privilegedAttributeTypeAddValue.equals("")) {
			j2eeAgent.getPrivilegedAttributeType().add(privilegedAttributeTypeAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Privileged Attribute Type list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributeTypeDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePrivilegedAttributeType")
	public void doDeletePrivilegedAttributeType(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] privilegedAttributeTypeDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (privilegedAttributeTypeDeleteValues != null) {
			for (int i = 0; i < privilegedAttributeTypeDeleteValues.length; i++) {
				j2eeAgent.getPrivilegedAttributeType().remove(privilegedAttributeTypeDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Privileged Attributes To Lower Case.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributesToLowerCaseKey
	 *            - {@link String} key to be insert
	 * @param privilegedAttributesToLowerCaseValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addPrivilegedAttributesToLowerCase")
	public void doAddPrivilegedAttributesToLowerCase(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String privilegedAttributesToLowerCaseKey,
			String privilegedAttributesToLowerCaseValue, ActionRequest request, ActionResponse response) {

		if (privilegedAttributesToLowerCaseKey != null && !privilegedAttributesToLowerCaseKey.equals("")
				&& privilegedAttributesToLowerCaseValue != null && !privilegedAttributesToLowerCaseValue.equals("")) {
			j2eeAgent.getPrivilegedAttributesToLowerCase().add(
					"[" + privilegedAttributesToLowerCaseKey + "]=" + privilegedAttributesToLowerCaseValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Privileged Attributes To Lower Case.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributesToLowerCaseDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePrivilegedAttributesToLowerCase")
	public void doDeletePrivilegedAttributesToLowerCase(
			@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent, BindingResult result,
			String[] privilegedAttributesToLowerCaseDeleteValues, ActionRequest request, ActionResponse response) {

		if (privilegedAttributesToLowerCaseDeleteValues != null) {
			for (int i = 0; i < privilegedAttributesToLowerCaseDeleteValues.length; i++) {
				j2eeAgent.getPrivilegedAttributesToLowerCase().remove(privilegedAttributesToLowerCaseDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Privileged Session Attribute list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedSessionAttributeAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addPrivilegedSessionAttribute")
	public void doAddPrivilegedSessionAttribute(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String privilegedSessionAttributeAddValue, ActionRequest request,
			ActionResponse response) {

		if (privilegedSessionAttributeAddValue != null && !privilegedSessionAttributeAddValue.equals("")) {
			j2eeAgent.getPrivilegedSessionAttribute().add(privilegedSessionAttributeAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Privileged Session Attribute list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedSessionAttributeDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePrivilegedSessionAttribute")
	public void doDeletePrivilegedSessionAttribute(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] privilegedSessionAttributeDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (privilegedSessionAttributeDeleteValues != null) {
			for (int i = 0; i < privilegedSessionAttributeDeleteValues.length; i++) {
				j2eeAgent.getPrivilegedSessionAttribute().remove(privilegedSessionAttributeDeleteValues[i]);
			}
		}
		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Privileged Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributeMappingKey
	 *            - {@link String} key to be insert
	 * @param privilegedAttributeMappingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addPrivilegedAttributeMapping")
	public void doAddPrivilegedAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String privilegedAttributeMappingKey, String privilegedAttributeMappingValue,
			ActionRequest request, ActionResponse response) {

		if (privilegedAttributeMappingKey != null && !privilegedAttributeMappingKey.equals("")
				&& privilegedAttributeMappingValue != null && !privilegedAttributeMappingValue.equals("")) {
			j2eeAgent.getPrivilegedAttributeMapping().add(
					"[" + privilegedAttributeMappingKey + "]=" + privilegedAttributeMappingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Privileged Attribute Mapping.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param privilegedAttributeMappingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePrivilegedAttributeMapping")
	public void doDeletePrivilegedAttributeMapping(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] privilegedAttributeMappingDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (privilegedAttributeMappingDeleteValues != null) {
			for (int i = 0; i < privilegedAttributeMappingDeleteValues.length; i++) {
				j2eeAgent.getPrivilegedAttributeMapping().remove(privilegedAttributeMappingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Custom Authentication Processing.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customAuthenticationProcessingKey
	 *            - {@link String} key to be insert
	 * @param customAuthenticationProcessingValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCustomAuthenticationProcessing")
	public void doAddCustomAuthenticationProcessing(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String customAuthenticationProcessingKey, String customAuthenticationProcessingValue,
			ActionRequest request, ActionResponse response) {

		if (customAuthenticationProcessingKey != null && !customAuthenticationProcessingKey.equals("")
				&& customAuthenticationProcessingValue != null && !customAuthenticationProcessingValue.equals("")) {
			j2eeAgent.getCustomAuthenticationProcessing().add(
					"[" + customAuthenticationProcessingKey + "]=" + customAuthenticationProcessingValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Custom Authentication Processing.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customAuthenticationProcessingDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCustomAuthenticationProcessing")
	public void doDeleteCustomAuthenticationProcessing(
			@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent, BindingResult result,
			String[] customAuthenticationProcessingDeleteValues, ActionRequest request, ActionResponse response) {

		if (customAuthenticationProcessingDeleteValues != null) {
			for (int i = 0; i < customAuthenticationProcessingDeleteValues.length; i++) {
				j2eeAgent.getCustomAuthenticationProcessing().remove(customAuthenticationProcessingDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Custom Logout Handler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customLogoutHandlerKey
	 *            - {@link String} key to be insert
	 * @param customLogoutHandlerValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCustomLogoutHandler")
	public void doAddCustomLogoutHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String customLogoutHandlerKey, String customLogoutHandlerValue,
			ActionRequest request, ActionResponse response) {

		if (customLogoutHandlerKey != null && !customLogoutHandlerKey.equals("") && customLogoutHandlerValue != null
				&& !customLogoutHandlerValue.equals("")) {
			j2eeAgent.getCustomLogoutHandler().add("[" + customLogoutHandlerKey + "]=" + customLogoutHandlerValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Custom Logout Handler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customLogoutHandlerDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCustomLogoutHandler")
	public void doDeleteCustomLogoutHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] customLogoutHandlerDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (customLogoutHandlerDeleteValues != null) {
			for (int i = 0; i < customLogoutHandlerDeleteValues.length; i++) {
				j2eeAgent.getCustomLogoutHandler().remove(customLogoutHandlerDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Custom Verification Handler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customVerificationHandlerKey
	 *            - {@link String} key to be insert
	 * @param customVerificationHandlerValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCustomVerificationHandler")
	public void doAddCustomVerificationHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String customVerificationHandlerKey, String customVerificationHandlerValue,
			ActionRequest request, ActionResponse response) {

		if (customVerificationHandlerKey != null && !customVerificationHandlerKey.equals("")
				&& customVerificationHandlerValue != null && !customVerificationHandlerValue.equals("")) {
			j2eeAgent.getCustomVerificationHandler().add(
					"[" + customVerificationHandlerKey + "]=" + customVerificationHandlerValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from CustomVerificationHandler.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param customVerificationHandlerDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCustomVerificationHandler")
	public void doDeleteCustomVerificationHandler(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, String[] customVerificationHandlerDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (customVerificationHandlerDeleteValues != null) {
			for (int i = 0; i < customVerificationHandlerDeleteValues.length; i++) {
				j2eeAgent.getCustomVerificationHandler().remove(customVerificationHandlerDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EEApplicationBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EEApplicationBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentGlobal(@ModelAttribute("j2eeAgent") @Valid J2EEApplicationBean j2eeAgent,
			BindingResult result, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}
		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveJ2EEApplication(token, realm, j2eeAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link J2EEApplicationBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentGlobal(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);

	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}
}
