/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.AttributeUtils.addPrefix;
import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a local agent.
 */
public class LocalAgent implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1)
	private String name;
	@NotNull
	@Size(min = 1)
	private String type;
	@NotNull
	@Size(min = 1)
	private String password;
	@NotNull
	@Size(min = 1)
	private String passwordConfirm;
	@NotNull
	@Size(min = 1)
	private String status;
	private List<String> agentRootURLforCDSSO = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getAgentRootURLforCDSSO() {
		return agentRootURLforCDSSO;
	}

	public void setAgentRootURLforCDSSO(List<String> agentRootURLforCDSSO) {
		this.agentRootURLforCDSSO = agentRootURLforCDSSO;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 */
	public static LocalAgent fromMap(String name, Map<String, Set<String>> attributes) {
		LocalAgent bean = new LocalAgent();
		bean.setName(name);
		bean.setPassword(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setPasswordConfirm(stringFromSet(attributes.get(AgentAttributesConstants.PASSWORD)));
		bean.setStatus(stringFromSet(attributes.get(AgentAttributesConstants.STATUS)));
		bean.setAgentRootURLforCDSSO(removePrefix(
				listFromSet(attributes.get(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES)),
				"agentRootURL="));

		bean.setType(stringFromSet((Set<String>) attributes.get(AgentAttributesConstants.AGENT_TYPE)));

		return bean;
	}

	/**
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		map.put(AgentAttributesConstants.PASSWORD, asSet(getPassword()));
		map.put(AgentAttributesConstants.STATUS, asSet(getStatus()));
		map.put(AgentAttributesConstants.AGENT_ROOT_URL_FOR_CDSSO_CURRENT_VALUES,
				asSet(addPrefix(getAgentRootURLforCDSSO(), "agentRootURL=")));

		return map;

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentRootURLforCDSSO == null) ? 0 : agentRootURLforCDSSO.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((passwordConfirm == null) ? 0 : passwordConfirm.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LocalAgent other = (LocalAgent) obj;
		if (agentRootURLforCDSSO == null) {
			if (other.agentRootURLforCDSSO != null) {
				return false;
			}
		} else if (!agentRootURLforCDSSO.equals(other.agentRootURLforCDSSO)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (passwordConfirm == null) {
			if (other.passwordConfirm != null) {
				return false;
			}
		} else if (!passwordConfirm.equals(other.passwordConfirm)) {
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

}
