/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.datastores;

import java.util.HashSet;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.service.DataStoresService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * DataStores portlet controller.
 * 
 */
@Controller("DataStoresController")
@RequestMapping(value = "view", params = "ctx=DataStoresController")
public class DataStoresController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataStoresController.class);

	@Autowired
	private DataStoresService dataStoresService;

	/**
	 * Shows existing DataStore list and the insert new DataStore form. For each
	 * item of the list are available a deleting action and a link to edit
	 * DataStores.
	 * 
	 * @param checkAllDataStores
	 *            - use to decide if all the DataStore are checked for deletion
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String view(String checkAllDataStores, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		model.addAttribute("dataStoresList", dataStoresService.getDataStoresList(getSSOToken(realm, request), realm));
		model.addAttribute("checkAllDataStores", checkAllDataStores);

		return "datastores/view";
	}

	/**
	 * Redirect to edit form.
	 * 
	 * @param entity
	 *            - entity to insert
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "action=doCreateDataStore")
	public void doCreateDataStore(@ModelAttribute("entity") @Valid BaseEntity entity, BindingResult result,
			ActionRequest request, ActionResponse response) {

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		response.setRenderParameter("name", entity.getName());
		response.setRenderParameter("action", CommonConstants.ACTION_INSERT);
		response.setRenderParameter("ctx", entity.getType() + "Controller");
	}

	/**
	 * Delete DataStore.
	 * 
	 * @param deleteDataStoresList
	 *            - list of DataStore names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteDataStores")
	public void doDeleteDataStores(String[] deleteDataStoresList, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);

		Set<String> list = new HashSet<String>();

		if (deleteDataStoresList != null) {
			for (int i = 0; i < deleteDataStoresList.length; i++) {
				list.add(deleteDataStoresList[i]);
			}
		}
		dataStoresService.deleteDataStore(getSSOToken(realm, request), realm, list);
	}

	/**
	 * Set value for "checkAllDataStores".
	 * 
	 * @param checkAllDataStores
	 *            - use to decide if all the DataStore are checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(String checkAllDataStores, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("checkAllDataStores", checkAllDataStores);
	}
}
