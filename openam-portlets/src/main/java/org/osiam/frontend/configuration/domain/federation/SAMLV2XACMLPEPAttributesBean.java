/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.shared.encode.Base64;

/**
 * 
 * Encapsulates data for a SAMLV2 XACML PEP Attributes Bean.
 * 
 */
public class SAMLV2XACMLPEPAttributesBean extends EntityProviderDescriptor {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2XACMLPEPAttributesBean.class);

	private String protocolSupportEnumeration;

	private boolean basicAuthorizationEnabled;
	private String basicAuthorizationUser;
	private String basicAuthorizationPassword;

	private boolean authorizationDecisionResponseSigned;

	private boolean assertionEncrypted;

	// old values used to recreate KeyDescriptor tag if the new certification is
	// invalid
	private String base64SigningCertificate;
	private String base64EncryptionCertificate;
	private String algorithm;
	private String location;

	public boolean getBasicAuthorizationEnabled() {
		return basicAuthorizationEnabled;
	}

	public void setBasicAuthorizationEnabled(boolean basicAuthorizationEnabled) {
		this.basicAuthorizationEnabled = basicAuthorizationEnabled;
	}

	public String getBasicAuthorizationUser() {
		return basicAuthorizationUser;
	}

	public String getProtocolSupportEnumeration() {
		return protocolSupportEnumeration;
	}

	public void setProtocolSupportEnumeration(String protocolSupportEnumeration) {
		this.protocolSupportEnumeration = protocolSupportEnumeration;
	}

	public void setBasicAuthorizationUser(String basicAuthorizationUser) {
		this.basicAuthorizationUser = basicAuthorizationUser;
	}

	public String getBasicAuthorizationPassword() {
		return basicAuthorizationPassword;
	}

	public void setBasicAuthorizationPassword(String basicAuthorizationPassword) {
		this.basicAuthorizationPassword = basicAuthorizationPassword;
	}

	public boolean getAuthorizationDecisionResponseSigned() {
		return authorizationDecisionResponseSigned;
	}

	public void setAuthorizationDecisionResponseSigned(boolean authorizationDecisionResponseSigned) {
		this.authorizationDecisionResponseSigned = authorizationDecisionResponseSigned;
	}

	public boolean getAssertionEncrypted() {
		return assertionEncrypted;
	}

	public void setAssertionEncrypted(boolean assertionEncrypted) {
		this.assertionEncrypted = assertionEncrypted;
	}

	public String getBase64SigningCertificate() {
		return base64SigningCertificate;
	}

	public void setBase64SigningCertificate(String base64SigningCertificate) {
		this.base64SigningCertificate = base64SigningCertificate;
	}

	public String getBase64EncryptionCertificate() {
		return base64EncryptionCertificate;
	}

	public void setBase64EncryptionCertificate(String base64EncryptionCertificate) {
		this.base64EncryptionCertificate = base64EncryptionCertificate;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static EntityProviderDescriptor fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		String entityConfig = stringFromSet(map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));
		Document entityConfigDocument = XmlUtil.toDOMDocument(entityConfig);
		Node configRoot = entityConfigDocument.getFirstChild();
		Node configNode = XmlUtil.getChildNode(configRoot, FederationConstants.XACML_PEP_CONFIG_NODE);

		SAMLV2XACMLPEPAttributesBean ret = new SAMLV2XACMLPEPAttributesBean();
		ret.setType("xacml_policy_enforcement_point");
		ret.setMetaAlias(XmlUtil.getNodeAttributeValue(configNode, "metaAlias"));

		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals("signingCertAlias")) {
				ret.setSigningCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("encryptionCertAlias")) {
				ret.setEncryptionCertificateAlias(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthUser")) {
				ret.setBasicAuthorizationUser(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthPassword")) {
				ret.setBasicAuthorizationPassword(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals("basicAuthOn")) {
				ret.setBasicAuthorizationEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals("wantXACMLAuthzDecisionResponseSigned")) {
				ret.setAuthorizationDecisionResponseSigned(XmlUtil.getBooleanValueFromAttributeValue(node));
			} else if (attributeName.equals("wantAssertionEncrypted")) {
				ret.setAssertionEncrypted(XmlUtil.getBooleanValueFromAttributeValue(node));
			}
		}

		String entityMetadata = stringFromSet(map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));
		Document entityMetadataDocument = XmlUtil.toDOMDocument(entityMetadata);
		Node metadataRoot = entityMetadataDocument.getFirstChild();
		if (XmlUtil.childNodeExists(metadataRoot, "XACMLAuthzDecisionQueryDescriptor")) {
			Node metadataNode = XmlUtil.getChildNode(metadataRoot, "XACMLAuthzDecisionQueryDescriptor");
			ret.setProtocolSupportEnumeration(XmlUtil.getNodeAttributeValue(metadataNode, "protocolSupportEnumeration"));

			NodeList children = metadataNode.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				Node node = children.item(i);
				if (node.getNodeName().equalsIgnoreCase("KeyDescriptor")) {
					String attributeName = XmlUtil.getNodeAttributeValue(node, FederationConstants.USE_ATTRIBUTE);
					if (attributeName == null) {
						continue;
					}
					if (attributeName.equals(FederationConstants.SIGNINNG)) {
						if (XmlUtil.childNodeExists(node, "ds:KeyInfo") || XmlUtil.childNodeExists(node, "KeyInfo")) {
							Node info = XmlUtil.getChildNodeWithPrefix(node, "KeyInfo");
							if (XmlUtil.childNodeExists(info, "ds:X509Data")
									|| XmlUtil.childNodeExists(info, "X509Data")) {
								Node data = XmlUtil.getChildNodeWithPrefix(info, "X509Data");
								if (XmlUtil.childNodeExists(data, "ds:X509Certificate")
										|| XmlUtil.childNodeExists(data, "X509Certificate")) {
									Node certNode = XmlUtil.getChildNodeWithPrefix(data, "X509Certificate");
									ret.setBase64SigningCertificate(certNode.getTextContent());
								}
							}
						}

					} else if (attributeName.equals(FederationConstants.ENCRYPTION)) {
						if (XmlUtil.childNodeExists(node, "ds:KeyInfo") || XmlUtil.childNodeExists(node, "KeyInfo")) {
							Node info = XmlUtil.getChildNodeWithPrefix(node, "KeyInfo");
							if (XmlUtil.childNodeExists(info, "ds:X509Data")
									|| XmlUtil.childNodeExists(info, "X509Data")) {
								Node data = XmlUtil.getChildNodeWithPrefix(info, "X509Data");
								if (XmlUtil.childNodeExists(data, "ds:X509Certificate")
										|| XmlUtil.childNodeExists(data, "X509Certificate")) {
									Node certNode = XmlUtil.getChildNodeWithPrefix(data, "X509Certificate");
									ret.setBase64EncryptionCertificate(certNode.getTextContent());
								}
							}
						}

						if (XmlUtil.childNodeExists(node, "ds:EncryptionMethod")
								|| XmlUtil.childNodeExists(node, "EncryptionMethod")) {
							Node methode = XmlUtil.getChildNodeWithPrefix(node, "EncryptionMethod");
							String methodeAttributeName = XmlUtil.getNodeAttributeValue(methode, "Algorithm");
							if (methodeAttributeName != null) {
								ret.setAlgorithm(methodeAttributeName);
							}
						}
					}
				}
			}
		}

		return ret;
	}

	/**
	 * SAMLV2XACMLPEPAttributesBean constructor.
	 */
	public SAMLV2XACMLPEPAttributesBean() {
		setEntityTypeKey(EntityProviderDescriptor.SAMLV2_XACML_POLICY_ENFORCEMENT_POINT);
	}

	@Override
	public void addAllDatatoMap(Map<String, Document> map) throws CertificateEncodingException {

		Document configDocument = map.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY);

		Document metadataDocument = map.get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY);

		Map<String, String> queryAttributes = new HashMap<String, String>();
		queryAttributes.put(FederationConstants.META_ALIAS_ATTRIBUTE, getMetaAliasWithSlash());
		Node attributeQueryConfigNode = XmlUtil.createElement(configDocument.getFirstChild(),
				FederationConstants.XACML_PEP_CONFIG_NODE, queryAttributes);
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_SIGNING,
				getSigningCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, FederationConstants.CERTIFICATE_ALIASES_ENCRYPTION,
				getEncryptionCertificateAlias());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthUser", getBasicAuthorizationUser());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthPassword", getBasicAuthorizationPassword());
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "basicAuthOn",
				String.valueOf(getBasicAuthorizationEnabled()));
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "wantXACMLAuthzDecisionResponseSigned",
				String.valueOf(getAuthorizationDecisionResponseSigned()));
		XmlUtil.createAttributeValue(attributeQueryConfigNode, "wantAssertionEncrypted",
				String.valueOf(getAssertionEncrypted()));

		Node metadataRoot = metadataDocument.getFirstChild();
		queryAttributes = new HashMap<String, String>();
		queryAttributes.put("WantAssertionsSigned", "false");
		queryAttributes.put("protocolSupportEnumeration", getProtocolSupportEnumeration());
		Node attributeQueryDescriptorNode = XmlUtil.createElement(metadataRoot, "XACMLAuthzDecisionQueryDescriptor",
				queryAttributes);

		// Start Key Descriptor Signing

		if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
			createSigningKeyDeScriptor(getBase64SigningCertificate(), attributeQueryDescriptorNode);
		} else {
			if (getSigningCertificateAlias() != null && !"".equals(getSigningCertificateAlias())) {
				X509Certificate cert = KeyUtil.getKeyProviderInstance()
						.getX509Certificate(getSigningCertificateAlias());

				if (cert != null) {
					base64SigningCertificate = Base64.encode(cert.getEncoded(), 76);
					createSigningKeyDeScriptor(base64SigningCertificate, attributeQueryDescriptorNode);
				}
			}
		}

		// Start Key Descriptor Encryption

		if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
			createEncryptionKeyDeScriptor(getBase64EncryptionCertificate(), getAlgorithm(),
					attributeQueryDescriptorNode);

		} else {
			if (getEncryptionCertificateAlias() != null && !"".equals(getEncryptionCertificateAlias())) {
				X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(
						getEncryptionCertificateAlias());
				if (cert != null) {
					base64EncryptionCertificate = Base64.encode(cert.getEncoded(), 76);
					algorithm = cert.getSigAlgName();
					createEncryptionKeyDeScriptor(base64EncryptionCertificate, algorithm, attributeQueryDescriptorNode);
				}
			}
		}
	}

	private void createSigningKeyDeScriptor(String cert, Node attributeQueryDescriptorNode) {

		Map<String, String> signingAttributes = new HashMap<String, String>();
		signingAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.SIGNINNG);
		Node signingNode = XmlUtil.createElement(attributeQueryDescriptorNode, FederationConstants.KEY_DESCRIPTOR_NODE,
				signingAttributes);

		Node keyInfo = XmlUtil.createElement(signingNode, FederationConstants.KEY_INFO_NODE,
				"http://www.w3.org/2000/09/xmldsig#", null);
		keyInfo.setPrefix("ds");

		Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
				"http://www.w3.org/2000/09/xmldsig#", null);
		x509Node.setPrefix("ds");

		Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
				"http://www.w3.org/2000/09/xmldsig#", cert, null);
		x509CertificateNode.setPrefix("ds");
	}

	private void createEncryptionKeyDeScriptor(String cert, String algorithm, Node attributeQueryDescriptorNode) {

		Map<String, String> encryptionAttributes = new HashMap<String, String>();
		encryptionAttributes.put(FederationConstants.USE_ATTRIBUTE, FederationConstants.ENCRYPTION);
		Node encryptionNode = XmlUtil.createElement(attributeQueryDescriptorNode,
				FederationConstants.KEY_DESCRIPTOR_NODE, encryptionAttributes);

		Node keyInfo = XmlUtil.createElement(encryptionNode, FederationConstants.KEY_INFO_NODE,
				"http://www.w3.org/2000/09/xmldsig#", null);
		keyInfo.setPrefix("ds");

		Node x509Node = XmlUtil.createElement(keyInfo, FederationConstants.X509DATA_NODE,
				"http://www.w3.org/2000/09/xmldsig#", null);
		x509Node.setPrefix("ds");

		Node x509CertificateNode = XmlUtil.createTextElement(x509Node, FederationConstants.X509CERTIFICATE_NODE,
				"http://www.w3.org/2000/09/xmldsig#", cert, null);
		x509CertificateNode.setPrefix("ds");

		Long keySize = new Long(128);

		Map<String, String> encryptionMethodAttributes = new HashMap<String, String>();
		encryptionMethodAttributes.put(FederationConstants.ALGORITHM_ATTRIBUTE, algorithm);
		Node encryptionMethod = XmlUtil.createElement(encryptionNode, FederationConstants.ENCRYPTION_METHOD_NODE, null,
				encryptionMethodAttributes);

		Node keySizeNode = XmlUtil.createTextElement(encryptionMethod, FederationConstants.KEY_SIZE_NODE,
				"http://www.w3.org/2001/04/xmlenc#", keySize.toString(), null);
		keySizeNode.setPrefix("xenc");

	}

}
