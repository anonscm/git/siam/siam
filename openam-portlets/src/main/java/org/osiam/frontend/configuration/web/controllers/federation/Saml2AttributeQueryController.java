/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.federation;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.SAMLV2AttributeQueryAttributesBean;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * Attribute Query
 * 
 * Saml2AttributeQueryController shows the edit SAMLv2 Attribute Query
 * Attributes form and does request processing of the edit SAMLv2 Attribute
 * Query Attributes action.
 * 
 */
@Controller("Saml2AttributeQueryController")
@RequestMapping(value = "view", params = { "ctx=samlv2attribute_query_providerController" })
public class Saml2AttributeQueryController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(Samlv2AttributeAuthorityController.class);

	@Autowired
	private FederationService federationService;

	@Autowired
	@Qualifier("entityProviderDescriptorValidator")
	private Validator entityProviderDescriptorValidator;

	/**
	 * Shows edit Attribute Query Attributes for current {@link EntityProvider}
	 * form.
	 * 
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editEntityProvider(String entityIdentifier, String serviceType, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		EntityProvider entityProvider = federationService.getEntityProvidersForGivenTypeAndGivenName(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier);

		SAMLV2AttributeQueryAttributesBean attributeQuery = (SAMLV2AttributeQueryAttributesBean) entityProvider
				.getDescriptorMap().get(serviceType);

		if (!model.containsAttribute("attributeQuery")) {
			model.addAttribute("attributeQuery", attributeQuery);
		}

		model.addAttribute("entityIdentifier", entityIdentifier);
		model.addAttribute("serviceType", serviceType);

		return "federation/entityProvider/samlv2/attrQuery/edit";
	}

	/**
	 * Add value for NameIDFormatCurrentValues list.
	 * 
	 * @param attributeQuery
	 *            - {@link SAMLV2AttributeQueryAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param nameIDFormatCurrentValuesAddValue
	 *            - list whit value to be added
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addNameIDFormatCurrentValues")
	public void doAddNameIDFormatCurrentValues(
			@ModelAttribute("attributeQuery") @Valid SAMLV2AttributeQueryAttributesBean attributeQuery,
			BindingResult result, String nameIDFormatCurrentValuesAddValue, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDFormatCurrentValuesAddValue != null && !nameIDFormatCurrentValuesAddValue.equals("")) {
			attributeQuery.getNameIDFormatCurrentValues().add(nameIDFormatCurrentValuesAddValue);
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Delete value for NameIDFormatCurrentValues list.
	 * 
	 * @param attributeQuery
	 *            - {@link SAMLV2AttributeQueryAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param nameIDFormatCurrentValuesDeleteValues
	 *            - list with values to be deleted
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteNameIDFormatCurrentValues")
	public void doDeleteNameIDValueMapCurrentValues(
			@ModelAttribute("attributeQuery") @Valid SAMLV2AttributeQueryAttributesBean attributeQuery,
			BindingResult result, String[] nameIDFormatCurrentValuesDeleteValues, String entityIdentifier,
			String serviceType, ActionRequest request, ActionResponse response) {

		if (nameIDFormatCurrentValuesDeleteValues != null) {
			for (int i = 0; i < nameIDFormatCurrentValuesDeleteValues.length; i++) {
				attributeQuery.getNameIDFormatCurrentValues().remove(nameIDFormatCurrentValuesDeleteValues[i]);
			}
		}

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

	}

	/**
	 * Save Attribute Query Attributes for current {@link EntityProvider}.
	 * 
	 * @param attributeQuery
	 *            - {@link SAMLV2AttributeQueryAttributesBean}
	 * @param result
	 *            - BindingResult
	 * @param entityIdentifier
	 *            - {@link EntityProvider} entityIdentifier
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("attributeQuery") @Valid SAMLV2AttributeQueryAttributesBean attributeQuery,
			BindingResult result, String entityIdentifier, String serviceType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		entityProviderDescriptorValidator.validate(new ObjectForValidate(realm, token, attributeQuery), result);

		if (result.hasErrors()) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Invalid form");
			}
			return;
		}

		federationService.updateEntityProviderDescriptor(token, realm,
				FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, entityIdentifier, attributeQuery);

		response.setRenderParameter("protocol", FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

	/**
	 * Reset Attribute Query Attributes form.
	 * 
	 * @param entityIdentifier
	 *            - {@link SAMLV2AttributeQueryAttributesBean}
	 * @param serviceType
	 *            - service type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String entityIdentifier, String serviceType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("serviceType", serviceType);
	}

	/**
	 * Back to service list.
	 * 
	 * @param entityIdentifier
	 *            - entity provider entityIdentifier
	 * @param protocol
	 *            - entity provider protocol
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String entityIdentifier, String protocol, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("entityIdentifier", entityIdentifier);
		response.setRenderParameter("protocol", protocol);
		response.setRenderParameter("ctx", "EntityProviderEditController");

	}

}
