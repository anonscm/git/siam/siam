/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service.impl;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.common.service.impl.BaseSSOService;
import org.osiam.frontend.configuration.domain.federation.CircleOfTrust;
import org.osiam.frontend.configuration.domain.federation.EntityProvider;
import org.osiam.frontend.configuration.domain.federation.EntityProviderDescriptor;
import org.osiam.frontend.configuration.domain.federation.SAMLv2AuthContext;
import org.osiam.frontend.configuration.domain.federation.SAMLv2IDPAuthContext;
import org.osiam.frontend.configuration.exceptions.FederationException;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.configuration.service.FederationService;
import org.osiam.frontend.util.CollectionUtils;
import org.osiam.frontend.util.XmlUtil;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceConfig;
import com.sun.identity.sm.ServiceConfigManager;

/**
 * Federation service implementation. Defines methods for entity providers
 * management.
 */
@Service("FederationService")
public class FederationServiceImpl extends BaseSSOService implements FederationService {

	// private static final String IDFF = "urn:liberty:metadata";
	// private static final String SAML2_PROTOCOL =
	// "urn:oasis:names:tc:SAML:2.0:metadata";
	private static final String SAML2_ENTITY_PROVIDERS_SERVICE = "sunFMSAML2MetadataService";
	private static final String WSFED_ENTITY_PROVIDERS_SERVICE = "sunFMWSFederationMetadataService";
	private static final String CIRCLE_OF_TRUST_SERVICE = "sunFMCOTConfigService";
	private static final String COT = "cot";
	private static final String ENTITY_DESCRIPTOR = "EntityDescriptor";
	private static final byte PRIORITY = 0;

	private static final Map<String, String> AUTH_CONTEXT_REF_NAMES = new HashMap<String, String>();

	private static final String INTERNET_PROTOCOL = "urn:oasis:names:tc:SAML:2.0:ac:classes:InternetProtocol";
	private static final String INTERNET_PROTOCOL_PASSWORD = "urn:oasis:names:tc:SAML:2.0:ac:classes:InternetProtocolPassword";
	private static final String KERBEROS = "urn:oasis:names:tc:SAML:2.0:ac:classes:Kerberos";
	private static final String MOBILE_ONE_FACTORY_UNREGISTERED = "urn:oasis:names:tc:SAML:2.0:ac:classes:MobileOneFactorUnregistered";
	private static final String MOBILE_TWO_FACTOR_UNREGISTERED = "urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorUnregistered";
	private static final String MOBILE_ONE_FACTOR_CONTRACT = "urn:oasis:names:tc:SAML:2.0:ac:classes:MobileOneFactorContract";
	private static final String MOBILE_TWO_FACTOR_CONTRACT = "urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract";
	private static final String PASSWORD = "urn:oasis:names:tc:SAML:2.0:ac:classes:Password";
	private static final String PASSWORD_PROTECTED_TRANSPORT = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport";
	private static final String PREVIOUS_SESSION = "urn:oasis:names:tc:SAML:2.0:ac:classes:PreviousSession";
	private static final String X509 = "urn:oasis:names:tc:SAML:2.0:ac:classes:X509";
	private static final String PGP = "urn:oasis:names:tc:SAML:2.0:ac:classes:PGP";
	private static final String SPKI = "urn:oasis:names:tc:SAML:2.0:ac:classes:SPKI";
	private static final String XMLD_SIG = "urn:oasis:names:tc:SAML:2.0:ac:classes:XMLDSig";
	private static final String SMARTCARD = "urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard";
	private static final String SMARTCARD_PKI = "urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI";
	private static final String SOFTWARE_PKI = "urn:oasis:names:tc:SAML:2.0:ac:classes:SoftwarePKI";
	private static final String TELEPHONY = "urn:oasis:names:tc:SAML:2.0:ac:classes:Telephony";
	private static final String NOMAD_TELEPHONY = "urn:oasis:names:tc:SAML:2.0:ac:classes:NomadTelephony";
	private static final String PERSONAL_TELEPHONY = "urn:oasis:names:tc:SAML:2.0:ac:classes:PersonalTelephony";
	private static final String AUTHENTICATED_TELEPHONY = "urn:oasis:names:tc:SAML:2.0:ac:classes:AuthenticatedTelephony";
	private static final String SECURE_REMOTE_PASSWORD = "urn:oasis:names:tc:SAML:2.0:ac:classes:SecureRemotePassword";
	private static final String TLS_CLIENT = "urn:oasis:names:tc:SAML:2.0:ac:classes:TLSClient";
	private static final String TIME_SYNC_TOKEN = "urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken";
	private static final String UNSPECIFIED = "urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified";

	static {
		AUTH_CONTEXT_REF_NAMES.put(INTERNET_PROTOCOL, "InternetProtocol");
		AUTH_CONTEXT_REF_NAMES.put(INTERNET_PROTOCOL_PASSWORD, "InternetProtocolPassword");
		AUTH_CONTEXT_REF_NAMES.put(KERBEROS, "Kerberos");
		AUTH_CONTEXT_REF_NAMES.put(MOBILE_ONE_FACTORY_UNREGISTERED, "MobileOneFactorUnregistered");
		AUTH_CONTEXT_REF_NAMES.put(MOBILE_TWO_FACTOR_UNREGISTERED, "MobileTwoFactorUnregistered");
		AUTH_CONTEXT_REF_NAMES.put(MOBILE_ONE_FACTOR_CONTRACT, "MobileOneFactorContract");
		AUTH_CONTEXT_REF_NAMES.put(MOBILE_TWO_FACTOR_CONTRACT, "MobileTwoFactorContract");
		AUTH_CONTEXT_REF_NAMES.put(PASSWORD, "Password");
		AUTH_CONTEXT_REF_NAMES.put(PASSWORD_PROTECTED_TRANSPORT, "PasswordProtectedTransport");
		AUTH_CONTEXT_REF_NAMES.put(PREVIOUS_SESSION, "PreviousSession");
		AUTH_CONTEXT_REF_NAMES.put(X509, "X509");
		AUTH_CONTEXT_REF_NAMES.put(PGP, "PGP");
		AUTH_CONTEXT_REF_NAMES.put(SPKI, "SPKI");
		AUTH_CONTEXT_REF_NAMES.put(XMLD_SIG, "XMLDSig");
		AUTH_CONTEXT_REF_NAMES.put(SMARTCARD, "Smartcard");
		AUTH_CONTEXT_REF_NAMES.put(SMARTCARD_PKI, "SmartcardPKI");
		AUTH_CONTEXT_REF_NAMES.put(SOFTWARE_PKI, "SoftwarePKI");
		AUTH_CONTEXT_REF_NAMES.put(TELEPHONY, "Telephony");
		AUTH_CONTEXT_REF_NAMES.put(NOMAD_TELEPHONY, "NomadTelephony");
		AUTH_CONTEXT_REF_NAMES.put(PERSONAL_TELEPHONY, "PersonalTelephony");
		AUTH_CONTEXT_REF_NAMES.put(AUTHENTICATED_TELEPHONY, "AuthenticatedTelephony");
		AUTH_CONTEXT_REF_NAMES.put(SECURE_REMOTE_PASSWORD, "SecureRemotePassword");
		AUTH_CONTEXT_REF_NAMES.put(TLS_CLIENT, "TLSClient");
		AUTH_CONTEXT_REF_NAMES.put(TIME_SYNC_TOKEN, "TimeSyncToken");
		AUTH_CONTEXT_REF_NAMES.put(UNSPECIFIED, "unspecified");
	}

	private static final Logger LOGGER = Logger.getLogger(FederationServiceImpl.class);

	private List<String> getEntityProviderNamesForGivenType(SSOToken token, String realm, String type)
			throws SSOException, SMSException, UnsupportedEncodingException {
		String serviceType = null;
		if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(type)) {
			serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
		}
		if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(type)) {
			serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
		}
		List<String> names = new ArrayList<String>();
		ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
		if (organizationConfig != null) {
			@SuppressWarnings("unchecked")
			Set<String> namesSet = organizationConfig.getSubConfigNames();
			Iterator<String> nameIterator = namesSet.iterator();
			while (nameIterator.hasNext()) {
				names.add(nameIterator.next());
			}
		} else {
			throw new IllegalArgumentException("Realm not found.");
		}
		return names;
	}

	@Override
	public List<String> getEntityProviderNamesList(SSOToken token, String realm) throws FederationException {
		validateTokenRealmParams(token, realm);
		List<String> retList = null;
		try {
			List<String> entityProvidersSaml = getEntityProviderNamesForGivenType(token, realm,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
			List<String> entityProvidersWsfed = getEntityProviderNamesForGivenType(token, realm,
					FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL);

			retList = new ArrayList<String>(entityProvidersSaml.size() + entityProvidersWsfed.size());

			retList.addAll(entityProvidersWsfed);
			retList.addAll(entityProvidersSaml);

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("UnsupportedEncodingException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

		return retList;

	}

	private List<EntityProvider> getEntityProvidersForGivenType(SSOToken token, String realm, String type)
			throws SSOException, SMSException, UnsupportedEncodingException {
		String serviceType = null;
		if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(type)) {
			serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
		}
		if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(type)) {
			serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
		}
		List<EntityProvider> entityProviders = new ArrayList<EntityProvider>();
		ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
		if (organizationConfig != null) {
			@SuppressWarnings("unchecked")
			Set<String> namesSet = organizationConfig.getSubConfigNames();
			Iterator<String> nameIterator = namesSet.iterator();
			while (nameIterator.hasNext()) {
				String entityName = nameIterator.next();
				ServiceConfig entityConfig = organizationConfig.getSubConfig(entityName);
				@SuppressWarnings("unchecked")
				Map<String, Set<String>> attributes = entityConfig.getAttributes();
				if (attributes.containsKey(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {
					EntityProvider entityProvider = EntityProvider.fromMap(attributes);
					entityProvider.setProtocol(type);
					entityProviders.add(entityProvider);
				}
				if (attributes.containsKey(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {
					EntityProvider entityProvider = EntityProvider.fromMap(attributes);
					entityProvider.setProtocol(type);
					entityProviders.add(entityProvider);
				}
			}
		} else {
			throw new IllegalArgumentException("Realm not found.");
		}
		return entityProviders;
	}

	@Override
	public List<EntityProvider> getEntityProviderList(SSOToken token, String realm) throws FederationException {
		validateTokenRealmParams(token, realm);
		List<EntityProvider> retList = null;
		try {
			List<EntityProvider> entityProvidersSaml = getEntityProvidersForGivenType(token, realm,
					FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL);
			List<EntityProvider> entityProvidersWsfed = getEntityProvidersForGivenType(token, realm,
					FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL);

			retList = new ArrayList<EntityProvider>(entityProvidersSaml.size() + entityProvidersWsfed.size());

			retList.addAll(entityProvidersWsfed);
			retList.addAll(entityProvidersSaml);

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("UnsupportedEncodingException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

		return retList;

	}

	private void validateTokenRealmEntityProviderNameParams(SSOToken token, String realm, String agentName) {
		validateTokenRealmParams(token, realm);
		if (agentName == null) {
			throw new IllegalArgumentException(
					String.format("Param agentName cannot be null. Example : %s", "'agent1'"));
		}
	}

	private void validateTokenRealmBeanParams(SSOToken token, String realm, Object bean) {
		validateTokenRealmParams(token, realm);
		if (bean == null) {
			throw new IllegalArgumentException("Param bean cannot be null.");
		}
	}

	@Override
	public List<CircleOfTrust> getCircleOfTrustList(SSOToken token, String realm) throws FederationException {
		validateTokenRealmParams(token, realm);
		List<CircleOfTrust> circleOfTrustList = new ArrayList<CircleOfTrust>();
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				@SuppressWarnings("unchecked")
				Set<String> circleOfTrustNamesSet = organizationConfig.getSubConfigNames();
				Iterator<String> nameIterator = circleOfTrustNamesSet.iterator();
				while (nameIterator.hasNext()) {
					String circleOfTrustName = nameIterator.next();
					ServiceConfig circleOfTrustConfig = organizationConfig.getSubConfig(circleOfTrustName);
					@SuppressWarnings("unchecked")
					Map<String, Set<String>> attributes = circleOfTrustConfig.getAttributes();
					CircleOfTrust circleOfTrust = CircleOfTrust.fromMap(circleOfTrustName, attributes);
					circleOfTrust.setRealm(realm);
					circleOfTrustList.add(circleOfTrust);
				}
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
		return circleOfTrustList;
	}

	@Override
	public CircleOfTrust getCircleOfTrust(SSOToken token, String realm, String circleOfTrustName)
			throws FederationException {
		validateTokenRealmParams(token, realm);
		if (circleOfTrustName == null) {
			throw new IllegalArgumentException("Param circleOfTrustName cannot be null.");
		}
		CircleOfTrust circleOfTrust = null;
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				ServiceConfig circleOfTrustConfig = organizationConfig.getSubConfig(circleOfTrustName);
				if (circleOfTrustConfig != null) {
					@SuppressWarnings("unchecked")
					Map<String, Set<String>> attributes = circleOfTrustConfig.getAttributes();
					circleOfTrust = CircleOfTrust.fromMap(circleOfTrustName, attributes);
					circleOfTrust.setRealm(realm);
				}
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
		return circleOfTrust;
	}

	@Override
	public void addCircleOfTrust(SSOToken token, String realm, CircleOfTrust circleOfTrust) throws FederationException {
		validateTokenRealmParams(token, realm);
		if (circleOfTrust == null) {
			throw new IllegalArgumentException("Param circleOfTrust cannot be null.");
		}
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				organizationConfig.addSubConfig(circleOfTrust.getName(), COT, PRIORITY, circleOfTrust.toMap());
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public void updateCircleOfTrust(SSOToken token, String realm, CircleOfTrust newCircleOfTrust)
			throws FederationException {
		validateTokenRealmParams(token, realm);
		if (newCircleOfTrust == null) {
			throw new IllegalArgumentException("newCircleOftrust parameter cannot be null.");
		}
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				ServiceConfig circleOfTrustConfig = organizationConfig.getSubConfig(newCircleOfTrust.getName());
				circleOfTrustConfig.setAttributes(newCircleOfTrust.toMap());
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public void saveAttributesEntityProvider(SSOToken token, String realm, EntityProvider bean)
			throws FederationException {
		validateTokenRealmBeanParams(token, realm, bean);
		String serviceType = null;
		if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(bean.getProtocol())) {
			serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
		}
		if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(bean.getProtocol())) {
			serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
		}

		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
			if (organizationConfig != null) {
				ServiceConfig entityProviderConfig = organizationConfig.getSubConfig(bean.getEntityIdentifier());
				entityProviderConfig.setAttributes(bean.toMap());

				// Map<String, Set<String>> testMap = getTestMap();
				// Map<String, Set<String>> realMap = bean.toMap();

				// testMap.put(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY,
				// realMap
				// .get(FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY));

				// entityProviderConfig.setAttributes(realMap);

			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		} catch (CertificateEncodingException e) {
			LOGGER.error("CertificateEncodingException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

	}

	private ServiceConfig getOrganizationConfig(SSOToken token, String realm, String type) throws SSOException,
			SMSException {
		// TODO:Not sure whether we act for only one realm, or for all realms.
		// If we act for all realms, we need then to use adminToken.

		// @SuppressWarnings("unchecked")
		// SSOToken adminToken = (SSOToken)
		// AccessController.doPrivileged(AdminTokenAction.getInstance());
		ServiceConfigManager serviceConfigManager = new ServiceConfigManager(type, token);
		ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
		return organizationConfig;
	}

	@Override
	public void addEntityProvider(SSOToken token, String realm, EntityProvider entityProvider)
			throws FederationException {

		validateTokenRealmParams(token, realm);
		if (entityProvider == null) {
			throw new IllegalArgumentException("Param entityProvider cannot be null.");
		}
		ServiceConfig organizationConfig = null;
		try {
			if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(entityProvider.getProtocol())) {
				organizationConfig = getOrganizationConfig(token, realm, SAML2_ENTITY_PROVIDERS_SERVICE);
			}
			if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(entityProvider.getProtocol())) {
				organizationConfig = getOrganizationConfig(token, realm, WSFED_ENTITY_PROVIDERS_SERVICE);
			}

			if (organizationConfig != null) {
				Map<String, Set<String>> attributes = entityProvider.toMap();
				organizationConfig.addSubConfig(entityProvider.getEntityIdentifier(), ENTITY_DESCRIPTOR, PRIORITY,
						attributes);
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		} catch (CertificateEncodingException e) {
			LOGGER.error("CertificateEncodingException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public List<String> getEntityProviderProtocolList() {
		return Arrays.asList("samlv2", "wsfed");
	}

	@Override
	public void deleteEntityProvider(SSOToken token, String realm, String entityProviderName)
			throws FederationException {
		validateTokenRealmParams(token, realm);
		if (entityProviderName == null) {
			throw new IllegalArgumentException("Param entityProviderName cannot be null.");
		}
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, SAML2_ENTITY_PROVIDERS_SERVICE);
			if (organizationConfig != null) {
				organizationConfig.removeSubConfig(entityProviderName);
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}

			organizationConfig = getOrganizationConfig(token, realm, WSFED_ENTITY_PROVIDERS_SERVICE);
			if (organizationConfig != null) {
				organizationConfig.removeSubConfig(entityProviderName);
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public void deleteCircleOfTrust(SSOToken token, String realm, String circleOfTrustName) throws FederationException {
		validateTokenRealmParams(token, realm);
		if (circleOfTrustName == null) {
			throw new IllegalArgumentException("Param circleOfTrust cannot be null.");
		}
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				organizationConfig.removeSubConfig(circleOfTrustName);
			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

	}

	@Override
	public List<String> getAvailablelEntities(SSOToken token, String realm) throws FederationException {
		List<EntityProvider> entityProviders = getEntityProviderList(token, realm);
		List<String> availableEntitesList = new ArrayList<String>(entityProviders.size());

		for (int i = 0; i < entityProviders.size(); i++) {
			EntityProvider entityProvider = entityProviders.get(i);

			availableEntitesList
					.add(entityProvider.getEntityIdentifier()
							+ "|"
							+ (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(entityProvider.getProtocol()) ? FederationConstants.ENTITY_PROVIDER_SAML2_PROTOCOL
									: entityProvider.getProtocol()));
		}
		return availableEntitesList;

	}

	@Override
	public EntityProvider getEntityProvidersForGivenTypeAndGivenName(SSOToken token, String realm, String type,
			String name) throws FederationException {
		validateTokenRealmParams(token, realm);
		EntityProvider entityProvider = null;

		try {
			String serviceType = null;
			if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(type)) {
				serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
			}
			if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(type)) {
				serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
			}

			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
			if (organizationConfig != null) {

				ServiceConfig entityConfig = organizationConfig.getSubConfig(name);

				@SuppressWarnings("unchecked")
				Map<String, Set<String>> attributes = entityConfig.getAttributes();
				entityProvider = EntityProvider.fromMap(attributes);
				entityProvider.setProtocol(type);

			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("UnsupportedEncodingException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

		return entityProvider;

	}

	@Override
	public Map<String, String> getAuthenticationContextList() {
		return AUTH_CONTEXT_REF_NAMES;
	}

	@Override
	public List<String> getKeyType() {
		// Service, Module, User, Role, Authentication Level (authlevel),
		// Resource URL(resourceURL)
		// TODO: verify how this list is created
		return Arrays.asList("service", "module", "user", "role", "authlevel", "resourceURL");
	}

	@Override
	public List<SAMLv2AuthContext> getAuthContext(List<SAMLv2AuthContext> supportedAuthContext) {

		List<SAMLv2AuthContext> authContextList = new ArrayList<SAMLv2AuthContext>();

		Set<String> keys = getAuthenticationContextList().keySet();

		for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
			String samLv2AuthContext = iterator.next();

			boolean exist = false;

			for (Iterator<SAMLv2AuthContext> iterator2 = supportedAuthContext.iterator(); iterator2.hasNext();) {
				SAMLv2AuthContext supported = iterator2.next();
				if (supported.getName().equals(samLv2AuthContext)) {
					exist = true;
					authContextList.add(supported);
					break;
				}
			}
			if (!exist) {
				authContextList.add(new SAMLv2AuthContext(samLv2AuthContext));
			}
		}

		return authContextList;

	}

	@Override
	public List<SAMLv2IDPAuthContext> getIDPAuthContext(List<SAMLv2IDPAuthContext> supportedAuthContext) {

		List<SAMLv2IDPAuthContext> authContextList = new ArrayList<SAMLv2IDPAuthContext>();

		Set<String> keys = getAuthenticationContextList().keySet();

		for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
			String samLv2AuthContext = iterator.next();

			boolean exist = false;

			for (Iterator<SAMLv2IDPAuthContext> iterator2 = supportedAuthContext.iterator(); iterator2.hasNext();) {
				SAMLv2IDPAuthContext supported = iterator2.next();
				if (supported.getName().equals(samLv2AuthContext)) {
					exist = true;
					authContextList.add(supported);
					break;
				}
			}
			if (!exist) {
				authContextList.add(new SAMLv2IDPAuthContext(samLv2AuthContext));
			}
		}

		return authContextList;

	}

	@Override
	public Map<String, String> getSingleLogoutService() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("HTTP-REDIRECT", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect");
		map.put("POST", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST");
		map.put("SOAP", "urn:oasis:names:tc:SAML:2.0:bindings:SOAP");

		return map;

	}

	@Override
	public List<String> getSPServiceAttributes() {

		return Arrays
				.asList(FederationConstants.HTTP_REDIRECT, FederationConstants.HTTP_POST, FederationConstants.SOAP);

	}

	@Override
	public String importEntityProvider(SSOToken token, String realm, String metadataFile, String configFile)
			throws FederationException {

		String serviceType = null;
		String entityConfigKey = null;
		String metadataKey = null;

		validateTokenRealmParams(token, realm);

		try {

			Document documentMetadataFile = XmlUtil
					.toDOMDocument("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + metadataFile);

			Document documentConfigFile = XmlUtil
					.toDOMDocument("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + configFile);

			String firstNodeNameMetadataFile = documentMetadataFile.getFirstChild().getNodeName();
			String firstNodeNameConfigFile = documentConfigFile.getFirstChild().getNodeName();

			String identifier = "";

			if (FederationConstants.ENTITY_DESCRIPTOR_TAG.equals(firstNodeNameMetadataFile)) {
				serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
				entityConfigKey = FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY;
				metadataKey = FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY;

				if (!FederationConstants.ENTITY_CONFIG_TAG.equals(firstNodeNameConfigFile)) {
					throw new Exception("Invalid config file. This file must contains \"EntityConfig\" tag.");
				}

				identifier = documentMetadataFile.getFirstChild().getAttributes().getNamedItem("entityID")
						.getTextContent();

			} else {
				if ("Federation".equals(firstNodeNameMetadataFile)) {
					serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
					entityConfigKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY;
					metadataKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY;

					if (!"FederationConfig".equals(firstNodeNameConfigFile)) {
						throw new Exception("Invalid config file. This file must contains \"FederationConfig\" tag.");
					}

					identifier = documentMetadataFile.getFirstChild().getAttributes().getNamedItem("FederationID")
							.getTextContent();

				} else {
					throw new Exception(
							"Invalid metadata file. This file must contains \"EntityDescriptor\" or \"Federation\" tag.");
				}
			}

			if ("".equals(identifier)) {
				throw new Exception("Invalid metadata file.");
			}

			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
			if (organizationConfig != null) {

				Map<String, Set<String>> attributesMap = new HashMap<String, Set<String>>();

				attributesMap.put(
						metadataKey,
						CollectionUtils.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
								+ metadataFile));

				attributesMap.put(
						entityConfigKey,
						CollectionUtils.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
								+ configFile));

				// validation for data files, if not able to instantiate a bean,
				// then the import files are wrong.
				EntityProvider.fromMap(attributesMap);

				organizationConfig.addSubConfig(identifier, ENTITY_DESCRIPTOR, PRIORITY, attributesMap);
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			return e.getMessage();

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			return e.getMessage();
		} catch (Exception e) {
			LOGGER.error("Invalid XML while importing", e);
			return e.getMessage();
		}
		return null;
	}

	@Override
	public void updateEntityProviderDescriptor(SSOToken token, String realm, String type, String name,
			EntityProviderDescriptor newEntityProviderDescriptor) {
		validateTokenRealmEntityProviderNameParams(token, realm, name);
		EntityProvider entityProvider = getEntityProvidersForGivenTypeAndGivenName(token, realm, type, name);
		entityProvider.getDescriptorMap().put(newEntityProviderDescriptor.getEntityTypeKey(),
				newEntityProviderDescriptor);
		saveAttributesEntityProvider(token, realm, entityProvider);

	}

	@Override
	public boolean circleOfTrustNameAlreadyExist(SSOToken token, String realm, String name) {
		validateTokenRealmEntityProviderNameParams(token, realm, name);
		try {
			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, CIRCLE_OF_TRUST_SERVICE);
			if (organizationConfig != null) {
				ServiceConfig circleOfTrustConfig = organizationConfig.getSubConfig(name);
				return (circleOfTrustConfig != null);
			} else {
				return false;
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public boolean entityProviderNameAlreadyExist(SSOToken token, String realm, String name, String protocol) {
		validateTokenRealmEntityProviderNameParams(token, realm, name);
		ServiceConfig organizationConfig = null;
		try {
			if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(protocol)) {
				organizationConfig = getOrganizationConfig(token, realm, SAML2_ENTITY_PROVIDERS_SERVICE);
			}
			if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(protocol)) {
				organizationConfig = getOrganizationConfig(token, realm, WSFED_ENTITY_PROVIDERS_SERVICE);
			}
			if (organizationConfig != null) {
				ServiceConfig entityConfig = organizationConfig.getSubConfig(name);
				return (entityConfig != null);
			} else {
				return false;
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}
	}

	@Override
	public boolean validateCertification(SSOToken token, String realm, String certification) {
		X509Certificate cert = KeyUtil.getKeyProviderInstance().getX509Certificate(certification);
		if (cert != null) {
			return true;
		}
		return false;
	}

	@Override
	public String getEntityProvidersForExportMetadata(SSOToken token, String realm, String type, String name)
			throws FederationException {
		validateTokenRealmParams(token, realm);
		StringBuffer sb = new StringBuffer();

		String metadataKey = "";
		try {
			String serviceType = null;
			if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(type)) {
				serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
				metadataKey = FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY;
			}
			if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(type)) {
				serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
				metadataKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY;
			}

			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
			if (organizationConfig != null) {

				ServiceConfig entityConfig = organizationConfig.getSubConfig(name);
				@SuppressWarnings("unchecked")
				Map<String, Set<String>> attr = entityConfig.getAttributes();

				String s1 = stringFromSet(attr.get(metadataKey));

				sb.append(s1.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", ""));

			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

		return sb.toString();

	}

	@Override
	public String getEntityProvidersForExportExtendedData(SSOToken token, String realm, String type, String name)
			throws FederationException {
		validateTokenRealmParams(token, realm);
		StringBuffer sb = new StringBuffer();

		String entityConfigKey = "";
		try {
			String serviceType = null;
			if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(type)) {
				serviceType = SAML2_ENTITY_PROVIDERS_SERVICE;
				entityConfigKey = FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY;
			}
			if (FederationConstants.ENTITY_PROVIDER_WSFED_PROTOCOL.equals(type)) {
				serviceType = WSFED_ENTITY_PROVIDERS_SERVICE;
				entityConfigKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY;
			}

			ServiceConfig organizationConfig = getOrganizationConfig(token, realm, serviceType);
			if (organizationConfig != null) {

				ServiceConfig entityConfig = organizationConfig.getSubConfig(name);
				@SuppressWarnings("unchecked")
				Map<String, Set<String>> attr = entityConfig.getAttributes();
				String s2 = stringFromSet(attr.get(entityConfigKey));
				sb.append(s2.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", ""));

			} else {
				throw new IllegalArgumentException("Realm not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new FederationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new FederationException(e.getMessage(), e);
		}

		return sb.toString();

	}

}
