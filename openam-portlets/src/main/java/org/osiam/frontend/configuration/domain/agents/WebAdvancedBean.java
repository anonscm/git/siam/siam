/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.agents;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.booleanFromSet;
import static org.osiam.frontend.util.CollectionUtils.integerFromSet;
import static org.osiam.frontend.util.CollectionUtils.listFromSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.osiam.frontend.configuration.service.AgentAttributesConstants;

/**
 * Encapsulates data for a web advanced bean.
 * 
 */
public class WebAdvancedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String name;

	private String clientIPAddressHeader;
	private String clientHostnameHeader;
	private boolean loadBalancerSetup;
	private boolean overrideRequestURLProtocol;
	private boolean overrideRequestURLHost;
	private boolean overrideRequestURLPort;
	private boolean overrideNotificationURL;
	private boolean postDataPreservation;
	@NotNull
	@Min(0)
	private Integer postDataEntriesCachePeriod;
	private boolean overrideProxyServerHostAndPort;
	private String authenticationType;
	private String replayPasswordKey;
	private String filterPriority;
	private boolean filterconfiguredWithOWA;
	private boolean changeURLProtocolToHttps;
	private String idleSessionTimeoutPageURL;
	private boolean checkUserInDominoDatabase;
	private boolean useLTPAToken;
	private String ltpaTokenCookieName;
	private String ltpaTokenConfigurationName;
	private String ltpaTokenOrganizationName;
	private List<String> customProperties = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientIPAddressHeader() {
		return clientIPAddressHeader;
	}

	public void setClientIPAddressHeader(String clientIPAddressHeader) {
		this.clientIPAddressHeader = clientIPAddressHeader;
	}

	public String getClientHostnameHeader() {
		return clientHostnameHeader;
	}

	public void setClientHostnameHeader(String clientHostnameHeader) {
		this.clientHostnameHeader = clientHostnameHeader;
	}

	public boolean getLoadBalancerSetup() {
		return loadBalancerSetup;
	}

	public void setLoadBalancerSetup(boolean loadBalancerSetup) {
		this.loadBalancerSetup = loadBalancerSetup;
	}

	public boolean getOverrideRequestURLProtocol() {
		return overrideRequestURLProtocol;
	}

	public void setOverrideRequestURLProtocol(boolean overrideRequestURLProtocol) {
		this.overrideRequestURLProtocol = overrideRequestURLProtocol;
	}

	public boolean getOverrideRequestURLHost() {
		return overrideRequestURLHost;
	}

	public void setOverrideRequestURLHost(boolean overrideRequestURLHost) {
		this.overrideRequestURLHost = overrideRequestURLHost;
	}

	public boolean getOverrideRequestURLPort() {
		return overrideRequestURLPort;
	}

	public void setOverrideRequestURLPort(boolean overrideRequestURLPort) {
		this.overrideRequestURLPort = overrideRequestURLPort;
	}

	public boolean getOverrideNotificationURL() {
		return overrideNotificationURL;
	}

	public void setOverrideNotificationURL(boolean overrideNotificationURL) {
		this.overrideNotificationURL = overrideNotificationURL;
	}

	public boolean getPostDataPreservation() {
		return postDataPreservation;
	}

	public void setPostDataPreservation(boolean postDataPreservation) {
		this.postDataPreservation = postDataPreservation;
	}

	public Integer getPostDataEntriesCachePeriod() {
		return postDataEntriesCachePeriod;
	}

	public void setPostDataEntriesCachePeriod(Integer postDataEntriesCachePeriod) {
		this.postDataEntriesCachePeriod = postDataEntriesCachePeriod;
	}

	public boolean getOverrideProxyServerHostAndPort() {
		return overrideProxyServerHostAndPort;
	}

	public void setOverrideProxyServerHostAndPort(boolean overrideProxyServerHostAndPort) {
		this.overrideProxyServerHostAndPort = overrideProxyServerHostAndPort;
	}

	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}

	public String getReplayPasswordKey() {
		return replayPasswordKey;
	}

	public void setReplayPasswordKey(String replayPasswordKey) {
		this.replayPasswordKey = replayPasswordKey;
	}

	public String getFilterPriority() {
		return filterPriority;
	}

	public void setFilterPriority(String filterPriority) {
		this.filterPriority = filterPriority;
	}

	public boolean getFilterconfiguredWithOWA() {
		return filterconfiguredWithOWA;
	}

	public void setFilterconfiguredWithOWA(boolean filterconfiguredWithOWA) {
		this.filterconfiguredWithOWA = filterconfiguredWithOWA;
	}

	public boolean getChangeURLProtocolToHttps() {
		return changeURLProtocolToHttps;
	}

	public void setChangeURLProtocolToHttps(boolean changeURLProtocolToHttps) {
		this.changeURLProtocolToHttps = changeURLProtocolToHttps;
	}

	public String getIdleSessionTimeoutPageURL() {
		return idleSessionTimeoutPageURL;
	}

	public void setIdleSessionTimeoutPageURL(String idleSessionTimeoutPageURL) {
		this.idleSessionTimeoutPageURL = idleSessionTimeoutPageURL;
	}

	public boolean getCheckUserInDominoDatabase() {
		return checkUserInDominoDatabase;
	}

	public void setCheckUserInDominoDatabase(boolean checkUserInDominoDatabase) {
		this.checkUserInDominoDatabase = checkUserInDominoDatabase;
	}

	public boolean getUseLTPAToken() {
		return useLTPAToken;
	}

	public void setUseLTPAToken(boolean useLTPAToken) {
		this.useLTPAToken = useLTPAToken;
	}

	public String getLtpaTokenCookieName() {
		return ltpaTokenCookieName;
	}

	public void setLtpaTokenCookieName(String ltpaTokenCookieName) {
		this.ltpaTokenCookieName = ltpaTokenCookieName;
	}

	public String getLtpaTokenConfigurationName() {
		return ltpaTokenConfigurationName;
	}

	public void setLtpaTokenConfigurationName(String ltpaTokenConfigurationName) {
		this.ltpaTokenConfigurationName = ltpaTokenConfigurationName;
	}

	public String getLtpaTokenOrganizationName() {
		return ltpaTokenOrganizationName;
	}

	public void setLtpaTokenOrganizationName(String ltpaTokenOrganizationName) {
		this.ltpaTokenOrganizationName = ltpaTokenOrganizationName;
	}

	public List<String> getCustomProperties() {
		return customProperties;
	}

	public void setCustomProperties(List<String> customProperties) {
		this.customProperties = customProperties;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param name
	 *            - the bean name
	 * @param attributes
	 *            - map of attributes.
	 * 
	 */
	public static WebAdvancedBean fromMap(String name, Map<String, Set<String>> attributes) {
		WebAdvancedBean bean = new WebAdvancedBean();
		bean.setName(name);
		bean.setClientIPAddressHeader(stringFromSet(attributes.get(AgentAttributesConstants.CLIENT_IP_ADDRESS_HEADER)));
		bean.setClientHostnameHeader(stringFromSet(attributes.get(AgentAttributesConstants.CLIENT_HOSTNAME_HEADER)));
		bean.setLoadBalancerSetup(booleanFromSet(attributes.get(AgentAttributesConstants.LOAD_BALANCER_SETUP)));
		bean.setOverrideRequestURLProtocol(booleanFromSet(attributes
				.get(AgentAttributesConstants.OVERRIDE_REQUEST_URL_PROTOCOL)));
		bean.setOverrideRequestURLHost(booleanFromSet(attributes
				.get(AgentAttributesConstants.OVERRIDE_REQUEST_URL_HOST)));
		bean.setOverrideRequestURLPort(booleanFromSet(attributes
				.get(AgentAttributesConstants.OVERRIDE_REQUEST_URL_PORT)));
		bean.setOverrideNotificationURL(booleanFromSet(attributes
				.get(AgentAttributesConstants.OVERRIDE_NOTIFICATION_URL)));
		bean.setPostDataPreservation(booleanFromSet(attributes.get(AgentAttributesConstants.POST_DATA_PRESERVATION)));
		bean.setPostDataEntriesCachePeriod(integerFromSet(attributes
				.get(AgentAttributesConstants.POST_DATA_ENTRIES_CACHE_PERIOD)));
		bean.setOverrideProxyServerHostAndPort(booleanFromSet(attributes
				.get(AgentAttributesConstants.OVERRIDE_PROXY_SERVERS_HOST_AND_PORT)));
		bean.setAuthenticationType(stringFromSet(attributes.get(AgentAttributesConstants.AUTHENTICATION_TYPE)));
		bean.setReplayPasswordKey(stringFromSet(attributes.get(AgentAttributesConstants.REPLAY_PASSWORD_KEY)));
		bean.setFilterPriority(stringFromSet(attributes.get(AgentAttributesConstants.FILTER_PRIORITY)));
		bean.setFilterconfiguredWithOWA(booleanFromSet(attributes
				.get(AgentAttributesConstants.FILTER_CONFIGURED_WITH_OWA)));
		bean.setChangeURLProtocolToHttps(booleanFromSet(attributes
				.get(AgentAttributesConstants.CHANGE_URL_PROTOCOL_TO_HTTPS)));
		bean.setIdleSessionTimeoutPageURL(stringFromSet(attributes
				.get(AgentAttributesConstants.IDLE_SESSION_TIMEOUT_PAGE_URL)));
		bean.setCheckUserInDominoDatabase(booleanFromSet(attributes
				.get(AgentAttributesConstants.CHECK_USER_IN_DOMINO_DATABASE)));
		bean.setUseLTPAToken(booleanFromSet(attributes.get(AgentAttributesConstants.USE_LTPA_TOKEN)));
		bean.setLtpaTokenCookieName(stringFromSet(attributes.get(AgentAttributesConstants.LTPA_TOKEN_COOKIE_NAME)));
		bean.setLtpaTokenConfigurationName(stringFromSet(attributes
				.get(AgentAttributesConstants.LTPA_TOKEN_CONFIGURATION_NAME)));
		bean.setLtpaTokenOrganizationName(stringFromSet(attributes
				.get(AgentAttributesConstants.LTPA_TOKEN_ORGANIZATION_NAME)));
		bean.setCustomProperties(listFromSet(attributes.get(AgentAttributesConstants.CUSTOM_PROPERTIES_CURRENT_VALUES)));

		return bean;

	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		map.put(AgentAttributesConstants.CLIENT_IP_ADDRESS_HEADER, asSet(getClientIPAddressHeader()));
		map.put(AgentAttributesConstants.CLIENT_HOSTNAME_HEADER, asSet(getClientHostnameHeader()));
		map.put(AgentAttributesConstants.LOAD_BALANCER_SETUP, asSet(getLoadBalancerSetup()));
		map.put(AgentAttributesConstants.OVERRIDE_REQUEST_URL_PROTOCOL, asSet(getOverrideRequestURLProtocol()));
		map.put(AgentAttributesConstants.OVERRIDE_REQUEST_URL_HOST, asSet(getOverrideRequestURLHost()));
		map.put(AgentAttributesConstants.OVERRIDE_REQUEST_URL_PORT, asSet(getOverrideRequestURLPort()));
		map.put(AgentAttributesConstants.OVERRIDE_NOTIFICATION_URL, asSet(getOverrideNotificationURL()));
		map.put(AgentAttributesConstants.POST_DATA_PRESERVATION, asSet(getPostDataPreservation()));
		map.put(AgentAttributesConstants.POST_DATA_ENTRIES_CACHE_PERIOD, asSet(getPostDataEntriesCachePeriod()));
		map.put(AgentAttributesConstants.OVERRIDE_PROXY_SERVERS_HOST_AND_PORT,
				asSet(getOverrideProxyServerHostAndPort()));
		map.put(AgentAttributesConstants.AUTHENTICATION_TYPE, asSet(getAuthenticationType()));
		map.put(AgentAttributesConstants.REPLAY_PASSWORD_KEY, asSet(getReplayPasswordKey()));
		map.put(AgentAttributesConstants.FILTER_PRIORITY, asSet(getFilterPriority()));
		map.put(AgentAttributesConstants.FILTER_CONFIGURED_WITH_OWA, asSet(getFilterconfiguredWithOWA()));
		map.put(AgentAttributesConstants.CHANGE_URL_PROTOCOL_TO_HTTPS, asSet(getChangeURLProtocolToHttps()));
		map.put(AgentAttributesConstants.IDLE_SESSION_TIMEOUT_PAGE_URL, asSet(getIdleSessionTimeoutPageURL()));
		map.put(AgentAttributesConstants.CHECK_USER_IN_DOMINO_DATABASE, asSet(getCheckUserInDominoDatabase()));
		map.put(AgentAttributesConstants.USE_LTPA_TOKEN, asSet(getUseLTPAToken()));
		map.put(AgentAttributesConstants.LTPA_TOKEN_COOKIE_NAME, asSet(getLtpaTokenCookieName()));
		map.put(AgentAttributesConstants.LTPA_TOKEN_CONFIGURATION_NAME, asSet(getLtpaTokenConfigurationName()));
		map.put(AgentAttributesConstants.LTPA_TOKEN_ORGANIZATION_NAME, asSet(getLtpaTokenOrganizationName()));
		map.put(AgentAttributesConstants.CUSTOM_PROPERTIES_CURRENT_VALUES, asSet(getCustomProperties()));

		return map;
	}

}
