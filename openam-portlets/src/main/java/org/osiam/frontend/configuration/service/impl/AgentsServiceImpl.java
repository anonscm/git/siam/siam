/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.service.impl;

import static org.osiam.frontend.util.AttributeUtils.removePrefix;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.common.service.impl.BaseSSOService;
import org.osiam.frontend.configuration.domain.agents.Agent;
import org.osiam.frontend.configuration.domain.agents.J2EEAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.J2EEApplicationBean;
import org.osiam.frontend.configuration.domain.agents.J2EEGlobalBean;
import org.osiam.frontend.configuration.domain.agents.J2EEMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.J2EEOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.J2EESSOBean;
import org.osiam.frontend.configuration.domain.agents.LocalAgent;
import org.osiam.frontend.configuration.domain.agents.WebAdvancedBean;
import org.osiam.frontend.configuration.domain.agents.WebApplicationBean;
import org.osiam.frontend.configuration.domain.agents.WebGlobalBean;
import org.osiam.frontend.configuration.domain.agents.WebMiscellaneousBean;
import org.osiam.frontend.configuration.domain.agents.WebOpenSSOServicesBean;
import org.osiam.frontend.configuration.domain.agents.WebSSOBean;
import org.osiam.frontend.configuration.exceptions.AgentsException;
import org.osiam.frontend.configuration.service.AgentAttributesConstants;
import org.osiam.frontend.configuration.service.AgentsConstants;
import org.osiam.frontend.configuration.service.AgentsService;
import org.springframework.stereotype.Service;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.AMIdentity;
import com.sun.identity.idm.AMIdentityRepository;
import com.sun.identity.idm.IdConstants;
import com.sun.identity.idm.IdRepoException;
import com.sun.identity.idm.IdSearchControl;
import com.sun.identity.idm.IdSearchResults;
import com.sun.identity.idm.IdType;
import com.sun.identity.security.AdminTokenAction;
import com.sun.identity.shared.FQDNUrl;
import com.sun.identity.sm.AttributeSchema;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.SchemaType;
import com.sun.identity.sm.ServiceSchema;
import com.sun.identity.sm.ServiceSchemaManager;

/**
 * Agents service implementation. Defines methods for agents management.
 */
@Service("AgentsService")
public class AgentsServiceImpl extends BaseSSOService implements AgentsService {

	private static final Logger LOGGER = Logger.getLogger(AgentsServiceImpl.class);

	private static final String ATTR_NAME_PWD = "userpassword";
	private static final String VAL_CONFIG_REPO_LOCAL = "local";
	private static final String ATTR_CONFIG_REPO = "com.sun.identity.agents.config.repository.location";
	private static final String ATTR_NAME_FREE_FORM = "com.sun.identity.agents.config.freeformproperties";

	private static final String AGENT_ROOT_URL = "agentRootURL=";
	private static final String DEVICE_KEY = "sunIdentityServerDeviceKeyValue";

	private static final int MAX_PORT_NUMBER = 65535;

	private void validateTokenRealmAgentNameParams(SSOToken token, String realm, String agentName) {
		validateTokenRealmParams(token, realm);
		if (agentName == null) {
			throw new IllegalArgumentException(
					String.format("Param agentName cannot be null. Example : %s", "'agent1'"));
		}
	}

	private void validateTokenRealmBeanParams(SSOToken token, String realm, Object bean) {
		validateTokenRealmParams(token, realm);
		if (bean == null) {
			throw new IllegalArgumentException("Param bean cannot be null.");
		}
	}

	private void validateAMIdentityRepository(AMIdentityRepository identityRepository, String realm) {
		if (identityRepository == null) {
			LOGGER.error(String.format("Realm '%s' is not registered", realm));
			throw new IllegalArgumentException(String.format("Realm '%s' is not registered", realm));
		}
	}

	private void validateAMIdentity(AMIdentity identity, String realm) {
		if (identity == null) {
			LOGGER.error(String.format("Realm '%s' is not registered", realm));
			throw new IllegalArgumentException(String.format("Realm '%s' is not registered", realm));
		}
	}

	@Override
	public List<String> getAgentTypesList() {
		return Arrays.asList(AgentsConstants.AGENT_TYPE_WEB, AgentsConstants.AGENT_TYPE_J2EE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Agent> getAgentsList(SSOToken token, String realm, int maxResults) throws AgentsException {
		validateTokenRealmParams(token, realm);

		if (maxResults < 0) {
			throw new IllegalArgumentException("Parameter maxResults must be pozitive.");
		}

		List<Agent> agentList = new ArrayList<Agent>();

		IdSearchControl idSearchControl = new IdSearchControl();
		idSearchControl.setMaxResults(maxResults);
		idSearchControl.setTimeOut(Integer.MAX_VALUE);
		idSearchControl.setAllReturnAttributes(false);

		AMIdentityRepository identityRepository = null;

		IdSearchResults idSearchResults = null;
		try {
			identityRepository = new AMIdentityRepository(token, realm);
			validateAMIdentityRepository(identityRepository, realm);
			idSearchResults = identityRepository.searchIdentities(IdType.AGENTONLY, "*", idSearchControl);

			Set<AMIdentity> resultSet = idSearchResults.getSearchResults();

			if ((resultSet != null) && !resultSet.isEmpty()) {
				Iterator<AMIdentity> iterator = resultSet.iterator();
				while (iterator.hasNext()) {
					AMIdentity amIdentity = iterator.next();

					Agent agent = new Agent();
					agent.setName(amIdentity.getName());
					agent.setType(stringFromSet((Set<String>) amIdentity.getAttributes().get(IdConstants.AGENT_TYPE)));
					agent.setConfiguration(stringFromSet((Set<String>) amIdentity.getAttributes().get(
							AgentsConstants.ATTR_CONFIG_REPO)));
					// all the agent with type "WebAgent" and "J2EEAgent" can
					// have "Export Configurations" link
					agent.setExport(true);

					String agentURL = stringFromSet(
							(Set<String>) amIdentity.getAttributes().get("sunidentityserverdevicekeyvalue")).replace(
							"agentRootURL=", "");
					agent.setAgentURL(agentURL);

					String serverURL = null;
					if (AgentsConstants.AGENT_CONFIGURATION_CENTRALIZED.equals(agent.getConfiguration())) {
						serverURL = stringFromSet(
								(Set<String>) amIdentity.getAttributes().get(
										"com.sun.identity.agents.config.cdsso.cdcservlet.url")).replace("[0]=", "")
								.replace("cdcservlet", "");
					}
					agent.setServerURL(serverURL);

					agentList.add(agent);
				}

			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return agentList;
	}

	@Override
	public void addAgent(SSOToken token, String realm, Agent agent) throws AgentsException {
		validateTokenRealmParams(token, realm);
		if (agent == null) {
			throw new IllegalArgumentException("error.parameter_agent_cannot_be_null");
		}
		String agentType = agent.getType();
		String agentName = agent.getName().trim();
		String password = agent.getPassword();

		String choice = agent.getConfiguration();
		try {
			if (agentType.equals(AgentsConstants.AGENT_TYPE_J2EE) || agentType.equals(AgentsConstants.AGENT_TYPE_WEB)) {
				String agentURL = agent.getAgentURL().trim();
				if (choice.equals(AgentsConstants.AGENT_CONFIGURATION_LOCAL)) {
					createAgentLocal(token, realm, agentName, agentType, password, agentURL);
				} else {
					String serverURL = agent.getServerURL().trim();
					createAgentWebOrJ2ee(token, realm, agentName, agentType, password, serverURL, agentURL);
				}
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (AgentsException e) {
			LOGGER.error("AgentsException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
	}

	private void createAgentWebOrJ2ee(SSOToken token, String realmName, String name, String type, String password,
			String serverURL, String agentURL) throws SSOException, SMSException, AgentsException, IdRepoException {

		Map<String, Set<String>> map = getDefaultValues(type, false);
		Set<String> set = new HashSet<String>(2);
		map.put(ATTR_NAME_PWD, set);
		set.add(password);
		createAgent(token, realmName, name, type, map, serverURL, agentURL);
	}

	private void createAgent(SSOToken ssoToken, String realm, String agentName, String agentType,
			Map<String, Set<String>> attrValues, String serverURL, String agentURL) throws SSOException,
			AgentsException, IdRepoException, SMSException {
		if ((serverURL == null) || (serverURL.trim().length() == 0)) {
			throw new IllegalArgumentException("error.parameter_serverurl_cannot_be_null");
		}
		if ((agentURL == null) || (agentURL.trim().length() == 0)) {
			throw new IllegalArgumentException("error.parameter_agenturl_cannot_be_null");
		}

		FQDNUrl serverFQDNURL = null;
		FQDNUrl agentFQDNURL = null;
		try {
			serverFQDNURL = new FQDNUrl(serverURL);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("error.parameter_serverurl_must_be_a_valid_url");
		}

		try {
			agentFQDNURL = new FQDNUrl(agentURL);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("error.parameter_agenturl_must_be_valid_url");
		}

		createAgentEx(ssoToken, realm, agentName, agentType, attrValues, serverFQDNURL, agentFQDNURL);
	}

	private void createAgentLocal(SSOToken token, String realm, String name, String type, String password,
			String agentURL) throws SSOException, SMSException, AgentsException, IdRepoException {

		Map<String, Set<String>> map = getDefaultValues(type, false);
		Set<String> set = new HashSet<String>(2);
		map.put(ATTR_NAME_PWD, set);
		set.add(password);
		Set<String> newset = new HashSet<String>(2);
		newset.add(VAL_CONFIG_REPO_LOCAL);
		map.put(ATTR_CONFIG_REPO, newset);
		createAgentLocal(token, realm, name, type, map, agentURL);

	}

	private void createAgentLocal(SSOToken ssoToken, String realm, String agentName, String agentType,
			Map<String, Set<String>> attrValues, String agentURL) throws SSOException, AgentsException,
			IdRepoException, SMSException {
		if ((agentURL == null) || (agentURL.trim().length() == 0)) {
			throw new IllegalArgumentException("error.parameter_agenturl_cannot_be_null");
		}
		try {
			createAgentEx(ssoToken, realm, agentName, agentType, attrValues, null, new FQDNUrl(agentURL));
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("error.parameter_agenturl_is_not_valid");
		}
	}

	private void validateAgentType(String type) throws AgentsException {
		validateAgentType(type, false);
	}

	private void validateAgentType(String type, boolean isGroup) throws AgentsException {
		List<String> types = getAgentTypesList();
		if (isGroup) {
			types.remove(AgentsConstants.AGENT_TYPE_2_DOT_2_AGENT);
		}

		if (!types.contains(type)) {
			throw new IllegalArgumentException("Invalid agent type");
		}

	}

	private Map<String, Set<String>> parseAttributeMap(String agentType, Map<String, Set<String>> attrValues)
			throws SMSException, SSOException {
		Map<String, Set<String>> dummy = new HashMap<String, Set<String>>();
		dummy.putAll(attrValues);
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		Set<AttributeSchema> attributeSchemas = getAgentAttributeSchemas(agentType);

		if ((attributeSchemas != null) && !attributeSchemas.isEmpty()) {
			for (Iterator<AttributeSchema> i = attributeSchemas.iterator(); i.hasNext();) {
				AttributeSchema as = i.next();
				Set<String> values = parseAttributeMap(as, dummy);
				if (values != null) {
					result.put(as.getName(), values);
				}
			}
		}
		if (!dummy.isEmpty()) {
			Set<String> freeForm = new HashSet<String>();
			for (Iterator<String> i = dummy.keySet().iterator(); i.hasNext();) {
				String name = i.next();
				Set<String> values = dummy.get(name);
				for (Iterator<String> j = values.iterator(); j.hasNext();) {
					freeForm.add(name + "=" + (j.next()));
				}
			}
			result.put(ATTR_NAME_FREE_FORM, freeForm);
		}

		return result;
	}

	private Set<String> parseAttributeMap(AttributeSchema as, Map<String, Set<String>> attrValues) {
		Set<String> results = null;
		String attrName = as.getName();
		if (as.getType().equals(AttributeSchema.Type.LIST)) {
			Set<String> keysToDelete = new HashSet<String>();
			for (Iterator<String> i = attrValues.keySet().iterator(); i.hasNext();) {
				String key = i.next();
				if (key.equals(attrName)) {
					if (results == null) {
						results = new HashSet<String>();
					}
					Set<String> set = attrValues.get(key);
					if ((set != null) && !set.isEmpty()) {
						results.addAll(set);
					}
					keysToDelete.add(key);
				} else if (key.startsWith(attrName + "[")) {
					if (results == null) {
						results = new HashSet<String>();
					}
					Set<String> set = attrValues.get(key);
					String v = ((set != null) && !set.isEmpty()) ? (String) set.iterator().next() : "";
					results.add(key.substring(attrName.length()) + "=" + v);
					keysToDelete.add(key);
				}
			}
			for (Iterator<String> i = keysToDelete.iterator(); i.hasNext();) {
				attrValues.remove(i.next());
			}
		} else {
			results = attrValues.remove(attrName);
		}
		return results;
	}

	private void tagswapAttributeValues(Map<String, Set<String>> attributeValues, String agentType, FQDNUrl serverURL,
			FQDNUrl agentURL) {
		Map<String, String> map = new HashMap<String, String>();

		if (serverURL != null) {
			if (!serverURL.isValid()) {
				throw new IllegalArgumentException("error.parameter_serverurl_must_be_a_valid_url");
			}

			if (!serverURL.isFullyQualified()) {
				throw new IllegalArgumentException("error.parameter_serverurl_must_be_fully_qualified_url");
			}

			String uri = serverURL.getURI();
			if (uri.length() == 0) {
				throw new IllegalArgumentException("error.parameter_serverurl_must_contain_valid_uri");
			}

			String port = serverURL.getPort();
			if (port.equals("-1")) {
				throw new IllegalArgumentException("error.parameter_serverurl_must_contain_valid_port");
			}

			map.put("SERVER_PROTO", serverURL.getProtocol());
			map.put("SERVER_HOST", serverURL.getHost());
			map.put("SERVER_PORT", port);
			map.put("AM_SERVICES_DEPLOY_URI", uri);
		}

		if (agentURL != null) {
			String port = agentURL.getPort();
			map.put("AGENT_PROTO", agentURL.getProtocol());
			map.put("AGENT_HOST", agentURL.getHost());
			map.put("AGENT_PORT", port);

			if (agentType.equals(AgentsConstants.AGENT_TYPE_J2EE)) {
				if (!agentURL.isValid()) {
					throw new IllegalArgumentException("error.parameter_agenturl_is_not_valid");
				}
				if (!agentURL.isFullyQualified()) {
					throw new IllegalArgumentException("error.parameter_agenturl_must_contain_valid_uri");
				}

				String uri = agentURL.getURI();
				if (uri.length() == 0) {
					throw new IllegalArgumentException("error.parameter_agenturl_must_contain_valid_uri");
				}
				map.put("AGENT_APP_URI", uri);

				String logFileName = agentURL.getHost();
				logFileName = "amAgent_" + logFileName.replaceAll("\\.", "_") + "_" + port + ".log";
				map.put("AUDIT_LOG_FILENAME", logFileName);
			} else if (agentType.equals(AgentsConstants.AGENT_TYPE_WEB)) {
				String uri = agentURL.getURI();
				if (uri.length() > 0) {
					throw new IllegalArgumentException("error.parameter_agenturl_is_not_valid");
				}
				map.put("AGENT_APP_URI", uri);

				String logFileName = agentURL.getHost();
				logFileName = "amAgent_" + logFileName.replaceAll("\\.", "_") + "_" + port + ".log";
				map.put("AUDIT_LOG_FILENAME", logFileName);
			}
		}
		tagswapAttributeValues(attributeValues, map);
		if (agentURL != null) {
			addAgentRootURLKey(agentType, attributeValues);
		}
	}

	private void addAgentRootURLKey(String agentType, Map<String, Set<String>> map) {
		if (agentType.equals(AgentsConstants.AGENT_TYPE_J2EE) || agentType.equals(AgentsConstants.AGENT_TYPE_WEB)
				|| agentType.equals(AgentsConstants.AGENT_TYPE_AGENT_AUTHENTICATOR)) {
			Set<String> values = map.get(DEVICE_KEY);
			if ((values != null) && !values.isEmpty()) {
				Set<String> newValues = new HashSet<String>();
				for (Iterator<String> i = values.iterator(); i.hasNext();) {
					String val = AGENT_ROOT_URL + i.next();
					validateAgentRootURL(val);
					newValues.add(val);
				}
				map.put(DEVICE_KEY, newValues);
			}
		}
	}

	private void validateAgentRootURL(String value) {

		String[] strs = value.split("=", 2);
		String key = strs[0] + "=";
		if (strs.length == 1 || !(key.equalsIgnoreCase(AGENT_ROOT_URL))) {
			return;
		}

		if (!key.equals(AGENT_ROOT_URL)) {
			throw new IllegalArgumentException("Invalid value for parameter agentURL");
		}

		if (!value.endsWith("/")) {
			throw new IllegalArgumentException("Invalid value for parameter agentURL");
		}

		String urlStr = strs[1];
		URL url = null;
		try {
			url = new URL(urlStr);
		} catch (MalformedURLException e) {

			// Object[] param = { urlStr };
			throw new IllegalArgumentException("Invalid value for parameter agentURL");
		}

		int port = url.getPort();
		if (port == -1) {
			throw new IllegalArgumentException("Invalid value for parameter agentURL");
		} else if (port < 1 || port > MAX_PORT_NUMBER) {
			// Object[] param = { Integer.toString(port) };
			throw new IllegalArgumentException("Invalid value for parameter agentURL");
		}
	}

	private void tagswapAttributeValues(Map<String, Set<String>> attributeValues, Map<String, String> tagswapInfo) {
		for (Iterator<String> i = attributeValues.keySet().iterator(); i.hasNext();) {
			String attrName = i.next();
			Set<String> values = attributeValues.get(attrName);
			Set<String> newValues = new HashSet<String>(values.size() * 2);

			for (Iterator<String> j = values.iterator(); j.hasNext();) {
				String value = j.next();
				newValues.add(tagswap(tagswapInfo, value));
			}
			values.clear();
			values.addAll(newValues);
		}
	}

	private String tagswap(Map<String, String> map, String value) {
		for (Iterator<String> i = map.keySet().iterator(); i.hasNext();) {
			String k = i.next();
			value = value.replaceAll("@" + k + "@", map.get(k));
		}
		return value;
	}

	private AMIdentity createAgentEx(SSOToken ssoToken, String realm, String agentName, String agentType,
			Map<String, Set<String>> attrValues, FQDNUrl serverURL, FQDNUrl agentURL) throws AgentsException,
			SSOException, IdRepoException, SMSException {

		if ((agentName == null) || (agentName.trim().length() == 0)) {
			throw new IllegalArgumentException("error.parameter_agentname_cannot_be_null");
		}

		validateAgentType(agentType);
		AMIdentityRepository amir = new AMIdentityRepository(ssoToken, realm);
		Map<String, Set<String>> attributeValues = parseAttributeMap(agentType, attrValues);
		Set<String> setAgentType = new HashSet<String>(2);
		setAgentType.add(agentType);
		attributeValues.put(IdConstants.AGENT_TYPE, setAgentType);
		Map<String, Set<String>> inheritedValues = getDefaultValues(agentType, false);
		// overwrite inherited values with what user has given
		inheritedValues.putAll(attributeValues);

		if (serverURL == null) {
			// need to set an arbitrary number to com.iplanet.am.server.port
			// so that number validator will pass
			Map<String, String> map = new HashMap<String, String>(2);
			map.put("SERVER_PORT", "80");
			tagswapAttributeValues(attributeValues, map);
		}

		if (agentURL == null) {
			// need to set an arbitrary number to com.iplanet.am.server.port
			// so that number validator will pass
			Map<String, String> map = new HashMap<String, String>(2);
			map.put("AGENT_PORT", "80");
			tagswapAttributeValues(attributeValues, map);
		}

		if ((serverURL != null) || (agentURL != null)) {
			tagswapAttributeValues(inheritedValues, agentType, serverURL, agentURL);
		}

		AMIdentity amIdentity = null;
		try {
			amIdentity = amir.createIdentity(IdType.AGENTONLY, agentName, inheritedValues);
		} catch (IdRepoException e) {
			throw new IllegalArgumentException("error.duplicate_agent_name");

		}
		return amIdentity;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Set<String>> getDefaultValues(String agentType, boolean bGroup) throws SMSException,
			SSOException {
		Map<String, Set<String>> mapDefault = new HashMap<String, Set<String>>();
		Set<AttributeSchema> attributeSchemas = getAgentAttributeSchemas(agentType);

		if ((attributeSchemas != null) && !attributeSchemas.isEmpty()) {
			for (Iterator<AttributeSchema> i = attributeSchemas.iterator(); i.hasNext();) {
				AttributeSchema as = i.next();
				mapDefault.put(as.getName(), as.getDefaultValues());
			}
		}
		if (bGroup) {
			mapDefault.remove(ATTR_NAME_PWD);
		}
		return mapDefault;
	}

	private Set<AttributeSchema> getAgentAttributeSchemas(String agentTypeName) throws SMSException, SSOException {
		Set<AttributeSchema> attrSchemas = new HashSet<AttributeSchema>();
		ServiceSchema ss = getOrganizationSchema();
		if (ss != null) {
			ServiceSchema ssType = ss.getSubSchema(agentTypeName);
			@SuppressWarnings("unchecked")
			Set<AttributeSchema> attrs = ssType.getAttributeSchemas();
			if ((attrs != null) && !attrs.isEmpty()) {
				attrSchemas.addAll(attrs);
			}
		}

		for (Iterator<AttributeSchema> i = attrSchemas.iterator(); i.hasNext();) {
			AttributeSchema as = i.next();
			if (as.getType().equals(AttributeSchema.Type.VALIDATOR)) {
				i.remove();
			}
		}
		return attrSchemas;
	}

	private ServiceSchema getOrganizationSchema() throws SMSException, SSOException {
		ServiceSchema ss = null;
		@SuppressWarnings("unchecked")
		SSOToken adminToken = (SSOToken) AccessController.doPrivileged(AdminTokenAction.getInstance());
		ServiceSchemaManager ssm = new ServiceSchemaManager(IdConstants.AGENT_SERVICE, adminToken);
		if (ssm != null) {
			ss = ssm.getSchema(SchemaType.ORGANIZATION);
		}
		return ss;
	}

	@Override
	public void deleteAgent(SSOToken token, String realm, String name) throws AgentsException {
		validateTokenRealmParams(token, realm);
		if (name == null) {
			throw new IllegalArgumentException("Param name cannot be null. ");
		}

		AMIdentityRepository repository;
		try {
			repository = new AMIdentityRepository(token, realm);
			AMIdentity amIdentity = new AMIdentity(token, name, IdType.AGENTONLY, realm, null);
			Set<AMIdentity> agentSet = new HashSet<AMIdentity>();
			agentSet.add(amIdentity);
			repository.deleteIdentities(agentSet);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
	}

	@Override
	public List<String> getGroupsList(SSOToken token, String realm, int maxResults) throws AgentsException {
		validateTokenRealmParams(token, realm);
		if (maxResults < 0) {
			throw new IllegalArgumentException("Param maxResults must be positive. ");
		}
		if (maxResults < 0) {
			throw new IllegalArgumentException("Parameter maxResults must be pozitive.");
		}

		List<String> agentGroupNamesList = new ArrayList<String>();

		IdSearchControl idSearchControl = new IdSearchControl();
		idSearchControl.setMaxResults(maxResults);
		idSearchControl.setTimeOut(Integer.MAX_VALUE);
		idSearchControl.setAllReturnAttributes(false);

		AMIdentityRepository identityRepository = null;

		IdSearchResults idSearchResults = null;
		try {
			identityRepository = new AMIdentityRepository(token, realm);
			idSearchResults = identityRepository.searchIdentities(IdType.AGENTGROUP, "*", idSearchControl);

			@SuppressWarnings("unchecked")
			Set<AMIdentity> resultSet = idSearchResults.getSearchResults();

			if ((resultSet != null) && !resultSet.isEmpty()) {
				Iterator<AMIdentity> iterator = resultSet.iterator();
				while (iterator.hasNext()) {
					AMIdentity amIdentity = iterator.next();
					agentGroupNamesList.add(amIdentity.getName());
				}

			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return agentGroupNamesList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public WebGlobalBean getAttributesForWebGlobal(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);

		WebGlobalBean webGlobalBean = new WebGlobalBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		validateAMIdentity(amIdentity, realm);
		try {
			webGlobalBean = WebGlobalBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new IllegalArgumentException(String.format("Realm '%s' is not registered", realm));
		}
		return webGlobalBean;
	}

	@Override
	public void saveWebGlobal(SSOToken token, String realm, WebGlobalBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = null;
		attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	private void saveAttributes(SSOToken token, String agentName, String realm, Map<String, Set<String>> attributes)
			throws AgentsException {

		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			Map<String, Set<String>> currentAttributes = new HashMap<String, Set<String>>();

			currentAttributes.putAll(attributes);

			amIdentity.setAttributes(currentAttributes);
			amIdentity.store();
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
	}

	private void saveLocalAttributes(SSOToken token, String agentName, String realm, Map<String, Set<String>> attributes)
			throws AgentsException {
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			Map<String, Set<String>> currentAttributes = new HashMap<String, Set<String>>();
			currentAttributes.putAll(attributes);
			amIdentity.setAttributes(currentAttributes);
			amIdentity.store();
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
	}

	@Override
	public void saveLocalAgentAttributes(SSOToken token, String realm, LocalAgent agent) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, agent);
		Map<String, Set<String>> attributes = null;
		attributes = agent.toMap();
		saveAttributes(token, agent.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public WebApplicationBean getAttributesForWebApplication(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);

		WebApplicationBean bean = new WebApplicationBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = WebApplicationBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveWebApplication(SSOToken token, String realm, WebApplicationBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public WebSSOBean getAttributesForWebSSO(SSOToken token, String realm, String agentName) throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		WebSSOBean bean = new WebSSOBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = WebSSOBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveWebSSO(SSOToken token, String realm, WebSSOBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public WebOpenSSOServicesBean getAttributesForWebOpenSSOServices(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		WebOpenSSOServicesBean bean = new WebOpenSSOServicesBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = WebOpenSSOServicesBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveWebOpenSSOServices(SSOToken token, String realm, WebOpenSSOServicesBean bean)
			throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public WebMiscellaneousBean getAttributesForWebMiscellaneous(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		WebMiscellaneousBean bean = new WebMiscellaneousBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = WebMiscellaneousBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveWebMiscellaneous(SSOToken token, String realm, WebMiscellaneousBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public WebAdvancedBean getAttributesForWebAdvanced(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		WebAdvancedBean bean = new WebAdvancedBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = WebAdvancedBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveWebAdvanced(SSOToken token, String realm, WebAdvancedBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EEGlobalBean getAttributesForJ2EEGlobal(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EEGlobalBean bean = new J2EEGlobalBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EEGlobalBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EEGlobal(SSOToken token, String realm, J2EEGlobalBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EEApplicationBean getAttributesForJ2EEApplication(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EEApplicationBean bean = new J2EEApplicationBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EEApplicationBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EEApplication(SSOToken token, String realm, J2EEApplicationBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EESSOBean getAttributesForJ2EESSO(SSOToken token, String realm, String agentName) throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EESSOBean bean = new J2EESSOBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EESSOBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EESSO(SSOToken token, String realm, J2EESSOBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EEOpenSSOServicesBean getAttributesForJ2EEOpenSSOServices(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EEOpenSSOServicesBean bean = new J2EEOpenSSOServicesBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EEOpenSSOServicesBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EEOpenSSOServices(SSOToken token, String realm, J2EEOpenSSOServicesBean bean)
			throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EEAdvancedBean getAttributesForJ2EEAdvancedBean(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EEAdvancedBean bean = new J2EEAdvancedBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EEAdvancedBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EEAdvancedBean(SSOToken token, String realm, J2EEAdvancedBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public J2EEMiscellaneousBean getAttributesForJ2EEMiscellaneousBean(SSOToken token, String realm, String agentName)
			throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		J2EEMiscellaneousBean bean = new J2EEMiscellaneousBean();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = J2EEMiscellaneousBean.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveJ2EEMiscellaneous(SSOToken token, String realm, J2EEMiscellaneousBean bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		saveAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public LocalAgent getAttributesForLocalAgent(SSOToken token, String realm, String agentName) throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		LocalAgent bean = new LocalAgent();
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			bean = LocalAgent.fromMap(agentName, amIdentity.getAttributes());
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		return bean;
	}

	@Override
	public void saveLocalAgent(SSOToken token, String realm, LocalAgent bean) throws AgentsException {
		validateTokenRealmBeanParams(token, realm, bean);
		Map<String, Set<String>> attributes = bean.toMap();
		try {
			String agentURL = removePrefix(bean.getAgentRootURLforCDSSO(), "agentRootURL=").get(0);
			createAgentLocal(token, realm, bean.getName(), bean.getType(), bean.getPassword(), agentURL);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		saveLocalAttributes(token, bean.getName(), realm, attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getExportConfiguration(SSOToken token, String realm, String agentName) throws AgentsException {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		Map<String, Set<String>> attributes = null;
		try {
			attributes = amIdentity.getAttributes();
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
		String location = stringFromSet(attributes.get(ATTR_CONFIG_REPO));
		boolean filtered = false;
		List<String> toBeExported = new ArrayList<String>(3);
		if (VAL_CONFIG_REPO_LOCAL.equals(location)) {
			filtered = true;
			toBeExported = Arrays.asList(AgentAttributesConstants.AGENT_KEY_VALUES_CURRENT_VALUES.toLowerCase(),
					AgentAttributesConstants.STATUS.toLowerCase(), AgentAttributesConstants.PASSWORD.toLowerCase());
		}

		Iterator<String> iterator = attributes.keySet().iterator();
		List<String> exportList = new ArrayList<String>(attributes.keySet().size());
		while (iterator.hasNext()) {
			String key = iterator.next();
			boolean exportKey = true;
			if (filtered) {
				if (!toBeExported.contains(key.toLowerCase())) {
					exportKey = false;
				}
			}
			if (exportKey) {
				String value = stringFromSet(attributes.get(key));
				if (value != null) {
					if (value.length() > 0) {
						exportList.add(processExportLine(key, value));
					}
				}

			}
		}
		return exportList;
	}

	@Override
	public boolean agentNameAlreadyExist(SSOToken token, String realm, String agentName) {
		validateTokenRealmAgentNameParams(token, realm, agentName);
		AMIdentity amIdentity = new AMIdentity(token, agentName, IdType.AGENTONLY, realm, null);
		try {
			return amIdentity.isExists();
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}

	}

	private String processExportLine(String key, String value) {
		String line = null;

		boolean processed = false;

		if (value.startsWith("[]")) {
			line = key + "[]" + value.replace("[]", "");
			processed = true;
		}
		if (!processed) {
			if (value.startsWith("[")) {
				int nextPosition = value.indexOf("]");
				if (nextPosition > 0) {
					String indexAsString = value.substring(1, nextPosition);
					try {
						int index = Integer.parseInt(indexAsString);
						line = key + "[" + index + "]" + value.substring(nextPosition + 1);
						processed = true;
					} catch (NumberFormatException e) {
						processed = false;
					}
				}
			}
		}

		if (!processed) {
			line = key + "=" + value;
		}

		return line;
	}

}
