/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.web.controllers.agents;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.frontend.configuration.domain.agents.J2EESSOBean;
import org.osiam.frontend.configuration.service.AgentsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * J2EEAgentSSOController shows the edit "J2EE Agent" (SSO attributes) form and
 * does request processing of the edit web agent action.
 * 
 */
@Controller("J2EEAgentSSOController")
@RequestMapping(value = "view", params = { "ctx=J2EEAgentSSOController" })
public class J2EEAgentSSOController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(J2EEAgentSSOController.class);

	@Autowired
	private AgentsService agentsService;

	/**
	 * Show edit form for a "J2EE Agent" - SSO attributes.
	 * 
	 * @param name
	 *            - Agent name
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name.
	 */
	@RenderMapping
	public String editAgent(String name, Model model, String message, RenderRequest request) {

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "j2eeAgent")) {

			String realm = getCurrentRealm(request);
			SSOToken token = getSSOToken(realm, request);
			J2EESSOBean agent = agentsService.getAttributesForJ2EESSO(token, realm, name);
			agent.setName(name);

			model.addAttribute("j2eeAgent", agent);
		}

		model.addAttribute("name", name);
		model.addAttribute("message", message);
		return "agents/j2ee/sso/edit";

	}

	/**
	 * Save new value in CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCdssoServletURL")
	public void doAddCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent, BindingResult result,
			String cdssoServletURLAddValue, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLAddValue != null && !cdssoServletURLAddValue.equals("")) {
			j2eeAgent.getCdssoServletURL().add(cdssoServletURLAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletedCdssoServletURL")
	public void doDeleteCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = 0; i < cdssoServletURLSelectValues.length; i++) {
				j2eeAgent.getCdssoServletURL().remove(cdssoServletURLSelectValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move up values from CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpCdssoServletURL")
	public void doMoveUpCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = 0; i < cdssoServletURLSelectValues.length; i++) {
				int poz = j2eeAgent.getCdssoServletURL().indexOf(cdssoServletURLSelectValues[i]);
				if (poz > 0) {
					j2eeAgent.setCdssoServletURL(moveUp(j2eeAgent.getCdssoServletURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move down values from CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownCdssoServletURL")
	public void doMoveDownCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			for (int i = cdssoServletURLSelectValues.length - 1; i >= 0; i--) {
				int poz = j2eeAgent.getCdssoServletURL().indexOf(cdssoServletURLSelectValues[i]);
				if (poz < j2eeAgent.getCdssoServletURL().size() - 1) {
					j2eeAgent.setCdssoServletURL(moveDown(j2eeAgent.getCdssoServletURL(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move top values from CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopCdssoServletURL")
	public void doMoveTopCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			j2eeAgent.setCdssoServletURL(moveTop(j2eeAgent.getCdssoServletURL(), cdssoServletURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move bottom values from CDSSO Servlet URL list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoServletURLSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomCdssoServletURL")
	public void doMoveBottomCdssoServletURL(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoServletURLSelectValues, ActionRequest request, ActionResponse response) {

		if (cdssoServletURLSelectValues != null) {
			j2eeAgent.setCdssoServletURL(moveBottom(j2eeAgent.getCdssoServletURL(), cdssoServletURLSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCdssoTrustedIDProvider")
	public void doAddCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String cdssoTrustedIDProviderAddValue, ActionRequest request, ActionResponse response) {

		if (cdssoTrustedIDProviderAddValue != null && !cdssoTrustedIDProviderAddValue.equals("")) {
			j2eeAgent.getCdssoTrustedIDProvider().add(cdssoTrustedIDProviderAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderSelectValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCdssoTrustedIDProvider")
	public void doDeleteCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoTrustedIDProviderSelectValues, ActionRequest request,
			ActionResponse response) {

		if (cdssoTrustedIDProviderSelectValues != null) {
			for (int i = 0; i < cdssoTrustedIDProviderSelectValues.length; i++) {
				j2eeAgent.getCdssoTrustedIDProvider().remove(cdssoTrustedIDProviderSelectValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move up values from CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveUpCdssoTrustedIDProvider")
	public void doMoveUpCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoTrustedIDProviderSelectValues, ActionRequest request,
			ActionResponse response) {

		if (cdssoTrustedIDProviderSelectValues != null) {
			for (int i = 0; i < cdssoTrustedIDProviderSelectValues.length; i++) {
				int poz = j2eeAgent.getCdssoTrustedIDProvider().indexOf(cdssoTrustedIDProviderSelectValues[i]);
				if (poz > 0) {
					j2eeAgent.setCdssoTrustedIDProvider(moveUp(j2eeAgent.getCdssoTrustedIDProvider(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move down values from CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveDownCdssoTrustedIDProvider")
	public void doMoveDownCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoTrustedIDProviderSelectValues, ActionRequest request,
			ActionResponse response) {

		if (cdssoTrustedIDProviderSelectValues != null) {
			for (int i = cdssoTrustedIDProviderSelectValues.length - 1; i >= 0; i--) {
				int poz = j2eeAgent.getCdssoTrustedIDProvider().indexOf(cdssoTrustedIDProviderSelectValues[i]);
				if (poz < j2eeAgent.getCdssoTrustedIDProvider().size() - 1) {
					j2eeAgent.setCdssoTrustedIDProvider(moveDown(j2eeAgent.getCdssoTrustedIDProvider(), poz));
				}
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move top values from CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveTopCdssoTrustedIDProvider")
	public void doMoveTopCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoTrustedIDProviderSelectValues, ActionRequest request,
			ActionResponse response) {

		if (cdssoTrustedIDProviderSelectValues != null) {
			j2eeAgent.setCdssoTrustedIDProvider(moveTop(j2eeAgent.getCdssoTrustedIDProvider(),
					cdssoTrustedIDProviderSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Move bottom values from CDSSO Trusted ID Provider list.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoTrustedIDProviderSelectValues
	 *            - {@link String} values to be move up
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "moveBottomCdssoTrustedIDProvider")
	public void doMoveBottomCdssoTrustedIDProvider(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoTrustedIDProviderSelectValues, ActionRequest request,
			ActionResponse response) {

		if (cdssoTrustedIDProviderSelectValues != null) {
			j2eeAgent.setCdssoTrustedIDProvider(moveBottom(j2eeAgent.getCdssoTrustedIDProvider(),
					cdssoTrustedIDProviderSelectValues));
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in CDSSO Domain List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoDomainListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCdssoDomainList")
	public void doAddCdssoDomainList(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent, BindingResult result,
			String cdssoDomainListAddValue, ActionRequest request, ActionResponse response) {

		if (cdssoDomainListAddValue != null && !cdssoDomainListAddValue.equals("")) {
			j2eeAgent.getCdssoDomainList().add(cdssoDomainListAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from CDSSO Domain List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cdssoDomainListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCdssoDomainList")
	public void doDeleteCdssoDomainList(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cdssoDomainListDeleteValues, ActionRequest request, ActionResponse response) {

		if (cdssoDomainListDeleteValues != null) {
			for (int i = 0; i < cdssoDomainListDeleteValues.length; i++) {
				j2eeAgent.getCdssoDomainList().remove(cdssoDomainListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Cookies Reset Name List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetNameListAddValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCookiesResetNameList")
	public void doAddCookiesResetNameList(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String cookiesResetNameListAddValue, ActionRequest request, ActionResponse response) {

		if (cookiesResetNameListAddValue != null && !cookiesResetNameListAddValue.equals("")) {
			j2eeAgent.getCookiesResetNameList().add(cookiesResetNameListAddValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Cookies Reset Name List.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetNameListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCookiesResetNameList")
	public void doDeleteCookiesResetNameList(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cookiesResetNameListDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (cookiesResetNameListDeleteValues != null) {
			for (int i = 0; i < cookiesResetNameListDeleteValues.length; i++) {
				j2eeAgent.getCookiesResetNameList().remove(cookiesResetNameListDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Cookies Reset Domain Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetDomainMapKey
	 *            - {@link String} key to be insert
	 * @param cookiesResetDomainMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCookiesResetDomainMap")
	public void doAddCookiesResetDomainMap(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String cookiesResetDomainMapKey, String cookiesResetDomainMapValue,
			ActionRequest request, ActionResponse response) {

		if (cookiesResetDomainMapKey != null && !cookiesResetDomainMapKey.equals("")
				&& cookiesResetDomainMapValue != null && !cookiesResetDomainMapValue.equals("")) {
			j2eeAgent.getCookiesResetDomainMap()
					.add("[" + cookiesResetDomainMapKey + "]=" + cookiesResetDomainMapValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Cookies Reset Domain Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetDomainMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCookiesResetDomainMap")
	public void doDeleteCookiesResetDomainMap(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cookiesResetDomainMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (cookiesResetDomainMapDeleteValues != null) {
			for (int i = 0; i < cookiesResetDomainMapDeleteValues.length; i++) {
				j2eeAgent.getCookiesResetDomainMap().remove(cookiesResetDomainMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save new value in Cookies Reset Path Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetPathMapKey
	 *            - {@link String} key to be insert
	 * @param cookiesResetPathMapValue
	 *            - {@link String} value to be insert
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCookiesResetPathMap")
	public void doAddCookiesResetPathMap(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String cookiesResetPathMapKey, String cookiesResetPathMapValue,
			ActionRequest request, ActionResponse response) {

		if (cookiesResetPathMapKey != null && !cookiesResetPathMapKey.equals("") && cookiesResetPathMapValue != null
				&& !cookiesResetPathMapValue.equals("")) {
			j2eeAgent.getCookiesResetPathMap().add("[" + cookiesResetPathMapKey + "]=" + cookiesResetPathMapValue);
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Delete values from Cookies Reset Path Map.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param cookiesResetPathMapDeleteValues
	 *            - {@link String} values to be deleted
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCookiesResetPathMap")
	public void doDeleteCookiesResetPathMap(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent,
			BindingResult result, String[] cookiesResetPathMapDeleteValues, ActionRequest request,
			ActionResponse response) {

		if (cookiesResetPathMapDeleteValues != null) {
			for (int i = 0; i < cookiesResetPathMapDeleteValues.length; i++) {
				j2eeAgent.getCookiesResetPathMap().remove(cookiesResetPathMapDeleteValues[i]);
			}
		}

		response.setRenderParameter("name", j2eeAgent.getName());
	}

	/**
	 * Save {@link J2EESSOBean}.
	 * 
	 * @param j2eeAgent
	 *            - {@link J2EESSOBean}
	 * @param result
	 *            - BindingResult
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveJ2EEAgentGlobal(@ModelAttribute("j2eeAgent") @Valid J2EESSOBean j2eeAgent, BindingResult result,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", j2eeAgent.getName());

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);
		agentsService.saveJ2EESSO(token, realm, j2eeAgent);
		response.setRenderParameter("message", "message.prifile_was_updated");
	}

	/**
	 * Reset {@link J2EESSOBean} form.
	 * 
	 * @param name
	 *            - Agent name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetWebAgentGlobal(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to agent list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AgentsController");
	}

}
