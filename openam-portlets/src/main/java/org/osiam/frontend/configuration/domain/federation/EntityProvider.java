/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.map.LazyMap;
import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.exceptions.FederationException;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.CollectionUtils;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;

import com.sun.identity.saml2.meta.SAML2MetaException;

/**
 * 
 * Encapsulates data for an entity provider.
 * 
 */
public class EntityProvider implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(EntityProvider.class);

	@NotNull
	@Size(min = 1)
	private String realm;

	@NotNull
	@Size(min = 1)
	private String entityIdentifier;

	@NotNull
	@Size(min = 1)
	private String protocol;

	private String location;

	/**
	 * Keeps the list of {@link EntityProviderDescriptor}. Each
	 * {@link EntityProviderDescriptor} describes on function of the
	 * {@link EntityProvider} (e.g. is one tab in the user interface).
	 */
	@SuppressWarnings("unchecked")
	private Map<String, EntityProviderDescriptor> descriptorMap = LazyMap.decorate(
			new HashMap<String, EntityProviderDescriptor>(), new Factory() {

				@Override
				public Object create() {
					return new EntityProviderDescriptor();
				}
			});

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getProtocol() {
		return protocol;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getRealm() {
		return realm;
	}

	public String getEntityIdentifier() {
		return entityIdentifier;
	}

	public void setEntityIdentifier(String entityIdentifier) {
		this.entityIdentifier = entityIdentifier;
	}

	public Map<String, EntityProviderDescriptor> getDescriptorMap() {
		return descriptorMap;
	}

	public void setDescriptorMap(Map<String, EntityProviderDescriptor> descriptorMap) {
		this.descriptorMap = descriptorMap;
	}

	/**
	 * Creates an entity provider with given name, protocol, list of types and
	 * location.
	 * 
	 * @param realm
	 *            - the realm
	 * @param protocol
	 *            - the protocol
	 * @param location
	 *            - the location
	 */
	public EntityProvider(String protocol, String realm, String location) {
		this.protocol = protocol;
		this.realm = realm;
		this.location = location;
	}

	/**
	 * Creates an {@link EntityProvider} initialized with given field.
	 * 
	 * @param protocol
	 *            - the protocol
	 * @param realm
	 *            - the realm
	 */
	public EntityProvider(String protocol, String realm) {
		this.protocol = protocol;
		this.realm = realm;
	}

	/**
	 * Creates an empty {@link EntityProvider}.
	 */
	public EntityProvider() {

	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param attributes
	 *            - map of attributes.
	 * @throws UnsupportedEncodingException
	 *             - when given XML contained in the map cannot be parsed due to
	 *             wrong encoding.
	 * 
	 */
	public static EntityProvider fromMap(Map<String, Set<String>> attributes) throws UnsupportedEncodingException {
		String realm = stringFromSet(attributes.get(FederationConstants.REALM));

		EntityProvider bean = null;
		if (attributes.containsKey(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {

			bean = new EntityProvider(FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, realm);

			String entityConfig = stringFromSet(attributes.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY));

			if (entityConfig != null && entityConfig.length() != 0) {
				Document document = XmlUtil.toDOMDocument(entityConfig);
				bean.setLocation(getLocation(document));

				bean.setEntityIdentifier(XmlUtil.getNodeAttributeValue(document.getFirstChild(),
						FederationConstants.ENTITY_IDENTIFIER_ATTRIBUTE));

			}

			bean.setDescriptorMap(EntityProviderDescriptor.fromAttributeMaptoMap(attributes));

		} else if (attributes.containsKey(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY)) {

			bean = new EntityProvider(FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL, realm);

			String entityConfig = stringFromSet(attributes.get(FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY));

			if (entityConfig != null && entityConfig.length() != 0) {
				Document document = XmlUtil.toDOMDocument(entityConfig);
				bean.setLocation(getLocation(document));
				bean.setEntityIdentifier(XmlUtil.getNodeAttributeValue(document.getFirstChild(), "FederationID"));

			}

			bean.setDescriptorMap(EntityProviderDescriptor.fromAttributeMaptoMap(attributes));

		}

		return bean;
	}

	/**
	 * @return map with object attributes, ready to be persisted.
	 * @throws CertificateEncodingException 
	 */
	public Map<String, Set<String>> toMap() throws CertificateEncodingException {
		if (FederationConstants.ENTITY_PROVIDER_SAMLV2_PROTOCOL.equals(protocol)) {
			return toSAMLV2Map();
		} else {
			return toWSFedMap();
		}
	}

	private boolean isDefaultData() {
		Iterator<EntityProviderDescriptor> iterator = getDescriptorMap().values().iterator();
		while (iterator.hasNext()) {
			EntityProviderDescriptor entityProviderDescriptor = iterator.next();
			if (EntityProviderDescriptor.ENTITY_PROVIDER.equals(entityProviderDescriptor.getEntityTypeKey())) {
				return true;
			}
		}
		return false;
	}

	private Map<String, Set<String>> toSAMLV2Map() throws CertificateEncodingException {
		Map<String, Set<String>> returnMap = new HashMap<String, Set<String>>();

		String entityConfigKey = FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY;
		String metadataKey = FederationConstants.SAML2_METADATA_ATTRIBUTE_KEY;

		String xmlConfigString = null;
		String xmlMetadataString = null;

		if (isDefaultData()) {
			boolean hosted;
			if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
				hosted = false;
			} else {
				hosted = true;
			}
			xmlConfigString = SAMLV2DefaultDataCreator.createDefaultConfigDataTemplate(getEntityIdentifier(), realm,
					null, hosted, getDescriptorMap(), false);
			try {
				xmlMetadataString = SAMLV2DefaultDataCreator.createDefaultMetadataTemplate(getEntityIdentifier(),
						realm, null, getDescriptorMap(), false);
			} catch (SAML2MetaException e) {
				LOGGER.error(e);
				throw new FederationException(e.getMessage());
			}
		} else {
			Iterator<EntityProviderDescriptor> iterator = getDescriptorMap().values().iterator();
			Map<String, Document> nodesMap = new HashMap<String, Document>();
			Document metadataDocument = XmlUtil.createEmptyDOMDocument();
			Document configDocument = XmlUtil.createEmptyDOMDocument();

			Map<String, String> configAttributes = new HashMap<String, String>();
			configAttributes.put(FederationConstants.ENTITY_IDENTIFIER_ATTRIBUTE, getEntityIdentifier());
			String hosted = null;
			if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
				hosted = "false";
			} else {
				hosted = "true";
			}
			configAttributes.put(FederationConstants.HOSTED_ATTRIBUTE, hosted);
			configAttributes.put(FederationConstants.XMLNS_ATTRIBUTE, "urn:sun:fm:SAML:2.0:entityconfig");
			XmlUtil.createElement(configDocument, FederationConstants.ENTITY_CONFIG_TAG, configAttributes);

			Map<String, String> metadataAttributes = new HashMap<String, String>();
			metadataAttributes.put(FederationConstants.ENTITY_IDENTIFIER_ATTRIBUTE, getEntityIdentifier());

			metadataAttributes.put(FederationConstants.XMLNS_ATTRIBUTE, "urn:oasis:names:tc:SAML:2.0:metadata");

			XmlUtil.createElement(metadataDocument, FederationConstants.ENTITY_DESCRIPTOR_TAG, metadataAttributes);

			nodesMap.put(entityConfigKey, configDocument);
			nodesMap.put(metadataKey, metadataDocument);

			while (iterator.hasNext()) {
				EntityProviderDescriptor entityProviderDescriptor = iterator.next();
				entityProviderDescriptor.addAllDatatoMap(nodesMap);
			}
			xmlConfigString = XmlUtil.getString(nodesMap.get((entityConfigKey)));
			xmlMetadataString = XmlUtil.getString(nodesMap.get((metadataKey)));
		}

		returnMap.put(entityConfigKey, CollectionUtils
				.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xmlConfigString));
		returnMap.put(
				metadataKey,
				CollectionUtils.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
						+ xmlMetadataString));

		return returnMap;
	}

	private Map<String, Set<String>> toWSFedMap() throws CertificateEncodingException {
		Map<String, Set<String>> returnMap = new HashMap<String, Set<String>>();
		String xmlConfigString = null;
		String xmlMetadataString = null;

		String entityConfigKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_KEY;
		String metadataKey = FederationConstants.WSF_ENTITY_PROVIDER_ATTRIBUTE_METADATA_KEY;

		if (isDefaultData()) {
			boolean hosted;
			if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
				hosted = false;
			} else {
				hosted = true;
			}
			xmlConfigString = WSFedDefaultDataCreator.createDefaultConfigDataTemplate(getEntityIdentifier(), realm,
					null, hosted, getDescriptorMap(), false);
			try {
				xmlMetadataString = WSFedDefaultDataCreator.createDefaultMetadataTemplate(getEntityIdentifier(), realm,
						null, getDescriptorMap());
			} catch (SAML2MetaException e) {
				LOGGER.error(e);
				throw new FederationException(e.getMessage());
			}
		} else {
			Iterator<EntityProviderDescriptor> iterator = getDescriptorMap().values().iterator();
			Map<String, Document> nodesMap = new HashMap<String, Document>();
			Document metadataDocument = XmlUtil.createEmptyDOMDocument();
			Document configDocument = XmlUtil.createEmptyDOMDocument();

			Map<String, String> configAttributes = new HashMap<String, String>();
			configAttributes.put("FederationID", getEntityIdentifier());
			String hosted = null;
			if (FederationConstants.REMOTE_VALUE.equals(getLocation())) {
				hosted = "false";
			} else {
				hosted = "true";
			}
			configAttributes.put(FederationConstants.HOSTED_ATTRIBUTE, hosted);
			configAttributes.put(FederationConstants.XMLNS_ATTRIBUTE, "urn:sun:fm:wsfederation:1.0:federationconfig");
			XmlUtil.createElement(configDocument, "FederationConfig", configAttributes);

			Map<String, String> metadataAttributes = new HashMap<String, String>();
			metadataAttributes.put("FederationID", getEntityIdentifier());

			metadataAttributes.put(FederationConstants.XMLNS_ATTRIBUTE,
					"http://schemas.xmlsoap.org/ws/2006/12/federation");

			XmlUtil.createElement(metadataDocument, "Federation", metadataAttributes);

			nodesMap.put(entityConfigKey, configDocument);
			nodesMap.put(metadataKey, metadataDocument);

			while (iterator.hasNext()) {
				EntityProviderDescriptor entityProviderDescriptor = iterator.next();
				entityProviderDescriptor.addAllDatatoMap(nodesMap);
			}
			xmlConfigString = XmlUtil.getString(nodesMap.get((entityConfigKey)));
			xmlMetadataString = XmlUtil.getString(nodesMap.get((metadataKey)));

		}

		returnMap.put(entityConfigKey, CollectionUtils
				.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xmlConfigString));

		returnMap.put(
				metadataKey,
				CollectionUtils.asSet("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
						+ xmlMetadataString));

		return returnMap;
	}

	private static String getLocation(Document document) throws UnsupportedEncodingException {

		if (document == null || document.getFirstChild() == null) {
			return null;
		}

		String hosted = XmlUtil.getNodeAttributeValue(document.getFirstChild(), "hosted");
		if (Boolean.parseBoolean(hosted)) {
			return "Hosted";
		} else {
			return "Remote";
		}

	}

}
