/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.federation;

import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Encapsulates data for a SAMLV2 IDP Advanced bean.
 * 
 */
public class SAMLV2IDPAdvanced {

	private static final Logger LOGGER = Logger.getLogger(SAMLV2IDPAdvanced.class);

	private String saeConfigurationIDPURL;
	private List<String> saeConfigurationApplicationSecurityConfigurationCurrentValues = new ArrayList<String>();

	private String ecpConfigurationIDPSessionMapper;

	private boolean sessionSynchronizationEnabled;

	private List<String> relayStateURLList = new ArrayList<String>();

	public String getSaeConfigurationIDPURL() {
		return saeConfigurationIDPURL;
	}

	public void setSaeConfigurationIDPURL(String saeConfigurationIDPURL) {
		this.saeConfigurationIDPURL = saeConfigurationIDPURL;
	}

	public List<String> getSaeConfigurationApplicationSecurityConfigurationCurrentValues() {
		return saeConfigurationApplicationSecurityConfigurationCurrentValues;
	}

	public void setSaeConfigurationApplicationSecurityConfigurationCurrentValues(
			List<String> saeConfigurationApplicationSecurityConfigurationCurrentValues) {
		this.saeConfigurationApplicationSecurityConfigurationCurrentValues = saeConfigurationApplicationSecurityConfigurationCurrentValues;
	}

	public String getEcpConfigurationIDPSessionMapper() {
		return ecpConfigurationIDPSessionMapper;
	}

	public void setEcpConfigurationIDPSessionMapper(String ecpConfigurationIDPSessionMapper) {
		this.ecpConfigurationIDPSessionMapper = ecpConfigurationIDPSessionMapper;
	}

	public Boolean getSessionSynchronizationEnabled() {
		return sessionSynchronizationEnabled;
	}

	public void setSessionSynchronizationEnabled(Boolean sessionSynchronizationEnabled) {
		this.sessionSynchronizationEnabled = sessionSynchronizationEnabled;
	}

	public List<String> getRelayStateURLList() {
		return relayStateURLList;
	}

	public void setRelayStateURLList(List<String> relayStateURLList) {
		this.relayStateURLList = relayStateURLList;
	}

	/**
	 * Parse XML attributes map and creates {@link EntityProviderDescriptor}
	 * instance based on that.
	 * 
	 * @param map
	 *            - XML attributes map, as read from the persistence layer.
	 * @return {@link EntityProviderDescriptor} instance based on the given
	 *         attributes map.
	 * @throws UnsupportedEncodingException
	 *             - when the given XML cannot be parsed due to wrong encoding.
	 */
	public static SAMLV2IDPAdvanced fromMap(Map<String, Set<String>> map) throws UnsupportedEncodingException {

		SAMLV2IDPAdvanced bean = new SAMLV2IDPAdvanced();

		Document document = XmlUtil.toDOMDocument(stringFromSet(map
				.get(FederationConstants.SAML2_ENTITY_PROVIDER_ATTRIBUTE_KEY)));

		Node rootNode = document.getFirstChild();
		Set<Node> configNodeSet = XmlUtil.getChildNodes(rootNode, FederationConstants.IDPSSO_CONFIG_NODE);

		Node configNode = configNodeSet.iterator().next();
		NodeList configChildNodeList = configNode.getChildNodes();
		for (int i = 0; i < configChildNodeList.getLength(); i++) {

			Node node = configChildNodeList.item(i);
			if (node.getNodeType() == Node.TEXT_NODE) {
				continue;
			}
			if (!node.getNodeName().equals(XmlUtil.ATTRIBUTE_NODE)) {
				continue;
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute node \n\r %s", XmlUtil.getString(node)));
			}

			String attributeName = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Attribute name: %s", attributeName));
			}

			if (attributeName.equals(FederationConstants.SAE_CONFIGURATION_IDP_URL)) {
				bean.setSaeConfigurationIDPURL(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName
					.equals(FederationConstants.SAE_CONFIGURATION_APPLICATION_SECURITY_CONFIGURATION_CURRENT_VALUES)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setSaeConfigurationApplicationSecurityConfigurationCurrentValues((valuePair.getValueList()));
			} else if (attributeName.equals(FederationConstants.RELAY_STATE_IDP_URL_LIST)) {
				AttributeValuePair valuePair = XmlUtil.getAttributeValue(node);
				bean.setRelayStateURLList((valuePair.getValueList()));
			} else if (attributeName.equals(FederationConstants.ECP_CONFIGURATION_IDP_SESSION_MAPPER)) {
				bean.setEcpConfigurationIDPSessionMapper(XmlUtil.getValueFromAttributeValue(node));
			} else if (attributeName.equals(FederationConstants.SESSION_SYNCHRONIZATION_ENABLED)) {
				bean.setSessionSynchronizationEnabled(XmlUtil.getBooleanValueFromAttributeValue(node));

			}

		}

		return bean;
	}

	
}
