/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.configuration.domain.datastores;

import static org.osiam.frontend.util.CollectionUtils.asSet;
import static org.osiam.frontend.util.CollectionUtils.stringFromSet;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import org.osiam.frontend.configuration.service.DataStoresConstants;

/**
 * LDAPv3ForADBean object which defines attributes for a "Active Directory"
 * {@link DataStore}.
 * 
 */
public class LDAPv3ForADBean extends LDAPv3DataStore implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userStatusActiveValue;
	private String userStatusInactiveValue;

	public String getUserStatusActiveValue() {
		return userStatusActiveValue;
	}

	public void setUserStatusActiveValue(String userStatusActiveValue) {
		this.userStatusActiveValue = userStatusActiveValue;
	}

	public String getUserStatusInactiveValue() {
		return userStatusInactiveValue;
	}

	public void setUserStatusInactiveValue(String userStatusInactiveValue) {
		this.userStatusInactiveValue = userStatusInactiveValue;
	}

	/**
	 * 
	 * @return attributes map with the current bean attribute values.
	 */
	public Map<String, Set<String>> toMap() {
		Map<String, Set<String>> map = super.toMap();
		map.put(DataStoresConstants.USER_STATUS_ACTIVE_VALUE, asSet(getUserStatusActiveValue()));
		map.put(DataStoresConstants.USER_STATUS_INACTIVE_VALUE, asSet(getUserStatusInactiveValue()));

		return map;
	}

	/**
	 * @return bean created be parsing given attribute map.
	 * 
	 * @param map
	 *            - map of attributes.
	 * 
	 */
	public static LDAPv3ForADBean fromMap(Map<String, Set<String>> map) {
		LDAPv3ForADBean bean = new LDAPv3ForADBean();
		LDAPv3DataStore.setBaseFields(map, bean);

		bean.setUserStatusActiveValue(stringFromSet(map.get(DataStoresConstants.USER_STATUS_ACTIVE_VALUE)));
		bean.setUserStatusInactiveValue(stringFromSet(map.get(DataStoresConstants.USER_STATUS_INACTIVE_VALUE)));

		return bean;
	}

}
