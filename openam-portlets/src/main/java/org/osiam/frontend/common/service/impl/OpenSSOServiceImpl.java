/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service.impl;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;

import org.apache.log4j.Logger;
import org.osiam.frontend.common.exceptions.OSIAMException;
import org.osiam.frontend.common.service.OpenSSOService;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.authentication.AuthContext;
import com.sun.identity.authentication.spi.AuthLoginException;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceManager;

/**
 * This class encapsulates methods for using the open SSO service.
 * 
 */
public class OpenSSOServiceImpl implements OpenSSOService {

	private static final Logger LOGGER = Logger.getLogger(OpenSSOServiceImpl.class);

	private String username;
	private String password;

	private static Long lastCacheResetMillis = System.currentTimeMillis();

	private Long maxCacheRefreshIntervalSecs;

	@Override
	public SSOToken authenticate(String organization) throws OSIAMException {
		AuthContext authContext;
		try {
			authContext = new AuthContext("/");
			// authContext = new AuthContext(organization);
			authContext.login();
		} catch (AuthLoginException e1) {
			throw new OSIAMException(e1.getMessage(), e1);
		}
		while (authContext.hasMoreRequirements()) {
			Callback[] callbacks = authContext.getRequirements();
			for (int i = 0; i < callbacks.length; i++) {
				if (callbacks[i] instanceof NameCallback) {
					NameCallback nc = (NameCallback) callbacks[i];
					nc.setName(username);
				} else if (callbacks[i] instanceof PasswordCallback) {
					PasswordCallback pc = (PasswordCallback) callbacks[i];
					pc.setPassword(password.toCharArray());
				}
			}
			authContext.submitRequirements(callbacks);
		}

		if (authContext.getStatus() != AuthContext.Status.SUCCESS) {
			LOGGER.info(String.format("Login fail for organization %s", organization));
			throw new OSIAMException("Login fail");
		} else {
			LOGGER.info(String.format("Login success for organization %s", organization));
			try {
				return authContext.getSSOToken();
			} catch (Exception e) {
				throw new OSIAMException(e.getMessage());
			}
		}
	}

	@Override
	public void resetCache(SSOToken token) throws SSOException, SMSException {
		if (System.currentTimeMillis() - lastCacheResetMillis > maxCacheRefreshIntervalSecs) {
			ServiceManager service = new ServiceManager(token);
			service.clearCache();
			lastCacheResetMillis = System.currentTimeMillis();
		}
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMaxCacheRefreshIntervalSecs(Long maxCacheRefreshInterval) {
		this.maxCacheRefreshIntervalSecs = maxCacheRefreshInterval * 1000;
	}

}
