/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service.impl;

import com.iplanet.sso.SSOToken;

/**
 * Services working with OpenSSO extend this class.
 * 
 */
public abstract class BaseSSOService {

	/**
	 * Validate token and realm.
	 * 
	 * @param token
	 *            - the sso token
	 * @param realm
	 *            - the realm
	 * @throws IllegalArgumentException
	 *             if token or realm is null.
	 */
	protected void validateTokenRealmParams(SSOToken token, String realm) throws IllegalArgumentException {
		if (token == null) {
			throw new IllegalArgumentException(
					"Param token cannot be null. Obtain first the token object using an OpenSSOService implementation.");
		}
		if (realm == null) {
			throw new IllegalArgumentException(String.format("Param realm cannot be null. Example : %s", "'/'"));
		}
	}

}
