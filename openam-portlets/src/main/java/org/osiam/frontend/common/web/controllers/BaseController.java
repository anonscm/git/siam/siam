/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;

import org.osiam.frontend.common.exceptions.OSIAMException;
import org.osiam.frontend.common.service.OpenSSOService;
import org.osiam.frontend.common.service.RealmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.portlet.ModelAndView;

import com.iplanet.sso.SSOToken;

/**
 * Contains methods used in portlets controllers.
 * 
 */

public class BaseController {

	private static final String SSO_TOKEN_MAP = "ssoTokenMap";

	@Autowired
	private OpenSSOService openSSOService;

	@Autowired
	private RealmService realmService;

	@ExceptionHandler()
	public ModelAndView handleFunctionalException(Exception exception, PortletRequest request) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("error");
		mav.addObject("exception", exception);
		if (exception instanceof OSIAMException) {
			resetTokenRealmMap(request);
		}
		return mav;
	}

	/**
	 * 
	 * Use this method to reset the map when the user tokens are kept. Do this
	 * every time you catch an exception that notifies about the token being
	 * invalid or expired.
	 * 
	 * @param request
	 *            - the portlet request
	 */
	protected void resetTokenRealmMap(PortletRequest request) {
		request.getPortletSession().setAttribute(SSO_TOKEN_MAP, null);
	}

	/**
	 * @return {@link SSOToken} the openSSO token.
	 * 
	 * @param realm
	 *            - the realm
	 * @param request
	 *            - the request
	 * @throws OSIAMException
	 *             - when something wrong happens.
	 */
	protected SSOToken getSSOToken(String realm, PortletRequest request) throws OSIAMException {
		@SuppressWarnings("unchecked")
		Map<String, SSOToken> tokenRealmMap = (Map<String, SSOToken>) request.getPortletSession().getAttribute(
				SSO_TOKEN_MAP);

		SSOToken ssoToken;

		if (tokenRealmMap == null) {
			tokenRealmMap = new HashMap<String, SSOToken>();
		}
		ssoToken = tokenRealmMap.get(realm);
		for (int i = 0; i < 5; i++) {
			if (ssoToken == null) {
				ssoToken = openSSOService.authenticate(realm);
				tokenRealmMap.put(realm, ssoToken);
				request.getPortletSession().setAttribute(SSO_TOKEN_MAP, tokenRealmMap);
			}
			if (validate(ssoToken)) {
				break;
			} else {
				ssoToken = null;
			}
		}

		if (ssoToken == null) {
			throw new OSIAMException("Token not initialised.");
		}

		return ssoToken;
	}

	/**
	 * Validate token and reset cache.
	 * 
	 * @param token - SSOToken
	 * @return true if token is valid
	 */
	private boolean validate(SSOToken token) {
		try {
			realmService.getRealmSet(token);
			openSSOService.resetCache(token);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * @return String the realm the portlet is configured on
	 * @param request
	 *            - the request
	 * @throws OSIAMException
	 *             - when something wrong happens.
	 */
	protected String getCurrentRealm(PortletRequest request) throws OSIAMException {
		String realm = request.getPreferences().getValue("realm", null);
		if (realm == null || realm.length() == 0) {
			throw new OSIAMException("error.portlet_not_configured");
		}
		return realm;
	}

	/**
	 * Move up an element from a list.
	 * 
	 * @param list
	 *            - {@link List}
	 * @param index
	 *            - position to element to move
	 * @return the list reordered
	 */
	protected List<String> moveUp(List<String> list, int index) {
		String value = list.get(index);
		list.set(index, list.get(index - 1));
		list.set(index - 1, value);
		return list;
	}

	/**
	 * Move down an element from a list.
	 * 
	 * @param list
	 *            - {@link List}
	 * @param index
	 *            - position to element to move
	 * @return the list reordered
	 */
	protected List<String> moveDown(List<String> list, int index) {
		String value = list.get(index);
		list.set(index, list.get(index + 1));
		list.set(index + 1, value);
		return list;
	}

	/**
	 * Move top elements from a list.
	 * 
	 * @param list
	 *            - {@link List}
	 * @param selectList
	 *            - {@link List} whit elements to move
	 * @return the list reordered
	 */
	protected List<String> moveTop(List<String> list, String[] selectList) {

		List<String> newList = new ArrayList<String>();
		for (int i = 0; i < selectList.length; i++) {
			newList.add(selectList[i]);
		}

		for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			if (!newList.contains(string)) {
				newList.add(string);
			}
		}

		return newList;
	}

	/**
	 * Move bottom elements from a list.
	 * 
	 * @param list
	 *            - {@link List}
	 * @param selectList
	 *            - {@link List} whit elements to move
	 * @return the list reordered
	 */
	protected List<String> moveBottom(List<String> list, String[] selectList) {

		List<String> newList = new ArrayList<String>();

		List<String> newListToBootom = new ArrayList<String>();
		for (int i = 0; i < selectList.length; i++) {
			newListToBootom.add(selectList[i]);
		}

		for (Iterator<String> iterator = list.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			if (!newListToBootom.contains(string)) {
				newList.add(string);
			}
		}

		newList.addAll(newListToBootom);

		return newList;
	}

}
