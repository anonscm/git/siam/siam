/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.RealmException;
import org.osiam.frontend.common.service.RealmService;
import org.osiam.frontend.configuration.domain.realms.NameDNRealm;
import org.springframework.stereotype.Service;

import com.iplanet.sso.SSOToken;
import com.sun.identity.sm.OrganizationConfigManager;
import com.sun.identity.sm.SMSException;

/**
 * This class encapsulates methods for using realm set.
 */
@Service("RealmService")
public class RealmServiceImpl extends BaseSSOService implements RealmService {

	private static final Logger LOGGER = Logger.getLogger(RealmServiceImpl.class);

	private static final String TOP_REALM = "/";

	@Override
	public Set<String> getRealmSet(SSOToken token) throws RealmException {

		return getRealmSet(token, "*");
	}

	@Override
	public Set<String> getRealmSet(SSOToken token, String filter) throws RealmException {

		Set<String> realmSet = new TreeSet<String>();
		try {
			OrganizationConfigManager ocm = new OrganizationConfigManager(token, TOP_REALM);
			realmSet.add("/");

			@SuppressWarnings("unchecked")
			Set<String> subRealmSet = ocm.getSubOrganizationNames(filter, true);

			for (String real : subRealmSet) {
				// add a slash before the name of the realm . Ex /yahoo.com
				realmSet.add(String.format("/%s", real));
			}

			if (LOGGER.isDebugEnabled()) {
				StringBuilder builder = new StringBuilder();
				int index = 0;
				for (String realm : realmSet) {
					if (index != 0) {
						builder.append(",");
					}
					builder.append(realm);
					index++;
				}
				LOGGER.debug(String.format("Realm set : %s", builder.toString()));

			}
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new RealmException(e.getMessage(), e);
		}

		return realmSet;
	}

	@Override
	public Set<NameDNRealm> getRealmChildrenSet(SSOToken token, String parentRealm, String filter)
			throws RealmException {
		return toNameDNRealmSet(getRealmChildrenNameSet(token, parentRealm, filter));
	}

	private Set<String> getRealmChildrenNameSet(SSOToken token, String parentRealm, String filter)
			throws RealmException {
		Set<String> realmSet = getRealmSet(token, filter);
		Iterator<String> realmIterator = realmSet.iterator();
		removeNonChildren(realmIterator, parentRealm);
		return realmSet;
	}

	private Set<NameDNRealm> toNameDNRealmSet(Set<String> realmNames) {
		Set<NameDNRealm> nameDNRealms = new HashSet<NameDNRealm>(realmNames.size());
		Iterator<String> realmNamesIterator = realmNames.iterator();
		while (realmNamesIterator.hasNext()) {
			nameDNRealms.add(new NameDNRealm(realmNamesIterator.next()));
		}
		return nameDNRealms;
	}

	private void removeNonChildren(Iterator<String> realmIterator, String parentRealm) {
		int parentSlashCount = countSlashes(parentRealm);
		while (realmIterator.hasNext()) {
			String realm = realmIterator.next();
			if (!realm.startsWith(parentRealm)) {
				realmIterator.remove();
			} else {
				if (!(realm.length() > parentRealm.length())) {
					realmIterator.remove();
				} else {
					int i = countSlashes(realm) - parentSlashCount;
					if (parentRealm.length() > 1) {
						if (i > 1) {
							realmIterator.remove();
						}
					} else if (i > 0) {
						realmIterator.remove();
					}

				}
			}
		}
	}

	@Override
	public Set<NameDNRealm> getRealmSiblingsSet(SSOToken token, String realm, String filter) throws RealmException {
		if (realm.length() > 1) {
			int slashIndex = realm.lastIndexOf("/");
			String parentRealm = realm.substring(0, slashIndex + 1);
			int parentSlashCount = countSlashes(realm);
			Set<String> siblingsSet = getRealmChildrenNameSet(token, parentRealm, filter);
			siblingsSet.remove(realm);
			Iterator<String> siblingIterator = siblingsSet.iterator();
			while (siblingIterator.hasNext()) {
				String sibling = siblingIterator.next();
				if (parentSlashCount != countSlashes(sibling)) {
					siblingIterator.remove();
				}
			}
			return toNameDNRealmSet(siblingsSet);
		}
		return new TreeSet<NameDNRealm>();
	}

	private int countSlashes(String str) {
		int slashCount = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == '/') {
				slashCount++;
			}
		}
		return slashCount;
	}
}
