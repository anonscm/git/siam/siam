/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service;

import org.osiam.frontend.common.exceptions.OSIAMException;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.sm.SMSException;

/**
 * This class encapsulates methods for using the open SSO service.
 * 
 */
public interface OpenSSOService {

	/**
	 * Authentication is made using user, pass and organization (realm).
	 * 
	 * @param organization
	 *            - the organization
	 * @return SSOToken
	 * 
	 * @throws OSIAMException
	 *             - when something wrong happens.
	 */
	SSOToken authenticate(String organization) throws OSIAMException;

	/**
	 * Reset cache.
	 * 
	 * @param token
	 *            SSOToken
	 * @throws SSOException
	 * @throws SMSException
	 */
	void resetCache(SSOToken token) throws SSOException, SMSException;

}
