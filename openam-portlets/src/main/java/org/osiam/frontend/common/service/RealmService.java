/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.service;

import java.util.Set;

import org.osiam.frontend.am.exceptions.RealmException;
import org.osiam.frontend.configuration.domain.realms.NameDNRealm;

import com.iplanet.sso.SSOToken;

/**
 * This class encapsulates methods for using realm set.
 */
public interface RealmService {

	/**
	 * Retrieve the realm set . The method will retrieve all the realm set under
	 * the top realm.
	 * 
	 * @param token
	 *            OpenSSO token.
	 * @return the realm set.
	 * @throws RealmException
	 *             - when something wrong happens.
	 */
	Set<String> getRealmSet(SSOToken token) throws RealmException;

	/**
	 * Retrieve the realm siblings. The method will retrieve all the realm set
	 * under the given realm.
	 * 
	 * @param token
	 *            - OpenSSO token.
	 * @param realm
	 *            - the realm.
	 * @param filter
	 *            - used to filtered the returned realms.
	 * @return the realm set.
	 * @throws RealmException
	 *             - when something wrong happens.
	 */
	Set<NameDNRealm> getRealmSiblingsSet(SSOToken token, String realm, String filter) throws RealmException;

	/**
	 * Retrieve the realm children. The method will retrieve all the realm set
	 * under the given realm.
	 * 
	 * @param token
	 *            - OpenSSO token.
	 * @param parentRealm
	 *            - the parent realm.
	 * @param filter
	 *            - used to filtered the returned realms.
	 * @return the realm set.
	 * @throws RealmException
	 *             - when something wrong happens.
	 */
	Set<NameDNRealm> getRealmChildrenSet(SSOToken token, String parentRealm, String filter) throws RealmException;

	/**
	 * Retrieve the realm set . The method will retrieve all the realm set under
	 * the top realm.
	 * 
	 * @param token
	 *            OpenSSO token.
	 * @param filter
	 *            - filter the realset
	 * @return the realm set.
	 * @throws RealmException
	 *             - when something wrong happens.n
	 */
	Set<String> getRealmSet(SSOToken token, String filter) throws RealmException;

}
