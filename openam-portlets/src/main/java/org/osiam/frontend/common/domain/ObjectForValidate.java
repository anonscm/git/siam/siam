/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.domain;

import com.iplanet.sso.SSOToken;

/**
 * Encapsulates all data for an ObjectForValidate object.
 *
 */
public class ObjectForValidate {

	private String realm;

	private SSOToken token;

	private Object object = new Object();

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public SSOToken getToken() {
		return token;
	}

	public void setToken(SSOToken token) {
		this.token = token;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	/**
	 * ObjectForValidate constructor.
	 * 
	 * @param realm
	 *            - realm
	 * @param token
	 *            - SSOToken
	 * @param object
	 *            - object
	 */
	public ObjectForValidate(String realm, SSOToken token, Object object) {
		super();
		this.realm = realm;
		this.token = token;
		this.object = object;
	}

}
