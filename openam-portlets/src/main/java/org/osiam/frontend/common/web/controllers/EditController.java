/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.web.controllers;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.ValidatorException;

import org.osiam.frontend.common.exceptions.OSIAMException;
import org.osiam.frontend.common.service.RealmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * EditController shows the edit portlets preferences mode form.
 * 
 */
@Controller("EditController")
@RequestMapping(value = "edit")
public class EditController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EditController.class);

	@Autowired
	private RealmService realmService;

	/**
	 * Show the realm selection forms. The selected realm is used in view mode.
	 * 
	 * @param model
	 *            - the model
	 * @param request
	 *            - the request
	 * @return {@link String} the view's name.
	 * 
	 */
	@RenderMapping
	public String edit(Model model, RenderRequest request) {

		String realm = request.getPreferences().getValue("realm", null);

		model.addAttribute("currentRealm", realm);
		model.addAttribute("realmsList", realmService.getRealmSet(getSSOToken("/", request)));

		return "edit";
	}

	/**
	 * Handles realm save.
	 * 
	 * @param realmName
	 *            - selected realm name
	 * @param request
	 *            - the request
	 * @param response
	 *            - the response
	 */
	@ActionMapping(params = "cmd=doSaveRealm")
	public void doSaveRealm(String realmName, ActionRequest request, ActionResponse response) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Realm selected in preferences mode: " + realmName);
		}

		try {
			request.getPreferences().setValue("realm", realmName);
			request.getPreferences().store();
		} catch (ReadOnlyException e) {
			throw new OSIAMException(e.getMessage(), e);
		} catch (ValidatorException e) {
			throw new OSIAMException(e.getMessage(), e);
		} catch (IOException e) {
			throw new OSIAMException(e.getMessage(), e);
		}
	}
}
