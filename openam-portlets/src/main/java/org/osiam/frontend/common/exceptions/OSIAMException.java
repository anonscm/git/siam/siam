/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.common.exceptions;

/**
 * Encapsulates any exception thrown from the OpenSSOService.
 * 
 */
public class OSIAMException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates an exception instance.
	 */
	public OSIAMException() {
		super();
	}

	/**
	 * Creates an exception instance with given message.
	 * 
	 * @param message
	 *            - the exception message
	 * 
	 */
	public OSIAMException(String message) {
		super(message);
	}

	/**
	 * Creates an exception instance with given message and given exception.
	 * 
	 * @param message
	 *            - the exception message
	 * @param exception
	 *            - the encapsulated exception
	 */
	public OSIAMException(String message, Exception exception) {
		super(message, exception);
	}

}
