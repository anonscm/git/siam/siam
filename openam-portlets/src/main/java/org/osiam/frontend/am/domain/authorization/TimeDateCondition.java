/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Time Condition.
 */
public class TimeDateCondition extends Condition {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Condition type value.
	 */
	public static final String TYPE = "SimpleTimeCondition";

	private static final Logger LOGGER = Logger.getLogger(TimeDateCondition.class);

	private static final String START_DAY = "StartDay";
	private static final String TIMEZONE = "EnforcementTimeZone";
	private static final String END_DATE = "EndDate";
	private static final String START_DATE = "StartDate";
	private static final String END_DAY = "EndDay";
	private static final String END_TIME = "EndTime";
	private static final String START_TIME = "StartTime";

	@Pattern(regexp = "^$|^(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\\d\\d$")
	private String startDate;

	@Pattern(regexp = "^$|^(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\\d\\d$")
	private String endDate;

	@Min(0)
	@Max(23)
	private Long startHour;
	@Min(0)
	@Max(59)
	private Long startMinute;

	private String startAmPm;
	@Min(0)
	@Max(23)
	private Long endHour;
	@Min(0)
	@Max(59)
	private Long endMinute;

	private String endAmPm;

	private String startDay;

	private String endDay;

	private String timezoneType;

	private String customTimezoneValue;

	private String timezone;

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public TimeDateCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		String endTimeString = null;
		String startTimeString = null;

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(START_DAY)) {
				String value = attributeValuePair.getValueList().get(0);
				startDay = value;
			} else if (attributeValuePair.getName().equals(TIMEZONE)) {
				String value = attributeValuePair.getValueList().get(0);
				timezone = value;
			} else if (attributeValuePair.getName().equals(END_DATE)) {
				String value = attributeValuePair.getValueList().get(0);
				String[] dateParts = value.split(":");
				endDate = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
			} else if (attributeValuePair.getName().equals(START_DATE)) {
				String value = attributeValuePair.getValueList().get(0);
				String[] dateParts = value.split(":");
				startDate = dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
			} else if (attributeValuePair.getName().equals(END_DAY)) {
				String value = attributeValuePair.getValueList().get(0);
				endDay = value;
			} else if (attributeValuePair.getName().equals(START_TIME)) {
				String value = attributeValuePair.getValueList().get(0);
				startTimeString = value;
			} else if (attributeValuePair.getName().equals(END_TIME)) {
				String value = attributeValuePair.getValueList().get(0);
				endTimeString = value;
			}
		}

		if (startTimeString != null && !"".equals(startTimeString)) {
			Time startTime = Time.fromString(startTimeString);
			startHour = new Long(startTime.getHour() > 12 ? startTime.getHour() - 12 : startTime.getHour());
			startMinute = new Long(startTime.getMinute());
			startAmPm = startTime.getAmpm();
		}
		if (endTimeString != null && !"".equals(endTimeString)) {
			Time endTime = Time.fromString(endTimeString);
			endHour = new Long(endTime.getHour() > 12 ? endTime.getHour() - 12 : endTime.getHour());
			endMinute = new Long(endTime.getMinute());
			endAmPm = endTime.getAmpm();
		}

	}

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the condition name.
	 */
	public TimeDateCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public TimeDateCondition() {

	}

	public Long getStartHour() {
		return startHour;
	}

	public void setStartHour(Long startHour) {
		this.startHour = startHour;
	}

	public Long getStartMinute() {
		return startMinute;
	}

	public void setStartMinute(Long startMinute) {
		this.startMinute = startMinute;
	}

	public String getStartAmPm() {
		return startAmPm;
	}

	public void setStartAmPm(String startAmPm) {
		this.startAmPm = startAmPm;
	}

	public Long getEndHour() {
		return endHour;
	}

	public void setEndHour(Long endHour) {
		this.endHour = endHour;
	}

	public Long getEndMinute() {
		return endMinute;
	}

	public void setEndMinute(Long endMinute) {
		this.endMinute = endMinute;
	}

	public String getEndAmPm() {
		return endAmPm;
	}

	public void setEndAmPm(String endAmPm) {
		this.endAmPm = endAmPm;
	}

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTimezoneType() {
		return timezoneType;
	}

	public void setTimezoneType(String timezoneType) {
		this.timezoneType = timezoneType;
	}

	public String getCustomTimezoneValue() {
		return customTimezoneValue;
	}

	public void setCustomTimezoneValue(String customTimezoneValue) {
		this.customTimezoneValue = customTimezoneValue;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		String[] dateParts = null;

		if (startDate != null && !"".equals(startDate)) {
			dateParts = startDate.split("/");
			XmlUtil.appendAttributeValuePairToString(buffer, START_DATE, dateParts[2] + ":" + dateParts[0] + ":"
					+ dateParts[1]);
		}

		if (endDate != null && !"".equals(endDate)) {
			dateParts = endDate.split("/");
			XmlUtil.appendAttributeValuePairToString(buffer, END_DATE, dateParts[2] + ":" + dateParts[0] + ":"
					+ dateParts[1]);
		}

		if ((startHour != null) && (startMinute != null) && (startAmPm != null)) {
			Time startTime = new Time(startHour, startMinute, startAmPm);
			XmlUtil.appendAttributeValuePairToString(buffer, START_TIME, startTime.toString());
		}

		if ((endHour != null) && (endMinute != null) && (endAmPm != null)) {
			Time endTime = new Time(endHour, endMinute, endAmPm);
			XmlUtil.appendAttributeValuePairToString(buffer, END_TIME, endTime.toString());
		}

		if (startDay != null && !"".equals(startDay)) {
			XmlUtil.appendAttributeValuePairToString(buffer, START_DAY, startDay);
		}

		if (endDay != null && !"".equals(endDay)) {
			XmlUtil.appendAttributeValuePairToString(buffer, END_DAY, endDay);
		}

		if (timezone != null) {
			XmlUtil.appendAttributeValuePairToString(buffer, TIMEZONE, timezone);
		}
	}

}
