/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.xml.bind.JAXBException;
import javax.xml.ws.soap.SOAPFaultException;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

import org.osiam.frontend.am.domain.authorization.XacmlPolicy;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.policy_repo.PolicySetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * XACMLPolicyController shows the edit "XACML Policy" form and does request
 * processing of the edit policy action.
 * 
 */
@Controller("XACMLPolicyController")
@RequestMapping(value = "view", params = "ctx=XACMLPolicyController")
public class XACMLPolicyController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(XACMLPolicyController.class);

	@Autowired
	private PolicySetService policySetService;

	/**
	 * Shows add/edit "XACML Policy" form.
	 * 
	 * @param policyName
	 *            - Policy name
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param error
	 *            - error to display
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editPolicy(String policyName, String action, String error, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "xacmlPolicy")) {

			XacmlPolicy xacmlPolicy = new XacmlPolicy();

			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				PolicySetType pst = policySetService.retrievePolicySet(realm, policyName);
				xacmlPolicy.setPolicyName(pst.getPolicySetId());
				try {
					xacmlPolicy.setPolicyXml(XacmlPolicy.getStringFromPolicySet(pst));
				} catch (Exception e) {
					LOGGER.debug("Invalid XACML Policy " + e);
				}

			} else {
				xacmlPolicy.setPolicyName(policyName);
				xacmlPolicy.setPolicyXml("");
			}

			model.addAttribute("xacmlPolicy", xacmlPolicy);

		}

		model.addAttribute("actionValue", action);
		model.addAttribute("error", error);

		return "authorization/policy/xacml/edit";
	}

	/**
	 * Save policy.
	 * 
	 * @param xacmlPolicy
	 *            - XacmlPolicy to save
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(XacmlPolicy xacmlPolicy, String action, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);

		PolicySetType xml;
		try {
			xml = XacmlPolicy.getPolicySetFromString(xacmlPolicy.getPolicyXml());

		} catch (JAXBException e) {
			LOGGER.debug("Invalid xml: " + e);
			response.setRenderParameter("error", "Invalid XML: "+e.getLinkedException().getMessage());
			return;
		}

		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			try {
				policySetService.updatePolicySet(realm, xacmlPolicy.getPolicyName(), xml);
			} catch (SOAPFaultException e) {
				LOGGER.debug("Error on update: " + e);
				response.setRenderParameter("error", "Error saving XACML policy: "+e.getMessage());
				return;
			}
		} else {
			try {
				xml.setPolicySetId(xacmlPolicy.getPolicyName());
				policySetService.addPolicySet(realm, xml);
			} catch (SOAPFaultException e) {
				LOGGER.debug("Error on insert: " + e);
				response.setRenderParameter("error", "Error saving XACML policy: "+e.getMessage());
				return;
			}
		}
		response.setRenderParameter("ctx", "AuthorizationController");
	}

	/**
	 * Validate xml.
	 * 
	 * @param xacmlPolicy
	 *            - xacmlPolicy
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "validate")
	public void doValidate(XacmlPolicy xacmlPolicy, ActionRequest request, ActionResponse response) {

		try {
			XacmlPolicy.getPolicySetFromString(xacmlPolicy.getPolicyXml());

		} catch (JAXBException e) {
			LOGGER.debug("Invalid xml: " + e);
			response.setRenderParameter("error", e.getLinkedException().getMessage());
			return;
		}

	}

	/**
	 * Back to policy list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AuthorizationController");
	}

	/**
	 * Reset form.
	 * 
	 * @param policyName
	 *            - policy name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String policyName, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
	}

}
