/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

import com.iplanet.sso.SSOToken;

/**
 * This class encapsulates the data for a policy entity.
 */
public class Policy extends BaseEntity implements Serializable {
	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(Policy.class);

	private String createdBy;

	private String lastModifiedBy;

	private Date creationDate;

	private Date lastModifiedDate;

	private Boolean referralPolicy;

	private Boolean active;

	private String description;

	private List<Rule> rules = new ArrayList<Rule>();

	private List<Subject> subjects = new ArrayList<Subject>();

	private List<Condition> conditions = new ArrayList<Condition>();

	private List<ResponseProvider> responseProviders = new ArrayList<ResponseProvider>();

	private List<Referral> referrals = new ArrayList<Referral>();

	/**
	 * Creates a Policy with the given name and type.
	 * 
	 * @param name
	 *            - the name of the policy.
	 * @param type
	 *            - the type of the policy.
	 */
	public Policy(final String name, final String type) {
		super(name, type);

	}

	/**
	 * Creates a Policy with the given name, rules, subjects, conditions and
	 * response providers.
	 * 
	 * @param name
	 *            - the name of the policy.
	 * @param type
	 *            - the type of the policy.
	 * @param rules
	 *            - the list of {@link Rule}.
	 * @param subjects
	 *            - the list of {@link Subject}.
	 * @param conditions
	 *            - the list of {@link Condition}.
	 * @param responseProviders
	 *            - the list of {@link ResponseProvider}.
	 */
	public Policy(String name, String type, List<Rule> rules, List<Subject> subjects, List<Condition> conditions,
			List<ResponseProvider> responseProviders) {
		super(name, type);
		this.rules = rules;
		this.subjects = subjects;
		this.conditions = conditions;
		this.responseProviders = responseProviders;
	}

	/**
	 * Creates a Policy.
	 */
	public Policy() {

	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public List<Condition> getConditions() {
		return conditions;
	}

	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}

	public List<ResponseProvider> getResponseProviders() {
		return responseProviders;
	}

	public void setResponseProviders(List<ResponseProvider> responseProviders) {
		this.responseProviders = responseProviders;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Boolean getReferralPolicy() {
		return referralPolicy;
	}

	public void setReferralPolicy(Boolean referralPolicy) {
		this.referralPolicy = referralPolicy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<Referral> getReferrals() {
		return referrals;
	}

	public void setReferrals(List<Referral> referrals) {
		this.referrals = referrals;
	}

	/**
	 * @return policy created based on the given XML node.
	 * @param token
	 *            - the SSO token.
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static Policy fromNode(SSOToken token, Node node) throws AuthorizationException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Reading Policy xml node.");
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}

		String name = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.NAME);
		String description = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.DESCRIPTION);
		if (description == null) {
			description = "";
		}
		String createdBy = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.CREATED_BY);
		String lastModifiedBy = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.LAST_MODIFIED_BY);
		String creationDate = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.CREATION_DATE);
		String lastModifiedDate = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.LAST_MODIFIED_DATE);
		String referralPolicy = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.REFERRAL_POLICY);
		String active = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.ACTIVE);

		String type = "";
		if (Boolean.TRUE.toString().equals(referralPolicy)) {
			type = AuthorizationConstants.POLICY_TYPE_REFERRAL;
		} else {
			type = AuthorizationConstants.POLICY_TYPE_CDSSO;
		}

		Policy policy = new Policy(name, type);
		policy.setDescription(description);
		if (createdBy != null) {
			policy.setCreatedBy(createdBy);
		}
		if (lastModifiedBy != null) {
			policy.setLastModifiedBy(lastModifiedBy);
		}
		if (creationDate != null) {
			policy.setCreationDate(new Date(Long.parseLong(creationDate)));
		}
		if (lastModifiedDate != null) {
			policy.setLastModifiedDate(new Date(Long.parseLong(lastModifiedDate)));
		}
		policy.setReferralPolicy(Boolean.parseBoolean(referralPolicy));
		policy.setActive(Boolean.parseBoolean(active));

		List<Rule> rules = new ArrayList<Rule>();
		List<Subject> subjects = new ArrayList<Subject>();
		List<Condition> conditions = new ArrayList<Condition>();
		List<ResponseProvider> responseProviders = new ArrayList<ResponseProvider>();
		List<Referral> referrals = new ArrayList<Referral>();

		Set<Node> ruleNodes = XmlUtil.getChildNodes(node, AuthorizationConstants.POLICY_RULE_NODE);
		Iterator<Node> ruleNodesIterator = ruleNodes.iterator();

		Iterator<Node> subjectNodesIterator = null;
		Iterator<Node> conditionNodesIterator = null;
		Iterator<Node> responseProviderNodesIterator = null;
		Iterator<Node> referralNodesIterator = null;

		if (AuthorizationConstants.POLICY_TYPE_CDSSO.equals(type)) {
			Set<Node> subjectParentNodes = XmlUtil.getChildNodes(node, AuthorizationConstants.POLICY_SUBJECTS_NODE);

			if (subjectParentNodes.size() > 0) {
				Node subjectParentNode = subjectParentNodes.iterator().next();
				Set<Node> subjectNodes = XmlUtil.getChildNodes(subjectParentNode,
						AuthorizationConstants.POLICY_SUBJECT_NODE);
				subjectNodesIterator = subjectNodes.iterator();
			}

			Set<Node> conditionParentNodes = XmlUtil.getChildNodes(node, AuthorizationConstants.POLICY_CONDITIONS_NODE);

			if (conditionParentNodes.size() > 0) {
				Node conditionParentNode = conditionParentNodes.iterator().next();
				Set<Node> conditionNodes = XmlUtil.getChildNodes(conditionParentNode,
						AuthorizationConstants.POLICY_CONDITION_NODE);
				conditionNodesIterator = conditionNodes.iterator();
			}

			Set<Node> responseProviderParentNodes = XmlUtil.getChildNodes(node,
					AuthorizationConstants.POLICY_RESPONSE_PROVIDERS_NODE);

			if (responseProviderParentNodes.size() > 0) {
				Node responseProviderParentNode = responseProviderParentNodes.iterator().next();
				Set<Node> responseProviderNodes = XmlUtil.getChildNodes(responseProviderParentNode,
						AuthorizationConstants.POLICY_RESPONSE_PROVIDER_NODE);
				responseProviderNodesIterator = responseProviderNodes.iterator();
			}
		}

		if (AuthorizationConstants.POLICY_TYPE_REFERRAL.equals(type)) {
			Set<Node> referralParentNodes = XmlUtil.getChildNodes(node, AuthorizationConstants.POLICY_REFERRALS_NODE);

			if (referralParentNodes.size() > 0) {
				Node referralParentNode = referralParentNodes.iterator().next();
				Set<Node> referralNodes = XmlUtil.getChildNodes(referralParentNode,
						AuthorizationConstants.POLICY_REFERRAL_NODE);
				referralNodesIterator = referralNodes.iterator();
			}
		}

		while (ruleNodesIterator.hasNext()) {
			Node currentNode = ruleNodesIterator.next();
			Rule rule = Rule.fromNode(currentNode);
			rules.add(rule);
		}

		if (AuthorizationConstants.POLICY_TYPE_CDSSO.equals(type)) {
			if (subjectNodesIterator != null) {
				while (subjectNodesIterator.hasNext()) {
					Node currentNode = subjectNodesIterator.next();
					Subject subject = Subject.fromNode(token, currentNode);
					subjects.add(subject);
				}
			}
			if (conditionNodesIterator != null) {
				while (conditionNodesIterator.hasNext()) {
					Node currentNode = conditionNodesIterator.next();
					Condition condition = Condition.fromNode(currentNode);
					conditions.add(condition);
				}
			}
			if (responseProviderNodesIterator != null) {
				while (responseProviderNodesIterator.hasNext()) {
					Node currentNode = responseProviderNodesIterator.next();
					ResponseProvider responseProvider = ResponseProvider.fromNode(currentNode);
					responseProviders.add(responseProvider);
				}
			}
		}

		if (AuthorizationConstants.POLICY_TYPE_REFERRAL.equals(type)) {
			if (referralNodesIterator != null) {
				while (referralNodesIterator.hasNext()) {
					Node currentNode = referralNodesIterator.next();
					Referral referral = Referral.fromNode(currentNode);
					referrals.add(referral);
				}
			}
		}

		policy.setRules(rules);
		policy.setSubjects(subjects);
		policy.setConditions(conditions);
		policy.setResponseProviders(responseProviders);
		policy.setReferrals(referrals);

		return policy;
	}

	/**
	 * @return an XML string describing this object.
	 */
	public String toXML() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		buffer.append("<Policy");

		buffer.append(" name=\"");
		buffer.append(getName());
		buffer.append("\"");

		String descriptionLocal = getDescription();
		if (descriptionLocal == null) {
			descriptionLocal = "";
		}
		buffer.append(" description=\"");
		buffer.append(descriptionLocal);
		buffer.append("\"");

		buffer.append(" createdby=\"");
		buffer.append(getCreatedBy());
		buffer.append("\"");

		buffer.append(" lastmodifiedby=\"");
		buffer.append(getLastModifiedBy());
		buffer.append("\"");

		if (getCreationDate() != null) {
			buffer.append(" creationdate=\"");
			buffer.append(Long.toString(getCreationDate().getTime()));
			buffer.append("\"");
		}

		if (getLastModifiedDate() != null) {
			buffer.append(" lastmodifieddate=\"");
			buffer.append(Long.toString(getLastModifiedDate().getTime()));
			buffer.append("\"");
		}

		if (getReferralPolicy() != null) {
			buffer.append(" referralPolicy=\"");
			buffer.append(getReferralPolicy().toString());
			buffer.append("\"");
		}

		String activeBoolean = Boolean.TRUE.toString();
		if (getActive() != null) {
			activeBoolean = getActive().toString();
		}
		buffer.append(" active=\"");
		buffer.append(activeBoolean);
		buffer.append("\"");
		buffer.append(">");

		if (rules != null) {
			for (int i = 0; i < rules.size(); i++) {
				Rule rule = rules.get(i);
				rule.appendXML(buffer);
			}
		}

		if (AuthorizationConstants.POLICY_TYPE_CDSSO.equals(getType())) {
			if ((subjects != null) && (subjects.size() > 0)) {
				buffer.append("<Subjects ");
				buffer.append("name=\"null\" description=\"\">");
				for (int i = 0; i < subjects.size(); i++) {
					Subject subject = subjects.get(i);
					subject.appendXML(buffer);
				}
				buffer.append("</Subjects>");
			}

			if ((conditions != null) && (conditions.size() > 0)) {
				buffer.append("<Conditions ");
				buffer.append("name=\"null\" description=\"\">");
				for (int i = 0; i < conditions.size(); i++) {
					Condition condition = conditions.get(i);
					condition.appendXML(buffer);
				}
				buffer.append("</Conditions>");
			}

			if ((responseProviders != null) && (responseProviders.size() > 0)) {
				buffer.append("<ResponseProviders ");
				buffer.append("name=\"null\" description=\"\">");
				for (int i = 0; i < responseProviders.size(); i++) {
					ResponseProvider responseProvider = responseProviders.get(i);
					responseProvider.appendXML(buffer);
				}
				buffer.append("</ResponseProviders>");
			}
		}
		if (AuthorizationConstants.POLICY_TYPE_REFERRAL.equals(getType())) {
			if ((referrals != null) && (referrals.size() > 0)) {
				buffer.append("<Referrals ");
				buffer.append("name=\"null\" description=\"\">");
				for (int i = 0; i < referrals.size(); i++) {
					Referral referral = referrals.get(i);
					referral.appendXML(buffer);
				}
				buffer.append("</Referrals>");
			}
		}

		buffer.append("</Policy>");
		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((lastModifiedBy == null) ? 0 : lastModifiedBy.hashCode());
		result = prime * result + ((lastModifiedDate == null) ? 0 : lastModifiedDate.hashCode());
		result = prime * result + ((referralPolicy == null) ? 0 : referralPolicy.hashCode());
		result = prime * result + ((referrals == null) ? 0 : referrals.hashCode());
		result = prime * result + ((responseProviders == null) ? 0 : responseProviders.hashCode());
		result = prime * result + ((rules == null) ? 0 : rules.hashCode());
		result = prime * result + ((subjects == null) ? 0 : subjects.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Policy other = (Policy) obj;
		if (active == null) {
			if (other.active != null) {
				return false;
			}
		} else if (!active.equals(other.active)) {
			return false;
		}
		if (conditions == null) {
			if (other.conditions != null) {
				return false;
			}
		} else if (!conditions.equals(other.conditions)) {
			return false;
		}
		if (createdBy == null) {
			if (other.createdBy != null) {
				return false;
			}
		} else if (!createdBy.equals(other.createdBy)) {
			return false;
		}
		if (creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!creationDate.equals(other.creationDate)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (lastModifiedBy == null) {
			if (other.lastModifiedBy != null) {
				return false;
			}
		} else if (!lastModifiedBy.equals(other.lastModifiedBy)) {
			return false;
		}
		if (lastModifiedDate == null) {
			if (other.lastModifiedDate != null) {
				return false;
			}
		} else if (!lastModifiedDate.equals(other.lastModifiedDate)) {
			return false;
		}
		if (referralPolicy == null) {
			if (other.referralPolicy != null) {
				return false;
			}
		} else if (!referralPolicy.equals(other.referralPolicy)) {
			return false;
		}
		if (referrals == null) {
			if (other.referrals != null) {
				return false;
			}
		} else if (!referrals.equals(other.referrals)) {
			return false;
		}
		if (responseProviders == null) {
			if (other.responseProviders != null) {
				return false;
			}
		} else if (!responseProviders.equals(other.responseProviders)) {
			return false;
		}
		if (rules == null) {
			if (other.rules != null) {
				return false;
			}
		} else if (!rules.equals(other.rules)) {
			return false;
		}
		if (subjects == null) {
			if (other.subjects != null) {
				return false;
			}
		} else if (!subjects.equals(other.subjects)) {
			return false;
		}
		return true;
	}

}
