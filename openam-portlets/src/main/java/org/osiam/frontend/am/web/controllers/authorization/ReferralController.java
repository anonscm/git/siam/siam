/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.Referral;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.common.service.RealmService;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * ReferralController shows the edit {@link Referral} form and does request
 * processing of the edit {@link Referral} action.
 * 
 */
@Controller("ReferralController")
@RequestMapping(value = "view", params = { "ctx=ReferralController" })
public class ReferralController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralController.class);

	@Autowired
	private RealmService realmService;

	/**
	 * Shows add/edit {@link Referral} form.
	 * 
	 * @param filter
	 *            - {@link String} value used to filter the realm list
	 * @param referralName
	 *            - {@link Referral} name
	 * @param referralType
	 *            - {@link Referral} type
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editReferral(String filter, String referralName, String referralType, String policyName,
			String policyType, String action, Model model, RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "referral")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			Referral referral = Referral.getReferralByName(referralName, policy.getReferrals());
			if (referral == null) {
				referral = new Referral(referralName, referralType);
			}

			model.addAttribute("referral", referral);
		}

		filter = filter == null ? "*" : filter;

		if (AuthorizationConstants.REFERRAL_TYPE_SUB_ORG_REFERRAL.equals(referralType)) {
			model.addAttribute("realmList", realmService.getRealmChildrenSet(token, realm, filter));
		}
		if (AuthorizationConstants.REFERRAL_TYPE_PEER_ORG_REFERRAL.equals(referralType)) {
			model.addAttribute("realmList", realmService.getRealmSiblingsSet(token, realm, filter));
		}

		model.addAttribute("referralName", referralName);
		model.addAttribute("referralType", referralType);
		model.addAttribute("actionValue", action);

		return "authorization/referral/edit";

	}

	/**
	 * Filter realm list by given name.
	 * 
	 * @param referral
	 *            - {@link Referral}
	 * @param result
	 *            - BindingResult
	 * @param filter
	 *            - {@link String} value used to filter the realm list
	 * @param referralName
	 *            - {@link Referral} name
	 * @param referralType
	 *            - {@link Referral} type
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "search")
	public void doSearch(@ModelAttribute("referral") @Valid Referral referral, BindingResult result, String filter,
			String referralName, String referralType, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("referralName", referralName);
		response.setRenderParameter("referralType", referralType);
		response.setRenderParameter("filter", filter);

	}

	/**
	 * Save {@link Referral} to current {@link Policy}.
	 * 
	 * @param referral
	 *            - {@link Referral} to save
	 * @param result
	 *            - BindingResult
	 * @param referralName
	 *            - {@link Referral} name
	 * @param referralType
	 *            - {@link Referral} type
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("referral") @Valid Referral referral, BindingResult result, String referralName,
			String referralType, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("referralName", referralName);
		response.setRenderParameter("referralType", referralType);
		response.setRenderParameter("action", action);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Referral referralOld = Referral.getReferralByName(referralName, policy.getReferrals());
		policy.getReferrals().remove(referralOld);
		policy.getReferrals().add(referral);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link Referral} form.
	 * 
	 * @param referralName
	 *            - {@link Referral} name
	 * @param referralType
	 *            - {@link Referral} type
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String referralName, String referralType, String policyName, String policyType,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("referralName", referralName);
		response.setRenderParameter("referralType", referralType);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
