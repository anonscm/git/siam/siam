/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authentication;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authentication.Chain;
import org.osiam.frontend.am.domain.authentication.ChainProperty;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * ChainPropertiesController portlet controller.
 * 
 */
@Controller("ChainPropertiesController")
@RequestMapping(value = "view", params = { "ctx=ChainPropertiesController" })
public class ChainPropertiesController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ChainPropertiesController.class);

	@Autowired
	private AuthenticationService authenticationService;

	/**
	 * Show the {@link ChainProperty} list and insert new {@link ChainProperty}
	 * form. For each item of the list are available delete, edit and reorder
	 * action.
	 * 
	 * @param name
	 *            - the chain name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param checkAllProperties
	 *            - use to decide if all the {@link ChainProperty} are checked
	 *            for deletion.
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name.
	 */
	@RenderMapping
	public String editChainProperties(String name, String action, String checkAllProperties, Model model,
			RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "chain")) {
			Chain chain = new Chain();
			chain.setName(name);
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				chain.setPropertiesList(authenticationService.getChainPropertyList(token, realm, name));
			}

			model.addAttribute("chain", chain);
		}

		model.addAttribute("actionValue", action);

		model.addAttribute("instanceList", authenticationService.getInstanceList(getSSOToken(realm, request), realm));
		model.addAttribute("criteriaList", authenticationService.getCriteriaList());

		model.addAttribute("checkAllProperties", checkAllProperties);

		return "authentication/editChainProperties";
	}

	/**
	 * Add a new {@link ChainProperty} for current chain.
	 * 
	 * @param chain
	 *            - current {@link Chain}
	 * @param result
	 *            - BindingResult
	 * @param name
	 *            - {@link Chain} name
	 * @param chainProp
	 *            - {@link ChainProperty} to add
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addChainProperties")
	public void doAddChainProperties(@ModelAttribute("chain") @Valid Chain chain, BindingResult result, String name,
			ChainProperty chainProp, ActionRequest request, ActionResponse response) {

		chain.getPropertiesList().add(chainProp);

		response.setRenderParameter("name", name);
	}

	/**
	 * Save {@link Chain}. <br>
	 * If action parameter has value CommonConstants.ACTION_INSERT is made the
	 * insertion of new chain. <br>
	 * The chain properties are saved for any situation (edit or insert chain).
	 * 
	 * @param chain
	 *            - {@link Chain} to save
	 * @param result
	 *            - BindingResult
	 * @param name
	 *            - {@link Chain} name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("chain") @Valid Chain chain, BindingResult result, String name, String action,
			ActionRequest request, ActionResponse response) {

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (CommonConstants.ACTION_INSERT.equals(action)) {
			authenticationService.addNewChain(token, realm, chain.getName());
		}

		authenticationService.saveChainPropertyList(token, realm, name, chain.getPropertiesList());

		response.setRenderParameter("ctx", "AuthenticationController");
	}

	/**
	 * Reset add/edit {@link Chain} form.
	 * 
	 * @param name
	 *            - {@link Chain} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String name, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
	}

	/**
	 * Back to chain list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		response.setRenderParameter("ctx", "AuthenticationController");
	}

	/**
	 * Set value for "checkAllProperties".
	 * 
	 * @param chain
	 *            - {@link Chain}
	 * @param result
	 *            - BindingResult
	 * @param name
	 *            - {@link Chain} name
	 * @param checkAllProperties
	 *            - use to decide if all the {@link ChainProperty} are checked
	 *            for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(@ModelAttribute("chain") @Valid Chain chain, BindingResult result, String name,
			String checkAllProperties, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("name", name);
		response.setRenderParameter("checkAllProperties", checkAllProperties);
	}

	/**
	 * Delete {@link ChainProperty} for current {@link Chain}.
	 * 
	 * @param chain
	 *            - {@link Chain}
	 * @param result
	 *            - BindingResult
	 * @param deletePropertiesList
	 *            - list of {@link ChainProperty} index
	 * @param name
	 *            - {@link Chain} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteProperties")
	public void doDeleteProperties(@ModelAttribute("chain") @Valid Chain chain, BindingResult result,
			int[] deletePropertiesList, String name, ActionRequest request, ActionResponse response) {

		if (deletePropertiesList != null) {
			for (int i = deletePropertiesList.length - 1; i >= 0; i--) {
				chain.getPropertiesList().remove(deletePropertiesList[i]);
			}
		}

		response.setRenderParameter("name", name);
	}

	/**
	 * Reorder {@link ChainProperty} list.
	 * 
	 * @param chain
	 *            - {@link Chain}
	 * @param result
	 *            - BindingResult
	 * @param name
	 *            - {@link Chain} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping
	public void doReorder(@ModelAttribute("chain") @Valid Chain chain, BindingResult result, String name,
			ActionRequest request, ActionResponse response) {

		int index = 0;
		boolean isUp = false;
		boolean isDown = false;

		for (int i = 0; i < chain.getPropertiesList().size(); i++) {
			if (request.getParameter("up_" + i) != null) {
				index = i;
				isUp = true;
				break;
			}
			if (request.getParameter("down_" + i) != null) {
				index = i;
				isDown = true;
				break;
			}
		}

		int reorderFirst = 0;
		int reorderSecond = 0;

		if (isUp) {
			reorderFirst = index;
			reorderSecond = index - 1;
		} else if (isDown) {
			reorderFirst = index;
			reorderSecond = index + 1;
		}

		ChainProperty first = chain.getPropertiesList().get(reorderFirst);
		chain.getPropertiesList().set(reorderFirst, chain.getPropertiesList().get(reorderSecond));
		chain.getPropertiesList().set(reorderSecond, first);

		response.setRenderParameter("name", name);
	}

}
