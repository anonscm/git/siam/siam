/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.OpenSSOIdentitySubject;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.Subject;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.AMIdentity;

/**
 * AMIdentitySubjectSubjectController shows the add/edit
 * {@link OpenSSOIdentitySubject} form and does request processing of the
 * edit/add {@link OpenSSOIdentitySubject} action.
 * 
 */
@Controller("AMIdentitySubjectSubjectController")
@RequestMapping(value = "view", params = "ctx=AMIdentitySubjectSubjectController")
public class AMIdentitySubjectSubjectController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticatedUsersSubjectController.class);

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	@Qualifier("openSSOIdentitySubjectValidator")
	private Validator openSSOIdentitySubjectValidator;

	/**
	 * Shows add/edit {@link OpenSSOIdentitySubject} form.
	 * 
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param searchValuesType
	 *            -
	 * @param searchValuesFiled
	 *            -
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editSubject(String subjectName, String policyName, String policyType, String action,
			String searchValuesType, String searchValuesFiled, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");
		OpenSSOIdentitySubject subject = (OpenSSOIdentitySubject) Subject.getSubjectByNameAndType(subjectName,
				AuthorizationConstants.SUBJECT_TYPE_AM_IDENTITY_SUBJECT, policy.getSubjects());

		if (subject == null) {
			subject = new OpenSSOIdentitySubject(subjectName);
		}

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "subject")) {
			model.addAttribute("subject", subject);

		}

		AMIdentity[] selectedValuesList = (AMIdentity[]) request.getPortletSession().getAttribute("selectedValuesList");
		if (selectedValuesList == null) {
			selectedValuesList = new AMIdentity[subject.getValues().size()];
			subject.getValues().toArray(selectedValuesList);
			request.getPortletSession().setAttribute("selectedValuesList", selectedValuesList);
		}

		model.addAttribute("selectedValuesList", selectedValuesList);

		Set<AMIdentity> identitySubjectValues = new HashSet<AMIdentity>();
		if (searchValuesType != null && searchValuesType.length() > 0) {
			identitySubjectValues = authorizationService.getAvailableIdentity(token, realm, searchValuesType,
					searchValuesFiled == null ? "*" : searchValuesFiled);
		}

		model.addAttribute("availabelValues",
				getIdentitySubjectAvailablelValues(identitySubjectValues, Arrays.asList(selectedValuesList)));

		model.addAttribute("subjectName", subjectName);
		model.addAttribute("actionValue", action);

		model.addAttribute("filterTypeList", authorizationService.getSupportedEntityTypes(token, realm));

		return "authorization/subject/amIdentitySubject/edit";

	}

	/**
	 * Get available values for current {@link Subject}.
	 * 
	 * @param list
	 *            - all the values
	 * @param selectedValue
	 *            - values selected for the current subject
	 * @return {@link List} contains values not use by the current subject
	 */
	private List<AMIdentity> getIdentitySubjectAvailablelValues(Set<AMIdentity> list, List<AMIdentity> selectedValue) {

		List<AMIdentity> result = new ArrayList<AMIdentity>();

		for (Iterator<AMIdentity> iterator = list.iterator(); iterator.hasNext();) {
			AMIdentity string = iterator.next();
			result.add(string);
		}

		if (selectedValue != null) {
			for (Iterator<AMIdentity> iterator = selectedValue.iterator(); iterator.hasNext();) {
				AMIdentity string = iterator.next();

				((ArrayList<AMIdentity>) result).remove(string);
			}
		}
		return result;

	}

	/**
	 * Search available values for current {@link Subject}.
	 * 
	 * @param subject
	 *            - {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param searchValuesType
	 *            - {@link String} value for filter type
	 * @param searchValuesFiled
	 *            - {@link String} value
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "searchValues")
	public void doSearchValues(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject, BindingResult result,
			String searchValuesType, String searchValuesFiled, String subjectName, String policyName,
			String policyType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		response.setRenderParameter("searchValuesType", searchValuesType);
		response.setRenderParameter("searchValuesFiled", searchValuesFiled);

	}

	/**
	 * Select new values for current {@link Subject}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param availableValues
	 *            - {@link List} of selected values
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addValues")
	public void doAddValues(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject, BindingResult result,
			String[] availableValues, String subjectName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		AMIdentity[] selectedValuesList = (AMIdentity[]) request.getPortletSession().getAttribute("selectedValuesList");

		request.getPortletSession().setAttribute("selectedValuesList",
				addValue(selectedValuesList, availableValues, token));

	}

	/**
	 * Select all the values for current {@link Subject}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param allAvailabelValuea
	 *            - list that contains all available values
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addAllValues")
	public void doAddAllValues(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject, BindingResult result,
			String[] allAvailabelValuea, String subjectName, String policyName, String policyType,
			ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		AMIdentity[] selectedValuesList = (AMIdentity[]) request.getPortletSession().getAttribute("selectedValuesList");

		request.getPortletSession().setAttribute("selectedValuesList",
				addValue(selectedValuesList, allAvailabelValuea, token));

	}

	/**
	 * Add values form a list in a given list.
	 * 
	 * @param list
	 *            - list to update
	 * @param listToAdd
	 *            - contains values to add in new list
	 * @return list that contains all the values
	 */
	private AMIdentity[] addValue(AMIdentity[] list, String[] listToAdd, SSOToken token) {

		List<AMIdentity> myList = new ArrayList<AMIdentity>(Arrays.asList(list));

		if (listToAdd != null) {
			for (int i = 0; i < listToAdd.length; i++) {
				AMIdentity amIdentity = authorizationService.getAMIdentityByUniversalId(token, listToAdd[i]);
				if (amIdentity != null) {
					myList.add(amIdentity);
				}
			}
		}
		AMIdentity[] result = new AMIdentity[myList.size()];

		return myList.toArray(result);

	}

	/**
	 * Remove values for current {@link Subject}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param selectedValues
	 *            - list that contains values to remove
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "removeValues")
	public void doRemoveValues(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject, BindingResult result,
			String[] selectedValues, String subjectName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		AMIdentity[] selectedValuesList = (AMIdentity[]) request.getPortletSession().getAttribute("selectedValuesList");

		request.getPortletSession().setAttribute("selectedValuesList", removeValue(selectedValuesList, selectedValues));

	}

	/**
	 * Remove all values for current {@link Subject}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "removeAllvalues")
	public void doRemoveAllvalues(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject,
			BindingResult result, String subjectName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		request.getPortletSession().setAttribute("selectedValuesList", new AMIdentity[0]);

	}

	/**
	 * Remove values form list.
	 * 
	 * @param list
	 *            - list to update
	 * @param listToRemove
	 *            - contains values to remove from given list
	 * @return list that contains values remaining after deletion
	 */
	private AMIdentity[] removeValue(AMIdentity[] list, String[] listToRemove) {

		List<AMIdentity> myList = new ArrayList<AMIdentity>(Arrays.asList(list));

		if (listToRemove != null) {
			for (int i = 0; i < listToRemove.length; i++) {
				for (Iterator<AMIdentity> iterator = myList.iterator(); iterator.hasNext();) {
					AMIdentity amIdentity = iterator.next();
					if (amIdentity.getUniversalId().equals(listToRemove[i])) {
						myList.remove(amIdentity);
						break;
					}
				}
			}
		}
		AMIdentity[] result = new AMIdentity[myList.size()];

		return myList.toArray(result);
	}

	/**
	 * Save {@link OpenSSOIdentitySubject} for current {@link Policy}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveSubject(@ModelAttribute("subject") @Valid OpenSSOIdentitySubject subject, BindingResult result,
			String subjectName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);
		response.setRenderParameter("action", action);

		AMIdentity[] selectedValuesList = (AMIdentity[]) request.getPortletSession().getAttribute("selectedValuesList");

		subject.setValues(Arrays.asList(selectedValuesList));

		openSSOIdentitySubjectValidator.validate(subject, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid ");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Subject subjectOld = Subject.getSubjectByNameAndType(subjectName,
				AuthorizationConstants.SUBJECT_TYPE_AM_IDENTITY_SUBJECT, policy.getSubjects());
		policy.getSubjects().remove(subjectOld);
		policy.getSubjects().add(subject);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link OpenSSOIdentitySubject} form.
	 * 
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetSubject(String subjectName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

		request.getPortletSession().setAttribute("selectedValuesList", null);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
