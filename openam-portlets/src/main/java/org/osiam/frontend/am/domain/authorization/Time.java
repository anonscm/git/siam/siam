/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

/**
 * This class encapsulates methods for dealing with time information stored
 * inside a {@link Policy}.
 * 
 */
public class Time {
	private int hour;
	private int minute;
	private String ampm;

	private static final byte FIRST_MINUTE = 0;
	private static final byte FIRST_HOUR = 0;
	private static final byte LAST_HOUR = 23;
	private static final byte LAST_MINUTE = 59;
	private static final byte LAST_AM_HOUR = 12;

	private static final String INVALID_FORMAT_EXCEPTION_MESSAGE = "Time format is not valid. Use HOUR:MINUTE format.";

	/**
	 * Creates a time object with given hour, minute and AMPM setting.
	 * 
	 * @param hour
	 *            - the hour
	 * @param minute
	 *            - the minute
	 * @param ampm
	 *            - the AM/PM setting. Can be either "AM" or "PM".
	 */
	public Time(int hour, int minute, String ampm) {
		boolean valid = true;
		if (hour < FIRST_HOUR) {
			valid = false;
		}
		if (hour > LAST_HOUR) {
			valid = false;
		}
		if (minute < FIRST_MINUTE) {
			valid = false;
		}
		if (minute > LAST_MINUTE) {
			valid = false;
		}
		if (!valid) {
			throw new IllegalArgumentException("Invalid time arguments.");
		}

		this.hour = hour;
		this.minute = minute;
		this.ampm = ampm;
	}

	/**
	 * Creates a time object with given hour, minute and AMPM setting.
	 * 
	 * @param hour
	 *            - the hour
	 * @param minute
	 *            - the minute
	 * @param ampm
	 *            - the AM/PM setting. Can be either "AM" or "PM".
	 */
	public Time(Long hour, Long minute, String ampm) {
		this(hour.intValue(), minute.intValue(), ampm);
	}

	/**
	 * Creates an empty time object.
	 */
	public Time() {

	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public String getAmpm() {
		return ampm;
	}

	public void setAmpm(String ampm) {
		this.ampm = ampm;
	}

	/**
	 * @return a time object by parsing the given string.
	 * 
	 * @param timeString
	 *            - the string containing the time.
	 */
	public static final Time fromString(String timeString) {
		if (timeString == null) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		Time time = new Time();
		String[] timeParts = timeString.split(":");
		if (timeParts.length != 2) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		int hour = -1;
		try {
			hour = Integer.parseInt(timeParts[0]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		int minute = -1;
		try {
			minute = Integer.parseInt(timeParts[1]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		if (hour < 0) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		if (minute < 0) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		if (hour > LAST_HOUR) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		if (minute > LAST_MINUTE) {
			throw new IllegalArgumentException(INVALID_FORMAT_EXCEPTION_MESSAGE);
		}
		if (hour < LAST_AM_HOUR) {
			time.setAmpm("am");
		} else {
			time.setAmpm("pm");
		}
		time.setHour(hour);
		time.setMinute(minute);
		return time;
	}

	/**
	 * @return a string describing this object values.
	 */
	public String toString() {
		if (hour > LAST_AM_HOUR) {
			ampm = "pm";
		}

		if ((hour < LAST_AM_HOUR) && ("pm".equalsIgnoreCase(ampm))) {
			hour = hour + LAST_AM_HOUR;
		}
		return hour + ":" + minute;

	}

}
