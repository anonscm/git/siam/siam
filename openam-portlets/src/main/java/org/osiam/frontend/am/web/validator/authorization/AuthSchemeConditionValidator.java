/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.validator.authorization;

import org.osiam.frontend.am.domain.authorization.AuthSchemeCondition;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the AuthSchemeCondition object.
 * <p>
 * Instance List can not be empty.
 * </p>
 * 
 */
@Component("authSchemeConditionValidator")
public class AuthSchemeConditionValidator implements Validator {

	@Override
	public boolean supports(Class<?> klass) {
		return AuthSchemeCondition.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AuthSchemeCondition condition = (AuthSchemeCondition) target;

		if (condition.getInstanceList() == null || condition.getInstanceList().size() < 1) {
			errors.rejectValue("instanceList", "NotEmptyList");
		}

	}
}
