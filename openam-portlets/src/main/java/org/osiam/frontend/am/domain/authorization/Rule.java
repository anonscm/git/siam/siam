/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.reflect.ConstructorUtils;
import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * The Rule class encapsulates the data for a rule.
 */
public abstract class Rule extends BaseEntity {

	@NotNull
	@Size(min = 1)
	private String resourceName;

	private static final Logger LOGGER = Logger.getLogger(Condition.class);

	private static final Map<String, Class<? extends Rule>> CLAZZ_MAP = new HashMap<String, Class<? extends Rule>>();

	static {
		CLAZZ_MAP.put(DiscoveryServiceRule.TYPE, DiscoveryServiceRule.class);
		CLAZZ_MAP.put(LiberyPersonalProfileServiceRule.TYPE, LiberyPersonalProfileServiceRule.class);
		CLAZZ_MAP.put(URLPolicyAgentRule.TYPE, URLPolicyAgentRule.class);
	}

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @return rule created based on the given XML node.
	 * 
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static Rule fromNode(Node node) throws AuthorizationException {

		String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

		Set<Node> ruleChildNodes = XmlUtil.getChildNodes(node, XmlUtil.TYPE_ATTRIBUTE_RULE);
		Iterator<Node> ruleChildrenIterator = ruleChildNodes.iterator();
		Node typeNode = ruleChildrenIterator.next();
		String type = XmlUtil.getNodeAttributeValue(typeNode, XmlUtil.NAME_ATTRIBUTE);

		Set<Node> ruleChildNodes2 = XmlUtil.getChildNodes(node, XmlUtil.RESOURCE_NAME);
		Iterator<Node> ruleChildrenIterator2 = ruleChildNodes2.iterator();
		Node resourceNameNode = ruleChildrenIterator2.next();
		String resourceName = XmlUtil.getNodeAttributeValue(resourceNameNode, XmlUtil.NAME_ATTRIBUTE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Read rule with name %s and type %s from xml node.", name, type));
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}

		Class<? extends Rule> ruleClazz = CLAZZ_MAP.get(type);

		if (ruleClazz == null) {
			String error = String.format("Invalid rule type %s for rule name %s  . Xml : %s", type, name,
					XmlUtil.getString(node));
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}

		Rule rule = null;
		try {
			rule = (Rule) ConstructorUtils.invokeConstructor(ruleClazz, new Object[] { name, node });
			rule.setResourceName(resourceName);
		} catch (NoSuchMethodException e) {
			throw new AuthorizationException("Error creating Rule instance.", e);
		} catch (IllegalAccessException e) {
			throw new AuthorizationException("Error creating Rule instance.", e);
		} catch (InvocationTargetException e) {
			throw new AuthorizationException("Error creating Rule instance.", e);
		} catch (InstantiationException e) {
			throw new AuthorizationException("Error creating Rule instance.", e);
		}

		return rule;

	}

	/**
	 * 
	 * Creates a {@link Rule} with the the given name and the given type.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param type
	 *            - the condition type.
	 */
	public Rule(String name, String type) {
		super(name, type);
	}

	/**
	 * Creates a {@link Rule} with the the given name, the given type and given
	 * resourceName.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param type
	 *            - the condition type.
	 * @param resourceName
	 *            - the condition resourceName.
	 */
	public Rule(String name, String type, String resourceName) {
		super(name, type);
		this.resourceName = resourceName;
	}

	/**
	 * Creates a rule.
	 */
	public Rule() {
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * 
	 * Appends XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the string buffer to append to.
	 */
	public void appendXML(StringBuilder buffer) {
		buffer.append("<Rule ");
		buffer.append("name=\"");
		buffer.append(getName());
		buffer.append("\"");
		buffer.append(">");
		buffer.append("<ServiceName ");
		buffer.append("name=\"");
		buffer.append(getType());
		buffer.append("\"");
		buffer.append("/>");
		buffer.append("<ResourceName");
		buffer.append(" name=\"");
		buffer.append(resourceName);
		buffer.append("\"/>");
		appendInnerXML(buffer);
		buffer.append("</Rule>");
	}

	/**
	 * 
	 * Appends the XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the buffer to append the XML to.
	 */
	public abstract void appendInnerXML(StringBuilder buffer);

	/**
	 * 
	 * @return type of rule.
	 */
	@Override
	public abstract String getType();

	/**
	 * Get {@link Rule} by name from given list.
	 * 
	 * @param name
	 *            - {@link Rule} name
	 * @param list
	 *            - {@link List} of rules.
	 * @return - {@link Rule}
	 */
	public static Rule getRuleByName(String name, List<Rule> list) {
		for (Iterator<Rule> iterator = list.iterator(); iterator.hasNext();) {
			Rule rule = iterator.next();
			if (rule.getName().equals(name)) {
				return rule;
			}
		}
		return null;
	}

	/**
	 * Get {@link Rule} by name and type from given list.
	 * 
	 * @param name
	 *            - {@link Rule} name
	 * @param type
	 *            - {@link Rule} type
	 * @param list
	 *            - list of rules
	 * @return {@link Rule}
	 */
	public static Rule getRuleByNameAndType(String name, String type, List<Rule> list) {
		for (Iterator<Rule> iterator = list.iterator(); iterator.hasNext();) {
			Rule rule = iterator.next();
			if (rule.getName().equals(name) && rule.getType().equals(type)) {
				return rule;
			}
		}
		return null;
	}

}
