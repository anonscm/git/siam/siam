/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.reflect.ConstructorUtils;
import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class is the base class for all Policy Conditions.
 */
public abstract class Condition extends BaseEntity {

	private static final Logger LOGGER = Logger.getLogger(Condition.class);

	private static final Map<String, Class<? extends Condition>> CLAZZ_MAP = new HashMap<String, Class<? extends Condition>>();

	static {

		CLAZZ_MAP.put(SessionCondition.TYPE, SessionCondition.class);
		CLAZZ_MAP.put(AuthenticationToRealmCondition.TYPE, AuthenticationToRealmCondition.class);
		CLAZZ_MAP.put(AuthLevelCondition.TYPE, AuthLevelCondition.class);
		CLAZZ_MAP.put(IPDNSCondition.TYPE, IPDNSCondition.class);
		CLAZZ_MAP.put(LEAuthLevelCondition.TYPE, LEAuthLevelCondition.class);
		CLAZZ_MAP.put(AuthenticateToServiceCondition.TYPE, AuthenticateToServiceCondition.class);
		CLAZZ_MAP.put(AuthSchemeCondition.TYPE, AuthSchemeCondition.class);
		CLAZZ_MAP.put(ResourceEnvironmentIPCondition.TYPE, ResourceEnvironmentIPCondition.class);
		CLAZZ_MAP.put(SessionPropertiesCondition.TYPE, SessionPropertiesCondition.class);
		CLAZZ_MAP.put(TimeDateCondition.TYPE, TimeDateCondition.class);

	}

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Creates a {@link Condition} with the the given name and the given type.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param type
	 *            - the condition type.
	 */
	public Condition(final String name, final String type) {
		super(name, type);
	}

	/**
	 * Creates a {@link Condition}.
	 */
	public Condition() {

	}

	/**
	 * @return condition created based on the given XML node.
	 * 
	 *         Creates a {@link Condition} from an XML {@link Node}.
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static Condition fromNode(Node node) throws AuthorizationException {

		String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);
		String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Read condition with name %s and type %s from xml node.", name, type));
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}

		Class<? extends Condition> conditionClazz = CLAZZ_MAP.get(type);

		if (conditionClazz == null) {
			String error = String.format("Invalid condition type %s for condition name %s  . Xml : %s", type, name,
					XmlUtil.getString(node));
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}
		Condition condition = null;
		try {
			condition = (Condition) ConstructorUtils.invokeConstructor(conditionClazz, new Object[] { name, node });
		} catch (NoSuchMethodException e) {
			LOGGER.error(e);
			throw new AuthorizationException("Error creating Condition instance.", e);
		} catch (IllegalAccessException e) {
			LOGGER.error(e);
			throw new AuthorizationException("Error creating Condition instance.", e);
		} catch (InvocationTargetException e) {
			LOGGER.error(e);
			throw new AuthorizationException("Error creating Condition instance.", e);
		} catch (InstantiationException e) {
			LOGGER.error(e);
			throw new AuthorizationException("Error creating Condition instance.", e);
		}

		return condition;

	}

	/**
	 * 
	 * Appends the XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the buffer to append the XML to.
	 */
	public void appendXML(StringBuilder buffer) {
		buffer.append("<Condition ");
		buffer.append("name=\"");
		buffer.append(getName());
		buffer.append("\"");
		buffer.append(" type=\"");
		buffer.append(getType());
		buffer.append("\"");
		buffer.append(">");
		appendInnerXML(buffer);
		buffer.append("</Condition>");
	}

	/**
	 * 
	 * Appends the XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the buffer to append the XML to.
	 */
	public abstract void appendInnerXML(StringBuilder buffer);

	/**
	 * 
	 * @return type of condition.
	 */
	@Override
	public abstract String getType();

	/**
	 * Get {@link Condition} by name from given list.
	 * 
	 * @param name
	 *            - {@link Condition} name
	 * @param list
	 *            - {@link List} of conditions
	 * @return - {@link Condition}
	 */
	public static Condition getConditionsByName(String name, List<Condition> list) {
		for (Iterator<Condition> iterator = list.iterator(); iterator.hasNext();) {
			Condition condition = (Condition) iterator.next();
			if (condition.getName().equals(name)) {
				return condition;
			}
		}
		return null;
	}

	/**
	 * Get {@link Condition} by name and type from given list.
	 * 
	 * @param name
	 *            - {@link Condition} name
	 * @param type
	 *            - {@link Condition} type
	 * @param list
	 *            - list of conditions
	 * @return - {@link Condition}
	 */
	public static Condition getConditionsByNameAndType(String name, String type, List<Condition> list) {
		for (Iterator<Condition> iterator = list.iterator(); iterator.hasNext();) {
			Condition condition = (Condition) iterator.next();
			if (condition.getName().equals(name) && condition.getType().equals(type)) {
				return condition;
			}
		}
		return null;
	}

}
