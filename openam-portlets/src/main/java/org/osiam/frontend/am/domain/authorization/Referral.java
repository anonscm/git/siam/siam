/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * 
 * This class encapsulates all data for a referral bean.
 * 
 */
public class Referral extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(Referral.class);

	// private static final Map<String, Class<? extends Referral>> CLAZZ_MAP =
	// new HashMap<String, Class<? extends Referral>>();

	private static final String VALUES = "Values";

	private String value;

	// static {
	// CLAZZ_MAP.put(SubOrgReferral.TYPE, SubOrgReferral.class);
	// CLAZZ_MAP.put(PeerOrgReferral.TYPE, PeerOrgReferral.class);
	// }

	/**
	 * Create referral with given name and type.
	 * 
	 * @param name
	 *            - referral name
	 * @param type
	 *            - referral type
	 */
	public Referral(String name, String type) {
		super(name, type);
	}

	/**
	 * Create referral.
	 */
	public Referral() {

	}

	// /**
	// * Append XML string containing object values.
	// * @param buffer - the string buffer to append to.
	// */
	// public abstract void appendInnerXML(StringBuilder buffer);

	/**
	 * Appends XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the string buffer to append to.
	 */
	public void appendXML(StringBuilder buffer) {
		buffer.append("<Referral ");

		buffer.append("name=\"");
		buffer.append(getName());
		buffer.append("\"");

		buffer.append(" type=\"");
		buffer.append(getType());
		buffer.append("\"");

		buffer.append(">");
		XmlUtil.appendAttributeValuePairToString(buffer, VALUES, value);

		// appendInnerXML(buffer);

		buffer.append("</Referral>");
	}

	// /**
	// * @return type of referral.
	// */
	// @Override
	// public abstract String getType();

	/**
	 * @return referral created based on the given XML node. Creates a
	 *         {@link Referral} from an XML {@link Node}.
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static Referral fromNode(Node node) throws AuthorizationException {

		String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
		String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Read Referral with name %s and type %s from xml node.", name, type));
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}
		String referralValues = null;

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Referral contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(VALUES)) {
				List<String> values = attributeValuePair.getValueList();
				if (values.size() > 0) {
					referralValues = values.get(0);
				}
			}
		}
		// Class<? extends Referral> clazz = CLAZZ_MAP.get(type);
		//
		// if (clazz == null) {
		// String error = String.format("Invalid type %s for %s  . Xml : %s",
		// type, name, XmlUtil.getString(node));
		// LOGGER.error(error);
		// throw new IllegalArgumentException(error);
		// }

		Referral referral = new Referral(name, type);
		// try {
		// referral = (Referral) ConstructorUtils.invokeConstructor(clazz, new
		// Object[] { name, node });
		// } catch (NoSuchMethodException e) {
		// throw new AuthorizationException("Error creating instance.", e);
		// } catch (IllegalAccessException e) {
		// throw new AuthorizationException("Error creating instance.", e);
		// } catch (InvocationTargetException e) {
		// throw new AuthorizationException("Error creating instance.", e);
		// } catch (InstantiationException e) {
		// throw new AuthorizationException("Error creating instance.", e);
		// }

		referral.setValue(referralValues);
		return referral;
	}

	/**
	 * @return value.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Set value.
	 * 
	 * @param value
	 *            - the value.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Get {@link Referral} by name from given list.
	 * 
	 * @param name
	 *            - {@link Referral} name
	 * @param list
	 *            - {@link List} of rules.
	 * @return - {@link Referral}
	 */
	public static Referral getReferralByName(String name, List<Referral> list) {
		for (Iterator<Referral> iterator = list.iterator(); iterator.hasNext();) {
			Referral referral = (Referral) iterator.next();
			if (referral.getName().equals(name)) {
				return referral;
			}
		}
		return null;
	}

}
