/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.IdentityRepositoryResponseProvider;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResponseProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * IDRepoResponseProviderController shows the add/edit
 * {@link IdentityRepositoryResponseProvider} form and does request processing
 * of the edit/add {@link IdentityRepositoryResponseProvider} action.
 * 
 */
@Controller("IDRepoResponseProviderController")
@RequestMapping(value = "view", params = { "ctx=IDRepoResponseProviderController" })
public class IDRepoResponseProviderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IDRepoResponseProviderController.class);

	@Autowired
	@Qualifier("identityRepositoryResponseProviderValidator")
	private Validator identityRepositoryResponseProviderValidator;

	/**
	 * Shows add/edit {@link IdentityRepositoryResponseProvider} form.
	 * 
	 * @param responseProviderName
	 *            - {@link ResponseProvider} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editResponseProvider(String responseProviderName, String policyName, String policyType,
			String action, Model model, RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "responseProvider")) {

			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			IdentityRepositoryResponseProvider responseProvider = (IdentityRepositoryResponseProvider) ResponseProvider
					.getResponseProviderByName(responseProviderName, policy.getResponseProviders());
			if (responseProvider == null) {
				responseProvider = new IdentityRepositoryResponseProvider(responseProviderName);
			}

			model.addAttribute("responseProvider", responseProvider);

		}
		model.addAttribute("responseProviderName", responseProviderName);

		model.addAttribute("actionValue", action);

		return "authorization/responsProvider/identityRepositoryResponseProvider/edit";

	}

	/**
	 * Add value to StaticAttributeList.
	 * 
	 * @param responseProvider
	 *            - {@link ResponseProvider}
	 * @param result
	 *            - BindingResult
	 * @param staticAttributeListAddValue
	 *            {@link String} value to be insert
	 * @param responseProviderName
	 *            - {@link ResponseProvider} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addStaticAttributeList")
	public void doAddStaticAttributeList(
			@ModelAttribute("responseProvider") @Valid IdentityRepositoryResponseProvider responseProvider,
			BindingResult result, String staticAttributeListAddValue, String responseProviderName, String policyName,
			String policyType, ActionRequest request, ActionResponse response) {

		if (staticAttributeListAddValue != null && !staticAttributeListAddValue.equals("")) {
			responseProvider.getStaticAttributeList().add(staticAttributeListAddValue);
		}

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("responseProviderName", responseProviderName);
	}

	/**
	 * Remove values from StaticAttributeList.
	 * 
	 * @param responseProvider
	 *            - {@link ResponseProvider}
	 * @param result
	 *            - BindingResult
	 * @param staticAttributeListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param responseProviderName
	 *            - {@link ResponseProvider} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteStaticAttributeList")
	public void doDeleteStaticAttributeList(
			@ModelAttribute("responseProvider") @Valid IdentityRepositoryResponseProvider responseProvider,
			BindingResult result, String[] staticAttributeListDeleteValues, String responseProviderName,
			String policyName, String policyType, ActionRequest request, ActionResponse response) {

		if (staticAttributeListDeleteValues != null) {
			for (int i = 0; i < staticAttributeListDeleteValues.length; i++) {
				responseProvider.getStaticAttributeList().remove(staticAttributeListDeleteValues[i]);
			}
		}

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("responseProviderName", responseProviderName);

	}

	/**
	 * Save {@link IdentityRepositoryResponseProvider} for current
	 * {@link Policy}.
	 * 
	 * @param responseProvider
	 *            - {@link ResponseProvider} to save
	 * @param result
	 *            - BindingResult
	 * @param responseProviderName
	 *            {@link ResponseProvider} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("responseProvider") @Valid IdentityRepositoryResponseProvider responseProvider,
			BindingResult result, String responseProviderName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("responseProviderName", responseProviderName);
		response.setRenderParameter("action", action);

		identityRepositoryResponseProviderValidator.validate(responseProvider, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		ResponseProvider responseProviderOld = ResponseProvider.getResponseProviderByName(responseProviderName,
				policy.getResponseProviders());

		policy.getResponseProviders().remove(responseProviderOld);
		policy.getResponseProviders().add(responseProvider);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link IdentityRepositoryResponseProvider} form.
	 * 
	 * @param responseProviderName
	 *            - {@link ResponseProvider} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String responseProviderName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("responseProviderName", responseProviderName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
