/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service;

import java.util.List;
import java.util.Set;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResponseProvider;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.domain.authorization.Subject;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;

import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.AMIdentity;
import com.sun.identity.sm.SMSException;

/**
 * Authorization service interface. Defines methods for policy management.
 */
public interface AuthorizationService {

	/**
	 * @return list of policy types.
	 */
	List<String> getPolicyTypesList();

	/**
	 * @return list of all defined policies for a given realm.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @throws AuthorizationException
	 *             - if something wrong happens. - if something wrong happens.
	 */
	List<Policy> getPoliciesList(SSOToken token, String realm) throws AuthorizationException;

	/**
	 * @return list of defined rules for a given policy.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	List<Rule> getRulesListForPolicy(SSOToken token, String realm, String policyName) throws AuthorizationException;

	/**
	 * @return list of rule types.
	 * @param token
	 *            - the SSO token
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	List<String> getRulesTypeList(SSOToken token) throws AuthorizationException;

	/**
	 * @return list of subjects for given policy.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	List<BaseEntity> getSubjectListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException;

	/**
	 * @return list of subject types.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	List<String> getSubjectTypeList(SSOToken token, String realm) throws AuthorizationException;

	/**
	 * @return list of defined conditions for given policy.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	List<BaseEntity> getConditionListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException;

	/**
	 * @return list of condition types.
	 */
	List<String> getConditionTypeList();

	/**
	 * @return list of defined response providers for given policy.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 * 
	 */
	List<BaseEntity> getResponseProviderListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException;

	/**
	 * @return list of response provider types.
	 */
	List<String> getResponsProviderTypeList();

	/**
	 * @return {@link Condition} with given name, for the given policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param conditionName
	 *            - the condition name to return
	 * @param policyName
	 *            - the policy name where the condition is attached
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Condition getCondition(SSOToken token, String realm, String policyName, String conditionName)
			throws AuthorizationException;

	/**
	 * Removes condition with given name from the given policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy where the condition is belonging
	 * @param conditionName
	 *            - the condition name to be removed
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void removeCondition(SSOToken token, String realm, String policyName, String conditionName)
			throws AuthorizationException;

	/**
	 * @return {@link Rule} with given name, for the given policy
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param ruleName
	 *            - the rule name to return
	 * @param policyName
	 *            - the policy name where the rule is attached
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Rule getRule(SSOToken token, String realm, String policyName, String ruleName) throws AuthorizationException;

	/**
	 * Removes rule with given name from the given policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy where the condition is belonging
	 * @param ruleName
	 *            - the rule name to be removed
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void removeRule(SSOToken token, String realm, String policyName, String ruleName) throws AuthorizationException;

	/**
	 * @return {@link Subject} with given name, for the given policy
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy name where the subject is attached
	 * @param subjectName
	 *            - the subject name to return
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Subject getSubject(SSOToken token, String realm, String policyName, String subjectName)
			throws AuthorizationException;

	/**
	 * @return {@link Set} with supported entity types
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * 
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Set<String> getSupportedEntityTypes(SSOToken token, String realm) throws AuthorizationException;

	/**
	 * @return {@link Set} with available {@link AMIdentity} for given filter.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param entity
	 *            - the entity type to filter.
	 * @param filter
	 *            - the filter pattern used to filter the returned data.
	 * 
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Set<AMIdentity> getAvailableIdentity(SSOToken token, String realm, String entity, String filter)
			throws AuthorizationException;

	/**
	 * Removes subject with given name from the given policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy where the condition is belonging
	 * @param subjectName
	 *            - the subject name to be removed
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void removeSubject(SSOToken token, String realm, String policyName, String subjectName)
			throws AuthorizationException;

	/**
	 * @return {@link ResponseProvider} with given name, for the given policy
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy name where the response provider is attached
	 * @param responseProviderName
	 *            - the response provider name
	 * 
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	ResponseProvider getResponseProvider(SSOToken token, String realm, String policyName, String responseProviderName)
			throws AuthorizationException;

	/**
	 * Removes response provider with given name from the given policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the policy where the condition is belonging
	 * @param responseProviderName
	 *            - the response provider name to be removed
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void removeResponseProvider(SSOToken token, String realm, String policyName, String responseProviderName)
			throws AuthorizationException;

	/**
	 * Adds a new policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policy
	 *            - the Policy to be added.
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void addPolicy(SSOToken token, String realm, Policy policy) throws AuthorizationException, SMSException;

	/**
	 * Removes a policy.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy to be removed
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void removePolicy(SSOToken token, String realm, String policyName) throws AuthorizationException;

	/**
	 * @return {@link Policy} with given name.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - the name of the policy to return
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	Policy getPolicy(SSOToken token, String realm, String policyName) throws AuthorizationException;

	/**
	 * Replace {@link Policy} with given old name by object given as parameter.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param policyOldName
	 *            - the name of the policy to return
	 * @param policy
	 *            - the new policy object
	 * @throws AuthorizationException
	 *             - if something wrong happens.
	 */
	void updatePolicy(SSOToken token, String realm, String policyOldName, Policy policy) throws AuthorizationException,
			SMSException;

	/**
	 * @return {@link AMIdentity} by given universalID.
	 * 
	 * @param token
	 *            - the SSOToken.
	 * @param universalId
	 *            - the given universalID.
	 * @throws AuthorizationException
	 *             - when something wrong happens.
	 */
	AMIdentity getAMIdentityByUniversalId(SSOToken token, String universalId) throws AuthorizationException;

	/**
	 * Verify if policy name already exist.
	 * 
	 * @param token
	 *            - the SSOToken
	 * @param realm
	 *            - the realm
	 * @param policyName
	 *            - policy name
	 * @return true if policy name already exist
	 */
	boolean policyNameAlreadyExist(SSOToken token, String realm, String policyName);

}
