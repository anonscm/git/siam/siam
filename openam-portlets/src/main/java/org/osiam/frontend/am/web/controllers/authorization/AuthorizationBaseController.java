/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;

import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.common.web.controllers.BaseController;

/**
 * Contains methods used in authorization portlet controllers.
 * 
 */
public class AuthorizationBaseController extends BaseController {

	/**
	 * Get policy form session and set value for name, description and active.
	 * 
	 * @param policy
	 *            - policy object that contains value for name, description and
	 *            active attribute
	 * @param request
	 *            - ActionRequest
	 * @return - {@link Policy}
	 */
	public Policy getPolicyFromSession(Policy policy, ActionRequest request) {

		Policy policySession = (Policy) request.getPortletSession().getAttribute("policy");

		if(policySession!=null && policy != null){
			policySession.setName(policy.getName());
			policySession.setDescription(policy.getDescription());
			policySession.setActive(policy.getActive());
		}
		
		return policySession;

	}

}
