/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Active Session Time Condition.
 */
public class SessionCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(SessionCondition.class);

	/**
	 * Condition type string code.
	 */
	public static final String TYPE = "SessionCondition";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String MAX_SESSION_TIME = "MaxSessionTime";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String TERMINATE_SESSION = "TerminateSession";

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Maximum session time (minutes).
	 */
	@NotNull
	@Min(1)
	private Integer maximumSessionTime;

	/**
	 * Whether this should terminate session.
	 */
	private String terminateSession;

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the condition name.
	 */
	public SessionCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public SessionCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public SessionCondition(String name, Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(MAX_SESSION_TIME)) {
				maximumSessionTime = new Integer(Integer.parseInt(attributeValuePair.getValueList().get(0)));
			} else if (attributeValuePair.getName().equals(TERMINATE_SESSION)) {
				terminateSession = attributeValuePair.getValueList().get(0);
			}
		}
	}

	public Integer getMaximumSessionTime() {
		return maximumSessionTime;
	}

	public void setMaximumSessionTime(Integer maximumSessionTime) {
		this.maximumSessionTime = maximumSessionTime;
	}

	public String getTerminateSession() {
		return terminateSession;
	}

	public void setTerminateSession(String terminateSession) {
		this.terminateSession = terminateSession;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuePairToString(buffer, MAX_SESSION_TIME, maximumSessionTime.toString());
		XmlUtil.appendAttributeValuePairToString(buffer, TERMINATE_SESSION, terminateSession);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
