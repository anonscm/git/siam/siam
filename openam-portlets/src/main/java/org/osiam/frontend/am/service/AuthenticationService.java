/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service;

import java.util.List;
import java.util.Set;

import org.osiam.frontend.am.domain.authentication.ChainProperty;
import org.osiam.frontend.am.exceptions.AuthenticationException;

import com.iplanet.sso.SSOToken;

/**
 * Authentication service interface. Defines methods for chain management.
 */
public interface AuthenticationService {

	/**
	 * @return the set of chain in the realm. If no chains are defined in the
	 *         realm, return an empty set.
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	Set<String> getChainNameSet(SSOToken token, String realm) throws AuthenticationException;

	/**
	 * Delete chain for the given name.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param chain
	 *            - the chain to be deleted
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	void deleteChain(SSOToken token, String realm, String chain) throws AuthenticationException;

	/**
	 * Save a new chain.
	 * 
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param chain
	 *            -chain name
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	void addNewChain(SSOToken token, String realm, String chain) throws AuthenticationException;

	/**
	 * @return the organization chain for a given realm.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	String getOrganizationChain(SSOToken token, String realm) throws AuthenticationException;

	/**
	 * @return the administrator chain for a given realm.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	String getAdministratorChain(SSOToken token, String realm) throws AuthenticationException;

	/**
	 * Save the authentication configuration (organization chain and
	 * administrator chain).
	 * 
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * @param organizationChain
	 *            - the organization chain
	 * @param administratorChain
	 *            - the administrator chain
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	void saveAuthenticationConfiguration(SSOToken token, String realm, String organizationChain,
			String administratorChain) throws AuthenticationException;

	/**
	 * @return the list of instances for a given realm.
	 * 
	 * @param token
	 *            - the SSO token
	 * @param realm
	 *            - the realm
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	Set<String> getInstanceList(SSOToken token, String realm) throws AuthenticationException;

	/**
	 * @return the list of possible criteria values.
	 */
	List<String> getCriteriaList();

	/**
	 * @return the list of properties for a chain.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param chain
	 *            - the chain
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	List<ChainProperty> getChainPropertyList(SSOToken token, String realm, String chain) throws AuthenticationException;

	/**
	 * Save the list of properties for a chain.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param chain
	 *            - the chain
	 * @param chainPropertyList
	 *            - the list of properties
	 * 
	 * @throws AuthenticationException
	 *             - if something wrong happens.
	 */
	void saveChainPropertyList(SSOToken token, String realm, String chain, List<ChainProperty> chainPropertyList)
			throws AuthenticationException;

	/**
	 * Verify if chain name already exist.
	 * 
	 * @param token
	 *            - the token
	 * @param realm
	 *            - the realm
	 * @param chainName
	 *            - chain name
	 * @return true in chain name already exist
	 */
	boolean chainNameAlreadyExist(SSOToken token, String realm, String chainName);
}
