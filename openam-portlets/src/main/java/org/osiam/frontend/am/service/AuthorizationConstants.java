/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service;

/**
 * Class holding constants for the {@link AuthorizationService}.
 */
public final class AuthorizationConstants {

	private AuthorizationConstants() {

	}

	/**
	 * Constant string describing policy type.
	 */
	public static final String POLICY_TYPE_XACML = "XACML";

	/**
	 * Constant string describing policy type.
	 */
	public static final String POLICY_TYPE_CDSSO = "CDSSO";

	/**
	 * Constant string describing policy type.
	 */
	public static final String POLICY_TYPE_REFERRAL = "REFERRAL";

	/**
	 * Constant string used as key to describe the policy status.
	 */
	public static final String ACTIVE = "active";

	/**
	 * Constant string used as key to describe the referral field policy.
	 */
	public static final String REFERRAL_POLICY = "referralPolicy";

	/**
	 * Constant string used as key to describe the last modified date.
	 */
	public static final String LAST_MODIFIED_DATE = "lastmodifieddate";

	/**
	 * Constant string used as key to describe the creation date.
	 */
	public static final String CREATION_DATE = "creationdate";

	/**
	 * Constant string used as key to describe the last modified by data.
	 */
	public static final String LAST_MODIFIED_BY = "lastmodifiedby";

	/**
	 * Constant string used as key to describe the created by data.
	 */
	public static final String CREATED_BY = "createdby";

	/**
	 * Constant string used as key to describe the description field.
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * Constant string used as key to describe the name field.
	 */
	public static final String NAME = "name";

	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_ROOT_NODE = "Policy";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_RULE_NODE = "Rule";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_SUBJECTS_NODE = "Subjects";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_SUBJECT_NODE = "Subject";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_CONDITIONS_NODE = "Conditions";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_CONDITION_NODE = "Condition";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_RESPONSE_PROVIDERS_NODE = "ResponseProviders";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_RESPONSE_PROVIDER_NODE = "ResponseProvider";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String SUBJECT = "Subject";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_REFERRALS_NODE = "Referrals";
	/**
	 * Constant used as a key when persisting this object to XML.
	 */
	public static final String POLICY_REFERRAL_NODE = "Referral";
	/**
	 * Constant string describing referral type.
	 */
	public static final String REFERRAL_TYPE_SUB_ORG_REFERRAL = "SubOrgReferral";
	/**
	 * Constant string describing referral type.
	 */
	public static final String REFERRAL_TYPE_PEER_ORG_REFERRAL = "PeerOrgReferral";
	/**
	 * Constant string describing rule type.
	 */
	public static final String RULE_TYPE_SUN_IDENTITY_SERVER_LIBERTY_PP_SERVICE = "sunIdentityServerLibertyPPService";
	/**
	 * Constant string describing rule type.
	 */
	public static final String RULE_TYPE_IPLANET_AM_WEB_AGENT_SERVICE = "iPlanetAMWebAgentService";
	/**
	 * Constant string describing rule type.
	 */
	public static final String RULE_TYPE_SUN_IDENTITY_SERVER_DISCOVERY_SERVICE = "sunIdentityServerDiscoveryService";
	/**
	 * Constant string describing subject type.
	 */
	public static final String SUBJECT_TYPE_AUTHENTICATED_USERS = "AuthenticatedUsers";
	/**
	 * Constant string describing subject type.
	 */
	public static final String SUBJECT_TYPE_AM_IDENTITY_SUBJECT = "AMIdentitySubject";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_SESSION_CONDITION = "SessionCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_AUTHENTICATE_TO_SERVICE_CONDITION = "AuthenticateToServiceCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_AUTH_SCHEME_CONDITION = "AuthSchemeCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_AUTH_LEVEL_CONDITION = "AuthLevelCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_LE_AUTH_LEVEL_CONDITION = "LEAuthLevelCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_AUTHENTICATE_TO_REALM_CONDITION = "AuthenticateToRealmCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_SESSION_PROPERTY_CONDITION = "SessionPropertyCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_IP_CONDITION = "IPCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_RESOURCE_ENV_IP_CONDITION = "ResourceEnvIPCondition";
	/**
	 * Constant string describing condition type.
	 */
	public static final String CONDITION_TYPE_SIMPLE_TIME_CONDITION = "SimpleTimeCondition";

}
