/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.validator.authorization;

import org.osiam.frontend.am.domain.authorization.IPDNSCondition;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for IPDNSCondition.
 * <p>
 * IP Address or DNS Name is required.
 * </p>
 * 
 */
@Component("ipDNSConditionValidator")
public class IPDNSConditionValidator implements Validator {

	@Override
	public boolean supports(Class<?> klass) {
		return IPDNSCondition.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		IPDNSCondition condition = (IPDNSCondition) target;

		boolean fromNull = ((condition.getFromIPBlock1() == null || condition.getFromIPBlock1().equals(Long.valueOf(0)))
				&& (condition.getFromIPBlock2() == null || condition.getFromIPBlock2().equals(Long.valueOf(0)))
				&& (condition.getFromIPBlock3() == null || condition.getFromIPBlock3().equals(Long.valueOf(0))) && (condition
				.getFromIPBlock4() == null || condition.getFromIPBlock4().equals(Long.valueOf(0)))) ? true : false;

		boolean toNull = ((condition.getToIPBlock1() == null || condition.getToIPBlock1().equals(Long.valueOf(0)))
				&& (condition.getToIPBlock2() == null || condition.getToIPBlock2().equals(Long.valueOf(0)))
				&& (condition.getToIPBlock3() == null || condition.getToIPBlock3().equals(Long.valueOf(0))) && (condition
				.getToIPBlock4() == null || condition.getToIPBlock1().equals(Long.valueOf(0)))) ? true : false;

		boolean dnsNull = (condition.getDns() == null || "".equals(condition.getDns())) ? true : false;

		if (fromNull && toNull && dnsNull) {
			errors.rejectValue("name", "AtLeastOne");
		}

		if (!fromNull && !toNull) {
			boolean error = false;

			if (condition.getFromIPBlock1() > condition.getToIPBlock1()) {
				error = true;
			} else if (condition.getFromIPBlock1().equals(condition.getToIPBlock1())) {
				if (condition.getFromIPBlock2() > condition.getToIPBlock2()) {
					error = true;
				} else if (condition.getFromIPBlock2().equals(condition.getToIPBlock2())) {
					if (condition.getFromIPBlock3() > condition.getToIPBlock3()) {
						error = true;
					} else if (condition.getFromIPBlock3().equals(condition.getToIPBlock3())) {
						if (condition.getFromIPBlock4() > condition.getToIPBlock4()) {
							error = true;
						}
					}
				}
			}

			if (error) {
				errors.rejectValue("fromIPBlock1", "StartLargerEnd");
			}
		}
	}
}
