/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.validator.authentication;

import org.osiam.frontend.am.domain.authentication.Chain;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.domain.ObjectForValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for Chain object.
 * <p>
 * Validate if the Chain name already exist.
 * </p>
 * 
 */
@Component("chainValidator")
public class ChainValidator implements Validator {

	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public boolean supports(Class<?> klass) {
		return Chain.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ObjectForValidate obj = (ObjectForValidate) target;
		Chain chain = (Chain) obj.getObject();

		if (authenticationService.chainNameAlreadyExist(obj.getToken(), obj.getRealm(), chain.getName())) {
			errors.rejectValue("name", "NameExist");
		}
	}
}
