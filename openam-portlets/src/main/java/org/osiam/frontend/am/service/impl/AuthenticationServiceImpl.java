/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.domain.authentication.ChainProperty;
import org.osiam.frontend.am.exceptions.AuthenticationException;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.service.impl.BaseSSOService;
import org.osiam.frontend.util.XmlUtil;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.authentication.util.ISAuthConstants;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceConfig;
import com.sun.identity.sm.ServiceConfigManager;
import com.sun.identity.sm.ServiceNotFoundException;
import com.sun.identity.sm.ServiceSchema;
import com.sun.identity.sm.ServiceSchemaManager;

/**
 * Authentication service implementation. Defines methods for chain management.
 */
@Service("AuthenticationService")
public class AuthenticationServiceImpl extends BaseSSOService implements AuthenticationService {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationServiceImpl.class);

	private static final String NAMED_CONFIGURATION = "Configurations";
	private static final String NAMED_CONFIGURATION_ID = "NamedConfiguration";

	private static final String SERVICE_NAME = "iPlanetAMAuthConfiguration";

	private static final String ATTR_VALUE_PAIR_NODE = "AttributeValuePair";
	private static final String ATTR_VALUE_NODE = "Value";

	private static final String SERVICE_VERSION = "1.0";

	private void validateChainParam(String chain) {
		if (chain == null) {
			throw new IllegalArgumentException(String.format("Parameter chain cannot be null. Example : %s", "'chain'"));
		}

	}

	private void validateServiceConfig(ServiceConfig serviceConfig, String realm) throws AuthenticationException {
		if (serviceConfig == null) {
			LOGGER.error(String.format("Realm '%s' is not registered", realm));
			throw new AuthenticationException(String.format("Realm '%s' is not registered", realm));
		}
	}

	private void validateNamedConfig(ServiceConfig namedConfig) throws AuthenticationException {
		if (namedConfig == null) {
			LOGGER.error(String.format("ServiceConfig '%s' is not registered", NAMED_CONFIGURATION));
			throw new AuthenticationException(
					String.format("ServiceConfig '%s' is not registered", NAMED_CONFIGURATION));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getChainNameSet(SSOToken token, String realm) throws AuthenticationException {

		validateTokenRealmParams(token, realm);

		ServiceConfigManager serviceConfigManager;
		Set<String> chainSet = null;
		try {
			serviceConfigManager = new ServiceConfigManager(token, ISAuthConstants.AUTHCONFIG_SERVICE_NAME,
					SERVICE_VERSION);

			ServiceConfig serviceConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(serviceConfig, realm);

			ServiceConfig namedConfig = serviceConfig.getSubConfig(NAMED_CONFIGURATION);
			validateNamedConfig(namedConfig);

			chainSet = namedConfig.getSubConfigNames("*");
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("getChainNameSet, return value - chainSet =%s", chainSet));
		}

		return chainSet;
	}

	@Override
	public void deleteChain(SSOToken token, String realm, String chain) throws AuthenticationException {

		validateTokenRealmParams(token, realm);
		validateChainParam(chain);

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(SERVICE_NAME, token);
			ServiceConfig serviceConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(serviceConfig, realm);

			ServiceConfig namedConfig = serviceConfig.getSubConfig(NAMED_CONFIGURATION);
			validateNamedConfig(namedConfig);

			ServiceConfig chainConfig = namedConfig.getSubConfig(chain);
			if (chainConfig == null) {
				LOGGER.warn(String.format("Chain '%s' is not registered", chain));
				return;
			}

			namedConfig.removeSubConfig(chain);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Chain deleted - chain =%s, realm=%s", chain, realm));
			}

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@Override
	public void addNewChain(SSOToken token, String realm, String chain) throws AuthenticationException {

		validateTokenRealmParams(token, realm);
		validateChainParam(chain);

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(token, SERVICE_NAME, SERVICE_VERSION);

			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			ServiceConfig authConfig = organizationConfig.getSubConfig(NAMED_CONFIGURATION);
			if (authConfig == null) {
				organizationConfig.addSubConfig(NAMED_CONFIGURATION, null, 0, null);
				authConfig = organizationConfig.getSubConfig(NAMED_CONFIGURATION);
			}

			authConfig.addSubConfig(chain, NAMED_CONFIGURATION_ID, 0, Collections.EMPTY_MAP);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@Override
	public String getOrganizationChain(SSOToken token, String realm) throws AuthenticationException {

		validateTokenRealmParams(token, realm);

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(token, ISAuthConstants.AUTH_SERVICE_NAME, "1.0");
			ServiceConfig serviceConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(serviceConfig, realm);

			@SuppressWarnings("unchecked")
			Set<String> set = (Set<String>) serviceConfig.getAttributes().get(ISAuthConstants.AUTHCONFIG_ORG);

			String organizationChain = null;

			if (set != null) {
				Iterator<String> iterator = set.iterator();
				if (iterator.hasNext()) {
					organizationChain = iterator.next();
				}
			}

			return organizationChain;
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}

	}

	@Override
	public String getAdministratorChain(SSOToken token, String realm) throws AuthenticationException {

		validateTokenRealmParams(token, realm);

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(token, ISAuthConstants.AUTH_SERVICE_NAME, "1.0");
			ServiceConfig serviceConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(serviceConfig, realm);

			@SuppressWarnings("unchecked")
			Set<String> set = (Set<String>) serviceConfig.getAttributes().get(ISAuthConstants.AUTHCONFIG_ADMIN);

			String organizationChain = null;
			if (set != null) {
				Iterator<String> iterator = set.iterator();
				if (iterator.hasNext()) {
					organizationChain = iterator.next();
				}
			}

			return organizationChain;
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@Override
	public void saveAuthenticationConfiguration(SSOToken token, String realm, String organizationConfiguration,
			String administratorConfiguration) throws AuthenticationException {

		validateTokenRealmParams(token, realm);

		if (organizationConfiguration == null) {
			throw new IllegalArgumentException(
					"Param organizationConfiguration cannot be null. Example : 'organization'.");
		}

		if (administratorConfiguration == null) {
			throw new IllegalArgumentException("Param administratorConfiguration cannot be null. Example : 'admin'");
		}

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(token, ISAuthConstants.AUTH_SERVICE_NAME, "1.0");

			ServiceConfig serviceConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(serviceConfig, realm);

			Map<String, Set<String>> modifiedValues = new HashMap<String, Set<String>>();

			Set<String> organizationConfigurationSet = new HashSet<String>();
			organizationConfigurationSet.add(organizationConfiguration);

			Set<String> administratorConfigurationSet = new HashSet<String>();
			administratorConfigurationSet.add(administratorConfiguration);

			modifiedValues.put(ISAuthConstants.AUTHCONFIG_ORG, organizationConfigurationSet);
			modifiedValues.put(ISAuthConstants.AUTHCONFIG_ADMIN, administratorConfigurationSet);
			serviceConfig.setAttributes(modifiedValues);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@Override
	public List<ChainProperty> getChainPropertyList(SSOToken token, String realm, String chain)
			throws AuthenticationException {

		validateTokenRealmParams(token, realm);
		validateChainParam(chain);

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(SERVICE_NAME, token);

			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateNamedConfig(organizationConfig);

			ServiceConfig namedConfig = organizationConfig.getSubConfig(NAMED_CONFIGURATION);
			validateNamedConfig(namedConfig);

			ServiceConfig pConfig = namedConfig.getSubConfig(chain);
			validateNamedConfig(pConfig);

			@SuppressWarnings("unchecked")
			Map<String, Set<String>> attributeDataMap = pConfig.getAttributes();
			Set<String> s = attributeDataMap.get(ISAuthConstants.AUTHCONFIG_ROLE);
			Iterator<String> i = s.iterator();
			List<ChainProperty> chainPropertieList = new ArrayList<ChainProperty>();

			while (i.hasNext()) {
				String a = i.next();
				try {
					chainPropertieList.addAll(xmlToAuthConfigurationEntry(a));
				} catch (UnsupportedEncodingException e) {
					throw new AuthenticationException(e.getMessage(), e);
				}
			}

			return chainPropertieList;
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@Override
	public boolean chainNameAlreadyExist(SSOToken token, String realm, String chainName) {
		validateTokenRealmParams(token, realm);
		if (chainName != null && !"".equals(chainName)) {
			ServiceConfigManager serviceConfigManager;
			try {
				serviceConfigManager = new ServiceConfigManager(SERVICE_NAME, token);

				ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
				validateNamedConfig(organizationConfig);

				ServiceConfig namedConfig = organizationConfig.getSubConfig(NAMED_CONFIGURATION);
				validateNamedConfig(namedConfig);

				ServiceConfig pConfig = namedConfig.getSubConfig(chainName);
				return (pConfig != null);

			} catch (SSOException e) {
				LOGGER.error("SSOException occured", e);
				throw new AuthenticationException(e.getMessage(), e);

			} catch (SMSException e) {
				LOGGER.error("SMSException occured", e);
				throw new AuthenticationException(e.getMessage(), e);
			}
		} else {
			return false;
		}
	}

	@Override
	public void saveChainPropertyList(SSOToken token, String realm, String chain, List<ChainProperty> chainPropertyList)
			throws AuthenticationException {

		validateTokenRealmParams(token, realm);
		validateChainParam(chain);

		if (chainPropertyList == null) {
			throw new IllegalArgumentException(String.format("Parameter chainPropertyList cannot be null."));
		}

		ServiceConfigManager serviceConfigManager;
		try {
			serviceConfigManager = new ServiceConfigManager(SERVICE_NAME, token);

			ServiceConfig organizationConfig = serviceConfigManager.getOrganizationConfig(realm, null);
			validateServiceConfig(organizationConfig, realm);

			ServiceConfig namedConfig = organizationConfig.getSubConfig(NAMED_CONFIGURATION);
			validateNamedConfig(namedConfig);

			ServiceConfig pConfig = namedConfig.getSubConfig(chain);
			validateNamedConfig(pConfig);

			@SuppressWarnings("unchecked")
			Map<String, Set<String>> attributeDataMap = pConfig.getAttributes();

			String xmlValue = authConfigurationEntryToXMLString(chainPropertyList);
			Set<String> s = new HashSet<String>(2);
			s.add(xmlValue);
			attributeDataMap.put(ISAuthConstants.AUTHCONFIG_ROLE, s);
			pConfig.setAttributes(attributeDataMap);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getInstanceList(SSOToken token, String realm) throws AuthenticationException {

		Set<String> authTypes = new HashSet<String>();
		Map<String, String> moduleServiceNames = new HashMap<String, String>();

		Map<String, String> moduleServiceMap = new HashMap<String, String>();

		// Set<String> globalModuleNames = Collections.EMPTY_SET;

		ServiceSchemaManager scm;
		try {
			scm = new ServiceSchemaManager(ISAuthConstants.AUTH_SERVICE_NAME, token);

			ServiceSchema schema = scm.getGlobalSchema();
			Set<String> authenticators = (Set<String>) schema.getAttributeDefaults()
					.get(ISAuthConstants.AUTHENTICATORS);
			for (Iterator<String> it = authenticators.iterator(); it.hasNext();) {
				String module = it.next();
				int index = module.lastIndexOf(".");
				if (index != -1) {
					module = module.substring(index + 1);
				}
				// Application is not one of the selectable instance type.
				if (!module.equals(ISAuthConstants.APPLICATION_MODULE)) {
					authTypes.add(module);
				}
				String serviceName = moduleServiceNames.get(module);
				if (serviceName == null) {

					serviceName = moduleServiceMap.get(module);
					if (serviceName == null) {

						if (module.equals("RADIUS")) {
							serviceName = "iPlanetAMAuthRadiusService";
						} else {
							serviceName = "iPlanetAMAuth" + module + "Service";
						}

						try {
							new ServiceSchemaManager(serviceName, token);
						} catch (Exception e) {
							serviceName = ISAuthConstants.AUTH_ATTR_PREFIX_NEW + module + "Service";
						}
						moduleServiceMap.put(module, serviceName);
					}

					try {
						new ServiceSchemaManager(serviceName, token);
						synchronized (moduleServiceNames) {
							Map<String, String> newMap = new HashMap<String, String>(moduleServiceNames);
							newMap.put(module, serviceName);
							moduleServiceNames = newMap;
						}
					} catch (Exception e) {
						authTypes.remove(module);
						continue;
					}
				}
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthenticationException(e.getMessage(), e);
		}

		Set<String> instanceSet = new HashSet<String>();
		Iterator<String> iterator = moduleServiceMap.keySet().iterator();
		while (iterator.hasNext()) {

			String key = iterator.next();

			String serviceName = moduleServiceMap.get(key);

			ServiceConfig config;
			try {
				ServiceConfigManager serviceConfig = new ServiceConfigManager(serviceName, token);

				config = serviceConfig.getOrganizationConfig(realm, null);

				Map<?, ?> defaultAttrs = null;
				if (config != null) {
					defaultAttrs = config.getAttributesWithoutDefaults();
				}
				if (defaultAttrs != null && !defaultAttrs.isEmpty()) {
					instanceSet.add(key);
				}

				instanceSet.addAll(config.getSubConfigNames());
			} catch (ServiceNotFoundException e) {
				LOGGER.warn("Service not found - " + serviceName, e);
			} catch (Exception e) {
				System.out.println("#########################3Exception occured - " + serviceName);
				LOGGER.error("Exception occured - " + serviceName, e);
				throw new AuthenticationException(e.getMessage(), e);
			}

		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("getInstanceList returned %d instances", instanceSet.size()));
		}

		return instanceSet;
	}

	@Override
	public List<String> getCriteriaList() {
		return Arrays.asList("REQUIRED", "OPTIONAL", "SUFFICIENT", "REQUISITE");
	}

	private List<ChainProperty> xmlToAuthConfigurationEntry(String xmlValue) throws UnsupportedEncodingException {

		List<ChainProperty> entries = new ArrayList<ChainProperty>();
		// call util method to parse the document
		Document document = XmlUtil.toDOMDocument(xmlValue);
		if (document == null) {
			return entries;
		}

		// get document elements of the documents
		Element valuePair = document.getDocumentElement();
		// retrieve child elements (<Value>) of the root (<AttributeValuePair>)
		// each element corresponding to one AuthConfigurationEntry
		NodeList children = valuePair.getChildNodes();
		final int number = children.getLength();
		// process each child
		for (int i = 0; i < number; i++) {
			try {
				entries.add(getChainPropertyFormNode(children.item(i)));
			} catch (Exception e) {
				LOGGER.error("parseValue", e);
				// continue next item
			}
		}
		return entries;
	}

	private String authConfigurationEntryToXMLString(List<ChainProperty> entries) {

		if (entries != null) {
			final byte stringBufferLength = 100;
			StringBuffer sb = new StringBuffer(stringBufferLength);
			sb.append("<" + ATTR_VALUE_PAIR_NODE + ">");
			int len = entries.size();
			for (int i = 0; i < len; i++) {
				ChainProperty entry = entries.get(i);
				sb.append("<").append(ATTR_VALUE_NODE).append(">").append(entry.getInstance()).append(" ")
						.append(entry.getCriteria().toString()).append(" ");
				String options = entry.getOptions();
				if (options != null) {
					sb.append(options.toString());
				}
				sb.append("</").append(ATTR_VALUE_NODE).append(">");
			}
			sb.append("</" + ATTR_VALUE_PAIR_NODE + ">");

			return sb.toString();
		} else {
			return "";
		}
	}

	private void checkFlag(String flag) throws AuthenticationException {
		if (flag == null || flag.length() == 0) {
			throw new AuthenticationException("Invalid config.");
		}
		if (!flag.equalsIgnoreCase("REQUIRED") && !flag.equalsIgnoreCase("OPTIONAL")
				&& !flag.equalsIgnoreCase("REQUISITE") && !flag.equalsIgnoreCase("SUFFICIENT")) {
			LOGGER.error(String.format("AuthConfigEntry, invalid flag : %s", flag));
			throw new AuthenticationException("Invalid config.");
		}
	}

	private ChainProperty getChainPropertyFormNode(Node node) throws AuthenticationException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("AuthConfigEntry, value=%s", node.toString()));
		}

		String value = node.getFirstChild().getNodeValue();
		if (value == null || value.length() == 0) {
			LOGGER.error(String.format("AuthConfigEntry, invalid value=%s", value));
			throw new AuthenticationException("Invalid config.");
		}

		value = value.trim();
		int pos = value.indexOf(" ");
		if (pos == -1) {
			LOGGER.error(String.format("AuthConfigEntry, invalid value=%s", value));
			throw new AuthenticationException("Invalid config.");
		}
		// set module
		String module;
		String flag;
		String options = null;
		module = value.substring(0, pos);
		value = value.substring(pos + 1).trim();
		pos = value.indexOf(" ");
		if (pos == -1) {
			// no options
			flag = value;
		} else {
			flag = value.substring(0, pos);
			options = value.substring(pos + 1).trim();
		}
		checkFlag(flag);

		ChainProperty property = new ChainProperty();
		property.setInstance(module);
		property.setCriteria(flag);
		property.setOptions(options);

		return property;

	}

}
