/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.validator.authorization;

import java.util.Iterator;
import java.util.regex.Pattern;

import org.osiam.frontend.am.domain.authorization.ResourceEnvironmentIPCondition;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the ResourceEnvironmentIPCondition object.
 * <p>
 * Validate if the element from Condition List are valid.
 * </p>
 * 
 */
@Component("resourceEnvironmentIPConditionValidator")
public class ResourceEnvironmentIPConditionValidator implements Validator {

	private static final Pattern PATTERN = Pattern.compile("^IF\\s.+\\sTHEN\\s.+");

	@Override
	public boolean supports(Class<?> klass) {
		return ResourceEnvironmentIPCondition.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ResourceEnvironmentIPCondition object = (ResourceEnvironmentIPCondition) target;

		if(object.getConditionList().size()>0){
			boolean valueValid = true;

			for (Iterator<String> iterator = object.getConditionList().iterator(); iterator.hasNext();) {
				String value = iterator.next();
				if (!PATTERN.matcher(value).matches()) {
					valueValid = false;
					break;
				}
			}

			if (!valueValid) {
				errors.rejectValue("conditionList", "InvalidPattern");
			}
		}else{
			errors.rejectValue("conditionList", "NotEmptyList");
		}
	}
}
