/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * The Rule class encapsulates the data for a rule.
 */
public class URLPolicyAgentRule extends Rule {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The type string code for this kind of subject.
	 */
	public static final String TYPE = "iPlanetAMWebAgentService";

	private static final String GET = "GET";
	private static final String POST = "POST";

	private boolean get;
	private String getValue;
	private boolean post;
	private String postValue;

	private static final Logger LOGGER = Logger.getLogger(URLPolicyAgentRule.class);

	/**
	 * Creates a new rule with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public URLPolicyAgentRule(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new rule.
	 */
	public URLPolicyAgentRule() {

	}

	/**
	 * Creates a new rule with the given name by parsing the given XML node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public URLPolicyAgentRule(String name, Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Rule XML contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {
			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);
			if (attributeValuePair.getName().equals(GET)) {
				getValue = attributeValuePair.getValueList().get(0);
				get = true;
			} else if (attributeValuePair.getName().equals(POST)) {
				postValue = attributeValuePair.getValueList().get(0);
				post = true;
			}
		}
	}

	/**
	 * Creates a new rule with the given name and fields.
	 * 
	 * @param name
	 *            - the name.
	 * @param getValue
	 *            - the value for the GET field.
	 * @param postValue
	 *            - the value for the POST field.
	 * @param resourceName
	 *            - the value for the resourceName field.
	 */
	public URLPolicyAgentRule(String name, String getValue, String postValue, String resourceName) {
		super(name, TYPE, resourceName);
		this.getValue = postValue;
		get = true;
		this.postValue = getValue;
		post = true;

	}

	public String getGetValue() {
		return getValue;
	}

	public void setGetValue(String queryValue) {
		this.getValue = queryValue;
	}

	public String getPostValue() {
		return postValue;
	}

	public void setPostValue(String updateValue) {
		this.postValue = updateValue;
	}

	public boolean getGet() {
		return get;
	}

	public void setGet(boolean get) {
		this.get = get;
	}

	public boolean getPost() {
		return post;
	}

	public void setPost(boolean post) {
		this.post = post;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		if (get) {
			XmlUtil.appendAttributeValuePairToString(buffer, GET, getValue);
		}
		if (post) {
			XmlUtil.appendAttributeValuePairToString(buffer, POST, postValue);
		}
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
