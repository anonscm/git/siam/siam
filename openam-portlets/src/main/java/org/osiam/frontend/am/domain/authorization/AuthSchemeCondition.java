/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Authentication By Module Instance
 * Condition.
 */
public class AuthSchemeCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(AuthSchemeCondition.class);

	/**
	 * The type string constant for this type of condition.
	 */
	public static final String TYPE = "AuthSchemeCondition";

	private static final String APPLICATION_NAME = "ApplicationName";
	private static final String APPLICATION_IDLE_TIMEOUT = "ApplicationIdleTimeout";
	private static final String AUTH_SCHEME = "AuthScheme";

	private static final long serialVersionUID = 1L;

	private List<String> instanceList = new ArrayList<String>();
	private String applicationName;
	private Long timeout;

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public AuthSchemeCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public AuthSchemeCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public AuthSchemeCondition(String name, Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(APPLICATION_NAME)) {
				applicationName = attributeValuePair.getValueList().get(0);
			} else if (attributeValuePair.getName().equals(APPLICATION_IDLE_TIMEOUT)) {
				timeout = new Long(Long.parseLong(attributeValuePair.getValueList().get(0)));
			} else if (attributeValuePair.getName().equals(AUTH_SCHEME)) {
				instanceList = attributeValuePair.getValueList();
			}
		}

	}

	public List<String> getInstanceList() {
		return instanceList;
	}

	public void setInstanceList(List<String> instanceList) {
		this.instanceList = instanceList;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Long getTimeout() {
		return timeout;
	}

	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuesPairToString(buffer, AUTH_SCHEME, instanceList);
		if (applicationName != null && !"".equals(applicationName)) {
			XmlUtil.appendAttributeValuePairToString(buffer, APPLICATION_NAME, applicationName);
		}
		if (timeout != null) {
			XmlUtil.appendAttributeValuePairToString(buffer, APPLICATION_IDLE_TIMEOUT, timeout.toString());
		}
	}

}
