/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Ip Address Dns name Condition.
 */
public class IPDNSCondition extends Condition {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(IPDNSCondition.class);

	private static final String START_IP = "StartIp";
	private static final String END_IP = "EndIp";
	private static final String DNS_NAME = "DnsName";

	private static final byte POZ0 = 0;
	private static final byte POZ1 = 1;
	private static final byte POZ2 = 2;
	private static final byte POZ3 = 3;

	/**
	 * Condition type value.
	 */
	public static final String TYPE = "IPCondition";

	@Min(0)
	@Max(255)
	private Long fromIPBlock1;
	@Min(0)
	@Max(255)
	private Long fromIPBlock2;
	@Min(0)
	@Max(255)
	private Long fromIPBlock3;
	@Min(0)
	@Max(255)
	private Long fromIPBlock4;

	@Min(0)
	@Max(255)
	private Long toIPBlock1;
	@Min(0)
	@Max(255)
	private Long toIPBlock2;
	@Min(0)
	@Max(255)
	private Long toIPBlock3;
	@Min(0)
	@Max(255)
	private Long toIPBlock4;

	private String dns;

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public IPDNSCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		String startIp = "";
		String endIp = "";
		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(START_IP)) {
				String value = attributeValuePair.getValueList().get(0);
				startIp = value;
			} else if (attributeValuePair.getName().equals(END_IP)) {
				String value = attributeValuePair.getValueList().get(0);
				endIp = value;
			} else if (attributeValuePair.getName().equals(DNS_NAME)) {
				String value = attributeValuePair.getValueList().get(0);
				dns = value;
			}
		}
		String[] ipParts = null;
		if (startIp.length() > 0) {
			ipParts = startIp.split("\\.");
			fromIPBlock1 = Long.valueOf(ipParts[POZ0]);
			fromIPBlock2 = Long.valueOf(ipParts[POZ1]);
			fromIPBlock3 = Long.valueOf(ipParts[POZ2]);
			fromIPBlock4 = Long.valueOf(ipParts[POZ3]);
		}
		if (endIp.length() > 0) {
			ipParts = endIp.split("\\.");
			toIPBlock1 = Long.valueOf(ipParts[POZ0]);
			toIPBlock2 = Long.valueOf(ipParts[POZ1]);
			toIPBlock3 = Long.valueOf(ipParts[POZ2]);
			toIPBlock4 = Long.valueOf(ipParts[POZ3]);
		}

	}

	/**
	 * Creates a new condition with the given name.
	 * 
	 * 
	 * @param name
	 *            - the name.
	 */
	public IPDNSCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public IPDNSCondition() {

	}

	public Long getFromIPBlock1() {
		return fromIPBlock1;
	}

	public void setFromIPBlock1(Long fromIPBlock1) {
		this.fromIPBlock1 = fromIPBlock1;
	}

	public Long getFromIPBlock2() {
		return fromIPBlock2;
	}

	public void setFromIPBlock2(Long fromIPBlock2) {
		this.fromIPBlock2 = fromIPBlock2;
	}

	public Long getFromIPBlock3() {
		return fromIPBlock3;
	}

	public void setFromIPBlock3(Long fromIPBlock3) {
		this.fromIPBlock3 = fromIPBlock3;
	}

	public Long getFromIPBlock4() {
		return fromIPBlock4;
	}

	public void setFromIPBlock4(Long fromIPBlock4) {
		this.fromIPBlock4 = fromIPBlock4;
	}

	public Long getToIPBlock1() {
		return toIPBlock1;
	}

	public void setToIPBlock1(Long toIPBlock1) {
		this.toIPBlock1 = toIPBlock1;
	}

	public Long getToIPBlock2() {
		return toIPBlock2;
	}

	public void setToIPBlock2(Long toIPBlock2) {
		this.toIPBlock2 = toIPBlock2;
	}

	public Long getToIPBlock3() {
		return toIPBlock3;
	}

	public void setToIPBlock3(Long toIPBlock3) {
		this.toIPBlock3 = toIPBlock3;
	}

	public Long getToIPBlock4() {
		return toIPBlock4;
	}

	public void setToIPBlock4(Long toIPBlock4) {
		this.toIPBlock4 = toIPBlock4;
	}

	public String getDns() {
		return dns;
	}

	public void setDns(String dns) {
		this.dns = dns;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {

		boolean fromNull = ((fromIPBlock1 == null || fromIPBlock1.equals(Long.valueOf(0)))
				&& (fromIPBlock2 == null || fromIPBlock2.equals(Long.valueOf(0)))
				&& (fromIPBlock3 == null || fromIPBlock3.equals(Long.valueOf(0))) && (fromIPBlock4 == null || fromIPBlock4
				.equals(Long.valueOf(0)))) ? true : false;

		boolean toNull = ((toIPBlock1 == null || toIPBlock1.equals(Long.valueOf(0)))
				&& (toIPBlock2 == null || toIPBlock2.equals(Long.valueOf(0)))
				&& (toIPBlock3 == null || toIPBlock3.equals(Long.valueOf(0))) && (toIPBlock4 == null || toIPBlock1
				.equals(Long.valueOf(0)))) ? true : false;

		if (!fromNull) {
			XmlUtil.appendAttributeValuePairToString(buffer, START_IP, fromIPBlock1 + "." + fromIPBlock2 + "."
					+ fromIPBlock3 + "." + fromIPBlock4);
		}
		if (!toNull) {
			XmlUtil.appendAttributeValuePairToString(buffer, END_IP, toIPBlock1 + "." + toIPBlock2 + "." + toIPBlock3
					+ "." + toIPBlock4);
		}

		if (dns != null && !"".equals(dns)) {
			XmlUtil.appendAttributeValuePairToString(buffer, DNS_NAME, dns);
		}

	}
}
