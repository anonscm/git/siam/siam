/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.Date;
import java.util.Iterator;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.Referral;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.service.RealmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;
import com.sun.identity.sm.SMSException;

/**
 * REFERRALPolicyController shows the edit "REFERRAL Policy" form and does
 * request processing of the edit policy action.
 * 
 */
@Controller("REFERRALPolicyController")
@RequestMapping(value = "view", params = "ctx=REFERRALPolicyController")
public class REFERRALPolicyController extends AuthorizationBaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(REFERRALPolicyController.class);

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private RealmService realmService;

	/**
	 * Shows add/edit {@link Policy} form.
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param checkAllRule
	 *            - use to decide if all the {@link Rule} are checked for
	 *            deletion
	 * @param checkAllReferral
	 *            - use to decide if all the {@link Referral} are checked for
	 *            deletion
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display when policy is modified
	 * @param error
	 *            - error message to display
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editPolicy(String policyName, String action, String checkAllRule, String checkAllReferral,
			Model model, String message, String error, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		// if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX +
		// "policy")) {
		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");
		if (policy == null) {
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				policy = authorizationService.getPolicy(token, realm, policyName);
			} else {
				policy = new Policy(policyName, AuthorizationConstants.POLICY_TYPE_REFERRAL);
				policy.setActive(true);
			}
			request.getPortletSession().setAttribute("policy", policy);
		}
		model.addAttribute("policy", policy);
		// }

		model.addAttribute("ruleTypeList", authorizationService.getRulesTypeList(token));

		model.addAttribute("hasSubRealm", realmService.getRealmChildrenSet(token, realm, "*").size() > 0 ? true : false);
		model.addAttribute("hesPeerRealm", realmService.getRealmSiblingsSet(token, realm, "*").size() > 0 ? true
				: false);

		model.addAttribute("policyName", policyName);

		model.addAttribute("actionValue", action);

		model.addAttribute("checkAllRule", checkAllRule);
		model.addAttribute("checkAllReferral", checkAllReferral);

		model.addAttribute("message", message);
		model.addAttribute("error", error);

		return "authorization/policy/referral/edit";

	}

	/**
	 * Delete {@link Rule} for current {@link Policy}.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteRuleList
	 *            - list of {@link Rule} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteRules")
	public void doDeleteRules(Policy policy, String policyName, String[] deleteRuleList, ActionRequest request,
			ActionResponse response) {

		Policy policySession = getPolicyFromSession(policy, request);
		if (policySession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteRuleList != null) {
				for (int i = 0; i < deleteRuleList.length; i++) {
					policySession.getRules().remove(Rule.getRuleByName(deleteRuleList[i], policySession.getRules()));
				}
			}
			request.getPortletSession().setAttribute("policy", policySession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link Rule} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param ruleName
	 *            - {@link Rule} name
	 * @param ruleType
	 *            - {@link Rule} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addRules")
	public void doAddRules(Policy policy, String policyName, String policyType, String ruleName, String ruleType,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);

		boolean ruleNameExist = false;
		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			for (Iterator<Rule> iterator = policyFromSession.getRules().iterator(); iterator.hasNext();) {
				Rule rule = iterator.next();
				if (rule.getName().equals(ruleName)) {
					ruleNameExist = true;
					break;
				}
			}

			if (ruleNameExist) {
				response.setRenderParameter("error", "Rules name exist.");
				return;
			}

			response.setRenderParameter("ruleName", ruleName);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", "Rules" + ruleType + "Controller");
		}
	}

	/**
	 * Set value for "checkAllRule".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllRule
	 *            - use to decide if all the {@link Rule} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteRule")
	public void doCheckForDeleteRule(String policyName, String checkAllRule, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllRule", checkAllRule);
	}

	/**
	 * Delete {@link Referral} for current {@link Policy}.
	 * 
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteReferralList
	 *            - list of {@link Referral} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            -ActionResponse
	 */
	@ActionMapping(params = "deleteReferral")
	public void doDeleteReferral(Policy policy, String policyName, String[] deleteReferralList, ActionRequest request,
			ActionResponse response) {

		Policy policySession = getPolicyFromSession(policy, request);
		if (policySession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteReferralList != null) {
				for (int i = 0; i < deleteReferralList.length; i++) {
					policySession.getReferrals().remove(
							Referral.getReferralByName(deleteReferralList[i], policySession.getReferrals()));
				}
			}
			request.getPortletSession().setAttribute("policy", policySession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link Referral} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param referralName
	 *            - {@link Referral} name
	 * @param referralType
	 *            - {@link Referral} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addReferral")
	public void doAddReferral(Policy policy, String policyName, String policyType, String referralName,
			String referralType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("referralName", referralName);

		boolean referralNameExist = false;
		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			for (Iterator<Referral> iterator = policyFromSession.getReferrals().iterator(); iterator.hasNext();) {
				Referral referral = iterator.next();
				if (referral.getName().equals(referralName)) {
					referralNameExist = true;
					break;
				}
			}

			if (referralNameExist) {
				response.setRenderParameter("error", "Referral name exist.");
				return;
			}

			response.setRenderParameter("referralType", referralType);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", "ReferralController");
		}
	}

	/**
	 * Set value for "checkAllReferral".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllReferral
	 *            - use to decide if all the {@link Referral} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteReferral")
	public void doCheckForDeleteReferral(String policyName, String checkAllReferral, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllReferral", checkAllReferral);
	}

	/**
	 * Save {@link Policy}.
	 * 
	 * @param policyName
	 *            - old {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param policy
	 *            - {@link Policy} to save
	 * @param result
	 *            - BindingResult
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(String policyName, String policyType, @ModelAttribute("policy") @Valid Policy policy,
			BindingResult result, String action, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policyOnSession = (Policy) request.getPortletSession().getAttribute("policy");
		policyOnSession.setName(policy.getName());
		policyOnSession.setDescription(policy.getDescription());
		policyOnSession.setActive(policy.getActive());
		policyOnSession.setReferralPolicy(true);

		policyOnSession.setLastModifiedDate(new Date());
		policyOnSession.setLastModifiedBy("");
		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			try {
				authorizationService.updatePolicy(token, realm, policyName, policyOnSession);
			} catch (SMSException e) {
				response.setRenderParameter("error", e.getMessage());
			}
			request.getPortletSession().setAttribute("policy", null);
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			policyOnSession.setCreatedBy("");
			policyOnSession.setCreationDate(new Date());
			try {
				authorizationService.addPolicy(token, realm, policyOnSession);
				request.getPortletSession().setAttribute("policy", null);
				response.setRenderParameter("ctx", "AuthorizationController");
			} catch (SMSException e) {
				response.setRenderParameter("error", e.getMessage());
				response.setRenderParameter("ctx", "REFERRALPolicyController");
			}
		}

	}

	/**
	 * Reset add/edit {@link Policy} form.
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String policyName, ActionRequest request, ActionResponse response) {

		request.getPortletSession().setAttribute("policy", null);
		response.setRenderParameter("policyName", policyName);
	}

	/**
	 * Back to policy list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		request.getPortletSession().setAttribute("policy", null);
		response.setRenderParameter("ctx", "AuthorizationController");
	}

}
