/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.AuthenticatedUsersSubject;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.Subject;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * AuthenticatedUsersSubjectController shows the add/edit
 * {@link AuthenticatedUsersSubject}form and does request processing of the
 * edit/add {@link AuthenticatedUsersSubject} action.
 */
@Controller("AuthenticatedUsersSubjectController")
@RequestMapping(value = "view", params = "ctx=AuthenticatedUsersSubjectController")
public class AuthenticatedUsersSubjectController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticatedUsersSubjectController.class);

	/**
	 * Shows add/edit {@link AuthenticatedUsersSubject} form.
	 * 
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editSubject(String subjectName, String policyName, String policyType, String action, Model model,
			RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "subject")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			AuthenticatedUsersSubject subject = (AuthenticatedUsersSubject) Subject.getSubjectByNameAndType(
					subjectName, AuthorizationConstants.SUBJECT_TYPE_AUTHENTICATED_USERS, policy.getSubjects());
			if (subject == null) {
				subject = new AuthenticatedUsersSubject(subjectName);
			}
			model.addAttribute("subject", subject);

		}
		model.addAttribute("subjectName", subjectName);
		model.addAttribute("actionValue", action);

		return "authorization/subject/authenticatedUser/edit";

	}

	/**
	 * Save {@link AuthenticatedUsersSubject} for current {@link Policy}.
	 * 
	 * @param subject
	 *            - current {@link Subject}
	 * @param result
	 *            - BindingResult
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveSubject(@ModelAttribute("subject") @Valid AuthenticatedUsersSubject subject,
			BindingResult result, String subjectName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);
		response.setRenderParameter("action", action);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Subject subjectOld = Subject.getSubjectByNameAndType(subjectName,
				AuthorizationConstants.SUBJECT_TYPE_AUTHENTICATED_USERS, policy.getSubjects());
		policy.getSubjects().remove(subjectOld);
		policy.getSubjects().add(subject);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link AuthenticatedUsersSubject} form.
	 * 
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doResetSubject(String subjectName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("subjectName", subjectName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
