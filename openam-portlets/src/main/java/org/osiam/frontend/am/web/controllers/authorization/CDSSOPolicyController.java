/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.Date;
import java.util.Iterator;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResponseProvider;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.domain.authorization.Subject;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.service.CommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;
import com.sun.identity.sm.SMSException;

/**
 * CDSSOPolicyController shows the edit/add "CDSSO Policy" form and does request
 * processing of the edit/add policy action.
 * 
 */
@Controller("CDSSOPolicyController")
@RequestMapping(value = "view", params = "ctx=CDSSOPolicyController")
public class CDSSOPolicyController extends AuthorizationBaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CDSSOPolicyController.class);

	@Autowired
	private AuthorizationService authorizationService;

	/**
	 * Shows add/edit {@link Policy} form.
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param checkAllRule
	 *            - use to decide if all the {@link Rule} are checked for
	 *            deletion
	 * @param checkAllSubject
	 *            - use to decide if all the {@link Subject} are checked for
	 *            deletion
	 * @param checkAllCondition
	 *            - use to decide if all the {@link Condition} are checked for
	 *            deletion
	 * @param checkAllResponseProvider
	 *            - use to decide if all the {@link ResponseProvider} are
	 *            checked for deletion
	 * @param error
	 *            - error message to display
	 * @param model
	 *            - Model
	 * @param message
	 *            - message to display when policy is modified
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editPolicy(String policyName, String action, String checkAllRule, String checkAllSubject,
			String checkAllCondition, String checkAllResponseProvider, String error, Model model, String message,
			RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		// if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX +
		// "policy")) {
		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");
		if (policy == null) {
			if (CommonConstants.ACTION_UPDATE.equals(action)) {
				policy = authorizationService.getPolicy(token, realm, policyName);
			} else {
				policy = new Policy(policyName, AuthorizationConstants.POLICY_TYPE_CDSSO);
				policy.setActive(true);
			}
			request.getPortletSession().setAttribute("policy", policy);
		}
		model.addAttribute("policy", policy);
		// }

		request.getPortletSession().setAttribute("selectedValuesList", null);
		request.getPortletSession().setAttribute("sessionPropertyListNew", null);

		model.addAttribute("ruleTypeList", authorizationService.getRulesTypeList(token));
		model.addAttribute("subjectTypeList", authorizationService.getSubjectTypeList(token, realm));
		model.addAttribute("conditionTypeList", authorizationService.getConditionTypeList());
		model.addAttribute("responsProviderTypeList", authorizationService.getResponsProviderTypeList());

		model.addAttribute("policyName", policyName);
		model.addAttribute("actionValue", action);

		model.addAttribute("checkAllRule", checkAllRule);
		model.addAttribute("checkAllSubject", checkAllSubject);
		model.addAttribute("checkAllCondition", checkAllCondition);
		model.addAttribute("checkAllResponseProvider", checkAllResponseProvider);

		model.addAttribute("message", message);
		model.addAttribute("error", error);

		return "authorization/policy/cdsso/edit";
	}

	/**
	 * Delete {@link Rule} for current {@link Policy}.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteRuleList
	 *            - list of {@link Rule} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteRules")
	public void doDeleteRules(Policy policy, String policyName, String[] deleteRuleList, ActionRequest request,
			ActionResponse response) {

		Policy policySession = getPolicyFromSession(policy, request);
		if (policySession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteRuleList != null) {
				for (int i = 0; i < deleteRuleList.length; i++) {
					policySession.getRules().remove(Rule.getRuleByName(deleteRuleList[i], policySession.getRules()));
				}
			}
			request.getPortletSession().setAttribute("policy", policySession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link Rule} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param ruleName
	 *            - {@link Rule} name
	 * @param ruleType
	 *            - {@link Rule} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addRules")
	public void doAddRules(Policy policy, String policyName, String policyType, String ruleName, String ruleType,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);

		boolean ruleNameExist = false;
		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			for (Iterator<Rule> iterator = policyFromSession.getRules().iterator(); iterator.hasNext();) {
				Rule rule = iterator.next();
				if (rule.getName().equals(ruleName)) {
					ruleNameExist = true;
					break;
				}
			}

			if (ruleNameExist) {
				response.setRenderParameter("error", "Rules name exist.");
				return;
			}

			response.setRenderParameter("ruleName", ruleName);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", "Rules" + ruleType + "Controller");
		}
	}

	/**
	 * Set value for "checkAllRule".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllRule
	 *            - use to decide if all the {@link Rule} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteRule")
	public void doCheckForDeleteRule(String policyName, String checkAllRule, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllRule", checkAllRule);
	}

	/**
	 * Delete {@link Subject} for current {@link Policy}.
	 * 
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteSubjectList
	 *            - list of {@link Subject} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSubject")
	public void doDeleteSubject(Policy policy, String policyName, String[] deleteSubjectList, ActionRequest request,
			ActionResponse response) {

		Policy policySession = getPolicyFromSession(policy, request);
		if (policySession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteSubjectList != null) {
				for (int i = 0; i < deleteSubjectList.length; i++) {
					policySession.getSubjects().remove(
							Subject.getSubjectByName(deleteSubjectList[i], policySession.getSubjects()));
				}
			}
			request.getPortletSession().setAttribute("policy", policySession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link Subject} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param subjectName
	 *            - {@link Subject} name
	 * @param subjectType
	 *            - {@link Subject} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSubject")
	public void doAddSubject(Policy policy, String policyName, String policyType, String subjectName,
			String subjectType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);

		boolean subjectNameExist = false;
		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			for (Iterator<Subject> iterator = policyFromSession.getSubjects().iterator(); iterator.hasNext();) {
				Subject subject = iterator.next();
				if (subject.getName().equals(subjectName)) {
					subjectNameExist = true;
					break;
				}
			}

			if (subjectNameExist) {
				response.setRenderParameter("error", "Subject name exist.");
				return;
			}

			response.setRenderParameter("subjectName", subjectName);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", subjectType + "SubjectController");
		}
	}

	/**
	 * Set value for "checkAllSubject".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllSubject
	 *            - use to decide if all the {@link Subject} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteSubject")
	public void doCheckForDeleteSubject(String policyName, String checkAllSubject, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllSubject", checkAllSubject);
	}

	/**
	 * Delete {@link Condition} for current {@link Policy}.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteConditionList
	 *            - list of {@link Condition} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteCondition")
	public void doDeleteCondition(Policy policy, String policyName, String[] deleteConditionList,
			ActionRequest request, ActionResponse response) {

		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteConditionList != null) {
				for (int i = 0; i < deleteConditionList.length; i++) {
					policyFromSession.getConditions().remove(
							Condition.getConditionsByName(deleteConditionList[i], policyFromSession.getConditions()));
				}
			}
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link Condition} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param conditionType
	 *            - {@link Condition} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addCondition")
	public void doAddCondition(Policy policy, String policyName, String policyType, String conditionName,
			String conditionType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);

		boolean conditionNameExist = false;

		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			for (Iterator<Condition> iterator = policyFromSession.getConditions().iterator(); iterator.hasNext();) {
				Condition condition = iterator.next();
				if (condition.getName().equals(conditionName)) {
					conditionNameExist = true;
					break;
				}
			}

			if (conditionNameExist) {
				response.setRenderParameter("error", "Condition name exist.");
				return;
			}

			response.setRenderParameter("conditionName", conditionName);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", conditionType + "Controller");
		}
	}

	/**
	 * Set value for "checkAllCondition".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllCondition
	 *            - use to decide if all the {@link Condition} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteCondition")
	public void doCheckForDeleteCondition(String policyName, String checkAllCondition, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllCondition", checkAllCondition);
	}

	/**
	 * Delete {@link ResponseProvider} for current {@link Policy}.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param deleteResponseProviderList
	 *            - list of {@link ResponseProvider} names
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteResponseProvider")
	public void doDeleteResponseProvider(Policy policy, String policyName, String[] deleteResponseProviderList,
			ActionRequest request, ActionResponse response) {

		Policy policySession = getPolicyFromSession(policy, request);
		if (policySession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {

			if (deleteResponseProviderList != null) {
				for (int i = 0; i < deleteResponseProviderList.length; i++) {
					policySession.getResponseProviders().remove(
							ResponseProvider.getResponseProviderByName(deleteResponseProviderList[i],
									policySession.getResponseProviders()));
				}
			}
			request.getPortletSession().setAttribute("policy", policySession);
			response.setRenderParameter("message", "message.policy_modified");
			response.setRenderParameter("policyName", policyName);
		}
	}

	/**
	 * Redirect to add {@link ResponseProvider} form.
	 * 
	 * @param policy
	 *            - current {@link Policy}
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param responseProviderName
	 *            {@link ResponseProvider} name
	 * @param responseProviderType
	 *            {@link ResponseProvider} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addResponseProvider")
	public void doAddResponseProvider(Policy policy, String policyName, String policyType, String responseProviderName,
			String responseProviderType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);

		boolean responseProviderNameExist = false;
		Policy policyFromSession = getPolicyFromSession(policy, request);
		if (policyFromSession == null) {
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			for (Iterator<ResponseProvider> iterator = policyFromSession.getResponseProviders().iterator(); iterator
					.hasNext();) {
				ResponseProvider responseProvider = iterator.next();
				if (responseProvider.getName().equals(responseProviderName)) {
					responseProviderNameExist = true;
					break;
				}
			}

			if (responseProviderNameExist) {
				response.setRenderParameter("error", "ResponseProvider name exist.");
				return;
			}

			response.setRenderParameter("responseProviderName", responseProviderName);
			request.getPortletSession().setAttribute("policy", policyFromSession);
			response.setRenderParameter("ctx", responseProviderType + "Controller");
		}
	}

	/**
	 * Set value for "checkAllResponseProvider".
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param checkAllResponseProvider
	 *            - use to decide if all the {@link ResponseProvider} are
	 *            checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDeleteResponseProvider")
	public void doCheckForDeleteResponseProvider(String policyName, String checkAllResponseProvider,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("checkAllResponseProvider", checkAllResponseProvider);
	}

	/**
	 * Save {@link Policy}.
	 * 
	 * @param policyName
	 *            - old {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param policy
	 *            - {@link Policy} to save
	 * @param result
	 *            - BindingResult
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(String policyName, String policyType, @ModelAttribute("policy") @Valid Policy policy,
			BindingResult result, String action, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policyOnSession = (Policy) request.getPortletSession().getAttribute("policy");
		policyOnSession.setName(policy.getName());
		policyOnSession.setDescription(policy.getDescription());
		policyOnSession.setActive(policy.getActive());
		policyOnSession.setReferralPolicy(false);

		policyOnSession.setLastModifiedDate(new Date());
		if (CommonConstants.ACTION_UPDATE.equals(action)) {
			try {
				authorizationService.updatePolicy(token, realm, policyName, policyOnSession);
			} catch (SMSException e) {
				response.setRenderParameter("error", e.getMessage());
			}
			request.getPortletSession().setAttribute("policy", null);
			response.setRenderParameter("ctx", "AuthorizationController");
		} else {
			policyOnSession.setCreationDate(new Date());
			try {
				authorizationService.addPolicy(token, realm, policyOnSession);
				request.getPortletSession().setAttribute("policy", null);
				response.setRenderParameter("ctx", "AuthorizationController");
			} catch (SMSException e) {
				response.setRenderParameter("error", e.getMessage());
				response.setRenderParameter("ctx", "CDSSOPolicyController");
			}
			
		}

		
	}

	/**
	 * Reset add/edit {@link Policy} form.
	 * 
	 * @param policyName
	 *            - {@link Policy} name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String policyName, ActionRequest request, ActionResponse response) {

		request.getPortletSession().setAttribute("policy", null);
		response.setRenderParameter("policyName", policyName);
	}

	/**
	 * Back to policy list.
	 * 
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(ActionRequest request, ActionResponse response) {

		request.getPortletSession().setAttribute("policy", null);
		response.setRenderParameter("ctx", "AuthorizationController");
	}
}
