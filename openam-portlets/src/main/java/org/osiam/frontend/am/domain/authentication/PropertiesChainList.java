/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authentication;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.Factory;
import org.apache.commons.collections.list.LazyList;

/**
 * Encapsulates the data required for a chain list.
 */
public class PropertiesChainList {
	@SuppressWarnings("unchecked")
	private final List<ChainProperty> listValues = LazyList.decorate(new ArrayList<ChainProperty>(), new Factory() {
		@Override
		public Object create() {
			ChainProperty pc = new ChainProperty();
			return pc;
		}
	});

	public List<ChainProperty> getListValues() {
		return listValues;
	}

}
