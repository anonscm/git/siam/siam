/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.domain.authorization.URLPolicyAgentRule;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * RulesiPlanetAMWebAgentServiceController shows the add/edit
 * {@link URLPolicyAgentRule} form and does request processing of the edit/add
 * {@link URLPolicyAgentRule} action.
 * 
 */
@Controller("RulesiPlanetAMWebAgentServiceController")
@RequestMapping(value = "view", params = { "ctx=RulesiPlanetAMWebAgentServiceController" })
public class RulesiPlanetAMWebAgentServiceController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RulesiPlanetAMWebAgentServiceController.class);

	@Autowired
	@Qualifier("urlPolicyAgentRuleValidatorimplements")
	private Validator urlPolicyAgentRuleValidatorimplements;

	/**
	 * Shows add/edit {@link URLPolicyAgentRule} form.
	 * 
	 * @param ruleName
	 *            - {@link Rule} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editRule(String ruleName, String policyName, String policyType, String action, Model model,
			RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "rule")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			URLPolicyAgentRule rule = (URLPolicyAgentRule) Rule.getRuleByNameAndType(ruleName,
					AuthorizationConstants.RULE_TYPE_IPLANET_AM_WEB_AGENT_SERVICE, policy.getRules());
			if (rule == null) {
				rule = new URLPolicyAgentRule(ruleName);
			}
			model.addAttribute("rule", rule);

		}
		model.addAttribute("ruleName", ruleName);
		model.addAttribute("actionValue", action);

		return "authorization/rule/URLPolicyAgent/edit";

	}

	/**
	 * Save {@link URLPolicyAgentRule} for current {@link Policy}.
	 * 
	 * @param rule
	 *            - {@link Rule} to save
	 * @param result
	 *            - BindingResult
	 * @param ruleName
	 *            - {@link Rule} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSaveRule(@ModelAttribute("rule") @Valid URLPolicyAgentRule rule, BindingResult result,
			String ruleName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("ruleName", ruleName);
		response.setRenderParameter("action", action);

		if (AuthorizationConstants.POLICY_TYPE_CDSSO.equals(policyType)) {
			urlPolicyAgentRuleValidatorimplements.validate(rule, result);
		}

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Rule ruleOld = Rule.getRuleByNameAndType(ruleName,
				AuthorizationConstants.RULE_TYPE_IPLANET_AM_WEB_AGENT_SERVICE, policy.getRules());
		policy.getRules().remove(ruleOld);
		policy.getRules().add(rule);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link URLPolicyAgentRule} form.
	 * 
	 * @param ruleName
	 *            - {@link Rule} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String ruleName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("ruleName", ruleName);
	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
