/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Authentication By Module Chain
 * Condition.
 */
public class AuthenticateToServiceCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(AuthenticateToServiceCondition.class);

	/**
	 * The type string constant for this type of response provider.
	 */
	public static final String TYPE = "AuthenticateToServiceCondition";

	private static final String AUTHENTICATE_TO_SERVICE = "AuthenticateToService";

	private static final long serialVersionUID = 1L;

	@NotNull
	@Size(min = 1)
	private String realm;
	@NotNull
	@Size(min = 1)
	private String chain;

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public AuthenticateToServiceCondition(final String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public AuthenticateToServiceCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public AuthenticateToServiceCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(AUTHENTICATE_TO_SERVICE)) {
				String v = attributeValuePair.getValueList().get(0);
				String[] comps = v.split(":");
				if (comps.length == 2) {
					realm = comps[0];
					chain = comps[1];
				} else if (comps.length == 1) {
					realm = null;
					chain = comps[0];
				}
			}

		}

	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getChain() {
		return chain;
	}

	public void setChain(String chain) {
		this.chain = chain;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		String realmToAppend = "";
		if (realm != null) {
			realmToAppend = realm + ":";
		}
		XmlUtil.appendAttributeValuePairToString(buffer, AUTHENTICATE_TO_SERVICE, realmToAppend + chain);
	}

}
