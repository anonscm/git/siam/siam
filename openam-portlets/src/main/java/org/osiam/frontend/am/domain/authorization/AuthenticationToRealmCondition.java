/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for the authentication to realm condition.
 */
public class AuthenticationToRealmCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(LEAuthLevelCondition.class);
	/**
	 * The type string code for this kind of condition.
	 */
	public static final String TYPE = "AuthenticateToRealmCondition";

	/**
	 * This constant is used as a key when persisting this object to XML.
	 */
	public static final String AUTHENTICATE_TO_REALM = "AuthenticateToRealm";

	private static final long serialVersionUID = 1L;

	private String realm;

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the condition name.
	 */
	public AuthenticationToRealmCondition(final String name) {
		super(name, TYPE);

	}

	/**
	 * Creates a new condition with the given name and given realm.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param realm
	 *            - the realm.
	 * 
	 */
	public AuthenticationToRealmCondition(final String name, String realm) {
		super(name, TYPE);
		setRealm(realm);

	}

	/**
	 * Creates a new condition.
	 */
	public AuthenticationToRealmCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param node
	 *            - the XML node to parse.
	 */

	public AuthenticationToRealmCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(AUTHENTICATE_TO_REALM)) {
				realm = attributeValuePair.getValueList().get(0);
			}

		}

	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuePairToString(buffer, AUTHENTICATE_TO_REALM, realm);

	}

}
