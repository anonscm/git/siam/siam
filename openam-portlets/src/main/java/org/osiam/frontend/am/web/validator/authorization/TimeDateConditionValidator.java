/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.validator.authorization;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.domain.authorization.TimeDateCondition;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Custom validation for the TimeDateCondition object.
 * <p>
 * Validate if at least one of the properties Start Date, Start Time, Start Day
 * are define with non null value.<br>
 * Validate if the custom time zone is valid.
 * </p>
 * 
 */
@Component("timeDateConditionValidator")
public class TimeDateConditionValidator implements Validator {
	
	private static final Logger LOGGER = Logger.getLogger(TimeDateConditionValidator.class);

	@Override
	public boolean supports(Class<?> klass) {
		return TimeDateCondition.class.isAssignableFrom(klass);
	}

	@Override
	public void validate(Object target, Errors errors) {
		TimeDateCondition condition = (TimeDateCondition) target;

		boolean dataNull = (condition.getStartDate() == null || "".equals(condition.getStartDate())) ? true : false;

		boolean timeNull = (condition.getStartHour() == null || condition.getStartMinute() == null) ? true : false;

		boolean dayNull = (condition.getStartDay() == null || "".equals(condition.getStartDay())) ? true : false;

		if (dataNull && timeNull && dayNull) {
			errors.rejectValue("name", "AtLeastOne.DataTimeDay");
		}

		if ("custom".equals(condition.getTimezoneType())) {
			if (isValidTimezone(condition.getCustomTimezoneValue())) {
				condition.setTimezone(condition.getCustomTimezoneValue());
			} else {
				errors.rejectValue("timezone", "Invalid");
			}
		}

		if (condition.getStartDate() != null && !"".equals(condition.getStartDate()) && condition.getEndDate() != null
				&& !"".equals(condition.getEndDate())) {

			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");
			try {
				Date start = formatter.parse(condition.getStartDate());
				Date end = formatter.parse(condition.getEndDate());

				if (start.after(end)) {
					errors.rejectValue("startDate", "StartLargerEnd");
				}
			} catch (Exception e) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Invalid date");
				}
			}
		}

	}

	/**
	 * Verify if given time zone is valid.
	 * 
	 * @param timezone
	 *            - time zone value
	 * @return true if is a valid time zone
	 */
	private boolean isValidTimezone(String tz) {
		
		boolean valid = false;

        if ((tz == null) || (tz.trim().length() == 0)) {
            valid = true;
        } else {
            TimeZone t = TimeZone.getTimeZone(tz);
            String id = t.getID();
            
            // Validate like in opensso
            valid = id.equals(tz) ||
                id.equals(TimeZone.getTimeZone("GMT+10:00").getID());
        }
        return valid;
		
	}
}
