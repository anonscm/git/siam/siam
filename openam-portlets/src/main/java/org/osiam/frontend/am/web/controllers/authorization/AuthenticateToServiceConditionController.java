/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.AuthenticateToServiceCondition;
import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * AuthenticateToServiceConditionController shows the add/edit
 * {@link AuthenticateToServiceCondition} form and does request processing of
 * the edit/add {@link AuthenticateToServiceCondition} action.
 * 
 */
@Controller("AuthenticateToServiceConditionController")
@RequestMapping(value = "view", params = { "ctx=AuthenticateToServiceConditionController" })
public class AuthenticateToServiceConditionController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticateToServiceConditionController.class);

	@Autowired
	private AuthenticationService authenticationService;

	/**
	 * Shows add/edit {@link AuthenticateToServiceCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editCondition(String conditionName, String policyName, String policyType, String action, Model model,
			RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		String realmForChain = realm;

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "condition")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			AuthenticateToServiceCondition condition = (AuthenticateToServiceCondition) Condition
					.getConditionsByNameAndType(conditionName,
							AuthorizationConstants.CONDITION_TYPE_AUTHENTICATE_TO_SERVICE_CONDITION,
							policy.getConditions());
			if (condition == null) {
				condition = new AuthenticateToServiceCondition(conditionName);
				condition.setRealm(realm);
			} else {
				realmForChain = condition.getRealm();
			}
			model.addAttribute("condition", condition);

		}

		model.addAttribute("chainList", authenticationService.getChainNameSet(token, realmForChain));

		model.addAttribute("actionValue", action);
		model.addAttribute("conditionName", conditionName);

		return "authorization/condition/authenticateToServiceCondition/edit";

	}

	/**
	 * Save {@link AuthenticateToServiceCondition} for current {@link Policy}.
	 * 
	 * @param condition
	 *            - {@link Condition} to save
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("condition") @Valid AuthenticateToServiceCondition condition,
			BindingResult result, String conditionName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Condition conditionOld = Condition.getConditionsByNameAndType(conditionName,
				AuthorizationConstants.CONDITION_TYPE_AUTHENTICATE_TO_SERVICE_CONDITION, policy.getConditions());
		policy.getConditions().remove(conditionOld);
		policy.getConditions().add(condition);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link AuthenticateToServiceCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String conditionName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
