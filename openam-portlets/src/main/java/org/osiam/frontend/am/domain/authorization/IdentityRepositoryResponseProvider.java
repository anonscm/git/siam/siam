/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for an identity repository response
 * provider.
 * 
 */
public class IdentityRepositoryResponseProvider extends ResponseProvider {

	private static final long serialVersionUID = 1L;

	/**
	 * The type string constant for this type of response provider.
	 */
	public static final String TYPE = "IDRepoResponseProvider";

	private static final String STATIC_ATTRIBUTE = "StaticAttribute";
	private static final String DYNAMIC_ATTRIBUTE = "DynamicAttribute";

	private List<String> staticAttributeList = new ArrayList<String>();

	private List<String> dynamicAttributeList = new ArrayList<String>();

	private static final Logger LOGGER = Logger.getLogger(IdentityRepositoryResponseProvider.class);

	/**
	 * Creates a new response provider with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public IdentityRepositoryResponseProvider(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new response provider.
	 */
	public IdentityRepositoryResponseProvider() {

	}

	/**
	 * Creates a new response provider with the given name by parsing the given
	 * XML node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public IdentityRepositoryResponseProvider(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		staticAttributeList = new ArrayList<String>();
		dynamicAttributeList = new ArrayList<String>();

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(STATIC_ATTRIBUTE)) {
				List<String> values = attributeValuePair.getValueList();
				staticAttributeList.addAll(values);
			} else if (attributeValuePair.getName().equals(DYNAMIC_ATTRIBUTE)) {
				List<String> values = attributeValuePair.getValueList();
				dynamicAttributeList.addAll(values);

			}

		}

	}

	/**
	 * @return list of static attributes.
	 */
	public List<String> getStaticAttributeList() {
		return staticAttributeList;
	}

	public void setStaticAttributeList(List<String> staticAttributeList) {
		this.staticAttributeList = staticAttributeList;
	}

	/**
	 * @return list of dynamic attributes.
	 */
	public List<String> getDynamicAttributeList() {
		return dynamicAttributeList;
	}

	public void setDynamicAttributeList(List<String> dynamicAttributeList) {
		this.dynamicAttributeList = dynamicAttributeList;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuesPairToString(buffer, STATIC_ATTRIBUTE, staticAttributeList);
		XmlUtil.appendAttributeValuesPairToString(buffer, DYNAMIC_ATTRIBUTE, dynamicAttributeList);
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
