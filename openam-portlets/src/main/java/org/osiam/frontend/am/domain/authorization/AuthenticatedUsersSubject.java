/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

import com.iplanet.sso.SSOToken;

/**
 * This class encapsulates the data for an authentication users subject.
 */
public class AuthenticatedUsersSubject extends Subject {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * The type string code for this kind of subject.
	 */
	public static final String TYPE = "AuthenticatedUsers";

	private static final Logger LOGGER = Logger.getLogger(AuthenticatedUsersSubject.class);

	/**
	 * Creates a new subject with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public AuthenticatedUsersSubject(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new subject.
	 */
	public AuthenticatedUsersSubject() {

	}

	/**
	 * Creates a new subject with the given name by parsing the given XML node.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param node
	 *            - the XML node to parse.
	 * @param token
	 *            - not used
	 */
	public AuthenticatedUsersSubject(SSOToken token, String name, Node node) {
		super(name, TYPE);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("AuthenticatedUsersSubject created with name %s from XML.", name));
			LOGGER.debug("XML :");
			LOGGER.debug(XmlUtil.getString(node));
		}
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {

	}

	@Override
	public String getType() {
		return TYPE;
	}

}
