/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Session Properties Condition.
 */
public class SessionPropertiesCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(LEAuthLevelCondition.class);
	/**
	 * The type string constant for this type of condition.
	 */
	public static final String TYPE = "SessionPropertyCondition";
	private static final String VALUE_CASE_INSENSITIVE = "valueCaseInsensitive";

	private static final long serialVersionUID = 1L;

	/**
	 * If the property is true the system will ignore the case when will
	 * evaluate the session properties.
	 */
	private Boolean ignoreCaseofValues = Boolean.FALSE;

	/**
	 * Session property list.
	 */
	private List<SessionProperty> sessionPropertyList = new ArrayList<SessionProperty>();

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public SessionPropertiesCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public SessionPropertiesCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public SessionPropertiesCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		sessionPropertyList = new ArrayList<SessionProperty>();

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(VALUE_CASE_INSENSITIVE)) {
				String s = attributeValuePair.getValueList().get(0);
				ignoreCaseofValues = Boolean.valueOf(s);

			} else {
				SessionProperty sessionProperty = new SessionProperty();
				sessionProperty.setName(attributeValuePair.getName());
				sessionProperty.setValueList(attributeValuePair.getValueList());
				sessionPropertyList.add(sessionProperty);

			}

		}

	}

	public Boolean getIgnoreCaseofValues() {
		return ignoreCaseofValues;
	}

	public void setIgnoreCaseofValues(Boolean ignoreCaseofValues) {
		this.ignoreCaseofValues = ignoreCaseofValues;
	}

	public List<SessionProperty> getSessionPropertyList() {
		return sessionPropertyList;
	}

	public void setSessionPropertyList(List<SessionProperty> sessionPropertyList) {
		this.sessionPropertyList = sessionPropertyList;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuePairToString(buffer, VALUE_CASE_INSENSITIVE, ignoreCaseofValues.toString());
		for (int i = 0; i < sessionPropertyList.size(); i++) {
			SessionProperty sessionProperty = sessionPropertyList.get(i);
			XmlUtil.appendAttributeValuesPairToString(buffer, sessionProperty.getName(), sessionProperty.getValueList());
		}
	}

}
