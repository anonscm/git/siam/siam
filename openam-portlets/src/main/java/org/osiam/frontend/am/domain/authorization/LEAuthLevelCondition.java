/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Less or Equal Authentication Level
 * Condition.
 */
public class LEAuthLevelCondition extends Condition {

	private static final Logger LOGGER = Logger.getLogger(LEAuthLevelCondition.class);

	/**
	 * The type string constant for this type of condition.
	 */
	public static final String TYPE = "LEAuthLevelCondition";

	private static final String AUTH_LEVEL = "AuthLevel";

	private static final long serialVersionUID = 1L;
	@NotNull
	@Size(min = 1)
	private String realm;
	@NotNull
	private Integer level;

	/**
	 * Creates a new condition with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public LEAuthLevelCondition(final String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public LEAuthLevelCondition() {

	}

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public LEAuthLevelCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(AUTH_LEVEL)) {
				String v = attributeValuePair.getValueList().get(0);
				String[] comps = v.split(":");
				if (comps.length == 2) {
					realm = comps[0];
					level = Integer.valueOf(comps[1]);
				} else if (comps.length == 1) {
					realm = null;
					level = Integer.valueOf(comps[0]);
				}
			}

		}

	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuePairToString(buffer, AUTH_LEVEL, realm + ":" + level);
	}

}
