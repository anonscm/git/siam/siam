/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.reflect.ConstructorUtils;
import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * 
 * This class encapsulates the data for a response provider.
 * 
 */
public abstract class ResponseProvider extends BaseEntity {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;
	private String exclusive;

	private static final Logger LOGGER = Logger.getLogger(ResponseProvider.class);

	private static final Map<String, Class<? extends ResponseProvider>> CLAZZ_MAP = new HashMap<String, Class<? extends ResponseProvider>>();

	static {
		CLAZZ_MAP.put(IdentityRepositoryResponseProvider.TYPE, IdentityRepositoryResponseProvider.class);
	}

	/**
	 * @return response provider created based on the given XML node.
	 * 
	 *         Creates a {@link ResponseProvider} from an XML {@link Node}.
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static ResponseProvider fromNode(Node node) throws AuthorizationException {

		String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
		String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Read ResponseProvider with name %s and type %s from xml node.", name, type));
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}

		Class<? extends ResponseProvider> subjectClazz = CLAZZ_MAP.get(type);

		if (subjectClazz == null) {
			String error = String.format("Invalid type %s for %s  . Xml : %s", type, name, XmlUtil.getString(node));
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}

		ResponseProvider responseProvider = null;
		try {
			responseProvider = (ResponseProvider) ConstructorUtils.invokeConstructor(subjectClazz, new Object[] { name,
					node });
		} catch (NoSuchMethodException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (IllegalAccessException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (InvocationTargetException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (InstantiationException e) {
			throw new AuthorizationException("Error creating instance.", e);
		}

		return responseProvider;

	}

	/**
	 * Creates a response provider with the the given name and the given type.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param type
	 *            - the condition type.
	 */
	public ResponseProvider(String name, String type) {
		super(name, type);
	}

	/**
	 * Creates a response provider.
	 */
	public ResponseProvider() {

	}

	public String getExclusive() {
		return exclusive;
	}

	public void setExclusive(String exclusive) {
		this.exclusive = exclusive;
	}

	/**
	 * 
	 * Appends XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the string buffer to append to.
	 */
	public void appendXML(StringBuilder buffer) {
		buffer.append("<ResponseProvider ");
		buffer.append("name=\"");
		buffer.append(getName());
		buffer.append("\"");
		buffer.append(" type=\"");
		buffer.append(getType());
		buffer.append("\"");
		buffer.append(">");
		appendInnerXML(buffer);
		buffer.append("</ResponseProvider>");
	}

	/**
	 * 
	 * Append XML string containing object values.
	 * 
	 * @param buffer
	 *            - the string buffer to append to.
	 */
	public abstract void appendInnerXML(StringBuilder buffer);

	/**
	 * 
	 * @return type of response provider.
	 */
	@Override
	public abstract String getType();

	/**
	 * Get {@link ResponseProvider} by name from given list.
	 * 
	 * @param name
	 *            - {@link ResponseProvider} name
	 * @param list
	 *            - {@link List} of responsProviders
	 * @return - {@link ResponseProvider}
	 */
	public static ResponseProvider getResponseProviderByName(String name, List<ResponseProvider> list) {
		for (Iterator<ResponseProvider> iterator = list.iterator(); iterator.hasNext();) {
			ResponseProvider responseProvider = (ResponseProvider) iterator.next();
			if (responseProvider.getName().equals(name)) {
				return responseProvider;
			}
		}
		return null;
	}

}
