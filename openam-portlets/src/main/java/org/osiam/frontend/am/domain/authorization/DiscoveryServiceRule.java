/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * The DiscoveryServiceRule class encapsulates the data for a discovery service
 * rule .
 */
public class DiscoveryServiceRule extends Rule {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The type string code for this kind of subject.
	 */
	public static final String TYPE = "sunIdentityServerDiscoveryService";

	private static final String LOOKUP = "LOOKUP";
	private static final String UPDATE = "UPDATE";

	private boolean lookup;
	private String lookupValue;
	private boolean update;
	private String updateValue;

	private static final Logger LOGGER = Logger.getLogger(DiscoveryServiceRule.class);

	/**
	 * Creates a new rule with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public DiscoveryServiceRule(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new rule.
	 */
	public DiscoveryServiceRule() {

	}

	/**
	 * Creates a new rule with the given name by parsing the given XML node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public DiscoveryServiceRule(String name, Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Rule XML contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {
			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);
			if (attributeValuePair.getName().equals(LOOKUP)) {
				lookupValue = attributeValuePair.getValueList().get(0);
				lookup = true;
			} else if (attributeValuePair.getName().equals(UPDATE)) {
				updateValue = attributeValuePair.getValueList().get(0);
				update = true;
			}
		}
	}

	public String getLookupValue() {
		return lookupValue;
	}

	public void setLookupValue(String lookupValue) {
		this.lookupValue = lookupValue;
	}

	public String getUpdateValue() {
		return updateValue;
	}

	public void setUpdateValue(String updateValue) {
		this.updateValue = updateValue;
	}

	public boolean getLookup() {
		return lookup;
	}

	public void setLookup(boolean lookup) {
		this.lookup = lookup;
	}

	public boolean getUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		if (lookup) {
			XmlUtil.appendAttributeValuePairToString(buffer, LOOKUP, lookupValue);
		}
		if (update) {
			XmlUtil.appendAttributeValuePairToString(buffer, UPDATE, updateValue);
		}
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
