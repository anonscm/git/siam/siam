/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

/**
 * This class encapsulates the data for a XACML policy.
 * 
 */
public class XacmlPolicy implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String XACML_NAMESPACE = "urn:oasis:names:tc:xacml:2.0:policy:schema:os";

	private static final String POLICYSET_LOCAL_NAME = "PolicySet";

	private String policyName;

	private String policyXml;

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getPolicyXml() {
		return policyXml;
	}

	public void setPolicyXml(String policyXml) {
		this.policyXml = policyXml;
	}

	/**
	 * convert xml-string to policy set.
	 * 
	 * @param policySetString
	 *            - string to convert
	 * @return PolicySetType
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	public static PolicySetType getPolicySetFromString(String policySetString) throws JAXBException {
		JAXBElement<PolicySetType> policySetTypeJaxBElement;
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		policySetTypeJaxBElement = (JAXBElement<PolicySetType>) context.createUnmarshaller().unmarshal(
				new StringReader(policySetString));
		PolicySetType policySetTypeFromXml = policySetTypeJaxBElement.getValue();
		return policySetTypeFromXml;
	}

	/**
	 * convert policy set to string.
	 * 
	 * @param policySetType
	 *            - PolicySetType to concvert
	 * @return String
	 * @throws JAXBException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String getStringFromPolicySet(PolicySetType policySetType) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		StringWriter writer0 = new StringWriter();
		context.createMarshaller().marshal(
				new JAXBElement(new QName(XACML_NAMESPACE, POLICYSET_LOCAL_NAME), PolicySetType.class, policySetType),
				writer0);
		return writer0.toString();
	}

}
