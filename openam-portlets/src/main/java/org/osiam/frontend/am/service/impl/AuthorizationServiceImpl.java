/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResponseProvider;
import org.osiam.frontend.am.domain.authorization.Rule;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.service.impl.BaseSSOService;
import org.osiam.frontend.configuration.exceptions.AgentsException;
import org.osiam.frontend.util.XmlUtil;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.iplanet.services.util.XMLException;
import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.AMIdentity;
import com.sun.identity.idm.AMIdentityRepository;
import com.sun.identity.idm.IdRepoException;
import com.sun.identity.idm.IdSearchControl;
import com.sun.identity.idm.IdSearchResults;
import com.sun.identity.idm.IdType;
import com.sun.identity.idm.IdUtils;
import com.sun.identity.sm.SMSException;
import com.sun.identity.sm.ServiceConfig;
import com.sun.identity.sm.ServiceConfigManager;

/**
 * Authorization service interface. Defines methods for policy management.
 */
@Service("AuthorizationService")
public class AuthorizationServiceImpl extends BaseSSOService implements AuthorizationService {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationServiceImpl.class);

	private static final String NAMED_POLICY = "Policies";
	private static final String POLICY_SERVICE_NAME = "iPlanetAMPolicyService";
	private static final String POLICY_XML = "xmlpolicy";
	private static final String NAMED_POLICY_ID = "NamedPolicy";

	private void validateServiceConfig(ServiceConfig serviceConfig, String realm) {
		if (serviceConfig == null) {
			LOGGER.error(String.format("Realm '%s' is not registered", realm));
			throw new IllegalArgumentException(String.format("Realm '%s' is not registered", realm));
		}
	}

	private String username;

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public List<Policy> getPoliciesList(SSOToken token, String realm) throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		List<Policy> policyList = null;
		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			validateServiceConfig(namedPolicy, realm);

			if (namedPolicy == null) {
				return new ArrayList<Policy>();
			} else {
				@SuppressWarnings("unchecked")
				Set<String> policySet = namedPolicy.getSubConfigNames();
				policyList = new ArrayList<Policy>();
				Iterator<String> iterator = policySet.iterator();
				while (iterator.hasNext()) {
					String policyName = iterator.next();
					String policyType = "";
					Node node = getPolicy(namedPolicy, policyName);
					String referralPolicy = XmlUtil.getNodeAttributeValue(node, AuthorizationConstants.REFERRAL_POLICY);
					if (Boolean.TRUE.toString().equals(referralPolicy)) {
						policyType = AuthorizationConstants.POLICY_TYPE_REFERRAL;
					} else {
						policyType = AuthorizationConstants.POLICY_TYPE_CDSSO;
					}

					Policy policy = new Policy(policyName, policyType);
					policy.setRules(getRulesListForPolicy(token, realm, policyName));
					policyList.add(policy);
				}
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			LOGGER.error("IllegalArgumentException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

		return policyList;
	}

	@Override
	public List<String> getPolicyTypesList() {
		return Arrays.asList(AuthorizationConstants.POLICY_TYPE_XACML, AuthorizationConstants.POLICY_TYPE_CDSSO,
				AuthorizationConstants.POLICY_TYPE_REFERRAL);
	}

	@Override
	public List<Rule> getRulesListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		Set<Node> childNodes = getPolicyChildNodes(token, realm, policyName, AuthorizationConstants.POLICY_RULE_NODE);
		List<Rule> baseEntityList = new ArrayList<Rule>(childNodes.size());
		if (childNodes != null) {
			Iterator<Node> items = childNodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				baseEntityList.add(Rule.fromNode(node));
			}
		}

		return baseEntityList;
	}

	@Override
	public List<String> getRulesTypeList(SSOToken token) throws AuthorizationException {
		return Arrays.asList(AuthorizationConstants.RULE_TYPE_SUN_IDENTITY_SERVER_LIBERTY_PP_SERVICE,
				AuthorizationConstants.RULE_TYPE_IPLANET_AM_WEB_AGENT_SERVICE,
				AuthorizationConstants.RULE_TYPE_SUN_IDENTITY_SERVER_DISCOVERY_SERVICE);
	}

	@Override
	public List<BaseEntity> getSubjectListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		Set<Node> childNodes = getPolicyChildNodes(token, realm, policyName,
				AuthorizationConstants.POLICY_SUBJECTS_NODE);
		Set<Node> subjectNodes = XmlUtil.getChildNodes(childNodes.iterator().next(),
				AuthorizationConstants.POLICY_SUBJECT_NODE);
		List<BaseEntity> baseEntityList = new ArrayList<BaseEntity>(subjectNodes.size());
		if (subjectNodes != null) {
			Iterator<Node> items = subjectNodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
				String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);
				BaseEntity baseEntity = new BaseEntity(name, type);
				baseEntityList.add(baseEntity);
			}
		}
		return baseEntityList;
	}

	@Override
	public List<String> getSubjectTypeList(SSOToken token, String realm) throws AuthorizationException {
		return Arrays.asList(AuthorizationConstants.SUBJECT_TYPE_AM_IDENTITY_SUBJECT,
				AuthorizationConstants.SUBJECT_TYPE_AUTHENTICATED_USERS);
	}

	@Override
	public List<BaseEntity> getConditionListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		Set<Node> childNodes = getPolicyChildNodes(token, realm, policyName,
				AuthorizationConstants.POLICY_CONDITIONS_NODE);
		Set<Node> allNodes = XmlUtil.getChildNodes(childNodes.iterator().next(),
				AuthorizationConstants.POLICY_CONDITION_NODE);
		List<BaseEntity> baseEntityList = new ArrayList<BaseEntity>(allNodes.size());
		if (allNodes != null) {
			Iterator<Node> items = allNodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
				String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);
				BaseEntity baseEntity = new BaseEntity(name, type);
				baseEntityList.add(baseEntity);
			}
		}
		return baseEntityList;
	}

	@Override
	public List<String> getConditionTypeList() {
		// Active Session Time - SessionCondition
		// Authentication by Module Chain - AuthenticateToServiceCondition
		// Authentication by Module Instance - AuthSchemeCondition
		// Authentication Level (>=) - AuthLevelCondition
		// Authentication Level (<=) - LEAuthLevelCondition
		// Authentication to a Realm - AuthenticateToRealmCondition
		// Current Session Properties - SessionPropertyCondition
		// IP-Address/DNS Name - IPCondition
		// Resource/Environment/IP Address - ResourceEnvIPCondition
		// Time (day, date, time, and timezone) - SimpleTimeCondition

		return Arrays.asList(AuthorizationConstants.CONDITION_TYPE_SESSION_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_AUTHENTICATE_TO_SERVICE_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_AUTH_SCHEME_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_AUTH_LEVEL_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_LE_AUTH_LEVEL_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_AUTHENTICATE_TO_REALM_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_SESSION_PROPERTY_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_IP_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_RESOURCE_ENV_IP_CONDITION,
				AuthorizationConstants.CONDITION_TYPE_SIMPLE_TIME_CONDITION);
	}

	@Override
	public List<BaseEntity> getResponseProviderListForPolicy(SSOToken token, String realm, String policyName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		Set<Node> childNodes = getPolicyChildNodes(token, realm, policyName,
				AuthorizationConstants.POLICY_RESPONSE_PROVIDERS_NODE);
		Set<Node> allNodes = XmlUtil.getChildNodes(childNodes.iterator().next(),
				AuthorizationConstants.POLICY_RESPONSE_PROVIDER_NODE);
		List<BaseEntity> baseEntityList = new ArrayList<BaseEntity>(allNodes.size());
		if (allNodes != null) {
			Iterator<Node> items = allNodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
				String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);
				BaseEntity baseEntity = new BaseEntity(name, type);
				baseEntityList.add(baseEntity);
			}
		}
		return baseEntityList;
	}

	@Override
	public List<String> getResponsProviderTypeList() {
		return Arrays.asList("IDRepoResponseProvider");
	}

	@Override
	public Condition getCondition(SSOToken token, String realm, String policyName, String conditionName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (conditionName == null) {
			throw new IllegalArgumentException("Param conditionName cannot be null. ");
		}
		Node node = getPolicyChildNodeByName(token, realm, policyName, AuthorizationConstants.POLICY_CONDITIONS_NODE,
				AuthorizationConstants.POLICY_CONDITION_NODE, conditionName);
		if (node != null) {
			return Condition.fromNode(node);
		}
		return null;

	}

	@Override
	public Rule getRule(SSOToken token, String realm, String policyName, String ruleName) throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (ruleName == null) {
			throw new IllegalArgumentException("Param ruleName cannot be null. ");
		}
		Node node = getPolicyChildNodeByName(token, realm, policyName, AuthorizationConstants.POLICY_RULE_NODE,
				ruleName);
		if (node != null) {
			return Rule.fromNode(node);
		}
		return null;
	}

	@Override
	public org.osiam.frontend.am.domain.authorization.Subject getSubject(SSOToken token, String realm,
			String policyName, String subjectName) throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (subjectName == null) {
			throw new IllegalArgumentException("Param subjectName cannot be null. ");
		}
		Node node = getPolicyChildNodeByName(token, realm, policyName, AuthorizationConstants.POLICY_SUBJECTS_NODE,
				AuthorizationConstants.POLICY_SUBJECT_NODE, subjectName);
		if (node != null) {
			return org.osiam.frontend.am.domain.authorization.Subject.fromNode(token, node);

		}
		return null;
	}

	@Override
	public Set<String> getSupportedEntityTypes(SSOToken token, String realm) throws AuthorizationException {

		validateTokenRealmParams(token, realm);

		@SuppressWarnings("rawtypes")
		Set supportedTypes;
		try {
			AMIdentityRepository repo = new AMIdentityRepository(token, realm);
			supportedTypes = repo.getSupportedIdTypes();
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}
		Set<String> entityTypeSet = new HashSet<String>(supportedTypes.size() * 2);
		for (@SuppressWarnings("rawtypes")
		Iterator iter = supportedTypes.iterator(); iter.hasNext();) {
			IdType type = (IdType) iter.next();
			if ((!type.equals(IdType.AGENTONLY) && !type.equals(IdType.AGENTGROUP) && !type.equals(IdType.AGENT))) {
				entityTypeSet.add(type.getName());
			}
		}
		return entityTypeSet;

		/*
		 * Set<String> entityTypeSet = new HashSet<String>();
		 * entityTypeSet.add("group"); entityTypeSet.add("user"); //
		 * entityTypeSet.add("agent"); return entityTypeSet;
		 */

	}

	@Override
	public Set<AMIdentity> getAvailableIdentity(SSOToken token, String realm, String entity, String filter)
			throws AuthorizationException {

		IdSearchResults results;
		Set<AMIdentity> identitySet = new HashSet<AMIdentity>();
		try {
			final int sizeLimit = 10;
			final int timeoutLimit = 2000;
			results = getIdentity(token, realm, entity, filter, sizeLimit, timeoutLimit);

			@SuppressWarnings("unchecked")
			Set<AMIdentity> possibleValues = results.getSearchResults();

			Iterator<AMIdentity> iter = possibleValues.iterator();
			while (iter.hasNext()) {
				AMIdentity amIdentity = iter.next();
				if ((!("dsameuser".equals(amIdentity.getName()))) && (!("amldapuser".equals(amIdentity.getName())))
						&& (!("amService-URLAccessAgent".equals(amIdentity.getName())))) {
					identitySet.add(amIdentity);
				}
			}

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		} catch (IdRepoException e) {
			LOGGER.error("IDRepoException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

		return identitySet;
	}

	@SuppressWarnings("deprecation")
	private IdSearchResults getIdentity(SSOToken token, String realmName, String entity, String filter, int sizeLimit,
			int timeoutLimit) throws SSOException, IdRepoException {
		if (realmName == null) {
			realmName = "/";
		}

		if ((filter == null) || (filter.trim().length() == 0)) {
			filter = "*";
		}

		AMIdentityRepository repo = new AMIdentityRepository(token, realmName);
		IdType type = IdUtils.getType(entity);

		IdSearchControl idsc = new IdSearchControl();
		idsc.setRecursive(true);
		idsc.setMaxResults(sizeLimit);
		idsc.setTimeOut(timeoutLimit);

		IdSearchResults results = repo.searchIdentities(type, filter, idsc);

		return results;

	}

	@Override
	public ResponseProvider getResponseProvider(SSOToken token, String realm, String policyName,
			String responseProviderName) throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (responseProviderName == null) {
			throw new IllegalArgumentException("Param responseProviderName cannot be null. ");
		}
		Node node = getPolicyChildNodeByName(token, realm, policyName,
				AuthorizationConstants.POLICY_RESPONSE_PROVIDERS_NODE,
				AuthorizationConstants.POLICY_RESPONSE_PROVIDER_NODE, responseProviderName);
		if (node != null) {
			return ResponseProvider.fromNode(node);
		}
		return null;
	}

	@Override
	public void removePolicy(SSOToken token, String realm, String policyName) throws AuthorizationException {

		if (policyName == null) {
			throw (new IllegalArgumentException("policyName cannot be null"));
		}
		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			if (namedPolicy != null) {
				namedPolicy.removeSubConfig(policyName);
			} else {
				throw new AuthorizationException("Policy not found.");
			}
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}
	}

	@Override
	public void addPolicy(SSOToken token, String realm, Policy policy) throws AuthorizationException, SMSException {
		validateTokenRealmParams(token, realm);
		if (policy == null) {
			throw new IllegalArgumentException("Param policy cannot be null. ");
		}

		policy.setCreatedBy(username);
		policy.setLastModifiedBy(username);
		Map<String, Set<String>> attrs = new HashMap<String, Set<String>>();
		Set<String> set = new HashSet<String>();
		set.add(policy.toXML());
		attrs.put(POLICY_XML, set);

		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			namedPolicy.addSubConfig(policy.getName(), NAMED_POLICY_ID, 0, attrs);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new SMSException(e.getMessage());
		}
	}

	@Override
	public Policy getPolicy(SSOToken token, String realm, String policyName) throws AuthorizationException {
		validateTokenRealmParams(token, realm);

		Node node = null;
		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			node = getPolicy(namedPolicy, policyName);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}
		if (node == null) {
			return null;
		}
		Policy policy = Policy.fromNode(token, node);
		return policy;
	}

	@Override
	public void updatePolicy(SSOToken token, String realm, String policyOldName, Policy policy)
			throws AuthorizationException, SMSException {
		try {
			policy.setLastModifiedBy(username);
			replacePolicy(token, realm, policyOldName, policy);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new SMSException(e.getMessage());
		}

	}

	@Override
	public void removeCondition(SSOToken token, String realm, String policyName, String conditionName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (conditionName == null) {
			throw new IllegalArgumentException("Param conditionName cannot be null. ");
		}
		Policy policy = getPolicy(token, realm, policyName);
		List<Condition> conditions = policy.getConditions();

		boolean found = false;
		int i = 0;
		while ((!found) & (i < conditions.size())) {
			if (conditions.get(i).getName().equals(conditionName)) {
				conditions.remove(i);
				found = true;
			}
			i++;
		}
		try {
			replacePolicy(token, realm, policy.getName(), policy);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}
	}

	@Override
	public void removeRule(SSOToken token, String realm, String policyName, String ruleName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (ruleName == null) {
			throw new IllegalArgumentException("Param ruleName cannot be null. ");
		}
		Policy policy = getPolicy(token, realm, policyName);
		List<Rule> rules = policy.getRules();

		boolean found = false;
		int i = 0;
		while ((!found) & (i < rules.size())) {
			if (rules.get(i).getName().equals(ruleName)) {
				rules.remove(i);
				found = true;
			}
			i++;
		}
		try {
			replacePolicy(token, realm, policy.getName(), policy);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

	}

	@Override
	public void removeSubject(SSOToken token, String realm, String policyName, String subjectName)
			throws AuthorizationException {
		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (subjectName == null) {
			throw new IllegalArgumentException("Param subjectName cannot be null. ");
		}
		Policy policy = getPolicy(token, realm, policyName);
		List<org.osiam.frontend.am.domain.authorization.Subject> subjects = policy.getSubjects();

		boolean found = false;
		int i = 0;
		while ((!found) & (i < subjects.size())) {
			if (subjects.get(i).getName().equals(subjectName)) {
				subjects.remove(i);
				found = true;
			}
			i++;
		}
		try {
			replacePolicy(token, realm, policy.getName(), policy);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

	}

	@Override
	public void removeResponseProvider(SSOToken token, String realm, String policyName, String responseProviderName)
			throws AuthorizationException {

		validateTokenRealmParams(token, realm);
		if (policyName == null) {
			throw new IllegalArgumentException("Param policyName cannot be null. ");
		}
		if (responseProviderName == null) {
			throw new IllegalArgumentException("Param responseProviderName cannot be null. ");
		}
		Policy policy = getPolicy(token, realm, policyName);
		List<ResponseProvider> responseProviders = policy.getResponseProviders();

		boolean found = false;
		int i = 0;
		while ((!found) & (i < responseProviders.size())) {
			if (responseProviders.get(i).getName().equals(responseProviderName)) {
				responseProviders.remove(i);
				found = true;
			}
			i++;
		}
		try {
			replacePolicy(token, realm, policy.getName(), policy);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

	}

	private Node getPolicyChildNodeByName(SSOToken token, String realm, String policyName, String parentNodeType,
			String nodeType, String nodeName) throws AuthorizationException {

		Set<Node> parentNode = getPolicyChildNodes(token, realm, policyName, parentNodeType);

		if (parentNode == null) {
			throw new AuthorizationException(String.format("Something is wrong with the data. No '%s' tag found.",
					parentNodeType));
		} else if (parentNode.size() == 0) {
			return null;
		} else if (parentNode.size() > 1) {
			throw new AuthorizationException(String.format(
					"Something is wrong with the data. Must be at most one '%s' tag, seems there are many.",
					parentNodeType));
		}

		Set<Node> nodes = XmlUtil.getChildNodes(parentNode.iterator().next(), nodeType);

		if (nodes != null) {
			Iterator<Node> items = nodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
				if (name.equals(nodeName)) {
					return node;
				}
			}
		}

		return null;
	}

	private Node getPolicyChildNodeByName(SSOToken token, String realm, String policyName, String nodeType,
			String nodeName) throws AuthorizationException {

		Set<Node> nodes = getPolicyChildNodes(token, realm, policyName, nodeType);

		if (nodes != null) {
			Iterator<Node> items = nodes.iterator();
			while (items.hasNext()) {
				Node node = items.next();
				String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
				if (name.equals(nodeName)) {
					return node;
				}
			}
		}

		return null;
	}

	private Document getXMLDocument(InputStream in) throws XMLException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(in);
			return doc;
		} catch (SAXParseException pe) {
			throw (new XMLException("XMLDocument parse error."));
		} catch (SAXException sax) {
			throw (new XMLException("XMLDocument parse error."));
		} catch (ParserConfigurationException pc) {
			throw (new XMLException("XMLDocument parse error."));
		} catch (IOException ioe) {
			throw (new XMLException("XMLDocument parse error."));
		}
	}

	private Node getRootNode(Document doc, String nodeName) {
		NodeList nodes = doc.getElementsByTagName(nodeName);
		if (nodes == null || nodes.getLength() == 0) {
			return (null);
		}
		return (nodes.item(0));
	}

	private Node getPolicy(ServiceConfig namedPolicy, String policyName) throws AuthorizationException, SSOException,
			SMSException {

		ServiceConfig policy = null;

		Set<String> res = null;
		policy = namedPolicy.getSubConfig(policyName);
		if (policy == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		Map<String, Set<String>> attrs = policy.getAttributes();
		res = attrs.get(POLICY_XML);

		Iterator<String> it = res.iterator();
		String policyXml = it.next();
		Document doc = null;
		try {
			doc = getXMLDocument(new ByteArrayInputStream(policyXml.getBytes("UTF8")));
		} catch (Exception xmle) {
			throw new AuthorizationException("error.error_getting_policy_xml");
		}
		Node rootNode = getRootNode(doc, AuthorizationConstants.POLICY_ROOT_NODE);
		if (rootNode == null) {
			throw new AuthorizationException("invalid_xml_policy_root_node");
		}
		return rootNode;
	}

	private Set<Node> getPolicyChildNodes(SSOToken token, String realm, String policyName, String nodeName)
			throws AuthorizationException {

		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			Node node = getPolicy(namedPolicy, policyName);
			if (node == null) {
				throw new IllegalArgumentException("Policy not found.");
			}
			Set<Node> nodes = XmlUtil.getChildNodes(node, nodeName);
			return nodes;

		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AuthorizationException(e.getMessage(), e);

		} catch (SMSException e) {
			LOGGER.error("SMSException occured", e);
			throw new AuthorizationException(e.getMessage(), e);
		}

	}

	private ServiceConfig getNamedPolicy(SSOToken token, String realm) throws SSOException, SMSException,
			IllegalArgumentException {
		ServiceConfigManager scm;
		scm = new ServiceConfigManager(POLICY_SERVICE_NAME, token);
		ServiceConfig organizationConfig = scm.getOrganizationConfig(realm, null);
		validateServiceConfig(organizationConfig, realm);
		ServiceConfig namedPolicy = organizationConfig.getSubConfig(NAMED_POLICY);
		validateServiceConfig(namedPolicy, realm);
		return namedPolicy;

	}

	private void replacePolicy(SSOToken token, String realm, String originalPolicyName, Policy policy)
			throws AuthorizationException, SSOException, SMSException {

		Map<String, Set<String>> attrs = new HashMap<String, Set<String>>();
		Set<String> set = new HashSet<String>();
		set.add(policy.toXML());
		attrs.put(POLICY_XML, set);

		ServiceConfig namedPolicy = getNamedPolicy(token, realm);

		
		String oldPolicyName = originalPolicyName;
	
		ServiceConfig oldPolicyEntry = null;
		if (oldPolicyName != null) {
			oldPolicyEntry = namedPolicy.getSubConfig(oldPolicyName);
		}

		if (oldPolicyEntry != null) {
			removePolicy(token, realm, oldPolicyName);
			addPolicy(token, realm, policy);
		} else {
			throw new AuthorizationException("Policy not found.");
		}

	}

	@Override
	public AMIdentity getAMIdentityByUniversalId(SSOToken token, String universalId) throws AuthorizationException {
		try {
			return new AMIdentity(token, universalId);
		} catch (IdRepoException e) {
			throw new AuthorizationException(e.getMessage(), e);
		}
	}

	@Override
	public boolean policyNameAlreadyExist(SSOToken token, String realm, String policyName) {
		validateTokenRealmParams(token, realm);
		try {
			ServiceConfig namedPolicy = getNamedPolicy(token, realm);
			Node node = null;
			node = getPolicy(namedPolicy, policyName);
			return (node != null);
		} catch (SSOException e) {
			LOGGER.error("SSOException occured", e);
			throw new AgentsException(e.getMessage(), e);
		} catch (SMSException e) {
			LOGGER.error("IdRepoException occured", e);
			throw new AgentsException(e.getMessage(), e);
		}
	}
}
