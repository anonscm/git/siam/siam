/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.reflect.ConstructorUtils;
import org.apache.log4j.Logger;
import org.osiam.frontend.am.exceptions.AuthorizationException;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

import com.iplanet.sso.SSOToken;

/**
 * The Subject class encapsulates the data for a rule.
 */
public abstract class Subject extends BaseEntity {

	/**
	 * Automatically generated field.
	 */
	private static final long serialVersionUID = 1L;

	private String exclusive;

	private static final Logger LOGGER = Logger.getLogger(Condition.class);

	private static final Map<String, Class<? extends Subject>> CLAZZ_MAP = new HashMap<String, Class<? extends Subject>>();

	static {
		CLAZZ_MAP.put(AuthenticatedUsersSubject.TYPE, AuthenticatedUsersSubject.class);
		CLAZZ_MAP.put(OpenSSOIdentitySubject.TYPE, OpenSSOIdentitySubject.class);
	}

	/**
	 * @return subject created based on the given XML node.
	 * @param token
	 *            - the SSO token.
	 * @param node
	 *            - the XML node to be parsed.
	 * @throws AuthorizationException
	 *             - if something is wrong during the object creation.
	 */
	public static Subject fromNode(SSOToken token, Node node) throws AuthorizationException {

		String name = XmlUtil.getNodeAttributeValue(node, XmlUtil.NAME_ATTRIBUTE);
		String type = XmlUtil.getNodeAttributeValue(node, XmlUtil.TYPE_ATTRIBUTE);
		String exclusiveAttribute = XmlUtil.getNodeAttributeValue(node, XmlUtil.INCLUDE_TYPE_ATTRIBUTE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Read subject with name %s and type %s from xml node.", name, type));
			LOGGER.debug("Xml :");
			LOGGER.debug(XmlUtil.getString(node));
		}

		Class<? extends Subject> subjectClazz = CLAZZ_MAP.get(type);

		if (subjectClazz == null) {
			String error = String.format("Invalid type %s for %s  . Xml : %s", type, name, XmlUtil.getString(node));
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}

		Subject subject = null;
		try {
			subject = (Subject) ConstructorUtils.invokeConstructor(subjectClazz, new Object[] { token, name, node });
			subject.setExclusive(exclusiveAttribute);
		} catch (NoSuchMethodException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (IllegalAccessException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (InvocationTargetException e) {
			throw new AuthorizationException("Error creating instance.", e);
		} catch (InstantiationException e) {
			throw new AuthorizationException("Error creating instance.", e);
		}

		return subject;

	}

	/**
	 * 
	 * Creates a {@link Subject} with the the given name and the given type.
	 * 
	 * @param name
	 *            - the condition name.
	 * @param type
	 *            - the condition type.
	 */
	public Subject(String name, String type) {
		super(name, type);
	}

	/**
	 * Creates a {@link Subject}.
	 */
	public Subject() {

	}

	public String getExclusive() {
		return exclusive;
	}

	public void setExclusive(String exclusive) {
		this.exclusive = exclusive;
	}

	/**
	 * 
	 * Appends XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the string buffer to append to.
	 */
	public void appendXML(StringBuilder buffer) {
		buffer.append("<Subject ");

		buffer.append("name=\"");
		buffer.append(getName());
		buffer.append("\"");

		buffer.append(" type=\"");
		buffer.append(getType());
		buffer.append("\"");

		buffer.append(" includeType=\"");
		buffer.append(getExclusive());
		buffer.append("\"");
		buffer.append(">");

		appendInnerXML(buffer);

		buffer.append("</Subject>");
	}

	/**
	 * 
	 * Appends the XML for this object to given buffer.
	 * 
	 * @param buffer
	 *            - the buffer to append the XML to.
	 */
	public abstract void appendInnerXML(StringBuilder buffer);

	/**
	 * 
	 * @return type of subject.
	 */
	@Override
	public abstract String getType();

	/**
	 * Get {@link Subject} by name from given list.
	 * 
	 * @param name
	 *            - {@link Subject} name
	 * @param list
	 *            - {@link List} of subjects
	 * @return - {@link Subject}
	 */
	public static Subject getSubjectByName(String name, List<Subject> list) {
		for (Iterator<Subject> iterator = list.iterator(); iterator.hasNext();) {
			Subject subject = iterator.next();
			if (subject.getName().equals(name)) {
				return subject;
			}
		}
		return null;
	}

	/**
	 * Get {@link Subject} by name and type from given list.
	 * 
	 * @param name
	 *            - {@link Subject} name
	 * @param type
	 *            - {@link Subject} type
	 * @param list
	 *            - list of subjects
	 * @return {@link Subject}
	 */
	public static Subject getSubjectByNameAndType(String name, String type, List<Subject> list) {
		for (Iterator<Subject> iterator = list.iterator(); iterator.hasNext();) {
			Subject subject = iterator.next();
			if (subject.getName().equals(name) && subject.getType().equals(type)) {
				return subject;
			}
		}
		return null;
	}

}
