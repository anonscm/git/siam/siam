/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.SessionProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * AddSessionPropertyConditionController shows the edit/add SessionProperty form
 * and does request processing of the edit/add SessionProperty action.
 * 
 */
@Controller("AddSessionPropertyConditionController")
@RequestMapping(value = "view", params = { "ctx=AddSessionPropertyConditionController" })
public class AddSessionPropertyConditionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AddSessionPropertyConditionController.class);

	/**
	 * Shows add/edit SessionProperty form.
	 * 
	 * @param sessionPropertyName
	 *            - SessionProperty name
	 * @param conditionName
	 *            - Condition name
	 * @param policyName
	 *            - Policy Name
	 * @param policyType
	 *            - Policy type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editSessionProperty(String sessionPropertyName, String conditionName, String policyName,
			String policyType, String action, Model model, RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);
		model.addAttribute("sessionPropertyName", sessionPropertyName);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "sessionProperty")) {
			SessionProperty sessionProperty = new SessionProperty();
			SessionProperty[] sessionPropertyListOld = (SessionProperty[]) request.getPortletSession().getAttribute(
					"sessionPropertyListNew");

			if (sessionPropertyName != null) {
				for (int i = 0; i < sessionPropertyListOld.length; i++) {
					if (sessionPropertyListOld[i].getName().equals(sessionPropertyName)) {
						sessionProperty = sessionPropertyListOld[i];
					}
				}
			} else {
				sessionProperty = new SessionProperty();
			}

			model.addAttribute("sessionProperty", sessionProperty);
		}

		model.addAttribute("conditionName", conditionName);
		model.addAttribute("actionValue", action);

		return "authorization/condition/currentSessionProperties/editSessionProperty";

	}

	/**
	 * Add value in SessionProperty list.
	 * 
	 * @param sessionProperty
	 *            - SessionProperty
	 * @param sessionPropertyAddValue
	 *            - {@link String} value to be insert
	 * @param sessionPropertyName
	 *            - SessionProperty name
	 * @param conditionName
	 *            - Condition name
	 * @param policyName
	 *            - Policy name
	 * @param policyType
	 *            - Policy type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addValueList")
	public void doAddValueList(SessionProperty sessionProperty, String sessionPropertyAddValue,
			String sessionPropertyName, String conditionName, String policyName, String policyType,
			ActionRequest request, ActionResponse response) {

		if (sessionPropertyAddValue != null && !sessionPropertyAddValue.equals("")) {
			sessionProperty.getValueList().add(sessionPropertyAddValue);
		}

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("sessionPropertyName", sessionPropertyName);
	}

	/**
	 * Remove values from SessionProperty list.
	 * 
	 * @param sessionProperty
	 *            - SessionProperty
	 * @param sessionPropertyDeleteValues
	 *            - {@link String} values to be deleted
	 * @param sessionPropertyName
	 *            - SessionProperty name
	 * @param conditionName
	 *            - Condition name
	 * @param policyName
	 *            - Policy name
	 * @param policyType
	 *            - Policy type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteValueList")
	public void doDeleteValueList(SessionProperty sessionProperty, String[] sessionPropertyDeleteValues,
			String sessionPropertyName, String conditionName, String policyName, String policyType,
			ActionRequest request, ActionResponse response) {

		if (sessionPropertyDeleteValues != null) {
			for (int i = 0; i < sessionPropertyDeleteValues.length; i++) {
				sessionProperty.getValueList().remove(sessionPropertyDeleteValues[i]);
			}
		}

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("sessionPropertyName", sessionPropertyName);
	}

	/**
	 * Save SessionProperty for current Condition.
	 * 
	 * @param sessionProperty
	 *            - SessionProperty to save
	 * @param result
	 *            - BindingResult
	 * @param sessionPropertyName
	 *            - SessionProperty name
	 * @param conditionName
	 *            - Condition name
	 * @param policyName
	 *            - Policy name
	 * @param policyType
	 *            - Policy type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("sessionProperty") @Valid SessionProperty sessionProperty, BindingResult result,
			String sessionPropertyName, String conditionName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("sessionPropertyName", sessionPropertyName);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		SessionProperty[] sp = (SessionProperty[]) request.getPortletSession().getAttribute("sessionPropertyListNew");

		List<SessionProperty> list = new ArrayList<SessionProperty>();
		if (sp != null) {
			list = new ArrayList<SessionProperty>(Arrays.asList(sp));
		}

		for (Iterator<SessionProperty> iterator = list.iterator(); iterator.hasNext();) {
			SessionProperty sessionP = iterator.next();
			if (sessionP.getName().equals(sessionPropertyName)) {
				list.remove(sessionP);
				break;
			}
		}
		list.add(sessionProperty);

		SessionProperty[] sessionPropertyListNew = new SessionProperty[list.size()];
		request.getPortletSession().setAttribute("sessionPropertyListNew", list.toArray(sessionPropertyListNew));

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", "SessionPropertyConditionController");

	}

	/**
	 * Reset add/edit SessionProperty form.
	 * 
	 * @param sessionPropertyName
	 *            - SessionProperty name
	 * @param conditionName
	 *            - Condition name
	 * @param policyName
	 *            - Policy name
	 * @param policyType
	 *            - Policy type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String sessionPropertyName, String conditionName, String policyName, String policyType,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("sessionPropertyName", sessionPropertyName);

	}

	/**
	 * Back to condition form.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            - Policy name
	 * @param policyType
	 *            - Policy type
	 * @param conditionName
	 *            - Condition name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, String conditionName,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("ctx", "SessionPropertyConditionController");

	}

}
