/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.service.AuthorizationService;
import org.osiam.frontend.common.domain.BaseEntity;
import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.osiam.policy_repo.PolicySetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * AuthorizationController portlet controller.
 * 
 */
@Controller("AuthorizationController")
@RequestMapping(value = "view", params = "ctx=AuthorizationController")
public class AuthorizationController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationController.class);

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private PolicySetService policySetService;

	@Autowired
	@Qualifier("policyValidator")
	private Validator policyValidator;

	/**
	 * Show the policies list.
	 * 
	 * @param checkAllPolicy
	 *            - use to decide if all the Datastore are checked for deletion
	 * @param model
	 *            - the model
	 * @param request
	 *            - the request
	 * @return - the view's name.
	 */
	@RenderMapping
	public String view(String checkAllPolicy, String error, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		model.addAttribute("checkAllPolicy", checkAllPolicy);

		request.getPortletSession().setAttribute("policy", null);

		model.addAttribute("policyTypesList", authorizationService.getPolicyTypesList());
		model.addAttribute("policiesList", authorizationService.getPoliciesList(token, realm));
		
		try{
			model.addAttribute("xacmlPoliciesList", policySetService.retrievePolicySetList(realm));
		}catch (Exception e) {
			model.addAttribute("xacmlPoliciesListError", e.getMessage());
		}

		model.addAttribute("error", error);
		return "authorization/view";
	}

	/**
	 * Delete policies.
	 * 
	 * @param deletePolicyList
	 *            - list of Policy name
	 * @param deleteXACMLPolicyList
	 *            - list of XACML Policy name
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deletePolicy")
	public void doDeletePolicy(String[] deletePolicyList, String[] deleteXACMLPolicyList, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (deletePolicyList != null) {
			for (int i = 0; i < deletePolicyList.length; i++) {
				authorizationService.removePolicy(token, realm, deletePolicyList[i]);
			}
		}

		if (deleteXACMLPolicyList != null) {
			for (int i = 0; i < deleteXACMLPolicyList.length; i++) {
				policySetService.removePolicySet(realm, deleteXACMLPolicyList[i]);
			}
		}
	}

	/**
	 * Redirect to add Policy form.
	 * 
	 * @param entity
	 *            - entity to insert
	 * @param result
	 *            - BindingResult
	 * @param response
	 *            - the response
	 * @param request
	 *            - the request
	 */
	@ActionMapping(params = "action=doSavePolicy")
	public void doSavePolicy(@ModelAttribute("entity") @Valid BaseEntity entity, BindingResult result,
			ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		} else {
			policyValidator.validate(new ObjectForValidate(realm, token, entity), result);
			if (result.hasErrors()) {
				LOGGER.debug("Form invalid");
				return;
			}
		}

		response.setRenderParameter("policyName", entity.getName());
		response.setRenderParameter("action", CommonConstants.ACTION_INSERT);
		response.setRenderParameter("ctx", entity.getType() + "PolicyController");

	}

	/**
	 * Set value for "checkAllPolicy".
	 * 
	 * @param checkAllPolicy
	 *            - use to decide if all the Policy are checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(String checkAllPolicy, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("checkAllPolicy", checkAllPolicy);
	}
}
