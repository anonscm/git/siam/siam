/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.SessionPropertiesCondition;
import org.osiam.frontend.am.domain.authorization.SessionProperty;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * SessionPropertyConditionController shows the edit/add
 * {@link SessionPropertiesCondition} form and does request processing of the
 * edit/add {@link SessionPropertiesCondition} action.
 * 
 */
@Controller("SessionPropertyConditionController")
@RequestMapping(value = "view", params = { "ctx=SessionPropertyConditionController" })
public class SessionPropertyConditionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionPropertyConditionController.class);

	@Autowired
	@Qualifier("sessionPropertiesConditionValidator")
	private Validator sessionPropertiesConditionValidator;

	/**
	 * Shows add/edit {@link SessionPropertiesCondition} form.
	 * 
	 * @param checkAllSessionProperty
	 *            - use to decide if all the {@link SessionProperty} are checked
	 *            for deletion
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editCondition(String checkAllSessionProperty, String conditionName, String policyName,
			String policyType, String action, Model model, RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "condition")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			SessionPropertiesCondition condition = (SessionPropertiesCondition) Condition.getConditionsByNameAndType(
					conditionName, AuthorizationConstants.CONDITION_TYPE_SESSION_PROPERTY_CONDITION,
					policy.getConditions());
			if (condition == null) {
				condition = new SessionPropertiesCondition(conditionName);
			}

			model.addAttribute("condition", condition);

			SessionProperty[] sessionPropertyListNew = (SessionProperty[]) request.getPortletSession().getAttribute(
					"sessionPropertyListNew");
			if (sessionPropertyListNew == null) {
				sessionPropertyListNew = new SessionProperty[condition.getSessionPropertyList().size()];
				condition.getSessionPropertyList().toArray(sessionPropertyListNew);
				request.getPortletSession().setAttribute("sessionPropertyListNew", sessionPropertyListNew);
			}
		}

		model.addAttribute("sessionPropertyListNew", request.getPortletSession().getAttribute("sessionPropertyListNew"));

		model.addAttribute("conditionName", conditionName);
		model.addAttribute("actionValue", action);
		model.addAttribute("checkAllSessionProperty", checkAllSessionProperty);

		return "authorization/condition/currentSessionProperties/edit";

	}

	/**
	 * Delete {@link SessionProperty} for current
	 * {@link SessionPropertiesCondition}.
	 * 
	 * @param deleteSessionPropertyList
	 *            - list of {@link SessionProperty} name
	 * @param condition
	 *            - {@link Condition}
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteSessionProperty")
	public void doDeleteSessionProperty(String[] deleteSessionPropertyList,
			@ModelAttribute("condition") @Valid SessionPropertiesCondition condition, BindingResult result,
			String conditionName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		List<SessionProperty> list = new ArrayList<SessionProperty>(Arrays.asList((SessionProperty[]) request
				.getPortletSession().getAttribute("sessionPropertyListNew")));

		for (int i = 0; i < deleteSessionPropertyList.length; i++) {
			for (Iterator<SessionProperty> iterator = list.iterator(); iterator.hasNext();) {
				SessionProperty sessionProperty = iterator.next();
				if (deleteSessionPropertyList[i].equals(sessionProperty.getName())) {
					list.remove(sessionProperty);
					break;
				}
			}
		}

		SessionProperty[] sessionPropertyListNew = new SessionProperty[list.size()];
		list.toArray(sessionPropertyListNew);
		request.getPortletSession().setAttribute("sessionPropertyListNew", sessionPropertyListNew);

	}

	/**
	 * Redirect to add {@link SessionProperty} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addSessionProperty")
	public void doAddSessionProperty(String conditionName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		response.setRenderParameter("ctx", "AddSessionPropertyConditionController");

	}

	/**
	 * Save {@link SessionPropertiesCondition} for current {@link Policy}.
	 * 
	 * @param condition
	 *            - {@link Condition} to save
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("condition") @Valid SessionPropertiesCondition condition, BindingResult result,
			String conditionName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		condition.setSessionPropertyList(new ArrayList<SessionProperty>(Arrays.asList((SessionProperty[]) request
				.getPortletSession().getAttribute("sessionPropertyListNew"))));

		request.getPortletSession().setAttribute("sessionPropertyListNew", null);

		sessionPropertiesConditionValidator.validate(condition, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Condition conditionOld = Condition.getConditionsByNameAndType(conditionName,
				AuthorizationConstants.CONDITION_TYPE_SESSION_PROPERTY_CONDITION, policy.getConditions());
		policy.getConditions().remove(conditionOld);

		policy.getConditions().add(condition);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link SessionPropertiesCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String conditionName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		request.getPortletSession().setAttribute("sessionPropertyListNew", null);

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		request.getPortletSession().setAttribute("sessionPropertyListNew", null);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}
	
	/**
	 * Set value for "checkAllSessionProperty".
	 * 
	 * @param checkAllSessionProperty
	 *            - use to decide if all the Session Properties are checked for deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(String checkAllSessionProperty, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("checkAllSessionProperty", checkAllSessionProperty);
	}

}
