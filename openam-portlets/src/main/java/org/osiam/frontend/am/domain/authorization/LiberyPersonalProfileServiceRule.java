/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * The LiberyPersonalProfileServiceRule class encapsulates the data for a
 * liberty personal profile service rule.
 */
public class LiberyPersonalProfileServiceRule extends Rule {

	private static final long serialVersionUID = 1L;

	/**
	 * The type string constant for this type of response provider.
	 */
	public static final String TYPE = "sunIdentityServerLibertyPPService";

	private static final String MODIFY = "MODIFY";
	private static final String QUERY = "QUERY";

	private boolean modify;
	private String modifyValue;
	private boolean query;
	private String queryValue;

	private static final Logger LOGGER = Logger.getLogger(LiberyPersonalProfileServiceRule.class);

	/**
	 * Creates a new rule with the given name.
	 * 
	 * @param name
	 *            - the name.
	 */
	public LiberyPersonalProfileServiceRule(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new rule.
	 */
	public LiberyPersonalProfileServiceRule() {

	}

	/**
	 * Creates a new rule with the given name by parsing the given XML node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public LiberyPersonalProfileServiceRule(String name, Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Rule XML contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {
			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);
			if (attributeValuePair.getName().equals(MODIFY)) {
				modifyValue = attributeValuePair.getValueList().get(0);
				modify = true;
			} else if (attributeValuePair.getName().equals(QUERY)) {
				queryValue = attributeValuePair.getValueList().get(0);
				query = true;
			}
		}
	}

	public String getModifyValue() {
		return modifyValue;
	}

	public void setModifyValue(String aModifyValue) {
		this.modifyValue = aModifyValue;
	}

	public String getQueryValue() {
		return queryValue;
	}

	public void setQueryValue(String aQueryValue) {
		this.queryValue = aQueryValue;
	}

	public boolean getModify() {
		return modify;
	}

	public void setModify(boolean modify) {
		this.modify = modify;
	}

	public boolean getQuery() {
		return query;
	}

	public void setQuery(boolean query) {
		this.query = query;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		if (query) {
			XmlUtil.appendAttributeValuePairToString(buffer, QUERY, queryValue);
		}
		if (modify) {
			XmlUtil.appendAttributeValuePairToString(buffer, MODIFY, modifyValue);
		}
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
