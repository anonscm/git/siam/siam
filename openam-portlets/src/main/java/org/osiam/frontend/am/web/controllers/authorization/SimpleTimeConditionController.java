/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import java.util.TimeZone;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.TimeDateCondition;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * SimpleTimeConditionController shows the edit/add {@link TimeDateCondition}
 * form and does request processing of the edit/add {@link TimeDateCondition}
 * action.
 * 
 */
@Controller("SimpleTimeConditionController")
@RequestMapping(value = "view", params = { "ctx=SimpleTimeConditionController" })
public class SimpleTimeConditionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTimeConditionController.class);

	@Autowired
	@Qualifier("timeDateConditionValidator")
	private Validator timeDateConditionValidator;

	/**
	 * Shows add/edit {@link TimeDateCondition} form.
	 * 
	 * @param error
	 *            - error key
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editCondition(String error, String conditionName, String policyName, String policyType,
			String action, Model model, RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);
		model.addAttribute("error", error);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "condition") && error == null) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			TimeDateCondition condition = (TimeDateCondition) Condition.getConditionsByNameAndType(conditionName,
					AuthorizationConstants.CONDITION_TYPE_SIMPLE_TIME_CONDITION, policy.getConditions());
			if (condition == null) {
				condition = new TimeDateCondition(conditionName);
			}
			model.addAttribute("condition", condition);
			if(!isAvailableId(condition.getTimezone())){
				model.addAttribute("customTimezine", "true");
			}

		}
		model.addAttribute("conditionName", conditionName);
		model.addAttribute("actionValue", action);

		model.addAttribute("timezoneAvailable", TimeZone.getAvailableIDs());

		return "authorization/condition/simpleTimeCondition/edit";

	}
	
	private boolean isAvailableId(String timezone){
		boolean isValid = false;
		String[] list = TimeZone.getAvailableIDs();

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(timezone)) {
				isValid = true;
				break;
			}
		}
		return isValid;
		
	}

	/**
	 * Save {@link TimeDateCondition} for current {@link Policy}.
	 * 
	 * @param condition
	 *            - {@link Condition} to save
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("condition") @Valid TimeDateCondition condition, BindingResult result,
			String conditionName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		timeDateConditionValidator.validate(condition, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Condition conditionOld = Condition.getConditionsByNameAndType(conditionName,
				AuthorizationConstants.CONDITION_TYPE_SIMPLE_TIME_CONDITION, policy.getConditions());
		policy.getConditions().remove(conditionOld);

		policy.getConditions().add(setAditionalConditionValues(condition));

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	private TimeDateCondition setAditionalConditionValues(TimeDateCondition con) {

		boolean startDateNull = (con.getStartDate() == null || "".equals(con.getStartDate())) ? true : false;
		boolean endDateNull = (con.getEndDate() == null || "".equals(con.getEndDate())) ? true : false;

		if (!startDateNull && endDateNull) {
			con.setEndDate(con.getStartDate());
		}
		if (startDateNull && !endDateNull) {
			con.setStartDate(con.getEndDate());
		}

		boolean startTimeNull = (con.getStartHour() == null || con.getStartMinute() == null) ? true : false;
		boolean endTimeNull = (con.getEndHour() == null || con.getEndMinute() == null) ? true : false;

		if (!startTimeNull && endTimeNull) {
			con.setEndHour(con.getStartHour());
			con.setEndMinute(con.getStartMinute());
			con.setEndAmPm(con.getStartAmPm());
		}
		if (startTimeNull && !endTimeNull) {
			con.setStartHour(con.getEndHour());
			con.setStartMinute(con.getEndMinute());
			con.setStartAmPm(con.getEndAmPm());
		}

		boolean startDayNull = (con.getStartDay() == null || "".equals(con.getStartDay())) ? true : false;
		boolean endDayNull = (con.getEndDay() == null || "".equals(con.getEndDay())) ? true : false;

		if (!startDayNull && endDayNull) {
			con.setEndDay(con.getStartDay());
		}
		if (startDayNull && !endDayNull) {
			con.setStartDay(con.getEndDay());
		}

		return con;
	}

	/**
	 * Reset add/edit {@link TimeDateCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String conditionName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
