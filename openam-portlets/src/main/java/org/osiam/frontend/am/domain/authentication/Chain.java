/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authentication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Chain object which defines properties of a chain.
 */
public class Chain implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Chain name.
	 */
	@NotNull
	@Size(min = 1)
	private String name;

	private List<ChainProperty> propertiesList = new ArrayList<ChainProperty>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ChainProperty> getPropertiesList() {
		return propertiesList;
	}

	public void setPropertiesList(List<ChainProperty> propertiesList) {
		this.propertiesList = propertiesList;
	}

}
