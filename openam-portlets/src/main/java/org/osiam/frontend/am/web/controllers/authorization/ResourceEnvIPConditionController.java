/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.domain.authorization.ResourceEnvironmentIPCondition;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * ResourceEnvIPConditionController shows the add/edit
 * {@link ResourceEnvironmentIPCondition} form and does request processing of
 * the edit/add {@link ResourceEnvironmentIPCondition} action.
 * 
 */
@Controller("ResourceEnvIPConditionController")
@RequestMapping(value = "view", params = { "ctx=ResourceEnvIPConditionController" })
public class ResourceEnvIPConditionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceEnvIPConditionController.class);

	@Autowired
	@Qualifier("resourceEnvironmentIPConditionValidator")
	private Validator resourceEnvironmentIPConditionValidator;

	/**
	 * Shows add/edit {@link ResourceEnvironmentIPCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editCondition(String conditionName, String policyName, String policyType, String action, Model model,
			RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "condition")) {
			ResourceEnvironmentIPCondition condition = new ResourceEnvironmentIPCondition();

			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			condition = (ResourceEnvironmentIPCondition) Condition.getConditionsByNameAndType(conditionName,
					AuthorizationConstants.CONDITION_TYPE_RESOURCE_ENV_IP_CONDITION, policy.getConditions());
			if (condition == null) {
				condition = new ResourceEnvironmentIPCondition(conditionName);
			}

			model.addAttribute("condition", condition);

		}
		model.addAttribute("conditionName", conditionName);
		model.addAttribute("actionValue", action);

		return "authorization/condition/resourceEnvironmentIPAddress/edit";

	}

	/**
	 * Add value to Condition List.
	 * 
	 * @param condition
	 *            - {@link Condition}
	 * @param result
	 *            - BindingResult
	 * @param conditionListAddValue
	 *            - {@link String} value to be insert
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "addConditionList")
	public void doAddConditionList(@ModelAttribute("condition") @Valid ResourceEnvironmentIPCondition condition,
			BindingResult result, String conditionListAddValue, String conditionName, String policyName,
			String policyType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

		if (conditionListAddValue != null && !conditionListAddValue.equals("")) {
			condition.getConditionList().add(conditionListAddValue);
		}
	}

	/**
	 * Remove values from Condition List.
	 * 
	 * @param condition
	 *            - {@link Condition}
	 * @param result
	 *            - BindingResult
	 * @param conditionListDeleteValues
	 *            - {@link String} values to be deleted
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "deleteConditionList")
	public void doDeleteConditionList(@ModelAttribute("condition") @Valid ResourceEnvironmentIPCondition condition,
			BindingResult result, String[] conditionListDeleteValues, String conditionName, String policyName,
			String policyType, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

		if (conditionListDeleteValues != null) {
			for (int i = 0; i < conditionListDeleteValues.length; i++) {
				condition.getConditionList().remove(conditionListDeleteValues[i]);
			}
		}
	}

	/**
	 * Save {@link ResourceEnvironmentIPCondition} for current {@link Policy}.
	 * 
	 * @param condition
	 *            - {@link Condition} to save
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("condition") @Valid ResourceEnvironmentIPCondition condition,
			BindingResult result, String conditionName, String policyName, String policyType, String action,
			ActionRequest request, ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		resourceEnvironmentIPConditionValidator.validate(condition, result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Condition conditionOld = Condition.getConditionsByNameAndType(conditionName,
				AuthorizationConstants.CONDITION_TYPE_RESOURCE_ENV_IP_CONDITION, policy.getConditions());
		policy.getConditions().remove(conditionOld);
		policy.getConditions().add(condition);

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Reset add/edit {@link ResourceEnvironmentIPCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String conditionName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
