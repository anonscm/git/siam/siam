/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authentication;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authentication.Chain;
import org.osiam.frontend.am.service.AuthenticationService;
import org.osiam.frontend.common.domain.ObjectForValidate;
import org.osiam.frontend.common.service.CommonConstants;
import org.osiam.frontend.common.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.iplanet.sso.SSOToken;

/**
 * Authentication portlet controller.
 * 
 */
@Controller("AuthenticationController")
@RequestMapping(value = "view", params = "ctx=AuthenticationController")
public class AuthenticationController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	@Qualifier("chainValidator")
	private Validator chainValidator;

	/**
	 * Show the chain list, edit OrganizationChain and AdministratorChain form
	 * and insert new chain form. For each item of the list are available a
	 * deleting action and a link to edit chain.
	 * 
	 * @param checkAllChains
	 *            - use to decide if all the {@link Chain} are checked for
	 *            deletion
	 * @param model
	 *            - the model
	 * @param request
	 *            - the request
	 * @return - the view's name
	 */
	@RenderMapping
	public String view(String checkAllChains, Model model, RenderRequest request) {

		String realm = getCurrentRealm(request);

		request.getPortletSession().setAttribute("chainPropertiesList", null);
		model.addAttribute("chainNamesList", authenticationService.getChainNameSet(getSSOToken(realm, request), realm));
		model.addAttribute("checkAllChains", checkAllChains);
		model.addAttribute("orgAuth", authenticationService.getOrganizationChain(getSSOToken(realm, request), realm));
		model.addAttribute("adminAuth", authenticationService.getAdministratorChain(getSSOToken(realm, request), realm));

		return "authentication/view";
	}

	/**
	 * Handles chains delete. Will be delete a list of chains.
	 * 
	 * @param deleteChainList
	 *            - a list containing the chain names for deletion
	 * @param request
	 *            - the request
	 * @param response
	 *            - the response
	 */
	@ActionMapping(params = "deleteChain")
	public void doDeleteChain(String[] deleteChainList, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);

		if (deleteChainList != null) {
			for (int i = 0; i < deleteChainList.length; i++) {
				authenticationService.deleteChain(getSSOToken(realm, request), realm, deleteChainList[i]);
			}
		}
	}

	/**
	 * Handler Organization Chain and Administrator Chain configuration.
	 * 
	 * @param orgAuth
	 *            - the name of chain selected for Organization Authentication
	 * @param adminAuth
	 *            - the name of chain selected for Administrator Authentication
	 * @param request
	 *            - the request
	 * @param response
	 *            - the response
	 */
	@ActionMapping(params = "updateConfigChain")
	public void doUpdateConfigChain(String orgAuth, String adminAuth, ActionRequest request, ActionResponse response) {

		String realm = getCurrentRealm(request);

		authenticationService.saveAuthenticationConfiguration(getSSOToken(realm, request), realm, orgAuth, adminAuth);

	}

	/**
	 * Handler chain insert.
	 * 
	 * @param chain
	 *            - {@link Chain} to insert
	 * @param result
	 *            - the result
	 * @param request
	 *            - the request
	 * @param response
	 *            - the response
	 */
	@ActionMapping(params = "action=doSaveChain")
	public void doSaveChain(@ModelAttribute("chain") @Valid Chain chain, BindingResult result, ActionRequest request,
			ActionResponse response) {

		String realm = getCurrentRealm(request);
		SSOToken token = getSSOToken(realm, request);

		chainValidator.validate(new ObjectForValidate(realm, token, chain), result);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		}

		response.setRenderParameter("action", CommonConstants.ACTION_INSERT);

		response.setRenderParameter("name", chain.getName());
		response.setRenderParameter("ctx", "ChainPropertiesController");
	}

	/**
	 * Set value for "checkAllChains".
	 * 
	 * @param checkAllChains
	 *            - use to decide if all the {@link Chain} are checked for
	 *            deletion
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "checkForDelete")
	public void doCheckForDelete(String checkAllChains, ActionRequest request, ActionResponse response) {

		response.setRenderParameter("checkAllChains", checkAllChains);
	}

}
