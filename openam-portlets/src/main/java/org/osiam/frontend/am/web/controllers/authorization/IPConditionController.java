/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.web.controllers.authorization;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.osiam.frontend.am.domain.authorization.Condition;
import org.osiam.frontend.am.domain.authorization.IPDNSCondition;
import org.osiam.frontend.am.domain.authorization.Policy;
import org.osiam.frontend.am.service.AuthorizationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * IPConditionController shows the add/edit {@link IPDNSCondition} form and does
 * request processing of the edit/add {@link IPDNSCondition} action.
 * 
 */
@Controller("IPConditionController")
@RequestMapping(value = "view", params = { "ctx=IPConditionController" })
public class IPConditionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IPConditionController.class);

	@Autowired
	@Qualifier("ipDNSConditionValidator")
	private Validator ipDNSConditionValidator;

	/**
	 * Shows add/edit {@link IPDNSCondition} form.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param model
	 *            - Model
	 * @param request
	 *            - RenderRequest
	 * @return - the view's name
	 */
	@RenderMapping
	public String editCondition(String conditionName, String policyName, String policyType, String action, Model model,
			RenderRequest request) {

		model.addAttribute("policyName", policyName);
		model.addAttribute("policyType", policyType);

		if (!model.containsAttribute(BindingResult.MODEL_KEY_PREFIX + "condition")) {
			Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

			IPDNSCondition condition = (IPDNSCondition) Condition.getConditionsByNameAndType(conditionName,
					AuthorizationConstants.CONDITION_TYPE_IP_CONDITION, policy.getConditions());
			if (condition == null) {
				condition = new IPDNSCondition(conditionName);
			}
			model.addAttribute("condition", condition);

		}
		model.addAttribute("conditionName", conditionName);
		model.addAttribute("actionValue", action);

		return "authorization/condition/ipCondition/edit";

	}

	/**
	 * Save {@link IPDNSCondition} for current {@link Policy}.
	 * 
	 * @param condition
	 *            - {@link Condition} to save
	 * @param result
	 *            - BindingResult
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "save")
	public void doSave(@ModelAttribute("condition") @Valid IPDNSCondition condition, BindingResult result,
			String conditionName, String policyName, String policyType, String action, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);
		response.setRenderParameter("action", action);

		if (result.hasErrors()) {
			LOGGER.debug("Form invalid");
			return;
		} else {
			ipDNSConditionValidator.validate(condition, result);
			if (result.hasErrors()) {
				LOGGER.debug("Form invalid");
				return;
			}
		}

		Policy policy = (Policy) request.getPortletSession().getAttribute("policy");

		Condition conditionOld = Condition.getConditionsByNameAndType(conditionName,
				AuthorizationConstants.CONDITION_TYPE_IP_CONDITION, policy.getConditions());
		policy.getConditions().remove(conditionOld);
		policy.getConditions().add(getCondition(condition));

		request.getPortletSession().setAttribute("policy", policy);

		response.setRenderParameter("message", "message.policy_modified");

		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

	/**
	 * Set some values depending on existing values for given IPDNSCondition.
	 * <p>
	 * If the From IP Address has at least one block not null, the value for the
	 * null bloc will be set on 0. <br>
	 * If the To IP Address has at least one block not null, the value for the
	 * null bloc will be set on 0. <br>
	 * An IP Address is null if all the blocks are null or equal with 0.<br>
	 * If one IP Address id not null and the other is null than the two IP
	 * Addresses will have the same value (value of the not null IP Address) .
	 * </p>
	 * 
	 * @param con
	 * @return
	 */
	private IPDNSCondition getCondition(IPDNSCondition con) {

		boolean fromNull = ((con.getFromIPBlock1() == null || con.getFromIPBlock1().equals(Long.valueOf(0)))
				&& (con.getFromIPBlock2() == null || con.getFromIPBlock2().equals(Long.valueOf(0)))
				&& (con.getFromIPBlock3() == null || con.getFromIPBlock3().equals(Long.valueOf(0))) && (con
				.getFromIPBlock4() == null || con.getFromIPBlock4().equals(Long.valueOf(0)))) ? true : false;

		boolean toNull = ((con.getToIPBlock1() == null || con.getToIPBlock1().equals(Long.valueOf(0)))
				&& (con.getToIPBlock2() == null || con.getToIPBlock2().equals(Long.valueOf(0)))
				&& (con.getToIPBlock3() == null || con.getToIPBlock3().equals(Long.valueOf(0))) && (con.getToIPBlock4() == null || con
				.getToIPBlock1().equals(Long.valueOf(0)))) ? true : false;

		if (!fromNull && !toNull) {
			con.setFromIPBlock1((con.getFromIPBlock1() == null ? 0 : con.getFromIPBlock1()));
			con.setFromIPBlock2((con.getFromIPBlock2() == null ? 0 : con.getFromIPBlock2()));
			con.setFromIPBlock3((con.getFromIPBlock3() == null ? 0 : con.getFromIPBlock3()));
			con.setFromIPBlock4((con.getFromIPBlock4() == null ? 0 : con.getFromIPBlock4()));

			con.setToIPBlock1((con.getToIPBlock1() == null ? 0 : con.getToIPBlock1()));
			con.setToIPBlock2((con.getToIPBlock2() == null ? 0 : con.getToIPBlock2()));
			con.setToIPBlock3((con.getToIPBlock3() == null ? 0 : con.getToIPBlock3()));
			con.setToIPBlock4((con.getToIPBlock4() == null ? 0 : con.getToIPBlock4()));
		} else if (!fromNull && toNull) {
			con.setFromIPBlock1((con.getFromIPBlock1() == null ? 0 : con.getFromIPBlock1()));
			con.setFromIPBlock2((con.getFromIPBlock2() == null ? 0 : con.getFromIPBlock2()));
			con.setFromIPBlock3((con.getFromIPBlock3() == null ? 0 : con.getFromIPBlock3()));
			con.setFromIPBlock4((con.getFromIPBlock4() == null ? 0 : con.getFromIPBlock4()));

			con.setToIPBlock1(con.getFromIPBlock1());
			con.setToIPBlock2(con.getFromIPBlock2());
			con.setToIPBlock3(con.getFromIPBlock3());
			con.setToIPBlock4(con.getFromIPBlock4());

		} else if (fromNull && !toNull) {
			con.setToIPBlock1((con.getToIPBlock1() == null ? 0 : con.getToIPBlock1()));
			con.setToIPBlock2((con.getToIPBlock2() == null ? 0 : con.getToIPBlock2()));
			con.setToIPBlock3((con.getToIPBlock3() == null ? 0 : con.getToIPBlock3()));
			con.setToIPBlock4((con.getToIPBlock4() == null ? 0 : con.getToIPBlock4()));

			con.setFromIPBlock1(con.getToIPBlock1());
			con.setFromIPBlock2(con.getToIPBlock2());
			con.setFromIPBlock3(con.getToIPBlock3());
			con.setFromIPBlock4(con.getToIPBlock4());
		}

		return con;
	}

	/**
	 * Reset add/edit {@link IPDNSCondition} for current {@link Policy}.
	 * 
	 * @param conditionName
	 *            - {@link Condition} name
	 * @param policyName
	 *            - {@link Policy} name
	 * @param policyType
	 *            - {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "reset")
	public void doReset(String conditionName, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("policyType", policyType);
		response.setRenderParameter("conditionName", conditionName);

	}

	/**
	 * Back to policy.
	 * 
	 * @param action
	 *            - the action type.
	 *            <p>
	 *            Possible values for this parameter are :
	 *            <ul>
	 *            <li>CommonConstants.ACTION_UPDATE</li>
	 *            <li>CommonConstants.ACTION_INSERT</li>
	 *            </ul>
	 *            </p>
	 * @param policyName
	 *            {@link Policy} name
	 * @param policyType
	 *            {@link Policy} type
	 * @param request
	 *            - ActionRequest
	 * @param response
	 *            - ActionResponse
	 */
	@ActionMapping(params = "back")
	public void doBack(String action, String policyName, String policyType, ActionRequest request,
			ActionResponse response) {

		response.setRenderParameter("policyName", policyName);
		response.setRenderParameter("action", action);
		response.setRenderParameter("ctx", policyType + "PolicyController");

	}

}
