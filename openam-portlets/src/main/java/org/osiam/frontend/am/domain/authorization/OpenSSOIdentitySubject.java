/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.AMIdentity;
import com.sun.identity.idm.IdRepoException;

/**
 * This class encapsulates the data for an open SSO identity subject.
 */
public class OpenSSOIdentitySubject extends Subject {

	private static final long serialVersionUID = 1L;

	/**
	 * Subject type string code.
	 */
	public static final String TYPE = "AMIdentitySubject";

	private static final String VALUES = "Values";

	private static final Logger LOGGER = Logger.getLogger(OpenSSOIdentitySubject.class);

	private List<AMIdentity> values = new ArrayList<AMIdentity>();

	/**
	 * Creates a new subject with the given name.
	 * 
	 * @param name
	 *            - the subject name.
	 */
	public OpenSSOIdentitySubject(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new subject.
	 */
	public OpenSSOIdentitySubject() {

	}

	/**
	 * Creates a new subject with the given name by parsing the given XML node.
	 * 
	 * @param token
	 *            - the SSO Token.
	 * @param name
	 *            - the condition name.
	 * @param node
	 *            - the XML node to parse.
	 * @throws IdRepoException
	 *             - if the AMIdentity cannot be created (e.g. wrong input
	 *             data).
	 */
	public OpenSSOIdentitySubject(SSOToken token, String name, Node node) throws IdRepoException {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("XML contains %d attributes", attributesValueNodes.size()));
		}

		values = new ArrayList<AMIdentity>();

		for (Node n : attributesValueNodes) {
			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);
			if (attributeValuePair.getName().equals(VALUES)) {

				for (String dn : attributeValuePair.getValueList()) {
					if (!"null".equals(dn)) {
						values.add(new AMIdentity(token, dn));
					}

				}

			}
		}
	}

	public List<AMIdentity> getValues() {
		return values;
	}

	public void setValues(List<AMIdentity> values) {
		this.values = values;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		// XmlUtil.appendAttributeValuePairToString(buffer, VALUES, values);

		buffer.append("<AttributeValuePair>");
		buffer.append("<Attribute ");
		buffer.append("name=\"" + VALUES + "\"/>");

		for (int i = 0; i < values.size(); i++) {
			if (values.get(i).getUniversalId() != null && values.get(i).getUniversalId().length() > 0) {
				buffer.append("<Value>");
				buffer.append(values.get(i).getUniversalId());
				buffer.append("</Value>");
			}
		}
		buffer.append("</AttributeValuePair>");
	}

	@Override
	public String getType() {
		return TYPE;
	}

}
