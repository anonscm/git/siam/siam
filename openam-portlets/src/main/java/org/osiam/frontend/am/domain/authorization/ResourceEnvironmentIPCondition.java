/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.am.domain.authorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osiam.frontend.util.AttributeValuePair;
import org.osiam.frontend.util.XmlUtil;
import org.w3c.dom.Node;

/**
 * This class encapsulates the data for Resource Environment Ip Address
 * Condition.
 */

public class ResourceEnvironmentIPCondition extends Condition {
	private static final Logger LOGGER = Logger.getLogger(ResourceEnvironmentIPCondition.class);

	private static final long serialVersionUID = 1L;
	private static final String RESOURCE_ENV_IP_CONDITION_VALUE = "resourceEnvIPConditionValue";

	/**
	 * The type string constant for this type of condition.
	 */
	public static final String TYPE = "ResourceEnvIPCondition";

	private List<String> conditionList = new ArrayList<String>();

	/**
	 * Creates a new condition with the given name by parsing the given XML
	 * node.
	 * 
	 * @param name
	 *            - the name.
	 * @param node
	 *            - the XML node to parse.
	 */
	public ResourceEnvironmentIPCondition(final String name, final Node node) {
		super(name, TYPE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Read from xml node");
		}

		Set<Node> attributesValueNodes = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_VALUE_PAIR_NODE);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Condition contains %d attributes", attributesValueNodes.size()));
		}

		for (Node n : attributesValueNodes) {

			AttributeValuePair attributeValuePair = XmlUtil.getAttributeValuePair(n);

			if (attributeValuePair.getName().equals(RESOURCE_ENV_IP_CONDITION_VALUE)) {
				List<String> values = attributeValuePair.getValueList();
				conditionList = new ArrayList<String>(values.size());
				conditionList.addAll(values);
			}
		}
	}

	/**
	 * Creates a new condition with the given name .
	 * 
	 * @param name
	 *            - the name.
	 */
	public ResourceEnvironmentIPCondition(String name) {
		super(name, TYPE);
	}

	/**
	 * Creates a new condition.
	 */
	public ResourceEnvironmentIPCondition() {

	}

	/**
	 * @return environment condition value list e.g. IF IP=[127.0.0.1] THEN
	 *         module=LDAP.
	 * 
	 */
	public List<String> getConditionList() {
		return conditionList;
	}

	public void setConditionList(List<String> conditionList) {
		this.conditionList = conditionList;
	}

	@Override
	public String getType() {
		return TYPE;
	}

	@Override
	public void appendInnerXML(StringBuilder buffer) {
		XmlUtil.appendAttributeValuesPairToString(buffer, RESOURCE_ENV_IP_CONDITION_VALUE, conditionList);
	}

}
