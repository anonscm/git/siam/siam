/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.debug;

import java.io.Reader;
import java.io.Writer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Utility class for xml serialization. Usefull for logging complex objects.
 * 
 */
public final class XMLDebug {

	/**
	 * XMLDebug constructor.
	 */
	private XMLDebug() {
	}

	/**
	 * Internal XStream instance.
	 */
	private static final XStream XSTREAM = init();

	/**
	 * Serialize an object into a xml string.
	 * 
	 * @param o
	 *            input object
	 * @return xml string
	 */
	public static String toXML(Object o) {
		return XSTREAM.toXML(o);
	}

	/**
	 * Serialize an object to the given Writer.
	 * 
	 * @param o
	 *            - input object
	 * @param out
	 *            - Writer
	 * 
	 */
	public static void toXML(Object o, Writer out) {
		XSTREAM.toXML(o, out);
	}

	/**
	 * Deserialize an object from an xml string.
	 * 
	 * @param xml
	 *            input string
	 * @return the deserialized object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T fromXML(String xml) {
		if (xml == null) {
			return null;
		}
		return (T) XSTREAM.fromXML(xml);
	}

	/**
	 * Deserialize an object from an xml string.
	 * 
	 * @param reader
	 *            the reader to read from
	 * @return the deserialized object
	 */
	public static Object fromXML(Reader reader) {
		return XSTREAM.fromXML(reader);
	}

	/**
	 * Print to console an object in xml form.
	 * 
	 * @param o
	 *            the object to print.
	 */
	public static void printXML(Object o) {
		System.out.println(toXML(o));
	}

	/**
	 * Initialize the XStream object. Put here any customization required. For
	 * example add new Converters.
	 * 
	 * @return
	 */
	private static XStream init() {
		XStream x = new XStream(new DomDriver());
		return x;
	}
}
