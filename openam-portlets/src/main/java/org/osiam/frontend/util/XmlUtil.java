/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.osiam.frontend.configuration.service.FederationConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.sun.identity.shared.xml.XMLHandler;

/**
 * This class provides methods for creating objects from XML and persisting
 * objects to XML.
 */
public final class XmlUtil {

	private XmlUtil() {

	}

	private static final Logger LOGGER = Logger.getLogger(XmlUtil.class);
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String ATTRIBUTE_VALUE_PAIR_NODE = "AttributeValuePair";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String ATTRIBUTE_NODE = "Attribute";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String VALUE_NODE = "Value";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String NAME_ATTRIBUTE = "name";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String TYPE_ATTRIBUTE = "type";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String INCLUDE_TYPE_ATTRIBUTE = "includeType";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String TYPE_ATTRIBUTE_RULE = "ServiceName";
	/**
	 * This constant is used a key when persisting object to XML.
	 */
	public static final String RESOURCE_NAME = "ResourceName";

	/**
	 * @return {@link Document} created based on the given XML string.
	 * 
	 * @param xmlString
	 *            - the XML {@link String}
	 * @throws UnsupportedEncodingException
	 *             - if the encoding is not supported.
	 */
	public static Document toDOMDocument(String xmlString) throws UnsupportedEncodingException {

		if ((xmlString == null) || (xmlString.length() == 0)) {
			return null;
		}

		try {
			ByteArrayInputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
			return toDOMDocument(is);
		} catch (UnsupportedEncodingException uee) {

			LOGGER.error("Can't parse the XML document:\n" + xmlString, uee);

			throw uee;

		}
	}

	/**
	 * Converts the XML document from an input stream to DOM Document format.
	 * 
	 * @param is
	 *            - the InputStream that contains XML document
	 * @return Document is the DOM object obtained by parsing the input stream.
	 *         Returns null if there are any parser errores.
	 */
	public static Document toDOMDocument(InputStream is) {
		/*
		 * Constructing a DocumentBuilderFactory for every call is less
		 * expensive than a synchronizing a single instance of the factory and
		 * obtaining the builder
		 */
		DocumentBuilderFactory dbFactory = null;
		try {
			// Assign new debug object
			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setValidating(false);
			dbFactory.setNamespaceAware(false);
		} catch (Exception e) {
			LOGGER.error("DocumentBuilder init failed", e);

		}

		try {
			DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();

			if (documentBuilder == null) {
				LOGGER.error("XMLUtils.toDOM : null builder instance");
				return null;
			}
			documentBuilder.setEntityResolver(new XMLHandler());
			// documentBuilder.setErrorHandler(eh)
			// if (debug != null && debug.warningEnabled()) {
			// documentBuilder.setErrorHandler(new
			// ValidationErrorHandler(debug));
			// }

			return documentBuilder.parse(is);
		} catch (Exception e) {
			// Since there may potentially be several invalid XML documents
			// that are mostly client-side errors, only a warning is logged for
			// efficiency reasons.

			LOGGER.error("Can't parse the XML document", e);

		}

		return null;

	}

	/**
	 * Creates an emtpy DOM document.
	 * 
	 * @return empty DOM document.
	 * 
	 */
	public static Document createEmptyDOMDocument() {
		DocumentBuilderFactory dbFactory = null;
		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setValidating(false);
			dbFactory.setNamespaceAware(false);
		} catch (Exception e) {
			LOGGER.error("DocumentBuilder init failed", e);
		}

		try {
			DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
			if (documentBuilder == null) {
				LOGGER.error("XMLUtils.toDOM : null builder instance");
				return null;
			}
			documentBuilder.setEntityResolver(new XMLHandler());
			Document newDocument = documentBuilder.newDocument();

			return newDocument;
		} catch (Exception e) {
			// Since there may potentially be several invalid XML documents
			// that are mostly client-side errors, only a warning is logged for
			// efficiency reasons.

			LOGGER.error("Can't create new XML document", e);

		}

		return null;

	}

	/**
	 * @return {@link String} value for the given attribute name of the given
	 *         {@link Node}
	 * @param node
	 *            - the {@link Node}
	 * @param attrName
	 *            - the attribute name
	 */
	public static String getNodeAttributeValue(Node node, String attrName) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs == null) {
			return (null);
		}
		Node value = attrs.getNamedItem(attrName);
		if (value == null) {
			return (null);
		}
		return (value.getNodeValue());
	}

	/**
	 * @param node
	 *            - the {@link Node}
	 * @param attrName
	 *            - the attribute name
	 * @return value for the given attribute name of the given {@link Node}
	 */
	public static Boolean getNodeAttributeBooleanValue(Node node, String attrName) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs == null) {
			return false;
		}
		Node value = attrs.getNamedItem(attrName);
		if (value == null) {
			return false;
		}

		if ("true".equals(value.getNodeValue())) {
			return true;
		}
		return false;
	}

	/**
	 * @param node
	 *            - the {@link Node}
	 * @param attrName
	 *            - the attribute name
	 * @return value for the given attribute name of the given {@link Node}
	 */
	public static Long getNodeAttributeLongValue(Node node, String attrName) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs == null) {
			return (null);
		}
		Node value = attrs.getNamedItem(attrName);
		if (value == null) {
			return (null);
		}
		return new Long(value.getNodeValue());
	}

	/**
	 * @return {@link Set} of {@link Node} containing all child {@link Node}
	 *         with given name for given parent {@link Node}.
	 * @param parentNode
	 *            - the parent {@link Node}
	 * @param childName
	 *            - the child name
	 */
	public static Set<Node> getChildNodes(Node parentNode, String childName) {
		Set<Node> retVal = new HashSet<Node>();
		NodeList children = parentNode.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (node.getNodeName().equalsIgnoreCase(childName)) {
				retVal.add(node);
			}
		}
		return (retVal);
	}

	/**
	 * @return whether parent {@link Node} has a child with given name
	 * 
	 * @param parentNode
	 *            - the parent {@link Node}
	 * @param childName
	 *            - the child name
	 */
	public static boolean childNodeExists(Node parentNode, String childName) {
		Set<Node> children = getChildNodes(parentNode, childName);
		return (children.size() > 0);
	}

	/**
	 * @return {@link Node} containing first child {@link Node} for given parent
	 *         {@link Node}.
	 * @param parentNode
	 *            - the parent {@link Node}
	 */
	public static Node getFirstChildNode(Node parentNode) {

		NodeList children = parentNode.getChildNodes();
		return children.item(0);

	}

	/**
	 * @return {@link Node} containing child {@link Node} with given name for
	 *         given parent {@link Node}.
	 * @param parentNode
	 *            - the parent {@link Node}
	 * @param childName
	 *            - the child name
	 */
	public static Node getChildNode(Node parentNode, String childName) {

		if (childName == null) {
			throw new NullPointerException("The child name is null");
		}

		if (childName.length() == 0) {
			throw new IllegalArgumentException("The child name is an empty string ");
		}

		NodeList children = parentNode.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (node.getNodeName().equalsIgnoreCase(childName)) {
				return node;
			}
		}

		throw new IllegalArgumentException(String.format("The node doesn't contains a child with name %s", childName));
	}
	
	/**
	 * @return {@link Node} containing child {@link Node} with given name for
	 *         given parent {@link Node}.
	 * @param parentNode
	 *            - the parent {@link Node}
	 * @param childName
	 *            - the child name
	 */
	public static Node getChildNodeWithPrefix(Node parentNode, String childName) {

		if (childName == null) {
			throw new NullPointerException("The child name is null");
		}

		if (childName.length() == 0) {
			throw new IllegalArgumentException("The child name is an empty string ");
		}

		NodeList children = parentNode.getChildNodes();

		String criteria = childName;

		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			
			int index=node.getNodeName().indexOf(":");
			
			if (index!=-1) {
				criteria = node.getNodeName().substring(0, index) + ":" + childName;
			} else {
				criteria = childName;
			}
			if (node.getNodeName().equalsIgnoreCase(criteria)) {
				return node;
			}
		}

		throw new IllegalArgumentException(String.format("The node doesn't contains a child with name %s", childName));
	}

	/**
	 * 
	 * Read an AttributePair node and return an AttributeValuePair.
	 * 
	 * 
	 * 
	 * @return {@link AttributeValuePair} by parsing the given {@link Node}.
	 * @param node
	 *            - the XML {@link Node}
	 */
	public static AttributeValuePair getAttributeValuePair(Node node) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("AttributeValue xml : %s", getString(node)));
		}

		Set<Node> attributeNodeSet = XmlUtil.getChildNodes(node, XmlUtil.ATTRIBUTE_NODE);
		if (attributeNodeSet.size() != 1) {
			String error = String
					.format("Illegal number of attribute nodes in attributeValuePair node. Expected 1 but the actual number is %d",
							attributeNodeSet.size());
			LOGGER.error(error);
			throw new IllegalArgumentException(error);
		}

		Node attributeNode = attributeNodeSet.iterator().next();
		String name = getNodeAttributeValue(attributeNode, NAME_ATTRIBUTE);

		Set<Node> valueNodeSet = XmlUtil.getChildNodes(node, XmlUtil.VALUE_NODE);
		if (valueNodeSet.size() == 0) {
			return new AttributeValuePair(name, new ArrayList<String>());
		} else {
			List<String> valueList = new ArrayList<String>();
			for (Node valueNode : valueNodeSet) {

				NodeList valueNodeList = valueNode.getChildNodes();

				if (valueNodeList.getLength() != 1) {
					String error = String.format(
							"Illegal number of text nodes in value node. Expected 1 but the actual number is %d",
							valueNodeList.getLength());
					LOGGER.error(error);
					throw new IllegalArgumentException(error);
				}

				String value = valueNodeList.item(0).getNodeValue().trim();
				valueList.add(value);

			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute value pair with name %s and value %s", name, valueList));
			}

			return new AttributeValuePair(name, valueList);
		}

	}

	/**
	 * Read an Attribute node and return an AttributeValuePair. AttributeNode:
	 * <code>
	 * <Attribute name="idpAttributeMapper">
	 * <Value>com.sun.identity.saml2.plugins.DefaultIDPAttributeMapper</Value>
	 * </Attribute>
	 * </code>
	 * 
	 * 
	 * @return {@link AttributeValuePair} by parsing the given {@link Node}.
	 * @param node
	 *            - the XML {@link Node}
	 */
	public static AttributeValuePair getAttributeValue(Node node) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("Attribute xml : %s", getString(node)));
		}

		String name = getNodeAttributeValue(node, NAME_ATTRIBUTE);

		Set<Node> valueNodeSet = XmlUtil.getChildNodes(node, XmlUtil.VALUE_NODE);
		if (valueNodeSet.size() == 0) {
			return new AttributeValuePair(name, new ArrayList<String>());
		} else {
			List<String> valueList = new ArrayList<String>();
			for (Node valueNode : valueNodeSet) {

				NodeList valueNodeList = valueNode.getChildNodes();

				if (valueNodeList.getLength() != 0) {
					String value = valueNodeList.item(0).getNodeValue().trim();
					valueList.add(value);
				}

			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(String.format("Read attribute value pair with name %s and value %s", name, valueList));
			}

			return new AttributeValuePair(name, valueList);
		}

	}

	/**
	 * @return - value from given node.
	 * @param node
	 *            - the {@link Node}.
	 * 
	 */
	public static String getValueFromAttributeValue(Node node) {

		AttributeValuePair pair = getAttributeValue(node);
		if (pair.getValueList() != null && pair.getValueList().size() != 0) {

			return pair.getValueList().get(0);
		}

		return null;

	}

	/**
	 * @return - text value from given node
	 * @param node
	 *            - the {@link Node}
	 * 
	 */
	public static Boolean getBooleanValueFromAttributeValue(Node node) {

		AttributeValuePair pair = getAttributeValue(node);
		if (pair.getValueList() != null && pair.getValueList().size() != 0) {
			if ("true".equals(pair.getValueList().get(0))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param node
	 *            - the {@link Node}
	 * @return text value from given node
	 */
	public static Long getLongValueFromAttributeValue(Node node) {

		AttributeValuePair pair = getAttributeValue(node);
		if (pair.getValueList() != null && pair.getValueList().size() != 0) {
			return new Long(pair.getValueList().get(0));
		}
		return new Long(0);
	}

	/**
	 * @param node
	 *            - the {@link Node}
	 * @return text value from given node
	 */
	public static Integer getIntegerValueFromAttributeValue(Node node) {

		AttributeValuePair pair = getAttributeValue(node);
		if (pair.getValueList() != null && pair.getValueList().size() != 0) {
			return new Integer(pair.getValueList().get(0));
		}
		return new Integer(0);
	}

	/**
	 * @return - text value from given node
	 * @param node
	 *            - the {@link Node}
	 * 
	 */
	public static String getTextValueFromNode(Node node) {

		NodeList childList = node.getChildNodes();

		for (int i = 0; i < childList.getLength(); i++) {
			Node child = childList.item(i);

			if (child.getNodeType() == Node.TEXT_NODE) {
				return child.getNodeValue();
			}

		}

		return null;

	}

	/**
	 * @return {@link String} by parsing given {@link Node}
	 * @param node
	 *            - the XML {@link Node}
	 */
	public static String getString(Node node) {
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			// trans.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(node);
			trans.transform(source, result);
			String xmlString = sw.toString();

			return xmlString;
		} catch (Exception e) {
			LOGGER.error(e);
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}

	}

	/**
	 * Appends XML string to given {@link StringBuilder}, based on given name
	 * and value.
	 * 
	 * @param buffer
	 *            - the {@link StringBuilder} to append to
	 * @param name
	 *            - the {@link AttributeValuePair} name
	 * @param value
	 *            - the {@link AttributeValuePair} value
	 */
	public static void appendAttributeValuePairToString(StringBuilder buffer, String name, String value) {
		buffer.append("<AttributeValuePair>");
		buffer.append("<Attribute ");
		buffer.append("name=\"" + name + "\"/>");
		buffer.append("<Value>");
		buffer.append(value);
		buffer.append("</Value>");
		buffer.append("</AttributeValuePair>");

	}

	/**
	 * Appends XML string to given {@link StringBuilder}, based on list of
	 * values.
	 * 
	 * Create <Value>...</Value> that contains all the values from list.
	 * 
	 * @param buffer
	 *            - the {@link StringBuilder} to append to
	 * @param values
	 *            - the list of {@link AttributeValuePair} values to parse
	 * @param name
	 *            - the {@link AttributeValuePair} name
	 */
	public static void appendAttributeValuePairToString(StringBuilder buffer, String name, List<String> values) {
		buffer.append("<AttributeValuePair>");
		buffer.append("<Attribute ");
		buffer.append("name=\"" + name + "\"/>");
		if (values.size() > 0) {
			buffer.append("<Value>");
		}
		for (int i = 0; i < values.size(); i++) {
			buffer.append(values.get(i));
			if (i < values.size() - 1) {
				buffer.append(" ");
			}
		}
		if (values.size() > 0) {
			buffer.append("</Value>");
		}

		buffer.append("</AttributeValuePair>");

	}

	/**
	 * Appends XML string to given {@link StringBuilder}, based on list of
	 * values.
	 * 
	 * Create <Value>...</Value> for each value from list.
	 * 
	 * @param buffer
	 *            - the {@link StringBuilder} to append to
	 * @param values
	 *            - the list of {@link AttributeValuePair} values to parse
	 * @param name
	 *            - the {@link AttributeValuePair} name
	 */
	public static void appendAttributeValuesPairToString(StringBuilder buffer, String name, List<String> values) {
		buffer.append("<AttributeValuePair>");
		buffer.append("<Attribute ");
		buffer.append("name=\"" + name + "\"/>");

		for (int i = 0; i < values.size(); i++) {
			buffer.append("<Value>");
			buffer.append(values.get(i));
			buffer.append("</Value>");
		}
		buffer.append("</AttributeValuePair>");

	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param tagName
	 *            - the tag name.
	 * @param attributes
	 *            - the node attributes.
	 * @return - the new {@link Element}
	 */
	public static Element createElement(Node node, String tagName, Map<String, String> attributes) {
		return createElement(node, tagName, null, attributes);
	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param tagName
	 *            - the tag name.
	 * @param tagNamespace
	 *            - the namespace.
	 * @param attributes
	 *            the node attributes.
	 * @return - the new {@link Element}.
	 */
	public static Element createElement(Node node, String tagName, String tagNamespace, Map<String, String> attributes) {
		Element element = null;
		Document parentDocument = node.getOwnerDocument();
		if (parentDocument == null) {
			parentDocument = (Document) node;
		}
		if (tagNamespace != null) {
			element = parentDocument.createElementNS(tagNamespace, tagName);
		} else {
			element = parentDocument.createElement(tagName);
		}
		if (attributes != null) {
			Iterator<String> attributesIterator = attributes.keySet().iterator();
			while (attributesIterator.hasNext()) {
				String attributeName = attributesIterator.next();
				element.setAttribute(attributeName, attributes.get(attributeName));
			}
		}
		node.appendChild(element);
		return element;
	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param tagName
	 *            - the tag name.
	 * @return - the new {@link Element}.
	 */
	public static Element createElement(Node node, String tagName) {
		return createElement(node, tagName, null);
	}

	/**
	 * Creates a new text {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param value
	 *            - the node text value.
	 * @return - the new {@link Element}.
	 */
	public static Element createTextElement(Node parentNode, String tag, String value) {

		Element valueElement = createElement(parentNode, tag);

		if (value == null) {
			return valueElement;
		}
		Text element = parentNode.getOwnerDocument().createTextNode(value);
		valueElement.appendChild(element);
		return valueElement;
	}

	/**
	 * Creates a new text {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param value
	 *            - the node text value as {@link Integer}.
	 * @return - the new {@link Element}.
	 */
	public static Element createTextElement(Node parentNode, String tag, Integer value) {

		Element valueElement = createElement(parentNode, tag);

		if (value == null) {
			return valueElement;
		}
		Text element = parentNode.getOwnerDocument().createTextNode(value.toString());
		valueElement.appendChild(element);
		return valueElement;
	}

	/**
	 * Creates a new text {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param value
	 *            - the node text Valid
	 * @param attributes
	 *            - the node attributes.
	 * @return - the new {@link Element}.
	 */
	public static Element createTextElement(Node parentNode, String tag, String namespace, String value,
			Map<String, String> attributes) {

		Element valueElement = createElement(parentNode, tag, namespace, null);

		if (value == null) {
			return valueElement;
		}
		Text element = parentNode.getOwnerDocument().createTextNode(value);
		if (attributes != null) {
			Iterator<String> attributesIterator = attributes.keySet().iterator();
			while (attributesIterator.hasNext()) {
				String attributeName = attributesIterator.next();
				valueElement.setAttribute(attributeName, attributes.get(attributeName));
			}
		}
		valueElement.appendChild(element);
		return valueElement;
	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param value
	 *            - the element text value.
	 * @return - the new {@link Element}.
	 */
	public static Element createTextElement(Node parentNode, String tag, Boolean value) {

		Element valueElement = createElement(parentNode, tag);

		if (value == null) {
			return valueElement;
		}
		Text element = parentNode.getOwnerDocument().createTextNode(value.toString());
		valueElement.appendChild(element);
		return valueElement;
	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param value
	 *            - the {@link Element} text value.
	 * @return - the new {@link Element}.
	 */
	public static Element createTextElement(Node parentNode, String tag, Long value) {

		Element valueElement = createElement(parentNode, tag);

		if (value == null) {
			return valueElement;
		}
		Text element = parentNode.getOwnerDocument().createTextNode(value.toString());
		valueElement.appendChild(element);
		return valueElement;
	}

	/**
	 * Creates a new {@link Element} of attribute/value type.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param name
	 *            - the attribute name.
	 * @param value
	 *            -the attribute value.
	 * @return - the new {@link Element}.
	 */
	public static Element createAttributeValue(Node node, String name, Long value) {
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("name", name);
		Element attributeElement = createElement(node, "Attribute", attributesMap);
		createTextElement(attributeElement, "Value", value);

		return attributeElement;
	}

	/**
	 * Creates a new {@link Element} of attribute/value type.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param name
	 *            - the attribute name.
	 * @param value
	 *            -the attribute value.
	 * @return - the new {@link Element}.
	 */
	public static Element createAttributeValue(Node node, String name, Integer value) {
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("name", name);
		Element attributeElement = createElement(node, "Attribute", attributesMap);
		createTextElement(attributeElement, "Value", value);

		return attributeElement;
	}

	/**
	 * Creates a new {@link Element} of attribute/value type.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param name
	 *            - the attribute name.
	 * @param value
	 *            -the attribute value.
	 * @return - the new {@link Element}.
	 */
	public static Element createAttributeValue(Node node, String name, String value) {
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("name", name);
		Element attributeElement = createElement(node, "Attribute", attributesMap);
		createTextElement(attributeElement, "Value", value);

		return attributeElement;
	}

	/**
	 * Prefixes a string with slash if not starts already with slash.
	 * 
	 * @param s
	 *            - the string.
	 * @return the string prefixed with slash.
	 */
	public static String prefixWithSlash(String s) {
		if (s.startsWith("/")) {
			return s;
		} else {
			return "/" + s;
		}
	}

	/**
	 * Creates a new {@link Element} of attribute/value type.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param name
	 *            - the attribute name.
	 * @param value
	 *            -the attribute value.
	 * @return - the new {@link Element}.
	 */
	public static Element createAttributeValue(Node node, String name, Boolean value) {
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("name", name);
		Element attributeElement = createElement(node, "Attribute", attributesMap);
		createTextElement(attributeElement, "Value", value);

		return attributeElement;
	}

	/**
	 * Creates a new {@link Element} of attribute/value type.
	 * 
	 * @param node
	 *            - the parent node.
	 * @param name
	 *            - the attribute name.
	 * @param valueList
	 *            -the attribute values.
	 * @return - the new {@link Element}.
	 */
	public static Element createAttributeValue(Node node, String name, List<String> valueList) {
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("name", name);
		Element attributeElement = createElement(node, "Attribute", attributesMap);
		createListElement(attributeElement, "Value", valueList);

		return attributeElement;
	}

	/**
	 * Creates a new {@link Element}.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param name
	 *            - the tag name.
	 * @param valueList
	 *            - the list of values.
	 */
	public static void createListElement(Node parentNode, String name, List<String> valueList) {
		for (String value : valueList) {
			createTextElement(parentNode, name, value);
		}

	}

	/**
	 * Creates a new {@link Element}, binding/location type.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param binding
	 *            - the binding value.
	 * @param location
	 *            - the location value.
	 * @param extraAttributes
	 *            - extra attributes values.
	 */
	public static void createBindingLocationElement(Node parentNode, String tag, String binding, String location,
			Map<String, String> extraAttributes) {

		Map<String, String> map = new HashMap<String, String>();
		map.put(FederationConstants.BINDING_ATTRIBUTE, binding);
		map.put(FederationConstants.LOCATION_ATTRIBUTE, location);
		map.putAll(extraAttributes);

		createElement(parentNode, tag, map);
	}

	/**
	 * Creates a new {@link Element}, binding/location type.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param binding
	 *            - the binding value.
	 * @param location
	 *            - the location value.
	 */
	public static void createBindingLocationElement(Node parentNode, String tag, String binding, String location) {

		Map<String, String> map = new HashMap<String, String>();
		map.put(FederationConstants.BINDING_ATTRIBUTE, binding);
		map.put(FederationConstants.LOCATION_ATTRIBUTE, location);

		createElement(parentNode, tag, map);
	}

	/**
	 * Creates a new {@link Element}, binding/location type.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param binding
	 *            - the binding value.
	 * @param location
	 *            - the location value.
	 * @param responseLocation
	 *            - response location values.
	 */
	public static void createBindingLocationResponseElement(Node parentNode, String tag, String binding,
			String location, String responseLocation) {

		Map<String, String> map = new HashMap<String, String>();
		map.put(FederationConstants.BINDING_ATTRIBUTE, binding);
		map.put(FederationConstants.LOCATION_ATTRIBUTE, location);
		map.put(FederationConstants.RESPONSE_LOCATION_ATTRIBUTE, responseLocation);

		createElement(parentNode, tag, map);
	}

	/**
	 * Creates a new {@link Element}, binding/location type.
	 * 
	 * @param parentNode
	 *            - the parent node.
	 * @param tag
	 *            - the tag name.
	 * @param binding
	 *            - the binding value.
	 * @param location
	 *            - the location value.
	 * @param responseLocation
	 *            - response location values.
	 * @param attributes
	 *            - attributes values.
	 */
	public static void createBindingLocationResponseElement(Node parentNode, String tag, String binding,
			String location, String responseLocation, Map<String, String> attributes) {

		attributes.put(FederationConstants.BINDING_ATTRIBUTE, binding);
		attributes.put(FederationConstants.LOCATION_ATTRIBUTE, location);
		attributes.put(FederationConstants.RESPONSE_LOCATION_ATTRIBUTE, responseLocation);

		createElement(parentNode, tag, attributes);
	}
}
