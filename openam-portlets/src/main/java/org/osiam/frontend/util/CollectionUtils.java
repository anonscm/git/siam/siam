/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Encapsulates methods for dealing with data packaging as collections.
 */
public final class CollectionUtils {

	private CollectionUtils() {

	}

	/**
	 * @return list of values that are contained in the given Set.
	 * @param set
	 *            - the set of values
	 */
	public static List<String> listFromSet(Set<String> set) {
		if (set != null) {
			List<String> list = new ArrayList<String>(set.size());
			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()) {
				list.add(iterator.next());
			}
			return list;
		} else {
			return null;
		}
	}

	/**
	 * @return String value that is contained in the given Set.
	 * @param set
	 *            - the set of values
	 */
	public static String stringFromSet(Set<String> set) {
		if (set != null) {
			Iterator<String> iterator = set.iterator();
			if (iterator.hasNext()) {
				return iterator.next();
			} else {
				return "";
			}
		}
		return "";
	}

	/**
	 * @return Long value that is contained in the given Set.
	 * @param set
	 *            - the set of values
	 */
	public static Long longFromSet(Set<String> set) {
		if (set != null) {
			Iterator<String> iterator = set.iterator();
			if (iterator.hasNext()) {
				return new Long(set.iterator().next());
			} else {
				return new Long(0);
			}
		} else {
			return new Long(0);
		}
	}

	/**
	 * @return Integer value that is contained in the given Set.
	 * @param set
	 *            - the set of values
	 */
	public static Integer integerFromSet(Set<String> set) {
		if (set != null) {
			Iterator<String> iterator = set.iterator();
			if (iterator.hasNext()) {
				return new Integer(set.iterator().next());
			} else {
				return new Integer(0);
			}
		} else {
			return new Integer(0);
		}
	}

	/**
	 * @return Boolean value that is contained in the given Set.
	 * @param set
	 *            - the set of values
	 */
	public static boolean booleanFromSet(Set<String> set) {
		if ((set != null) && (set.size() > 0)) {
			String val = set.iterator().next();
			if ("true".equals(val)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return Set encapsulating the null value.
	 */
	public static Set<String> getNullSet() {

		Set<String> set1 = new HashSet<String>();
		set1.add(null);

		return set1;
	}

	/**
	 * @return Set encapsulating the given Long.
	 * @param value
	 *            - the Long value to encapsulate as Set
	 */
	public static Set<String> asSet(Long value) {
		Set<String> set1 = new HashSet<String>();
		if (value != null) {
			set1.add(value.toString());
		}
		return set1;
	}

	/**
	 * @return Set encapsulating the given Integer.
	 * @param value
	 *            - the Integer value to encapsulate as Set
	 */
	public static Set<String> asSet(Integer value) {
		Set<String> set1 = new HashSet<String>();
		if (value != null) {
			set1.add(value.toString());
		}
		return set1;
	}

	/**
	 * @return Set encapsulating the given String.
	 * @param value
	 *            - the String value to encapsulate as Set
	 */
	public static Set<String> asSet(String value) {
		if (value == null) {
			// throw new IllegalArgumentException("Value cannot be null.");
			value = "";
		}

		Set<String> set1 = new HashSet<String>();
		set1.add(value);
		return set1;
	}

	/**
	 * @return Set encapsulating the given List.
	 * @param list
	 *            - the List value to encapsulate as Set
	 */
	public static Set<String> asSet(List<String> list) {
		if (list == null) {
			// throw new IllegalArgumentException("Value cannot be null.");
			list = new ArrayList<String>();
		}
		Set<String> set1 = new HashSet<String>();
		for (int i = 0; i < list.size(); i++) {
			set1.add(list.get(i));
		}
		return set1;
	}

	/**
	 * @return Set encapsulating the given List.
	 * @param list
	 *            - the List value to encapsulate as Set
	 */
	public static Set<String> asSetWithSpecialEmptyList(List<String> list) {
		boolean isSet = false;
		Set<String> returnedSet = null;
		if ((list != null) && (list.size() == 0)) {
			returnedSet = asSet(Arrays.asList("[]="));
			isSet = true;
		}
		if (!isSet) {
			returnedSet = asSet(list);
		}
		return returnedSet;
	}

	/**
	 * @return Set encapsulating the given List.
	 * @param values
	 *            - the list to encapsulate as Set
	 * @param validValues
	 *            - the list of valid values.
	 * @param errorKey
	 *            - error key
	 * @throws IllegalArgumentException
	 *             - if one of the list values is not in the validValues
	 *             collection.
	 */
	public static Set<String> asSetWithValidation(List<String> values, Collection<String> validValues, String errorKey)
			throws IllegalArgumentException {

		if (values != null) {
			for (int i = 0; i < values.size(); i++) {
				String value = values.get(i);
				int equalIndex = value.indexOf("=");
				if (equalIndex > -1) {
					value = value.substring(equalIndex + 1);
				}
				if ((!"".equals(value)) && !validValues.contains(value)) {
					// throw new IllegalArgumentException(String.format(
					// "Illegal value received (->> %s). Should be one of the : {%s}",
					// value, validValues));
					throw new IllegalArgumentException(errorKey);
				}
			}
		} else {
			return null;
		}
		return asSet(values);
	}

	/**
	 * @return Set encapsulating the given List.
	 * @param value
	 *            - the value to encapsulate as Set
	 * @param validValues
	 *            - the list of valid values.
	 * @param errorKey
	 *            - error key
	 * @throws IllegalArgumentException
	 *             - if the value is not in the validValues collection.
	 */
	public static Set<String> asSetWithValidation(String value, Collection<String> validValues, String errorKey)
			throws IllegalArgumentException {

		if ((value != null) && !"".equals(value)) {
			if (!validValues.contains(value)) {
				// throw new
				// IllegalArgumentException(String.format("Illegal value received (->> %s).",
				// value));
				throw new IllegalArgumentException(errorKey);
			} else {
				return asSet(value);
			}
		}
		return null;
	}

	/**
	 * @return Set encapsulating the given Boolean.
	 * @param value
	 *            - the Boolean value to encapsulate as Set
	 */
	public static Set<String> asSet(boolean value) {
		Set<String> set1 = new HashSet<String>();
		// set1.add("[" + Boolean.toString(value) + "]");
		set1.add(Boolean.toString(value));
		return set1;
	}
}
