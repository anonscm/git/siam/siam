/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.frontend.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * This class is taking care to remove or add the prefixes or suffixes for
 * parameters that are prefixed with certain constant values when persisted. An
 * example of such parameter value is 'agentRootURL=http://www.opensso:9090/'.
 * This class provides helper methods which removes the 'agentRootURL=' string
 * when reading the object for UI, and adds this string when persisting it.
 * 
 */

public final class AttributeUtils {

	private AttributeUtils() {

	}

	/**
	 * 
	 * Adds the prefix for certain fields that need that before being persisted.
	 * 
	 * An example of such field value is
	 * 'agentRootURL=http://www.opensso:9090/'. Current method adds
	 * 'agentRootURL=' as prefix for all entries in the given list.
	 * 
	 * @return the processed list of values.
	 * 
	 * @param list
	 *            - the list of values to be processed
	 * @param prefix
	 *            - the prefix to be added
	 * 
	 */
	public static List<String> addPrefix(List<String> list, String prefix) {
		if (list == null) {
			return Arrays.asList(prefix);
		}
		if (list.size() == 0) {
			return Arrays.asList(prefix);
		}
		List<String> processedList = new ArrayList<String>(list.size());
		for (int i = 0; i < list.size(); i++) {
			String value = list.get(i);
			if (!value.startsWith(prefix)) {
				processedList.add(prefix + value);
			}
		}
		return processedList;
	}

	/**
	 * 
	 * Adds the prefix for certain fields that need that before being persisted.
	 * 
	 * An example of such field value is
	 * 'agentRootURL=http://www.opensso:9090/'. Current method adds
	 * 'agentRootURL=' as prefix for all entries in the given list.
	 * 
	 * @return the processed list of values.
	 * 
	 * @param list
	 *            - the list of values to be processed
	 * @param prefix
	 *            - the prefix to be added
	 * 
	 */
	public static List<String> addPrefixIfEmpty(List<String> list, String prefix) {
		if (list == null) {
			return Arrays.asList(prefix);
		}
		if (list.size() == 0) {
			return Arrays.asList(prefix);
		}

		return list;
	}

	/**
	 * 
	 * Adds the index in the list as prefix for certain fields that need that
	 * before being persisted.
	 * 
	 * @return the processed list of values.
	 * 
	 * @param list
	 *            - the list of values to be processed
	 * 
	 */
	public static List<String> addIndexPrefix(List<String> list) {
		if (list == null) {
			return Arrays.asList("[0]=");
		}
		if (list.size() == 0) {
			return Arrays.asList("[0]=");
		}
		List<String> processedList = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			processedList.add("[" + Integer.toString(i) + "]=" + list.get(i));
		}
		return processedList;
	}

	private static List<String> processSpecialCase(List<String> list, String prefix) {
		if ((list != null) && (list.size() == 1) && ("[]=".equals(list.get(0)))) {
			return new ArrayList<String>();
		}
		return null;
	}

	/**
	 * 
	 * Removes the prefix for certain fields that need that before displayed in
	 * the UI. An example of such field value is
	 * 'agentRootURL=http://www.opensso:9090/'. Current method removes
	 * 'agentRootURL=' as prefix for all entries in the given list and also
	 * removes any empty values in the given list.
	 * 
	 * @return the processed list of values.
	 * 
	 * @param list
	 *            - the list of values to be processed
	 * @param prefix
	 *            - the prefix to be removed
	 * 
	 */
	public static List<String> removePrefix(List<String> list, String prefix) {
		if (list == null) {
			return null;
		}
		List<String> specialCaseList = processSpecialCase(list, prefix);
		if (specialCaseList != null) {
			return specialCaseList;
		}
		List<String> processedList = new ArrayList<String>();
		String correctedPrefix;
		correctedPrefix = prefix.replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]");
		for (int i = 0; i < list.size(); i++) {
			processedList.add(list.get(i).replaceFirst(correctedPrefix, ""));
		}
		int index = 0;
		while (index < processedList.size()) {
			if ("".equals(processedList.get(index))) {
				processedList.remove(index);
			} else {
				index++;
			}
		}

		return processedList;
	}

	/**
	 * 
	 * Removes the prefix for certain fields that need that before displayed in
	 * the UI. An example of such field value is
	 * 'agentRootURL=http://www.opensso:9090/'. Current method removes
	 * 'agentRootURL=' as prefix for all entries in the given list and also
	 * removes any empty values in the given list.
	 * 
	 * @return the processed list of values.
	 * 
	 * @param list
	 *            - the list of values to be processed
	 * 
	 * 
	 */
	public static List<String> removeIndexPrefix(List<String> list) {
		if (list == null) {
			return null;
		}
		List<String> processedList = new ArrayList<String>(list.size());
		processedList.addAll(list);

		for (int i = 0; i < list.size(); i++) {
			String value = list.get(i);
			if (value.startsWith("[")) {
				int index = Integer.parseInt(value.substring(1, value.indexOf("]")));
				processedList.set(index, value.substring(value.indexOf("]") + 2));
			} else {
				processedList.set(i, value);
			}
		}

		int index = 0;
		while (index < processedList.size()) {
			if ("".equals(processedList.get(index))) {
				processedList.remove(index);
			} else {
				index++;
			}
		}

		return processedList;
	}
}
