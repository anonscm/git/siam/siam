/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the License). You may not use this file except in
 * compliance with the License.
 *
 * You can obtain a copy of the License at
 * http://www.sun.com/cddl/cddl.html
 * See the License for the specific language governing
 * permission and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * Header Notice in each file and include the License file
 * at src/main/recources/license/license.txt.
 * If applicable, add the following below the CDDL Header,
 * with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 */

package org.osiam.xacml.decryption;

import java.io.StringWriter;
import java.security.Key;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.identity.saml.xmlsig.KeyProvider;
import com.sun.identity.saml2.common.SAML2Constants;
import com.sun.identity.saml2.common.SAML2Exception;
import com.sun.identity.saml2.common.SAML2Utils;
import com.sun.identity.saml2.key.KeyUtil;
import com.sun.identity.saml2.soapbinding.QueryHandlerServlet;
import com.sun.identity.saml2.xmlenc.EncManager;



@SuppressWarnings("serial")
public class QueryHandlerDecryptionServlet extends QueryHandlerServlet{
	
	private static final String ENC_NAMESPACE = "http://www.w3.org/2001/04/xmlenc#";
	private static final String ENC_NODE_NAME = "EncryptedData";
	
	private static Logger logger = Logger.getLogger(QueryHandlerDecryptionServlet.class.getName());
	private Transformer transformer = null;

	
	/**
	 * Extends onMessage() method to deal with encrypted requests
	 */
	public SOAPMessage onMessage(SOAPMessage soapMsg, HttpServletRequest request, HttpServletResponse response, String realm,
            String pdpEntityID) throws SOAPException {
        
		
		boolean pdpWantAuthzQuerySigned =
            SAML2Utils.getWantXACMLAuthzDecisionQuerySigned(realm,
            pdpEntityID,SAML2Constants.PDP_ROLE);
		
		//Check whether encryption and signing is enabled
		//if not, super() is called with the incoming values
		if(pdpWantAuthzQuerySigned){
			
			
			SOAPBody body = soapMsg.getSOAPBody();
			NodeList nodes = body.getChildNodes();
			Node node = nodes.item(0);
			String nodeName = node.getLocalName();
			String nodeNamespace = node.getNamespaceURI();
			
			//Check whether the request was signed or encrypted and signed
			//if the request is encrypted, decryption is performed
			if(node.getNodeType() == Node.ELEMENT_NODE && nodeName.equals(ENC_NODE_NAME) && nodeNamespace.equals(ENC_NAMESPACE)){
				
				//performs a transformation from DOM to Sting
				try {
					transformer = TransformerFactory.newInstance().newTransformer();
				} catch (TransformerConfigurationException e) {
					logger.log(Level.ERROR, e);
					throw new SOAPException(e);
				} catch (TransformerFactoryConfigurationError e) {
					logger.log(Level.ERROR, e);
					throw new SOAPException(e);
				}
				transformer.setOutputProperty(OutputKeys.INDENT, "no");
				

				StreamResult result = new StreamResult(new StringWriter());
				DOMSource source = new DOMSource(body);
				
				try {
					transformer.transform(source, result);
				} catch (TransformerException e) {
					logger.log(Level.ERROR, e);
					throw new SOAPException(e);
				}
				
				String xmlString = result.getWriter().toString();
				
				//get the key for decryption from configured key alias
				String pdpSignCertAlias = 
	                SAML2Utils.getAttributeValueFromXACMLConfig(
	                    realm, SAML2Constants.PDP_ROLE,pdpEntityID,
	                    SAML2Constants.SIGNING_CERT_ALIAS);
				
				KeyProvider keyProvider = KeyUtil.getKeyProviderInstance();
				Key key = keyProvider.getPrivateKey(pdpSignCertAlias);;
				Element decElem = null;
				
				//decrypt the request

				try {
					decElem = EncManager.getEncInstance().decrypt(xmlString, key);
				} catch (SAML2Exception e1) {
					logger.log(Level.ERROR, e1);
					throw new SOAPException(e1);
					
				}
				
				//transform DOM to String
				Source domSource = new DOMSource(decElem);
				StreamResult streamResult = new StreamResult(new StringWriter());
				try {
					transformer.transform(domSource, streamResult);
				} catch (TransformerException e) {
					logger.log(Level.ERROR, e);
					throw new SOAPException(e);
				}

				//In the transformation above the substring (<?xml version=\"1.0\" encoding=\"UTF-8\"?>) is added in front of
				//this results in a parsing exception because the substring is misplaced
				//to avoid this exception the substring is deleted
				String decXMLString = streamResult.getWriter().toString();
				decXMLString = decXMLString.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","");
				
				
				//build a SOAP message to call super() with decrypted request
				SOAPMessage decSoapMsg = null;
				try {
					decSoapMsg = SAML2Utils.createSOAPMessage(decXMLString, false);
				} catch (SAML2Exception e) {
					logger.log(Level.ERROR, e);
					throw new SOAPException(e);
				}
				
				return super.onMessage(decSoapMsg, request, response, realm, pdpEntityID);
			}
				
		}
		
		return super.onMessage(soapMsg, request, response, realm, pdpEntityID);
    }

}
