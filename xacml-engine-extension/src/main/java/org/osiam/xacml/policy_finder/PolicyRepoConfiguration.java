/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.policy_finder;

/**
 * Policy Repo Configuration.
 * 
 * @author Christian Preilowski 
 *
 */
public class PolicyRepoConfiguration {
	
	private String realm;
	
	private String endpoint;
	
	private String defaultCombiningAlgorithm;

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public String getDefaultCombiningAlgorithm() {
		return this.defaultCombiningAlgorithm;		
	}

	public void setDefaultCombiningAlgorithm(String defaultCombiningAlgorithm) {
		this.defaultCombiningAlgorithm = defaultCombiningAlgorithm;		
	}

}
