/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.policy_finder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import oasis.names.tc.xacml._2_0.policy.schema.os.PolicySetType;

import org.jboss.security.xacml.sunxacml.AbstractPolicy;
import org.jboss.security.xacml.sunxacml.EvaluationCtx;
import org.jboss.security.xacml.sunxacml.ParsingException;
import org.jboss.security.xacml.sunxacml.UnknownIdentifierException;
import org.jboss.security.xacml.sunxacml.combine.CombiningAlgFactory;
import org.jboss.security.xacml.sunxacml.combine.PolicyCombiningAlgorithm;
import org.jboss.security.xacml.sunxacml.finder.PolicyFinder;
import org.jboss.security.xacml.sunxacml.finder.PolicyFinderModule;
import org.jboss.security.xacml.sunxacml.finder.PolicyFinderResult;
import org.jboss.security.xacml.sunxacml.support.finder.FilePolicyModule;
import org.jboss.security.xacml.sunxacml.support.finder.PolicyCollection;
import org.jboss.security.xacml.sunxacml.support.finder.PolicyReader;
import org.jboss.security.xacml.sunxacml.support.finder.TopLevelPolicyException;
import org.osiam.policy_repo.PolicySetService;
import org.osiam.policy_repo.ws_client.PolicyRepoClientFactory;
import org.osiam.xacml.cache.ExpiringMap;


/** 
 * Policy finder implementation that reads policies from a policy repository.
 * 
 *  @author Christian Preilowski (c.preilowski@tarent.de)
 *  @author Jochen Todea
 *
 */
public class RepoPolicyModule extends PolicyFinderModule {
	private static final Logger logger = Logger.getLogger(FilePolicyModule.class.getName());

	private static final String XACML_NAMESPACE = "urn:oasis:names:tc:xacml:2.0:policy:schema:os";
	private static final String POLICYSET_LOCAL_NAME = "PolicySet";

	// the policy identifier for any policy sets we dynamically create
	private static final String POLICY_ID = "urn:com:sun:xacml:support:finder:dynamic-policy-set";
	private static URI policyId;

	private final ExpiringMap<String, List<String>> cache;
	private final PolicyCollection policies;
	private PolicyFinder policyFinder;
	private final PolicySetService port;
	private final String realm;
	private File schemaFile;

	static {
		try {
			policyId = new URI(POLICY_ID);
		} catch( Exception exception ){
			if (logger.isLoggable(Level.SEVERE))
				logger.log(Level.SEVERE, "Couldn't assign default policy id");
		}
		
	};

	/**
	 * initializes a connection to the policy repo.
	 * @param cache 
	 * 
	 * @throws MalformedURLException 
	 * @throws URISyntaxException 
	 * @throws UnknownIdentifierException 
	 * @throws UnknownIdentifierException 
	 */
	public RepoPolicyModule(PolicyRepoConfiguration config, ExpiringMap<String, List<String>> cache) throws MalformedURLException, UnknownIdentifierException, URISyntaxException {
		if (!isNullOrEmpty(config.getDefaultCombiningAlgorithm())) {
			PolicyCombiningAlgorithm algorithm = (PolicyCombiningAlgorithm) CombiningAlgFactory.getInstance().createAlgorithm(new URI(config.getDefaultCombiningAlgorithm()));
			policies = new PolicyCollection(algorithm, policyId);
		} else {
			policies = new PolicyCollection();
		}

		this.cache = cache;

		realm = config.getRealm();
		port = PolicyRepoClientFactory.getInstance().getPolicyRepoPort(config.getEndpoint());

		setSchemaName();
	}

	private boolean isNullOrEmpty(String defaultCombiningAlgorithm) {
		return defaultCombiningAlgorithm == null || defaultCombiningAlgorithm.isEmpty();
	}

	private void setSchemaName() {
		String schemaName = System.getProperty(PolicyReader.POLICY_SCHEMA_PROPERTY);

		if (schemaName != null) {
			schemaFile = new File(schemaName);
		}
	}

	/**
	 * Finds a policy based on a request's context. If more than one applicable
	 * policy is found, this will return an error. Note that this is basically
	 * just a subset of the OnlyOneApplicable Policy Combining Alg that skips
	 * the evaluation step. See comments in there for details on this algorithm.
	 * 
	 * @param context
	 *            the representation of the request data
	 * 
	 * @return the result of trying to find an applicable policy
	 */
	@Override
	public PolicyFinderResult findPolicy(EvaluationCtx context) {
		if (cache.hasExpired(realm)) {
			refreshPolicySetCache();
		}

		updatePolicies();

		try {
			AbstractPolicy policy = policies.getPolicy(context);
			return new PolicyFinderResult(policy);
		} catch (TopLevelPolicyException e) {
			return new PolicyFinderResult(e.getStatus());
		}
	}

	/**
	 * 
	 */
	private void refreshPolicySetCache() {
		try {
			// Retrieves a list of PolicySet's by calling the Web Service
			List<PolicySetType> policySetTypeList = port.retrievePolicySetList(realm);

			if (policySetTypeList != null) {
				List<String> values = new ArrayList<String>();

				for (PolicySetType policySetType : policySetTypeList) {
					values.add(getStringFromPolicySet(policySetType));
				}

				cache.put(realm, values);
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	private void updatePolicies() {
		PolicyReader reader = new PolicyReader(policyFinder, logger, schemaFile);

		for (String policy : cache.get(realm)) {
			logger.log(Level.FINE, "found policy: \n" + policy);

			try {
				AbstractPolicy abstractPolicy = reader.readPolicy(new ByteArrayInputStream(policy.getBytes("UTF-8")));

				policies.addPolicy(abstractPolicy);
			} catch (ParsingException e) {
				if (logger.isLoggable(Level.WARNING)) {
					logger.log(Level.WARNING, "Error reading policy " + policy, e);
				}
			} catch (UnsupportedEncodingException e) {
				if (logger.isLoggable(Level.WARNING)) {
					logger.log(Level.WARNING, "Unsupported Encoding " + policy, e);
				}
			}
		}
	}

	/**Init method (which is called
	 * automatically by the <code>PolicyFinder</code> when the system comes up).
	 */
	@Override
	public void init(PolicyFinder finder) {
		this.policyFinder = finder;
	}

	@Override
	public boolean isRequestSupported() {
		return true;
	}

	// Helper method to get the policies as string
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getStringFromPolicySet(PolicySetType policySet) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(PolicySetType.class);
		StringWriter writer0 = new StringWriter();
		context.createMarshaller().marshal(
				new JAXBElement(new QName(XACML_NAMESPACE, POLICYSET_LOCAL_NAME), 
						PolicySetType.class, policySet), writer0);

		return writer0.toString();
	}
}