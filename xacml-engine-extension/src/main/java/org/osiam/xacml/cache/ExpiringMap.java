package org.osiam.xacml.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * A map which, in addition to mapping keys to values, also maps keys to timestamps. A timestamp represents the 
 * point in time a key-value pair was put into the map. Clients of the map can check if entries (key-value pairs)
 * have expired. For simplicity, does not implement java.util.Map.
 * 
 * @author oweile
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class ExpiringMap<K, V> {
	private final long duration;

	private final Map<K, V> values = new HashMap<K, V>();
	private final Map<K, Long> timestamps = new HashMap<K, Long>();

	private ExpiringMap(long duration, TimeUnit unit) {
		this.duration = unit.toNanos(duration);
	}

	public V get(K key) {
		return values.get(key);
	}

	public V put(K key, V value) {
		refreshTimestamp(key);

		return values.put(key, value);
	}

	private void refreshTimestamp(K key) {
		timestamps.put(key, System.nanoTime());
	}

	public boolean hasExpired(K key) {
		Long timestamp = timestamps.get(key);

		return (timestamp == null) ? true : (System.nanoTime() - timestamp) >= duration;	
	}

	public static <K, V> ExpiringMap<K, V> create(long duration, TimeUnit unit) {
		return new ExpiringMap<K, V>(duration, unit);
	}
}