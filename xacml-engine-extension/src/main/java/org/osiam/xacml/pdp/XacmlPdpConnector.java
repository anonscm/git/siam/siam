/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.pdp;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jboss.security.xacml.sunxacml.Indenter;
import org.jboss.security.xacml.sunxacml.PDPConfig;
import org.jboss.security.xacml.sunxacml.ParsingException;
import org.jboss.security.xacml.sunxacml.UnknownIdentifierException;
import org.jboss.security.xacml.sunxacml.ctx.RequestCtx;
import org.jboss.security.xacml.sunxacml.ctx.ResponseCtx;
import org.jboss.security.xacml.sunxacml.finder.AttributeFinder;
import org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule;
import org.jboss.security.xacml.sunxacml.finder.PolicyFinder;
import org.jboss.security.xacml.sunxacml.finder.PolicyFinderModule;
import org.jboss.security.xacml.sunxacml.finder.ResourceFinder;
import org.jboss.security.xacml.sunxacml.finder.impl.CurrentEnvModule;
import org.jboss.security.xacml.sunxacml.finder.impl.SelectorModule;
import org.osiam.xacml.cache.ExpiringHashMap;
import org.osiam.xacml.pip.TarentIDMAttributeFinderModule;
import org.osiam.xacml.pip.TarentIdmAttributeFinderConfiguration;
import org.osiam.xacml.policy_finder.PolicyRepoConfiguration;
import org.osiam.xacml.policy_finder.RepoPolicyModule;


/**
 *  xacml pdp connector class.
 * 
 *  @author Christian Preilowski (c.preilowski@tarent.de)
 *  @author Jochen Todea
 *
 */
public class XacmlPdpConnector {

	private org.jboss.security.xacml.sunxacml.PDP xacmlPdp;
	
	private static Logger logger = Logger.getLogger(XacmlPdpConnector.class.getName());
	
	/**
	 * initializes a xacml pdp with the given module configuration.
	 * 
	 * @param policyRepoConfig the policy repo configuration
	 * @param tidmAttrFinderConfig the tarent IdM attibute finder config
	 */
	public XacmlPdpConnector(PolicyRepoConfiguration policyRepoConfig, TarentIdmAttributeFinderConfiguration tidmAttrFinderConfig){
		ExpiringHashMap<String, List<String>> cache = ExpiringHashMap.create(30, TimeUnit.SECONDS);

		RepoPolicyModule repoPolicyModule = null;
		//Set up the PolicyFinder that this PDP will use
		try {
			repoPolicyModule = new RepoPolicyModule(policyRepoConfig, cache);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			logger.log(Level.ERROR,"initailization of policy repo finder module failed.", e);
		} catch (UnknownIdentifierException e) {
			e.printStackTrace();
			logger.log(Level.ERROR,"Unknown Identifier Exception.", e);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			logger.log(Level.ERROR,"URI Syntax Exception.", e);
		}

		PolicyFinder policyFinder = new PolicyFinder();
		Set<PolicyFinderModule> policyModules = new HashSet<PolicyFinderModule>();
		policyModules.add(repoPolicyModule);
		policyFinder.setModules(policyModules);
		
		//create new tarent idm attribute finder modules
		TarentIDMAttributeFinderModule tarentIDMAttributeFinderModule = new TarentIDMAttributeFinderModule(tidmAttrFinderConfig); 

	    //also using the example finder module 
	    SelectorModule selectorModule = new SelectorModule();

	    List<AttributeFinderModule> attributeModules = new ArrayList<AttributeFinderModule>();
	    attributeModules.add(tarentIDMAttributeFinderModule);
	    attributeModules.add(selectorModule);
	    attributeModules.add(new CurrentEnvModule()); 

	    AttributeFinder attributeFinder = new AttributeFinder();
	    attributeFinder.setModules(attributeModules);

	    //we don't use a resource finder
		ResourceFinder resourceFinder = null;

		//Create the PDP with all finder modules
		xacmlPdp = new org.jboss.security.xacml.sunxacml.PDP(new PDPConfig(attributeFinder, policyFinder, resourceFinder));
	}

	/** 
	 * Get the response with the decision from policy repository.
	 * 
	 * @param request is the request which is evaluated.
	 * @return response with the decision.
	 */
	public ResponseCtx callXacmlPdp (RequestCtx request){
		//Get the request send by the PEP
		try {
			request = RequestCtx.getInstance(request.getDocumentRoot());
		} catch (ParsingException e) {
			e.printStackTrace();
			logger.log(Level.ERROR, e);
		}

		//Evaluate the request. Generate the response.
		ResponseCtx response = xacmlPdp.evaluate(request);

		//Log the output
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		response.encode(os, new Indenter());
		logger.log(Level.DEBUG,"pdp response: " + os.toString());
		
		return response;
		
	}
	
}