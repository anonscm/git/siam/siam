/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.pip;

import java.net.URI;

import org.jboss.security.xacml.sunxacml.attr.AnyURIAttribute;
import org.jboss.security.xacml.sunxacml.attr.AttributeDesignator;
import org.jboss.security.xacml.sunxacml.attr.RFC822NameAttribute;
import org.jboss.security.xacml.sunxacml.attr.StringAttribute;

/**
 * constants class.
 * 
 *  @author Umer Kayani (u.kayani@tarent.de)
 *
 */
public interface Constants {

	//XACML predefined constants
	public static final String SUBJECT_ID_URN = "urn:oasis:names:tc:xacml:1.0:subject:subject-id";
	public static final String ACTION_ID_URN = "urn:oasis:names:tc:xacml:1.0:action:action-id";
	public static final String RESOURCE_ID_URN = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";

	public static final URI STR_ATTR_TYPE = URI.create(StringAttribute.identifier);
	public static final URI URI_ATTR_TYPE = URI.create(AnyURIAttribute.identifier);
	public static final URI RFC822NAME_ATTR_TYPE = URI.create(RFC822NameAttribute.identifier);

	public static final URI SUBJECT_CATEGORY_DEFAULT = URI.create(AttributeDesignator.SUBJECT_CATEGORY_DEFAULT);

	public static final URI SUBJECT_ID = URI.create( SUBJECT_ID_URN );
	public static final URI ACTION_ID = URI.create( ACTION_ID_URN );
	public static final URI RESOURCE_ID = URI.create( RESOURCE_ID_URN );

	
	//TARENT-IDM custom defined constants
	public static final String TOKEN_SEPERATOR = ":";
	
	public static final String USER_GROUPS_IDENTIFIER = "urn:tarent:names:tarentidm:groupsandroles:groupnames";
	public static final String USER_ROLES_IDENTIFIER = "urn:tarent:names:tarentidm:groupsandroles:rolenames";

	public static final int PRIVATE_DATA_TOKEN_LENGTH = 7;
	public static final String PRIVATE_DATA_FIRST_TOKEN = "urn"; 
	public static final String PRIVATE_DATA_SECOND_TOKEN = "tarent"; 
	public static final String PRIVATE_DATA_THIRD_TOKEN = "names"; 
	public static final String PRIVATE_DATA_FOURTH_TOKEN = "tarentidm"; 
	public static final String PRIVATE_DATA_FIFTH_TOKEN = "privatedata"; 

	public static final String CONTACTDATA_MOBILE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:contactData:mobile";
	public static final String CONTACTDATA_FAX_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:contactData:fax";
	public static final String CONTACTDATA_PHONE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:contactData:phone";
		
	public static final String PERSONALDATA_GENDER_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:personalData:gender";
	public static final String PERSONALDATA_BIRTHDAY_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:personalData:birthdate";
	public static final String PERSONALDATA_LASTNAME_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:personalData:lastname";
	public static final String PERSONALDATA_FIRSTNAME_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:personalData:firstname";
	public static final String PERSONALDATA_SALUTATION_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:personalData:salutation";
	
	public static final String PRIMARYADDRESS_STATE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:state";
	public static final String PRIMARYADDRESS_CITY_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:city";
	public static final String PRIMARYADDRESS_ZIPCODE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:zipCode";
	public static final String PRIMARYADDRESS_COUNTRY_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:country";
	public static final String PRIMARYADDRESS_STREETNUMBER_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:streetNumber";
	public static final String PRIMARYADDRESS_STREETNAME_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:primaryAddress:streetName";

	public static final String FURTHERADDRESS_STATE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:state";
	public static final String FURTHERADDRESS_CITY_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:city";
	public static final String FURTHERADDRESS_ZIPCODE_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:zipCode";
	public static final String FURTHERADDRESS_COUNTRY_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:country";
	public static final String FURTHERADDRESS_STREETNUMBER_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:streetNumber";
	public static final String FURTHERADDRESS_STREETNAME_IDENTIFIER = "urn:tarent:names:tarentidm:privatedata:furtherAddresses:streetName";

	
	public static final String UNDEFINED_ACTION = "[undefined action]";
	public static final String UNDEFINED_USER = "[undefined user]";
	public static final String UNDEFINED_ATTRIBUTE = "[undefined attribute]";

}