/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.pip;

import java.net.URI;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;


/**
 * Private data management query handler class.
 * 
 *  @author Umer Kayani (u.kayani@tarent.de)
 *
 */
public class PrivateDataManagementHandler {
	
	private InitialContext ctx = null;
	private PrivateDataManagement privateDataManagement;
	private GroupAndRoleManagement groupAndRoleManagement;
	private static Logger logger = Logger.getLogger("PrivateDataManagementHandler.class");
	
	/**
	 * Constructor. Initializes the RMI handler.
	 * 
	 * @param rmiEndpoint is the rmi address of the remote machine.
	 * @param clientName is the clientName on the idm-backend.
	 */
	public PrivateDataManagementHandler(TarentIdmAttributeFinderConfiguration config) 
			throws NamingException {

		logger.info("PrivateDataManagementHandler, Parameters: Endpoint="+config.getRMIEndpoint()+", ClientName="+config.getClientName());
		Properties props = new Properties();
		props.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		props.setProperty("java.naming.provider.url", config.getRMIEndpoint());
		ctx = new InitialContext(props);	
	}
	
	/**
	 * Initialization: Looks up PrivateDataManagementImp and & GroupAndRoleManagementImpl remotely via rmi.
	 * @throws NamingException 
	 */
	public void init() throws NamingException {
		privateDataManagement = (PrivateDataManagement) ctx.lookup("idm-backend-ear/PrivateDataManagementImpl/remote");
		groupAndRoleManagement = (GroupAndRoleManagement) ctx.lookup("idm-backend-ear/GroupAndRoleManagementImpl/remote");
	}

	/** Get the value of a specific attribute from Tarent-IDM.
	 * 
	 * @param userUuid universally unique identifier for the user
	 * @param pipAttributeUri PIP URI to reference IdM attribute values
	 * @return value of the referenced attribute for the given user.
	 * @throws BackendException 
	 * @throws IllegalRequestException 
	 * @throws AccessDeniedException 
	 */
	public String getAttributeValueFromTarentIDM(String userUuid, URI pipAttributeUri) 
		throws AccessDeniedException, IllegalRequestException, BackendException {		
		logger.log(Level.INFO, 
				"getAttributeValueFromTarentIDM() Parameters: " +
				"User UUID: "+userUuid + ", PIP Attribute URI: " + pipAttributeUri);

		String returnAttributeValue = Constants.UNDEFINED_ATTRIBUTE;
		String attributeGroupName = null;
		String attributeName = null;

		StringTokenizer st = new StringTokenizer(pipAttributeUri.toString(), Constants.TOKEN_SEPERATOR);
		if (st.countTokens() == Constants.PRIVATE_DATA_TOKEN_LENGTH && isURNNameCorrect( st )) {
			attributeGroupName = st.nextToken();
			attributeName = st.nextToken();
		}
		String idmAttributeUri = attributeGroupName + "." + attributeName;
		returnAttributeValue = privateDataManagement.searchValueByUri(userUuid, idmAttributeUri);
		logger.log(Level.INFO, 
				"Searched Attribute: PIP Attribute URI: "+pipAttributeUri+ ", Value="+returnAttributeValue);
		return returnAttributeValue;
	}
	
	/** Ensures that the attributeId conforms to a predefined format for the first 5 tokens.
	 * 
	 * @param attributeIdTokenString is the StringTokenizer contating all tokens of attributeId.
	 * @return true if attribute conforms to the predefined format otherwise false
	 */
	private boolean isURNNameCorrect(StringTokenizer attributeIdTokenString) {
		if( attributeIdTokenString.nextToken().equals( Constants.PRIVATE_DATA_FIRST_TOKEN) 
				&& attributeIdTokenString.nextToken().equals(Constants.PRIVATE_DATA_SECOND_TOKEN) 
				&& attributeIdTokenString.nextToken().equals(Constants.PRIVATE_DATA_THIRD_TOKEN) 
				&& attributeIdTokenString.nextToken().equals(Constants.PRIVATE_DATA_FOURTH_TOKEN) 
				&& attributeIdTokenString.nextToken().equals(Constants.PRIVATE_DATA_FIFTH_TOKEN) ) {
			return true;
		} else {
			return false;
		}
	}

	public List<Group> getGroups(String userName) 
		throws IllegalRequestException, BackendException {
		List<Group> groups = groupAndRoleManagement.getGroupsByUser( userName );
		logger.log(Level.INFO, "Groups loaded from Tarent-Idm: "+groups.toString());
		return groups;
	}

	public List<Role> getRoles(String userName) 
		throws IllegalRequestException, BackendException {
		// TODO Auto-generated method stub
		List<Role> roles = groupAndRoleManagement.getRolesByUser( userName, null );
		logger.log(Level.INFO, "Roles loaded from Tarent-Idm: "+roles.toString());
		return roles;
	}

}

