/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.pip;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.jboss.security.xacml.sunxacml.EvaluationCtx;
import org.jboss.security.xacml.sunxacml.attr.AttributeDesignator;
import org.jboss.security.xacml.sunxacml.attr.AttributeValue;
import org.jboss.security.xacml.sunxacml.attr.BagAttribute;
import org.jboss.security.xacml.sunxacml.attr.StringAttribute;
import org.jboss.security.xacml.sunxacml.cond.EvaluationResult;
import org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule;
import org.w3c.dom.Node;


/** A custom attribute finder module (PIP) for resolving Subject attributes from
 *  Tarent-IDM data store.
 * 
 * @author Umer Kayani (u.kayani@tarent.de)
 *
 */
public class TarentIDMAttributeFinderModule 
					extends AttributeFinderModule {
	private PrivateDataManagementHandler pdmh = null;
	private static Logger logger = Logger
			.getLogger(TarentIDMAttributeFinderModule.class.getName());
	private Set<URI> supportedIdsSet;
	
	/** Constructor: Does initialization for making connection to the remote rmi 
	 *  server.
	 * 
	 * @param RMIEndpoint is the address and port of rmi server.
	 * @param clientName is the name of the backend Tarent-IDM client.
	 */
	public TarentIDMAttributeFinderModule(TarentIdmAttributeFinderConfiguration configuration) {
		try {
			pdmh = new PrivateDataManagementHandler(configuration);
			pdmh.init();
			initializeSupportedIds();
		} catch (NamingException e) {
			e.printStackTrace();
		}

	}

	
	/* This must return true because this attributeFinder module supports designators.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#isDesignatorSupported()
	 */
	@Override
	public boolean isDesignatorSupported() {
		boolean designatorSupported = true;
		logger.log(Level.INFO,
				"isDesignatorSupported() returning: "+designatorSupported);
		return designatorSupported;
	}

	/* This must return false because this attributeFinder module does not 
	 * supports selectors (xpath).
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#isSelectorSupported()
	 */
	@Override
	public boolean isSelectorSupported() {
		boolean selectorSupproted = false;
		logger.log(Level.INFO,
				"isSelectorSupported() returning: "+selectorSupproted);
		return selectorSupproted;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#getSupportedDesignatorTypes()
	 */
	@Override
	public Set<Integer> getSupportedDesignatorTypes() {
		Set<Integer> set = new HashSet<Integer>();
		//Returns only <code>SUBJECT_TARGET</code> since this module only supports Subject attributes.
		set.add(Integer.valueOf(AttributeDesignator.SUBJECT_TARGET));
		logger.log(Level.INFO,
				"getSupportedDesignatorTypes() returning: "+set);
		return set;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#findAttribute(java.net.URI,
	 * java.net.URI, java.net.URI, java.net.URI, org.jboss.security.xacml.sunxacml.EvaluationCtx,
	 * int)
	 */
	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
			URI issuer, URI subjectCategory, EvaluationCtx context,
			int designatorType) {
		logger.log(Level.INFO,
				"findAttribute() "
						+ "Parameters: (attributeType=" + attributeType
						+ ", attributeId=" + attributeId + ", issuer=" + issuer
						+ ", subjectCategory=" + subjectCategory + ", context="
						+ context + ", designatorType=" + designatorType + ")");

		// Only supported DesignatorTypes are serviced
		if ( !getSupportedDesignatorTypes().contains( designatorType ) ) {
			logger.log(Level.SEVERE,
					"Only DesignatorType are supported.");
			return emptyBag( attributeType );
		}

		// TODO: This is a workaround. The supported Ids should be enforced in findAttribute 
		// method of AttributeFinder class.
		// Only supported identifiers are serviced
		if ( !getSupportedIds().contains( attributeId ) ) {
			logger.log(Level.SEVERE,
					"The attribute "
						+ attributeId 
						+ " is not supported by the attributeFinder module.");
			return emptyBag( attributeType );
		}

		String requestAction = getActionFromRequest(context);
		String requestUser = getUserFromRequest(context, attributeType, issuer, subjectCategory);

		// User must be retrievable from the request
		if( requestUser == null || requestUser.equals(Constants.UNDEFINED_USER) ) {
			logger.log(Level.SEVERE,
					"AttributeId = "
						+ Constants.SUBJECT_ID_URN
						+ ", Could not be retrieved from request." );
			return emptyBag( attributeType );
		}

		logger.log(Level.INFO, "USER : " + requestUser);
		logger.log(Level.INFO, "ACTION : " + requestAction);

		
		//Everything ok, now get the values of appropriate attribute from tarent-idm
		if( attributeId.toString().equals(Constants.USER_GROUPS_IDENTIFIER)  
				|| attributeId.toString().equals(Constants.USER_ROLES_IDENTIFIER)){
			//If groups or roles are requested
			return getGroupsOrRolesForUser(attributeId, attributeType, requestUser);
		}else{
			//An attribute from private date management is requested
			return getPrivateDataForUser(attributeId, attributeType, requestUser);
		}
	
	}

/*
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#findAttribute(java.lang.String
	 * , org.w3c.dom.Node, java.net.URI, org.jboss.security.xacml.sunxacml.EvaluationCtx, java.lang.String)
	 */
	@Override
	public EvaluationResult findAttribute(String contextPath,
			Node namespaceNode, URI attributeType, EvaluationCtx context,
			String xpathVersion) {
		logger.log(Level.SEVERE,
				"findAttribute() for xpath is not supported: "
						+ "Parameters: (contextPath= " + contextPath
						+ ", NameSpaceNode= " + namespaceNode
						+ ", attributeType=" + attributeType + ", context="
						+ context + ", xpathVersion=" + xpathVersion + ")");
		return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jboss.security.xacml.sunxacml.finder.AttributeFinderModule#getSupportedIds()
	 */
	@Override
	public Set<URI> getSupportedIds() {
		return this.supportedIdsSet;
	}

	/*******************************Private Helper methods***********************************/

	private EvaluationResult getPrivateDataForUser(URI attributeUri, URI attributeType, String userUuid){
		String attributeValue = null;
		try {
			attributeValue = pdmh.getAttributeValueFromTarentIDM(userUuid, attributeUri);
		} catch (AccessDeniedException e) {
			logger.log(Level.SEVERE, e.getMessage());
		} catch (IllegalRequestException e) {
			logger.log(Level.SEVERE, e.getMessage());
		} catch (BackendException e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		
		if ( attributeValue == null ) {
			return emptyBag( attributeType );
		}
		
		Set<StringAttribute> set = new HashSet<StringAttribute>();
		set.add(new StringAttribute(attributeValue));
		return new EvaluationResult(new BagAttribute(attributeType, set));		
	}
	
	private EvaluationResult getGroupsOrRolesForUser(URI attributeId, URI attributeType, String user)
	{
		EvaluationResult evaluationResult = null;
		
		try {
			if( attributeId.toString().equals(Constants.USER_GROUPS_IDENTIFIER)){
				evaluationResult = getGroups( user );
				AttributeValue av = evaluationResult.getAttributeValue();
				logger.log(Level.INFO, "Groups of User: "+ getAllChildren(av));
			}else if( attributeId.toString().equals(Constants.USER_ROLES_IDENTIFIER)){
				evaluationResult = getRoles( user );
				AttributeValue av = evaluationResult.getAttributeValue();
				logger.log(Level.INFO, "Roles of User: "+ getAllChildren(av));
			}else{
				logger.log(Level.SEVERE, "Wrong membership requested: "+attributeId.toString());
			}
		} catch (IllegalRequestException e) {
			logger.log(Level.SEVERE, e.getMessage());	
		} catch (BackendException e) {
			logger.log(Level.SEVERE, e.getMessage());	
		}
		
		if ( evaluationResult == null ) {
			return emptyBag( attributeType );
		}
				
		return evaluationResult;
			
	}

	
	private void initializeSupportedIds() {
		
	    this.supportedIdsSet = new HashSet<URI>();
	    try {
	    	
	        supportedIdsSet.add(new URI( Constants.USER_GROUPS_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.USER_ROLES_IDENTIFIER ));

	        supportedIdsSet.add(new URI( Constants.CONTACTDATA_FAX_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.CONTACTDATA_MOBILE_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.CONTACTDATA_PHONE_IDENTIFIER ) );

	        supportedIdsSet.add(new URI( Constants.PERSONALDATA_BIRTHDAY_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.PERSONALDATA_FIRSTNAME_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.PERSONALDATA_GENDER_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.PERSONALDATA_LASTNAME_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.PERSONALDATA_SALUTATION_IDENTIFIER ) );

	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_STATE_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_CITY_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_ZIPCODE_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_COUNTRY_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_STREETNUMBER_IDENTIFIER ));
	        supportedIdsSet.add(new URI( Constants.PRIMARYADDRESS_STREETNAME_IDENTIFIER ));

	        supportedIdsSet.add(new URI( Constants.FURTHERADDRESS_CITY_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.FURTHERADDRESS_COUNTRY_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.FURTHERADDRESS_STREETNAME_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.FURTHERADDRESS_STREETNUMBER_IDENTIFIER ) );
	        supportedIdsSet.add(new URI( Constants.FURTHERADDRESS_ZIPCODE_IDENTIFIER ) );
	   	    
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	
	}

	private String getAllChildren(AttributeValue av) {
		StringBuffer sb = new StringBuffer();
		
		for(Iterator<StringAttribute> it = ((BagAttribute)av).iterator(); it.hasNext(); ){
			StringAttribute sa = (StringAttribute) it.next();
			sb.append( sa.toString() );
		}
		
		return sb.toString();
	}
	
	/** Gets the action of request as a string using the Evaluation Context.
	 * 
	 * @param ctx is the Evaluation Context.
	 * @return Action of the request as a string.
	 */
	private String getActionFromRequest(EvaluationCtx ctx) {
		EvaluationResult result = ctx.getActionAttribute(Constants.STR_ATTR_TYPE,
				Constants.ACTION_ID, null);
		if (result.indeterminate()) {
			return Constants.UNDEFINED_ACTION;
		}

		BagAttribute bag = (BagAttribute) result.getAttributeValue();

		if (bag == null || bag.isEmpty()) {
			return Constants.UNDEFINED_ACTION;
		}

		return ((StringAttribute) bag.iterator().next()).getValue();
	}

	
	/** Gets the user (subject) of the request as a string using the Evaluation Context.
	 * @param context is the Evaluation Context
	 * @param attributeType is the type of attribute
	 * @param issuer is the Issuer of the request
	 * @param subjectCategory is the category to which subject attribute belongs
	 * @return a string representation of the user
	 */
	private String getUserFromRequest(EvaluationCtx context, URI attributeType, 
			URI issuer, URI subjectCategory) {
		EvaluationResult result = context.getSubjectAttribute(attributeType, Constants.SUBJECT_ID,
               issuer, subjectCategory);

		BagAttribute bag = (BagAttribute) (result.getAttributeValue());
		Iterator<StringAttribute> iterator = bag.iterator();
		if (iterator.hasNext()) {
			StringAttribute attr = (StringAttribute) (iterator.next());
			return attr.getValue();
		}else{
			return null;
       }    
	}

	//Gets a bag consisting of the groups of the user
	private EvaluationResult getGroups(String user) 
		throws IllegalRequestException, BackendException {
		List<Group> groupArray = null;
		groupArray = pdmh.getGroups( user );

		int size = (groupArray == null) ? 0 : groupArray.size();
		Set<StringAttribute> groupAttributes = new HashSet<StringAttribute>(size);
		for(int i = 0; i < size; ++i) {
			groupAttributes.add(new StringAttribute( groupArray.get(i).getName() ));
		}
		AttributeValue value = new BagAttribute(Constants.STR_ATTR_TYPE, groupAttributes);
		return new EvaluationResult(value);
		
	}

	//gets a bag consisting of the roles of the user
	private EvaluationResult getRoles(String user) 
		throws IllegalRequestException, BackendException {
		List<Role> roleArray = null;
		roleArray = pdmh.getRoles( user );
		
		int size = (roleArray == null) ? 0 : roleArray.size();
		Set<StringAttribute> roleAttributes = new HashSet<StringAttribute>(size);
		for(int i = 0; i < size; ++i) {
			roleAttributes.add(new StringAttribute( roleArray.get(i).getName() ));
		}
		AttributeValue value = new BagAttribute(Constants.STR_ATTR_TYPE, roleAttributes);
		return new EvaluationResult(value);
		
	}
	
	private EvaluationResult emptyBag(URI attributeType){
		return new EvaluationResult(BagAttribute
				.createEmptyBag(attributeType));
	}


}
