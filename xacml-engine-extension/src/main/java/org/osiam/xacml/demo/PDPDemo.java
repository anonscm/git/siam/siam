/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.xacml.demo;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jboss.security.xacml.sunxacml.Indenter;
import org.jboss.security.xacml.sunxacml.ParsingException;
import org.jboss.security.xacml.sunxacml.combine.DenyOverridesPolicyAlg;
import org.jboss.security.xacml.sunxacml.ctx.RequestCtx;
import org.jboss.security.xacml.sunxacml.ctx.ResponseCtx;
import org.osiam.xacml.pdp.XacmlPdpConnector;
import org.osiam.xacml.pip.TarentIdmAttributeFinderConfiguration;
import org.osiam.xacml.policy_finder.PolicyRepoConfiguration;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/** Demo class with main method
 * 
 */
public class PDPDemo {
	
	private static Logger logger = Logger.getLogger(PDPDemo.class.getName());


	/** Main method
	 * Reads a request from a file and calls the PDP
	 * 
	 */
	public static void main(String[] args) {
		
		try {

			InputStream in = new FileInputStream("/home/oweile/workspace/siam/trunk/xacml-engine-extension/src/main/resources/xacml_demo_files/testR.xml");
			
			PolicyRepoConfiguration repoConfig = new PolicyRepoConfiguration();
			repoConfig.setRealm("/");
			repoConfig.setEndpoint("http://osiam1-backend.bonn.tarent.de:8080/ear-container-XacmlPolicyRepo/PolicySetServiceImpl");
			repoConfig.setDefaultCombiningAlgorithm(DenyOverridesPolicyAlg.algId);
			
			TarentIdmAttributeFinderConfiguration attrFinderConfig = new TarentIdmAttributeFinderConfiguration();
			attrFinderConfig.setRMIEndpoint("jnp://osiam1-backend.bonn.tarent.de:1099");
			attrFinderConfig.setClientName("siam");
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(in);
			
			ByteArrayOutputStream os1 = new ByteArrayOutputStream();
			RequestCtx request = RequestCtx.getInstance(document.getDocumentElement());
			request.encode(os1, new Indenter());
			logger.info("Printing Request: " + os1.toString());
			
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			XacmlPdpConnector pdp = new XacmlPdpConnector(repoConfig, attrFinderConfig);
			ResponseCtx response = pdp.callXacmlPdp(request);
			response.encode(os, new Indenter());
			logger.info("Printing Response: " + os.toString());
		
		}
		catch (ParsingException e) {
			e.printStackTrace();	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
