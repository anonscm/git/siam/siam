/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package org.osiam.openam.tarent_idm_ds_plugin;

interface TarentDsPluginConstants {
	
	// Plugin variables mapping in the configuration interface.
    public static final String ENDPOINT  = "tarentIdmEndpointurl";
    public static final String CLIENTNAME  = "tarentIdmClientName";

	
	//Constants for USER attributes
	public static final String IDREPO_USER_ATTRIBUTE_FULLNAME = "cn";
	public static final String IDREPO_USER_ATTRIBUTE_FIRSTNAME = "givenname";
	public static final String IDREPO_USER_ATTRIBUTE_LASTNAME = "sn";
	public static final String IDREPO_USER_ATTRIBUTE_PASSWORD = "unicodePwd";
	public static final String IDREPO_USER_ATTRIBUTE_PASSWORD_REPEAT = "unicodePwdRepeat";
	public static final String IDREPO_USER_ATTRIBUTE_UID = "uid";
	public static final String IDREPO_USER_ATTRIBUTE_UUID = "useruuid";
	public static final String IDREPO_USER_ATTRIBUTE_ACTIVE = "inetuserstatus";
	
	//Constants for PrivateDataManagement attributes
	public static final String IDREPO_USER_ATTRIBUTE_EMAIL_ADDRESS = "mail";
	public static final String IDREPO_USER_ATTRIBUTE_EMPLOYEE_NUMBER = "employeenumber";
	public static final String IDREPO_USER_ATTRIBUTE_HOME_ADDRESS = "postaladdress";
	public static final String IDREPO_USER_ATTRIBUTE_TELEPHONE_NUMBER = "telephonenumber";
	public static final String IDREPO_USER_ATTRIBUTE_ACCOUNT_EXPIRATION_DATE = "iplanet-am-user-account-life";
	public static final String IDREPO_USER_ATTRIBUTE_ACCOUNT_MSISDN_NUMBER = "sunIdentityMSISDNNumber";
	public static final String IDREPO_USER_ATTRIBUTE_ACCOUNT_SUCCESS_URL = "iplanet-am-user-success-url";
	public static final String IDREPO_USER_ATTRIBUTE_ACCOUNT_FAILURE_URL = "iplanet-am-user-failure-url";
	public static final String IDREPO_USER_ATTRIBUTE_ACCOUNT_USER_ALIAS_LIST = "iplanet-am-user-alias-list";

	//Constants for GROUP attributes
	public static final String IDREPO_GROUP_XML_ID = "xmlID";
	public static final String IDREPO_GROUP_CLIENT = "client";
	public static final String IDREPO_GROUP_BREAKABLE_LABEL = "breakableLabel";
	public static final String IDREPO_GROUP_LABEL = "label";
	public static final String IDREPO_GROUP_REFERENCED_ENTITY_CONTAINER = "referencedEntityContainer";
	public static final String IDREPO_GROUP_SYNC_STATUS = "syncStatus";
	public static final String IDREPO_GROUP_WRITE_PROTECTED = "writeProtected";
	public static final String IDREPO_GROUP_ID = "id";
	public static final String IDREPO_GROUP_PREVIOUS_SYNC_STATUS = "previousSyncStatus";
	public static final String IDREPO_GROUP_CLIENT_NAME = "clientName";
	public static final String IDREPO_GROUP_NAME = "name";
	public static final String IDREPO_GROUP_CLIENT_NAME_IGNORE_XML_FLAG = "clientNameIgnoreXmlFlag";
	public static final String IDREPO_GROUP_DISPLAYNAME = "displayName";
	
	
    // Role and Group membership attribute
	public static final String ROLE_MEMBERSHIP_ATTRIBUTE = "nsRoleDN";
	public static final String GROUP_MEMBERSHIP_ATTRIBUTE = "memberOfGroup";

	
	//Error Constants
	public static final String TARENT_IDM_FIELD_PATTERN_ERROR_CODE = "600";
	public static final String TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE = "601";
	public static final String TARENT_IDM_BACKEND_EXCEPTION_CODE = "602";
//	public static final String TARENT_IDM_NAMING_EXCPETION_CODE = "603";
	public static final String TARENT_IDM_EXTRA_FEATURES_EXCEPTION_CODE = "604";
	public static final String TARENT_IDM_OPERATIONS_NOT_SUPPORTED_EXCEPTION_CODE = "605";
	
}
