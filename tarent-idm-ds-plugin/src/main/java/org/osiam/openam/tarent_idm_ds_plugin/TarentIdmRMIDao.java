/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.openam.tarent_idm_ds_plugin;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.identity.account.service.AuthenticationService;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;

import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.IdRepoBundle;
import com.sun.identity.idm.IdRepoException;
import com.sun.identity.idm.IdType;


/**
 * This class provides access to the TarentIdm project. It does this by using the 
 * remote interface UserManagement through RMI calls. 
 * 
 * @author Umer Kayani (u.kayani@tarent.de)
 *
 */
public class TarentIdmRMIDao {
	
	private Properties props = null;
	private InitialContext ctx = null;
	private UserManagement userManagementService;
	private AuthenticationService authenticationService;
	private GroupAndRoleManagement groupAndRoleManagementService;
	private String clientName = null;
	private String endpoint = null;
	private static Logger logger = Logger.getLogger( TarentIdmRMIDao.class.getName() );
	
	/**
	 * Constructor. Initializes the RMI handler.
	 * 
	 * @param endpoint is the rmi address of the remote machine.
	 * @param clientName is the name of tarent-idm client.
	 * @throws NamingException 
	 */
	public TarentIdmRMIDao(String endpoint, String clientName) 
									throws NamingException {
		logger.info("TarentIdmRMIDao.initalize called: Endpoint: "+endpoint+", ClientName: "+clientName);
		this.clientName = clientName;
		this.endpoint  = endpoint;

		props = new Properties();
		props.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		props.setProperty("java.naming.provider.url", this.endpoint);  //"jnp://172.25.0.83" + ":1099"
		
		ctx = new InitialContext(props);	
	}
	
	private boolean isJnpPortListening(String endpoint) {
		boolean rmiPortListening = false;
		System.out.println(endpoint);
		
		StringTokenizer st = new StringTokenizer(endpoint,":");
		st.nextToken();
		String host = st.nextToken();
		String port = st.nextToken();
		
		host = (String) host.subSequence(2, host.length());
		
		logger.info("Host: "+host+", Port: "+port);
		
		Socket socket = null;
		try {
			socket = new Socket( host, Integer.parseInt(port));
			if( socket.isBound() )
				rmiPortListening = true;			
		} catch (IOException e) {
			logger.error("Cannot connect to host: "+host);
		} finally { 
		    // Clean up
		    if (socket != null)
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				} 
		}
		
		return rmiPortListening;
	}

	/**
	 * Initialization.
	 * @throws NamingException 
	 */
	public void init() throws NamingException {
		logger.info("TarentIdmRMIDao.init called, Looking up: idm-backend-ear/UserManagementImpl/remote, idm-backend-ear/AuthenticationServiceImpl/remote, " +
				"idm-backend-ear/ClientManagementImpl/remote & idm-backend-ear/GroupAndRoleManagementImpl/remote");	

		if( isJnpPortListening(this.endpoint) ){
			logger.info("SUCCESS: RMI endpoint is listening.");
		
			userManagementService = (UserManagement) ctx.lookup("idm-backend-ear/UserManagementImpl/remote");
			authenticationService = (AuthenticationService) ctx.lookup("idm-backend-ear/AuthenticationServiceImpl/remote");
			groupAndRoleManagementService = (GroupAndRoleManagement) ctx.lookup("idm-backend-ear/GroupAndRoleManagementImpl/remote");
			ClientManagement clientManagementService = (ClientManagement) ctx.lookup("idm-backend-ear/ClientManagementImpl/remote");
			 if ( clientIsPresent(clientManagementService) ){
				 logger.info("SUCCESS: IDM Client is present.");					
			 }else{
				 logger.error("FAILED: Wrong IDM Client.");
				 throw new NamingException("FAILED: Wrong IDM Client.");
			 }
		}else{
			logger.error("FAILED: Either services are not Bound or Client is not present.");
			throw new NamingException("FAILED: Either services are not Bound or Client is not present.");
		}
		
	}
	
	private boolean clientIsPresent(ClientManagement clientManagementService) {
		boolean clientPresent = false;
		
		try {
			Client client = clientManagementService.getClientByName(this.clientName);
			logger.info("Client check was successful for client: "+client.getClientName());
			clientPresent = true;
		} catch (Exception e) {
			logger.error("Client check was not successful, message was:"+ e.getMessage(),e);
		}
		
		return clientPresent;
	}

//	private boolean servicesAreBound(UserManagement userManagementService2) {
//		boolean servicesBound = false;
//		
//		List<User> users= this.searchUsers(null, 100, null, 0);
//		 
//		if (users != null)
//			servicesBound = true;
//		
//		return servicesBound;
//	}

	/**
	 * Shutdown.
	 */
	public void shutdown() {
		logger.info("TarentIdmRMIDao.shutdown called.");	
	}
	
	/**
	 * Loads a user object from its user name.
	 * 
	 * @param name is the username of the User to be loaded.
	 * @return An object containing the User object.
	 * @throws IdRepoException 
	 */
	public User loadUser(String userName) 
				throws IdRepoException {
		logger.info("TarentIdmRMIDao.loadUser called, Name="+userName);	
		
		User user = null;
		try {
			user = userManagementService.getUserByUsername(clientName, userName);
			printUser(user);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		
		return user;
	}
	
	/**
	 * Checks if a user exists by the a specific user name.
	 * 
	 * @param userName is the name of the user to be checked for existence.
	 * @return true if user exists, false otherwise.
	 * @throws IdRepoException 
	 */
	public boolean isExists(String userName) 
					throws IdRepoException {
		boolean userExists = false;
		
		logger.info("TarentIdmRMIDao.isExists,  Name="+userName);
		
		try {
			userExists = userManagementService.isUserExisting(clientName, userName );
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		
		return userExists;
	}
	
	/**
	 * Checks if a specific user is active.
	 * 
	 * @param userName is the name of the user to be checked if it is active or not.
	 * @return true if user is active, false otherwise.
	 * @throws IdRepoException 
	 */
	public boolean isActive(String userName) 
					throws IdRepoException {
		boolean userActive = false;
		
		logger.info("TarentIdmRMIDao.isActive,  Name="+userName);	
		

		User user = null;
		try {
			user = userManagementService.getUserByUsername(clientName, userName);
			userActive = user.isActive();
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		
		return userActive;
	}
	
	/**
	 * Creates a new user in the tarent-idm.
	 * 
	 * @param newUser is the User object that needs to be created.
	 * @throws IdRepoException 
	 */
	public void createUser(User newUser) 
				throws IdRepoException {
		
		logger.info("TarentIdmRMIDao.createUser,  New User="+newUser);	
		
		printUser( newUser );

		try {
			ValidationUtil.validate( newUser );
		} catch (IllegalRequestException e1) {
			logger.info(e1.getIllegalProperties().toString());
			throwCustomIdRepoException( e1.getIllegalProperties().toString(), TarentDsPluginConstants.TARENT_IDM_FIELD_PATTERN_ERROR_CODE);
		}catch (Exception e){
			System.out.println("Exception: "+e.toString());			
		}
		
		try {
			userManagementService.setUser(clientName, newUser );
			changePassword( newUser.getUsername(), newUser.getPassword());
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
	}
	
	/**
	 * Updates a User from a User object.
	 * 
	 * @param user the User object that needs to be updated.
	 */
	public void updateUser(User user) 
				throws IdRepoException {
		logger.info("TarentIdmRMIDao.updateUser, User to be updated: ="+user);	

			try {
				userManagementService.setUser( clientName, user );
			} catch (IllegalRequestException e) {
				e.printStackTrace();
				throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
			} catch (BackendException e) {
				e.printStackTrace();
				throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
			}
		
	}
	
	
	/**
	 * Change password for a user.
	 * 
	 * @param userName is the target username, for which password should be changed
	 * @param newPassword is the new password to be set
	 * @return true or false depending on whether password was changed or not
	 * @throws IdRepoException
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	public boolean changePassword(String userName, String newPassword) 
		throws IdRepoException, IllegalRequestException, BackendException {
		logger.info("TarentIdmRMIDao.changePassword called, " + ")");
		boolean passwordChanged = false;
		
		User user = loadUser( userName );
		
		if ( user != null ){
			logger.info("User loaded." );

			authenticationService.updatePassword( clientName, user.getUsername(), user.getUuid(), newPassword);
			logger.info("Password changed. " );
			passwordChanged = true;
		}
				
		return passwordChanged;		
	}
	
	
	/**
	 * Deletes a User.
	 * 
	 * @param userName of the User that needs to be deleted.
	 */
	public void deleteUser( String userName ) 
				throws IdRepoException {
		logger.info("TarentIdmRMIDao.deleteUser,  Name="+userName);	
		
		User user;
		try {
			user = userManagementService.getUserByUsername(clientName, userName);
			userManagementService.deleteUser( user.getUuid(), true);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		
		
	}
	
	/**
	 * Searches for users and returns a list based on the following parameters.
	 * 
	 * @param pattern is the pattern to search.
	 * @param maxResults is the max number of users to search.
	 * @param queryParams are query parameters.
	 * @param filterOp are operations to filter result.
	 * @param avPairs 
	 * @param queryDescriptor 
	 * @return a List of users meeting the specified criteria.
	 */
	@SuppressWarnings("unchecked")
	List<User> searchUsers(String pattern, Map<String, Object> queryParams, int filterOp, QueryDescriptor queryDescriptor){
		logger.info("TarentIdmRMIDao.search called,  "+pattern+", "+queryParams+", "+filterOp+", "+queryDescriptor);	
		
		BeanOrderProperty beanOrderProperty = null;
		BeanSearchProperty beanSearchProperty = null;

		if(( queryParams != null) && !(queryParams.isEmpty()) ) {
			if( "*".equals(pattern) ){
				
				Set<String> uidVals = (Set<String>) queryParams.get(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_UID);
				Set<String> fullNameVals = (Set<String>) queryParams.get(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FULLNAME);
				Set<String> firstNameVals = (Set<String>) queryParams.get(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FIRSTNAME);
				Set<String> lastNameVals = (Set<String>) queryParams.get(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_LASTNAME);
				Set<String> activeVals = (Set<String>) queryParams.get(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACTIVE);
				
				if( uidVals != null && !uidVals.isEmpty() ){
					beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_USERNAME, (String) uidVals.iterator().next());
					queryDescriptor.addSearchProperty(beanSearchProperty);
				}
				
				if( fullNameVals != null && !fullNameVals.isEmpty() ){
					beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_DISPLAYNAME, (String) fullNameVals.iterator().next());
					queryDescriptor.addSearchProperty(beanSearchProperty);
				}

				if( firstNameVals != null && !firstNameVals.isEmpty() ){
					beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_FIRSTNAME, (String) firstNameVals.iterator().next());
					queryDescriptor.addSearchProperty(beanSearchProperty);
				}

				
				if( lastNameVals != null && !lastNameVals.isEmpty() ){
					beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_LASTNAME, (String) lastNameVals.iterator().next());
					queryDescriptor.addSearchProperty(beanSearchProperty);
				}

				if( activeVals != null && !activeVals.isEmpty() ){
					beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_ACTIVE, (String) activeVals.iterator().next());
					queryDescriptor.addSearchProperty(beanSearchProperty);
				}

				
			}else{
				throw new RuntimeException("Queryparams are not null and pattern is not *");
			}
			
		}else{
			if( !"*".equals(pattern) ){
				beanSearchProperty = new BeanSearchProperty(User.class, UserPropertyFields.FIELD_PATH_USERNAME, pattern);
				queryDescriptor.addSearchProperty(beanSearchProperty);
			}
		}
			
		beanOrderProperty = new BeanOrderProperty(User.class, UserPropertyFields.FIELD_PATH_USERNAME, true); //ascending order field
		queryDescriptor.addOrderProperty(beanOrderProperty);

		QueryResult<User> usersResult = loadUsers( queryDescriptor );
		
		return usersResult.getResultList();
	}
	
	public String getPropertyStringValue(Map<String, Set<String>> configParams, String key) {
        String value = null;
        Set<String> valueSet = configParams.get(key);
        if (valueSet != null && !valueSet.isEmpty()) {
            value = valueSet.iterator().next();
        } else {
                logger.info("getPropertyStringValue failed"
                        + "to set value for:" + key);
        }
        return value;
    }
	
	/**
	 * Searches for users and returns a list based on the following parameters.
	 * 
	 * @param pattern is the pattern to search.
	 * @param maxResults is the max number of users to search.
	 * @param queryParams are query parameters.
	 * @param filterOp are operations to filter result.
	 * @param queryDescriptor 
	 * @return a List of users meeting the specified criteria.
	 */
	List<Group> searchGroups(String pattern, Map<String, Object> queryParams, int filterOp, QueryDescriptor queryDescriptor){
		logger.info("TarentIdmRMIDao.search called,  "+pattern+", "+queryParams+", "+filterOp);	
//		logger.info("search called");
		
		List<Group> results = new ArrayList<Group>();
		
		QueryResult<Group> groupsResult = null;
		
		if( queryParams == null ) {
		
			if( "*".equals(pattern) ){
				groupsResult = loadGroups( queryDescriptor );
			}else{
				logger.error("Search groups with pattern :"+pattern+", NOT IMPLEMENTED");
			}
		
		}else{
			throw new RuntimeException("Queryparams are not null. NOT IMPLEMENTED");
		}
		
		
		logger.info( "GroupsResult: "+ (groupsResult != null ? groupsResult.toString() : "NULL") );
   		
		if( groupsResult != null ){
			for(int i=0;i< groupsResult.size();i++) {
				results.add( groupsResult.get(i) );
			}
		}
		
		return results;		
	}
	
	/**
	 * Searches for users and returns a list based on the following parameters.
	 * 
	 * @param pattern is the pattern to search.
	 * @param maxResults is the max number of users to search.
	 * @param queryParams are query parameters.
	 * @param filterOp are operations to filter result.
	 * @param queryDescriptor 
	 * @return a List of users meeting the specified criteria.
	 */
	List<Role> searchRoles(String pattern, Map<String, Object> queryParams, int filterOp, QueryDescriptor queryDescriptor){
		logger.info("TarentIdmRMIDao.searchRoles called,  "+pattern+", "+queryParams+", "+filterOp);	
//		logger.info("search called");
		
		List<Role> results = new ArrayList<Role>();
		
		QueryResult<Role> rolesResult = null;
		
		if( queryParams == null ) {
		
			if( "*".equals(pattern) ){
				rolesResult = loadRoles( queryDescriptor );
			}else{
				logger.error("Search roles with pattern :"+pattern+", NOT IMPLEMENTED");
			}
		
		}else{
			throw new RuntimeException("Queryparams are not null. NOT IMPLEMENTED");
		}
			
		logger.info( "RolesResult: " + ( rolesResult != null ? rolesResult.toString() : "NULL" ));
   		
		if( rolesResult != null ){
			for(int i=0;i< rolesResult.size();i++) {
				results.add( rolesResult.get(i) );
			}
		}
		
		return results;		
	}
	
	/**
	 * Using a userName and password, authenticates a user
	 * 
	 * @param userName of the user to be authenticated
	 * @param password of the user to be authenticated
	 * @return
	 */
	public boolean authenticateUser(String userName, String password){
		boolean authSuccess = false;
		
		try {
			authSuccess = authenticationService.authenticate( clientName, userName, password);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
		} catch (BackendException e) {
			e.printStackTrace();
		}
		
		return authSuccess;
		
	} 
	
	public Set<String> getAllUsersInGroups(SSOToken token, IdType type, String name, IdType membersType){
		
		logger.info("TarentIdmRMIDao.getAllUsersInGroups called, Group Name=" + name);
		Set<String> idResults = null;
        try {
            idResults = new HashSet<String>();
            List<Account> res = loadAllUsersInGroup( name );

            if (res != null) {
                Iterator<Account> it = res.iterator();
                while (it.hasNext()) {
                    Account userAccount = (Account) it.next();
                    idResults.add( userAccount.getUsername() );
                }
            }
            
        } catch (Exception ex) {
            //processException(ex);
        	logger.info(ex.getMessage());
        }
        
        return idResults;

	}
	
	public Set<String> getAllUsersInRoles(SSOToken token, IdType type,
			String name, IdType membersType) throws IdRepoException {
		logger.info("TarentIdmRMIDao.getAllUsersInRoles called, Role Name=" + name);
		Set<String> idResults = null;
        try {
            idResults = new HashSet<String>();
            List<Account> res = loadAllUsersInRole( name );

            if (res != null) {
                Iterator<Account> it = res.iterator();
                while (it.hasNext()) {
                    Account userAccount = (Account) it.next();
                    idResults.add( userAccount.getUsername() );
                }
            }
            
        } catch (Exception ex) {
            //processException(ex);
        	logger.info(ex.getMessage());
        }
        
        return idResults;
	}
	
	// -------------------------------------- Private methods ---------------------------------------------------

	public Group loadGroup(String groupName) 
					throws IdRepoException {
	logger.info("TarentIdmRMIDao.loadGroup called, Name="+groupName);	
	
		Group group = null;
		try {
			group = groupAndRoleManagementService.getGroupByName(clientName, groupName);
			//printUser(user);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException( e.getMessage().toString(), TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		
		return group;
	}
	
	public List<Account> loadAllUsersInGroup(String groupName) 
			throws IdRepoException {
		List<Account> usersInGroups = null;
		
//		Group group = null;
		try {
//			group = groupAndRoleManagement.getGroupByName(clientName, groupName);
			usersInGroups = groupAndRoleManagementService.getUsersByGroup(clientName, groupName);
			// printUser(user);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException(
					e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException(e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		return usersInGroups;
	}
	
	public List<Account> loadAllUsersInRole(String roleName)
			throws IdRepoException {
		List<Account> roleUsers = null;
		
		try {
//			String roleScope = "test-Scope";
			roleUsers = groupAndRoleManagementService
					.getUsersByRoleId(this.clientName, new Long(roleName), null);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException(
					e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException(e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}
		return roleUsers;
	}

		
	public QueryResult<User> loadUsers(QueryDescriptor queryDescriptor) {
    	QueryResult<User> users = null;
    	try {
			users = userManagementService.getUsers(this.clientName, queryDescriptor);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
		} catch (BackendException e) {
			e.printStackTrace();
		}
		logger.info("LoadUsers called "+users);
		return users;
    }
	
	public QueryResult<Group> loadGroups(QueryDescriptor queryDescriptor) {
    	QueryResult<Group> groups = null;
    	try {
    		groups = groupAndRoleManagementService.getGroups(this.clientName, queryDescriptor);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
		} catch (BackendException e) {
			e.printStackTrace();
		}
		logger.info("LoadUsers called "+groups);
		return groups;
    }

	public Role loadRole(String roleName) throws IdRepoException {
		logger.info("TarentIdmRMIDao.loadRole called, Name=" + roleName);

		Role role = null;
		try {
			// TODO what about Application name
			role = groupAndRoleManagementService.getRole(clientName, new Long( roleName ));
			logger.info( "Loaded role: "+role );
			// printUser(user);
		} catch (IllegalRequestException e) {
			e.printStackTrace();
			throwCustomIdRepoException(
					e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_ILLEGAL_REQUEST_EXCPETION_CODE);
		} catch (BackendException e) {
			e.printStackTrace();
			throwCustomIdRepoException(e.getMessage().toString(),
					TarentDsPluginConstants.TARENT_IDM_BACKEND_EXCEPTION_CODE);
		}

		return role;
	}
	
	public QueryResult<Role> loadRoles(QueryDescriptor queryDescriptor) {
    	QueryResult<Role> roles = null;
    	try {
    		roles = groupAndRoleManagementService.getRolesByClient(this.clientName, queryDescriptor);
    		//String roleName0 = roles.get(0).getName() + " (" + roles.get(0).getApplication().getName() + ")";
		} catch (IllegalRequestException e) {
			e.printStackTrace();
		} catch (BackendException e) {
			e.printStackTrace();
		}
		logger.info("TarentIdmRMIDao.loadRoles called "+roles);
		return roles;
	}
	
	private void printUser(User u) {
		logger.info("User: Pwd: ********"+", PwdRepeat: ********"+", Fstname:"+", "+u.getFirstname()+
				", Username:"+u.getUsername()+", Id:"+u.getUuid()+", Lstname:"+u.getLastname());
	}
	
	
	void throwCustomIdRepoException( String errorMessage, String errorCode) throws IdRepoException{
		String args[] =  { errorMessage } ;
		throw new IdRepoException(IdRepoBundle.BUNDLE_NAME, errorCode, args);
	}

	public void checkExtraFields(Map<String, Object> props)
			throws IdRepoException {
		
		if( 	props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_EMAIL_ADDRESS) || 
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_TELEPHONE_NUMBER) ||
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_HOME_ADDRESS) || 
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_EMPLOYEE_NUMBER) )
		{
			throwCustomIdRepoException( props.toString(),
					TarentDsPluginConstants.TARENT_IDM_EXTRA_FEATURES_EXCEPTION_CODE);
		}
		
	}

	public void checkUnSupportedFields(Map<String, Object> props) 
			throws IdRepoException {

		if( 	props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACCOUNT_EXPIRATION_DATE) || 
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACCOUNT_MSISDN_NUMBER) ||
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACCOUNT_SUCCESS_URL) || 
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACCOUNT_FAILURE_URL) || 
				props.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACCOUNT_USER_ALIAS_LIST) )
		{
			throwCustomIdRepoException( props.toString(),
					TarentDsPluginConstants.TARENT_IDM_OPERATIONS_NOT_SUPPORTED_EXCEPTION_CODE);
		}
		
	}
	
	
}

