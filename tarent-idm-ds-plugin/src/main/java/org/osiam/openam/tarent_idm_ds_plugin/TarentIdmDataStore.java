/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.openam.tarent_idm_ds_plugin;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;

import com.iplanet.sso.SSOException;
import com.iplanet.sso.SSOToken;
import com.sun.identity.authentication.spi.AuthLoginException;
import com.sun.identity.idm.IdOperation;
import com.sun.identity.idm.IdRepo;
import com.sun.identity.idm.IdRepoBundle;
import com.sun.identity.idm.IdRepoException;
import com.sun.identity.idm.IdRepoListener;
import com.sun.identity.idm.IdRepoUnsupportedOpException;
import com.sun.identity.idm.IdType;
import com.sun.identity.idm.RepoSearchResults;
import com.sun.identity.sm.SchemaType;



/**
 * A plugin for OpenAM providing an interface for TarentIDM as Data Store.
 * 
 * @author Umer Kayani (u.kayani@tarent.de)
 *
 */
public class TarentIdmDataStore extends IdRepo {
	
	private TarentIdmRMIDao tarentIdmRMIDao = null;
	private IdRepoListener listener;
	private static Logger logger = Logger.getLogger( TarentIdmDataStore.class.getName() );
	private static String endpointURI;
	private static String clientName;
	

	private static final Set<IdType> SUPPORTED_TYPES;
	static 
	{
		SUPPORTED_TYPES = new HashSet<IdType>();
		SUPPORTED_TYPES.add(IdType.USER);
		SUPPORTED_TYPES.add(IdType.GROUP);
		SUPPORTED_TYPES.add(IdType.ROLE);
//		SUPPORTED_TYPES.add(IdType.REALM);
	}

	private static final Set<IdOperation> SUPPORTED_OPS_GROUPS_AND_ROLES;
	static 
	{
		SUPPORTED_OPS_GROUPS_AND_ROLES = new HashSet<IdOperation>();
		SUPPORTED_OPS_GROUPS_AND_ROLES.add(IdOperation.READ);
	}

	private static final Set<IdOperation> SUPPORTED_OPS_USERS;
	static 
	{
		SUPPORTED_OPS_USERS = new HashSet<IdOperation>();
		SUPPORTED_OPS_USERS.add(IdOperation.READ);
		SUPPORTED_OPS_USERS.add(IdOperation.CREATE);
		SUPPORTED_OPS_USERS.add(IdOperation.EDIT);
		SUPPORTED_OPS_USERS.add(IdOperation.DELETE);
	}

	/**
	 * No argument constructor, must be present for OpenSSO to load this plugin. 
	 */
	public TarentIdmDataStore() {
		super();
	}
	

	// ------------------------------------------------ Capability Methods --------------------------------

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getSupportedOperations(com.sun.identity.idm.IdType)
	 */
	public Set<IdOperation> getSupportedOperations(IdType type) {
		logger.info("TarentIdmDataStore.getSupportedOperations called.");

		if( IdType.USER.equals(type) ){
			return SUPPORTED_OPS_USERS;
		}else if(IdType.GROUP.equals(type) || IdType.ROLE.equals(type)) {
			return SUPPORTED_OPS_GROUPS_AND_ROLES;			
		}else{
			return Collections.emptySet();
		}
			
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getSupportedTypes()
	 */
	public Set<IdType> getSupportedTypes() {
		logger.info("TarentIdmDataStore.getSupportedTypes called.");

		return SUPPORTED_TYPES;
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#supportsAuthentication()
	 */
	public boolean supportsAuthentication() {
		boolean returnValue = true;
		logger.info("TarentIdmDataStore.supportsAuthentication called. returnValue="+returnValue );
		
		return returnValue;
	}


	// ------------------------------------------------- Plugin Life Cycle Methods ---------------------------

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#initialize(java.util.Map)
	 */
	public void initialize(Map configParams) {
		super.initialize(configParams);
		
		logger.info("TarentIdmDataStore.initalize called, " 
				+ "Parameters: (configParams="+configParams.toString()+")");
		
		// Get endpoint and clientname from configuration parameters of openam interface.
		TarentIdmDataStore.endpointURI = getPropertyStringValue(configParams, TarentDsPluginConstants.ENDPOINT);
		TarentIdmDataStore.clientName = getPropertyStringValue(configParams, TarentDsPluginConstants.CLIENTNAME);
		
        if( endpointURI != null && clientName != null){
	        logger.info("***************Initialization Parameters********************");
	        logger.info("RMIEnd-point: "+endpointURI);
	        logger.info("Clientname: "+clientName);
	        logger.info("************************************************************");

			try {
				tarentIdmRMIDao = new TarentIdmRMIDao(endpointURI, clientName);
				tarentIdmRMIDao.init();
			} catch (NamingException e) { 
				logger.error( "NamingException: "+e.getMessage() );
			}
        
        }else{
        	logger.error("RMIEnd-point or Client-name missing...");
        }
		
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#shutdown()
	 */
	public void shutdown() {
		super.shutdown();
		logger.info("TarentIdmDataStore.shutdown called.");
		tarentIdmRMIDao.shutdown();
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#addListener(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdRepoListener)
	 */
	public int addListener(SSOToken token, IdRepoListener listener)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.addListener called, " 
				+ "Parameters: (SSOToken="+token+", IdRepoListener="+listener+")");
		// TODO: needs to be discussed
		this.setListener(listener);
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#removeListener()
	 */
	public void removeListener() {
		logger.info("TarentIdmDataStore.removeListener called.");
		// TODO: needs to be discussed
		this.setListener(null);
	}


	// -- READ identity operations ----
	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#isExists(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String)
	 */
	public boolean isExists(SSOToken token, IdType type, String name)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.isExists called, " 
				+ "Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+")");
		assertUserType(type);
		boolean result = tarentIdmRMIDao.isExists( name );
		logger.info("TarentIdmDataStore.isExists() returns with: "+result);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#isActive(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String)
	 */
	public boolean isActive(SSOToken token, IdType type, String name)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.isActive called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+")");
		assertUserType(type);
		boolean active = tarentIdmRMIDao.isActive( name );
		logger.info("TarentIdmDataStore.isActive() returns with: "+active);
		return active;
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String)
	 */
	public Map<String, Set<Object>> getAttributes(SSOToken token, IdType type, String name)
			throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getAttributes called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+")");
		Map<String, Set<Object>> attributesMap = null;
		
		if( IdType.USER.equals(type) ){
			User user;
			user = tarentIdmRMIDao.loadUser(name);
			attributesMap = userToAttrs( user );
		}else if( IdType.GROUP.equals(type) ){
			Group group;
			group = tarentIdmRMIDao.loadGroup(name);
			attributesMap = groupToAttrs( group );
		}else if( IdType.ROLE.equals(type) ){
			Role role;
			role = tarentIdmRMIDao.loadRole( name );
			attributesMap = roleToAttrs( role );
		}else{
			logger.error("Undefined IdType: getAttributes (type)="+type);
		}
		
		logger.info("TarentIdmDataStore.getAttributes() returns with: "+ attributesMap); 
		return attributesMap;
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Set)
	 */
	public Map<String, Set<Object>> getAttributes(SSOToken token, IdType type, String name, Set attrNames)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getAttributes called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name
				+ ", Subset Attributes=" + attrNames+")");

		// Return subset of all attributes
        Map<String, Set<Object>>  answer = (attrNames == null) ? null : new HashMap<String, Set<Object>>();
        Map<String, Set<Object>>  map = getAttributes(token, type, name);
        if (attrNames == null) {
            answer = map;
        } else {
            for (Iterator<String> items = attrNames.iterator(); items.hasNext();) {
                Object key = items.next();
                Set<Object> value = map.get(key);
                if (value != null) {
                    answer.put(key.toString(),  value);
                }
            }
        }

        return (answer);
		
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getBinaryAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Set)
	 */
	public Map getBinaryAttributes(SSOToken token, IdType type, String name, Set attrNames)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getBinaryAttributes called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+ name +
				", Subset Attributes=" + attrNames+")");
		assertUserType(type);
		// TODO: Not sure about binary attributes,
		return Collections.EMPTY_MAP;
	}
		
	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#search(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, int, int, java.util.Set, boolean, int, java.util.Map, boolean)
	 */
	public RepoSearchResults search(SSOToken token, IdType searchType,
			String pattern, int maxTime, int maxResults,
			Set returnAttrs, boolean returnAllAttrs, int filterOp,
			Map avPairs, boolean recursive)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.search called, " +
				"Parameters: (SSOToken="+token+", "+searchType+", Pattern="+pattern+", MaxTime="+maxTime+
				", MaxResults="+maxResults+", ReturnAttributes="+returnAttrs+", ReturnAllAttributes="+returnAllAttrs +
				", FilterOp="+filterOp+", AvPairs="+avPairs+", Recursive="+recursive+")");
		
		
		Map<String, Object> queryParams = avPairs==null ? null : remap(avPairs);
		Set<String> searchResults = new HashSet<String>();
		int errorCode = RepoSearchResults.SUCCESS;
		Map<String, Map<String, Set<Object>>> resultsMap = null;
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		//TODO: Bug fix for idm, move this into backend later
		if(maxResults == 0)
			maxResults++;
		queryDescriptor.setLimit( maxResults );

		assertAllAllowedTypes( searchType );
		
		if( IdType.USER.equals(searchType) ){
			List<User> users = tarentIdmRMIDao.searchUsers(pattern, queryParams, filterOp, queryDescriptor);
			logger.info("    User search results => "+users);
			searchResults = listToUsernameSet(users);
			resultsMap = usersToEntryAttributeSetMap(users);
		}else if( IdType.GROUP.equals(searchType) ){
			List<Group> groups = tarentIdmRMIDao.searchGroups(pattern, queryParams, filterOp, queryDescriptor);
			logger.info("    Group search results => "+groups);
			searchResults = listToGroupnameSet(groups);
			resultsMap = groupsToEntryAttributeSetMap(groups);
		}else if( IdType.ROLE.equals(searchType) ){
			List<Role> roles = tarentIdmRMIDao.searchRoles(pattern, queryParams, filterOp, queryDescriptor);
			logger.info("    Role search results => "+roles);
			searchResults = listToRolenameSet(roles);
			resultsMap = rolesToEntryAttributeSetMap(roles);
		}else{
			logger.error("Undefined IdType: searchtype="+searchType);
		}
		
		return new RepoSearchResults(searchResults,
		                             errorCode,
		                             resultsMap,
		                             searchType);
	}
	

	// -- CREATE identity operations ----
	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#create(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Map)
	 */
	public String create(SSOToken token, IdType type, String name,
	                     Map attrMap)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.create called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+")");

		assertUserType(type);
		User newUser = userFromAttributes(name, attrMap);		
		tarentIdmRMIDao.createUser( newUser );

		return "["+name+"]";
	}


	// -- EDIT identity operations ----

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#setAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Map, boolean)
	 */
	public void setAttributes(SSOToken token, IdType type, String name, Map attributes, boolean isAdd)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.setAttributes called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ", AttributeMap="+attributes+", isAdd="+isAdd+")");
		assertUserType(type);
		// load user and copy changed properties
		User targetUser = tarentIdmRMIDao.loadUser(name);
		
		Map<String, Object> changedAttributes = attrMapToPropMap(attributes);
		if( changedAttributes != null ){
			logger.info( "Attributes to be updated="+changedAttributes.toString() );
			tarentIdmRMIDao.checkExtraFields( changedAttributes );
			tarentIdmRMIDao.checkUnSupportedFields( changedAttributes );
		}
		
		if( targetUser != null){
			logger.info( "User loaded: "+targetUser.toString() );
			if( changedAttributes.containsKey(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_PASSWORD) 
					&& changedAttributes.size() == 1){
				try {
					logger.info("Password Change request: "+
							tarentIdmRMIDao.changePassword(targetUser.getUsername(), 
									changedAttributes.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_PASSWORD ).toString()));
				} catch (IllegalRequestException e) {
					e.printStackTrace();
					throw new IdRepoUnsupportedOpException("IllegalRequestException: "+e.getMessage());
				} catch (BackendException e) {
					e.printStackTrace();
					throw new IdRepoUnsupportedOpException("BackendException: "+e.getMessage());
				}
				return;
			}
			copyProperties(targetUser, changedAttributes);
			tarentIdmRMIDao.updateUser(targetUser);
		}else 
			logger.info("Cannot update user because it is NULL");
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#setBinaryAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Map, boolean)
	 */
	public void setBinaryAttributes(SSOToken token, IdType type, String name, Map attributes, boolean isAdd)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.setBinaryAttributes calle, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ", AttributeMap="+attributes+", isAdd="+isAdd+")");
		assertUserType(type);
		// TODO: Don't know about Binary Attributes. 
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#removeAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Set)
	 */
	public void removeAttributes(SSOToken token, IdType type, String name, Set attrNames)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.removeAttributes called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ")");
		assertUserType(type);
		// at the moment, there are no attributes that can resonably be
		// removed.  We could in theory set the columns to 'null' in
		// the database, except we've deliberately defined the columns
		// to dissallow this.
	}


	// -- DELETE operations ----

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#delete(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String)
	 */
	public void delete(SSOToken token, IdType type, String name)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.delete called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ")");
		assertUserType(type);
		tarentIdmRMIDao.deleteUser( name );
	}


	// -- authentication operations ----

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#authenticate(javax.security.auth.callback.Callback[])
	 */
	public boolean authenticate(Callback[] credentials) 
		throws IdRepoException, AuthLoginException {
		logger.info("TarentIdmDataStore.authenticate called. sizeof Callback[] =" + (credentials != null ? credentials.length : "null") );
		// Obtain user name and password from credentials and authenticate
		String username = null;
		String password = null;
		if( credentials != null ){
			for (int i = 0; i < credentials.length; i++) {
				Callback cred = credentials[i];
				if (cred instanceof NameCallback) {
					username = ((NameCallback)cred).getName();
				} else if (cred instanceof PasswordCallback) {
					char[] passwd = ((PasswordCallback)cred).getPassword();
					if (passwd != null) {
						password = new String(passwd);
					}
				}
			}
		}
		
//		logger.info("Username and password from credentials: "+(username!=null?username:"null")+
//				(password!=null?password:"null")  );
		
		if (username == null || password == null) {
			Object args[] = { getClass().getName() };
			throw new IdRepoException(IdRepoBundle.BUNDLE_NAME, "221", args);
		}
		User user = new User();
		user = tarentIdmRMIDao.loadUser( username );
		boolean userAuthenticated = false;
		userAuthenticated = tarentIdmRMIDao.authenticateUser(username, password);
		
		boolean authenticationResult = (user != null) && userAuthenticated;
		logger.info("Result of authenticate: "+ authenticationResult);
		return authenticationResult;
	}


	// -- membership operations ----

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#modifyMemberShip(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Set, com.sun.identity.idm.IdType, int)
	 */
	public void modifyMemberShip(SSOToken token, IdType type, String name, Set members, IdType membersType, int operation)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.modifyMemberShip called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ 
				", Members="+members+", MemberType="+membersType+", Operation="+operation+")");
		throw new IdRepoUnsupportedOpException("only USER type supported; users can't have members");
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getMembers(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, com.sun.identity.idm.IdType)
	 */
	public Set<String> getMembers(SSOToken token, IdType type, String name, IdType membersType)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getMembers called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ", MemberType="+membersType+")");
		Set<String> members = null;
		
		if( IdType.GROUP.equals( type ) ){
			members = tarentIdmRMIDao.getAllUsersInGroups(token, type, name, membersType);
		}else if( IdType.ROLE.equals( type ) ){
			members = tarentIdmRMIDao.getAllUsersInRoles(token, type, name, membersType);
		}else if( IdType.USER.equals( type ) ){
			throw new IdRepoUnsupportedOpException("Users can't have members");
		}
		
		if (members != null)
			return members;
		else
			return Collections.emptySet();
			
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getMemberships(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, com.sun.identity.idm.IdType)
	 */
	public Set getMemberships(SSOToken token, IdType type, String name, IdType membershipType)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getMemberships called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ", MembershipType="+membershipType+")");

        // Memerships can be returned for users and agents
        if (!type.equals(IdType.USER) && !type.equals(IdType.AGENT)) {
            logger.info(
                "TarentIdmDataStore:getMemberships supported for users and agents");
            Object args[] = { "TarentIdmDataStore:getMemberships supported for users and agents" };
            throw (new IdRepoException(IdRepoBundle.BUNDLE_NAME, "206", args));
        }

        // Set to maintain the members
        Set<Object> results = new HashSet<Object>();
        if (membershipType.equals(IdType.ROLE)) {
            // Get the role attribute and return
            Set<String> returnAttrs = new HashSet<String>();
            returnAttrs.add( TarentDsPluginConstants.ROLE_MEMBERSHIP_ATTRIBUTE );
            Map<String, Set<Object>> attrs = getAttributes(token, type, name, returnAttrs);
            if (attrs != null) {
                Set<Object> roles = (Set<Object>) attrs.get( TarentDsPluginConstants.ROLE_MEMBERSHIP_ATTRIBUTE );
                if (roles != null) {
                    results = roles;
                }
            }
        } else if (membershipType.equals(IdType.GROUP)) {
            // Get the list of groups and search for memberships
            Set<String> returnAttrs = new HashSet<String>();
            returnAttrs.add( TarentDsPluginConstants.GROUP_MEMBERSHIP_ATTRIBUTE );
            RepoSearchResults allGroups = search(token, membershipType, "*", 0,
                    0, returnAttrs, false, IdRepo.OR_MOD, null, false);
            Map groupAttrs = null;
            if ((allGroups != null) 
            		&& ((groupAttrs = allGroups.getResultAttributes()) != null)
            ) {
                // Prefix name with IdType
                name = type.getName() + name;
                for (Iterator i = groupAttrs.entrySet().iterator(); i.hasNext();){
                    String sname = (String) i.next();
                    Map attrs = (Map) groupAttrs.get(sname);
                    Set ids = (Set) attrs.get( TarentDsPluginConstants.GROUP_MEMBERSHIP_ATTRIBUTE );
                    if (ids != null && ids.contains(name)) {
                        results.add(sname);
                    }
                }
            }
        } else {
            // throw unsupported operation exception
            Object args[] = { "NAME", IdOperation.READ.getName(),
                membershipType.getName() };
            throw new IdRepoUnsupportedOpException(IdRepoBundle.BUNDLE_NAME, "305", args);
        }
        return (results);
	}


	// -- SERVICE operations ----

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#assignService(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.lang.String, com.sun.identity.sm.SchemaType, java.util.Map)
	 */
	public void assignService(SSOToken token, IdType type, String name, String serviceName, SchemaType stype, Map attrMap)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.assignService called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ 
				", ServiceName="+serviceName+", SchemaType="+stype+", AttrMap="+attrMap+")");
		throw new IdRepoUnsupportedOpException("SERVICE operations not supported");
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getAssignedServices(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.util.Map)
	 */
	public Set getAssignedServices(SSOToken token, IdType type, String name, Map mapOfServicesAndOCs)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getAssignedServices called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ ", MapOfServicesAndOCs="+mapOfServicesAndOCs+")");
//		throw new IdRepoUnsupportedOpException("SERVICE operations not supported");
		return Collections.EMPTY_SET;
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#unassignService(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.lang.String, java.util.Map)
	 */
	public void unassignService(SSOToken token, IdType type, String name, String serviceName, Map attrMap)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.unassignService called, " +
				"Parameters: (SSOToken="+token+", IdType="+type+", Name="+name+ 
				", ServiceName="+serviceName+", AttrMap="+attrMap+")");
		throw new IdRepoUnsupportedOpException("SERVICE operations not supported");
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getServiceAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.lang.String, java.util.Set)
	 */
	public Map getServiceAttributes(SSOToken token, IdType type, String name, String serviceName, Set attrNames)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getServiceAttributes called, " +
				"Parameters=(SSOToken="+token+", IdType="+type+", Name="+name+", ServiceName="+serviceName+", AttrName="+attrNames);
		return Collections.EMPTY_MAP;
		
//		TODO: Call this method for a generalized processing later, if necessary 
//      return(getServiceAttributes(token, type, name, serviceName,
//      attrNames, true));
		
	}
	
    /*
     * (non-Javadoc)
     * 
     * @see com.sun.identity.idm.IdRepo#getServiceAttributes(
     *      com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType,
     *      java.lang.String, java.lang.String, java.util.Set)
     */
//    private Map getServiceAttributes(SSOToken token, IdType type, String name,
//        String serviceName, Set attrNames, boolean isString)
//        throws IdRepoException, SSOException {
//        
//    	logger.info("TarentIdmDataStore.getServiceAttributes called. " + name +
//            "\n\t" + serviceName + "=" + attrNames);
//
//        if (!type.equals(IdType.USER) && !type.equals(IdType.ROLE) &&
//            !type.equals(IdType.REALM)
//        ) {
//            // Unsupported Operation
//            Object args[] = { "NAME", IdOperation.SERVICE.getName() };
//            throw new IdRepoUnsupportedOpException(IdRepoBundle.BUNDLE_NAME,
//                    "305", args);
//        }
//
//        // Get attributes from Identity Object
//        Map results = (isString ?
//            getAttributes(token, type, name, attrNames)
//            : getBinaryAttributes(token, type, name, attrNames));
//        if (results == null) {
//            results = new HashMap();
//        }
//
//        // For types role and realm, return the attributes
//        if (!type.equals(IdType.USER)) {
//        	logger.info("Return result: "+results.toString());
//            return (results);
//        }
//
//        // Get the roles for the identity and add the service attributes
//        Set roles = getMemberships(token, type, name, IdType.ROLE);
//        for (Iterator items = roles.iterator(); items.hasNext();) {
//            String role = (String) items.next();
//            Map roleAttrs = Collections.EMPTY_MAP;
//            try {
//                roleAttrs = (isString ?
//                    getAttributes(token, IdType.ROLE, role, attrNames)
//                    : getBinaryAttributes(token, IdType.ROLE,
//                        role, attrNames));
//            } catch (FilesRepoEntryNotFoundException fnf) {
//                roleAttrs = Collections.EMPTY_MAP; 
//            }
//            // Add the attributes to results
//            for (Iterator ris = roleAttrs.keySet().iterator(); ris.hasNext();) {
//                Object roleAttrName = ris.next();
//                Object roleAttrValues = (Object) roleAttrs.get(roleAttrName);
//                Object idAttrValues = (Object) results.get(roleAttrName);
//                if (idAttrValues == null) {
//                    results.put(roleAttrName, roleAttrValues);
//                } else {
//                    if (isString) {
//                        ((Set) idAttrValues).addAll((Set) roleAttrValues);
//                    } else {
//                        byte[][] resultsArr =
//                            (byte[][])results.get(roleAttrName);
//                        byte[][] roleArr =
//                            (byte[][])roleAttrs.get(roleAttrName);
//                        resultsArr = combineByteArray(resultsArr, roleArr);
//                        results.put(roleAttrName, resultsArr);
//                    }
//                }
//            }
//        }
//
//        // Get the service attributes for the realm and add it
//        Map realmAttrs = (isString ?
//            getAttributes(token, IdType.REALM,
//                "ContainerDefaultTemplateRole", attrNames)
//            : getBinaryAttributes(token, IdType.REALM,
//                "ContainerDefaultTemplateRole", attrNames));
//        // Add the attributes to results
//        for (Iterator ris = realmAttrs.keySet().iterator(); ris.hasNext();) {
//            Object realmAttrName = ris.next();
//            Object realmAttrValues = (Object) realmAttrs.get(realmAttrName);
//            Object idAttrValues = (Object) results.get(realmAttrName);
//            if (idAttrValues == null) {
//                results.put(realmAttrName, realmAttrValues);
//            } else {
//                // combine the values
//                if (isString) {
//                    ((Set) idAttrValues).addAll((Set) realmAttrValues);
//                } else {
//                    byte[][] resultsArr = (byte[][]) results.get(realmAttrName);
//                    byte[][] realmArr = (byte[][]) realmAttrs.get(realmAttrName);
//                    resultsArr = combineByteArray(resultsArr, realmArr);
//                    results.put(realmAttrName, resultsArr);
//                }
//            }
//        }
//        
//        logger.info("Return result: "+results.toString());
//        return (results);
//    }
	
	

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#modifyService(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.lang.String, com.sun.identity.sm.SchemaType, java.util.Map)
	 */
	public void modifyService(SSOToken token, IdType type, String name, String serviceName, SchemaType sType, Map attrMap)
		throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.modifyService called, " +
				"Parameters=(SSOToken="+token+", IdType="+type+", Name="+name+
				", ServiceName="+serviceName+", ServiceName="+serviceName+", SchemaType="+sType+", AttrMap="+attrMap+")");
		throw new IdRepoUnsupportedOpException("SERVICE operations not supported");
	}


	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#setActiveStatus(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, boolean)
	 */
	@Override
	public void setActiveStatus(SSOToken token, IdType type, String name,
			boolean active) throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.setActiveStatus called, " +
				"Parameters=(SSOToken="+token+", IdType="+type+", Name="+name+ ", Active="+active+")");
		throw new IdRepoUnsupportedOpException("SERVICE operations not supported");
		
	}

	/* (non-Javadoc)
	 * @see com.sun.identity.idm.IdRepo#getBinaryServiceAttributes(com.iplanet.sso.SSOToken, com.sun.identity.idm.IdType, java.lang.String, java.lang.String, java.util.Set)
	 */
	@Override
	public Map getBinaryServiceAttributes(	SSOToken token, IdType type, String name, 
											String password, Set attrNames) 
								throws IdRepoException, SSOException {
		logger.info("TarentIdmDataStore.getBinaryServiceAttributes called, " +
				"Parameters=(SSOToken="+token+", IdType="+type+", Name="+name+ ", Password="+password+", AttrNames="+attrNames+")");
		// TODO Auto-generated method stub
		assertUserType(type);
		return Collections.EMPTY_MAP;
	}
	
	// -------------------------------------- Private helper methods ------------------------------------------

	/**
	 * Converts an Attribute Map to Properties Map.
	 * 
	 * @param attrMap is the input attributes map.
	 * @return a Map containing properties 
	 */
	private Map<String, Object> attrMapToPropMap(Map attrMap) {
		// flatten the set of values from the given Map, which are
		// Sets, so that the values in the result Map are the first
		// (usually only) entry from each Set,
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator i = attrMap.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry attr = (Map.Entry) i.next();
			String key = (String)attr.getKey();
//			key = PROP_REMAPPER.getDbPropForSSOProp(key);
			Object val = ((Set)attr.getValue()).toArray()[0];
			result.put(key, val);
		}
		return result;
	}

	/**
	 * @param attrMap
	 * @return
	 */
	private Map<String, Object> remap(Map attrMap) {
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Map.Entry<String, String>> i = attrMap.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry<String, String> attr = (Map.Entry<String, String>)i.next();
			String key = (String)attr.getKey();
//			key = PROP_REMAPPER.getDbPropForSSOProp(key);
			result.put(key, attr.getValue());
		}
		return result;
	}

	/**
	 * Convert a List of Users to a set containing email addresses.
	 * 
	 * @param users is a list of users.
	 * @return a set containing email addresses of users.
	 */
	private Set<String> listToUsernameSet(List<User> users) {
		Set<String> result = new HashSet<String>();
		for (Iterator<User> i=users.iterator(); i.hasNext(); ) {
			User user = i.next();
			result.add(user.getUsername());
		}
		return result;
	}

	private Set<String> listToGroupnameSet(List<Group> groups) {
		Set<String> result = new HashSet<String>();
		for (Iterator<Group> i=groups.iterator(); i.hasNext(); ) {
			Group group = i.next();
			result.add(group.getName());
		}
		return result;
	}
	
	private Set<String> listToRolenameSet(List<Role> roles) {
		Set<String> result = new HashSet<String>();
		for (Iterator<Role> i=roles.iterator(); i.hasNext(); ) {
			Role role = i.next();
			result.add( role.getId().toString() );
		}
		return result;
	}
	
	
	/**
	 * Convert a List of Users to Map of entity attributes.
	 * 
	 * @param users is a list of User objects.
	 * @return a Map of entity attributes.
	 */
	private Map<String, Map<String, Set<Object>>> usersToEntryAttributeSetMap(List<User> users) 
	{
		Map<String, Map<String, Set<Object>>> result = new HashMap<String, Map<String, Set<Object>>>();
		for (Iterator<User> i=users.iterator(); i.hasNext(); ) {
			User user = i.next();
			String userName = user.getUsername();
			result.put(userName, userToAttrs(user));
		}
		return result;
	}

	private Map<String, Map<String, Set<Object>>> groupsToEntryAttributeSetMap(List<Group> groups) {
		Map<String, Map<String, Set<Object>>> result = new HashMap<String, Map<String, Set<Object>>>();
		for (Iterator<Group> i=groups.iterator(); i.hasNext(); ) {
			Group group = i.next();
			String displayName = group.getName();
			result.put(displayName, groupToAttrs(group));
		}
		return result;
		
	}
	
	private Map<String, Map<String, Set<Object>>> rolesToEntryAttributeSetMap(List<Role> roles) {
		Map<String, Map<String, Set<Object>>> result = new HashMap<String, Map<String, Set<Object>>>();
		for (Iterator<Role> i=roles.iterator(); i.hasNext(); ) {
			Role role = i.next();
			String displayName = role.getName();
			result.put(displayName, roleToAttrs(role));
		}
		return result;
		
	}

	/**
	 * Creates an object User from attributes.
	 * 
	 * @param name is the username of the new user.
	 * @param attrMap is the Map containing attributes for the user.
	 * @return a user object. 
	 */
	private User userFromAttributes(String name, Map attrMap) {
		logger.info("userFromAttributes called. userName: "+name);
		User newUser = new User();
		Map<String, Object> props = attrMapToPropMap(attrMap);
		//logger.info("userFromAttributes("+name+", "+attrMap+") : props="+props);
		copyProperties(newUser, props);
		newUser.setUsername(name);
		return newUser;
	}

	private void copyProperties(User destUser, Map<String, Object> sourceProps) {
		
		Object firstName = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FIRSTNAME );
		Object lastName = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_LASTNAME );
		Object fullName = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FULLNAME );
		Object password = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_PASSWORD );
		Object active = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACTIVE );
//		Object uuid = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_UUID );
		Object username = sourceProps.get( TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_UID );
		
		if (firstName != null) 
			destUser.setFirstname(firstName.toString());
		if (lastName != null) 
			destUser.setLastname(lastName.toString());
		if (fullName != null) 
			destUser.setDisplayName(fullName.toString());
		if (password != null){ 
			destUser.setPassword(password.toString());
			// Password and repeat password is handled by opensso frontend so safe to do this
			destUser.setPasswordRepeat( password.toString() );
		}
		if (username != null) {
			destUser.setUsername(username.toString());
			destUser.setUuid( username.toString() );
		}
		if (active != null) 
			destUser.setActive( active.toString().toString().equals("Active") );
//		if (uuid != null) 
//			destUser.setUuid( uuid.toString() );
		
	}

	/**
	 * Return the entire set of properties for which the specified bean
     * provides a read method.
	 * 
	 * @param user is a simple bean describing a User.
	 * @return Map
	 */
	private Map<String, String> describeUser(User user) {
		try {
			return BeanUtils.describe( user );
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private Map<String, String> describeGroup(Group group) {
		try {
			return BeanUtils.describe( group );
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private Map<String, String> describeRole(Role role) {
		try {
			return BeanUtils.describe( role );
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}


	/**
	 * Converts a bean object to a Map of attribute values pairs.
	 * 
	 * @param user is a simple user bean.
	 * @return a Map containing attribute values pairs.
	 */
	private Map<String, Set<Object>> userToAttrs(User user) {
		logger.info("TarentIdmDataStore.userToAttributes called, " +
				"Parameter: User="+(user!=null?user:"null"));
		
		Map<String, Set<Object>> attrs = new HashMap<String, Set<Object>>();
		
		if( user != null){
			Map<String, String> props = describeUser(user);
			logger.info("Describing user: "+props);
			for (Iterator<Map.Entry<String, String>> i=props.entrySet().iterator(); i.hasNext(); ) {
				Map.Entry<String, String> prop = (Map.Entry<String, String>) i.next();
				String attrName = "";
	//			PROP_REMAPPER.getSSOPropForDBProp((String)prop.getKey());
				attrs.put(attrName,
				          Collections.singleton((Object)prop.getValue()));
			}
			
			Object firstName = user.getFirstname();
			Object lastName = user.getLastname();
			Object fullName = user.getDisplayName();
			Object password = user.getPassword();
			Object passwordRepeat = user.getPasswordRepeat();
	//		Object emailAddress = sourceProps.get("unicodePwd");
			boolean active = user.isActive();
			Object uuid = user.getUuid();
			Object username = user.getUsername();
			
			if( firstName != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FIRSTNAME, Collections.singleton( firstName ));
			if( lastName != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_LASTNAME, Collections.singleton( lastName ));
			if( username != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_UID, Collections.singleton( (Object)String.valueOf( username )));
			if ( uuid != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_UUID, Collections.singleton( (Object)String.valueOf( uuid )));
			if( password != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_PASSWORD, Collections.singleton( (Object)password));
			if( passwordRepeat != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_PASSWORD_REPEAT, Collections.singleton( (Object)passwordRepeat));
			if( fullName != null )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_FULLNAME, Collections.singleton( (Object)user.getDisplayName())); //TODO: No exact match getFullName <> getDisplayName 
			if( active )
				attrs.put(TarentDsPluginConstants.IDREPO_USER_ATTRIBUTE_ACTIVE, Collections.singleton( (Object)"Active"));
		
		}
		
		return attrs;
	}
	
	private Map<String, Set<Object>> groupToAttrs(Group group) {
		logger.info("TarentIdmDataStore.groupToAttrs called, " +
				"Parameter: Group="+ (group!=null?group:"null"));
		
		Map<String, Set<Object>> attrs = new HashMap<String, Set<Object>>();
		
		if( group != null ){
			Map<String, String> props = describeGroup(group);
			logger.info("Describing group: "+props);
			
			for (Iterator<Map.Entry<String, String>> i=props.entrySet().iterator(); i.hasNext(); ) {
				Map.Entry<String, String> prop = (Map.Entry<String, String>)i.next();
				String attrName = "";
	//			PROP_REMAPPER.getSSOPropForDBProp((String)prop.getKey());
				attrs.put(attrName,
				          Collections.singleton((Object)prop.getValue()));
			}
			
	//		Object id = group.getId().toString();
			Object xmlId = group.getXmlID();
			Object name = group.getName();
			Object breakableLabel = group.getBreakableLabel();
			Object displayName = group.getDisplayName();
	//		Object syncStatus = group.getSyncStatus();
	//		boolean writeProtected = group.isWriteProtected();
			Object clientNameIgnoreXmlFlag = group.getClientNameIgnoreXmlFlag();
			
	//		if( id != null )
	//			attrs.put(TarentDsPluginConstants.IDREPO_GROUP_ID, Collections.singleton( id ));
			if( xmlId != null)
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_XML_ID, Collections.singleton( xmlId ));
			if( name != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_NAME, Collections.singleton( name ));
			if( breakableLabel != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_BREAKABLE_LABEL, Collections.singleton( breakableLabel ));
			if( displayName != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_DISPLAYNAME, Collections.singleton( displayName ));
	//		if( syncStatus != null )
	//			attrs.put(TarentDsPluginConstants.IDREPO_GROUP_SYNC_STATUS, Collections.singleton( syncStatus ));
	//		if( writeProtected )
	//			attrs.put(TarentDsPluginConstants.IDREPO_GROUP_WRITE_PROTECTED, Collections.singleton( (Object)"WriteProtected") );
			if( clientNameIgnoreXmlFlag != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_CLIENT_NAME_IGNORE_XML_FLAG, Collections.singleton( clientNameIgnoreXmlFlag ));
		}
		
		return attrs;
	}
	
	private Map<String, Set<Object>> roleToAttrs(Role role) {
		logger.info("TarentIdmDataStore.roleToAttrs called, " +
				"Parameter: Role="+ (role!=null?role:"null"));
		
		Map<String, Set<Object>> attrs = new HashMap<String, Set<Object>>();
		
		if( role != null ){
			
			Map<String, String> props = describeRole(role);
			logger.info("Describing role: "+props);
			
			for (Iterator<Map.Entry<String, String>> i=props.entrySet().iterator(); i.hasNext(); ) {
				Map.Entry<String,String> prop = (Map.Entry<String,String>)i.next();
				String attrName = "";
	//			PROP_REMAPPER.getSSOPropForDBProp((String)prop.getKey());
				attrs.put(attrName,
				          Collections.singleton((Object)prop.getValue()));
			}
			
			Object xmlId = role.getXmlID();
			Object name = role.getName();
			Object displayName = role.getDisplayName();
			Object clientNameIgnoreXmlFlag = role.getClientNameIgnoreXmlFlag();
			
			if( xmlId != null)
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_XML_ID, Collections.singleton( xmlId ));
			if( name != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_NAME, Collections.singleton( name ));
			if( displayName != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_DISPLAYNAME, Collections.singleton( displayName ));
			if( clientNameIgnoreXmlFlag != null )
				attrs.put(TarentDsPluginConstants.IDREPO_GROUP_CLIENT_NAME_IGNORE_XML_FLAG, Collections.singleton( clientNameIgnoreXmlFlag ));
		}
		
		return attrs;
	}
	

	/**
	 * This method raise an exception if the identifier type is not USER.
	 * 
	 * @param type is the type of supported identifier.
	 * @throws IdRepoUnsupportedOpException
	 */
	private void assertUserType(IdType type) 
		throws IdRepoUnsupportedOpException {
		if (	!IdType.USER.equals(type) ){	
			throw new IdRepoUnsupportedOpException("only types USER is supported");
		}
	}
	
	/**
	 * This method raise an exception if the identifier type is not one of 
	 * USER, GROUP or ROLE.  
	 * 
	 * @param type is the type of supported identifier.
	 * @throws IdRepoUnsupportedOpException
	 */
	private void assertAllAllowedTypes(IdType type) 
		throws IdRepoUnsupportedOpException 
	{
		if (	IdType.USER.equals(type)	||
				IdType.GROUP.equals(type)	||
				IdType.ROLE.equals(type)	)
			return; //ok
		else
			throw new IdRepoUnsupportedOpException("only types USER, GROUP & ROLE are supported");
		
	}
	
	public String getPropertyStringValue(Map<String, Set<String>> configParams, String key) {
        String value = null;
        Set<String> valueSet = configParams.get(key);
        if (valueSet != null && !valueSet.isEmpty()) {
            value = valueSet.iterator().next();
        } else {
                logger.info("getPropertyStringValue failed"
                        + "to set value for:" + key);
        }
        return value;
    }


	public void setListener(IdRepoListener listener) {
		this.listener = listener;
	}


	public IdRepoListener getListener() {
		return listener;
	}

//    private byte[][] combineByteArray(byte[][] resultsArr, byte[][] realmArr) {
//        int combinedSize = realmArr.length;
//        if (resultsArr != null) {
//            combinedSize = resultsArr.length + realmArr.length;
//            byte[][] tmpSet = new byte[combinedSize][];
//            for (int i = 0; i < resultsArr.length; i++) {
//                tmpSet[i] = (byte[]) resultsArr[i];
//            }
//            for (int i = 0; i < realmArr.length; i++) {
//                tmpSet[i] = (byte[]) realmArr[i];
//            }
//            resultsArr = tmpSet;
//        } else {
//            resultsArr = (byte[][]) realmArr.clone();
//        }
//        return(resultsArr);
//    }
	
}