/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.openam.tarent_idm_ds_plugin;


import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.relation.permission.model.Group;
import org.junit.Before;
import org.junit.Test;

import com.iplanet.sso.SSOToken;
import com.sun.identity.idm.IdType;

/**
 * Todo:cleanup and fix tests
 *
 */
public class RMITest {
	
	@Test
	public void dummy(){		
	}
	
	/*
	TarentIdmRMIDao dao = null;
	private static Logger logger = Logger.getLogger("RMITest.class");

	@Before
	public void setUp() throws Exception {
		BasicConfigurator.configure(); //init logging
//		dao = new TarentIdmRMIDao("jnp://172.25.0.83:1099", "s3osiam00");
		dao = new TarentIdmRMIDao("jnp://ukayan.bonn.tarent.de:1099", "siam");
		dao.init();
	}
	
	@Test
	public void isUserPresent() throws Exception {
		String userName = "abcdef@test.com";
		System.out.println("Authentication result: "+ dao.authenticateUser(userName, "11111111"));
	
	}

	@Test
	public void checkJnpEndpoint(){
//		System.out.println("Users:"+ dao.searchUsers(null, 100, null, 0));
	}
	
	@Test
	public void changePasswordAndAuthenticate() throws Exception {
		
		System.out.println("Password changed: "+ dao.changePassword("umer10@tarent.de", "password10"));
	}

	
	@Test
	public void searchUsers() throws Exception {
		
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		
		System.out.println("Users: "+ dao.searchUsers("*", null, 0, queryDescriptor) );
	}
	
	
	
	
	@Test
	public void createUser() throws Exception {
		User newUser = new User();
		newUser.setActive(true);
		newUser.setUuid("test@test.com");
		newUser.setUsername("test@test.com");
		newUser.setFirstname("first name");
		newUser.setLastname("last name");
		newUser.setDisplayName("display name");
		newUser.setLastname("last name");
		newUser.setPassword("password10");
//		newUser.setId("umer10ID");
		dao.createUser(newUser);
	}
*/
	
}
