/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp;

import javax.jws.WebService;

import org.osiam.sc_sp_demo.td1.NotAuthExceptionFault;
import org.osiam.sc_sp_demo.td1.SayHelloPortType;
import org.osiam.xacml.securtiy_annotations.Action;
import org.osiam.xacml.securtiy_annotations.AttributeType;
import org.osiam.xacml.securtiy_annotations.ProtectedResource;
import org.osiam.xacml.securtiy_annotations.Resource;



/**
 * say hello demo service.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */

@WebService(targetNamespace = "http://www.osiam.org/sc_sp_demo/td1", 
portName="SayHelloPort",
serviceName="SayHelloService", 
endpointInterface="org.osiam.sc_sp_demo.td1.SayHelloPortType")
public class SayHelloPortTypeImpl implements  SayHelloPortType {
	
	
	
	
    /* (non-Javadoc)
     * @see org.osiam.sc_sp_demo.td1.SayHelloPortType#sayHello(java.lang.String)
     */
	
	@ProtectedResource(action =
		@Action(attributes = {
				@AttributeType(attributeId = "urn:oasis:names:tc:xacml:1.0:action:action-id", dataType = "http://www.w3.org/2001/XMLSchema#string", attributeValue = "read")
		}), 
		resources = { @Resource(
				 attributes={
				 @AttributeType(attributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id",dataType="http://www.w3.org/2001/XMLSchema#anyURI",attributeValue="http://server.example.com/code/docs/developer-guide.html")
				 }
		) }

		)
    public String sayHello(String inputString) throws NotAuthExceptionFault {

		String tmp = "Hello " + inputString;
		
		return tmp;
    }
    

	

}
