/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebService;

import org.osiam.sc_sp_demo.td1.SayHelloPortType;

/**
 * say hello demo service.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
@WebService(targetNamespace = "http://www.osiam.org/sc_sp_demo/td1", 
portName="SayHelloPort",
serviceName="SayHelloService", 
endpointInterface="org.osiam.sc_sp_demo.td1.SayHelloPortType")
public class SayHelloPortTypeImpl implements  SayHelloPortType {
	
	private static Logger logger = Logger.getLogger(SayHelloPortTypeImpl.class.getName());


    public String sayHello(String inputString){
    	logger.log(Level.INFO, "Successfully accessed service");
		return "Hello " + inputString;
		
    }
}
