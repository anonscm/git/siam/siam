/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sc;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.ws.BindingProvider;

import org.osiam.sc_sp_demo.sc.callback_handler.SAMLCallbackhandler;
import org.osiam.sc_sp_demo.td1.NotAuthExceptionFault;

/**
 * demo service consumer for SIAM UC1,UC3 und UC4.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 * 
 */
public class WSClient {

	private static Logger logger = Logger.getLogger(WSClient.class.getName());

	/**
	 * main method to trigger
	 * 
	 * @param args
	 *            arg 0: UC (can be 1 oder 3) 
	 *            arg 1/2: SP endpoints
	 */
	public static void main(String[] args) {
		String usernameTD1 = "tester1";
		String usernameTD2 = "tester2";
		String endPointTD1 = null;
		String endPointTD2 = null;
		int useCase = 1;
		String command_usage = "Usage: java -classpath service_consumer*.jar org.osiam.sc_sp_demo.sc.WSClient UC# sp1_endpoint [sp2_endpoint]\n"
				+ "where UC# could be either 1 or 3\n"
				+ "and sp1_endpoint, sp2_endpoint are service provider endpoints, e.g. http://osiam-backend:8080/say_hello_td1_metro/services/say_hello";

		if (args.length > 0) {
			try {
				useCase = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("First argument must be an integer");
				System.exit(1);
			}
		} else {
			System.err.println(command_usage);
			System.exit(1);
		}

		/*
		 * check command line args
		 */
		if (args.length==1){
			System.out.println("using endpoints given in wsdl");
		}else{
			if (args.length == 2) {
				endPointTD1 = args[1];
			} else if (args.length == 3) {
				endPointTD1 = args[1];
				endPointTD2 = args[2];
			} else {
				System.err.println("Too many arguments...");
				System.err.println(command_usage);
				System.exit(1);
			}
		}

		/*
		 * getting ports
		 */
		org.osiam.sc_sp_demo.td1.SayHelloPortType port1 = null;
		org.osiam.sc_sp_demo.td2.SayHelloPortType port2 = null;
		try {
			if (args.length <= 2)
				port1 = getSP1Port(endPointTD1);
			if (args.length <= 3) {
				port1 = getSP1Port(endPointTD1);
				port2 = getSP2Port(endPointTD2);
			}
		} catch (MalformedURLException e) {
			logger.log(Level.SEVERE, "malformed urls: '" + endPointTD1
					+ "' and '" + endPointTD2 + "'");
			e.printStackTrace();
		}
		/*
		 * calling service in TD1
		 */
		sayHelloTD1(port1, usernameTD1);
		/*
		 * check if UC3 should be started
		 */
		if (useCase == 3) {
			/*
			 * get the saml token from response context of SP1 call and put it
			 * into the request context of SP2 call
			 */
			Map<String, Object> resCtx = ((BindingProvider) port1)
					.getResponseContext();
			((BindingProvider) port2).getRequestContext().put(
					SAMLCallbackhandler.SAML_TOKEN_PROP_KEY,
					resCtx.get(SAMLCallbackhandler.SAML_TOKEN_PROP_KEY));
			sayHelloTD2(port2, usernameTD2);
		}
	}

	private static void sayHelloTD1(
			org.osiam.sc_sp_demo.td1.SayHelloPortType port, String inputString) {
		logger.log(Level.INFO, "\n\ncalling service in td1");
		String resp = null;
		try {
			resp = port.sayHello(inputString);
		} catch (NotAuthExceptionFault e) {
			logger.log(Level.SEVERE, "an exception occured while calling sp1: "
					+ e);
			return;
		}
		logger.log(Level.INFO, "response: '" + resp + "'");
	}

	private static void sayHelloTD2(
			org.osiam.sc_sp_demo.td2.SayHelloPortType port, String inputString) {
		logger.log(Level.INFO, "\n\ncalling service in td2");
		String resp = port.sayHello(inputString);
		logger.log(Level.INFO, "response: '" + resp + "'");
	}

	private static org.osiam.sc_sp_demo.td1.SayHelloPortType getSP1Port(
			String endPoint) throws MalformedURLException {
		org.osiam.sc_sp_demo.td1.SayHelloPortType port = null;
		/*
		 * set ws-endpoint if not null
		 */
		if (endPoint != null && !endPoint.trim().equals("")) {
//			URL wsdlUrl = new URL(endPoint + "?wsdl");
//			org.osiam.sc_sp_demo.td1.SayHelloService service = new org.osiam.sc_sp_demo.td1.SayHelloService(
//					wsdlUrl, new QName("http://www.osiam.org/sc_sp_demo/td1",
//							"SayHelloService"));
			org.osiam.sc_sp_demo.td1.SayHelloService service = new org.osiam.sc_sp_demo.td1.SayHelloService();
			port = service.getSayHelloPort();
			((BindingProvider) port).getRequestContext().put(
					BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		} else {
			org.osiam.sc_sp_demo.td1.SayHelloService service = new org.osiam.sc_sp_demo.td1.SayHelloService();
			port = service.getSayHelloPort();
		}
		return port;
	}

	private static org.osiam.sc_sp_demo.td2.SayHelloPortType getSP2Port(
			String endPoint) throws MalformedURLException {
		org.osiam.sc_sp_demo.td2.SayHelloPortType port = null;
		/*
		 * set ws-endpoint if not null
		 */
		if (endPoint != null && !endPoint.trim().equals("")) {
//			URL wsdlUrl = new URL(endPoint + "?wsdl");
//			org.osiam.sc_sp_demo.td2.SayHelloService service = new org.osiam.sc_sp_demo.td2.SayHelloService(
//					wsdlUrl, new QName("http://www.osiam.org/sc_sp_demo/td2",
//							"SayHelloService"));
			org.osiam.sc_sp_demo.td2.SayHelloService service = new org.osiam.sc_sp_demo.td2.SayHelloService();
			port = service.getSayHelloPort();
			((BindingProvider) port).getRequestContext().put(
					BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		} else {
			org.osiam.sc_sp_demo.td2.SayHelloService service = new org.osiam.sc_sp_demo.td2.SayHelloService();
			port = service.getSayHelloPort();
		}
		return port;
	}

}
