/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.osiam.sc_sp_demo.td1.NotAuthExceptionFault;

/**
 * demo service consumer for SIAM UC1.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class WSClient_UC1 {

	private static Logger logger = Logger.getLogger(WSClient_UC1.class.getName());

	/**
	 * main method to trigger UC1
	 * 
	 * @param args no args used
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		String usernameTD1 = "tester";
		String endPointTD1 = null;
		if (args.length>0){
			endPointTD1 = args[0];
		}
	       /*
         * calling service in TD1
         */
		sayHelloTD1(endPointTD1, usernameTD1);
	}

	public static void sayHelloTD1(String endPoint, String inputString) throws MalformedURLException {
		org.osiam.sc_sp_demo.td1.SayHelloPortType port = null;
		if (endPoint!=null&&!endPoint.trim().equals("")){
			URL wsdlUrl = new URL(endPoint + "?wsdl");
			org.osiam.sc_sp_demo.td1.SayHelloService service = 
				new org.osiam.sc_sp_demo.td1.SayHelloService(wsdlUrl,new QName("http://www.osiam.org/sc_sp_demo/td1", "SayHelloService"));
			port = service.getSayHelloPort();
			((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		}else{
			org.osiam.sc_sp_demo.td1.SayHelloService service = new org.osiam.sc_sp_demo.td1.SayHelloService();
			port = service.getSayHelloPort();
		}
		
		logger.log(Level.INFO,"\n\ncalling service in td1");
		String resp = null;
		try {
			resp = port.sayHello(inputString);
		} catch (NotAuthExceptionFault e) {
			logger.log(Level.SEVERE, "an exception occured while calling sp1: " + e);
			return;
		}
		logger.log(Level.INFO, "response: '"+resp+"'");
	}
}

