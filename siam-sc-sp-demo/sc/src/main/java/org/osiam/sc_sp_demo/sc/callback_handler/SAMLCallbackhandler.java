/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sc.callback_handler;

import java.io.IOException;
import java.util.Map;
import com.sun.xml.wss.impl.callback.*;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.w3c.dom.Element;

/**
 * saml callback handler.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class SAMLCallbackhandler implements CallbackHandler {
	
	/**
	 * The SAML token is stored with this key in request context.
	 */
	public static final String SAML_TOKEN_PROP_KEY = "IssuedSAMLToken";

	/* (non-Javadoc)
	 * @see javax.security.auth.callback.CallbackHandler#handle(javax.security.auth.callback.Callback[])
	 */
	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof SAMLCallback) {
				SAMLCallback samlCallback = (SAMLCallback) callbacks[i];
				@SuppressWarnings("rawtypes")
				Map runtimeProp = samlCallback.getRuntimeProperties();
				Element samlAssertion = (Element) runtimeProp.get(SAML_TOKEN_PROP_KEY);
				samlCallback.setAssertionElement(samlAssertion);
			}
		}
	}
}
