/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.opensaml.xml.ConfigurationException;
import org.osiam.sc_sp_demo.td1.NotAuthExceptionFault;
import org.osiam.sc_sp_demo.td1.SayHelloPortType;
import org.osiam.xacml.KeyStoreConfig;
import org.osiam.xacml.PolicyEnforcementPointFactory;
import org.osiam.xacml.pep.api.Action;
import org.osiam.xacml.pep.api.Environment;
import org.osiam.xacml.pep.api.PolicyEnforcementPoint;
import org.osiam.xacml.pep.api.Attribute;
import org.osiam.xacml.pep.api.PolicyResult;
import com.sun.xml.wss.XWSSecurityException;

/**
 * say hello demo service with integrated PEP.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
@WebService(targetNamespace = "http://www.osiam.org/sc_sp_demo/td1", 
portName="SayHelloPort",
serviceName="SayHelloService", 
endpointInterface="org.osiam.sc_sp_demo.td1.SayHelloPortType")
public class SayHelloPortTypeImpl implements  SayHelloPortType {
	
	@Resource 
	WebServiceContext wsContext;
	
	private ServletContext servletContext;

	private static Logger logger = Logger.getLogger(SayHelloPortTypeImpl.class.getName());
	
	private KeyStoreConfig keyStoreConfig = null;


    public String sayHello(String inputString) throws NotAuthExceptionFault {
    	
    	servletContext = (ServletContext) wsContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		String subject = null;
		
		try {
			subject = SubjectParser.getSubjectStringFromWSContext(wsContext);
		} catch (XWSSecurityException e) {
			logger.log(Level.SEVERE, "error while getting subject from request");
			throw new SecurityException("error while getting subject from request",e);
		}
		String decision = checkAuthorization(subject);
		
		if (isAcceptedDecision(decision)){
			logger.log(Level.INFO, subject+"successfully accessed service, decision was: "+decision);
			return "Hello " + inputString + " // Decision: " + decision;
		}else{
			throw new NotAuthExceptionFault ("Authentication failed! // Decision: " + decision, null);
		}
		
    }
    
	
	/**
	 * method to check, if a subject is authorized to access the service.
	 * 
	 * @param subject the subject
	 * @return the decision
	 */
	private String checkAuthorization(String subject){
    	//create pep instance
		PolicyEnforcementPoint pep = null;
		String issuer = servletContext.getInitParameter("Issuer");
		String pdpUrl =  servletContext.getInitParameter("URL");
		keyStoreConfig.setEnableSignatureAndEncryption(servletContext.getInitParameter("enableSignatureAndEncryption"));
		keyStoreConfig.setKeyPwd(servletContext.getInitParameter("keyPwd").toCharArray());
		keyStoreConfig.setKeyStoreFilename(servletContext.getInitParameter("sp1Keystore"));
		keyStoreConfig.setKeystorePwd(servletContext.getInitParameter("keyStorePwd").toCharArray());
		keyStoreConfig.setPrivateKeyAlias(servletContext.getInitParameter("privateKeyAlias"));
		keyStoreConfig.setPublicKeyAlias(servletContext.getInitParameter("publicKeyAlias"));
		keyStoreConfig.setTrustStoreFilename(servletContext.getInitParameter("sp1Truststore"));
		
  		try {
			pep = PolicyEnforcementPointFactory.getPolicyEnforcementPoint(
					pdpUrl, issuer, keyStoreConfig);
		} catch (ParserConfigurationException e) {
			logger.warning(e.getMessage());
		} catch (ConfigurationException e) {
			logger.warning(e.getMessage());
		}
		//get configured parameters
		String resourceID = servletContext.getInitParameter("resourceID");
		String resourceType = servletContext.getInitParameter("resourceType");
		String resourceString = servletContext.getInitParameter("resourceString");
		String subjectID = servletContext.getInitParameter("subjectID");
		String subjectType = servletContext.getInitParameter("subjectType");
		String actionString = servletContext.getInitParameter("actionString");
		String actionID = servletContext.getInitParameter("actionID");
		String actionType = servletContext.getInitParameter("actionType");
		String environmentString = servletContext.getInitParameter("environmentString");
		String environmentID = servletContext.getInitParameter("environmentID");
		String environmentType = servletContext.getInitParameter("environmentType");
		//prepare request
		Environment environment = null;
		if(environmentString != null && environmentID != null && environmentType != null){
			environment= new Environment(new Attribute(environmentID, environmentType, environmentString));
		}
		Action action = null;
		if(actionString != null && actionID != null && actionType != null){
			action=new Action(new Attribute(actionID, actionType, actionString));
		}	
		List<org.osiam.xacml.pep.api.Resource> resources = new ArrayList<org.osiam.xacml.pep.api.Resource>();
		resources.add(new org.osiam.xacml.pep.api.Resource(new Attribute(resourceID, resourceType, resourceString)));
		List<org.osiam.xacml.pep.api.Subject> subjects = new ArrayList<org.osiam.xacml.pep.api.Subject>();
		subjects.add(new org.osiam.xacml.pep.api.Subject(new Attribute(subjectID, subjectType, subject)));
		//query pdp
		PolicyResult policyResult = pep.query(new org.osiam.xacml.pep.api.Subjects(subjects), new org.osiam.xacml.pep.api.Resources(resources), action, environment);
		return policyResult.getDecision();
	}
	
    /**
     * helper method to check if a given decision is in the list of accepted decisions
     * 
     * @param decision the decision
     * @return true if decision is accepted
     */
    private boolean isAcceptedDecision(String decision){
    	String permitStatements = servletContext.getInitParameter("PermitStatements");
    	String[] permitStatementsArray = permitStatements.split( Pattern.quote( "," ) );
    	for (int i = 0; i < permitStatementsArray.length; i++) {
			String permitStatement = permitStatementsArray[i];
			if (decision.trim().equals(permitStatement.trim())){
				return true;
			}	
    	}
    	return false;
    }
}
