/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp;

import java.util.Set;
import java.util.regex.Pattern;

import javax.security.auth.Subject;
import javax.xml.ws.WebServiceContext;

import com.sun.xml.wss.SubjectAccessor;
import com.sun.xml.wss.XWSSecurityException;
import com.sun.xml.wss.saml.Assertion;

/**
 * This class provides several abstract methods for parsing SAML Assertions from
 * different kinds of contexts and for parsing the name out of a SAML Assertion.
 * 
 * @author Fabian Foerster
 * 
 */
public class SubjectParser {

	/**
	 * Tries to extract an SAML Assertion from a webservice context. 
	 * In order for this to work, the webservice must have a subject which has been authenticated via SAML2
	 * @param context The webservice context containing an authenticated subject
	 * @return the Assertion used to authenticate the subject
	 * @throws XWSSecurityException
	 */
	public static Assertion getAssertionFromWebServiceContext(
			WebServiceContext context) throws XWSSecurityException {
		if(context==null)
			return null;
		Subject subj = SubjectAccessor.getRequesterSubject(context);
		if(subj==null)
			return null;
		Set<Object> set = subj.getPublicCredentials();
		for (Object obj : set) {
			if (obj instanceof Assertion) {
				Assertion samlAssertion = (Assertion) obj;
				return samlAssertion;
			}
		}
		return null;
	}

	/**
	 * Extracts the String representing the authenticated subject from a SAML2 Assertion
	 * @param samlAssertion the assertion used for authentication of the subject.
	 * @return String representation of the nameID 
	 */
	public static String getSubjectStringFromAssertion(Assertion samlAssertion) {

		if (samlAssertion != null && samlAssertion.getSubject() != null) {
			String tmp = samlAssertion.getSubject().getNameId().getValue();
			String[] tmparray = tmp.split(Pattern.quote(","));
			tmp = tmparray[0];
			tmparray = tmp.split(Pattern.quote("="));
			return tmparray[1];
		} else
			return "";
	}

	/**
	 * Tries to extract an SAML Assertion from an authenticated subject. 
	 * @param subject the authenticated subject 
	 * @return the Assertion used to authenticate the subject
	 * @throws XWSSecurityException
	 */
	public static Assertion getAssertionFromAuthSubject(Subject subject)
			throws XWSSecurityException {
		if (subject == null)
			return null;
		Set<Object> set = subject.getPublicCredentials();
		for (Object obj : set) {
			if (obj instanceof Assertion) {
				Assertion samlAssertion = (Assertion) obj;
				return samlAssertion;
			}
		}
		return null;
	}

	/**
	 * Extracts the string representing the authenticated subject from a webservice context if the subject has been authenticated via SAML2
	 * @param context the webservice context containing the authentication information
	 * @return the string contained in the nameID
	 * @throws XWSSecurityException
	 */
	public static String getSubjectStringFromWSContext(	WebServiceContext context) throws XWSSecurityException {
		return getSubjectStringFromAssertion(getAssertionFromWebServiceContext(context));
	}	
	
	/**
	 * Extracts the string representing the SAML nameID from an authenticated subject, if the subject has been authenticated via SAML2
	 * @param subject SAML 2 authenticated subject.
	 * @return the string contained in the nameID
	 * @throws XWSSecurityException
	 */
	public static String getSubjectStringFromAuthSubject(	Subject subject) throws XWSSecurityException {
		return getSubjectStringFromAssertion(getAssertionFromAuthSubject(subject));
	}

}
