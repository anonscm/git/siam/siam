/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.osiam.sc_sp_demo.td2.SayHelloPortType;



/**
 * say hello demo service.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
@WebService(targetNamespace = "http://www.osiam.org/sc_sp_demo/td2", 
portName= "SayHelloPort",
serviceName= "SayHelloService", 
endpointInterface= "org.osiam.sc_sp_demo.td2.SayHelloPortType")
public class SayHelloPortTypeImpl implements  SayHelloPortType {
	
	@Resource WebServiceContext wsContext; 

    /* (non-Javadoc)
     * @see org.osiam.sc_sp_demo.td1.SayHelloPortType#sayHello(java.lang.String)
     */
    public String sayHello(String inputString) {
//    	http://weblogs.java.net/blog/2009/05/26/overriding-webservicecontext-metro-handle-security-related-methods
//    	System.out.println("accessed by "+wsContext.getUserPrincipal().getName());
        return "Hello "+ inputString;
    }
}
