/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.osiam.sc_sp_demo.sp.validators;


import com.sun.xml.wss.impl.callback.PasswordValidationCallback;
import com.sun.xml.wss.impl.callback.PasswordValidationCallback.PasswordValidationException;
import com.sun.xml.wss.impl.callback.PasswordValidationCallback.Request;

/**
 * demo username/password authentication validator class.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class PasswordValidator implements PasswordValidationCallback.PasswordValidator {
	
	private static final String USERNAME = "test";
	
	private static final String PASSWORD = "test";
	
    /* (non-Javadoc)
     * @see com.sun.xml.wss.impl.callback.PasswordValidationCallback.PasswordValidator#validate(com.sun.xml.wss.impl.callback.PasswordValidationCallback.Request)
     */
    public boolean validate(Request request) throws PasswordValidationException {
        PasswordValidationCallback.PlainTextPasswordRequest ptreq 
            = (PasswordValidationCallback.PlainTextPasswordRequest) request;
        return USERNAME.equals(ptreq.getUsername()) 
        && PASSWORD.equals(ptreq.getPassword());
    }
}
