/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.security_annotations;

import org.osiam.xacml.securtiy_annotations.Action;
import org.osiam.xacml.securtiy_annotations.AttributeType;
import org.osiam.xacml.securtiy_annotations.ProtectedResource;
import org.osiam.xacml.securtiy_annotations.Resource;

/**
 * annotated demo class.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 * 
 */
public class ServiceClass {


	@ProtectedResource(action =
			@Action(attributes = {
					@AttributeType(attributeId = "1", dataType = "", attributeValue = ""),
					@AttributeType(attributeId = "2", dataType = "", attributeValue = "") 
			}), 
			resources = { @Resource(
					 attributes = {
					 @AttributeType(attributeId="1",dataType="",attributeValue=""),
					 @AttributeType(attributeId="2",dataType="",attributeValue="")
					 }
			) }
	)
	public void testService() {

	}

}
