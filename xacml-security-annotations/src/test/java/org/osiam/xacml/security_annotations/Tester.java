/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.security_annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;


/**
 * demo/test class.
 * 
 * @author Christian Preilowski (c.preilowski@tarent.de)
 *
 */
public class Tester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Class<ServiceClass> classObj = ServiceClass.class;

		Annotation[] annotations = classObj.getAnnotations();
		for (Annotation annotation : annotations) {
			System.out.println("class-annotation: " + annotation.toString());
		}
		Method [] methods = classObj.getMethods();
		for (Method method : methods) {
			Annotation[] methodAnnotations = method.getAnnotations();
			for (Annotation annotation : methodAnnotations) {
				System.out.println("method-annotation: " + annotation.toString());
			}
		}
	}
}
