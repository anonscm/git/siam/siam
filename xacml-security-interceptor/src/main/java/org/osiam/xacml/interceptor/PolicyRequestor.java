/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.interceptor;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.opensaml.xml.ConfigurationException;
import org.osiam.xacml.KeyStoreConfig;
import org.osiam.xacml.PolicyEnforcementPointFactory;
import org.osiam.xacml.pep.api.Action;
import org.osiam.xacml.pep.api.Attribute;
import org.osiam.xacml.pep.api.Environment;
import org.osiam.xacml.pep.api.PolicyEnforcementPoint;
import org.osiam.xacml.pep.api.PolicyResult;
import org.osiam.xacml.pep.api.Resource;
import org.osiam.xacml.pep.api.Resources;
import org.osiam.xacml.pep.api.Subjects;
import org.osiam.xacml.securtiy_annotations.AttributeType;
import org.osiam.xacml.securtiy_annotations.ProtectedResource;

/**
 * contains the actual logic for processing the annotations of the requested
 * resource and invocation of a PEP.
 * 
 * @author Fabian Foerster
 * 
 */
class PolicyRequestor {
	private final static Logger logger = Logger.getLogger(PolicyRequestor.class
			.getCanonicalName());

	private PolicyEnforcementPoint pep;
	private Resources resources;
	private Action action;

	/**
	 * Creates a policy requestor.
	 * @param pdp_url URL of the PDP to query
	 * @param issuer The issuer of the webservice call to the PDP
	 */
	public PolicyRequestor(String pdp_url, String issuer, KeyStoreConfig storeconfig) {
		try {
			pep = PolicyEnforcementPointFactory.getPolicyEnforcementPoint(
					pdp_url, issuer, storeconfig);
		} catch (ParserConfigurationException e) {
			logger.warning(e.getMessage());
		} catch (ConfigurationException e) {
			logger.warning(e.getMessage());
		}
	}

	/**
	 * Invokes the PEP to query for the policy results of the specified parameters
	 * @param subjects	the subjects originating the access to be verified
	 * @param protectedResource the protected resource
	 * @param environment the environment
	 * @return the decision of the PDP.
	 */
	public PolicyResult queryPolicy(Subjects subjects,
			ProtectedResource protectedResource, Environment environment) {
		processAnnotations(protectedResource);

		// here goes actual invocation code
		PolicyResult policyResult = pep.query(subjects, resources, action,
				environment);

		logger.info("successfully queried the PDP");

		return policyResult;
	}

	/**
	 * Converts the security annotations of a protected resource to SIAM PEP API Artifacts 
	 * @param attribute the annotation
	 */
	private void processAnnotations(ProtectedResource protectedResource) {
		resources = convertResources(protectedResource.resources());
		action = convertAction(protectedResource.action());
	}

	/**
	 * converts the security annotation Action to a siam PEP API Action object 
	 * @param action the annotation
	 * @return the API Artifact
	 */
	private Action convertAction(
			org.osiam.xacml.securtiy_annotations.Action action) {
		return new Action(convertAttributes(action.attributes()));
	}

	/**
	 * converts the security annotation Resources to a siam PEP API Resources object 
	 * @param resources the annotation
	 * @return the API Artifact
	 */
	private Resources convertResources(
			org.osiam.xacml.securtiy_annotations.Resource[] resources) {
		Resource[] target = new Resource[resources.length];
		for (int i = 0; i < resources.length; i++) {
			target[i] = convertResource(resources[i]);
		}
		return new Resources(Arrays.asList(target));
	}

	/**
	 * converts the security annotation Resource to a siam PEP API Resource object 
	 * @param resource the annotation
	 * @return the API Artifact
	 */
	private Resource convertResource(
			org.osiam.xacml.securtiy_annotations.Resource resource) {
		return new Resource(convertAttributes(resource.attributes()));
	}

	/**
	 * converts the security annotation Attributes to a list siam PEP API Attribute object 
	 * @param attribute the annotation
	 * @return the API Artifact
	 */
	private List<Attribute> convertAttributes(AttributeType[] attributes) {
		Attribute[] target = new Attribute[attributes.length];
		for (int i = 0; i < attributes.length; i++) {
			target[i] = convertAttribute(attributes[i]);
		}
		return Arrays.asList(target);
	}

	/**
	 * converts the security annotation Attribute to a siam PEP API Attribute object 
	 * @param attribute the annotation
	 * @return the API Artifact
	 */
	private Attribute convertAttribute(AttributeType attribute) {
		return new Attribute(attribute.attributeId(), attribute.dataType(),
				attribute.attributeValue());
	};
}
