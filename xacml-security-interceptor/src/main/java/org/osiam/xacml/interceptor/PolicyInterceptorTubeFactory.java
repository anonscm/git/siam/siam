/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.interceptor;

import java.util.logging.Logger;

import javax.xml.ws.WebServiceException;

import com.sun.xml.ws.api.pipe.Tube;
import com.sun.xml.ws.assembler.ClientTubelineAssemblyContext;
import com.sun.xml.ws.assembler.ServerTubelineAssemblyContext;
import com.sun.xml.ws.assembler.TubeFactory;


/**
 * Factory for use by metro. Do not use manually.
 * @author Fabian Foerster
 *
 */
public class PolicyInterceptorTubeFactory implements TubeFactory {
    private static final Logger logger = Logger.getLogger(PolicyInterceptorTubeFactory.class.getName());
 
    public Tube createTube(ClientTubelineAssemblyContext context) throws WebServiceException {
        logger.info("Creating client-side interceptor tube");
 
        return new PolicyInterceptorMetroTube(context.getTubelineHead(), PolicyInterceptorMetroTube.Side.Client);
    }
 
    public Tube createTube(ServerTubelineAssemblyContext context) throws WebServiceException {
        logger.info("Creating server-side interceptor tube");
 
        return new PolicyInterceptorMetroTube(context.getTubelineHead(), PolicyInterceptorMetroTube.Side.Endpoint);
    }
}