/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.interceptor;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.security.auth.Subject;
import javax.xml.namespace.QName;

import org.osiam.xacml.pep.api.Attribute;
import org.osiam.xacml.pep.api.PolicyResult;
import org.osiam.xacml.pep.api.Subjects;
import org.osiam.xacml.securtiy_annotations.ProtectedResource;

import com.sun.xml.ws.api.message.Packet;
import com.sun.xml.ws.api.model.JavaMethod;
import com.sun.xml.ws.api.pipe.NextAction;
import com.sun.xml.ws.api.pipe.Tube;
import com.sun.xml.ws.api.pipe.TubeCloner;
import com.sun.xml.ws.api.pipe.helper.AbstractFilterTubeImpl;
import com.sun.xml.wss.XWSSecurityException;

/**
 * Metro tube querying an Osiam XACML PDP. Up to now, the appropriate Subject is
 * taken from the uid String parameter of the invoked method. Methods to be
 * protected by this Interceptor MUST have a meaningfull uid - parameter.
 * 
 * @author Fabian Foerster
 * 
 */
public class PolicyInterceptorMetroTube extends AbstractFilterTubeImpl {
	private final static Logger logger = Logger
			.getLogger(PolicyInterceptorMetroTube.class.getCanonicalName());

	private final Side side;

	static enum Side {
		Client, Endpoint
	}

	/**
	 * Private cloning constructor
	 * @param original original Tube
	 * @param cloner tubeCloner
	 */
	private PolicyInterceptorMetroTube(PolicyInterceptorMetroTube original,
			TubeCloner cloner) {
		super(original, cloner);
		this.side = original.side;
	}

	@Override
	/**
	 * Copy Constructor calling the cloning constructor
	 */
	public PolicyInterceptorMetroTube copy(TubeCloner cloner) {
		return new PolicyInterceptorMetroTube(this, cloner);
	}
	

	/**
	 * standard constructor called by the framework
	 * @param tube 
	 * @param side
	 */
	PolicyInterceptorMetroTube(Tube tube, Side side) {
		super(tube);
		this.side = side;
	}

	/**
	 * Checks, if the invoked method on the implementing class is a protected resource and if so, returns the protected resource annotation object
	 * @param request the request to be checked for calling a protected resource
	 * @return the protected resource annotation object
	 */
	private ProtectedResource checkForProtectedResource(Packet request) {
		QName wsdlOperation = request.getWSDLOperation();
		JavaMethod javaMethod = request.endpoint.getSEIModel()
				.getJavaMethodForWsdlOperation(wsdlOperation);
		Method calledMethod = javaMethod.getMethod();

		ProtectedResource protectedResource = calledMethod
				.getAnnotation(ProtectedResource.class);

		if (protectedResource == null) {
			logger.info("not a protected resource");
			return null;
		} else
			return protectedResource;
	}

	/**
	 * Extracts the authenticated subject from the calling context if the message caller has been authenticated via SAML 2
	 * @param request the message packet which must have been authenticated via SAML 2 token
	 * @return the PEP API Subjects object representing the authenticated caller. Or null, if the message has not yet been authenticated. 
	 */
	//TODO: handle case of non-SAML authentication
	private Subjects extractSubjects(Packet request) {
		Subject invSubject = (Subject) request.invocationProperties
				.get("javax.security.auth.Subject");

		if (invSubject != null) {
			try {
				String subjectString = SubjectParser
						.getSubjectStringFromAuthSubject(invSubject);

				logger.info(String.format("hello %s", subjectString));
			
				logger.info("subjectId= "+PEPConfigData.getSubjectId());
				logger.info("subjectType= "+PEPConfigData.getSubjectType());
				 
				Attribute subjectAttribute=new Attribute(PEPConfigData.getSubjectId(), PEPConfigData.getSubjectType(), subjectString);
				org.osiam.xacml.pep.api.Subject subject= new org.osiam.xacml.pep.api.Subject(subjectAttribute);
				Subjects subjects=new Subjects(subject);
				return subjects;
				
			} catch (XWSSecurityException e) {
				logger.warning(e.getMessage());
			}
		}
		return null;
	}
	
	/**
	 * Invokes the PEP to call the PDP for access decision to the protected resource by the authenticated subject. 
	 * @param subjects PEP API subject which has been authenticated.
	 * @param protectedResourece Resource which is called by the subject
	 * @return The String representation of the decision of the PDP
	 */
	private String callPep(Subjects subjects, ProtectedResource protectedResourece){
		logger.info("calling PEP");
		
		PolicyRequestor policyRequestor=new PolicyRequestor(PEPConfigData.getPdpUrl(), PEPConfigData.getIssuer()
				,PEPConfigData.getStoreConfig());
		logger.info("pdpurl "+PEPConfigData.getPdpUrl());
		logger.info("issuer "+PEPConfigData.getIssuer());
		PolicyResult policyResult = policyRequestor.queryPolicy(subjects, protectedResourece, PEPConfigData.getEnvironment());
		
		return policyResult.getDecision();
	}

	@Override
	public NextAction processRequest(Packet request) {
		logger.info(String.format(
				"Message request intercepted on %s side for %s", side,
				request.endpoint.getImplementationClass().getCanonicalName()));

	
		ProtectedResource protectedResource = checkForProtectedResource(request);

		if (protectedResource == null) {
			return super.processRequest(request);
		}
		
		Subjects subjects = extractSubjects(request);
		String decision =null;
		
		if(subjects!=null){
			decision = callPep(subjects, protectedResource);
		}else{
			//TODO: better exception?
			throw new SecurityException("Subject has not been authenticated (at least not via SAML2)");
		}
		
		logger.info("decision is "+decision);
		

		if (PEPConfigData.getPermitStatements().contains(decision)){
			return super.processRequest(request);
		}else
			throw new SecurityException("Subject has not been authorised to access this resource.");
	}

}
