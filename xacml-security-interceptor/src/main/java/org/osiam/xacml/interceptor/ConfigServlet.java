/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.interceptor;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.osiam.xacml.KeyStoreConfig;
import org.osiam.xacml.pep.api.Attribute;
import org.osiam.xacml.pep.api.Environment;

/**
 * This class provides a workaround for making necessary configuration parameters available. This should be replaced by a more elegant solution as soon as possible.
 * 
 * @author Fabian Foerster
 *
 */
public class ConfigServlet extends HttpServlet{

	private static final long serialVersionUID = 7794604062959599704L;
	
	private static final String KEY_STORE_LOCATION_KEY = "sp1Keystore";
	private static final String TRUST_STORE_LOCATION_KEY = "sp1Truststore";
	private static final String KEY_STORE_ALIAS_KEY = "privateKeyAlias";
	private static final String TRUST_STORE_ALIAS_KEY = "publicKeyAlias";
	private static final String KEYSTORE_PASSWORD_KEY = "keyStorePwd";
	private static final String KEY_PASSWORD_KEY = "keyPwd";
	private static final String ENVIRONMENT_STRING_KEY = "environmentString";
	private static final String ENVIRONMENT_ID_KEY = "environmentID";
	private static final String ENVIRONMENT_TYPE_KEY = "environmentType";
	private static final String ISSUER_KEY = "Issuer";
	private static final String PDP_URL_KEY = "URL";
	private static final String SUBJECT_ID_KEY = "subjectID";
	private static final String SUBJECT_TYPE_KEY = "subjectType";
	private static final String PERMIT_STATEMENTS_KEY = "PermitStatements";
	private static final String PERMIT_STATEMENTS_SEPARATOR = ",";
	private static final String ENABLE_SIGNATURE_AND_ENCRYPTION_KEY = "enableSignatureAndEncryption";
	
	private Environment generateEnvironment(ServletContext servletContext){
		String environmentString = servletContext.getInitParameter(ENVIRONMENT_STRING_KEY);
		String environmentID = servletContext.getInitParameter(ENVIRONMENT_ID_KEY);
		String environmentType = servletContext.getInitParameter(ENVIRONMENT_TYPE_KEY);
		return new Environment(new Attribute(environmentID, environmentType, environmentString));
	}
	
	@Override
	/**
	 * Initialises the servlet and parses the necessary config parameters from web.xml 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init();
		 
		ServletContext servletContext=config.getServletContext();
		
		PEPConfigData.setEnvironment(generateEnvironment(servletContext));
		PEPConfigData.setIssuer(servletContext.getInitParameter(ISSUER_KEY));
		PEPConfigData.setPdpUrl(servletContext.getInitParameter(PDP_URL_KEY));
		PEPConfigData.setSubjectId(servletContext.getInitParameter(SUBJECT_ID_KEY));
		PEPConfigData.setSubjectType(servletContext.getInitParameter(SUBJECT_TYPE_KEY));
		
		String permitStatement = servletContext.getInitParameter(PERMIT_STATEMENTS_KEY);
		Set<String> permitStatements = new HashSet<String>();
		for (String permit : permitStatement.split( Pattern.quote( PERMIT_STATEMENTS_SEPARATOR ) )){
			permitStatements.add(permit.trim());
		}
		PEPConfigData.setPermitStatements(permitStatements );
		
			KeyStoreConfig storeConfig = new KeyStoreConfig();
			storeConfig.setKeyStoreFilename(servletContext.getInitParameter(KEY_STORE_LOCATION_KEY));
			storeConfig.setTrustStoreFilename(servletContext.getInitParameter(TRUST_STORE_LOCATION_KEY));
			storeConfig.setPrivateKeyAlias(servletContext.getInitParameter(KEY_STORE_ALIAS_KEY));
			storeConfig.setPublicKeyAlias(servletContext.getInitParameter(TRUST_STORE_ALIAS_KEY));
			storeConfig.setKeystorePwd(servletContext.getInitParameter(KEYSTORE_PASSWORD_KEY).toCharArray());
			storeConfig.setKeyPwd(servletContext.getInitParameter(KEY_PASSWORD_KEY).toCharArray());
			storeConfig.setEnableSignatureAndEncryption(servletContext.getInitParameter(ENABLE_SIGNATURE_AND_ENCRYPTION_KEY));
					
			PEPConfigData.setStoreConfig(storeConfig);
	}
	
}
