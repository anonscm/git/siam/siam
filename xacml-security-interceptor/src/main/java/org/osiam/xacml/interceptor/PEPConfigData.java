/**
 * Copyright (C) 2011 tarent Solutions <info@tarent.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.osiam.xacml.interceptor;

import java.util.Set;

import org.osiam.xacml.KeyStoreConfig;
import org.osiam.xacml.pep.api.Environment;

/**
 * This class stores configuration parameters for PEP querying components such as filters etc.
 * @author Fabian Foerster
 *
 */
public class PEPConfigData {
	private static Environment environment;
	private static String pdpUrl;
	private static String issuer;
	private static String subjectId;
	private static String subjectType;
	private static Set<String> permitStatements;
	private static KeyStoreConfig storeConfig;
	
	public static KeyStoreConfig getStoreConfig() {
		return storeConfig;
	}
	public static void setStoreConfig(KeyStoreConfig storeConfig) {
		PEPConfigData.storeConfig = storeConfig;
	}
	public static Environment getEnvironment() {
		return environment;
	}
	public static void setEnvironment(Environment environment) {
		PEPConfigData.environment = environment;
	}
	public static String getPdpUrl() {
		return pdpUrl;
	}
	public static void setPdpUrl(String pdpUrl) {
		PEPConfigData.pdpUrl = pdpUrl;
	}
	public static String getIssuer() {
		return issuer;
	}
	public static void setIssuer(String issuer) {
		PEPConfigData.issuer = issuer;
	}
	public static String getSubjectId() {
		return subjectId;
	}
	public static void setSubjectId(String subjectId) {
		PEPConfigData.subjectId = subjectId;
	}
	public static String getSubjectType() {
		return subjectType;
	}
	public static void setSubjectType(String subjectType) {
		PEPConfigData.subjectType = subjectType;
	}
	public static void setPermitStatements(Set<String> permitStatements) {
		PEPConfigData.permitStatements = permitStatements;
	}
	public static Set<String> getPermitStatements() {
		return permitStatements;
	}

}
